<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class Create_Tb_Courses_Table extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();
        Schema::create('tb_courses',function(Blueprint $table){
            $table->increments("id");
            $table->enum("status", ["Draft", "Pending", "Accept", "Reject"]);
            $table->string("teacher_id");
            $table->integer("tb_professional_id")->references("id")->on("tb_professional");
            $table->integer("tb_categories_type_id")->references("id")->on("tb_categories_type");
            $table->string("title");
            $table->text("summary")->nullable();
            $table->string("image")->nullable();
            $table->string("promo_video")->nullable();
            $table->enum("price_type", ["Free", "Paid"]);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_courses');
    }

}