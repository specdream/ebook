<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class Create_Tb_Languages_Settings_Table extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();
        Schema::create('tb_languages_settings',function(Blueprint $table){
            $table->increments("id");
            $table->integer("tb_languages_id")->references("id")->on("tb_languages");
            $table->string("key_name");
            $table->string("value");
            $table->tinyInteger("uniq_record")->default(0)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_languages_settings');
    }

}