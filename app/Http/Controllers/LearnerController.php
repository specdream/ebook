<?php

namespace App\Http\Controllers;

use App\Role;
use App\Learners;
use App\Tb_Professional;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Helper;
use App;
use App\User;
use App\Tb_Purchased_Course;
use App\Tb_Courses;
use App\Lession;
use App\Tb_Categories_Type;
use App\Tb_Test_Code_Generated;
use App\Tb_Learner_Projects;
use Session;
use Redirect;
class LearnerController extends Controller
{
    
    public function profile()
    {
        $tb_professional=Tb_Professional::pluck("name", "id")->prepend('Please select', '');
        $user=User::find(\Auth::user()->id);
        return view('learner/profile', compact('user','tb_professional'));
    }
    public function paymenthistory()
    {
        $Purchased_Course=Tb_Purchased_Course::where('learner_id',\Auth::user()->id)->get();
        return view('learner/paymenthistory', compact('Purchased_Course'));
    }
    public function purchasedcourse()
    {
        $Purchased_Course=Tb_Purchased_Course::where('learner_id',\Auth::user()->id)->get();
        return view('learner/purchasedcourse', compact('Purchased_Course'));
    }
    public function keyGenerate()
    {
        $Key_Generated=Tb_Test_Code_Generated::where('learner_id',\Auth::user()->id)->get();
        $Purchased_Course=Tb_Purchased_Course::where('learner_id',\Auth::user()->id)->get();
        return view('learner/keygenerate', compact('Key_Generated','Purchased_Course'));
    }
    public function examKeyValidation(Request $request)
    {
        $Key_Generated=Tb_Test_Code_Generated::where('learner_id',\Auth::user()->id)->where('key_code',$request['key_code'])->first();
        if($Key_Generated){
            if($Key_Generated->is_used=='yes'){
                return Redirect::to('learner/writeExam')->withErrors([ 'used']);
            }else{
                 return Redirect::to('learner/viewExam/'.$Key_Generated->id)->withErrors([ 'success']);
            }
        }else{
            return Redirect::to('learner/writeExam')->withErrors([ 'invalid']);
        }

    }
    public function viewExam($id)
    {
        if(\Auth::check()){
        $Key_Generated=Tb_Test_Code_Generated::where('learner_id',\Auth::user()->id)->where('id',$id)->where('is_used','no')->first();
        if($Key_Generated){
            return view('learner/viewExam',compact('Key_Generated'));
        }else{
            return Redirect::to('learner/writeExam')->withErrors([ 'invalid']);
        }
    }else{
        return Redirect::to('/');
    }
        
    }
    public function keyGenerateAdd(Request $request)
    {
        $request['learner_id']=\Auth::user()->id;
        $request['key_code']=mt_rand(1000000000, 9999999999);
        $tcode=Tb_Test_Code_Generated::where('learner_id',\Auth::user()->id)->where('course_id',$request['course_id'])->where('lesson_id',$request['lesson_id'])->where('is_used','no')->get();
        if(count($tcode)==0){
            $attempt=Tb_Test_Code_Generated::where('learner_id',\Auth::user()->id)->where('course_id',$request['course_id'])->where('lesson_id',$request['lesson_id'])->where('is_used','no')->orderBy('id','desc')->first();
            if($attempt){
                $request['attempt']=$attempt->attempt+1;
            }else{
              $request['attempt']='1';  
            }         

            Tb_Test_Code_Generated::create($request->all());

            return Redirect::to('learner/keygenerate')->withErrors([ 'success']);
        }else{
            return Redirect::to('learner/keygenerate')->withErrors([ 'pending']);
        }
    }   
    public function writeExam()
    {
        if(\Auth::check()){
            return view('learner/writeExam');
        }else{
            return Redirect::to('');
        }
        
    }
    public function getLesson(Request $request)
    {
        $lesson=Lession::where('course_id',$request['id'])->where('assessment_test','yes')->get();

        $aValues=array();
        $Key_Generated=Tb_Test_Code_Generated::where('learner_id',\Auth::user()->id)->where('course_id',$request['id'])->orderBy('id','DESC')->first();
        if($Key_Generated){
            if($Key_Generated->is_used=='yes' && $Key_Generated->is_pass=='yes'){

                foreach ($lesson as $key => $value) {
                    if($value->id>$Key_Generated->lesson_id){
                       $aValues['id']=$value->id;
                       $aValues['name']=\Helper::getLangValue(\Helper::getLessonKey($value->id,'key'));
                       $aValues['msg']='';
                       break;
                   }

             }

            }else if($Key_Generated->is_used=='yes' && $Key_Generated->is_pass=='no'){
                $aValues['id']=$Key_Generated->lesson_id;
                $aValues['name']=\Helper::getLangValue(\Helper::getLessonKey($Key_Generated->lesson_id,'key'));
                $aValues['msg']='';
            }else{
                $aValues['msg']='Already generated key is NOT YET used. So you may use it for this key';
            }
        }else{
            foreach ($lesson as $key => $value) {
                if($key==0){
                    $aValues['id']=$value->id;
                    $aValues['name']=\Helper::getLangValue(\Helper::getLessonKey($value->id,'key'));
                    $aValues['msg']='';
                }
           
        }
        }
        

        echo json_encode($aValues);
    }
    public function learnerDetail($id)
    {
        $aCat_courses = Tb_Courses::where('status','Accept')->where('id',$id)->orderBy('id', 'desc')->with("tb_languages_settings")->with("tb_languages_settings_summary")->with("creater_name")->with("teacher_name")->first();  
        $aCat_name=Tb_Categories_Type::select('name')->where('id',$aCat_courses->cat_id)->first();    
        return view('learner/detail', compact('aCat_courses','aCat_name'));
    }
    public function learnerLesson($course_id)
    {
        $course_id = $course_id; 
        $course = Tb_Courses::where('id',$course_id)->get();
        $PurchCourse=Tb_Purchased_Course::where('learner_id',\Auth::user()->id)->where('course_id',$course_id)->where('payment_status','paid')->get();
        if(count($PurchCourse)>0){
            $course_name=$course[0]->title;
            $lessions = Lession::where('course_id',$course_id)->get(); 
            return view('learner/lesson',compact('lessions','course_id','course_name'));
        }else{
            $Purchased_Course=Tb_Purchased_Course::where('learner_id',\Auth::user()->id)->get();
        return view('learner/purchasedcourse', compact('Purchased_Course'));
        }
        
    }
    public function update(Request $request)
    {
        $id=\Auth::user()->id;
        $learner = Learners::findOrFail($id); 
        $request = $this->saveFiles($request);
        $request['role_id'] = '2';
        $request['username'] = $request->name;
        $learner->update($request->all());

        return Redirect::to('learner/profile')->withErrors([ 'updated successfully.']);
    }
    public function completedCourse(Request $request)
    {
        $PurchCourse=Tb_Purchased_Course::where('learner_id',\Auth::user()->id)->where('payment_status','paid')->get();
        $aCourse_id=array();
        foreach($PurchCourse as $key=>$value){
            $Key_Generated_yes=Tb_Test_Code_Generated::where('learner_id',\Auth::user()->id)->where('course_id',$value->course_id)->orderBy('id','DESC')->where('is_pass','yes')->get();
            $lesson=Lession::where('course_id',$value->course_id)->where('assessment_test','yes')->get();  

            if(count($Key_Generated_yes)==count($lesson)){
                $aCourse_id[]=$value->course_id;
            }
        }

    $Completed_Course=Tb_Purchased_Course::where('learner_id',\Auth::user()->id)->whereIn('course_id',$aCourse_id)->where('payment_status','paid')->get();
        return view('learner/completedCourse', compact('Completed_Course'));
    }

    public function lessonView($cid,$lid)
    {
         $lesson=Lession::where('course_id',$cid)->where('id',$lid)->first();          
         return view('learner/lessonView',compact('lesson'));
    }
    public function manageProject($cid)
    {
         $learner_projects=Tb_Learner_Projects::where('learner_id',\Auth::user()->id)->where('course_id',$cid)->get();          
         return view('learner/manageProject',compact('learner_projects','cid'));
    }


}