<?php

namespace App\Http\Controllers;

use App\Role;
use App\Tb_Courses;
use App\Tb_Categories_Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Helper;
use App;
use Session;
class HomeController extends Controller
{
    /**
     * Show a list of users
     * @return \Illuminate\View\View
     */
    
    public function index()
    {
       
        //print_r(\Auth::user()->id);exit;
        //print_r(\Session::get('ipCurrencySymbol'));exit;
$book = \DB::table('eb_book')->where('status','1')->get();
$categories=\DB::table("tb_book_categorie")->where('status','1')->get();
       // \Session::flash('message', 'You are logged out'); 
        //    \Session::flash('alert-class', 'alert-warning');
return view('home', compact('book','categories'));
}

public function courseTitle(Request $request){
    $aCourses = Tb_Courses::where('status','Accept')->with("tb_languages_settings")->get();  
    $aValue=array();$aValue1=array();
    foreach($aCourses as $course){
        $aValue['title']=$course->tb_languages_settings['value'];
        $aValue['data']=$course->id;
        $aValue1[]=json_encode($aValue);
    }
    $jVal=implode(",", $aValue1);
    echo "[".$jVal."]";
}
public function catCourse($id)
{
    $aCat_courses = Tb_Courses::where('status','Accept')->where('cat_id',$id)->orderBy('id', 'desc')->with("tb_languages_settings")->with("tb_languages_settings_summary")->with("teacher_name")->get();  
    $aCat_name=Tb_Categories_Type::select('name')->where('id',$id)->first();      
    return view('site/cat_course_search', compact('aCat_courses','aCat_name'));
}

public function detail($id)
{

    $book = \DB::table('eb_book')->where('status','1')->where('id',$id)->first();
    if(count($book) > 0){
            //print_r($book->title);exit;
        $similarTit = \DB::table('eb_book')->where('status','1')->where('id','!=',$book->id)->where('title','like','%'.$book->title.'%')->get();    
        $sameAuthor = \DB::table('eb_book')->where('status','1')->where('id','!=',$book->id)->where('user_id',$book->user_id)->get();    
        return view('site/detail', compact('book','similarTit','sameAuthor'));
    }else{
        return redirect('');
    }
}

public function bookPreview($id){
    $book = \DB::table('eb_book')->where('status','1')->where('id',$id)->first();
    if(count($book) > 0){
        $bookFile = \DB::table('eb_book_files')->where('book_id',$book->id)->get();    
        return view('book-flip', compact('book','bookFile'));
    }else{
        return redirect('');
    }
}

public function college_details($id){
   $book = \DB::table('eb_college_book')->where('status','1')->where('id',$id)->first();
   if(count($book) > 0){
            //print_r($book->title);exit;

    $similarSem = \DB::table('eb_college_book')->where('status','1')->where('id','!=',$book->id)->where('semester',$book->semester)->get();    
    return view('site/college_detail', compact('book','similarSem'));
}else{
    return redirect('');
}
}


}