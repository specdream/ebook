<?php namespace App\Http\Controllers\mobile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth, Redirect, Validator, View, Session, File, DB;
use App\Http\Services\Helper;
use Hash;
use DateTime;

class UserController extends Controller
{

    public function register() {

        $_REQUEST   = str_replace('"','', $_REQUEST);
        $rules = array(
            'role_id'       =>'required',
            'username'       =>'required',
            'email'            => 'required|email|unique:users',
            'country'        =>'required',
            'city'            => 'required',
            'phone_number'            => 'required|unique:users',
            'password'            => 'required',
            'confirm_password' => 'required',
            );  

        $validator = Validator::make($_REQUEST, $rules);

        if ($validator->passes()) 
        {
            $code = rand(10000,10000000);

            $authen                 = new User;
            $authen->username       = $_REQUEST['username'];
            $authen->email          = trim($_REQUEST['email']);
            $authen->password       =\Hash::make($_REQUEST['password']);
            //$authen->confirm_password       = \Hash::make($_REQUEST['confirm_password']);
            if(isset($_REQUEST['phone_number']) != ''){
                $authen->phone_number   = trim($_REQUEST['phone_number']);
            }
            if(isset($_REQUEST['device']) != ''){
                $device = $_REQUEST['device'];
                $authen->device     = $device;
                $authen->$device    = $_REQUEST['token'];
            }
            $authen->activation     = $code;
            $authen->role_id       = $_REQUEST['role_id'];
            $authen->active = '1';
           // if(CNF_ACTIVATION == 'auto') { $authen->active = '1'; } else { $authen->active = '0'; }
            $authen->save();
            $last =$authen->id;
//$wallet =\DB::table('sdream_user_wallet')->insert(array('user_id'=>$last));

            $data = array(
                // 'username'          => $_REQUEST['first_name'].' '.$_REQUEST['last_name'],
                // 'first_name'        => $_REQUEST['first_name'] ,
                'username'         => $_REQUEST['username'],
                'email'             => $_REQUEST['email'] ,
    // 'phone_number'       => $_REQUEST['phone_number'] ,
                'password'          => $_REQUEST['password'] ,
                'code'              => $code
                );

            if(isset($_REQUEST['phone_number']) != ''){
                $data['phone_number']   = trim($_REQUEST['phone_number']);
            } else {
                $data['phone_number']   = '';
            }
             if($_REQUEST['role_id'] == 1)
                {
                    $var="Administrator";
                }
                elseif($_REQUEST['role_id'] == 2)
                {
                    $var="Learners";
                }
                elseif($_REQUEST['role_id'] == 3)
                {
                    $var="University";
                }
                elseif($_REQUEST['role_id'] == 4)
                {
                    $var="Teacher";
                }
                elseif($_REQUEST['role_id'] == 5)
                {
                    $var="Author";
                }
                elseif($_REQUEST['role_id'] == 6)
                {
                    $var="Publisighing House";
                }
               elseif($_REQUEST['role_id'] == 7)
                {
                    $var="Research Centre";
                }
                elseif($_REQUEST['role_id'] == 8)
                {
                    $var="Member";
                }
             
         $data = \DB::table('users')->select('id as user_id' ,'username','email','role_id')->where('id',$last)->get();
    
            $response["id"]         = 1;
            $response["message"]    = "Thanks for registering!..Your account is active now ";
            $response["roleidtype"]    = "WELCOME   ".$var;
            $response["userdetails"] =$data;
            // if(CNF_ACTIVATION == 'confirmation')
            // {

            //     $response["id"]         = "1";
            //     $response["message"]    = "Please check your email for activation";
            //     $response["insert_id"]  = $authen->id;

            //     if(($_REQUEST['device'] == 'android')) {
            //         \DB::table('users')->where('id', $authen->id)->update(['android' => $_REQUEST['token']]);
            //     } else{
            //         DB::table('users')->where('id', $authen->id)->update(['ios' => $_REQUEST['token']]);
            //     }
            //     $user_values  = \DB::table('users')->select('id','group_id','username','email','avatar','android','ios')->where('id', $authen->id)->get();
            //     foreach ($user_values as $key => $value) {
            //         if($value->avatar != ""){
            //             $image_url      = $this->image_url($value->avatar,'uploads/users');
            //             $value->avatar  = $image_url;                                   
            //         } else {
            //             $value->avatar  = \URL::to('').'/uploads/users/no-image.png';
            //         }
            //     }
            //     $response["user_data"]  = $user_values;
            // } elseif(CNF_ACTIVATION=='manual') {
            //     $response["id"]         = "2";
            //     $response["message"]    = "Thanks for registering!..We will validate you account before your account active";
            // } 
            // else {
                
            // }
        } else {
            $messages               = $validator->messages();
            $error                  = $messages->getMessages();
            $response["id"]         = "5";
            $response["message"]    = $error;
        }
        echo json_encode($response); exit;
    }

    public function login( Request $request) {

        $_REQUEST   = str_replace('"','', $_REQUEST);
        $rules = array(

            'email'  =>'required',
            'password'=>'required',

            );

        $validator = Validator::make($_REQUEST, $rules);
        if ($validator->passes()) {

            $remember = (!is_null($request->get('remember')) ? 'true' : 'false' );

            if (\Auth::attempt(array('email'=>$_REQUEST['email'] , 'password'=> $_REQUEST['password']) , $remember )) {
                if($_REQUEST['device']== "android"){
                    $value=array('device' =>"android",'android'=>$_REQUEST['token']);
                    $query=\DB::table('users')->where('email','=', $_REQUEST['email'])->update($value);
                }
                if($_REQUEST['device']== "ios"){
                    $value=array('device' =>"ios",'ios'=>$_REQUEST['token']);
                    $query=\DB::table('users')->where('email','=',$_REQUEST['email'])->update($value);
                }

                $row = User::find(\Auth::user()->id);

                if($row->active =='0') {

                    $response["id"]             = "1";
                    $response["message"]        = "Your Account is not active";
                } else  if($row->active=='2') {

                    $response["id"]             = "2";
                    $response["message"]        = "Your Account is BLocked";
                } else {

                    $pro=DB::select("SELECT `username`,`avatar`,`phone_number`,`email` FROM `users` WHERE `id`=".$row->id);
                    foreach ($pro as $key => $val) {
                        if ($val->avatar != '') {
                            $val->avatar =\URL::to('').'/uploads/users/'.$val->avatar;
                        } else {
                            $val->avatar =\URL::to('').'/avatar_m.png';
                        }
                    }


                    $response["id"]             = "3";
                    $response["user_id"]        = $row->id;
                    $response["message"]        = "Your Account is active";
                    $response["user_data"]  = $pro;                         
                }
            } else {
                $response["id"]             = "4";
                $response["message"]        = "Your Email password combination was incorrect";
            }
        } else {
            $messages               = $validator->messages();
            $error                  = $messages->getMessages();
            $response["id"]         = "5";
            $response["message"]    = $error;
        }
        echo json_encode($response);
    }

    public function Catagory()
    {
        $data = \DB::table('tb_book_categorie')->select('*')->get();
        foreach($data as $val)
        
        if($val->id != ''){
            $val->id = $val->id;
        }else{
            $val->id = '';
        }

        if($val->name != ''){
            $val->name = $val->name;
        }else{
            $val->name = '';
        }

        if($val->status != ''){
            $val->status = $val->status;
        }else{
            $val->status = '';
        }

        $response['category'] = $data;
        echo json_encode($response);
    }


    public function SubCatagory(Request $request)
    {
        $id = $request->id;
        $data = \DB::table('tb_book_sub_categorie')->select('*')->where('cat_id',$id)->get();
        $response['subcategory'] = $data;
        echo json_encode($response);
    }

    public function ListRoles()
    {
        $data = \DB::table('roles')->select('id','title')->get();
        $response['role'] = $data;
        echo json_encode($response);
    }

     public function VideoUpload(Request $request)
    {
        $data = \DB::table('videos')->select('*')->get();
        $response['video'] = $data;
        echo json_encode($response);
    }
    
    public function img_url($value='',$path=''){
        $image_arr = explode(',', $value);
        foreach ($image_arr as $key => $value) {
            $fpath = base_path().'/'.$path.'/'.$value;
            
            if(File::exists($fpath)){
                $final[] = \URL::to('').'/'.$path.'/'.$value;

            }else{
                $final[] =\URL::to('').'/300x300.png';
            }
        }
        if(!empty($final)){
            $return = implode(',', $final);

        } else {
            $return = '';
        }
        return
         $return;
    }
    public function postProfileview(){ 
        $_REQUEST   = str_replace('"','', $_REQUEST);
        $rules = array(
            'user_id'=>'required|numeric',
            );
        $validator = Validator::make($_REQUEST, $rules);
        if ($validator->passes()) {
            $profile = \DB::select("SELECT phone_number,dob,email,username,avatar,address FROM `users`where `id` = '".$_REQUEST['user_id']."'"); 
            foreach ($profile as $key => $value) {
                if($value->avatar != ""){
                    $image_url      = $this->img_url($value->avatar,'public/uploads/users'.$_REQUEST['user_id']);
                    $value->avatar  = $image_url;
                }  
                else {
                    $value->avatar  = \URL::to('').'/300x300.png';
                }
                if($value->dob!=''){
                    $value->dob =$value->dob;
                }else{
                    $value->dob='';
                }

                if($value->phone_number!=''){
                    $value->phone_number =$value->phone_number;
                }else{
                    $value->phone_number='';
                }
                if($value->address!=''){
                    $value->address =$value->address;
                }else{
                    $value->address='';
                }

            }
            $response['profile'] = $profile; 

        }else {
            $messages               = $validator->messages();
            $error                  = $messages->getMessages();
            $response["id"]         = "5";
            $response["message"]    = $error;
        }
        echo json_encode($response); exit;
    }
        public function postCountrycitylist(Request $request)
    {   
        $country = \DB::select("SELECT id, sortname,name,phonecode FROM `sdream_countries`"); 
        $city= \DB::select("SELECT id,sdream_states_id,name FROM `sdream_cities`"); 
        $response["id"]         = "1";
        $response["message"]    = "success";
        if($country!=''){
            $response['country']=$country;
        }else{
            $response['country']=[];
        }
        if($city!=''){
            $response['city']=$city;
        }else{
            $response['city']=[];
        }


        echo json_encode($response);
    }
    public function postEditprofile(Request $request) {

        $_REQUEST   = str_replace('"','', $_REQUEST);
        $rules = array(
            'user_id'       =>'required|numeric',
            );
        $validator = Validator::make($_REQUEST, $rules);

        if ($validator->passes()) 
        {
            $uid=$request->user_id;
            $check=\DB::table('users')->where('id',$uid)->exists();
            if($check)
            {
                if($request->username!="")
                {
                    $val['username'] = $request->username;
                }
                if($request->email!="")
                {
                    $val['email']  = $request->email;
                }
                if($request->country!="")
                {
                    $val['country']=$request->country;
                }
                if($request->city!="")
                {
                    $val['city']= $request->city;
                }
                if($request->phonenumber!="")
                {
                    $val['phone_number']= $request->phone_number;
                }
                if(isset($_REQUEST['avatar']) && $_REQUEST['avatar']!=""){
                    $avata = \DB::select("SELECT `avatar` FROM `users` where `id` = '".$_REQUEST['user_id']."'") ;
                    if($avata[0]->avatar !=""){
                        $path=base_path()."/public/uploads/users/".$avata[0]->avatar;
                        
                        if (File::exists($path)) {
                        File::delete($path);
                        } 
                    }
                    $uid    = $_REQUEST['user_id']."-".rand(3,10)."-".time().".jpg";
                
                    $image  = $_REQUEST['avatar'];
                    $path   = base_path()."/public/uploads/users/$uid";

                    file_put_contents($path,base64_decode($image));
                    $val['avatar'] = $uid;

                }
                if(!empty($val))
                {
                    $edit=\DB::table('users')->where('id',$uid)->update($val);
                    $userdetails=\DB::table('users')->select('id','username','email','country','city','phone_number')->where('id',$uid)->get();

                    $response['id'] = '1';
                    $response['message'] = "Updated successfully";
                    $response['userdetails'] = $userdetails;
                }
                else
                {
                    $response['id'] = '3';
                    $response['message'] = "Doesn't updated";
                }
            }
                else
                {
                   $response['id'] = '3';
                    $response['message'] = "User_ID doesn't exists";
                }
            
        }
               else {
            $messages               = $validator->messages();
            $error                  = $messages->getMessages();
            $val=$error;
            $response['id'] = '5';
            $response["message"]    = $val;
        
        } 
            echo json_encode($response,JSON_NUMERIC_CHECK);
            }

            public function postBooklanguage(Request $request)
     {   
        $lang = \DB::select("SELECT id,value,status FROM `tb_book_language`"); 
     
       
        if($lang!=''){
            $response['language']=$lang;
        }else{
            $response['language']=[];
        }
        
        echo json_encode($response);
    }
    public function postUploadbook(Request $request)
    {            
     $_REQUEST   = str_replace('"','', $_REQUEST);
        $rules = array(
            'author_id'        => 'required',
            'category_id'         => 'required',
            'title'            => 'required',
            'summary'          => 'required',           
            'price_type'       => 'required',
            'price'            => 'required',
            'book_language'    => 'required',
            'isbn'             => 'required'
         );  

        $validator = Validator::make($_REQUEST, $rules);

        if ($validator->passes()) 
        {

             $dt = new DateTime;
            $user_id    = $_REQUEST['author_id'];
            $categorie  = $_REQUEST['category_id'];
            $subcategorie  = '';
            $title      = $_REQUEST['title'];
            $summary      = $_REQUEST['summary'];
            $price_type  = $_REQUEST['price_type'];
            $price=  $_REQUEST['price'];
            $language      = $_REQUEST['book_language'];
            $isbn      = $_REQUEST['isbn'];
            $currency  = '';
            $cdate= $dt->format('Y-m-d h:m:s');
            $pdate= $cdate;
            $status=1;
             
           $var= array(
     'user_id' => $user_id,
     'categorie' =>$categorie,
     'sub_categorie' => $subcategorie,
     'title'=>$title,
     'summary'=>$summary,
     'price_type' => $price_type,
     'price' => $price,
     'currency'=>$currency,
     'language'=>$language,
     'isbn' =>$isbn,
     'created_date'=>$cdate,
     'published_date'=>$pdate,
     'status'=>$status
 );
         
            $ins=DB::table('eb_book')->insertGetId($var);
            if($ins!='')
            {
            $dir    = 'uploads/books_banner/'.$ins.'/';   
            $name = time().'-'.rand(1000,100).'.png';
            $directory  = public_path().'/uploads/books_banner/'.$ins.'/';
            if (!(\File::exists($directory))) {
                        $destinationPath = \File::makeDirectory($directory, 0777, true);
            }
            move_uploaded_file($_FILES['books_banner']['tmp_name'], $directory . $name);
            
            $upd=\DB::table('eb_book')->where('id',$ins)->update(['banner' => $name]);
             
         for( $i = 0; $i < count($_FILES['source']['name']); $i++ ) {
        if( isset($_FILES['source']['name'][$i]) ) {

          $files               =  pathinfo($_FILES['source']['name'][$i]); 
          $ext = $files['extension'];
          $newfilename        = "I".$i.$user_id.'-'.time().'.'.$ext;
           $dir  = public_path().'/uploads/books/'.$ins.'/';
          if (!(\File::exists($dir))) {
                        $destination = \File::makeDirectory($dir, 0777, true);
            }
          move_uploaded_file($_FILES['source']['tmp_name'][$i],$dir.$newfilename);
          if($ext=='jpg'||$ext=='png'||$ext=='bmp'||$ext=='tiff')
          {

             $type='1';
          }
          elseif($ext=='mp4'||$ext=='mov'||$ext=='wmv'||$ext=='avi')
          {
            $type='2';  
          }
           elseif($ext=='pdf')
          {
            $type='3'; 
          }
           elseif($ext=='pptx'||$ext=='ppt'||$ext=='pptm')
          {
            $type='4';  
          }

          $datas= array(
     'book_id' => $ins,
     'type' => $type,
     'file' =>$newfilename,
     );
             $fileins=DB::table('eb_book_files')->insertGetId($datas);
        }
        }
                  $response['id'] = '1';
                  $response['message'] = "Inserted successfully";
                
            }
        
            else
            {
            
                    $response['id'] = '5';
                    $response['message'] = "Error";
                
            }
            
}
  else
 {
               $response['id'] = '5';
               $response['message'] = "Field is missing";
                
 }
              echo json_encode($response);
 }
       public function postDetailpage(REQUEST $request)
       {
        $_REQUEST   = str_replace('"','', $_REQUEST);
        $rules = array(
            'book_id'       =>'required|numeric',
            );
        $validator = Validator::make($_REQUEST, $rules);

        if ($validator->passes()) 
        {

        $book_id= $request->book_id;
          $det = \DB::select("SELECT id,user_id,title,summary,banner,price  FROM `eb_book` WHERE id = '".$book_id."'"); 

         
        
     foreach ($det as $key => $values) {
        $u_id= \DB::select("SELECT username  FROM `users` WHERE id = '".$values->user_id."'"); 

            if($values->user_id != ''){
                    $values->user_name = $u_id[0]->username;    
                }else{
                    $values->user_name ='';
                }

                if($values->banner != ''){
                    $values->banner = \URL::to('').'/public/uploads/books_banner/'.$book_id.'/'.$values->banner;    
                }else{
                    $values->banner = \URL::to('').'/public/uploads/books_banner/no.png';
                }
                 if($values->summary != ''){
                 $values->summary =strip_tags($values->summary);

             }
                 else
                 {
                    $values->summary='';
                 }
                $related=\DB::select("SELECT id,user_id,title,banner,price  FROM `eb_book` WHERE user_id='".$values->user_id."'");
            }
             // $related=\DB::select("SELECT id,user_id,title,banner,price  FROM `eb_book` LIKE user_id='".$values->user_id."'");
               foreach ($related as $key => $values) {

                if($values->user_id != ''){
                    $values->user_name = $u_id[0]->username;    
                }else{
                    $values->user_name ='';
                }
                if($values->banner != ''){
                    $values->banner = \URL::to('').'/public/uploads/books_banner/'.$book_id.'/'.$values->banner;    
                }else{
                    $values->banner = \URL::to('').'/public/uploads/books_banner/no.png';
                }
                
            }
         $response['id'] = '1';
        $response['message'] = 'Success';
       
        if($det!=''){
            $response['details']=$det;
        }else{
            $response['details']=[];
        }
        if($related!=''){
            $response['relatedtopics']=$related;
        }else{
            $response['relatedtopics']=[];
        }
       
    }
    else
    {
          $response['id'] = '5';
          $response['message'] = "book_id is required";

    }
        
        echo json_encode($response);
       }
       public function postViewbooks(REQUEST $request)
       {
    $cat=\DB::table('eb_book')->select('id','user_id','title','banner','price','categorie')->get();
        foreach ($cat as $key => $values) {
        $u_id= \DB::select("SELECT username  FROM `users` WHERE id = '".$values->user_id."'"); 

            if($values->user_id != ''){
                    $values->user_name = $u_id[0]->username;    
                }else{
                    $values->user_name ='';
                }

                if($values->banner != ''){
                    $values->banner = \URL::to('').'/public/uploads/books_banner/'.$values->id.'/'.$values->banner;    
                }else{
                    $values->banner = \URL::to('').'/public/uploads/books_banner/no.png';
                }
                $catid=DB::select("SELECT name  FROM `tb_book_categorie` WHERE id = '".$values->categorie."'"); 
  
     

            if($values->categorie != ''){
                    $values->categorie =  $catid[0]->name;  
                }else{
                    $values->categorie ='';
                }
                 if($request->user_id!='')
            {
            	$wish=\DB::table('eb_book_wish')->where('user_id',$request->user_id)->where('book_id',$values->id)->exists();
            	if($wish!='')
            	{
            		$values->wishlist_status='1';
            	}
            	else
            	{
            	    $values->wishlist_status='0';	
            	}

            	
            }
            else
            	{
            		$values->wishlist_status='0';
            	}

            }





        //    $film=\DB::table('eb_book')->select('id','user_id','title','banner','price','categorie')->where('categorie','1')->get();
        //  foreach ($film as $key => $values) {
        // $u_id= \DB::select("SELECT username  FROM `users` WHERE id = '".$values->user_id."'"); 

        //     if($values->user_id != ''){
        //             $values->user_name = $u_id[0]->username;    
        //         }else{
        //             $values->user_name ='';
        //         }

        //         if($values->banner != ''){
        //             $values->banner = \URL::to('').'/public/uploads/books_banner/'.$values->id.'/'.$values->banner;    
        //         }else{
        //             $values->banner = \URL::to('').'/public/uploads/books_banner/no.png';
        //         }
        //     }
        //     $comedy=\DB::table('eb_book')->select('id','user_id','title','banner','price','categorie')->where('categorie','2')->get();
        //  foreach ($comedy as $key => $values) {
        // $u_id= \DB::select("SELECT username  FROM `users` WHERE id = '".$values->user_id."'"); 

        //     if($values->user_id != ''){
        //             $values->user_name = $u_id[0]->username;    
        //         }else{
        //             $values->user_name ='';
        //         }

        //         if($values->banner != ''){
        //             $values->banner = \URL::to('').'/public/uploads/books_banner/'.$values->id.'/'.$values->banner;    
        //         }else{
        //             $values->banner = \URL::to('').'/public/uploads/books_banner/no.png';
        //         }
        //     }
        //      $sports=\DB::table('eb_book')->select('id','user_id','title','banner','price','categorie')->where('categorie','5')->get();
        //  foreach ($sports as $key => $values) {
        // $u_id= \DB::select("SELECT username  FROM `users` WHERE id = '".$values->user_id."'"); 

        //     if($values->user_id != ''){
        //             $values->user_name = $u_id[0]->username;    
        //         }else{
        //             $values->user_name ='';
        //         }

        //         if($values->banner != ''){
        //             $values->banner = \URL::to('').'/public/uploads/books_banner/'.$values->id.'/'.$values->banner;    
        //         }else{
        //             $values->banner = \URL::to('').'/public/uploads/books_banner/no.png';
        //         }
        //     }
        //     $bm=\DB::table('eb_book')->select('id','user_id','title','banner','price','categorie')->where('categorie','6')->get();
        //  foreach ($bm as $key => $values) {
        // $u_id= \DB::select("SELECT username  FROM `users` WHERE id = '".$values->user_id."'"); 

        //     if($values->user_id != ''){
        //             $values->user_name = $u_id[0]->username;    
        //         }else{
        //             $values->user_name ='';
        //         }

        //         if($values->banner != ''){
        //             $values->banner = \URL::to('').'/public/uploads/books_banner/'.$values->id.'/'.$values->banner;    
        //         }else{
        //             $values->banner = \URL::to('').'/public/uploads/books_banner/no.png';
        //         }
        //     }
        $response['id'] = '1';
        $response['message'] = 'Success';

         $response["categorie"] =$cat;
       
        // if($film!=''){
        //     $response["categorie"]['filmsandtv']=$film;
        // }else{
        //     $response["categorie"]['filmsandtv']=[];
        // }

        // if($comedy!=''){
        //     $response["categorie"]['comedians']=$comedy;
        // }else{
        //     $response["categorie"]['comedians']=[];
        // }
        // if($sports!=''){
        //     $response["categorie"]['sports']=$sports;
        // }else{
        //     $response["categorie"]['sports']=[];
        // }
        //  if($bm!=''){
        //     $response["categorie"]['bodyandmind']=$bm;
        // }else{
        //     $response["categorie"]['bodyandmind']=[];
        // }
         echo json_encode($response);
       }
       public function postFilter(REQUEST $request)
       {
        if($categorie=$request->categorie && $lang=$request->language)
        {
            $both=\DB::table('eb_book')->select('id','user_id','title','banner','price','categorie','language')
                  ->where('categorie',$request->categorie)
                  ->where('language',$request->language)
                  ->get();
              foreach ($both as $key => $values) {
              $u_id= \DB::select("SELECT username  FROM `users` WHERE id = '".$values->user_id."'"); 

            if($values->user_id != ''){
                    $values->user_name = $u_id[0]->username;    
                }else{
                    $values->user_name ='';
                }

                if($values->banner != ''){
                    $values->banner = \URL::to('').'/public/uploads/books_banner/'.$values->id.'/'.$values->banner;    
                }else{
                    $values->banner = \URL::to('').'/public/uploads/books_banner/no.png';
                }
             }
              if($both!=''){
                 $response['categorywithlanguage']=$both;
              }else{
                 $response['categorywithlanguage']=[];
             }

        }

        if($categorie=$request->categorie && $request->language=='' )
             {
                $cat=\DB::table('eb_book')->select('id','user_id','title','banner','price','categorie','language')->where('categorie',$request->categorie)->get();
              foreach ($cat as $key => $values) {
              $u_id= \DB::select("SELECT username  FROM `users` WHERE id = '".$values->user_id."'"); 

            if($values->user_id != ''){
                    $values->user_name = $u_id[0]->username;    
                }else{
                    $values->user_name ='';
                }

                if($values->banner != ''){
                    $values->banner = \URL::to('').'/public/uploads/books_banner/'.$values->id.'/'.$values->banner;    
                }else{
                    $values->banner = \URL::to('').'/public/uploads/books_banner/no.png';
                }
             }
              if($cat!=''){
                 $response['category']=$cat;
              }else{
                 $response['category']=[];
             }
       }
       if($lang=$request->language && $request->categorie=='')
             {
                $lang=\DB::table('eb_book')->select('id','user_id','title','banner','price','categorie','language')->where('language',$request->language)->get();
              foreach ($lang as $key => $values) {
              $u_id= \DB::select("SELECT username  FROM `users` WHERE id = '".$values->user_id."'"); 

            if($values->user_id != ''){
                    $values->user_name = $u_id[0]->username;    
                }else{
                    $values->user_name ='';
                }

                if($values->banner != ''){
                    $values->banner = \URL::to('').'/public/uploads/books_banner/'.$values->id.'/'.$values->banner;    
                }else{
                    $values->banner = \URL::to('').'/public/uploads/books_banner/no.png';
                }
             }
              if($lang!=''){
                 $response['language']=$lang;
              }else{
                 $response['language']=[];
             }
       }
       if(!empty($request))
             {
             $all=\DB::table('eb_book')->select('id','user_id','title','banner','price','categorie','language')->get();
              foreach ($all as $key => $values) {
              $u_id= \DB::select("SELECT username  FROM `users` WHERE id = '".$values->user_id."'"); 

            if($values->user_id != ''){
                    $values->user_name = $u_id[0]->username;    
                }else{
                    $values->user_name ='';
                }

                if($values->banner != ''){
                    $values->banner = \URL::to('').'/public/uploads/books_banner/'.$values->id.'/'.$values->banner;    }
                    else{
                    $values->banner = \URL::to('').'/public/uploads/books_banner/no.png';
                }
             }
             if($request->categorie=='' && $request->language=='')
             {
              if($all!=''){
                 $response['allcategory']=$all;
              }else{
                 $response['allcategory']=[];
             }
         }
         }

                  echo json_encode($response);exit;
       }

       public function postSearch(Request $request)
       {
         $_REQUEST   = str_replace('"','', $_REQUEST);
        $rules = array(
            'search'       =>'required',
            );
        $validator = Validator::make($_REQUEST, $rules);

        if ($validator->passes()) 
        {

        $data=$request->search;
        $fl=\DB::table('eb_book')->select('eb_book.id','eb_book.user_id','eb_book.title','eb_book.banner','eb_book.price','eb_book.categorie','eb_book.language','eb_book.isbn','users.username')
                  ->join('users', 'eb_book.user_id','=','users.id')
                  ->where('title','like','%'.$data.'%')
                  ->orwhere('username','like','%'.$data.'%')
                  ->orwhere('isbn','like','%'.$data.'%')
                  ->get();
                  if($fl!=''){
                 $response['searchresult']=$fl;
              }else{
                 $response['searchresult']=[];
             }
         }
         else
         {
          $response['id'] = '5';
          $response['message'] = "search field is required";
         }
        
                  echo json_encode($response);exit;                  
       }
       public function postUserbook(Request $request)
       {
        $_REQUEST   = str_replace('"','', $_REQUEST);
        $rules = array(
            'user_id'       =>'required|numeric',
            );
        $validator = Validator::make($_REQUEST, $rules);

        if ($validator->passes()) 
        {

        $user_id= $request->user_id;
          $det = \DB::select("SELECT id,user_id,title,summary,banner,price  FROM `eb_book` WHERE user_id ='".$user_id."'"); 

         
        
     foreach ($det as $key => $values) {
        $u_id= \DB::select("SELECT username  FROM `users` WHERE id = '".$values->user_id."'"); 

            if($values->user_id != ''){
                    $values->user_name = $u_id[0]->username;    
                }else{
                    $values->user_name ='';
                }

                if($values->banner != ''){
                    $values->banner = \URL::to('').'/public/uploads/books_banner/'.$values->id.'/'.$values->banner;    
                }else{
                    $values->banner = \URL::to('').'/public/uploads/books_banner/no.png';
                }
                
            }
            
         $response['id'] = '1';
        $response['message'] = 'Success';
       
        if($det!=''){
            $response['details']=$det;
        }else{
            $response['details']=[];
        }
        
       
    }
    else
    {
          $response['id'] = '5';
          $response['message'] = "user_id is required";

    }
        
        echo json_encode($response);
       }
       public function postAddwish(Request $request)
       {

        $_REQUEST   = str_replace('"','', $_REQUEST);
        $rules = array(
            'user_id'       =>'required|numeric',
            'book_id'       =>'required|numeric'
            );
        $validator = Validator::make($_REQUEST, $rules);

        if ($validator->passes()) 
        {

        $user_id= $request->user_id;
        $book_id= $request->book_id;
         // $dt = new DateTime;
         // $cdate= $dt->format('h:m:s');

        $count=\DB::table('eb_book_wish')->where('user_id',$user_id)->where('book_id',$book_id)->exists();
        $datas= array(
     'book_id' => $book_id,
     'user_id' => $user_id,
     'created' => time(),
     );
            
        if($count>0)
        {
             DB::table('eb_book_wish')->where('user_id',$user_id)->where('book_id',$book_id)->delete();
             $add="wishlist removed successfully";
        }
        else
        {
             DB::table('eb_book_wish')->insert($datas);
           $add="wishlist added successfully";

        }
        $response['id'] = '1';
        $response['message'] = $add;
    }
    else
    { 
          $response['id'] = '5';
          $response['message'] = "user_id and book_id is required";

    }
         echo json_encode($response); exit;
    }
        public function postWishlist(Request $request) 
        {

        $_REQUEST   = str_replace('"','', $_REQUEST);
        $rules = array(
            'user_id'       =>'required|numeric',
            );
        $validator = Validator::make($_REQUEST, $rules);

        if ($validator->passes()) 
        {
               $wish=\DB::table('eb_book_wish')->select("*")->where('user_id',$_REQUEST['user_id'])->get();   
 
                    foreach ($wish as $key => $values) {

        $u_id= \DB::select("SELECT banner,title,summary,price FROM `eb_book` WHERE id = '".$values->book_id."'");
          //echo "<pre>"; print_r($u_id); exit;
                     $values->title = $u_id[0]->title;   
             $values->banner = \URL::to('').'/public/uploads/books_banner/'.$values->book_id.'/'.$u_id[0]->banner; 
                if($values->banner == ''){
                    $values->banner = \URL::to('').'/public/uploads/books_banner/no.png';
                }
             $values->summary = strip_tags($u_id[0]->summary);
             if($u_id[0]->price=='0')
             {
             	 $values->price = 'FREE';

             }
             else{
             	 $values->price = $u_id[0]->price;
             }
           }
            
         $response['id'] = '1';
        $response['message'] = 'Success';
       
        if($wish!=''){
            $response['your_wishlist']=$wish;
        }else{
            $response['your_wishlist']=[];
        }
        
       
    }
    else
    {
          $response['id'] = '5';
          $response['message'] = "user_id is required";

    }
        
        echo json_encode($response); exit;
       
}

       public function postAddtocart(REQUEST $request)
       {
       	$_REQUEST   = str_replace('"','', $_REQUEST);
        $rules = array(
            'book_id'       =>'required|numeric',
            'college' => 'required',
            );
        $validator = Validator::make($_REQUEST, $rules);

        if ($validator->passes()) 
        {
            $user_id=$request->user_id;
            if(isset($user_id))
            {
                $ins = array(
                    'user_id' => $request->user_id,
                    'product' => $request->book_id,
                    'college' => $request->college,
                    'created' => date("Y-m-d H:i:s") );

                $check=\DB::table('cart')->where('user_id',$user_id)->where('product',$request->book_id)->where('college',$request->college)->exists();
                if($check==0)
                {
                    \DB::table('cart')->insert($ins);
                      $response['id'] = '1';
                      $response['book_id']=$request->book_id;
                      $response['message'] = 'Your book added successfully';
                }
                else
                {
                     $response['id'] = '5';
                      $response['book_id']=$request->book_id;
                      $response['message'] = 'This book already exists';
                }
            }
            else
            {
                $key=$request->cookie_id;
                 $ins = array(
                    'key_cart' => $key,
                    'product' => $request->book_id,
                    'college' => $request->college,
                    'created' => date("Y-m-d H:i:s"));
                  $check=\DB::table('cart')->where('key_cart',$key)->where('product',$request->book_id)->where('college',$request->college)->exists();
                if($check==0)
                {
                    \DB::table('cart')->insert($ins);
                      $response['id'] = '1';
                      $response['book_id']=$request->book_id;
                      $response['message'] = 'Your book added successfully';
                }
                else
                {
                     $response['id'] = '5';
                      $response['book_id']=$request->book_id;
                      $response['message'] = 'This book already exists';
                }
            }
            }
            else
            {
          $response['id'] = '5';
          $response['message'] = "book_id is required";
          $response['message'] = "college field is required";
            }
      echo json_encode($response); exit;
       }
       public function postShowcart(REQUEST $request)
       {       

        if(isset($request->user_id))
        {
             
            $user_id=$request->user_id;
             $check=\DB::table('cart')->where('user_id',$user_id)->exists();
             
             if($check!=0)
             {

                $b_id=\DB::table('cart')->select('product','id')->where('user_id',$user_id)->get();   
              //  echo "<pre>"; print_r($b_id);exit;     
                foreach ($b_id as $key => $value) {
                   $bookinfo[]=\DB::table('eb_book')->select('id','title','price','banner')->where('id', $value->product)->first();
                    $bookinfo[$key]->cart_id=$value->id;
               }
                   foreach ($bookinfo as $key => $val) {        
                   if($val->banner == ''){
                    $val->banner = \URL::to('').'/public/uploads/books_banner/no.png';
                }
                else{    
                $val->banner = \URL::to('').'/public/uploads/books_banner/'.$value->product.'/'.$val->banner; 
                }
               
            }
                     $response['id'] = '1';
                      $response['message'] = 'Your cart products';
                      $response['product']=$bookinfo;
                  
         }
             else
             {
                      $response['id'] = '5';
                      $response['message'] = 'No products in your cart';
             }
           }
           if(isset($request->cookie_id))
           {
            $key_cart =$request->cookie_id;
             $check=\DB::table('cart')->where('key_cart',$key_cart)->exists();
             
             if($check!=0)
             {

                $b_id=\DB::table('cart')->select('product','id')->where('key_cart',$key_cart)->get();        
                foreach ($b_id as $key => $value) {
                   $bookinfo[]=\DB::table('eb_book')->select('id','title','price','banner')->where('id', $value->product)->first();
                   $bookinfo[$key]->cart_id=$value->id;
               }
                   foreach ($bookinfo as $key => $val) {        
                   if($val->banner == ''){
                    $val->banner = \URL::to('').'/public/uploads/books_banner/no.png';
                }
                else{    
                $val->banner = \URL::to('').'/public/uploads/books_banner/'.$value->product.'/'.$val->banner; 
                }
            }
                     $response['id'] = '1';
                      $response['message'] = 'Your cart products';
                      $response['product']=$bookinfo;
                  
         }
             else
             {
                      $response['id'] = '5';
                      $response['message'] = 'No products in your cart';
             }

               
           }
               echo json_encode($response); exit;
        }
        public function postRemovecart(Request $request)
        {
            $_REQUEST=str_replace('"','',$_REQUEST);
            $rules=array('cart_id' =>'required|numeric' );
            $validator=Validator::make($_REQUEST, $rules);
          if ($validator->passes()) 
          {
            $check=\DB::table('cart')->where('id',$_REQUEST['cart_id'])->exists();
            if($check>0)
            {
             \DB::table('cart')->where('id',$_REQUEST['cart_id'])->delete();
             $response['id'] = '1';
              $response['message'] ="Removed successfully";
            }
           else
          {
              $response['id'] = '5';
              $response['message'] = 'cart_id is not exists in table';
          }

          }
          else
          {
              $response['id'] = '5';
              $response['message'] = 'cart_id is required';
          }
          echo json_encode($response); exit;
        }  

}
?>