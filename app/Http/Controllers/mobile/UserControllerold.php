<?php namespace App\Http\Controllers\mobile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth, Redirect, Validator, View, Session, File, DB;
use App\Http\Services\Helper;

class UserController extends Controller
{

    public function studentregister() {

        $_REQUEST   = str_replace('"','', $_REQUEST);
        $rules = array(
            'role_id'       =>'required',
            'username'       =>'required',
            'email'            => 'required|email|unique:users',
            'country'        =>'required',
//'phone_number'   => 'required|unique:users',
            'city'            => 'required',
            'phone_number'            => 'required|unique:users',
            'password'            => 'required',
            'confirm_password' => 'required',
            );  

        $validator = Validator::make($_REQUEST, $rules);

        if ($validator->passes()) {
            $code = rand(10000,10000000);

            $authen                 = new User;
            $authen->username       = $_REQUEST['username'];
            $authen->email          = trim($_REQUEST['email']);
            $authen->password       =\hash::make($_REQUEST['password']);
            $authen->confirm_password       = \Hash::make($_REQUEST['confirm_password']);
            if(isset($_REQUEST['phone_number']) != ''){
                $authen->phone_number   = trim($_REQUEST['phone_number']);
            }
            if(isset($_REQUEST['device']) != ''){
                $device = $_REQUEST['device'];
                $authen->device     = $device;
                $authen->$device    = $_REQUEST['token'];
            }
            $authen->activation     = $code;
            $authen->group_id       = 3;
            if(CNF_ACTIVATION == 'auto') { $authen->active = '1'; } else { $authen->active = '0'; }
            $authen->save();
            $last =$authen->id;
//$wallet =\DB::table('sdream_user_wallet')->insert(array('user_id'=>$last));

            $data = array(
                'username'          => $_REQUEST['first_name'].' '.$_REQUEST['last_name'],
                'first_name'        => $_REQUEST['first_name'] ,
                'last_name'         => $_REQUEST['last_name'],
                'email'             => $_REQUEST['email'] ,
    // 'phone_number'       => $_REQUEST['phone_number'] ,
                'password'          => $_REQUEST['password'] ,
                'code'              => $code
                );

            if(isset($_REQUEST['phone_number']) != ''){
                $data['phone_number']   = trim($_REQUEST['phone_number']);
            } else {
                $data['phone_number']   = '';
            }

            if(CNF_ACTIVATION == 'confirmation')
            {

                $response["id"]         = "1";
                $response["message"]    = "Please check your email for activation";
                $response["insert_id"]  = $authen->id;

                if(($_REQUEST['device'] == 'android')) {
                    \DB::table('users')->where('id', $authen->id)->update(['android' => $_REQUEST['token']]);
                } else{
                    DB::table('users')->where('id', $authen->id)->update(['ios' => $_REQUEST['token']]);
                }
                $user_values  = \DB::table('users')->select('id','group_id','username','email','avatar','android','ios')->where('id', $authen->id)->get();
                foreach ($user_values as $key => $value) {
                    if($value->avatar != ""){
                        $image_url      = $this->image_url($value->avatar,'uploads/users');
                        $value->avatar  = $image_url;                                   
                    } else {
                        $value->avatar  = \URL::to('').'/uploads/users/no-image.png';
                    }
                }
                $response["user_data"]  = $user_values;
            } elseif(CNF_ACTIVATION=='manual') {
                $response["id"]         = "2";
                $response["message"]    = "Thanks for registering!..We will validate you account before your account active";
            } else {
                $response["id"]         = "3";
                $response["message"]    = "Thanks for registering!..Your account is active now ";
            }
        } else {
            $messages               = $validator->messages();
            $error                  = $messages->getMessages();
            $response["id"]         = "5";
            $response["message"]    = $error;
        }
        echo json_encode($response); exit;
    }

    public function studentlogin( Request $request) {

        $_REQUEST   = str_replace('"','', $_REQUEST);
        $rules = array(

            'email'  =>'required',
            'password'=>'required',

            );

        $validator = Validator::make($_REQUEST, $rules);
        if ($validator->passes()) {

            $remember = (!is_null($request->get('remember')) ? 'true' : 'false' );

            if (\Auth::attempt(array('email'=>$_REQUEST['email'] , 'password'=> $_REQUEST['password']) , $remember )) {
                if($_REQUEST['device']== "android"){
                    $value=array('device' =>"android",'android'=>$_REQUEST['token']);
                    $query=\DB::table('users')->where('email','=', $_REQUEST['email'])->update($value);
                }
                if($_REQUEST['device']== "ios"){
                    $value=array('device' =>"ios",'ios'=>$_REQUEST['token']);
                    $query=\DB::table('users')->where('email','=',$_REQUEST['email'])->update($value);
                }

                $row = User::find(\Auth::user()->id);

                if($row->active =='0') {

                    $response["id"]             = "1";
                    $response["message"]        = "Your Account is not active";
                } else  if($row->active=='2') {

                    $response["id"]             = "2";
                    $response["message"]        = "Your Account is BLocked";
                } else {

                    $pro=DB::select("SELECT `username`,`avatar`,`phone_number`,`email` FROM `users` WHERE `id`=".$row->id);
                    foreach ($pro as $key => $val) {
                        if ($val->avatar != '') {
                            $val->avatar =\URL::to('').'/uploads/users/'.$val->avatar;
                        } else {
                            $val->avatar =\URL::to('').'/avatar_m.png';
                        }
                    }

                    $response["id"]             = "3";
                    $response["user_id"]        = $row->id;
                    $response["message"]        = "Your Account is active";
                    $response["user_data"]  = $pro;                         
                }
            } else {
                $response["id"]             = "4";
                $response["message"]        = "Your Email password combination was incorrect";
            }
        } else {
            $messages               = $validator->messages();
            $error                  = $messages->getMessages();
            $response["id"]         = "5";
            $response["message"]    = $error;
        }
        echo json_encode($response);
    }

    public function Catagory()
    {
        $data = \DB::table('tb_book_categorie')->select('*')->get();
        foreach($data as $val)
        
        if($val->id != ''){
            $val->id = $val->id;
        }else{
            $val->id = '';
        }

        if($val->name != ''){
            $val->name = $val->name;
        }else{
            $val->name = '';
        }

        if($val->status != ''){
            $val->status = $val->status;
        }else{
            $val->status = '';
        }

        $response['category'] = $data;
        echo json_encode($response);
    }


    public function SubCatagory(Request $request)
    {
        $id = $request->id;
        $data = \DB::table('tb_book_sub_categorie')->select('*')->where('cat_id',$id)->get();
        $response['subcategory'] = $data;
        echo json_encode($response);
    }

    public function ListRoles()
    {
        $data = \DB::table('roles')->select('id','title')->get();
        $response['role'] = $data;
        echo json_encode($response);
    }

     public function VideoUpload(Request $request)
    {
        $data = \DB::table('videos')->select('*')->get();
        $response['video'] = $data;
        echo json_encode($response);
    }
    
    public function postProfileview(){ 
        $_REQUEST   = str_replace('"','', $_REQUEST);
        $rules = array(
            'user_id'=>'required|numeric',
            );
        $validator = Validator::make($_REQUEST, $rules);
        if ($validator->passes()) {
            $profile = \DB::select("SELECT phone_number,dob,email,first_name,last_name,avatar FROM `users`where `id` = '".$_REQUEST['user_id']."'"); 
            foreach ($profile as $key => $value) {
                if($value->avatar != ""){
                    $image_url      = $this->img_url($value->avatar,'images/avatar/'.$_REQUEST['user_id']);
                    $value->avatar  = $image_url;
                }  
                else {
                    $value->avatar  = \URL::to('').'/300x300.png';
                }
                if($value->dob!=''){
                    $value->dob =$value->dob;
                }else{
                    $value->dob='';
                }

                if($value->phone_number!=''){
                    $value->phone_number =$value->phone_number;
                }else{
                    $value->phone_number='';
                }

            }
            $response['profile'] = $profile; 

        }else {
            $messages               = $validator->messages();
            $error                  = $messages->getMessages();
            $response["id"]         = "5";
            $response["message"]    = $error;
        }
        echo json_encode($response); exit;
    }

}
?>