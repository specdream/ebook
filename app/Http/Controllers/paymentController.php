<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Library\PayTabs\PayTabs;
use App\User;
use App\Mail\invoiceMail;
use Helper;
use Session;
use Redirect;
use DB;

class paymentController extends Controller{    
  public function index(){
    if(\Auth::user()){
      if(\Auth::user()->role_id == '2' || \Auth::user()->role_id == '8'){
      $cookie = \Helper::getCartCookie();
      $cart = \DB::table('cart')->where('college','0')->where('key_cart',$cookie)->get();
      $cart1 = \DB::table('cart')->where('college','1')->where('key_cart',$cookie)->get();
      $totalPrice = '0';
      foreach ($cart as $value) {
        $product = \DB::table('eb_book')->where('id',$value->product)->first();
        $totalPrice += $product->price;
      }
      $cogTotalPrice = '0';
      foreach ($cart1 as $value) {
        $product = \DB::table('eb_college_book')->where('id',$value->product)->first();
        $cogTotalPrice += $product->price;
      }
      $user_id = \Auth::user()->id;
      $address = \DB::table('billingAddress')->where('user_id',$user_id)->first();
      $offerSem = \DB::table('offers')->where('id','1')->first();
      $offerYear = \DB::table('offers')->where('id','2')->first();
      
      //print_r($totalPrice);exit;  
      return view('checkout',compact('totalPrice','address','cogTotalPrice'));
      }else{
        \Session::flash('message', 'Only student or member can able buy a books'); 
        \Session::flash('alert-class', 'alert-warning');
       return redirect(''); 
      }

    }else{

      return redirect('login');
      
    }
  }

  public function billingAddress(Request $request){

    $user_id = \Auth::user()->id;
    $get = \DB::table('billingAddress')->where('user_id',$user_id)->get();
    if(count($get) > 0){
      $insData['address'] = $request->Address;
      $insData['city'] = $request->City;
      $insData['state'] = $request->state;
      $insData['zip'] = $request->postal;
      $insData['country'] = $request->country;
      $insData['phone'] = $request->phone;
      \DB::table('billingAddress')->where('user_id',$user_id)->update($insData);
      echo "success";exit;
    }else{
      $insData['user_id'] = \Auth::user()->id;
      $insData['address'] = $request->Address;
      $insData['city'] = $request->City;
      $insData['state'] = $request->state;
      $insData['zip'] = $request->postal;
      $insData['country'] = $request->country;
      $insData['phone'] = $request->phone;
      \DB::table('billingAddress')->insert($insData);
      echo "success";exit;
    }
  }

  public function paymentPaytabs(){

    if(\Auth::user()){

      $user_id = \Auth::user()->id;

      $address = \DB::table('billingAddress')->where('user_id',$user_id)->first();

      $country = \DB::table('sdream_countries')->where('id',$address->country)->first();

      $paytabs = new PayTabs();

      $paytabs->set_page_setting('title','ref number','USD',$_SERVER['SERVER_ADDR'],'English');

      $paytabs->set_customer(\Auth::user()->username,\Auth::user()->username,$country->phonecode,$address->phone,\Auth::user()->email);

      $cookie = \Helper::getCartCookie();
      $cart = \DB::table('cart')->where('college','0')->where('key_cart',$cookie)->get();

      foreach ($cart as $value) {
        $product = \DB::table('eb_book')->where('id',$value->product)->first();
        $paytabs->add_item($product->title,$product->price,'1');
      }


    //$paytabs->set_other_charges(3);
    //$paytabs->set_discount(1);

      $paytabs->set_address($address->address,$address->state,$address->city,$address->zip,$country->sortname);
      $paytabs->set_shipping_address($address->address,$address->state,$address->city,$address->zip,$country->sortname);
    //print_r($paytabs->create_pay_page());exit;
      $result = $paytabs->create_pay_page();
      if(($result->payment_url) != ''){

        $cookie = \Helper::getCartCookie();
        $cart = \DB::table('cart')->where('college','0')->where('key_cart',$cookie)->get();
        $book_id = array();
        foreach ($cart as $value) {
          $product = \DB::table('eb_book')->where('id',$value->product)->first();
          array_push($book_id,$product->id);
        }
        Session::put('paySucceesBook',$book_id);
        Session::put('payCollege','0');
        //print_r($result->payment_url);
        return Redirect::to($result->payment_url);    

      } else{
        //return Redirect::to('details/'.$request->eventId)->with('message', Helper::alert('error','Payment not succeed'));
        return redirect('paymentCancel/'.$result->result);
      }

    }else{
      return redirect('login');
    }

  }

  public function paymentSuccess(Request $request){
    $payment_reference = $request->payment_reference;
    $payCollege = Session::get('payCollege');
    if($payCollege == '1'){
      return redirect('paymentSuccessColg/'.$payment_reference);
    }
    $paytabs = new PayTabs();
    $status = $paytabs->verify_payment($payment_reference);
    //print_r($status);exit;
    if($status->response_code == '100'){
      $book_id = Session::get('paySucceesBook');
      $user_id = \Auth::user()->id;
      $address = \DB::table('billingAddress')->where('user_id',$user_id)->first();
      $country = \DB::table('sdream_countries')->where('id',$address->country)->first();
        //echo "string";exit;
      $bookAuth = array();
      $bookAuthId = array();
      if(count($book_id) > 0){
        $ba = '0';
        $uniqid = uniqid();
        Session::put('purUniq',$uniqid);

        $date = date("Y-m-d H:i:s");
        Session::put('purdate',$date);
        foreach ($book_id as $value) {

          $book = \DB::table('eb_book')->where('id',$value)->first();
          $cookie = \Helper::getCartCookie();
          $cartCookie = \DB::table('cart')->where('college','0')->where('key_cart',$cookie)->where('product',$value)->get();
          if(count($cartCookie) > 0){
            $insData['user_id'] = $user_id;
            $insData['author_id'] = $book->user_id;
            $insData['uniq_id'] = $uniqid;
            $insData['book_id'] = $value;
            $insData['price']   = $book->price;
            $insData['address'] = $address->address;
            $insData['state']   = $address->state;
            $insData['city']    = $address->city;
            $insData['postal']  = $address->zip;
            $insData['country'] = $country->name;
            $insData['created'] = $date;
            \DB::table('eb_book_purchase')->insert($insData);
            //$bookAuth[$book->user_id]['book_id'] = $book->id;
            //array_push($bookAuthId, $book->user_id);

            $userName = \Auth::user()->username;
            $bookTitle = $book->title;
            $authorName = \Helper::getUser($book->user_id,'name');
            $price = $book->price;

            //Notification to author

            $message = 'Hi '.$authorName.', '.$userName.' Buy your book at '.$date.'. Order ID #'.$uniqid;

            $notIns['user_id']= '1';
            $notIns['n_user_id']= $book->user_id;
            $notIns['n_role']= '5';
            $notIns['n_uname']= $authorName;
            $notIns['notification']= $message;
            $notIns['date']= date("Y-m-d H:i:s");
            $notIns['status']= '0';

            \DB::table('tb_notification')->insert($notIns);

            //Notification to admin

            $message = 'Order have been made at '.$date.'. Book author '.$authorName.' Book buyer '.$userName.', Order ID #'.$uniqid;

            $notIns['user_id']= '1';
            $notIns['n_user_id']= '1';
            $notIns['n_role']= '1';
            $notIns['n_uname']= CNF_APPNAME;
            $notIns['notification']= $message;
            $notIns['date']= date("Y-m-d H:i:s");
            $notIns['status']= '0';

            \DB::table('tb_notification')->insert($notIns);

            
            \DB::table('cart')->where('key_cart',$cookie)->where('product',$value)->delete();
          }

        }
            //print_r($bookAuth);exit;
            //print_r($admin->email);
            //echo CNF_APPNAME;exit;
            $user = User::findOrFail($user_id);
            /*$admin = User::findOrFail('1');
            $author = User::findOrFail($id);*/
            //Book invoice to user
            $from = CNF_EMAIL;
            $fromName = CNF_APPNAME;
            $purchase = DB::table('eb_book_purchase')->where('uniq_id',$uniqid)->get();
              /*$email = 'prasanna@sdreamtech.com';
        \Mail::to($email)->send(new invoiceMail(['book_id' => $book_id,'user_id' => $user_id,'address' => $address,'country' => $country]));*/
              if($_SERVER['HTTP_HOST'] != 'localhost'){
              \Mail::send('emails.bookInvoice',['book_id' => $book_id,'user_id' => $user_id,'address' => $address,'country' => $country], function($message) use ($user)
              {
              $message->from(CNF_EMAIL,CNF_APPNAME);
              $message->to('prasannap128@gmail.com','test')->subject('Invoice');
              });
            }


            //Notification to user

            $message = 'Greate, Your order has been complete, Invoice will send to your email.';

            $notIns['user_id']= '1';
            $notIns['n_user_id']= $user_id;
            $notIns['n_role']= '2';
            $notIns['n_uname']= $userName;
            $notIns['notification']= $message;
            $notIns['date']= date("Y-m-d H:i:s");
            $notIns['status']= '0';

            \DB::table('tb_notification')->insert($notIns);


            return redirect('paymentSuccessInvoice');
            

            
          }

        }else{
          return redirect('paymentCancel/'.$status->result);
        }
      }

      public function paymentCancel($message){
        $message = $message;
        return view('payment.paymentCancel',compact('message'));

      }

      public function paymentSuccessInvoice(){

        $user_id = \Auth::user()->id;
        $address = \DB::table('billingAddress')->where('user_id',$user_id)->first();
        $country = \DB::table('sdream_countries')->where('id',$address->country)->first();
        $data['address'] = $address;
        $data['country'] = $country;
        return view('payment.paymentSuccess')->with($data);

      }


      public function paymentPaytabsColg(){

    if(\Auth::user()){

      $user_id = \Auth::user()->id;

      $address = \DB::table('billingAddress')->where('user_id',$user_id)->first();

      $country = \DB::table('sdream_countries')->where('id',$address->country)->first();

      $paytabs = new PayTabs();

      $paytabs->set_page_setting('title','ref number','USD',$_SERVER['SERVER_ADDR'],'English');

      $paytabs->set_customer(\Auth::user()->username,\Auth::user()->username,$country->phonecode,$address->phone,\Auth::user()->email);

      $cookie = \Helper::getCartCookie();
      $cart = \DB::table('cart')->where('college','1')->where('key_cart',$cookie)->get();

      foreach ($cart as $value) {
        $product = \DB::table('eb_college_book')->where('id',$value->product)->first();
        $paytabs->add_item($product->title,$product->price,'1');
      }


    //$paytabs->set_other_charges(3);
    //$paytabs->set_discount(1);

      $paytabs->set_address($address->address,$address->state,$address->city,$address->zip,$country->sortname);
      $paytabs->set_shipping_address($address->address,$address->state,$address->city,$address->zip,$country->sortname);
    //print_r($paytabs->create_pay_page());exit;
      $result = $paytabs->create_pay_page();
      if(($result->payment_url) != ''){

        $cookie = \Helper::getCartCookie();
        $cart = \DB::table('cart')->where('college','1')->where('key_cart',$cookie)->get();
        $book_id = array();
        foreach ($cart as $value) {
          $product = \DB::table('eb_college_book')->where('id',$value->product)->first();
          array_push($book_id,$product->id);
        }
        Session::put('paySucceesBook',$book_id);
        Session::put('payCollege','1');
        //print_r($result->payment_url);
        return Redirect::to($result->payment_url);    

      } else{
        //return Redirect::to('details/'.$request->eventId)->with('message', Helper::alert('error','Payment not succeed'));
        return redirect('paymentCancel/'.$result->result);
      }

    }else{
      return redirect('login');
    }

  }

    public function paymentSuccessColg($payment_reference){
    
    $paytabs = new PayTabs();
    $status = $paytabs->verify_payment($payment_reference);
    //print_r($status);exit;
    if($status->response_code == '100'){
      $book_id = Session::get('paySucceesBook');
      $user_id = \Auth::user()->id;
      $address = \DB::table('billingAddress')->where('user_id',$user_id)->first();
      $country = \DB::table('sdream_countries')->where('id',$address->country)->first();
        //echo "string";exit;
      $bookAuth = array();
      $bookAuthId = array();
      if(count($book_id) > 0){
        $ba = '0';
        $uniqid = uniqid();
        Session::put('purUniq',$uniqid);

        $date = date("Y-m-d H:i:s");
        Session::put('purdate',$date);
        foreach ($book_id as $value) {

          $book = \DB::table('eb_college_book')->where('id',$value)->first();
          $cookie = \Helper::getCartCookie();
          $cartCookie = \DB::table('cart')->where('college','1')->where('key_cart',$cookie)->where('product',$value)->get();
          if(count($cartCookie) > 0){
            $insData['user_id'] = $user_id;
            $insData['author_id'] = $book->user_id;
            $insData['uniq_id'] = $uniqid;
            $insData['book_id'] = $value;
            $insData['price']   = $book->price;
            $insData['ISBN']    = $book->isbn;
            
            $insData['address'] = $address->address;
            $insData['state']   = $address->state;
            $insData['city']    = $address->city;
            $insData['postal']  = $address->zip;
            $insData['country'] = $country->name;
            $insData['created'] = $date;

            //Start access key get
            $aKeyGet = \DB::table('accesskey')->where('status','1')->where('book_id',$value)->first(); 
            $aKeyVal = '';
            if($aKeyGet){
            
            $aKeyVal_i = ($aKeyGet->c_row+1);
             
            if (($handle = fopen("public/uploads/access_csv/".$aKeyGet->book_id."/".$aKeyGet->type."/".$aKeyGet->file, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if($data[0] == $aKeyVal_i){
                        $data[5] = ($data[5] == '1') ? '0' : '0'; 
                        $aKeyVal = $data[3];
                    }   
                    $data_array[] = $data;
                    //fputcsv($fp, $data);

                }
            }
            
            $csv = "";
            $headers = ['id','book_id','prefix','key','place','status'];//Column headers
            foreach ($data_array as $record){
                $csv.= $record[0].','.$record[1].','.$record[2].','.$record[3].','.$record[4].','.$record[5]."\n"; //Append data to csv
            }
            $fileName = time().".csv";
            $csv_handler = fopen ("public/uploads/access_csv/".$aKeyGet->book_id.'/'.$aKeyGet->type."/".$aKeyGet->file,'w');
            //fputcsv($csv_handler, $headers);
            fwrite ($csv_handler,$csv);
            fclose ($csv_handler);

                        //fclose($fp);
            if($aKeyVal_i == '1000'){
                $dataInKey['status'] = '0';
            }
            $dataInKey['c_row'] = $aKeyVal_i;
            $upKeyTab = \DB::table('accesskey')->where('id',$aKeyGet->id)->update($dataInKey);
            }
            //End access key get
            $insData['access']  = $aKeyVal;
            \DB::table('eb_college_book_purchase')->insert($insData);
            //$bookAuth[$book->user_id]['book_id'] = $book->id;
            //array_push($bookAuthId, $book->user_id);

            $userName = \Auth::user()->username;
            $bookTitle = $book->title;
            $authorName = \Helper::getUser($book->user_id,'name');
            $price = $book->price;

            //Notification to author

            $message = 'Hi '.$authorName.', '.$userName.' Buy a book at '.$date.'. Order ID #'.$uniqid;

            $notIns['user_id']= '1';
            $notIns['n_user_id']= $book->user_id;
            $notIns['n_role']= '5';
            $notIns['n_uname']= $authorName;
            $notIns['notification']= $message;
            $notIns['date']= date("Y-m-d H:i:s");
            $notIns['status']= '0';

            \DB::table('tb_notification')->insert($notIns);

            

            //Notification to admin

            $message = 'Order have been made at '.$date.'. Book author '.$authorName.' Book buyer '.$userName.', Order ID #'.$uniqid;

            $notIns['user_id']= '1';
            $notIns['n_user_id']= '1';
            $notIns['n_role']= '1';
            $notIns['n_uname']= CNF_APPNAME;
            $notIns['notification']= $message;
            $notIns['date']= date("Y-m-d H:i:s");
            $notIns['status']= '0';

            \DB::table('tb_notification')->insert($notIns);

            
            \DB::table('cart')->where('key_cart',$cookie)->where('product',$value)->delete();
          }

        }
            //print_r($bookAuth);exit;
            //print_r($admin->email);
            //echo CNF_APPNAME;exit;
            $user = User::findOrFail($user_id);
            /*$admin = User::findOrFail('1');
            $author = User::findOrFail($id);*/
            //Book invoice to user
            $from = CNF_EMAIL;
            $fromName = CNF_APPNAME;
            $purchase = DB::table('eb_college_book_purchase')->where('uniq_id',$uniqid)->get();
              /*$email = 'prasanna@sdreamtech.com';
        \Mail::to($email)->send(new invoiceMail(['book_id' => $book_id,'user_id' => $user_id,'address' => $address,'country' => $country]));*/
              if($_SERVER['HTTP_HOST'] != 'localhost'){
              \Mail::send('emails.bookInvoice',['book_id' => $book_id,'user_id' => $user_id,'address' => $address,'country' => $country], function($message) use ($user)
              {
              $message->from(CNF_EMAIL,CNF_APPNAME);
              $message->to('prasannap128@gmail.com','test')->subject('Invoice');
              });
            }


            //Notification to user

            $message = 'Greate, Your order has been complete, Invoice will send to your email.';

            $notIns['user_id']= '1';
            $notIns['n_user_id']= $user_id;
            $notIns['n_role']= '2';
            $notIns['n_uname']= $userName;
            $notIns['notification']= $message;
            $notIns['date']= date("Y-m-d H:i:s");
            $notIns['status']= '0';

            \DB::table('tb_notification')->insert($notIns);


            return redirect('paymentSuccessInvoice');
            

            
          }

        }else{
          return redirect('paymentCancelColg/'.$status->result);
        }
      }

    }