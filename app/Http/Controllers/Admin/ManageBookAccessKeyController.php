<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Helper;
use Session;
use Redirect;
use Auth;


class ManageBookAccessKeyController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
	{
		if(\Auth::check()){
			$book = \DB::table('eb_college_book')->where('status','1')->get();
			return view('admin.managebookaccesskey.index',compact("book"));
		}else{

			\Session::flash('message', 'You are logged out'); 
			\Session::flash('alert-class', 'alert-error');	
			return redirect('login');

		}
		
	}

	public function bookKeys($id,$place){
		if(\Auth::check()){
			$book = \DB::table('eb_college_book')->where('id',$id)->first();
			$keys = \DB::table('accesskey')->where('book_id',$id)->where('type',$place)->get();
			return view('admin.managebookaccesskey.bookKeys',compact("id","book","keys","place"));
		}else{

			\Session::flash('message', 'You are logged out'); 
			\Session::flash('alert-class', 'alert-error');	
			return redirect('login');

		}
	}

	public function create($id){
		if(\Auth::check()){
			$book = \DB::table('eb_college_book')->where('id',$id)->first();
			return view('admin.managebookaccesskey.create',compact("id","book"));
		}else{

			\Session::flash('message', 'You are logged out'); 
			\Session::flash('alert-class', 'alert-error');	
			return redirect('login');

		}
	}

	public function store(Request $request){

		$url = public_path('uploads/access_csv').'/'.$request->bookId.'/';
		if (!file_exists($url)) {
			mkdir($url, 0777);
		}
		$url1 = public_path('uploads/access_csv').'/'.$request->bookId.'/'.$request->place.'/';
		if (!file_exists($url1)) {
			mkdir($url1, 0777);
		}
		$keys = \DB::table('accesskey')->where('book_id',$request->bookId)->where('type',$request->place)->get();
		if(count($keys) > 5){
			\Session::flash('message', 'Sorry, You have to cross the limits'); 
			\Session::flash('alert-class', 'alert-error');	
			return redirect('admin/bookKeys'.'/'.$request->bookId);
		}
		if(count($keys) > 0){
			//$iVal = count($keys).'001';
			//$niVal = (count($keys)+1).'000';
			$iVal = '1';	
			$niVal = '1000';
		}else{
			$iVal = '1';	
			$niVal = '1000';
		}
		$min = '1000';
		$max = '2000';
		//print_r($this->UniqueRandomNumbersWithinRange(4));exit;
		$data_array = array();
		$sn='1';
		for($i=$iVal;$i<=$niVal;$i++){
	    //echo mt_rand($min,$max).'</br>';
			$keyVal = $request->prefix.'_'.$this->UniqueRandomNumbersWithinRange(4).mt_rand($min,$max);
			$valData[] = array($i,$request->bookId,$request->prefix,$keyVal,$request->place,'1');
		}
	    //print_r($valData);
	    //exit;
		//print_r( $this->UniqueRandomNumbersWithinRange(1000,2000,5) );exit;
		$data_array = $valData;

//$csv = "id,book_id,prefix,key,place,status \n";//Column headers
		$csv = "";
$headers = ['id','book_id','prefix','key','place','status'];//Column headers
foreach ($data_array as $record){
    $csv.= $record[0].','.$record[1].','.$record[2].','.$record[3].','.$record[4].','.$record[5]."\n"; //Append data to csv
}
$fileName = time().".csv";
$csv_handler = fopen ("public/uploads/access_csv/".$request->bookId.'/'.$request->place."/".$fileName,'w');
//fputcsv($csv_handler, $headers);
fwrite ($csv_handler,$csv);
fclose ($csv_handler);
$dataInsert['book_id'] = $request->bookId;
$dataInsert['prefix'] = $request->prefix;
$dataInsert['file'] = $fileName;
$dataInsert['type'] = $request->place;
$dataInsert['status'] = '1';
$dataInsert['created'] = date('Y-m-d');
\DB::table('accesskey')->insert($dataInsert);
\Session::flash('message', 'Access key generated successfully'); 
\Session::flash('alert-class', 'alert-success');	
return redirect('admin/bookKeys'.'/'.$request->bookId.'/'.$request->place);
}

public function UniqueRandomNumbersWithinRange($lng) {
	$original_string = array_merge(range('A', 'Z'));
	$original_string = implode("", $original_string);
	return substr(str_shuffle($original_string), 0, $lng);
}

public function downloadBookKeys($id,$place){
	$row = 0;
	$keys = \DB::table('accesskey')->where('book_id',$id)->where('type',$place)->get();	    
	if(count($keys) > 0){
		$row1 = [];
		foreach ($keys as $value) {
			
			
			if (($handle = fopen("public/uploads/access_csv/".$value->book_id."/".$value->type."/".$value->file, "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
					$num = count($data);
        //echo "<p> $num fields in line $row: <br /></p>\n";
					$row++;
					
					for ($c=0; $c < $num; $c++) {
						
						$row1[$row][$c] = $data[$c];
						
					} 
				}
  //  $f = fopen('php://memory', 'w'); 
   //$csv = "id,book_id,prefix,key,place,status \n";//Column headers
				$csv = "";
$headers = ['id','book_id','prefix','key','place','status'];//Column headers
foreach ($row1 as $record){
    $csv.= $record[0].','.$record[1].','.$record[2].','.$record[3].','.$record[4].','.$record[5]."\n"; //Append data to csv
}

$csv_handler = fopen('php://memory', 'w'); 


fpassthru($csv_handler);
header("Content-type: application/csv");
header("Content-Disposition: attachment; filename=accessKeys.csv");
$fp = fopen('php://output', 'w');
fputcsv($fp, $headers);
fwrite ($fp,$csv);
}
} }
}
}