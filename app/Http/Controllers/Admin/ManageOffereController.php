<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Helper;
use Session;
use Redirect;
use Auth;

class ManageOffereController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	if(Auth::check()){
    		$offers = \DB::table('offers')->get();
    		return view('admin.manageoffere.index',compact('offers'));
    	}else{
    		\Session::flash('message', 'You are logged out'); 
            \Session::flash('alert-class', 'alert-error');  
            return redirect('login');
    	}
		
	}
	public function edit(Request $request,$id){
        if(\Auth::check()){
        if(isset($request->submit)){
            $data['label']       =   $request->name;
            $data['status'] =   $request->status;
            $data['type'] =   $request->type;
            $data['offer'] =   $request->offer;

            \DB::table('offers')->where('id',$id)->update($data);

            \Session::flash('message', 'Record updated successfully'); 
            \Session::flash('alert-class', 'alert-success');

            return redirect('admin/manageoffere');

        }else{
            $offer = \DB::table('offers')->where('id',$id)->first();

            return view('admin.manageoffere.edit',compact('offer'));
        }
        }else{
            \Session::flash('message', 'You are logged out'); 
            \Session::flash('alert-class', 'alert-error');  
            return redirect('login');
        }
    }

}