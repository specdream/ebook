<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTb_Course_AssignRequest;
use App\Tb_Course_Assign;
use App\Tb_Courses;
use App\User;
class Tb_Course_AssignController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	$tb_course_assign = Tb_Course_Assign::with('tb_courses')->with('users')->get();

		return view('admin.tb_course_assign.index', compact('tb_course_assign'));
		// return view('admin.tb_course_assign.index');
	}

	/**
	 * Show the form for creating a new sdream_states
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    $Tb_Courses = Tb_Courses::where('status','Accept')->orderBy('id', 'desc')->with("tb_languages_settings")->get(); 
	    $Users = User::where('role_id','3')->get();

	    
	    return view('admin.tb_course_assign.create', compact("Tb_Courses","Users"));
	}

	public function store(CreateTb_Course_AssignRequest $request)
	{
	    
		Tb_Course_Assign::create($request->all());
		return redirect()->route(config('quickadmin.route').'.tb_course_assign.index');
	}

	public function edit($id)
	{
		$tb_course_assign = Tb_Course_Assign::find($id); 
	    $Tb_Courses = Tb_Courses::where('status','Accept')->orderBy('id', 'desc')->with("tb_languages_settings")->get(); 
	    $Users = User::where('role_id','3')->get();

		return view('admin.tb_course_assign.edit', compact('tb_course_assign','Tb_Courses','Users'));
	}

	public function update($id, CreateTb_Course_AssignRequest $request)
	{
		$Course_Assign = Tb_Course_Assign::findOrFail($id); 		
	    $Course_Assign->update($request->all());
		return redirect()->route(config('quickadmin.route').'.tb_course_assign.index');
	}
	public function destroy($id)
	{
		Tb_Course_Assign::destroy($id);		
		return redirect()->route(config('quickadmin.route').'.tb_course_assign.index');
	}

}