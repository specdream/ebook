<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\sdream_Cities;
use App\Http\Requests\Createsdream_CitiesRequest;
use App\Http\Requests\Updatesdream_CitiesRequest;
use Illuminate\Http\Request;

use App\sdream_States;


class sdream_CitiesController extends Controller {

	/**
	 * Display a listing of sdream_cities
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $sdream_cities = sdream_Cities::with("sdream_states")->get();

		return view('admin.sdream_cities.index', compact('sdream_cities'));
	}

	/**
	 * Show the form for creating a new sdream_cities
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    $sdream_states = sdream_States::pluck("name", "id")->prepend('Please select', '');

	    
	    return view('admin.sdream_cities.create', compact("sdream_states"));
	}

	/**
	 * Store a newly created sdream_cities in storage.
	 *
     * @param Createsdream_CitiesRequest|Request $request
	 */
	public function store(Createsdream_CitiesRequest $request)
	{
	    
		sdream_Cities::create($request->all());

		return redirect()->route(config('quickadmin.route').'.sdream_cities.index');
	}

	/**
	 * Show the form for editing the specified sdream_cities.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$sdream_cities = sdream_Cities::find($id);
	    $sdream_states = sdream_States::pluck("name", "id")->prepend('Please select', '');

	    
		return view('admin.sdream_cities.edit', compact('sdream_cities', "sdream_states"));
	}

	/**
	 * Update the specified sdream_cities in storage.
     * @param Updatesdream_CitiesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, Updatesdream_CitiesRequest $request)
	{
		$sdream_cities = sdream_Cities::findOrFail($id);

        

		$sdream_cities->update($request->all());

		return redirect()->route(config('quickadmin.route').'.sdream_cities.index');
	}

	/**
	 * Remove the specified sdream_cities from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		sdream_Cities::destroy($id);

		return redirect()->route(config('quickadmin.route').'.sdream_cities.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            sdream_Cities::destroy($toDelete);
        } else {
            sdream_Cities::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.sdream_cities.index');
    }

}
