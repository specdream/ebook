<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Teacher;
use App\Tb_Professional;
use App\Http\Requests\Createsdream_TeacherRequest;
use App\Http\Requests\Updatesdream_TeacherRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class TeacherController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
		
		$teachers = Teacher::where('role_id','=','4')->get();

        return view('admin.teacher.index', compact('teachers'));
	}

	/**
	 * Show the form for creating a new sdream_countries
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    $tb_professional=Tb_Professional::pluck("name", "id")->prepend('Please select', 0);
	    
	    return view('admin.teacher.create', compact('tb_professional'));
	}

	/**
	 * Remove the specified sdream_states from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Teacher::destroy($id);

		return redirect()->route(config('quickadmin.route').'.teacher.index');
	}

	public function edit($id)
	{
		$teacher = Teacher::findOrFail($id);  
	    $tb_professional=Tb_Professional::pluck("name", "id")->prepend('Please select', 0);
		return view('admin.teacher.edit', compact('teacher','tb_professional'));
	}

	/**
	 * Store a newly created sdream_countries in storage.
	 *
     * @param Createsdream_CountriesRequest|Request $request
	 */
	public function store(Createsdream_TeacherRequest $request)
	{
		$request = $this->saveFiles($request);		
	    $request['password'] = Hash::make($request['password']);
	    $request['role_id'] = '4';
		$request['username'] = $request->name;
		// print_r($request);die();
		Teacher::create($request->all());

		return redirect()->route(config('quickadmin.route').'.teacher.index');
	}
	/**
	 * Update the specified sdream_countries in storage.
     * @param Updatesdream_CountriesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, Updatesdream_TeacherRequest $request)
	{
		$teacher = Teacher::findOrFail($id); 
		$request = $this->saveFiles($request);		
	    $request['password'] = Hash::make($request['password']);
	    $request['role_id'] = '4';
		$request['username'] = $request->name;
		$teacher->update($request->all());

		return redirect()->route(config('quickadmin.route').'.teacher.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Teacher::destroy($toDelete);
        } else {
            Teacher::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.teacher.index');
    }

}