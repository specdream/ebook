<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Helper;
use Session;
use Redirect;

class SemestersController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	if(\Auth::check()){
    		$semesters = \DB::table('eb_semesters')->get();
    		return view('admin.semesters.index',compact('semesters'));
    	}else{

    		\Session::flash('message', 'You are logged out'); 
	        \Session::flash('alert-class', 'alert-error');	
	        return redirect('login');

    	}
	}

    public function create(){
        if(\Auth::check()){
            return view('admin.semesters.create');
        }else{
            \Session::flash('message', 'You are logged out'); 
            \Session::flash('alert-class', 'alert-error');  
            return redirect('login');
        }
    }

    public function store(Request $request){
        if(\Auth::check()){
            $semester = $request->Semester;
            $start = $request->start;
            $end = $request->end;
        //print_r($semester);exit;
        for($i=0;$i<count($semester);$i++){
        
        if($semester[$i] != ''){
            $data['name']   =   $semester[0];
            $data['start']   =   $start[0];
            $data['end']   =   $end[0];
            $data['status'] =   '1';   
            $data['created'] =   time();    
            \DB::table('eb_semesters')->insert($data);
        }

        }
        
        

        \Session::flash('message', 'Record inserted successfully'); 
        \Session::flash('alert-class', 'alert-success');
        
        return redirect('admin/semesters');
        }else{
            \Session::flash('message', 'You are logged out'); 
            \Session::flash('alert-class', 'alert-error');  
            return redirect('login');
        }
    }

    public function edit(Request $request,$id){
        if(\Auth::check()){
        if(isset($request->submit)){
            $data['name']       =   $request->name;
            $data['status'] =   $request->status;
            $data['start'] =   $request->start;
            $data['end'] =   $request->end;

            \DB::table('eb_semesters')->where('id',$id)->update($data);

            \Session::flash('message', 'Record updated successfully'); 
            \Session::flash('alert-class', 'alert-success');

            return redirect('admin/semesters');

        }else{
            $semesters = \DB::table('eb_semesters')->where('id',$id)->first();

            return view('admin.semesters.edit',compact('semesters'));
        }
        }else{
            \Session::flash('message', 'You are logged out'); 
            \Session::flash('alert-class', 'alert-error');  
            return redirect('login');
        }
    }

    public function delete($id){
        if(\Auth::check()){
        \DB::table('eb_semesters')->where('id',$id)->delete();

        \Session::flash('message', 'Record deleted successfully'); 
        \Session::flash('alert-class', 'alert-success');
        return redirect('admin/semesters');
        }else{
            \Session::flash('message', 'You are logged out'); 
            \Session::flash('alert-class', 'alert-error');  
            return redirect('login');
        }
    }


}