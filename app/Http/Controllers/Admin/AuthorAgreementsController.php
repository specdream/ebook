<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Helper;
use Session;
use Redirect;

class AuthorAgreementsController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	if (\Auth::check())
        {
            $user_id = \Auth::user()->id;
            $activePlan = \DB::table('users')->select('authorPlan')->where('id',$user_id)->first();
            $policyData = \DB::table('authorPlan')->get();

            return view('admin.authoragreements.index',compact('activePlan','policyData'));
        }
        else
        {

	        \Session::flash('message', 'You are logged out'); 
	        \Session::flash('alert-class', 'alert-error');	
	        return redirect('login');

        }
		
	}

	public function setPlan(Request $request)
    {
        $updateData['authorPlan'] = $request->plan_id;
        \DB::table('users')->where('id',\Auth::user()->id)->update($updateData);
        echo "success"; 
        exit;
    }

}