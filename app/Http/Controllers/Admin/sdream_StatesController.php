<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\sdream_States;
use App\Http\Requests\Createsdream_StatesRequest;
use App\Http\Requests\Updatesdream_StatesRequest;
use Illuminate\Http\Request;

use App\sdream_Countries;


class sdream_StatesController extends Controller {

	/**
	 * Display a listing of sdream_states
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $sdream_states = sdream_States::with("sdream_countries")->get();

		return view('admin.sdream_states.index', compact('sdream_states'));
	}

	/**
	 * Show the form for creating a new sdream_states
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    $sdream_countries = sdream_Countries::pluck("name", "id")->prepend('Please select', '');

	    
	    return view('admin.sdream_states.create', compact("sdream_countries"));
	}

	/**
	 * Store a newly created sdream_states in storage.
	 *
     * @param Createsdream_StatesRequest|Request $request
	 */
	public function store(Createsdream_StatesRequest $request)
	{
	    
		sdream_States::create($request->all());

		return redirect()->route(config('quickadmin.route').'.sdream_states.index');
	}

	/**
	 * Show the form for editing the specified sdream_states.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$sdream_states = sdream_States::find($id);
	    $sdream_countries = sdream_Countries::pluck("name", "id")->prepend('Please select', '');

	    
		return view('admin.sdream_states.edit', compact('sdream_states', "sdream_countries"));
	}

	/**
	 * Update the specified sdream_states in storage.
     * @param Updatesdream_StatesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, Updatesdream_StatesRequest $request)
	{
		$sdream_states = sdream_States::findOrFail($id);

        

		$sdream_states->update($request->all());

		return redirect()->route(config('quickadmin.route').'.sdream_states.index');
	}

	/**
	 * Remove the specified sdream_states from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		sdream_States::destroy($id);

		return redirect()->route(config('quickadmin.route').'.sdream_states.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            sdream_States::destroy($toDelete);
        } else {
            sdream_States::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.sdream_states.index');
    }

}
