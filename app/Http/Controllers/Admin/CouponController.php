<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Coupon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class CouponController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
		
		$companies = \DB::table('coupon')->get();

		

        return view('admin.coupon.index', compact('companies'));
	}

	/**
	 * Show the form for creating a new sdream_countries
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    $keyVal = time();		
		$un=  uniqid();
		$dmtun = $keyVal.$un;
		$mdun = md5($dmtun.$un);
		$sort=substr($mdun, 20);
		
	    return view('admin.coupon.create', compact('sort'));
	}

	/**
	 * Remove the specified sdream_states from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Company::destroy($id);

		return redirect()->route(config('quickadmin.route').'.company.index');
	}

	public function edit($id)
	{
		$company = \DB::table('coupon')->find($id);  
	    
		return view('admin.coupon.edit', compact('company'));
	}

	/**
	 * Store a newly created sdream_countries in storage.
	 *
     * @param Createsdream_CountriesRequest|Request $request
	 */
	public function store(Request $request)
	{
		$data['code'] = $request->code;
		$data['expire'] = $request->date;
		$data['price'] = $request->price;
		$data['type'] = $request->type;
		\DB::table('coupon')->insert($data);

		return redirect('admin/coupon');
	}
	/**
	 * Update the specified sdream_countries in storage.
     * @param Updatesdream_CountriesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, Updatesdream_CompanyRequest $request)
	{
		$company = Company::findOrFail($id); 
		$request = $this->saveFiles($request);		
	    $request['password'] = Hash::make($request['password']);
	    $request['role_id'] = '3';
		$request['username'] = $request->name;
		$company->update($request->all());

		return redirect()->route(config('quickadmin.route').'.coupon.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Company::destroy($toDelete);
        } else {
            Company::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.coupon.index');
    }

    public function genCoupon(){
    	$keyVal = time();		
		$un=  uniqid();
		$dmtun = $keyVal.$un;
		$mdun = md5($dmtun.$un);
		$sort=substr($mdun, 20);
		echo $sort;
    }

}