<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateSupport_SystemRequest;
use App\Http\Requests\CreateSupport_SystemRepRequest;
use App\Support_System;
use Redirect;

class Support_SystemController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	$ticket=Support_System::where('subject','!=','')->where('to_id',\Auth::user()->id)->orWhere('from_id',\Auth::user()->id)->whereNull('rep_id')->get();
		return view('admin.support_system.index', compact('ticket'));
	}

	public function create()
	{
	    return view('admin.support_system.create');
	}
	public function store(CreateSupport_SystemRequest $request)
	{	    
		$request['from_id']=\Auth::user()->id;
		$request['to_id']='1';
		// print_r($request->all());die();

		Support_System::create($request->all());
		return Redirect::to(config('quickadmin.route').'/support_system')->withErrors([ 'create']);;
	}
	public function hold($id)
	{	  
        $ticket = Support_System::findOrFail($id);
        $request['status'] = 'hold';
        $ticket->update($request);
		return Redirect::to(config('quickadmin.route').'/support_system')->withErrors([ 'hold']);;
	}
	public function reply($id)
	{
		$ticket=Support_System::find($id);
		$reply_det=Support_System::where('rep_id',$id)->get();
	    return view('admin.support_system.reply',compact('ticket','reply_det'));
	}
	public function closed($id)
	{	  
        $ticket = Support_System::findOrFail($id);
        $request['status'] = 'close';
        $ticket->update($request);
		return Redirect::to(config('quickadmin.route').'/support_system/reply/'.$id)->withErrors([ 'close']);;
	}
	public function chat(CreateSupport_SystemRepRequest $request)
	{	    
		$id=$request['chat_id'];
		$ticket = Support_System::findOrFail($id);
		$request['from_id']=\Auth::user()->id;
		$request['to_id']=$ticket->from_id;
		$request['rep_id']=$id;
		if($ticket->status=='open' || $ticket->status=='hold'){
		$request1['status'] = 'replied';
        $ticket->update($request1);
    	}
		// print_r($request->all());die();

		Support_System::create($request->all());
		return Redirect::to(config('quickadmin.route').'/support_system/reply/'.$id)->withErrors([ 'reply']);;
	}

}