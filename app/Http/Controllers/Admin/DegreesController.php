<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Helper;
use Session;
use Redirect;

class DegreesController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	if(\Auth::check()){
    		$degrees = \DB::table('eb_degrees')->get();
    		return view('admin.degrees.index',compact('degrees'));
    	}else{

    		\Session::flash('message', 'You are logged out'); 
	        \Session::flash('alert-class', 'alert-error');	
	        return redirect('login');

    	}
	}

    public function create(){
        if(\Auth::check()){
            return view('admin.degrees.create');
        }else{
            \Session::flash('message', 'You are logged out'); 
            \Session::flash('alert-class', 'alert-error');  
            return redirect('login');
        }
    }

    public function store(Request $request){
        if(\Auth::check()){
            $degree = $request->degree;
        //print_r($degree);exit;
        for($i=0;$i<count($degree);$i++){
        
        if($degree[$i] != ''){
            $data['name']   =   $degree[$i];
            $data['status'] =   '1';   
            $data['created'] =   time();    
            \DB::table('eb_degrees')->insert($data);
        }

        }
        
        

        \Session::flash('message', 'Record inserted successfully'); 
        \Session::flash('alert-class', 'alert-success');
        
        return redirect('admin/degrees');
        }else{
            \Session::flash('message', 'You are logged out'); 
            \Session::flash('alert-class', 'alert-error');  
            return redirect('login');
        }
    }

    public function edit(Request $request,$id){
        if(\Auth::check()){
        if(isset($request->submit)){
            $data['name']       =   $request->name;
            $data['status'] =   $request->status;

            \DB::table('eb_degrees')->where('id',$id)->update($data);

            \Session::flash('message', 'Record updated successfully'); 
            \Session::flash('alert-class', 'alert-success');

            return redirect('admin/degrees');

        }else{
            $degrees = \DB::table('eb_degrees')->where('id',$id)->first();

            return view('admin.degrees.edit',compact('degrees'));
        }
        }else{
            \Session::flash('message', 'You are logged out'); 
            \Session::flash('alert-class', 'alert-error');  
            return redirect('login');
        }
    }

    public function delete($id){
        if(\Auth::check()){
        \DB::table('eb_degrees')->where('id',$id)->delete();

        \Session::flash('message', 'Record deleted successfully'); 
        \Session::flash('alert-class', 'alert-success');
        return redirect('admin/degrees');
        }else{
            \Session::flash('message', 'You are logged out'); 
            \Session::flash('alert-class', 'alert-error');  
            return redirect('login');
        }
    }

}