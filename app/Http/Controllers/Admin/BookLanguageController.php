<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Book_language;
use DB;

class BookLanguageController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	$getLanguage = Book_language::get();
		return view('admin.booklanguage.index',compact('getLanguage'));
	}

	public function create(){
		return view('admin.booklanguage.create');
	}

	public function store(Request $request){

		$data['value']		=	$request->language;
		$data['short']	=	$request->key;
		$data['status']	=	'1';

		\DB::table('tb_book_language')->insert($data);

		\Session::flash('message', 'Record inserted successfully'); 
		\Session::flash('alert-class', 'alert-success');
		
		return redirect('admin/booklanguage');

	}

	public function edit(Request $request,$id){
		if(isset($request->submit)){
			$data['value']		=	$request->language;
			$data['short']	=	$request->key;
			$data['status']	=	$request->status;

			\DB::table('tb_book_language')->where('id',$id)->update($data);

			return redirect('admin/booklanguage');

		}else{
			$language = Book_language::where('id',$id)->first();

			return view('admin.booklanguage.edit',compact('language'));
		}
	}

	public function delete($id){

		\DB::table('tb_book_language')->where('id',$id)->delete();
		return redirect('admin/booklanguage');
	}

}