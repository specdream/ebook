<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Lession;
use App\Test_Type;
use App\Tb_Courses;
use App\Question;
use App\Answer;
use App\Http\Requests\CreateQuestionRequest;
use App\Http\Requests\UpdateLessionRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class QuestionController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index($lession_id,$course_id)
	{
		
		$lession = Lession::where('id',$lession_id)->where('course_id',$course_id)->get();
		$course = Tb_Courses::where('id',$course_id)->get();

		if(count($lession)>0 && count($course)>0){
			$course_name=$course[0]->title;
			$lession_name=$lession[0]->name;
			$test_type=$lession[0]->Test_type->name;
			
			$questions = Question::where('course_id',$course_id)->where('lession_id',$lession_id)->get(); 


			return view('admin.question.index', compact('questions','course_id','lession_id','course_name','lession_name','test_type'));
		}else{
			return Redirect::to(config('quickadmin.route').'/lession/'.$lession_id);
		}
	}
	/**
	 * Show the form for creating a new sdream_countries
	 *
     * @return \Illuminate\View\View
	 */
	public function create($lession_id,$course_id)
	{
	    
	    $course_id = $course_id; 
	    $lession = Lession::where('id',$lession_id)->where('course_id',$course_id)->first();
	    $test_type_id=$lession->Test_type->id;
	    return view('admin.question.create', compact('course_id','lession_id','test_type','test_type_id'));
	}
	/**
	 * Store a newly created sdream_countries in storage.
	 *
     * @param Createsdream_CountriesRequest|Request $request
	 */
	public function store(CreateQuestionRequest $request)
	{
			
		
		$file_url='';
		if($request->question_type!='1' && $request->question_subtype=='1'){
			if($request->file('uploads_urlq')){
				$filename = time() . '-' . $request->file('uploads_urlq')->getClientOriginalName();
				$request->file('uploads_urlq')->move(public_path('uploads/question'), $filename);
				$file_url =$filename;
			}else{
				$file_url ='';
			}
		}else if($request->question_type!='1' && $request->question_subtype=='2'){
			$file_url = $request->file_urlq;
		}

			$data_question=array(
						'course_id'=>$request->course_id,
						'lession_id'=>$request->lession_id,
						'test_type'=>$request->test_type,
						'question_type'=>$request->question_type,
						'question_subtype'=>$request->question_subtype,
						'question'=>$request->question,
						'file_url'=>$file_url,
						'mark'=>$request->mark
				);
			$q_id=\DB::table('tb_lession_question')->insertGetId($data_question);
			
			foreach($request->answer as $key=>$value){
				$file_url ='';
				if($request->ans_type!='1' && $request->ans_subtype=='1'){
					if($request->file('uploads_url'.$key)){
						$filename = time() . '-' . $request->file('uploads_url'.$key)->getClientOriginalName();
						$request->file('uploads_url'.$key)->move(public_path('uploads/question'), $filename);
						$file_url =$filename;
					}else{
						$file_url ='';
					}
					
				}else if($request->ans_type!='1' && $request->ans_subtype=='2'){
					$file_url = $request->post('file_url'.$key);
				}
				
				$data_answer=array(
						'qid'=>$q_id,
						'answer_type'=>$request->ans_type,
						'answer_subtype'=>$request->ans_subtype,
						'answer'=>$value,
						'file_url'=>$filename,
						'correct_answer'=>$request->correct_answer[$key],
				);
			\DB::table('tb_lession_answer')->insertGetId($data_answer);
			}
		return Redirect::to(config('quickadmin.route').'/question/'.$request->course_id."/".$request->lession_id);
	
	}

	public function edit($id,$course_id,$lession_id)
	{
		$lession = Lession::where('id',$lession_id)->where('course_id',$course_id)->get();
		$course = Tb_Courses::where('id',$course_id)->get();
		$lession = Question::where('id',$id)->where('lession_id',$lession_id)->where('course_id',$course_id)->get();
		if(count($lession)>0 && count($course)>0 && count($lession)>0){
			$course_name=$course[0]->title;
			$lession_name=$lession[0]->name;
			$test_type=$lession[0]->Test_type->name;
			$test_type_id=$lession[0]->Test_type->id;
			
			$question = Question::where('id',$id)->where('course_id',$course_id)->where('lession_id',$lession_id)->first(); 
			$answers = \DB::table('tb_lession_answer')->where('qid',$id)->orderBy('id','ASC')->get(); 
			return view('admin.question.edit', compact('question','course_id','lession_id','course_name','lession_name','test_type','test_type_id','answers'));
		}else{
			return Redirect::to(config('quickadmin.route').'/lession/'.$course_id.'/'.$lession_id);
		}
	}
	/**
	 * Store a newly created sdream_countries in storage.
	 *
     * @param Createsdream_CountriesRequest|Request $request
	 */
	public function update(CreateQuestionRequest $request)
	{
			

		$file_url='';$file_urlqq='';
		if($request->question_type!='1' && $request->question_subtype=='1'){
			if($request->file('uploads_urlq')){
				$filename = time() . '-' . $request->file('uploads_urlq')->getClientOriginalName();
				$request->file('uploads_urlq')->move(public_path('uploads/question'), $filename);
				
				$file_urlqq=$filename;
			}else{
			$Tb_Lession_Question = Question::where('id',$request->qid)->first(); 
			$file_urlqq=$Tb_Lession_Question->file_url;
			}
		}else if($request->question_type!='1' && $request->question_subtype=='2'){
			$file_urlqq = $request->file_urlq;
			// $data_answer['file_url']=$file_url;
		}else{
			// $data_answer['file_url']='';
		}

			$data_question=array(
						'question_type'=>$request->question_type,
						'question_subtype'=>$request->question_subtype,
						'question'=>$request->question,
						'file_url'=>$file_urlqq,
						'mark'=>$request->mark
				);
			\DB::table('tb_lession_question')->where('id',$request->qid)->update($data_question);
			
			\DB::table('tb_lession_answer')->where('qid',$request->qid)->delete();
			$data_answer=array();
			foreach($request->answer as $key=>$value){

				$file_url ='';
				if($request->ans_type!='1' && $request->ans_subtype=='1'){
					// print_r($request->file('uploads_url'.$key));die();
					if($request->file('uploads_url'.$key)!=''){
						$filename = time() . '-' . $request->file('uploads_url'.$key)->getClientOriginalName();
						$request->file('uploads_url'.$key)->move(public_path('uploads/question'), $filename);
						
						$file_url=$filename;
					}else{
						$file_url=$request->post('file_url_org_'.$key);
					}
					
				}else if($request->ans_type!='1' && $request->ans_subtype=='2'){
					$file_url = $request->post('file_url'.$key);
					$file_url=$file_url;
				}else{
					$file_url='';
				}
				
				$data_answer=array(
						'qid'=>$request->qid,
						'answer_type'=>$request->ans_type,
						'answer_subtype'=>$request->ans_subtype,
						'answer'=>$value,
						'file_url'=>$file_url,
						'correct_answer'=>$request->correct_answer[$key],
				);

				
			\DB::table('tb_lession_answer')->insertGetId($data_answer);
			}
		return Redirect::to(config('quickadmin.route').'/question/'.$request->course_id."/".$request->lession_id);
	
	}

	/**
	 * Remove the specified Questions from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id,$course_id,$lession_id)
	{
		Question::destroy($id);	
		\DB::table('tb_lession_answer')->where('qid',$id)->delete();	
		return Redirect::to(config('quickadmin.route').'/question/'.$course_id.'/'.$lession_id);
	}

}