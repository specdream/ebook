<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Tb_Categories_Type;
use App\Http\Requests\CreateTb_Categories_TypeRequest;
use App\Http\Requests\UpdateTb_Categories_TypeRequest;
use Illuminate\Http\Request;



class Tb_Categories_TypeController extends Controller {

	/**
	 * Display a listing of tb_categories_type
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $tb_categories_type = Tb_Categories_Type::all();

		return view('admin.tb_categories_type.index', compact('tb_categories_type'));
	}

	/**
	 * Show the form for creating a new tb_categories_type
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
        $status = Tb_Categories_Type::$status;

	    return view('admin.tb_categories_type.create', compact("status"));
	}

	/**
	 * Store a newly created tb_categories_type in storage.
	 *
     * @param CreateTb_Categories_TypeRequest|Request $request
	 */
	public function store(CreateTb_Categories_TypeRequest $request)
	{
	    
		Tb_Categories_Type::create($request->all());

		return redirect()->route(config('quickadmin.route').'.tb_categories_type.index');
	}

	/**
	 * Show the form for editing the specified tb_categories_type.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$tb_categories_type = Tb_Categories_Type::find($id);
	    
	    
        $status = Tb_Categories_Type::$status;

		return view('admin.tb_categories_type.edit', compact('tb_categories_type', "status"));
	}

	/**
	 * Update the specified tb_categories_type in storage.
     * @param UpdateTb_Categories_TypeRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateTb_Categories_TypeRequest $request)
	{
		$tb_categories_type = Tb_Categories_Type::findOrFail($id);

        

		$tb_categories_type->update($request->all());

		return redirect()->route(config('quickadmin.route').'.tb_categories_type.index');
	}

	/**
	 * Remove the specified tb_categories_type from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Tb_Categories_Type::destroy($id);

		return redirect()->route(config('quickadmin.route').'.tb_categories_type.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Tb_Categories_Type::destroy($toDelete);
        } else {
            Tb_Categories_Type::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.tb_categories_type.index');
    }

}
