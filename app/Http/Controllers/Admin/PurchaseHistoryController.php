<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class PurchaseHistoryController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	if(\Auth::user()){
            $user_id = \Auth::user()->id;
            $history = \DB::table('eb_book_purchase')->where('author_id',$user_id)->get();
            $uniq_id = array();
            $historyData = array();
            if(count($history) > 0){
                foreach ($history as $value) {
                    array_push($uniq_id, $value->uniq_id);
                }
            }
            if(count($uniq_id) > 0){
                $pD = '0';
                //$uniq_id = array_unique($uniq_id);
                //print_r($uniq_id);exit;
                foreach ($uniq_id as $value) {
                    $pHistory = \DB::table('eb_book_purchase')->where('uniq_id',$value)->first();
                    $title = \Helper::bookDetail($pHistory->book_id)->title;
                    $author_id = \Helper::bookDetail($pHistory->book_id)->user_id;
                    $author = \Helper::getUser($author_id,'name');
                    $username = \Helper::getUser($pHistory->user_id,'name');
                    $historyData[$pD]['uniq_id'] = $value;
                    $historyData[$pD]['title'] = $title;
                    $historyData[$pD]['author'] = $author;
                    $historyData[$pD]['username'] = $username;
                    $historyData[$pD]['created'] = $pHistory->created;
                    $pD++;
                }
            }
            return view('admin.purchasehistory.index',compact('historyData'));
        }else{
            \Session::flash('message', 'You are logged out'); 
            \Session::flash('alert-class', 'alert-warning');
            return redirect('login');
        }
	}

}