<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\BookCategories;
use Illuminate\Http\Request;

class AuthoBookCatgController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	$getCatg = BookCategories::get();

		return view('admin.authobookcatg.index',compact('getCatg'));
	}

	public function create(){

		return view('admin.authobookcatg.create');
	}

	public function store(Request $request){

		$catg = $request->categorie;
		//print_r($catg);exit;
		for($i=0;$i<count($catg);$i++){
		
		if($catg[$i] != ''){
			$data['name']	=	$catg[$i];
			$data['status']	=	'1';	
			BookCategories::insert($data);
		}

		}
		
		

		\Session::flash('message', 'Record inserted successfully'); 
		\Session::flash('alert-class', 'alert-success');
		
		return redirect('admin/authobookcatg');

	}

	public function edit(Request $request,$id){
		if(isset($request->submit)){
			$data['name']		=	$request->name;
			$data['status']	=	$request->status;

			\DB::table('tb_book_categorie')->where('id',$id)->update($data);

			\Session::flash('message', 'Record updated successfully'); 
			\Session::flash('alert-class', 'alert-success');

			return redirect('admin/authobookcatg');

		}else{
			$catg = BookCategories::where('id',$id)->first();

			return view('admin.authobookcatg.edit',compact('catg'));
		}
	}

	public function delete($id){

		BookCategories::where('id',$id)->delete();

		\Session::flash('message', 'Record deleted successfully'); 
		\Session::flash('alert-class', 'alert-success');
		return redirect('admin/authobookcatg');
	}

}