<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Teacher;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Schema , DateTime;
use Helper;
use Session;
use Redirect;
use File;

class CollegeBooksController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	if(\Auth::check()){
            $book = \DB::table('eb_college_book')->get();
        	return view('admin.collegebooks.index',compact("book"));
    	}else{

    		\Session::flash('message', 'You are logged out'); 
	        \Session::flash('alert-class', 'alert-error');	
	        return redirect('login');

    	}
	}

    public function create(Request $request){

        if(\Auth::user()){

        $tb_users = Teacher::where('role_id','4')->pluck("name", "id")->prepend('Please select', '');       

        return view('admin.collegebooks.create',compact('tb_users'));

        }else{

        \Session::flash('message', 'You are logged out'); 
        \Session::flash('alert-class', 'alert-success');
        
        return redirect('');

        }
    }

    public function store(Request $request){
        //echo "string";exit;
        if(\Auth::user()){

        $dt = new DateTime;
        $cDate = $dt->format('Y-m-d h:m:s');
        $InsData['user_id']= $teacher_id = $request->teacher_id;
        $InsData['semester']=$request->semester;
        $InsData['degree']=$request->degrees;
        $InsData['title']=$request->title;
        $InsData['summary']=$request->summary;
        $InsData['price_type']=$request->price_type;
        $InsData['price']=$request->price;
        $InsData['isbn']=$request->ISBN;
        $InsData['created_date']=$cDate;
        $InsData['published_date']=$cDate;
        $InsData['status']='1';
        //print_r($InsData);exit;
        $insBook = \DB::table('eb_college_book')->insertGetID($InsData);
        if($request->hasFile('files'))
        {
        $request = $this->collegeBookFiles($request,$insBook);
        }

        
        $notification['user_id']='1';
        $User1=User::find($teacher_id);
        $notification['n_role']=$User1->role_id;
        $notification['n_user_id']=$User1->id;
        $notification['n_uname']=$User1->name;
        $notification['notification']='Hi '.$User1->name.', Admin create a new book under you';

        \DB::table('tb_notification')->insert($notification);

        \Session::flash('message', 'Record created successfully'); 
        \Session::flash('alert-class', 'alert-success');
        
        return redirect('admin/collegebooks');

        }else{

        \Session::flash('message', 'You are logged out'); 
        \Session::flash('alert-class', 'alert-success');
        
        return redirect('');

        }
    }

    public function edit(Request $request,$id){
        //echo "string";exit;
        if(\Auth::user()){

        if(isset($request->submit)){

            $dt = new DateTime;
            $cDate = $dt->format('Y-m-d h:m:s');
            $InsData['user_id']=$teacher_id = $request->teacher_id;
            $InsData['semester']=$request->semester;
            $InsData['degree']=$request->degrees;
            $InsData['title']=$request->title;
            $InsData['summary']=$request->summary;
            $InsData['price_type']=$request->price_type;
            $InsData['price']=$request->price;
            $InsData['isbn']=$request->ISBN;
            $InsData['status']=$request->status;
            //print_r($InsData);exit;
            $insBook = \DB::table('eb_college_book')->where('id',$id)->update($InsData);

            \Session::flash('message', 'Record deleted successfully'); 
            \Session::flash('alert-class', 'alert-success');
            
            return redirect('admin/collegebooks');

        }else{
            $tb_users = Teacher::where('role_id','4')->pluck("name", "id")->prepend('Please select', '');     
            $book = \DB::table('eb_college_book')->where('id',$id)->first();
            $bookFile = \DB::table('eb_college_book_files')->where('book_id',$id)->get();
            return view('admin.collegebooks.edit',compact("book","bookFile","tb_users"));
        }
        }else{

        \Session::flash('message', 'You are logged out'); 
        \Session::flash('alert-class', 'alert-success');
        
        return redirect('');

        }
    }


    public function delete($id){

        if(\Auth::user()){
        $checkPurchase = \DB::table('eb_college_book_purchase')->where('book_id',$id)->get();   

        if(count($checkPurchase) > 0){
            \Session::flash('message', 'Sorry, You can not able to delete this book becuase some one purchased this book'); 
            \Session::flash('alert-class', 'alert-warning');
            return redirect('admin/collegebooks');
        }
        
        File::deleteDirectory(public_path('uploads/college_books').'/'.$id);
        File::deleteDirectory(public_path('uploads/college_books_banner').'/'.$id);
        \DB::table('eb_college_book')->where('id',$id)->delete();
        \DB::table('eb_college_book_files')->where('book_id',$id)->delete();
        
        \Session::flash('message', 'Record deleted successfully'); 
        \Session::flash('alert-class', 'alert-success');
        return redirect('admin/collegebooks');
        }else{

        \Session::flash('message', 'You are logged out'); 
        \Session::flash('alert-class', 'alert-success');
        
        return redirect('');

        }

    }

    public function uploadBanner(Request $request){

        if(\Auth::user()){

        $book_id = $request->book_id;

        $book = \DB::table('eb_college_book')->where('id',$book_id)->first();
        //print_r($bookFile);exit;
                
        if($request->hasFile('banner'))
        { 
            //echo "string";exit;
        File::delete(public_path('uploads/college_books_banner').'/'.$book_id.'/'.$book->banner);   
        $request = $this->collegeBookFiles($request,$book_id);
        } 

        $book = \DB::table('eb_college_book')->where('id',$book_id)->first();
        echo url('public/uploads/college_books_banner').'/'.$book->id.'/'.$book->banner;
        }else{

        \Session::flash('message', 'You are logged out'); 
        \Session::flash('alert-class', 'alert-success');
        
        return redirect('');

        }
    }

    public function fileUpload(Request $request){

        if(\Auth::user()){

        $insBook = $request->book_id;

        if($request->hasFile('files'))
        { 
        $request = $this->collegeBookFiles($request,$insBook);
        } 

        $book = \DB::table('eb_college_book')->where('id',$insBook)->first();
        $bookFile = \DB::table('eb_college_book_files')->where('book_id',$insBook)->get();
        //print_r($bookFile);exit;
        $msi = '0'; $mri = '0';
        $echoData = '';
             for($mi=0;$mi<count($bookFile);$mi++){
                if($mi%4 == 0){
                    $mri = count($bookFile)-$mi;
                    if(4 > $mri){
                        if($mi != '0')
                        $msi = $mri+3;   
                        else
                        $msi = $mri-1;       
                    }else{
                    $msi = $mi+3;
                    }
                   
                    $echoData .= "<input type='hidden' id='totalFile' value='".count($bookFile)."'><div class='column'>";
                } 
                   if($bookFile[$mi]->type == '1'){ 
                $echoData .= "
                <div class='pre_image_div col-xs-12 col-sm-3 col-md-3'>
                <a href='javascript:void(0)' onclick='deletefile(".$bookFile[$mi]->id.",1)'><i class='fa fa-trash' aria-hidden='true'></i></a>
                  <img src='".url('public/uploads/college_books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."'>
                  <script type='text/javascript'>
                    $('#inputFile').attr('accept','image/*');
                    $('.up_btn').html('Add Image');
                  </script>
                  </div>";
                  }elseif($bookFile[$mi]->type == '2'){
                  $echoData .= "<div class='pre_image_div col-xs-12 col-sm-3 col-md-3'>
                  <a href='javascript:void(0)' onclick='deletefile(".$bookFile[$mi]->id.",1)'><i class='fa fa-trash' aria-hidden='true'></i></a>
                  <video src='".url('public/uploads/college_books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."'></video>
                  <script type='text/javascript'>
                      $('#inputFile').attr('accept','video/*');
                      $('.up_btn').html('Add Video');
                  </script>
                  </div>";
                  }elseif($bookFile[$mi]->type == '3'){
                  $echoData .= "<div class='pre_image_div col-xs-12 col-sm-3 col-md-3'>
                  <a href='javascript:void(0)' onclick='deletefile(".$bookFile[$mi]->id.",1)'><i class='fa fa-trash trash-pdf' aria-hidden='true'></i></a>
                  <object data='".url('public/uploads/college_books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."' type='application/pdf' width='100%' height='100%'>
                  <p>Alternative text - include a link <a href='".url('public/uploads/college_books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."'>to the PDF!</a></p>
                  </object>
                  <script type='text/javascript'>
                      $('#inputFile').attr('accept','application/pdf');
                      $('.up_btn').html('Add PDF');
                  </script>
                  </div>";
                  }elseif($bookFile[$mi]->type == '4'){
                  $echoData .= "<div class='pre_image_div col-xs-12 col-sm-3 col-md-3'>
                  <a href='javascript:void(0)' onclick='deletefile(".$bookFile[$mi]->id.",1)'><i class='fa fa-trash trash-pdf' aria-hidden='true'></i></a>
                  <object data='".url('public/uploads/college_books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."' type='application/vnd.ms-powerpoint' width='100%' height='100%'>
                  <p>Alternative text - include a link <a href='".url('public/uploads/college_books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."'>to the PDF!</a></p>
                  </object>
                  <script type='text/javascript'>
                      $('#inputFile').attr('accept','application/vnd.ms-powerpoint');
                      $('.up_btn').html('Add PowerPoint');
                  </script>
                  </div>";
              }
                 if($mi == $msi){
                    $echoData .= "</div>";    
                }
            }
        echo $echoData;exit;
        }else{

        \Session::flash('message', 'You are logged out'); 
        \Session::flash('alert-class', 'alert-success');
        
        return redirect('');

        }

    }

    public function deleteFile($id,$book_id,$type){

        if(\Auth::user()){
            
        $bookFile = \DB::table('eb_college_book_files')->where('id',$id)->first();
        //print_r($bookFile);exit;
        File::delete(public_path('uploads/college_books').'/'.$book_id.'/'.$bookFile->file);
        \DB::table('eb_college_book_files')->where('id',$id)->delete();

        $book = \DB::table('eb_college_book')->where('id',$book_id)->first();
        $bookFile = \DB::table('eb_college_book_files')->where('book_id',$book_id)->get();
        $msi = '0'; $mri = '0';
        $echoData = '';
             for($mi=0;$mi<count($bookFile);$mi++){
                if($mi%4 == 0){
                    $mri = count($bookFile)-$mi;
                    if(4 > $mri){
                        if($mi != '0')
                        $msi = $mri+3;   
                        else
                        $msi = $mri-1;       
                    }else{
                    $msi = $mi+3;
                    }
                   
                    $echoData .= "<input type='hidden' id='totalFile' value='".count($bookFile)."'><div class='column'>";
                } 
                   if($bookFile[$mi]->type == '1'){ 
                $echoData .= "<div class='pre_image_div col-xs-12 col-sm-3 col-md-3'>
                <a href='javascript:void(0)' onclick='deletefile(".$bookFile[$mi]->id.",1)'><i class='fa fa-trash' aria-hidden='true'></i></a>
                  <img src='".url('public/uploads/college_books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."'>
                  <script type='text/javascript'>
                    $('#inputFile').attr('accept','image/*');
                    $('.up_btn').html('Add Image');
                  </script>
                  </div>";
                  }elseif($bookFile[$mi]->type == '2'){
                  $echoData .= "<div class='pre_image_div col-xs-12 col-sm-3 col-md-3'>
                  <a href='javascript:void(0)' onclick='deletefile(".$bookFile[$mi]->id.",1)'><i class='fa fa-trash' aria-hidden='true'></i></a>
                  <video src='".url('public/uploads/college_books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."'></video>
                  <script type='text/javascript'>
                      $('#inputFile').attr('accept','video/*');
                      $('.up_btn').html('Add Video');
                  </script>
                  </div>";
                  }elseif($bookFile[$mi]->type == '3'){
                  $echoData .= "<div class='pre_image_div col-xs-12 col-sm-3 col-md-3'>
                  <a href='javascript:void(0)' onclick='deletefile(".$bookFile[$mi]->id.",1)'><i class='fa fa-trash trash-pdf' aria-hidden='true'></i></a>
                  <object data='".url('public/uploads/college_books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."' type='application/pdf' width='100%' height='100%'>
                  <p>Alternative text - include a link <a href='".url('public/uploads/college_books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."'>to the PDF!</a></p>
                  </object>
                  <script type='text/javascript'>
                      $('#inputFile').attr('accept','application/pdf');
                      $('.up_btn').html('Add PDF');
                  </script>
                  </div>";
                  }elseif($bookFile[$mi]->type == '4'){
                  $echoData .= "<div class='pre_image_div col-xs-12 col-sm-3 col-md-3'>
                  <a href='javascript:void(0)' onclick='deletefile(".$bookFile[$mi]->id.",1)'><i class='fa fa-trash trash-pdf' aria-hidden='true'></i></a>
                  <object data='".url('public/uploads/college_books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."' type='application/vnd.ms-powerpoint' width='100%' height='100%'>
                  <p>Alternative text - include a link <a href='".url('public/uploads/college_books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."'>to the PDF!</a></p>
                  </object>
                  <script type='text/javascript'>
                      $('#inputFile').attr('accept','application/vnd.ms-powerpoint');
                      $('.up_btn').html('Add PowerPoint');
                  </script>
                  </div>";
              }
                 if($mi == $msi){
                    $echoData .= "</div>";    
                }
            }
        echo $echoData;exit;
        }else{

        \Session::flash('message', 'You are logged out'); 
        \Session::flash('alert-class', 'alert-success');
        
        return redirect('');

        }
    }

}