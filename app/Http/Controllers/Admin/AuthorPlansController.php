<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Helper;
use Session;
use Redirect;

class AuthorPlansController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	if(\Auth::check())
        {
        $getData = \DB::table('authorPlan')->get();
        $policyData = $getData;
		return view('admin.authorplans.index',compact('policyData'));
		}else{
		\Session::flash('message', 'You are logged out'); 
        \Session::flash('alert-class', 'alert-error');	
        return redirect('login');
		}
	}

	public function edit($id){

		if(\Auth::check())
        {
        $getData = \DB::table('authorPlan')->where('id',$id)->first();
        $policyData = $getData;
        return view('admin.authorplans.edit',compact('policyData','id'));
		}else{
		\Session::flash('message', 'You are logged out'); 
        \Session::flash('alert-class', 'alert-error');	
        return redirect('login');	
		}
	}

	public function update(Request $request){

        $valEtcOpt = implode('||', $request->product_etcOpt);

        $updateData['price'] = $request->product_amount;
        $updateData['title'] = $request->product_name;
        $updateData['descp'] = $request->product_descp;
        $updateData['etcOpt'] = $valEtcOpt;
        $id = $request->id;
        $update = \DB::table('authorPlan')->where('id',$id)->update($updateData);
        \Session::flash('message', 'Plan updated successfully'); 
        \Session::flash('alert-class', 'alert-success');
        return redirect('admin/authorplans');
    }

}