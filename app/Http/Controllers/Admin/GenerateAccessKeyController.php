<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class GenerateAccessKeyController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	$companies = \DB::table('coupon')->get();
		return view('admin.generateaccesskey.index',compact("companies"));
	}

	public function create(){

		return view('admin.generateaccesskey.create');
	}

}