<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Lession;
use App\Test_Type;
use App\Tb_Courses;
use App\Http\Requests\CreateLessionRequest;
use App\Http\Requests\UpdateLessionRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class LessionController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index($course_id)
    {
		$course_id = $course_id; 
		$course = Tb_Courses::where('id',$course_id)->get();
		if(count($course)>0){
			$course_name=$course[0]->title;
			$lessions = Lession::where('course_id',$course_id)->get(); 


			return view('admin.lession.index', compact('lessions','course_id','course_name'));
		}else{
			return Redirect::to(config('quickadmin.route').'/tb_courses');
		}
	}

	/**
	 * Show the form for creating a new sdream_countries
	 *
     * @return \Illuminate\View\View
	 */
	public function create($course_id)
	{
	    
	    $course_id = $course_id; 
	    $test_type = Test_Type::pluck("name", "id")->prepend('Please select', 0);
	    $course = Tb_Courses::select('teacher_id')->where('id',$course_id)->first();
	    return view('admin.lession.create', compact('course_id','test_type','course'));
	}

	public function edit($id,$course_id)
	{
		$lession = Lession::find($id);  
	    $lession_id = $id; 
	    $test_type = Test_Type::pluck("name", "id")->prepend('Please select', 0);
	    $course = Tb_Courses::select('teacher_id')->where('id',$course_id)->first();
		return view('admin.lession.edit', compact('lession','lession_id','course_id','test_type','course'));
	}

	/**
	 * Store a newly created sdream_countries in storage.
	 *
     * @param Createsdream_CountriesRequest|Request $request
	 */
	public function store(CreateLessionRequest $request)
	{
		// die();
		// $request = $this->saveFiles($request);		
		// Lession::create($request->all());


		$file_url='';
		if($request->subtype=='1'){
			if($request->file('video')){
				$filename = time() . '-' . $request->file('video')->getClientOriginalName();
				$request->file('video')->move(public_path('uploads/lesson'), $filename);
				$file_url =$filename;
			}else{
				$file_url ='';
			}
		}else {
			$file_url = $request->file_url;
		}

			$data_question=array(
						'course_id'=>$request->course_id,
						'author_id'=>$request->author_id,
						'name'=>$request->name,
						'description'=>$request->description,
						'hours'=>$request->hours,
						'minutes'=>$request->minutes,
						'num_que'=>$request->num_que,
						'subtype_id'=>$request->subtype,
						'test_type'=>$request->test_type,
						'video'=>$file_url,
						'pass_percentage'=>$request->pass_percentage,
				);
			// print_r($data_question);die();
			$q_id=\DB::table('tb_lession')->insertGetId($data_question);
			


		return Redirect::to(config('quickadmin.route').'/lession/'.$request->course_id);
	}
	/**
	 * Update the specified sdream_countries in storage.
     * @param Updatesdream_CountriesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateLessionRequest $request)
	{
		$lession = Lession::where('id',$id)->first(); 
		// $request = $this->saveFiles($request);		
	 //    $lession->update($request->all());


	    $file_url='';
		if($request->subtype_id=='1'){
			if($request->file('video')){
				$filename = time() . '-' . $request->file('video')->getClientOriginalName();
				$request->file('video')->move(public_path('uploads/lesson'), $filename);
				$file_url =$filename;
			}else{
				$file_url =$lession->video;
			}
		}else {
			$file_url = $request->file_url;
		}

			$data_question=array(
						'course_id'=>$request->course_id,
						'author_id'=>$request->author_id,
						'name'=>$request->name,
						'description'=>$request->description,
						'hours'=>$request->hours,
						'minutes'=>$request->minutes,
						'num_que'=>$request->num_que,
						'subtype_id'=>$request->subtype_id,
						'test_type'=>$request->test_type,
						'video'=>$file_url,
						'pass_percentage'=>$request->pass_percentage,
				);
		\DB::table('tb_lession')->where('id',$request->id)->update($data_question);			

		return Redirect::to(config('quickadmin.route').'/lession/'.$request->course_id);
	}
	/**
	 * Remove the specified sdream_states from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id,$course_id)
	{
		Lession::destroy($id);

		
		return Redirect::to(config('quickadmin.route').'/lession/'.$course_id);
	}

	

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Lession::destroy($toDelete);
        } else {
            Lession::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.company.index');
    }

}