<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Tb_Languages;
use App\Http\Requests\CreateTb_LanguagesRequest;
use App\Http\Requests\UpdateTb_LanguagesRequest;
use Illuminate\Http\Request;



class Tb_LanguagesController extends Controller {

	/**
	 * Display a listing of tb_languages
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $tb_languages = Tb_Languages::all();

		return view('admin.tb_languages.index', compact('tb_languages'));
	}

	/**
	 * Show the form for creating a new tb_languages
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
	    return view('admin.tb_languages.create');
	}

	/**
	 * Store a newly created tb_languages in storage.
	 *
     * @param CreateTb_LanguagesRequest|Request $request
	 */
	public function store(CreateTb_LanguagesRequest $request)
	{
	    
		Tb_Languages::create($request->all());

		return redirect()->route(config('quickadmin.route').'.tb_languages.index');
	}

	/**
	 * Show the form for editing the specified tb_languages.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$tb_languages = Tb_Languages::find($id);
	    
	    
		return view('admin.tb_languages.edit', compact('tb_languages'));
	}

	/**
	 * Update the specified tb_languages in storage.
     * @param UpdateTb_LanguagesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateTb_LanguagesRequest $request)
	{
		$tb_languages = Tb_Languages::findOrFail($id);

        

		$tb_languages->update($request->all());

		return redirect()->route(config('quickadmin.route').'.tb_languages.index');
	}

	/**
	 * Remove the specified tb_languages from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Tb_Languages::destroy($id);

		return redirect()->route(config('quickadmin.route').'.tb_languages.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Tb_Languages::destroy($toDelete);
        } else {
            Tb_Languages::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.tb_languages.index');
    }

}
