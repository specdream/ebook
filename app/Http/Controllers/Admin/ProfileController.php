<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use Auth;
use App\Profile;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Requests\UpdateProfilepwdRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
class ProfileController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
		
		$profile = Profile::where('id','=',\Auth::user()->id)->first();

        return view('admin.profile.index', compact('profile'));
	}
	/**
	 * Update the specified profile in storage.
     * @param UpdateProfileRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateProfileRequest $request)
	{
		$teacher = Profile::findOrFail($id); 
		$request = $this->saveFiles($request);
	    $request['group_id'] = '1';
		$request['username'] = $request->username;
		$teacher->update($request->all());

		return redirect()->route(config('quickadmin.route').'.profile.index');
	}
	/**
	 * Update the specified profile in storage.
     * @param UpdateProfileRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function updatepwd($id, UpdateProfilepwdRequest $request)
	{
		if (!(Hash::check($request['oldpwd'], Auth::user()->password))) {
            // The passwords matches
            return Redirect::back()->withErrors(['msg', 'Your current password does not matches with the password you provided. Please try again.']);
        }
        if(strcmp($request['oldpwd'], $request['password']) == 0){
            //Current password and new password are same
            return Redirect::back()->withErrors(['msg', 'New Password cannot be same as your current password. Please choose a different password.']);
        }
 
        //Change Password
        $admin = Profile::findOrFail($id);
        // $user->password = bcrypt($request['password']);
        $password=Hash::make($request['password']);
        $group_id = '3';
		$username = $admin->username;
		\DB::table('users')->where('id',$id)->update(array('password'=>$password,'role_id'=>$group_id,'username'=>$username,));
        // $admin->update($request->all());
        return redirect()->back()->withErrors(["success","Password changed successfully !"]);
	}

	/**
	 * Update the specified profile in storage.
     * @param UpdateProfileRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function store($id, UpdateProfilepwdRequest $request)
	{
		$profile = Profile::findOrFail($id); 
		// print_r($profile);die();
		$request['role_id'] = '1';
		$request['username'] = $request->username;
		$request['password'] = Hash::make($request['password']);
		// if($request['password']==)
		//$profile->update($request->all());

		return redirect()->route(config('quickadmin.route').'.profile.index');
	}


}