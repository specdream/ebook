<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Schema , DateTime;
use App\Tb_Courses;
use App\Book;
use App\Question;
use App\Answer;
use App\User;
use App\Lession;
use App\Tb_Languages;
use App\Http\Requests\CreateTb_CoursesRequest;
use App\Http\Requests\UpdateTb_CoursesRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Traits\FileUploadTrait;
use App\Tb_Professional;
use App\Tb_Categories_Type;
use App\Teacher;

class AuthorBookCreateController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	$book = \DB::table('eb_book')->get();
		return view('admin.authorbookcreate.index',compact("book"));
	}

	public function create()
	{
		$tb_professional = Tb_Professional::pluck("name", "id")->prepend('Please select', 0);
		$tb_categories_type = Tb_Categories_Type::pluck("name", "id")->prepend('Please select', '');
		$tb_users = Teacher::where('role_id','4')->pluck("name", "id")->prepend('Please select', '');

        $status = Tb_Courses::$status;
        $price_type = Tb_Courses::$price_type;

		return view('admin.authorbookcreate.create',compact("tb_professional", "tb_categories_type", "tb_users", "status", "price_type"));
	}

		public function store(Request $request)
	{
		
	}
}