<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
// use Redirect;
use Schema , DateTime;
use App\Tb_Courses;
use App\Question;
use App\Answer;
use App\Lession;
use App\Tb_Languages;
use App\Http\Requests\CreateTb_CoursesRequest;
use App\Http\Requests\UpdateTb_CoursesRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Traits\FileUploadTrait;
use App\Tb_Professional;
use App\Tb_Categories_Type;
use App\Teacher;
use App\User;

class Tb_CoursesController extends Controller {

	/**
	 * Display a listing of tb_courses
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View

	 */
	  protected $retryAfter = 60;
	public function index(Request $request)
    {
    	if(\Auth::user()->role_id==1){
    		$tb_courses = Tb_Courses::with("tb_professional")->with("tb_categories_type")->get();
    	}else{
    		$tb_courses = Tb_Courses::with("tb_professional")->with("tb_categories_type")->where('teacher_id',\Auth::user()->id)->get();
    	}
        $language=Tb_Languages::all();

		return view('admin.tb_courses.index', compact('tb_courses','language'));
	}

	/**
	 * Show the form for creating a new tb_courses
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
		$tb_professional = Tb_Professional::pluck("name", "id")->prepend('Please select', 0);
		$tb_categories_type = Tb_Categories_Type::pluck("name", "id")->prepend('Please select', '');
		$tb_users = Teacher::where('role_id','4')->pluck("name", "id")->prepend('Please select', '');
		if(\Auth::user()->role_id == '4'){
		$book = \DB::table('eb_college_book')->where('status','1')->where('user_id',\Auth::user()->id)->get();
		}else{
		$book = \DB::table('eb_college_book')->where('status','1')->get();	
		}	
		if(count($book) == '0'){
			\Session::flash('message', 'There is no books are availabel'); 
            \Session::flash('alert-class', 'alert-success');
			return redirect('admin/tb_courses');
		}
        $status = Tb_Courses::$status;
        $price_type = Tb_Courses::$price_type;


	    return view('admin.tb_courses.create', compact("tb_professional", "tb_categories_type", "tb_users", "status", "price_type","book"));
	}

	/**
	 * Store a newly created tb_courses in storage.
	 *
     * @param CreateTb_CoursesRequest|Request $request
	 */
	public function store(CreateTb_CoursesRequest $request)
	{
	    $request = $this->saveFiles($request);
	    $dt = new DateTime;
	    $request['added_by']=\Auth::user()->id;
        if($request['status']=='Accept'){
        	$request['published_date']=$dt->format('Y-m-d');
        } 

        
		//print_r($request->all());exit;
		$insCourse = Tb_Courses::create($request->all());
		$insCourse->book_id = $request->book;
		$insCourse->save();
		
		/*if(\Auth::user()->role_id=='1'){
			$notification['user_id']='1';
			$userName = \Helper::getUser($book->user_id,'name')
			$notification['n_role']=\Auth::user()->role_id;
			$notification['n_user_id']=\Auth::user()->id;
			$notification['n_uname']=$User1->name;
			$notification['notification']=$User1->name.' created new course';
			
		}else{
			$notification['user_id']=$request['teacher_id'];
			$notification['n_role']='1';
			$notification['n_user_id']='1';
			$notification['n_uname']='Admin';
			$notification['notification']='Admin created one course to you';
		}*/
		//\DB::table('tb_notification')->insert($notification);

		return redirect()->route(config('quickadmin.route').'.tb_courses.index');
	}

	/**
	 * Show the form for editing the specified tb_courses.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$tb_courses = Tb_Courses::find($id);
	    $tb_professional = Tb_Professional::pluck("name", "id")->prepend('Please select', 0);
		$tb_categories_type = Tb_Categories_Type::pluck("name", "id")->prepend('Please select', '');
		$tb_users = Teacher::where('role_id','4')->pluck("name", "id")->prepend('Please select', '');

	    
        $status = Tb_Courses::$status;
        $price_type = Tb_Courses::$price_type;

		return view('admin.tb_courses.edit', compact('tb_courses', "tb_professional", "tb_categories_type", "tb_users", "status", "price_type"));
	}

	/**
	 * Update the specified tb_courses in storage.
     * @param UpdateTb_CoursesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateTb_CoursesRequest $request)
	{
		$tb_courses = Tb_Courses::findOrFail($id);

        $request = $this->saveFiles($request);
        $dt = new DateTime;
        if($request['status']=='Accept'){
        	$request['published_date']=$dt->format('Y-m-d');
        }        
// print_r($request->all());die();
		$tb_courses->update($request->all());

		return redirect()->route(config('quickadmin.route').'.tb_courses.index');
	}

	/**
	 * Remove the specified tb_courses from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Tb_Courses::destroy($id);

		return redirect()->route(config('quickadmin.route').'.tb_courses.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Tb_Courses::destroy($toDelete);
        } else {
            Tb_Courses::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.tb_courses.index');
    }


    public function view($id)
    {
    	$course = Tb_Courses::find($id);
		return view('admin.tb_courses.view', compact('id','course'));
    }
    public function view_lang($id,$lang)
    {
    	$course = Tb_Courses::find($id);

		return view('admin.tb_courses.view_lang', compact('id','course','lang'));
    }

    /**
	 * Approve the specified tb_courses from storage.
	 *
	 * @param  int  $id
	 */
	public function approve($id)
	{
		$tb_courses = Tb_Courses::findOrFail($id);
		$dt = new DateTime;
		$data=array(
			'status'=>'Accept',
			'published_date'=> $dt->format('Y-m-d')
			);
		$tb_courses->update($data);

		$notification['user_id']=$tb_courses->teacher_id;
		$notification['n_role']='1';
		$notification['n_user_id']='1';
		$notification['n_uname']='Admin';
		$notification['notification']='Admin has approved your course ( ID - '.$id.').';
		\DB::table('tb_notification')->insert($notification);

		
		return redirect()->route(config('quickadmin.route').'.tb_courses.index');
	}
	/**
	 * Approve the specified tb_courses from storage.
	 *
	 * @param  int  $id
	 */
	public function complete($id)
	{
		$tb_courses = Tb_Courses::findOrFail($id);
		$data=array(
			'status'=>'Draft',
			);
		$tb_courses->update($data);

		$notification['user_id']='1';
		$User1=\User::find($tb_courses->teacher_id);
		$notification['n_role']=\Auth::user()->role_id;
		$notification['n_user_id']=\Auth::user()->id;
		$notification['n_uname']=$User1->name;
		$notification['notification']=$User1->name.' has completed one course ( ID - '.$id.').';
		\DB::table('tb_notification')->insert($notification);

		
		return redirect()->route(config('quickadmin.route').'.tb_courses.index');
	}
	public function course_reject(Request $request)
	{
		$tb_courses = Tb_Courses::findOrFail($request->id);
		$data=array(
			'status'=>'Reject',
			);
		$tb_courses->update($data);

		\DB::table('tb_course_reject')->insert(array('c_id'=>$request->id,'reason'=>$request->reason));

		$notification['user_id']=$tb_courses->teacher_id;
		$notification['n_role']='1';
		$notification['n_user_id']='1';
		$notification['n_uname']='Admin';
		$notification['notification']='Admin has rejected your course ( ID - '.$id.').';
		\DB::table('tb_notification')->insert($notification);
		
		echo json_encode('');
	}
	public function checkkey(Request $request){
		$key=$request->keyval;
		$cnt=0;
		$course = Tb_Courses::where('title',$request->keyval)->orWhere('summary',$request->keyval)->get();
		if(count($course)==0){
			$question = Question::where('question',$request->keyval)->get();
			if(count($question)==0){
				$answer = Answer::where('answer',$request->keyval)->get();
				if(count($answer)==0){
					$lession = Lession::where('name',$request->keyval)->orWhere('description',$request->keyval)->get();
					$cnt=count($lession);

				}else{
					$cnt=count($answer);
				}				
			}else{
				$cnt=count($question);
			}
		}else{
			$cnt=count($course);
		}
		echo json_encode($cnt);
	}
	public function checkkey_edit(Request $request){
		$key=$request->keyval;
		$cid=$request->cid;
		$cnt=0;
		$course = Tb_Courses::Where('summary',$request->keyval)->where('id','!=',$cid)->get();
		$course1 = Tb_Courses::where('title',$request->keyval)->where('id','!=',$cid)->get();
		if((count($course)+count($course1))==0){
			$question = Question::where('question',$request->keyval)->get();
			if(count($question)==0){
				$answer = Answer::where('answer',$request->keyval)->get();
				if(count($answer)==0){
					$lession = Lession::where('name',$request->keyval)->orWhere('description',$request->keyval)->get();
					$cnt=count($lession);

				}else{
					$cnt=count($answer);
				}
			}else{
				$cnt=count($question);
			}
		}else{
			$cnt=(count($course)+count($course1));
		}
		echo json_encode($cnt);
	}
	public function acheckkey_edit(Request $request){
		$key=$request->keyval;
		$cid=$request->cid;
		$type=$request->type;
		$cnt=0;
		if($type=='que'){
			$course = Tb_Courses::where('title',$request->keyval)->orWhere('summary',$request->keyval)->get();
		if(count($course)==0){
			$question = Question::where('question',$request->keyval)->where('id','!=',$cid)->get();
			if(count($question)==0){
				$answer = Answer::where('answer',$request->keyval)->get();
				if(count($answer)==0){
					$lession = Lession::where('name',$request->keyval)->orWhere('description',$request->keyval)->get();
					$cnt=count($lession);

				}else{
					$cnt=count($answer);
				}
			}else{
				$cnt=count($question);
			}
		}else{
			$cnt=count($course);
		}

		}else{
			$course = Tb_Courses::where('title',$request->keyval)->orWhere('summary',$request->keyval)->get();
		if(count($course)==0){
			$question = Question::where('question',$request->keyval)->get();
			if(count($question)==0){
				$answer = Answer::where('answer',$request->keyval)->where('id','!=',$cid)->get();
				if(count($answer)==0){
					$lession = Lession::where('name',$request->keyval)->orWhere('description',$request->keyval)->get();
					$cnt=count($lession);

				}else{
					$cnt=count($answer);
				}
			}else{
				$cnt=count($question);
			}
		}else{
			$cnt=count($course);
		}

		}
		
		echo json_encode($cnt);
	}
	public function lcheckkey_edit(Request $request){
		$key=$request->keyval;
		$lid=$request->lid;
		$cnt=0;
			$course = Tb_Courses::where('title',$request->keyval)->orWhere('summary',$request->keyval)->get();
		if(count($course)==0){
			$question = Question::where('question',$request->keyval)->get();
			if(count($question)==0){
				$answer = Answer::where('answer',$request->keyval)->get();
				if(count($answer)==0){
					$lession = Lession::where('name',$request->keyval)->orWhere('description',$request->keyval)->where('id','!=',$lid)->get();
					$cnt=count($lession);

				}else{
					$cnt=count($answer);
				}
			}else{
				$cnt=count($question);
			}
		}else{
			$cnt=count($course);
		}
		
		echo json_encode($cnt);
	}
}
