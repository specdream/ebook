<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Tb_Professional;
use App\Http\Requests\CreateTb_ProfessionalRequest;
use App\Http\Requests\UpdateTb_ProfessionalRequest;
use Illuminate\Http\Request;



class Tb_ProfessionalController extends Controller {

	/**
	 * Display a listing of tb_professional
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $tb_professional = Tb_Professional::all();

		return view('admin.tb_professional.index', compact('tb_professional'));
	}

	/**
	 * Show the form for creating a new tb_professional
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
	    return view('admin.tb_professional.create');
	}

	/**
	 * Store a newly created tb_professional in storage.
	 *
     * @param CreateTb_ProfessionalRequest|Request $request
	 */
	public function store(CreateTb_ProfessionalRequest $request)
	{
	    
		Tb_Professional::create($request->all());

		return redirect()->route(config('quickadmin.route').'.tb_professional.index');
	}

	/**
	 * Show the form for editing the specified tb_professional.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$tb_professional = Tb_Professional::find($id);
	    
	    
		return view('admin.tb_professional.edit', compact('tb_professional'));
	}

	/**
	 * Update the specified tb_professional in storage.
     * @param UpdateTb_ProfessionalRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateTb_ProfessionalRequest $request)
	{
		$tb_professional = Tb_Professional::findOrFail($id);

        

		$tb_professional->update($request->all());

		return redirect()->route(config('quickadmin.route').'.tb_professional.index');
	}

	/**
	 * Remove the specified tb_professional from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Tb_Professional::destroy($id);

		return redirect()->route(config('quickadmin.route').'.tb_professional.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Tb_Professional::destroy($toDelete);
        } else {
            Tb_Professional::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.tb_professional.index');
    }

}
