<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Company;
use App\Http\Requests\Createsdream_CompanyRequest;
use App\Http\Requests\Updatesdream_CompanyRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class CompanyController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
		
		$companies = Company::where('role_id','=','3')->get();

        return view('admin.company.index', compact('companies'));
	}

	/**
	 * Show the form for creating a new sdream_countries
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
	    return view('admin.company.create');
	}

	/**
	 * Remove the specified sdream_states from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Company::destroy($id);

		return redirect()->route(config('quickadmin.route').'.company.index');
	}

	public function edit($id)
	{
		$company = Company::find($id);  
	    
		return view('admin.company.edit', compact('company'));
	}

	/**
	 * Store a newly created sdream_countries in storage.
	 *
     * @param Createsdream_CountriesRequest|Request $request
	 */
	public function store(Createsdream_CompanyRequest $request)
	{
		$request = $this->saveFiles($request);		
	    $request['password'] = Hash::make($request['password']);
	    $request['role_id'] = '3';
		$request['username'] = $request->name;
		// print_r($request);die();
		Company::create($request->all());

		return redirect()->route(config('quickadmin.route').'.company.index');
	}
	/**
	 * Update the specified sdream_countries in storage.
     * @param Updatesdream_CountriesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, Updatesdream_CompanyRequest $request)
	{
		$company = Company::findOrFail($id); 
		$request = $this->saveFiles($request);		
	    $request['password'] = Hash::make($request['password']);
	    $request['role_id'] = '3';
		$request['username'] = $request->name;
		$company->update($request->all());

		return redirect()->route(config('quickadmin.route').'.company.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Company::destroy($toDelete);
        } else {
            Company::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.company.index');
    }

}