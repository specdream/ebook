<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Schema , DateTime;
use App\User;
use App\Book;
use App\Book_file;
use File;
class AuthorBooksController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	if(\Auth::user()){

    	$user_id = \Auth::user()->id;
		$book = \DB::table('eb_book')->where('user_id',$user_id)->get();
		return view('admin.authorbookcreate.index',compact("book"));

		}else{

		\Session::flash('message', 'You are logged out'); 
		\Session::flash('alert-class', 'alert-success');
		
		return redirect('');

		}
	}

	public function create(Request $request){

		if(\Auth::user()){

		$defaulCurrency = \Helper::userCurrency('code');	
		//print_r($defaulCurrency);exit;
		$currency = \DB::table('currency')->where('status','1')->get();			

		return view('admin.authorbooks.create',compact('currency','defaulCurrency'));

		}else{

		\Session::flash('message', 'You are logged out'); 
		\Session::flash('alert-class', 'alert-success');
		
		return redirect('');

		}
	}

	public function store(Request $request){
		//echo "string";exit;
	    if(\Auth::user()){

	    $dt = new DateTime;
	    $cDate = $dt->format('Y-m-d h:m:s');
	    $InsData['user_id']=\Auth::user()->id;
	    $InsData['categorie']=$request->Categorie;
	    $InsData['sub_categorie']=$request->sub_categorie;
	    $InsData['title']=$request->title;
	    $InsData['summary']=$request->summary;
	    $InsData['price_type']=$request->price_type;
	    $InsData['price']=$request->price;
	    $InsData['currency']=$request->currency;
	    $InsData['language']=$request->bLanguage;
	    $InsData['isbn']=$request->ISBN;
	    $InsData['created_date']=$cDate;
	    $InsData['published_date']=$cDate;
	    $InsData['status']='1';
		//print_r($InsData);exit;
        $insBook = \DB::table('eb_book')->insertGetID($InsData);
		if($request->hasFile('files'))
		{
		$request = $this->bookFiles($request,$insBook);
		}

		
		$notification['user_id']='1';
		$User1=User::find(\Auth::user()->id);
		$notification['n_role']=\Auth::user()->role_id;
		$notification['n_user_id']=\Auth::user()->id;
		$notification['n_uname']=$User1->name;
		$notification['notification']='Your book has been created, Your book under admin approval';

		\DB::table('tb_notification')->insert($notification);

		$notification['user_id']=\Auth::user()->id;
		$notification['n_role']='1';
		$notification['n_user_id']='1';
		$notification['n_uname']='Admin';
		$notification['notification']=$User1->name.' created new book';


		\DB::table('tb_notification')->insert($notification);

		\Session::flash('message', 'Record created successfully'); 
		\Session::flash('alert-class', 'alert-success');
		
		return redirect('admin/authorbooks');

		}else{

		\Session::flash('message', 'You are logged out'); 
		\Session::flash('alert-class', 'alert-success');
		
		return redirect('');

		}
	}

	public function edit(Request $request,$id){
		//echo "string";exit;
		if(\Auth::user()){

		if(isset($request->submit)){

			$dt = new DateTime;
		    $cDate = $dt->format('Y-m-d h:m:s');
		    $InsData['user_id']=\Auth::user()->id;
		    $InsData['categorie']=$request->Categorie;
		    $InsData['sub_categorie']=$request->sub_categorie;
		    $InsData['title']=$request->title;
		    $InsData['summary']=$request->summary;
		    $InsData['price_type']=$request->price_type;
		    $InsData['price']=$request->price;
		    $InsData['language']=$request->bLanguage;
		    $InsData['isbn']=$request->ISBN;
		    $InsData['status']=$request->status;
			//print_r($InsData);exit;
	        $insBook = \DB::table('eb_book')->where('id',$id)->update($InsData);

	        \Session::flash('message', 'Record deleted successfully'); 
			\Session::flash('alert-class', 'alert-success');
			
			return redirect('admin/authorbooks');

		}else{
			$book = Book::where('id',$id)->first();
			$bookFile = Book_file::where('book_id',$id)->get();
			return view('admin.authorbooks.edit',compact("book","bookFile"));
		}
		}else{

		\Session::flash('message', 'You are logged out'); 
		\Session::flash('alert-class', 'alert-success');
		
		return redirect('');

		}
	}

	public function delete($id){

		if(\Auth::user()){
		$checkPurchase = \DB::table('eb_book_purchase')->where('book_id',$id)->get();	

		if(count($checkPurchase) > 0){
			\Session::flash('message', 'Sorry, You can not able to delete this book becuase some one purchased this book'); 
			\Session::flash('alert-class', 'alert-warning');
			return redirect('admin/authorbooks');
		}
		
		File::deleteDirectory(public_path('uploads/books').'/'.$id);
		File::deleteDirectory(public_path('uploads/books_banner').'/'.$id);
		Book::where('id',$id)->delete();
		Book_file::where('book_id',$id)->delete();
		
		\Session::flash('message', 'Record deleted successfully'); 
		\Session::flash('alert-class', 'alert-success');
		return redirect('admin/authorbooks');
		}else{

		\Session::flash('message', 'You are logged out'); 
		\Session::flash('alert-class', 'alert-success');
		
		return redirect('');

		}

	}

	public function uploadBanner(Request $request){

		if(\Auth::user()){

		$book_id = $request->book_id;

		$book = Book::where('id',$book_id)->first();
		//print_r($bookFile);exit;
				
		if($request->hasFile('banner'))
		{ 
			//echo "string";exit;
		File::delete(public_path('uploads/books_banner').'/'.$book_id.'/'.$book->banner);	
		$request = $this->bookFiles($request,$book_id);
		} 

		$book = Book::where('id',$book_id)->first();
		echo url('public/uploads/books_banner').'/'.$book->id.'/'.$book->banner;
		}else{

		\Session::flash('message', 'You are logged out'); 
		\Session::flash('alert-class', 'alert-success');
		
		return redirect('');

		}
	}

	public function fileUpload(Request $request){

		if(\Auth::user()){

		$insBook = $request->book_id;

		if($request->hasFile('files'))
		{ 
		$request = $this->bookFiles($request,$insBook);
		} 

		$book = Book::where('id',$insBook)->first();
		$bookFile = Book_file::where('book_id',$insBook)->get();
		//print_r($bookFile);exit;
		$msi = '0'; $mri = '0';
		$echoData = '';
             for($mi=0;$mi<count($bookFile);$mi++){
                if($mi%4 == 0){
                    $mri = count($bookFile)-$mi;
                    if(4 > $mri){
                        if($mi != '0')
                        $msi = $mri+3;   
                        else
                        $msi = $mri-1;       
                    }else{
                    $msi = $mi+3;
                    }
                   
                    $echoData .= "<input type='hidden' id='totalFile' value='".count($bookFile)."'><div class='column'>";
                } 
                   if($bookFile[$mi]->type == '1'){ 
                $echoData .= "
                <div class='pre_image_div col-xs-12 col-sm-3 col-md-3'>
                <a href='javascript:void(0)' onclick='deletefile(".$bookFile[$mi]->id.",1)'><i class='fa fa-trash' aria-hidden='true'></i></a>
                  <img src='".url('public/uploads/books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."'>
                  <script type='text/javascript'>
                    $('#inputFile').attr('accept','image/*');
                    $('.up_btn').html('Add Image');
                  </script>
                  </div>";
                  }elseif($bookFile[$mi]->type == '2'){
                  $echoData .= "<div class='pre_image_div col-xs-12 col-sm-3 col-md-3'>
                  <a href='javascript:void(0)' onclick='deletefile(".$bookFile[$mi]->id.",1)'><i class='fa fa-trash' aria-hidden='true'></i></a>
                  <video src='".url('public/uploads/books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."'></video>
                  <script type='text/javascript'>
                      $('#inputFile').attr('accept','video/*');
                      $('.up_btn').html('Add Video');
                  </script>
                  </div>";
                  }elseif($bookFile[$mi]->type == '3'){
                  $echoData .= "<div class='pre_image_div col-xs-12 col-sm-3 col-md-3'>
                  <a href='javascript:void(0)' onclick='deletefile(".$bookFile[$mi]->id.",1)'><i class='fa fa-trash trash-pdf' aria-hidden='true'></i></a>
                  <object data='".url('public/uploads/books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."' type='application/pdf' width='100%' height='100%'>
                  <p>Alternative text - include a link <a href='".url('public/uploads/books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."'>to the PDF!</a></p>
                  </object>
                  <script type='text/javascript'>
                      $('#inputFile').attr('accept','application/pdf');
                      $('.up_btn').html('Add PDF');
                  </script>
                  </div>";
                  }elseif($bookFile[$mi]->type == '4'){
                  $echoData .= "<div class='pre_image_div col-xs-12 col-sm-3 col-md-3'>
                  <a href='javascript:void(0)' onclick='deletefile(".$bookFile[$mi]->id.",1)'><i class='fa fa-trash trash-pdf' aria-hidden='true'></i></a>
                  <object data='".url('public/uploads/books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."' type='application/vnd.ms-powerpoint' width='100%' height='100%'>
                  <p>Alternative text - include a link <a href='".url('public/uploads/books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."'>to the PDF!</a></p>
                  </object>
                  <script type='text/javascript'>
                      $('#inputFile').attr('accept','application/vnd.ms-powerpoint');
                      $('.up_btn').html('Add PowerPoint');
                  </script>
                  </div>";
              }
                 if($mi == $msi){
                    $echoData .= "</div>";    
                }
            }
		echo $echoData;exit;
		}else{

		\Session::flash('message', 'You are logged out'); 
		\Session::flash('alert-class', 'alert-success');
		
		return redirect('');

		}

	}

	public function deleteFile($id,$book_id,$type){

		if(\Auth::user()){
			
		$bookFile = Book_file::where('id',$id)->first();
		//print_r($bookFile);exit;
		File::delete(public_path('uploads/books').'/'.$book_id.'/'.$bookFile->file);
		Book_file::where('id',$id)->delete();

		$book = Book::where('id',$book_id)->first();
		$bookFile = Book_file::where('book_id',$book_id)->get();
		$msi = '0'; $mri = '0';
		$echoData = '';
             for($mi=0;$mi<count($bookFile);$mi++){
                if($mi%4 == 0){
                    $mri = count($bookFile)-$mi;
                    if(4 > $mri){
                        if($mi != '0')
                        $msi = $mri+3;   
                        else
                        $msi = $mri-1;       
                    }else{
                    $msi = $mi+3;
                    }
                   
                    $echoData .= "<input type='hidden' id='totalFile' value='".count($bookFile)."'><div class='column'>";
                } 
                   if($bookFile[$mi]->type == '1'){ 
                $echoData .= "<div class='pre_image_div col-xs-12 col-sm-3 col-md-3'>
                <a href='javascript:void(0)' onclick='deletefile(".$bookFile[$mi]->id.",1)'><i class='fa fa-trash' aria-hidden='true'></i></a>
                  <img src='".url('public/uploads/books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."'>
                  <script type='text/javascript'>
                    $('#inputFile').attr('accept','image/*');
                    $('.up_btn').html('Add Image');
                  </script>
                  </div>";
                  }elseif($bookFile[$mi]->type == '2'){
                  $echoData .= "<div class='pre_image_div col-xs-12 col-sm-3 col-md-3'>
                  <a href='javascript:void(0)' onclick='deletefile(".$bookFile[$mi]->id.",1)'><i class='fa fa-trash' aria-hidden='true'></i></a>
                  <video src='".url('public/uploads/books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."'></video>
                  <script type='text/javascript'>
                      $('#inputFile').attr('accept','video/*');
                      $('.up_btn').html('Add Video');
                  </script>
                  </div>";
                  }elseif($bookFile[$mi]->type == '3'){
                  $echoData .= "<div class='pre_image_div col-xs-12 col-sm-3 col-md-3'>
                  <a href='javascript:void(0)' onclick='deletefile(".$bookFile[$mi]->id.",1)'><i class='fa fa-trash trash-pdf' aria-hidden='true'></i></a>
                  <object data='".url('public/uploads/books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."' type='application/pdf' width='100%' height='100%'>
                  <p>Alternative text - include a link <a href='".url('public/uploads/books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."'>to the PDF!</a></p>
                  </object>
                  <script type='text/javascript'>
                      $('#inputFile').attr('accept','application/pdf');
                      $('.up_btn').html('Add PDF');
                  </script>
                  </div>";
                  }elseif($bookFile[$mi]->type == '4'){
                  $echoData .= "<div class='pre_image_div col-xs-12 col-sm-3 col-md-3'>
                  <a href='javascript:void(0)' onclick='deletefile(".$bookFile[$mi]->id.",1)'><i class='fa fa-trash trash-pdf' aria-hidden='true'></i></a>
                  <object data='".url('public/uploads/books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."' type='application/vnd.ms-powerpoint' width='100%' height='100%'>
                  <p>Alternative text - include a link <a href='".url('public/uploads/books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file."'>to the PDF!</a></p>
                  </object>
                  <script type='text/javascript'>
                      $('#inputFile').attr('accept','application/vnd.ms-powerpoint');
                      $('.up_btn').html('Add PowerPoint');
                  </script>
                  </div>";
              }
                 if($mi == $msi){
                    $echoData .= "</div>";    
                }
            }
		echo $echoData;exit;
		}else{

		\Session::flash('message', 'You are logged out'); 
		\Session::flash('alert-class', 'alert-success');
		
		return redirect('');

		}
	}

}