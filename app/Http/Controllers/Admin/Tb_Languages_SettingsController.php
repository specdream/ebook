<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Tb_Languages_Settings;
use App\Http\Requests\CreateTb_Languages_SettingsRequest;
use App\Http\Requests\UpdateTb_Languages_SettingsRequest;
use Illuminate\Http\Request;

use App\Tb_Languages;


class Tb_Languages_SettingsController extends Controller {

	/**
	 * Display a listing of tb_languages_settings
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {	
    	if(\Auth::user()->id=='1'){
    		$tb_languages_settings = Tb_Languages_Settings::with("tb_languages")->paginate(10);	
    	}else{
    		$tb_languages_settings = Tb_Languages_Settings::where('added_by',\Auth::user()->id)->with("tb_languages")->paginate(10);
    	}
    	return view('admin.tb_languages_settings.index', compact('tb_languages_settings'));
	}

	/**
	 * Show the form for creating a new tb_languages_settings
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    $tb_languages = Tb_Languages::pluck("name", "id")->prepend('Please select', '');

	    
	    return view('admin.tb_languages_settings.create', compact("tb_languages"));
	}

	/**
	 * Store a newly created tb_languages_settings in storage.
	 *
     * @param CreateTb_Languages_SettingsRequest|Request $request
	 */
	public function store(CreateTb_Languages_SettingsRequest $request)
	{
	    $request['added_by']=\Auth::user()->id;
		Tb_Languages_Settings::create($request->all());

		return redirect()->route(config('quickadmin.route').'.tb_languages_settings.index');
	}

	/**
	 * Show the form for editing the specified tb_languages_settings.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$tb_languages_settings = Tb_Languages_Settings::find($id);
	    $tb_languages = Tb_Languages::pluck("name", "id")->prepend('Please select', '');

	    
		return view('admin.tb_languages_settings.edit', compact('tb_languages_settings', "tb_languages"));
	}

	/**
	 * Update the specified tb_languages_settings in storage.
     * @param UpdateTb_Languages_SettingsRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateTb_Languages_SettingsRequest $request)
	{
		$tb_languages_settings = Tb_Languages_Settings::findOrFail($id);

        

		$tb_languages_settings->update($request->all());

		return redirect()->route(config('quickadmin.route').'.tb_languages_settings.index');
	}

	/**
	 * Remove the specified tb_languages_settings from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Tb_Languages_Settings::destroy($id);

		return redirect()->route(config('quickadmin.route').'.tb_languages_settings.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Tb_Languages_Settings::destroy($toDelete);
        } else {
            Tb_Languages_Settings::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.tb_languages_settings.index');
    }

}
