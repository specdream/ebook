<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Learners;
use App\Tb_Professional;
use App\Http\Requests\Createsdream_TeacherRequest;
use App\Http\Requests\Updatesdream_TeacherRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LearnersController extends Controller {


	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
		
		$learners = Learners::where('role_id','=','2')->get();

        return view('admin.learners.index', compact('learners'));
	}

	/**
	 * Show the form for creating a new sdream_countries
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    $tb_professional=Tb_Professional::pluck("name", "id")->prepend('Please select', '');
	    
	    return view('admin.learners.create', compact('tb_professional'));
	}

	/**
	 * Remove the specified sdream_states from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Learners::destroy($id);

		return redirect()->route(config('quickadmin.route').'.learners.index');
	}

	public function edit($id)
	{
		$learner = Learners::findOrFail($id);  
	    $tb_professional=Tb_Professional::pluck("name", "id")->prepend('Please select', '');
		return view('admin.learners.edit', compact('learner','tb_professional'));
	}

	/**
	 * Store a newly created sdream_countries in storage.
	 *
     * @param Createsdream_CountriesRequest|Request $request
	 */
	public function store(Createsdream_TeacherRequest $request)
	{
		$request = $this->saveFiles($request);		
	    $request['password'] = Hash::make($request['password']);
	    $request['role_id'] = '2';
		$request['username'] = $request->name;
		// print_r($request);die();
		Learners::create($request->all());

		return redirect()->route(config('quickadmin.route').'.learners.index');
	}
	/**
	 * Update the specified sdream_countries in storage.
     * @param Updatesdream_CountriesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, Updatesdream_TeacherRequest $request)
	{
		$learner = Learners::findOrFail($id); 
		$request = $this->saveFiles($request);		
	    $request['password'] = Hash::make($request['password']);
	    $request['role_id'] = '2';
		$request['username'] = $request->name;
		$learner->update($request->all());

		return redirect()->route(config('quickadmin.route').'.learners.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Learners::destroy($toDelete);
        } else {
            Learners::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.learners.index');
    }



}