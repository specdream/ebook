<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\BookSubCategories;
use Illuminate\Http\Request;

class AuthoBookSubCatgController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	$getCatg = BookSubCategories::get();
		return view('admin.authobooksubcatg.index',compact('getCatg'));
	}

	public function create(){

		return view('admin.authobooksubcatg.create');
	}

	public function store(Request $request){

		$catg = $request->categorie;
		$main = $request->main;
		//print_r($catg);exit;
		for($i=0;$i<count($catg);$i++){
		
		if($catg[$i] != ''){
			$data['cat_id']	=	$main[$i];
			$data['name']	=	$catg[$i];
			$data['status']	=	'1';	
			BookSubCategories::insert($data);
		}

		}
		
		

		\Session::flash('message', 'Record inserted successfully'); 
		\Session::flash('alert-class', 'alert-success');
		
		return redirect('admin/authobooksubcatg');

	}
	public function edit(Request $request,$id){
		if(isset($request->submit)){
			$data['cat_id']	=	$request->main;
			$data['name']		=	$request->name;
			$data['status']	=	$request->status;

			\DB::table('tb_book_sub_categorie')->where('id',$id)->update($data);

			\Session::flash('message', 'Record updated successfully'); 
			\Session::flash('alert-class', 'alert-success');

			return redirect('admin/authobooksubcatg');

		}else{
			$catg = BookSubCategories::where('id',$id)->first();

			return view('admin.authobooksubcatg.edit',compact('catg'));
		}
	}

	public function delete($id){

		BookSubCategories::where('id',$id)->delete();

		\Session::flash('message', 'Record deleted successfully'); 
		\Session::flash('alert-class', 'alert-success');
		return redirect('admin/authobooksubcatg');
	}

	public function subCatg(Request $request){

		$subCat = BookSubCategories::where('cat_id',$request->id)->get();
		echo json_encode($subCat);exit;
	}


}