<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\sdream_Countries;
use App\Http\Requests\Createsdream_CountriesRequest;
use App\Http\Requests\Updatesdream_CountriesRequest;
use Illuminate\Http\Request;



class sdream_CountriesController extends Controller {

	/**
	 * Display a listing of sdream_countries
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */

	public function index(Request $request)
    {
        $sdream_countries = sdream_Countries::all();

		return view('admin.sdream_countries.index', compact('sdream_countries'));
	}

	/**
	 * Show the form for creating a new sdream_countries
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
	    return view('admin.sdream_countries.create');
	}

	/**
	 * Store a newly created sdream_countries in storage.
	 *
     * @param Createsdream_CountriesRequest|Request $request
	 */
	public function store(Createsdream_CountriesRequest $request)
	{
	    
		sdream_Countries::create($request->all());

		return redirect()->route(config('quickadmin.route').'.sdream_countries.index');
	}

	/**
	 * Show the form for editing the specified sdream_countries.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$sdream_countries = sdream_Countries::find($id);
	    
	    
		return view('admin.sdream_countries.edit', compact('sdream_countries'));
	}

	/**
	 * Update the specified sdream_countries in storage.
     * @param Updatesdream_CountriesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, Updatesdream_CountriesRequest $request)
	{
		$sdream_countries = sdream_Countries::findOrFail($id);

        

		$sdream_countries->update($request->all());

		return redirect()->route(config('quickadmin.route').'.sdream_countries.index');
	}

	/**
	 * Remove the specified sdream_countries from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		die();
		sdream_Countries::destroy($id);

		return redirect()->route(config('quickadmin.route').'.sdream_countries.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            sdream_Countries::destroy($toDelete);
        } else {
            sdream_Countries::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.sdream_countries.index');
    }

}
