<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class accountController extends Controller
{
    /**
     * Show a list of users
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    public function inbox(Request $request){
        if(\Auth::user()){
            $paginate = 10;
            $page = $request->get('page', 1);
            $user = \Auth::user();
            $where = 'WHERE n_user_id = '.$user->id;
            $query = "SELECT * FROM `tb_notification`" .$where.' ORDER BY id DESC';
            $result = \DB::select($query);
            $totalCount = count($result);
            $slice = array_slice($result, $paginate * ($page - 1), $paginate);
            $inbox = new Paginator($slice ,count($result), $paginate);
            return view('account.inbox',compact('inbox'));

        }else{
            \Session::flash('message', 'You are logged out'); 
            \Session::flash('alert-class', 'alert-error');
            return redirect('login');
        }
    }

    public function purchase_history(){
        if(\Auth::user()){
            $user_id = \Auth::user()->id;
            $history = \DB::table('eb_book_purchase')->where('user_id',$user_id)->get();
            $uniq_id = array();
            $historyData = array();
            if(count($history) > 0){
                foreach ($history as $value) {
                    array_push($uniq_id, $value->uniq_id);
                }
            }
            if(count($uniq_id) > 0){
                $pD = '0';
                //$uniq_id = array_unique($uniq_id);
                //print_r($uniq_id);exit;
                foreach ($uniq_id as $value) {
                    $pHistory = \DB::table('eb_book_purchase')->where('uniq_id',$value)->first();
                    $title = \Helper::bookDetail($pHistory->book_id)->title;
                    $author_id = \Helper::bookDetail($pHistory->book_id)->user_id;
                    $author = \Helper::getUser($author_id,'name');
                    $historyData[$pD]['uniq_id'] = $value;
                    $historyData[$pD]['title'] = $title;
                    $historyData[$pD]['author'] = $author;
                    $pD++;
                }
            }
            return view('purchase-history',compact('historyData'));
        }else{
            \Session::flash('message', 'You are logged out'); 
            \Session::flash('alert-class', 'alert-error');
            return redirect('login');
        }
    }

    public function college_purchase_history(){
        if(\Auth::user()){
            $user_id = \Auth::user()->id;
            $history = \DB::table('eb_college_book_purchase')->where('user_id',$user_id)->get();
            $uniq_id = array();
            $historyData = array();
            if(count($history) > 0){
                foreach ($history as $value) {
                    array_push($uniq_id, $value->uniq_id);
                }
            }
            if(count($uniq_id) > 0){
                $pD = '0';
                //$uniq_id = array_unique($uniq_id);
                //print_r($uniq_id);exit;
                foreach ($uniq_id as $value) {
                    $pHistory = \DB::table('eb_college_book_purchase')->where('uniq_id',$value)->first();
                    $title = \Helper::collegeBookDetail($pHistory->book_id)->title;
                    $author_id = \Helper::collegeBookDetail($pHistory->book_id)->user_id;
                    $author = \Helper::getUser($author_id,'name');
                    $historyData[$pD]['uniq_id'] = $value;
                    $historyData[$pD]['title'] = $title;
                    $historyData[$pD]['author'] = $author;
                    $pD++;
                }
            }
            return view('college-purchase-history',compact('historyData'));
        }else{
            \Session::flash('message', 'You are logged out'); 
            \Session::flash('alert-class', 'alert-error');
            return redirect('login');
        }
    }

    public function invoicePopup($uniq_id){
        //print_r($uniq_id);
        $user_id = \Auth::user()->id;
            $address = \DB::table('billingAddress')->where('user_id',$user_id)->first();
            $country = \DB::table('sdream_countries')->where('id',$address->country)->first();
            $data['address'] = $address;
            $data['country'] = $country;
            $data['uniq_id'] = $uniq_id;
            echo view('account.invoice')->with($data);
    }

    public function collegeInvoicePopup($uniq_id){
        //print_r($uniq_id);
        $user_id = \Auth::user()->id;
            $address = \DB::table('billingAddress')->where('user_id',$user_id)->first();
            $country = \DB::table('sdream_countries')->where('id',$address->country)->first();
            $data['address'] = $address;
            $data['country'] = $country;
            $data['uniq_id'] = $uniq_id;
            echo view('account.collegeInvoice')->with($data);
    }

    public function invoicePopupAdmin($uniq_id){
        //print_r($uniq_id);
        $user_detail = \DB::table('eb_book_purchase')->where('uniq_id',$uniq_id)->first();
        $user_id = $user_detail->user_id;
            $address = \DB::table('billingAddress')->where('user_id',$user_id)->first();
            $country = \DB::table('sdream_countries')->where('id',$address->country)->first();
            $data['address'] = $address;
            $data['country'] = $country;
            $data['uniq_id'] = $uniq_id;
            echo view('account.invoice')->with($data);
    }
    
    public function collegeInvoicePopupAdmin($uniq_id){
        //print_r($uniq_id);
        $user_detail = \DB::table('eb_college_book_purchase')->where('uniq_id',$uniq_id)->first();
        $user_id = $user_detail->user_id;
            $address = \DB::table('billingAddress')->where('user_id',$user_id)->first();
            $country = \DB::table('sdream_countries')->where('id',$address->country)->first();
            $data['address'] = $address;
            $data['country'] = $country;
            $data['uniq_id'] = $uniq_id;
            echo view('account.collegeInvoice')->with($data);
    }

    public function myaccount(){
        if(\Auth::user()){
            $user = \Auth::user();
            return view('myaccount',compact('user'));
        }else{
            return redirect('login');
        }
    }

    public function profile(Request $request){
        $user = \Auth::user();
        if(\Auth::user()){
        if($request->place == '1'){

            $dob = $request->dob;
            $email = $request->email;
            $name = $request->name;
            $radio = $request->radio;

            if($dob != ''){
                $data['dob'] = $dob;
            }
            if($email != ''){
                $data['email'] = $email;   
            }
            if($name != ''){
                $data['username'] = $name;    
            }   
            if($radio != ''){
                $data['gender'] = $radio;
            }

            \DB::table('users')->where('id',$user->id)->update($data);
            echo "success";exit;

        }elseif($request->place == '2'){
            if (Hash::check($request->old_password, \Auth::user()->password)) {
                $user = User::find(\Auth::user()->id);
                $user->password = bcrypt($request->new_password);
                $user->save();
                echo "success";exit;   
            }else{
                echo "WrongPwd";exit;
            }
            $oldpassword = $request->oldpassword;
        }elseif($request->place == '3'){
            $user = User::find(\Auth::user()->id);
            $user->active = 0;
            $user->save();
            \Auth::logout();
            return redirect(''); 
        }
        }else{
            echo "noLogin";
        }

    }

    public function mybook(Request $request){
        if(\Auth::user()){
            /*$where = 'WHERE AND 1 ';
            if(isset($request->search)){
                if($request->search != ''){
                    $where = 'WHERE AND 1 ';
                }
            }*/
            $paginate = 20;
            $page = $request->get('page', 1);
            $book = array();
            $user = \Auth::user();
            $getPurch = \DB::table('eb_book_purchase')->where('user_id',$user->id)->get();
            $b = '0';
            foreach ($getPurch as $value) {
                $getBook = \DB::table('eb_book')->where('id',$value->book_id)->first();
                if($getBook){
                $book[$b]['id'] = $getBook->id;
                $book[$b]['title'] = $getBook->title;
                $book[$b]['summary'] = $getBook->summary;
                $book[$b]['banner'] = $getBook->banner;
                $book[$b]['price'] = $value->price;
                $book[$b]['author'] = \Helper::getUser($getBook->user_id,'name');
                $book[$b]['categorie'] = \Helper::categoryName($getBook->categorie);
                $b++;
                }
            }
            $totalCount = count($book);
            $slice = array_slice($book, $paginate * ($page - 1), $paginate);
            $books = new Paginator($slice ,count($book), $paginate);
            //print_r($book);exit;
            return view('mybook',compact('books'));
        }else{
            \Session::flash('message', 'You are logged out'); 
            \Session::flash('alert-class', 'alert-error');
            return redirect('login');
        }
    }

    public function studentMybook(Request $request){
        if(\Auth::user()){
            /*$where = 'WHERE AND 1 ';
            if(isset($request->search)){
                if($request->search != ''){
                    $where = 'WHERE AND 1 ';
                }
            }*/
            $paginate = 20;
            $page = $request->get('page', 1);
            $book = array();
            $user = \Auth::user();
            $getPurch = \DB::table('eb_college_book_purchase')->where('user_id',$user->id)->get();
            $b = '0';
            foreach ($getPurch as $value) {
                $getBook = \DB::table('eb_college_book')->where('id',$value->book_id)->first();
                if($getBook){
                $book[$b]['id'] = $getBook->id;
                $book[$b]['title'] = $getBook->title;
                $book[$b]['summary'] = $getBook->summary;
                $book[$b]['banner'] = $getBook->banner;
                $book[$b]['price'] = $value->price;
                $book[$b]['author'] = \Helper::getUser($getBook->user_id,'name');
                
                $b++;
                }
            }
            $totalCount = count($book);
            $slice = array_slice($book, $paginate * ($page - 1), $paginate);
            $books = new Paginator($slice ,count($book), $paginate);
            //print_r($book);exit;
            return view('studentMybook',compact('books'));
        }else{
            \Session::flash('message', 'You are logged out'); 
            \Session::flash('alert-class', 'alert-error');
            return redirect('login');
        }
    }

    public function bookLogin(Request $request,$id){
        echo "string";exit;
    }

}