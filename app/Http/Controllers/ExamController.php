<?php

namespace App\Http\Controllers;

use App\Role;
use App\Learners;
use App\Tb_Professional;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Helper;
use App;
use App\User;
use App\Tb_Purchased_Course;
use App\Tb_Courses;
use App\Lession;
use App\Question;
use App\Tb_Categories_Type;
use App\Tb_Test_Code_Generated;
use App\Tb_Learner_Test_Store;
use Session;
use Redirect;
class ExamController extends Controller{    
    public function testPage($id){
        if(\Auth::check()){
            $Key_Generated=Tb_Test_Code_Generated::where('learner_id',\Auth::user()->id)->where('id',$id)->where('is_used','no')->first();
            if($Key_Generated){
                $lesson=Lession::where('id',$Key_Generated->lesson_id)->first();
                // $question=Question::where('lession_id',$Key_Generated->lesson_id)->orderByRaw('RAND()')->take($lesson->num_que)->get();
                $question=Question::where('lession_id',$Key_Generated->lesson_id)->take(6)->get();
                $keygen_id=$id;
                return view('learner/testPage',compact('Key_Generated','lesson','question','keygen_id'));
            }else{
                return Redirect::to('learner/writeExam')->withErrors([ 'invalid']);
            }
        }else{
            return Redirect::to('/');
        }
    }
    public function textSubmit(Request $request)
    {   
        echo "<pre>";
        $aCrt_ans=array();
        $aIs_ansCrt=array();
        $aMark=array();
        $acqus_count=0;
        $Key_Generated=Tb_Test_Code_Generated::where('learner_id',\Auth::user()->id)->where('id',$request['keygen_id'])->first();
        $request['learner_id']=$Key_Generated->learner_id;
        $request['key_code']  =$Key_Generated->key_code;
        $request['course_id'] =$Key_Generated->course_id;
        $request['lesson_id'] =$Key_Generated->lesson_id;
        $request['attempt']   =$Key_Generated->attempt;


        foreach ($request['ques_id'] as $key => $value) {
            $questions=\DB::table('tb_lession_answer')->where('qid',$value)->where('correct_answer','1')->whereNull('deleted_at')->first();
            $qus_Mark=\DB::table('tb_lession_question')->select('mark')->where('id',$value)->whereNull('deleted_at')->first();
            $aCrt_ans[$key]=$questions->id;
            if($questions->id==$request['answer'][$key]){
                $aIs_ansCrt[$key]=1;
                $aMark[$key]=$qus_Mark->mark;
                $acqus_count++;
            }else{
                $aIs_ansCrt[$key]=0;
                $aMark[$key]=0;
            }


            $aData=array(
                'learner_id'    =>  $request['learner_id'],
                'keygen_id'      =>  $request['keygen_id'],
                'key_code'      =>  $request['key_code'],
                'course_id'     =>  $request['course_id'],
                'lesson_id'     =>  $request['lesson_id'],
                'attempt'       =>  $request['attempt'],
                'question_id'   =>  $value,
                'answer_id'     =>  $request['ans_id'][$key],
                'correct_ans'   =>  $aCrt_ans[$key],
                'learner_ans'   =>  $request['answer'][$key],
                'is_correct'    =>  $aIs_ansCrt[$key],
                'mark'          =>  $aMark[$key],
                );
            Tb_Learner_Test_Store::create($aData);
        }

        $pass_percentage=(($acqus_count/$request['tot_que'])*100);

        $tb_lession=\DB::table('tb_lession')->where('id',$Key_Generated->lesson_id)->first();
        if($tb_lession->pass_percentage<=$pass_percentage){
            $is_pass='yes';
        }else{
            $is_pass='no';
        }

        \DB::table('tb_test_code_generated')->where('id',$request['keygen_id'])->update(array('is_used'=>'yes','is_pass'=>$is_pass,'percentage'=>$pass_percentage));
        // $request['correct_ans']=$aCrt_ans;
        // $request['is_correct']=$aIs_ansCrt;
        // $request['mark']=$aMark;
        // print_r($request->all());
        // print_r($aData);die();

        return Redirect::to('learner/lessonResult/'.$request['keygen_id']);

    }
    public function lessonResult($id)
    {
        // echo "string";die();
        $Key_Generated=Tb_Test_Code_Generated::where('learner_id',\Auth::user()->id)->where('id',$id)->where('is_used','yes')->first();
        $test_store=Tb_Learner_Test_Store::where('learner_id',\Auth::user()->id)->where('keygen_id',$id)->get();
        $Lession=Lession::where('id',$Key_Generated->lesson_id)->first();
        if($Key_Generated){
            return view('learner/lessonResult',compact('Key_Generated','test_store','Lession'));
        }else{
            return Redirect::to('/');
        }
    }
    public function finishedTest()
    {
        $Key_Generated=Tb_Test_Code_Generated::where('learner_id',\Auth::user()->id)->where('is_used','yes')->orderBy('id','ASC')->get();
        
        return view('learner/finishedTest', compact('Key_Generated'));
    }
}