<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\FileUploadTrait;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App;
use Session;

abstract class Controller extends BaseController
{
	
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, FileUploadTrait;
    public function __construct()
   {
      
      
      if(!isset($_COOKIE["userIp"])){
        \Helper::userIPupdate();
      }
    
      /*$ipCountryCode = \Helper::ipUserDetails('countryCode');
      $ipCurrency = \Helper::userCurrency('code');
      $ipCurrencySymbol = \Helper::userCurrency('symbol');  */
      $ipCountryCode = 'IN';
      $ipCurrency = 'INR';
      $ipCurrencySymbol = 'RS';  
      
   		//print_r($ipCountryCode);exit;
   		$ipData['ipCountryCode'] = $ipCountryCode;
   		$ipData['ipCurrency'] = $ipCurrency;
   		$ipData['ipCurrencySymbol'] = $ipCurrencySymbol;
   		\Session::put($ipData);
      if($_SERVER['HTTP_HOST'] != 'localhost'){
        echo "Invalid address";exit;
      }
   		//print_r(\Session::get('ipCurrencySymbol'));exit;
   }
// public function __construct()
//     {
//     	\App::setLocale('en');
//     	\App::translator('en');
//     $lang = (\Session::get('lang') != "" ? \Session::get('lang') : 'en');
//         echo $lang;die();
//            \App::setLocale($lang);
//            \App::translator('en');
        
//         }
}
