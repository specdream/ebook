<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Helper;
use Session;
use Redirect;
use DB;

class searchController extends Controller{   

	public function index(Request $request){

		$paginate = 20;
		$page = $request->get('page', 1);
		$where = 'WHERE status = 1 ';
		$enableCategorie = '0';
		$enableSubCategorie = '0';
		$subCategories = array();
		$language = array();

		if(isset($request->author)){
			if($request->author != ''){
				$where .=" AND user_id = '".$request->author."'";  
			}
		}
		if(isset($request->categories)){
			if($request->categories != ''){
				$where .=" AND categorie = '".$request->categories."'";  
				$subCategories = \DB::table("tb_book_sub_categorie")->where('cat_id',$request->categories)->where('status','1')->get();
				$enableCategorie = '1';
			}
		}
		if(isset($request->subCategories)){
			if($request->subCategories != ''){
				$where .=" AND sub_categorie = '".$request->subCategories."'";  
  			//$subCategories = \DB::table("tb_book_sub_categorie")->where('id',$request->subCategories)->where('status','1')->get();
				$enableCategorie = '0';
				$enableSubCategorie = '1';
			}
		}
		if(isset($request->language)){
			if($request->language != ''){
				$where .=" AND language = '".$request->language."'";  
			}
		}
		$subCatData = array();
		if(count($subCategories) > 0){
			$sI = '0';
			foreach ($subCategories as $value) {
				$SubCatquery = "SELECT * FROM `eb_book`" .$where." AND sub_categorie = '".$value->id."'";
				$SubCatresult = \DB::select($SubCatquery);
				$SubCattotalCount = count($SubCatresult);
				if($SubCattotalCount > 0){
					$subCatData[$sI]['count'] = $SubCattotalCount;
					$subCatData[$sI]['id'] = $value->id;
					$subCatData[$sI]['name'] = $value->name;
					$sI++;
				}
			}
		}
		$query = "SELECT * FROM `eb_book`" .$where;
		$result = \DB::select($query);
		$totalCount = count($result);
		$slice = array_slice($result, $paginate * ($page - 1), $paginate);
		$book = new Paginator($slice ,count($result), $paginate);

    ///Get language
		foreach ($result as $value) {
			array_push($language, $value->language);
		}
		$language = array_unique($language);
		$languageData = array();
		if(count($language) > 0){
			$lI = '0';
			foreach ($language as $value) {
				$langQuery = "SELECT * FROM `eb_book`" .$where." AND language = '".$value."'";
				$langResult = \DB::select($langQuery);
				$langTotalCount = count($langResult);
				if($langTotalCount > 0){
					$langVal = DB::table('tb_book_language')->where('id',$value)->first();			  		
					$languageData[$lI]['count'] = $langTotalCount;
					$languageData[$lI]['id'] = $value;
					$languageData[$lI]['name'] = $langVal->value;
					$lI++;
				}
			}
		}

		$getOverAll = DB::table('eb_book')->where('status','1')->get();

		$viewData['book'] = $book;
		$viewData['subCategories'] = $subCatData;
		$viewData['enableCategorie'] = $enableCategorie;
		$viewData['enableSubCategorie'] = $enableSubCategorie;
		$viewData['totalCount'] = $totalCount;
		$viewData['getOverAllCount'] = count($getOverAll);
		$viewData['language'] = $languageData;

		return view('search',with($viewData));
	}

	public function suggestion(Request $request){
		$keyword = $request->key;
		$author = DB::table('users')->where('username', 'like', '%'.$keyword.'%')->limit(6)->get();
		$echoData = '';
		$aI = '0';
		$bI = '0';
		$echoData .='
		<li class="title-name" >AUTHORS</li>';
		if(count($author) > 0){
			foreach ($author as $value) {
				$authBook = DB::table('eb_book')->where('user_id',$value->id)->get();
				if(count($authBook) > 0){
					$echoData .="<li><a href='".url('search').'?author='.$value->id."'>".$value->username."</a> </li>";
				}else{
					//$echoData .='<li> No matches found </li>';
				}
			}
		}
		$echoData .='<li class="search-all-cat">
<a href="">
	<p class="authorBar"></p>
</a>
</li>
<li class="title-name">BOOKS</li>';
$bookLike = DB::table('eb_book')->where('title', 'like', '%'.$keyword.'%')->limit(6)->get();
if(count($bookLike) > 0){
	foreach ($bookLike as $value) {
		$echoData .="<li><a href='".url('detail').'/'.$value->id."'>".$value->title."</a></li>";

	}
}else{
	//$echoData .='<li> No matches found </li>';
}
$echoData .='<li class="search-all-cat">
<a href="">
	<p class="authorBar"></p>
</a>
</li>';
echo $echoData;exit;
}

public function collegeBook(Request $request){

		$paginate = 20;
		$page = $request->get('page', 1);
		$where = 'WHERE status = 1 ';

		if(isset($request->semester)){
			if($request->semester != ''){
				$where .=" AND semester = '".$request->semester."'";  
			}
		}

		if(isset($request->degree)){
			if($request->degree != ''){
				$where .=" AND degree = '".$request->degree."'";  
			}
		}

		$query = "SELECT * FROM `eb_college_book`" .$where;
		$result = \DB::select($query);

		$semester = array();
		foreach($result as $sem){
			array_push($semester, $sem->semester);
		}
		$semester = array_unique($semester);

		$degree = array();
		foreach($result as $deg){
			array_push($degree, $deg->degree);
		}
		$degree = array_unique($degree);

		$totalCount = count($result);
		$slice = array_slice($result, $paginate * ($page - 1), $paginate);
		$book = new Paginator($slice ,count($result), $paginate);
		$viewData['book'] = $book;
		$viewData['semester'] = $semester;
		$viewData['degree'] = $degree;

		return view('search_collegeBook',with($viewData));

}

}