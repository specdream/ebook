<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Helper;
use Session;
use Redirect;
use DB;

class cartController extends Controller{    

  public function addToCart(Request $request){

    $key_cart = $this->keyCookie();
    $inData['product'] = $id = $request->id;
    $inData['key_cart'] = $key_cart;
    if(isset($request->college)){
      $inData['college'] = '1';
    }
    $inData['created'] = date("Y-m-d H:i:s");
    if(isset($request->college)){
      $getprodCart = \DB::table('cart')->where('college','1')->where('key_cart',$key_cart)->where('product',$id)->get();
        //print_r(count($getprodCart));exit;
      if(count($getprodCart) == 0){
        DB::table('cart')->insert($inData);
        $getCart = \DB::table('cart')->where('college','1')->where('key_cart',$key_cart)->get();
        $getCart1 = \DB::table('cart')->where('college','0')->where('key_cart',$key_cart)->get();
        $echoData = '';
        $totalPrice = '0';
        if(count($getCart) > 0){
          $totalCart = '0';
          $totalCart += count($getCart);
          $totalCart += count($getCart1);
          $echoData .= '<input type="hidden" id="totalCart" value="'.$totalCart.'">';
          
          foreach ($getCart as $value) {
            $book = \DB::table('eb_college_book')->where('id',$value->product)->first();
            $echoData .= '<div class="main-div-cart col-xs-12 col-md-12">
            <div class="first-div-cart">
             <div class="add-cart-img">
               <img class="img-responsive" src="'.asset('public/uploads/college_books_banner') . '/'.  $book->id . '/'.  $book->banner.'">
             </div>
             <div class="add-cart-detail">
               <h3>'.$book->title.'</h3>
               <p>by '.\Helper::getUser($book->user_id,'name').'</p>
               <a href="javascript:void(0)" onclick="removeToCartCollege('.$book->id.')">Remove</a>
             </div>
           </div>
           <div class="cart-price-table pull-right">';
                     /*$echoData .= '<h3 class="dontprazremove">';
          if($book->price_type == 'Paid'){
          $echoData .= \Session::get('ipCurrencySymbol').' '.\Helper::currencyPriceVal($book->price,$book->currency);
          $totalPrice += \Helper::currencyPriceVal($book->price,$book->currency);
          }
          else{
          $echoData .= 'Free';
          }
          $echoData .= '</h3>';*/
          $echoData .= '<h3>';

          if($book->price_type == 'Paid'){
            $echoData .= '$ '.$book->price;
            $totalPrice += $book->price;
          }else{
            $echoData .= 'Free';
          }
          
          $echoData .= '</h3>';
          $echoData .= '</div></div>';
        }
        if(count($getCart1) > 0){
          foreach($getCart1 as $value){
            $book = \DB::table('eb_book')->where('id',$value->product)->first();
            $totalPrice += $book->price;
          }
        }
        $echoData .= '<input type="hidden" id="totalCartPrice" value="'.$totalPrice.'">';
        echo $echoData;
      }


    }else{
      echo "exists";exit;
    }
  }else{
    $getprodCart = \DB::table('cart')->where('college','0')->where('key_cart',$key_cart)->where('product',$id)->get();
        //print_r(count($getprodCart));exit;
    if(count($getprodCart) == 0){
      DB::table('cart')->insert($inData);
      $getCart = \DB::table('cart')->where('college','0')->where('key_cart',$key_cart)->get();
      $getCart1 = \DB::table('cart')->where('college','1')->where('key_cart',$key_cart)->get();
      $echoData = '';
      $totalPrice = '0';
      if(count($getCart) > 0){
        $totalCart = '0';
        $totalCart += count($getCart);
        $totalCart += count($getCart1);
        $echoData .= '<input type="hidden" id="totalCart" value="'.$totalCart.'">';
        foreach ($getCart as $value) {
          $book = \DB::table('eb_book')->where('id',$value->product)->first();
          $echoData .= '<div class="main-div-cart col-xs-12 col-md-12">
          <div class="first-div-cart">
           <div class="add-cart-img">
             <img class="img-responsive" src="'.asset('public/uploads/books_banner') . '/'.  $book->id . '/'.  $book->banner.'">
           </div>
           <div class="add-cart-detail">
             <h3>'.$book->title.'</h3>
             <p>by '.\Helper::getUser($book->user_id,'name').'</p>
             <a href="javascript:void(0)" onclick="removeToCart('.$book->id.')">Remove</a>
           </div>
         </div>
         <div class="cart-price-table pull-right">';
                     /*$echoData .= '<h3 class="dontprazremove">';
          if($book->price_type == 'Paid'){
          $echoData .= \Session::get('ipCurrencySymbol').' '.\Helper::currencyPriceVal($book->price,$book->currency);
          $totalPrice += \Helper::currencyPriceVal($book->price,$book->currency);
          }
          else{
          $echoData .= 'Free';
          }
          $echoData .= '</h3>';*/
          $echoData .= '<h3>';

          if($book->price_type == 'Paid'){
            $echoData .= '$ '.$book->price;
            $totalPrice += $book->price;
          }else{
            $echoData .= 'Free';
          }
          
          $echoData .= '</h3>';
          $echoData .= '</div></div>';
        }
        if(count($getCart1) > 0){
          foreach($getCart1 as $value){
            $book = \DB::table('eb_college_book')->where('id',$value->product)->first();
            $totalPrice += $book->price;
          }
        }
        $echoData .= '<input type="hidden" id="totalCartPrice" value="'.$totalPrice.'">';
        echo $echoData;
      }


    }else{
      echo "exists";exit;
    }
  }



}

public function removeToCart(Request $request){

  $key_cart = $this->keyCookie();
  $id = $request->id;

  if(isset($request->college)){

   \DB::table('cart')->where('college','1')->where('key_cart',$key_cart)->where('product',$id)->delete(); 
   $getCart = \DB::table('cart')->where('college','1')->where('key_cart',$key_cart)->get();
   $getCart1 = \DB::table('cart')->where('college','0')->where('key_cart',$key_cart)->get();
   $echoData = '';
   $totalPrice = '0';
   if(count($getCart) > 0){
    $totalCart = '0';
    $totalCart += count($getCart);
    $totalCart += count($getCart1);
    $echoData .= '<input type="hidden" id="totalCart" value="'.$totalCart.'">';

    foreach ($getCart as $value) {
      $book = \DB::table('eb_college_book')->where('id',$value->product)->first();
      $echoData .= '<div class="main-div-cart col-xs-12 col-md-12">
      <div class="first-div-cart">
       <div class="add-cart-img">
         <img class="img-responsive" src="'.asset('public/uploads/college_books_banner') . '/'.  $book->id . '/'.  $book->banner.'">
       </div>
       <div class="add-cart-detail">
         <h3>'.$book->title.'</h3>
         <p>by '.\Helper::getUser($book->user_id,'name').'</p>
         <a href="javascript:void(0)" onclick="removeToCartCollege('.$book->id.')">Remove</a>
       </div>
     </div>
     <div class="cart-price-table pull-right">';
                     /*$echoData .= '<h3 class="dontprazremove">';
          if($book->price_type == 'Paid'){
          $echoData .= \Session::get('ipCurrencySymbol').' '.\Helper::currencyPriceVal($book->price,$book->currency);
          $totalPrice += \Helper::currencyPriceVal($book->price,$book->currency);
          }
          else{
          $echoData .= 'Free';
          }
          $echoData .= '</h3>';*/
          $echoData .= '<h3>';

          if($book->price_type == 'Paid'){
            $echoData .= '$ '.$book->price;
            $totalPrice += $book->price;
          }else{
            $echoData .= 'Free';
          }
          
          $echoData .= '</h3>';
          $echoData .= '</div></div>';
        }
        if(count($getCart1) > 0){
          foreach($getCart1 as $value){
            $book = \DB::table('eb_book')->where('id',$value->product)->first();
            $totalPrice += $book->price;
          }
        }
        $echoData .= '<input type="hidden" id="totalCartPrice" value="'.$totalPrice.'">';
        $echoData .= '<input type="hidden" id="zero" value="0">';
        echo $echoData;
      }else{
        $getCart1 = \DB::table('cart')->where('college','0')->where('key_cart',$key_cart)->get();
        $totalPrice = '0';
        if(count($getCart1) > 0){
          foreach($getCart1 as $value){
            $book = \DB::table('eb_book')->where('id',$value->product)->first();
            $totalPrice += $book->price;
          }
        }
        $echoData = '';

        $echoData .= '<input type="hidden" id="totalCart" value="'.count($getCart1).'">';
        $echoData .= '<input type="hidden" id="totalCartPrice" value="'.$totalPrice.'">';
        $echoData .= '<input type="hidden" id="zero" value="1">';
        echo $echoData;exit;
      }



    }else{
     //echo "string";exit;
      \DB::table('cart')->where('college','0')->where('key_cart',$key_cart)->where('product',$id)->delete();  

      $getprodCart = \DB::table('cart')->where('college','0')->where('key_cart',$key_cart)->get();
        //print_r(count($getprodCart));exit;
      if(count($getprodCart) > 0){
        $getCart = \DB::table('cart')->where('college','0')->where('key_cart',$key_cart)->get();
        $getCart1 = \DB::table('cart')->where('college','1')->where('key_cart',$key_cart)->get();
        $echoData = '';
        $totalPrice = '0';
        if(count($getCart) > 0){
          $totalCart = '0';
          $totalCart += count($getCart);
          $totalCart += count($getCart1);
          $echoData .= '<input type="hidden" id="totalCart" value="'.$totalCart.'">';
          foreach ($getCart as $value) {
            $book = \DB::table('eb_book')->where('id',$value->product)->first();
            $echoData .= '<div class="main-div-cart col-xs-12 col-md-12">
            <div class="first-div-cart">
             <div class="add-cart-img">
               <img class="img-responsive" src="'.asset('public/uploads/books_banner') . '/'.  $book->id . '/'.  $book->banner.'">
             </div>
             <div class="add-cart-detail">
               <h3>'.$book->title.'</h3>
               <p>by '.\Helper::getUser($book->user_id,'name').'</p>
               <a href="javascript:void(0)" onclick="removeToCart('.$book->id.')">Remove</a>
             </div>
           </div>
           <div class="cart-price-table pull-right">';
                     /*$echoData .= '<h3 class="dontprazremove">';
          if($book->price_type == 'Paid'){
          $echoData .= \Session::get('ipCurrencySymbol').' '.\Helper::currencyPriceVal($book->price,$book->currency);
          $totalPrice += \Helper::currencyPriceVal($book->price,$book->currency);
          }
          else{
          $echoData .= 'Free';
          }
          $echoData .= '</h3>';*/
          $echoData .= '<h3>';

          if($book->price_type == 'Paid'){
            $echoData .= '$ '.$book->price;
            $totalPrice += $book->price;
          }else{
            $echoData .= 'Free';
          }
          
          $echoData .= '</h3>';
          $echoData .= '</div></div>';
        }
        if(count($getCart1) > 0){
          foreach($getCart1 as $value){
            $book = \DB::table('eb_college_book')->where('id',$value->product)->first();
            $totalPrice += $book->price;
          }
        }
        $echoData .= '<input type="hidden" id="totalCartPrice" value="'.$totalPrice.'">';
        echo $echoData;
      }


    }else{
      
      $getCart1 = \DB::table('cart')->where('college','1')->where('key_cart',$key_cart)->get();
      $totalPrice = '0';
      if(count($getCart1) > 0){
        foreach($getCart1 as $value){
          $book = \DB::table('eb_college_book')->where('id',$value->product)->first();
          $totalPrice += $book->price;
        }
      }
      $echoData = '';

      $echoData .= '<input type="hidden" id="totalCart" value="'.count($getCart1).'">';
      $echoData .= '<input type="hidden" id="totalCartPrice" value="'.$totalPrice.'">';
      $echoData .= '<input type="hidden" id="zero" value="1">';
      echo $echoData;exit;
    }
  }
  
        //print_r(count($getprodCart));exit;

  




         //2018-06-21 00:00:00
}

public function addToWish(Request $request){
  $status = '';
  if(\Auth::user()){

    $id = $request->id;
    if(isset($request->college)){
      $getprodCart = \DB::table('eb_book_wish')->where('college','1')->where('book_id',$id)->where('user_id',\Auth::user()->id)->get();
      if(count($getprodCart) == 0){
        $insData['book_id'] = $id;
        $insData['user_id'] = \Auth::user()->id;
        $insData['college'] = '1';
        $insData['created'] = time();
        \DB::table('eb_book_wish')->insert($insData);
        $status = 'success';
      }else{
        $status = 'exists';
      }
    }else{
     $getprodCart = \DB::table('eb_book_wish')->where('college','0')->where('book_id',$id)->where('user_id',\Auth::user()->id)->get();
     if(count($getprodCart) == 0){
      $insData['book_id'] = $id;
      $insData['user_id'] = \Auth::user()->id;
      $insData['created'] = time();
      \DB::table('eb_book_wish')->insert($insData);
      $status = 'success';
    }else{
      $status = 'exists';
    }
  }
  
}else{
  $status = 'notLogin';
}
echo $status;
}

public function removeToWish(Request $request){
  $status = '';
  if(\Auth::user()){
    $id = $request->id;
    if(isset($request->college)){
      $getprodCart = \DB::table('eb_book_wish')->where('college','1')->where('book_id',$id)->where('user_id',\Auth::user()->id)->get();
      if(count($getprodCart) > 0){
       \DB::table('eb_book_wish')->where('book_id',$id)->where('user_id',\Auth::user()->id)->delete(); 
       $status = 'success';
     }else{
      $status = 'noData';
    }
  }else{
   $getprodCart = \DB::table('eb_book_wish')->where('college','0')->where('book_id',$id)->where('user_id',\Auth::user()->id)->get();
   if(count($getprodCart) > 0){
     \DB::table('eb_book_wish')->where('book_id',$id)->where('user_id',\Auth::user()->id)->delete(); 
     $status = 'success';
   }else{
    $status = 'noData';
  } 
}

}else{
  $status = 'notLogin';
}
echo $status;
}

public function keyCookie(){


  if(isset($_COOKIE["key_cart"])){

    $key_cart = $_COOKIE["key_cart"];

  }else{

    $key_cart = md5(microtime().rand());
    setcookie("key_cart", $key_cart, time() + (86400 * 30))  ;

  }
  return $key_cart;

}
public function wishlist(){
  if(\Auth::user()){
    $user_id = \Auth::user()->id;
    $wish = DB::table('eb_book_wish')->where('college','0')->where('user_id',$user_id)->get();
    return view('wishlist',compact('wish'));
  }else{
    return redirect('login');
  }
}

public function wishlistCollege(){
  if(\Auth::user()){
    $user_id = \Auth::user()->id;
    $wish = DB::table('eb_book_wish')->where('college','1')->where('user_id',$user_id)->get();
    return view('wishlistCollege',compact('wish'));
  }else{
    return redirect('login');
  }
}
}