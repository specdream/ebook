<?php namespace App\Http\Controllers\Auth;

use App\User;
use Auth;
//use Socialize;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Laravel\Socialite\Facades\Socialite;
use Exception;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    /**
    * Create a new authentication controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
    * Get a validator for an incoming registration request.
    *
    * @param  array  $data
    * @return \Illuminate\Contracts\Validation\Validator
    */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            ]);
    }

    /**
    * Create a new user instance after a valid registration.
    *
    * @param  array  $data
    * @return User
    */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            ]);
    }
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {        
        $user = Socialite::driver('google')->user();
        if($user->email){
            $authUser = User::where('email', $user->email)->first();
            if(count($authUser) > 0){
                if (\Auth::loginUsingId($authUser->id)) {
                    return redirect()->to('user/login');
                }
            } else {
                if($user->name){
                    $name = explode(' ', $user->name);
                    $first_name =  $name[0];
                    $last_name = $name[1];
                }
                $gmailusers = array(
                    'first_name'  => $first_name,
                    'last_name'   => $last_name,   
                    'email'       => $user->email , 
                    'avatar'      => $user->avatar,
                    'dob'         => /*$user->dob*/'' ,
                    );
                \Session::put('loginDatas',$gmailusers);
                \Session::set('popup','step');
                \Session::set('social','gm');
                return redirect()->to('/');
            }
        } else {
            return redirect()->to('user/login');
        }
    }

    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();
        if($user->email){
            $authUser = User::where('email', $user->email)->first();
            if(count($authUser) > 0){
                if (\Auth::loginUsingId($authUser->id)) {
                    return redirect()->to('user/login');
                }
            } else {
                if($user->name){
                    $name = explode(' ', $user->name);
                    $first_name =  $name[0];
                    $last_name = $name[1];
                }
                $gmailusers = array(
                    'first_name'  => $first_name,
                    'last_name'   => $last_name,   
                    'email'       => $user->email , 
                    'avatar'      => $user->avatar,
                    'dob'         => /*$user->dob*/'' ,
                    );
                \Session::put('loginDatas',$gmailusers);
                \Session::set('popup','step');
                \Session::set('social','fb');
                return redirect()->to('/');
            }
        } else {
            return redirect()->to('user/login');
        }
    }

    public function redirectToTwitter() {
        return Socialite::driver('twitter')->redirect();
    }

    public function handleTwitterCallback()
    {
        try {

            $user = Socialite::driver('twitter')->user();
            $authUser = $this->findOrCreateUsers($user);

            Auth::login($authUser, true);

            return redirect()->to('/user/login');

        } catch (Exception $e) {
            return redirect()->to('/user/twitter');
    //echo "Twitter dosen't return your email id";exit;
    //return redirect('auth/twitter');
        } 
    }

    public function redirectToLinkedin()
    { 
        return Socialite::with('linkedin')->redirect();
    }

    public function handleLinkedinCallback()
    {
        if(isset($_REQUEST['code']) && $_REQUEST['code'] != ''){
            $user   = $this->linkedCurl($_REQUEST['code']);
            $authUser   = $this->findOrCreateUserLinked($user);
            Auth::login($authUser, true);
            return redirect()->to('/user/login');
        } else {
            return redirect()->to('/user/login');
        }
    }

    public function linkedCurl($value='')
    {
        $config         = config('services');
        $client_id      = $config['linkedin']['client_id'];
        $client_secret  = $config['linkedin']['client_secret'];
        $redirect       = $config['linkedin']['redirect'];

        $curl = curl_init("https://www.linkedin.com/oauth/v2/accessToken?grant_type=authorization_code&code=".$value."&redirect_uri=".$redirect."&client_id=".$client_id."&client_secret=".$client_secret);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = json_decode(curl_exec($curl));

        $curl1 = curl_init("https://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,maiden-name,formatted-name,headline,location,industry,current-share,num-connections,num-connections-capped,summary,specialties,positions,picture-url,picture-urls::(original))?format=json&oauth2_access_token=".$result->access_token);

        curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);

        return json_decode(curl_exec($curl1));
    }

    private function findOrCreateUserLinked($User)
    {
        $authUser = User::where('email', $User->emailAddress)->first();

        if (!$authUser){
            // $user_name  = explode(' ', $User->name);
            $code       = rand(10000,10000000);
            $authen                 = new User;
            $authen->first_name     = $User->firstName;
            $authen->last_name      = $User->lastName;
            $authen->email          = trim($User->emailAddress);
            $authen->phone_number   = '';
            $authen->country_code   = '';
            $authen->activation     = $code;
            $authen->group_id       = '4';
            $authen->password       = '';
            $authen->active         = '1';
            $authen->save();

            $id = \DB::getPdo()->lastInsertId();
            if(!empty($User->pictureUrls)){
                if($User->pictureUrls->_total != 0){

                    $url    = $User->pictureUrls->values[0];
                    $name   = $id.".jpg";

                    $img    = base_path()."/uploads/users/".$name;
                    $suc    = file_put_contents($img, file_get_contents($url));

                    if($suc){
                        $image  = ['avatar'=>$name];
                        $query1 = \DB::table('tb_users')->where('id','=', $id)->update($image);
                    }
                }
            }
            $authUser = User::where('id', $id)->first();            
        }
        return $authUser;
    }

    private function findOrCreateUsers($twitterUser)
    {
        $authUser = User::where('email', $twitterUser->id)->first();
        if ($authUser){
            return $authUser;
        }else{
            return User::create([
                'username' => $twitterUser->name,
                'handle' => $twitterUser->nickname,
                'twitter_id' => $twitterUser->id,
                'avatar' => $twitterUser->avatar_original
            ]);
        }
    }

}