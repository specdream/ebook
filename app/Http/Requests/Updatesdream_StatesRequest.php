<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Updatesdream_StatesRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'sdream_countries_id' => 'required', 
            'name' => 'required', 
            
		];
	}
}
