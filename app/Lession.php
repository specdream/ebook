<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class Lession extends Model {
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $dates = ['deleted_at'];
    protected $table = 'tb_lession';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['test_type', 'name', 'description', 'video','course_id','author_id','status','order','subtype_id','assessment_test','pass_percentage'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public static function boot()
    {
        parent::boot();

        Lession::observe(new UserActionsObserver);
    }
    //  public static function author_name($id)
    // {
    //     $author=\DB::table('tb_users')->select('name')->where('id',$id)->first();
    //     return $author->name;
    // }
     public function author_name()
    {
        return $this->hasOne('App\Company', 'id', 'author_id');
    }
    public function Test_type()
    {
        return $this->hasOne('App\Test_Type', 'id', 'test_type');
    }
     public function QuestionDetails()
    {
        return $this->hasOne('App\Question', 'lession_id', 'id');
    }
   

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
    // public function getHelpfulCountAttribute() {
    //     $reviewshelpful = Lession::where('course_id', $this->attributes['id'])->whereNull('deleted_at')->count();
    //     return $reviewshelpful;
    // }
}
