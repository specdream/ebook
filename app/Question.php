<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model {
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $dates = ['deleted_at'];
    protected $table = 'tb_lession_question';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['course_id', 'lession_id', 'test_type', 'question','change_order','mark','hours','minutes','num_que'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public static function boot()
    {
        parent::boot();

        Lession::observe(new UserActionsObserver);
    }
    public function author_name()
    {
        return $this->hasOne('App\Company', 'id', 'author_id');
    }
    public function Test_type()
    {
        return $this->hasOne('App\Test_Type', 'id', 'test_type');
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
