<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class Tb_Languages_Settings extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'tb_languages_settings';
    
    protected $fillable = [
          'tb_languages_id',
          'key_name',
          'value',
          'uniq_record',
          'added_by'
    ];
    

    public static function boot()
    {
        parent::boot();

        Tb_Languages_Settings::observe(new UserActionsObserver);
    }
    
    public function tb_languages()
    {
        return $this->hasOne('App\Tb_Languages', 'id', 'tb_languages_id');
    }


    
    
    
}