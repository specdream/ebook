<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class Tb_Learner_Projects extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'tb_learner_projects';
    
    protected $fillable = [
          'learner_id',
          'course_id',
          'teacher_id',
          'file_type',
          'file',
          'status',
          'reason'
    ];
    

    public static function boot()
    {
        parent::boot();

        Tb_Learner_Projects::observe(new UserActionsObserver);
    }
}