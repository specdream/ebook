<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class Tb_Courses extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'tb_courses';
    
    protected $fillable = [
          'status',
          'teacher_id',
          'prof_id',
          'cat_id',
          'title',
          'summary',
          'image',
          'promo_video',
          'price_type',
          'price',
          'published_date',
          'added_by',
          'assessment_test',
    ];
    
    public static $status = ["Draft" => "Draft", "Pending" => "Pending", "Accept" => "Accept", "Reject" => "Reject"];
    public static $price_type = ["Free" => "Free", "Paid" => "Paid"];


    public static function boot()
    {
        parent::boot();

        Tb_Courses::observe(new UserActionsObserver);
    }
    
    public function tb_professional()
    {
        return $this->hasOne('App\Tb_Professional', 'id', 'prof_id');
    }

    public function teacher_name()
    {
        return $this->hasOne('App\Teacher', 'id', 'teacher_id');
    }
    public function creater_name()
    {
        return $this->hasOne('App\Teacher', 'id', 'added_by');
    }

     public function lession_details()
    {
        return $this->HasMany('App\Lession', 'course_id', 'id');
    }
    
    public function tb_categories_type()
    {
        return $this->hasOne('App\Tb_Categories_Type', 'id', 'cat_id');
    }
    public function tb_languages_settings()
    {
        return $this->hasOne('App\Tb_Languages_Settings', 'key_name', 'title');
    }
    public function tb_languages_settings_summary()
    {
        return $this->hasOne('App\Tb_Languages_Settings', 'key_name', 'summary');
    }


    
    
    
}