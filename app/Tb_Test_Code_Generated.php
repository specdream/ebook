<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class Tb_Test_Code_Generated extends Model {
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tb_test_code_generated';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['learner_id', 'key_code', 'course_id', 'attempt','is_used','lesson_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    public static function boot()
    {
        parent::boot();

        Tb_Test_Code_Generated::observe(new UserActionsObserver);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
