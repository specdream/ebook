<?php
namespace App\Helpers;
use App\Tb_Courses;
use App\sdream_Countries;
use App\Tb_Categories_Type;
use App\Tb_Languages_Settings;
use App\Tb_Languages;
use App\Teacher;
use App\Lession;
use App\User;
use App\Question;
use App\Answer;
use App\Tb_Test_Code_Generated;
use App\TB_Learner_Test_Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Collection;

class Helper{
 
    public static function getCourseDetails($cat_id){
        $aCat_courses = \DB::table('eb_book')->where('status','1')->where('categorie',$cat_id)->limit(10)->orderBy('id', 'desc')->get();

        return $aCat_courses;
    }

    public static function getCountryDetails(){
        $aCountries = sdream_Countries::all();

        return $aCountries;
    }
    public static function getCourseCategoryDetails(){
        $aCountries = Tb_Categories_Type::all();

        return $aCountries;
    }
    public function keyValue($title)
    {
        $lan=Tb_Languages_Settings::where('key',$title)->first();
        if(count($lan) > 0){
            return $lan->value;    
        }else{
            return $title;
        }
        
    }
    public static function getCourseDetails_random($cat_id,$id){
        $aCat_courses = Tb_Courses::where('status','Accept')->whereNotIn('id',[$id])->where('cat_id',$cat_id)->limit(10)->orderBy('id', 'desc')->with("tb_languages_settings")->with("teacher_name")->inRandomOrder()->limit('5')->get();

        return $aCat_courses;
        // $aCountries = sdream_Countries::inRandomOrder()->limit('5')->get();

        // return $aCountries;
    }
    public static function language(){
        $lang = (\Session::get('lang') != "" ? \Session::get('lang') : 'en');
        \App::setLocale($lang);
        return $lang;
    }
    public static function getLangValue($title)
    {
        $lang = (\Session::get('lang') != "" ? \Session::get('lang') : 'en');
        $langu=Tb_Languages::where('short_name',$lang)->first();
        $lan=\DB::table('tb_languages_settings')->where('key_name',$title)->where('tb_languages_id',$langu->id)->first();
        if($lan!=''){
            return $lan->value; 
        }else{
            $lan=\DB::table('tb_languages_settings')->where('key_name',$title)->where('tb_languages_id','1')->first();
            if($lan!=''){
                return $lan->value; 
            }else{
                return $title;
            }
        }

    }
    public static function getCourseKey($id,$type){
        $teacher=Tb_Courses::where('id',$id)->first();
        if($type=='key'){
            return $teacher->title;
        }else if($type=='image'){
            return $teacher->image;
        }else if($type=='teacher'){
            $teacher_name=User::find($teacher->teacher_id);
            return $teacher_name->name;
        }else if($type=='category'){
            $Tb_Categories_Type=Tb_Categories_Type::find($teacher->cat_id);
            return $Tb_Categories_Type->name;
        }
    }
    public static function getUser($id,$key){
        $user=User::where('id',$id)->first();
        if($user){
            if($key == 'name'){
                return $user->name;
            }elseif($key == 'avatar'){
                if($user->avatar !=''){
                    return asset('/public/uploads').'/'.$user->avatar;    
                }else{
                    return asset('sdream/img/no-user.png');    
                }
            }
        }else{
            return 'null';
        }
    }
    public static function getLessonKey($id,$type){
        $teacher=Lession::where('id',$id)->first();
        if($type=='key'){
            return $teacher->name;
        }
    }
    public static function getLearnerLessonStatus($lesson_id,$course_id,$learner_id)
    {
        // $status=TB_Learner_Test_Status::where('lesson_id',$lesson_id)->where('course_id',$course_id)->where('learner_id',$learner_id)->where('status','completed')->get();

        $status=Tb_Test_Code_Generated::where('learner_id',\Auth::user()->id)->where('course_id',$course_id)->where('lesson_id',$lesson_id)->orderBy('id','DESC')->where('is_pass','yes')->get();
        return count($status);
    }
    // public static function getTeacherKey($id){
    //     $teacher=Teacher::where('id',$id)->first();
    //     return $teacher->value; 
    // }
    public static function getAllAnswer($question_id){
        $questions=\DB::table('tb_lession_answer')->where('qid',$question_id)->whereNull('deleted_at')->orderBy('id','ASC')->get();
        return $questions;
    }
    public static function getQuestionKey($id,$type){
        $question=Question::where('id',$id)->first();
        if($type=='key'){
            return $question->question;
        }
    }
    public static function getAnswerKey($id,$type){
        if($id!=''){
            $question=Answer::where('id',$id)->first();
            if($type=='key'){
                return $question->answer;
            }
        }else{
            return 'null';
        }
    }
    public static function getCategory($id){
        $question=Tb_Courses::with("tb_categories_type")->where('id',$id)->first();
        return $question->tb_categories_type->name;
    }
    public static function getLessonNo($lid,$cid){
        $question=Lession::where('course_id',$cid)->get();
        $lno=0;
        if($question){
            foreach ($question as $key => $value) {
                if($value->id==$lid){
                    $lno=$key+1;
                }   
            }
        }else{
            $lno=0;
        }
        // return ordinalize($lno);
        $suff = 'th';
        if ( ! in_array(($lno % 100), array(11,12,13))){
            switch ($lno % 10) {
                case 1:  $suff = 'st'; break;
                case 2:  $suff = 'nd'; break;
                case 3:  $suff = 'rd'; break;
            }
            return "{$lno}{$suff}";
        }
        return "{$lno}{$suff}";
    }
    public static function getCourseStatus($id){
        $Key_Generated=Tb_Test_Code_Generated::where('learner_id',\Auth::user()->id)->where('course_id',$id)->orderBy('id','DESC')->groupBy('learner_id','course_id','lesson_id')->get();
        $Key_Generated_yes=Tb_Test_Code_Generated::where('learner_id',\Auth::user()->id)->where('course_id',$id)->orderBy('id','DESC')->where('is_pass','yes')->get();
        $lesson=Lession::where('course_id',$id)->where('assessment_test','yes')->get();
        if(count($Key_Generated)==0){
            return '<span class="label label-danger">NYS</span>';
        }elseif(count($Key_Generated)<count($lesson)){
           return '<span class="label label-warning">Progressing</span>';
       }elseif(count($Key_Generated_yes)==count($lesson)){
           return '<span class="label label-success">Completed</span>';
       }
   }
   
   public static function categoryName($id){
    $getCat = \DB::table("tb_book_categorie")->where('id',$id)->first();
    if(count($getCat) > 0)
        return $getCat->name;
    else
        return 'null';    
}

public static function subcategoryName($id){
    $getCat = \DB::table("tb_book_sub_categorie")->where('id',$id)->first();
    if(count($getCat) > 0)
        return $getCat->name;
    else
        return 'null';
}

public static function ipUserDetails($key){
    //$query = @unserialize (file_get_contents('http://ip-api.com/php/'));
    //if ($query && $query['status'] == 'success') {
    $query = \DB::table('user_ip_detail')->where('ip',$_COOKIE["userIp"])->first();
        if ($query) {
        $output = '';
        switch ($key) {
            case "lon":
            $output = $query->lon;
            break;
            case "ip":
            $output = $query->query;
            break;
            case "countryCode":
            $output = $query->countryCode;
            break;
            case "city":
            $output = $query->city;
            break;
            case "country":
            $output = $query->country;
            break;
            case "lat":
            $output = $query->lat;
            break;
            case "region":
            $output = $query->region;
            break;
            case "regionName":
            $output = $query->regionName;
            break;
            case "zip":
            $output = $query->zip;
            break;
            case "status":
            $output = $query->status;
            break;
            case "timezone":
            $output = $query->timezone;
            break;
        }
        return $output;
    }else{
        return '';
    }
}

public static function userCurrency($key){

    $countryCode = \Helper::ipUserDetails('countryCode');
    $cuncy = \DB::table("currency")->where('country_code_2',$countryCode)->first();
    $output = '';
    switch ($key) {
        case "code":
        $output = $cuncy->code;
        break;
        case "symbol":
        $output = $cuncy->symbol;
        break;
    }
    return $output;

}

public static function currencyPriceVal($price,$from){

    $to = \Session::get('ipCurrency');

    if($from == $to){
        return $price;
    }else{
        $getDefault = \DB::table('currency')->where('base','1')->first(); 
        $fromCurData = \DB::table('currency')->where('code',$from)->first(); 
        $toCurData = \DB::table('currency')->where('code',$to)->first(); 
        $returnPrice = '';    
        $returnPrice = ($price/$fromCurData->value);
        $returnPrice = ($returnPrice/100)*$toCurData->value;
        $returnPrice = $returnPrice*100;
        return number_format($returnPrice,'2');
    }
    
}

public static function getCartCookie(){
    if(isset($_COOKIE["key_cart"])){

        $key_cart = $_COOKIE["key_cart"];

    }else{

        $key_cart = md5(microtime().rand());
        setcookie("key_cart", $key_cart, time() + (86400 * 30))  ;

    }
    return $key_cart;
}
public static function userIPupdate(){
   
    $query = @unserialize (file_get_contents('http://ip-api.com/php/'));
    if ($query && $query['status'] == 'success') {

        $insOut['lon'] = $query['lon'];
        $insOut['ip'] = $query['query'];
        $insOut['countryCode'] = $query['countryCode'];
        $insOut['city'] = $query['city'];
        $insOut['country'] = $query['country'];
        $insOut['lat'] = $query['lat'];
        $insOut['region'] = $query['region'];
        $insOut['regionName'] = $query['regionName'];
        $insOut['status'] = $query['status'];
        $insOut['timezone'] = $query['timezone'];
        $insOut['zip'] = $query['zip'];
        \DB::table('user_ip_detail')->insert($insOut);
        setcookie("userIp", $query['query'], time() + (86400 * 30));
    }
}


 public static function bookCategory(){

    $getCat = \DB::table("tb_book_categorie")->where('status','1')->get();
    return $getCat;
    
}
 public static function bookDetail($id){

    $getBook = \DB::table("eb_book")->where('id',$id)->first();
    return $getBook;
    
}

 public static function collegeBookDetail($id){

    $getBook = \DB::table("eb_college_book")->where('id',$id)->first();
    return $getBook;
    
}

public static function semName($id){

    $getSem = \DB::table("eb_semesters")->where('id',$id)->first();
    
    if($getSem)
    return $getSem->name;
    else
    return '';    
    
}
public static function degreeName($id){

    $getDegree = \DB::table("eb_degrees")->where('id',$id)->first();

    if($getDegree)
    return $getDegree->name;
    else
    return '';    
    
}


}