<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class sdream_States extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'sdream_states';
    
    protected $fillable = [
          'sdream_countries_id',
          'name'
    ];
    

    public static function boot()
    {
        parent::boot();

        sdream_States::observe(new UserActionsObserver);
    }
    
    public function sdream_countries()
    {
        return $this->hasOne('App\sdream_Countries', 'id', 'sdream_countries_id');
    }


    
    
    
}