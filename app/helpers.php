<?php

    function author_name($id)
    {
        $author=\DB::table('users')->select('name')->where('id',$id)->first();
        return $author->name;
    }
    function getQuestions($course_id,$lession_id){
    	$questions=\DB::table('tb_lession_question')->where('course_id',$course_id)->where('lession_id',$lession_id)->whereNull('deleted_at')->get();
    	return $questions;
    }

    function getAnswer($question_id){
    	$questions=\DB::table('tb_lession_answer')->where('qid',$question_id)->whereNull('deleted_at')->orderBy('id','ASC')->get();
    	return $questions;
    }
    function keyValue($title)
    {
        $lan=\DB::table('tb_languages_settings')->where('key_name',$title)->first();
        if(count($lan) > 0){
            return $lan->value;    
        }else{
            return $title;
        }
    }
    function get_lang_value($title,$lang)
    {
    $lan=\DB::table('tb_languages_settings')->where('key_name',$title)->where('tb_languages_id',$lang)->first();
        if($lan!=''){
           return $lan->value; 
       }else{
        return "<b style='color:#F00'>Value not found</b>";
       }
        
    }
    function reject_reason($id,$type)
    {
        $lan=\DB::table('tb_course_reject')->where('c_id',$id)->orderBy('id', 'desc')->first();
        if($type=='reason'){
            return $lan->reason;
        }else{
            return date('D,M Y H:s',strtotime($lan->date));
        }
        
    }
    function getCourseScount($status)
    {   
        if(\Auth::user()->role_id=='1'){
            $tb_course=\DB::table('tb_courses')->select('id')->where('status',$status)->whereNotNull('deleted_at')->get();
        }else{
            $tb_course=\DB::table('tb_courses')->select('id')->where('teacher_id',\Auth::user()->id)->where('status',$status)->whereNotNull('deleted_at')->get();
        }
        
        return count($tb_course);
    }
    function notification()
    {
        $lan=\DB::table('tb_notification')->where('user_id',\Auth::user()->id)->where('status','0')->orderBy('id', 'desc')->get();
        return $lan;
        
    }
    function getRole($role)
    {
        $lan=\DB::table('roles')->where('id',$role)->first();
        return $lan->title;
        
    }
    function getAvatar($id)
    {
        $lan=\DB::table('users')->where('id',$id)->first();
        return $lan->avatar;
        
    }
    function getTicketCount($status)
    {
        $count=\DB::table('tb_support_system')->where('status',$status)->where('to_id',\Auth::user()->id)->get();
        return count($count);
        
    }
    function getRoleInId($id)
    {
        $lan=\DB::table('users')->where('id',$id)->first();
        $role=\DB::table('roles')->where('id',$lan->role_id)->first();
        return $role->title;
        
    }
?>
