<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class Tb_Categories_Type extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'tb_categories_type';
    
    protected $fillable = [
          'name',
          'status'
    ];
    
    public static $status = ["active" => "active", "inactive" => "inactive"];


    public static function boot()
    {
        parent::boot();

        Tb_Categories_Type::observe(new UserActionsObserver);
    }
    
    
    
    
}