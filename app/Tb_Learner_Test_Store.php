<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class Tb_Learner_Test_Store extends Model {
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tb_learner_test_store';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['learner_id', 'key_code', 'course_id', 'attempt','question_id','lesson_id','answer_id','correct_ans','learner_ans','is_correct','mark','keygen_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    public static function boot()
    {
        parent::boot();

        Tb_Learner_Test_Store::observe(new UserActionsObserver);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
