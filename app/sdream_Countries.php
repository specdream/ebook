<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class sdream_Countries extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'sdream_countries';
    
    protected $fillable = [
          'sortname',
          'name',
          'phonecode'
    ];
    

    public static function boot()
    {
        parent::boot();

        sdream_Countries::observe(new UserActionsObserver);
    }
    
    
    
    
}