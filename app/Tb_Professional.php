<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class Tb_Professional extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'tb_professional';
    
    protected $fillable = ['name'];
    

    public static function boot()
    {
        parent::boot();

        Tb_Professional::observe(new UserActionsObserver);
    }
    
    
    
    
}