<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class BookSubCategories extends Model {
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tb_book_sub_categorie';

  
}
