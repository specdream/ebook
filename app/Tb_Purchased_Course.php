<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class Tb_Purchased_Course extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $table    = 'tb_purchased_course';
    
    protected $fillable = ['learner_id','course_id','paid_amount','paid_amount_currency','payment_status','payment_mode','payment_type','change_id','date','paid_date'];
    

    public static function boot()
    {
        parent::boot();

        Tb_Purchased_Course::observe(new UserActionsObserver);
    }
    
    
    
    
}