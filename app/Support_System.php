<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class Support_System extends Model {
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $dates = ['deleted_at'];
    protected $table = 'tb_support_system';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['from_id', 'to_id', 'subject', 'priority','description','status','rep_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public static function boot()
    {
        parent::boot();

        Support_System::observe(new UserActionsObserver);
    }
   
}
