<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;

use Carbon\Carbon; 

use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
   
    protected $table    = 'coupon';
    
    protected $fillable = [
          'code',
          'price'
    ];
    

    public static function boot()
    {
        parent::boot();

        Coupon::observe(new UserActionsObserver);
    }
    
    
    
    

}