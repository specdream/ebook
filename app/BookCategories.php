<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class BookCategories extends Model {
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tb_book_categorie';

  
}
