<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class sdream_Cities extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'sdream_cities';
    
    protected $fillable = [
          'sdream_states_id',
          'name'
    ];
    

    public static function boot()
    {
        parent::boot();

        sdream_Cities::observe(new UserActionsObserver);
    }
    
    public function sdream_states()
    {
        return $this->hasOne('App\sdream_States', 'id', 'sdream_states_id');
    }


    
    
    
}