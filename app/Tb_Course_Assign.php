<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class Tb_Course_Assign extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'tb_course_assign';
    
    protected $fillable = [
          'id',
          'univ_id',
          'c_id',
          's_limit',
          's_date',
          'e_date',
          'c_key',
          'status'
    ];
    

    public static function boot()
    {
        parent::boot();

        Tb_Course_Assign::observe(new UserActionsObserver);
    }
    
    public function tb_courses()
    {
        return $this->hasOne('App\Tb_Courses', 'id', 'c_id');
    }
    public function users()
    {
        return $this->hasOne('App\User', 'id', 'univ_id');
    }


    
    
    
}