<?php echo $__env->make('layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="container login_page">
    <div class="panel panel-default panel-login">
        <div class="panel-heading">
            <img src="<?php echo asset('sdream/img/login.png'); ?>">
        </div>
        <div class="panel-body">
                 <form class="" role="form" method="POST" action="<?php echo e(url('login')); ?>">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                <div class="form-group">
                    <i class="fa fa-envelope"></i>
                    <input type="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>" placeholder="<?php echo app('translator')->getFromJson('core.E_mail'); ?>">
                </div>

                <div class="form-group">
                    <i class="fa fa-lock"></i>
                    <input type="password" class="form-control" name="password" placeholder="<?php echo app('translator')->getFromJson('core.password'); ?>">
                </div>

                <div class="form-group">
                    <label class="remember_me" for="remember_me"><input id="remember_me" type="checkbox" name="remember"><?php echo app('translator')->getFromJson('core.Remember_Me'); ?></label>
                     <small><?php echo app('translator')->getFromJson('core.by_signing_up_you_agree_to_our_terms_of_use_and_privacy_policy'); ?> .</small>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn login-btn"><?php echo app('translator')->getFromJson('core.Log_In'); ?></button>
                </div>
                <center>
                    <div class="forget_pwd"><?php echo app('translator')->getFromJson('core.or'); ?> <a href=""><?php echo app('translator')->getFromJson('core.Forgot_Password'); ?></a></div>
                   
                    <div class="sign_up"><?php echo app('translator')->getFromJson('core.Dont_have_an_account'); ?> <a href="<?php echo \URL::To('/'); ?>/register"><?php echo app('translator')->getFromJson('core.Sign_up'); ?></a></div>
                </center>
            </form>

        <div class="or_text"><span><?php echo app('translator')->getFromJson('core.OR'); ?></span></div>

            <a class="social_login facebook_btn" href=""><i class="fa fa-facebook-f"></i><?php echo app('translator')->getFromJson('core.Continue_with_Facebook'); ?></a>
            <a class="social_login google_btn" href=""><img src="<?php echo asset('sdream/img/google_plus.png'); ?>"><?php echo app('translator')->getFromJson('core.Continue_with_Google'); ?></a>
            <?php if(count($errors) > 0): ?>
            <div class="alert alert-danger">
                <strong><?php echo trans('quickadmin::auth.whoops'); ?></strong> <?php echo trans('quickadmin::auth.some_problems_with_input'); ?>

                <br><br>
                <ul>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo $error; ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
            <?php endif; ?>
            
   
        </div>
    </div>
</div>
<style type="text/css">
  
</style>
<?php echo $__env->make('layout.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
