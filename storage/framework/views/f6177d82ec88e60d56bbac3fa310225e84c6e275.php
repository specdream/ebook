<?php echo $__env->make('layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="container login_page">
    <div class="panel panel-default panel-login">
        <div class="panel-heading">
            <img src="<?php echo asset('sdream/img/stairs.png'); ?>">
        </div>
        <div class="panel-body">

                        <form class="" role="form" method="POST" action="">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                <div class="form-group">
                    <i class="fa fa-users"></i>
                    <select name="who" id="who" class="form-control">
                        <option value="0" disabled="true" >who am I</option>
                        <option value="2" >Student</option>
                        <option value="8" >Member</option>
                        <option value="5" >Author</option>
                    </select>
                </div>

                <div class="form-group">
                    <i class="fa fa-user"></i>
                    <input type="text" class="form-control" name="fname" value="" placeholder="<?php echo app('translator')->getFromJson('core.Full_Name'); ?>">
                </div>

                <div class="form-group">
                    <i class="fa fa-envelope"></i>
                    <input type="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>" placeholder="<?php echo app('translator')->getFromJson('core.E_mail'); ?>">
                </div>

                <div class="form-group">
                    <i class="fa fa-map-marker"></i>
                    <select name="country" id="country" class="form-control">
                        <option><?php echo app('translator')->getFromJson('core.Select_Country'); ?></option>
                        <?php $__currentLoopData = \Helper::getCountryDetails(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $aCountry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo $aCountry->id; ?>" data-type="<?php echo $aCountry->phonecode; ?>"><?php echo $aCountry->name; ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>

                <div class="form-group">
                    <i class="fa fa-mobile"></i>
                    <input type="text" class="form-control" name="city" placeholder="<?php echo app('translator')->getFromJson('core.Enter_your_city'); ?>">
                </div>

                <div class="form-group">
                <input type="hidden" name="country_code">
                    <span id="country_code1">+91</span><input type="text" class="form-control" name="phone_number" placeholder="<?php echo app('translator')->getFromJson('core.Mobile_Number'); ?>" onkeypress="return isNumber(event)">
                </div>

                <div class="form-group">
                    <i class="fa fa-lock"></i>
                    <input type="password" class="form-control" name="password" placeholder="<?php echo app('translator')->getFromJson('core.password'); ?>">
                </div>

                <div class="form-group">
                    <i class="fa fa-lock"></i>
                    <input type="password" class="form-control" name="confirmpassword" placeholder="<?php echo app('translator')->getFromJson('core.confirm_password'); ?>">
                </div>                

                <div class="form-group">
                    <label class="remember_me" for="remember_me"><input id="remember_me" type="checkbox" name="remember"><?php echo app('translator')->getFromJson('core.Remember_Me'); ?></label>
                     <small><?php echo app('translator')->getFromJson('core.by_signing_up_you_agree_to_our_terms_of_use_and_privacy_policy'); ?>.</small>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn login-btn"><?php echo app('translator')->getFromJson('core.Sign_up'); ?></button>
                </div>
                <center>
                   
                    <div class="sign_up"><?php echo app('translator')->getFromJson('core.Do_have_an_account'); ?> <a href="<?php echo \URL::To('/'); ?>/login"><?php echo app('translator')->getFromJson('core.Sign_in'); ?></a></div>
                </center>
            </form>

            <div class="or_text"><span><?php echo app('translator')->getFromJson('core.OR'); ?></span></div>

            <a class="social_login facebook_btn" href=""><i class="fa fa-facebook-f"></i><?php echo app('translator')->getFromJson('core.Continue_with_Facebook'); ?></a>
            <a class="social_login google_btn" href=""><img src="<?php echo asset('sdream/img/google_plus.png'); ?>"><?php echo app('translator')->getFromJson('core.Continue_with_Google'); ?></a>
            <?php if(count($errors) > 0): ?>
            <div class="alert alert-danger">
                <strong><?php echo trans('quickadmin::auth.whoops'); ?></strong> <?php echo trans('quickadmin::auth.some_problems_with_input'); ?>

                <br><br>
                <ul>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo $error; ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
            <?php endif; ?>
            

        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on('change','#country',function(){
        $("#country_code1").html($(this).find(':selected').attr('data-type'));
         $("#country_code").val($(this).find(':selected').attr('data-type'));
    })
    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
<?php echo $__env->make('layout.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
