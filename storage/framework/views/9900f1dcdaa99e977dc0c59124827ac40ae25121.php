<link href="<?php echo asset('quickadmin/css/animate.css'); ?>" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<?php if(Auth::user()): ?>
<?php $notifications=notification(); ?>
<?php else: ?>
<?php $notifications = array(); ?>
<?php endif; ?>

<style type="text/css">
    .link-block {
    font-size: 12px;
    padding: 10px;
}
</style>
<div class="page-header navbar navbar-fixed-top top-author-nav">
    <div class="page-header-inner">
        <div class="page-header-inner">
            <div class="navbar-header">
                <a href="<?php echo e(url(config('quickadmin.homeRoute'))); ?>" class="navbar-brand">
                    <img src="<?php echo \URL::To('/'); ?>/sdream/img/logo.jpeg"></a>
                </a>
            </div>
            <a href="javascript:;"
            class="menu-toggler responsive-toggler"
            data-toggle="collapse"
            data-target=".navbar-collapse">
        </a>

        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">

            <li id="refresh_cnt" class="user dropdown">   
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle count-info" aria-expanded="true">
        <span data-toggle="tooltip" title="Click to view experience details"> <i class="icon-yelp fa fa-bell-o"></i>  <span class=" label label-danger feacnt1" style="border-radius: 50%;">
                          <?php echo e(count($notifications)); ?></span>
                      </span></a>
                      <ul class="dropdown-menu dropdown-menu-right icons-right flipInY animated" code=""  style="width: 400px;max-height: 354px;overflow:auto">
                        <li><div class="text-center"><label><h4><b>Notification(s)</b></h4></label></div><hr></li>
                         <?php if(count($notifications)>0): ?>
                         <?php $__currentLoopData = $notifications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notifications1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li id="ffproid_<?php echo e($notifications1->id); ?>" >
            <div class="text-center link-block col-sm-12">
                <a href="<?php echo e(url('rooms/')); ?>/" target="_blank">
                    <span data-toggle="tooltip" title="Click to view this room details"> 
                        <div class="col-sm-2"><img src="<?php echo e(URL::To('/')); ?>/public/uploads/<?php echo getAvatar($notifications1->n_user_id); ?>" class="origin round user_image" style="width: 35px;"></div>
                        <div class="col-sm-10" style="text-align: left;font-size:12px"><strong>User Role :</strong> <?php echo e(getRole($notifications1->n_role)); ?><br><strong>User Name :</strong><?php echo e($notifications1->n_uname); ?> <br><strong>Notification :</strong>
                            <?php if(strlen($notifications1->notification)<=40): ?>
                            <?php echo e($notifications1->notification); ?>

                            <?php else: ?>
                            <?php echo e(substr($notifications1->notification,0,40)); ?>...
                            <?php endif; ?>
                        </div></span></a>
                        
                    </div>
                </li>                       
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                        <li id="npro_nodet"  <?php if(count($notifications)>0): ?> style="display: none;" <?php endif; ?> ><div class="text-center">No details found</div></li>
                    </ul>

                </li>  

                <li class="user dropdown"><a class="dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown"><img src="<?php echo asset('sdream/img/client-img.jpg'); ?>" class="rounded-circle" alt=""><i class="caret"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right icons-right flipInY animated">
                    <li><a href=""><i class="fa  fa-user"></i> <?php if(Auth::user()): ?> Auth::user()->name <?php endif; ?></a></li>
                        <li><a href="<?php echo URL::To('/'); ?>/admin"><i class="fa  fa-laptop"></i> Dashboard</a></li>
                        <li><a href="<?php echo URL::To('/'); ?>" target="_blank"><i class="fa fa-desktop"></i> Main Site</a></li>
                        <li><a href="<?php echo URL::To('/'); ?>/admin/profile"><i class="fa fa-user"></i> Profile</a></li>
                        <!-- <li><a href="http://sdreamtechdemo.com/products/airstar/airbnb/user/logout"><i class="fa fa-sign-out"></i> Logout</a>  </li> -->
                        <li> <?php echo Form::open(['url' => 'logout']); ?>

                <button type="submit" class="logout" style="color: #555;">
                    <i class="fa fa-sign-out fa-fw"></i>
                    <span class="title"><?php echo e(trans('quickadmin::admin.partials-sidebar-logout')); ?></span>
                </button>
                <?php echo Form::close(); ?></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
</div>
<style type="text/css">
    .user.dropdown img {
    height: 50px;
    width: 50px;
}

.rounded-circle {
    border-radius: 50%!important;
}
.user.dropdown img {
    max-width: 4.75rem;
}

</style>