<?php $__env->startSection('content'); ?>

    <!-- <?php echo e(trans('quickadmin::admin.dashboard-title')); ?> -->

<script type="text/javascript" src="<?php echo asset('quickadmin/js/icon.js'); ?>"></script>
<script type="text/javascript">

	jQuery(document).ready(function(){

    jQuery('#hideshow').on('click', function(event) {        

         jQuery('.row.m-l-none.m-r-none.m-t.white-bg.shortcut').toggle('show');

    }); 

	jQuery('.number, .squarebox h4').each(function () {
	    $(this).prop('Counter',0).animate({
	        Counter: $(this).text()
	    }, {
	        duration: 4000,
	        easing: 'swing',
	        step: function (now) {
	            $(this).text(Math.ceil(now));
	        }
	    });
	});

});
</script>


<div class="">
            <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInRightBig">
                <!-- Trans label pie charts strats here-->
                <div class="palebluecolorbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 pull-left">
                                    <span>Registered Users</span>

                                    <div class="number" id="myTargetElement4">463</div>
                                </div>
                                <i class="livicon pull-right" data-name="users" data-l="true" data-c="#4ed5a2" data-hc="#4ed5a2" data-s="70" id="livicon-51" style="width: 70px; height: 70px;"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <small class="stat-label">Last Week</small>
                                    <h4 id="myTargetElement4.1">5</h4>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <small class="stat-label">Last Month</small>
                                    <h4 id="myTargetElement4.2">10</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
                <!-- Trans label pie charts strats here-->
                <div class="lightbluebg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 pull-left">
                                    <span>Total Guest</span>

                                    <div class="number" id="myTargetElement1">300</div>
                                </div>
                                <i class="livicon  pull-right" data-name="eye-open" data-l="true" data-c="#418bca" data-hc="#418bca" data-s="70" id="livicon-48" style="width: 70px; height: 70px;"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <small class="stat-label">Last Week</small>
                                    <h4 id="myTargetElement1.1">4</h4>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <small class="stat-label">Last Month</small>
                                    <h4 id="myTargetElement1.2">8</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInUpBig">
                <!-- Trans label pie charts strats here-->
                <div class="redbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 pull-left">
                                    <span>Total Host</span>

                                    <div class="number" id="myTargetElement2">120</div>
                                </div>
                                <i class="livicon pull-right" data-name="piggybank" data-l="true" data-c="#ff4961" data-hc="#ff4961" data-s="70" id="livicon-49" style="width: 70px; height: 70px;"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <small class="stat-label">Last Week</small>
                                    <h4 id="myTargetElement2.1">25</h4>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <small class="stat-label">Last Month</small>
                                    <h4 id="myTargetElement2.2">40</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-md-6 margin_10 animated fadeInDownBig">
                <!-- Trans label pie charts strats here-->
                <div class="goldbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 pull-left">
                                    <span>Total Books</span>

                                    <div class="number" id="myTargetElement3">50000</div>
                                </div>
                                <i class="livicon pull-right" data-name="archive-add" data-l="true" data-c="#f89a14" data-hc="#f89a14" data-s="70" id="livicon-50" style="width: 70px; height: 70px;"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <small class="stat-label">Last Week</small>
                                    <h4 id="myTargetElement3.1">42000</h4>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <small class="stat-label">Last Month</small>
                                    <h4 id="myTargetElement3.2">173929</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    
            </div>


        
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mrgn">
     <div class="panel-body squarebox square_boxs pull-up">
         <div id="regions_div" style="width: 100%; height: 500px;"></div>
      </div>
     </div>    



 <div class="col-md-12 col-sm-12 col-xs-12">
 <div class="panel-body squarebox square_boxs pull-up">
        <div id="piechart" style="width: 100%; height: 500px;"></div>
        </div>
     </div>


<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <div class="panel-body squarebox square_boxs pull-up">
         <div id="curve_chart" style="width: 100%; height: 500px"></div>
        </div>
     </div> 
<div class="row m-t">
            	<div class="col-md-6 col-sm-6 col-xs-6 mrgn">
            		<div class="panel-body squarebox square_boxs pull-up">
            			<div class="cardtable">
            				<div class="recentuser"> 
            				<h3> Recent users </h3> 
            				</div>
            					<div class=" ">
            						<div class="row">
            							<div class="table-responsive tblres" >
            								<table class="table table-striped themetable">
            									<tr>
            										<th>  </th> 
            										<th class="themeth" style="text-align:;"> Users</th>
            										<th class="themeth"> Last activity </th>
            									</tr>
            									<?php
            										for ($i=0; $i <=5 ; $i++) { 
            											
            										 ?>
            									<tr>
            										<td><i class="fa fa-arrow-circle-right rundcolr"></i></td>
            										
            										<td>
            						  <a class="previewImage">
                                        <img src="<?php echo asset('sdream/img/no-user.png'); ?>" border="0" width="20" class="img-circle space"></a>
            										First name</td>
            										  <td>2018-07-17 07:48:18</td>
            											</tr>
            										 <?php } ?>
            										</table>
            									</div>
            								</div>    
            							</div>
            						</div>
            					</div>  
                                </div>
            				</div>

     <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


<style type="text/css">
	.pull-up {
    -webkit-transition: all .25s ease;
    -o-transition: all .25s ease;
    -moz-transition: all .25s ease;
    transition: all .25s ease;
    border: none;
    -webkit-box-shadow: 0 1px 15px 1px rgba(62,57,107,.07);
    box-shadow: 0 1px 15px 1px rgba(62,57,107,.07);
}
.pull-up {
    -webkit-transition: all .25s ease;
    -o-transition: all .25s ease;
    -moz-transition: all .25s ease;
    transition: all .25s ease;
    border: none;
    -webkit-box-shadow: 0 1px 15px 1px rgba(62,57,107,.07);
    box-shadow: 0 1px 15px 1px rgba(62,57,107,.07);
}
.pull-up:hover {
    -webkit-transform: translateY(-1px) scale(1.02);
    -moz-transform: translateY(-1px) scale(1.02);
    -ms-transform: translateY(-1px) scale(1.02);
    -o-transform: translateY(-1px) scale(1.02);
    transform: translateY(-1px) scale(1.02);
    -webkit-box-shadow: 0 14px 24px rgba(62,57,107,.2);
    box-shadow: 0 14px 24px rgba(62,57,107,.2);
    z-index: 999;
}
 .page-content{background: #f3f3f2;}
 .square_box .number {
    font-size: 30px;
    float: left;
    clear: both;
    margin-top: 0;
    text-align: left;
    font-weight: 700;
}
.palebluecolorbg  .number {
    color: #4ed5a2;
}

.lightbluebg .number {
    color: #418bca;
}
.redbg .number {
    color: #ef6f6c;
}
.goldbg .number {
    color: #f89a14;
}
.square_box span {
    font-size: 12px;
    float: left;
}
.square_box span {
    font-family: 'Quicksand', sans-serif;
}
.squarebox h4 {
    font-size: 14px;
    padding: 0;
    margin: 0;
}

.no-radius .panel-body {
    padding: 20px 8px;
}
.nopadmar {
    margin: 0;
    padding: 0;
}
 .palebluecolorbg:hover, .lightbluebg:hover,  .redbg:hover,  .goldbg:hover {
    box-shadow: 0 0 11px rgba(33,33,33,.2);
}

/*.animated {
    -webkit-animation-duration: 1s;
    animation-duration: 1s;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
}
.animated {
    animation-name: none !important;
}*/
.palebluecolorbg {
    color: #6b6f82;
    background: #fff;
    transition: box-shadow .3s;
}
 .lightbluebg {
    background: #fff;
    color: #6b6f82;
}
.redbg {
    background: #fff;
    color: #6b6f82;
}
.goldbg {
    background: #fff;
    color: #6b6f82;
}


.cardtable {
    padding: 20px;
    border: none;
}
.recentuser {
    margin-bottom: 30px;
}
.recentuser h3 {
    color: #464855;
    line-height: 1.2;
    font-weight: 600;
    font-size: 20px;
}
.recentuser small {
    float: right;
    border-color: #FF394F!important;
    background-color: #FF4961!important;
    color: #FFF;
    padding: 8px 15px;
    border-radius: 10px;
    -webkit-box-shadow: 0 10px 18px 0 rgba(62,57,107,.2);
    box-shadow: 0 10px 18px 0 rgba(62,57,107,.2);
    font-size: 16px;
}
.tblres {
    background: #fff;
    border: none;
    overflow-x: auto;
    border-top: ;
}
.themetable > tbody > tr > th {
    color: #6B6F82;
    padding: 20px 10px;
    vertical-align: bottom;
    border-bottom: 2px solid #E3EBF3;
    font-size: 16px;
    font-weight:normal;
}
.space {
    margin-right: 20px;
}
.img-circle {
    border-radius: 50%;
    margin-top:0px;
}
.row {
    padding: 0 15px;
}
.rundcolr {
    color: #28D094!important;
    margin-right: 20px;
}
</style>
    <script type="text/javascript">
      google.charts.load('current', {
        'packages':['geochart'],
        // Note: you will need to get a mapsApiKey for your project.
        // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
        'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
      });
      google.charts.setOnLoadCallback(drawRegionsMap);

      function drawRegionsMap() {
        var data = google.visualization.arrayToDataTable([
          ['Country', 'Popularity'],
          ['Germany', 200],
          ['United States', 300],
          ['Brazil', 400],
          ['Canada', 500],
          ['France', 600],
          ['India', 700]
        ]);

        var options = {};

        var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

        chart.draw(data, options);
      }
    </script>

 

    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);

        var options = {
          title: 'My Daily Activities'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>


    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
        ]);

        var options = {
          title: 'Performance',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>
  
   
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>