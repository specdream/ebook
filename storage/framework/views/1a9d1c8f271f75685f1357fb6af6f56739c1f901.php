<?php echo $__env->make('admin.partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('admin.partials.topbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="clearfix"></div>
<div class="page-container">

    <?php echo $__env->make('admin.partials.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="page-content-wrapper">
        <div class="page-content">

        <!-- script and css for drag drop-->
        


            <h3 class="page-title">
            <?php if(str_replace('Controller','',explode("@",class_basename(app('request')->route()->getAction()['controller']))[0])!="Company"): ?>

            <?php  $con1=str_replace("_"," ",preg_replace('/([a-z0-9])?([A-Z])/','$1 $2',str_replace('Controller','',explode("@",class_basename(app('request')->route()->getAction()['controller']))[0])));
                    $con2=str_replace("Tb","",$con1);
                    $con3=str_replace("sdream","",$con2);
             ?>
                <?php echo e("Manage ".$con3); ?>

                <?php else: ?>
                Universities
                <?php endif; ?>
            </h3>

            <div class="row">
                <div class="col-md-12">

                <!-- Start import model popup -->

               <!--  <div id="import" class="modal fade" role="dialog">
                  <div class="modal-dialog">-->

                    <!-- Modal content-->
                    <!--<div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Upload Here</h4>
                      </div>
                      <div class="modal-body">
                        <div id="output"></div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                    </div>

                  </div>
                </div> -->

                <!-- End import model popup -->

                    <?php if(Session::has('message')): ?>
                        <div class="note note-info">
                            <p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>

                        </div>
                    <?php endif; ?>

                    <?php echo $__env->yieldContent('content'); ?>

                </div>
            </div>

        </div>
    </div>
</div>



<div class="scroll-to-top"
     style="display: none;">
    <i class="fa fa-arrow-up"></i>
</div>
<?php echo $__env->make('admin.partials.javascripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<link href="<?php echo e(asset('sdream/css/bootstrap.fd.css')); ?>" rel="stylesheet">
<script src="<?php echo e(asset('sdream/js/bootstrap.fd.js')); ?>"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script type="text/javascript">
        $(".import").click(function() {

            $.FileDialog({multiple: false,accept: ".csv"}).on('files.bs.filedialog', function(ev) {
                var files = ev.files;
                var text = "";
                files.forEach(function(f) {
                    text += f.name + "<br/>";
                });
                $("#output").html(text);
            }).on('cancel.bs.filedialog', function(ev) {
                $("#output").html("Cancelled!");
            });
        });
        </script>
<script type="text/javascript">
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
});
</script>        
        <script>tinymce.init({ selector:'.tinymce' });</script>
<?php echo $__env->yieldContent('javascript'); ?>
<?php echo $__env->make('admin.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


