  <footer>
    <div class="footer-container no_pad">
      <div class="col-lg-12 no_pad col-md-offset-1">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <h5><?php echo app('translator')->getFromJson('core.About_Us'); ?></h5>
          <ul class="nav nav-pills nav-stacked">
            <li><a href="#"><?php echo app('translator')->getFromJson('core.Check_All_Our_Courses'); ?></a></li>
            <li><a href="#"><?php echo app('translator')->getFromJson('core.Professional_Professors'); ?></a></li>
            <li><a href="#"><?php echo app('translator')->getFromJson('core.Our_Prices'); ?></a></li>
            <li><a href="#"><?php echo app('translator')->getFromJson('core.Learn_More_About_Us'); ?></a></li>
          </ul>
        </div> 
        <div class=" col-sm-6 col-xs-12 footer-width">
          <h5><?php echo app('translator')->getFromJson('core.Latest_Courses'); ?></h5>
          <ul class="nav nav-pills nav-stacked">
            <li><a href="#"><?php echo app('translator')->getFromJson('core.self-publish'); ?></a></li>
            <li><a href="#"><?php echo app('translator')->getFromJson('core.affiliates'); ?></a></li>
            <!-- <li class="green_free"><?php echo app('translator')->getFromJson('core.FREE'); ?></li> -->
            <li><a href="#"><?php echo app('translator')->getFromJson('core.job-openings'); ?></a></li>
            <!-- <li class="green_free"><?php echo app('translator')->getFromJson('core.FREE'); ?></li> -->

          </ul>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 ">
          <h5><?php echo app('translator')->getFromJson('core.Instagram'); ?></h5>
         <!--  <img src="<?php echo \URL::To('/'); ?>/sdream/img/footer.jpg"> -->
         <p>Stay up to date on the latest khawarizmm news, deals and events</p>
               
              <div class="footer_social">
                <a href=""><i class="fa fa-facebook-f"></i></a>
                <a href=""><i class="fa fa-twitter"></i></a>
                <a href=""><i class="fa fa-google-plus"></i></a>
                <a href=""><i class="fa fa-instagram"></i></a>
              </div>
            
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <h5><?php echo app('translator')->getFromJson('core.Contact_Details'); ?></h5>
          <ul class="nav nav-pills nav-stacked">
            <li><a href="<?php echo url('/') ?>/contact-us"><i class="fa fa-phone"></i> <?php echo app('translator')->getFromJson('core.contact-us'); ?></a></li>
            <li><a href="#"><i class="fa fa-map-marker"></i> <?php echo app('translator')->getFromJson('core.SMTC_online'); ?></a></li>
            <li><a href="tel:+8801612345678"><i class="fa fa-mobile-phone"></i> (+880) 1612345678</a></li>
            <li><a href="tel:+5553458554"><i class="fa fa-phone"></i> 555 345 - 8554</a></li>
            <li><a href="mailto:contact@iamniloy.com"><i class="fa fa-paper-plane"></i> contact@e-Book.com</a></li>
          </ul>

        </div> 
      </div>
    </div>
    <div class="col-xs-12 copyright_footer">
      <div class="footer-container no_pad">
        <div class="col-sm-6 col-md-5 col-xs-12 col-md-offset-1 copyright">CopyRight © 2018 e-Book.com</div>        
        <div class="col-sm-6 col-md-6 col-xs-12 text-right">
            <a href="<?php echo URL::To('/terms'); ?>"><?php echo app('translator')->getFromJson('core.Terms'); ?></a> | <a href="<?php echo URL::To('/privacy'); ?>"> <?php echo app('translator')->getFromJson('core.Privacy_Policy'); ?></a> | <a href="<?php echo URL::To('/refund'); ?>"><?php echo app('translator')->getFromJson('core.Refund_Policy'); ?></a>
        </div>
      </div>
    </div>
  </footer>
</body>
</html>

<style type="text/css">
  .footer-container{width: 100%;max-width: 1160px;margin: 0 auto;}

  .footer_social .fa-facebook-f{background-color:#3b5999;}
  .footer_social .fa-twitter{background-color:#55acee;}
  .footer_social .fa-google-plus{background-color:#dd4b39;}
  .footer_social .fa-instagram{background-color:#e4405f;}
 
</style>