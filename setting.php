<?php 
define('CNF_APPNAME','SDeBook');
define('CNF_APPDESC','SDeBook is e-book site and e-book clone script website');
define('CNF_COMNAME','SpecDream');
define('CNF_EMAIL','developerprasanna128@gmail.com');
define('CNF_METAKEY','');
define('CNF_METADESC','<p>E-Book Description</p>');
define('CNF_GROUP','3');
define('CNF_ACTIVATION','confirmation');
define('CNF_MULTILANG','1');
define('CNF_LANG','en');
define('CNF_REGIST','true');
define('CNF_FRONT','true');
define('CNF_RECAPTCHA','false');
define('CNF_THEME','specdream');
define('CNF_RECAPTCHAPUBLICKEY','');
define('CNF_RECAPTCHAPRIVATEKEY','');
define('CNF_MODE','production');
define('CNF_LOGO','logo.png');
define('CNF_FAV_ICON','favicon.ico');
define('CNF_ALLOWIP','');
define('CNF_RESTRICIP','192.116.134 , 194.111.606.21 ');
?>