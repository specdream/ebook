-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 20, 2018 at 09:24 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e-book`
--

-- --------------------------------------------------------

--
-- Table structure for table `sdream_cities`
--

CREATE TABLE `sdream_cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `sdream_states_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sdream_cities`
--

INSERT INTO `sdream_cities` (`id`, `sdream_states_id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Madurai', '2018-03-29 22:53:49', '2018-03-29 22:53:49', NULL),
(2, 1, 'Chennai', '2018-03-29 22:53:59', '2018-03-29 22:53:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sdream_countries`
--

CREATE TABLE `sdream_countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `sortname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonecode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sdream_countries`
--

INSERT INTO `sdream_countries` (`id`, `sortname`, `name`, `phonecode`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'IND', 'India', '+91', '2018-03-29 22:53:07', '2018-03-29 22:53:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sdream_states`
--

CREATE TABLE `sdream_states` (
  `id` int(10) UNSIGNED NOT NULL,
  `sdream_countries_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sdream_states`
--

INSERT INTO `sdream_states` (`id`, `sdream_countries_id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Tamil Nadu', '2018-03-29 22:53:26', '2018-03-29 22:53:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `accesscode`
--

CREATE TABLE `accesscode` (
  `id` int(150) NOT NULL,
  `course_id` int(150) NOT NULL,
  `user_to` int(150) NOT NULL,
  `code` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accesscode`
--

INSERT INTO `accesscode` (`id`, `course_id`, `user_to`, `code`) VALUES
(11, 22, 0, 'adf4c1874d5274f8ab211d'),
(12, 22, 0, 'f97452d63b3d34a454a317'),
(13, 22, 0, 'dfb1dff010f113bf7e6c90'),
(14, 22, 0, 'ea2088b0a0a63a8ed5bf2d'),
(15, 22, 0, 'd0e906f804ea6115eb0986'),
(16, 22, 0, 'cae971d660b8a12fcc4d0f'),
(17, 22, 0, 'ac2a6094b08dd21ffc480a'),
(18, 22, 0, 'ce3e8b3312c4ba6a17cc09'),
(19, 22, 0, 'af81ab0ec1028f79f0ce1a'),
(20, 22, 0, '9d8c74a4fc629e06bdeb39'),
(21, 23, 0, '2f47818e2ae58b968623b0'),
(22, 23, 0, '87e012a2fcdd9f61d399a3'),
(23, 23, 0, 'c645d80b7371f8fec9da71'),
(24, 23, 0, '1a9a6ac22150c2cc499e46'),
(25, 23, 0, 'badeb6daa80b4656f8368f'),
(26, 23, 0, 'b852f23c02d5930f955925'),
(27, 23, 0, '18c691556f59263b14eed8'),
(28, 23, 0, '38d0891fd63fffdb8e3820'),
(29, 23, 0, 'e910f872d81247f1fe93cd'),
(30, 23, 0, 'b5529e655708d099e94611'),
(31, 24, 0, '321a3b119966f91f8166c4'),
(32, 24, 0, '6b1dc34f3f68ec90152c1b'),
(33, 24, 0, 'd09dc5ec8a825c02abe8ea'),
(34, 24, 0, '069ad585e3063391cda2be'),
(35, 24, 0, 'cd4a08d02467310e41f311'),
(36, 24, 0, '505301199c3e843affaefa'),
(37, 24, 0, 'ec3ecade0dafa265b6c34d'),
(38, 24, 0, '8ad025ae032229f12afe1a'),
(39, 24, 0, '3e058b8561cb948d349e3b'),
(40, 24, 0, 'ecf747dd0b16b841544b3f'),
(41, 25, 0, '323b4731f3da8054340d81'),
(42, 25, 0, '4929c1ba740c087ece14f0'),
(43, 25, 0, 'd2b367eb03f0fe8a0c6bac'),
(44, 25, 0, '234bad5412032b8c13b75a'),
(45, 25, 0, 'c2a2639e163ed0146ae0fb'),
(46, 25, 0, 'fbdcef0c5e7d5e660a6bc1'),
(47, 25, 0, '745338641ef9e7bacf95a3'),
(48, 25, 0, '4d0f737577a618a6441357'),
(49, 25, 0, '5fa875eb982b15c3af1d2c'),
(50, 25, 0, '1fb5caf14f379c951d79a4'),
(51, 26, 0, '9c7a84073002a3131272e9'),
(52, 26, 0, 'c40abd97634445b9c5e540'),
(53, 26, 0, '186bfc09cb1ee0e84941de');

-- --------------------------------------------------------

--
-- Table structure for table `accesskey`
--

CREATE TABLE `accesskey` (
  `id` int(150) NOT NULL,
  `book_id` int(150) NOT NULL,
  `prefix` text NOT NULL,
  `file` text NOT NULL,
  `type` text NOT NULL,
  `c_row` int(2) NOT NULL DEFAULT '0',
  `status` int(5) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accesskey`
--

INSERT INTO `accesskey` (`id`, `book_id`, `prefix`, `file`, `type`, `c_row`, `status`, `created`) VALUES
(19, 1, 'DSLR12', '1531899389.csv', 'online', 2, 1, '2018-07-18 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `authorPlan`
--

CREATE TABLE `authorPlan` (
  `id` int(150) NOT NULL,
  `price` int(150) DEFAULT NULL,
  `title` text,
  `descp` text,
  `etcOpt` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authorPlan`
--

INSERT INTO `authorPlan` (`id`, `price`, `title`, `descp`, `etcOpt`) VALUES
(1, 5, 'test1', 'dasd sa dasd asd as asd asd asdasd', 'ads asd,asd asd||dasda sd asd asdasd||adas das dasd asd||teste'),
(2, 6, 'test22', 'dasd sa dasd asd as asd asd asdasd', 'ads asd,asd asd||dasda sd asd asdasd||adas das dasd asd||asdadsa\'asdasd\' asdasd||dadasdasdasdasdasdasd'),
(3, 4, 'test3', 'dasd sa dasd asd as asd asd asdasd', 'ads asd,asd asd||dasda sd asd asdasd||adas das dasd asd||asdadsa\'asdasd\' asdasd');

-- --------------------------------------------------------

--
-- Table structure for table `billingAddress`
--

CREATE TABLE `billingAddress` (
  `id` int(150) NOT NULL,
  `user_id` int(150) DEFAULT NULL,
  `address` text,
  `city` text,
  `state` text,
  `zip` text,
  `country` text,
  `phone` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `billingAddress`
--

INSERT INTO `billingAddress` (`id`, `user_id`, `address`, `city`, `state`, `zip`, `country`, `phone`) VALUES
(3, 8, 'dasdasd', 'asda', 'asdasd', '234', '1', '9876543210'),
(4, 12, 'dasdasd', 'asdad', 'asdasda', '6215', '1', '9876543210');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(150) NOT NULL,
  `user_id` int(150) NOT NULL,
  `key_cart` text NOT NULL,
  `product` int(150) NOT NULL,
  `type` int(5) NOT NULL DEFAULT '0',
  `college` int(2) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE `coupon` (
  `id` int(150) NOT NULL,
  `code` text NOT NULL,
  `expire` datetime NOT NULL,
  `price` int(150) NOT NULL,
  `type` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupon`
--

INSERT INTO `coupon` (`id`, `code`, `expire`, `price`, `type`, `status`) VALUES
(1, '4e877cb80311', '2018-05-31 00:00:00', 10, 'Discount', 1),
(2, '661e97e4a6a7', '2018-05-30 00:00:00', 100, 'price-reduces', 1),
(3, '73da615f9b15', '2018-06-21 00:00:00', 234, 'price-reduces', 1),
(4, 'edb0d20e449c', '2018-06-21 00:00:00', 444, 'discount', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `country` varchar(100) DEFAULT NULL,
  `country_code_2` varchar(100) DEFAULT NULL,
  `country_code_3` varchar(100) DEFAULT NULL,
  `currency` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `symbol` varchar(100) DEFAULT NULL,
  `value` varchar(1000) DEFAULT NULL,
  `status` int(5) NOT NULL DEFAULT '1',
  `base` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `country`, `country_code_2`, `country_code_3`, `currency`, `code`, `symbol`, `value`, `status`, `base`) VALUES
(1, 'Albania', 'AL', 'ALB', 'Leke', 'ALL', 'Lek', '108.11', 1, 0),
(2, 'America', 'US', 'USA', 'Dollars', 'USD', '$', '1', 1, 1),
(3, 'Afghanistan', 'AF', 'AFG', 'Afghanis', 'AFN', '?', '71.55', 1, 0),
(4, 'Argentina', 'AR', 'ARG', 'Pesos', 'ARS', '$', '27.02', 1, 0),
(5, 'Aruba', 'AW', 'ABW', 'Guilders', 'AWG', 'ƒ', '1.79', 1, 0),
(6, 'Australia', 'AU', 'AUS', 'Dollars', 'AUD', '$', '1.35', 1, 0),
(7, 'Azerbaijan', 'AZ', 'AZE', 'New Manats', 'AZN', '???', '1.70', 1, 0),
(8, 'Bahamas', 'BS', 'BHS', 'Dollars', 'BSD', '$', '1.00', 1, 0),
(9, 'Barbados', 'BB', 'BRB', 'Dollars', 'BBD', '$', '2.00', 1, 0),
(10, 'Belarus', 'BY', 'BLR', 'Rubles', 'BYR', 'p.', '1.99', 1, 0),
(11, 'Belgium', 'BE', 'BEL', 'Euro', 'EUR', '€', '0.86', 1, 0),
(12, 'Beliz', 'BZ', 'BLZ', 'Dollars', 'BZD', 'BZ$', '2.01', 1, 0),
(13, 'Bermuda', 'BM', 'BMU', 'Dollars', 'BMD', '$', '1.00', 1, 0),
(14, 'Bolivia', 'BO', 'BOL', 'Bolivianos', 'BOB', '$b', '6.90', 1, 0),
(15, 'Bosnia and Herzegovina', 'BA', 'BIH', 'Convertible Marka', 'BAM', 'KM', '1.68', 1, 0),
(16, 'Botswana', 'BW', 'BWA', 'Pula', 'BWP', 'P', '10.30', 1, 0),
(17, 'Bulgaria', 'BG', 'BGR', 'Leva', 'BGN', '??', '1.68', 1, 0),
(18, 'Brazil', 'BR', 'BRA', 'Reais', 'BRL', 'R$', '3.79', 1, 0),
(20, 'Brunei Darussalam', 'BN', 'BRN', 'Dollars', 'BND', '$', '1.51', 1, 0),
(21, 'Cambodia', 'KH', 'KHM', 'Riels', 'KHR', '?', '4067.75', 1, 0),
(22, 'Canada', 'CA', 'CAN', 'Dollars', 'CAD', '$', '1.33', 1, 0),
(23, 'Cayman Islands', 'KY', 'CYM', 'Dollars', 'KYD', '$', '0.83', 1, 0),
(24, 'Chile', 'CL', 'CHL', 'Pesos', 'CLP', '$', '638.30', 1, 0),
(25, 'China', 'CN', 'CHN', 'Yuan Renminbi', 'CNY', '¥', '6.54', 1, 0),
(26, 'Colombia', 'CO', 'COL', 'Pesos', 'COP', '$', '2948.05', 1, 0),
(27, 'Costa Rica', 'CR', 'CRI', 'Colón', 'CRC', '?', '566.38', 1, 0),
(28, 'Croatia', 'HR', 'HRV', 'Kuna', 'HRK', 'kn', '6.33', 1, 0),
(29, 'Cuba', 'CU', 'CUB', 'Pesos', 'CUP', '?', '1.00', 1, 0),
(30, 'Cyprus', 'CY', 'CYP', 'Euro', 'EUR', '€', '0.86', 1, 0),
(31, 'Czech Republic', 'CZ', 'CZE', 'Koruny', 'CZK', 'K?', '22.25', 1, 0),
(32, 'Denmark', 'DK', 'DNK', 'Kroner', 'DKK', 'kr', '6.40', 1, 0),
(33, 'Dominican Republic', 'DO', 'DOM', 'Pesos', 'DOP ', 'RD$', '49.54', 1, 0),
(35, 'Egypt', 'EG', 'EGY', 'Pounds', 'EGP', '£', '17.90', 1, 0),
(36, 'El Salvador', 'SV', 'SLV', 'Colones', 'SVC', '$', '1', 1, 0),
(39, 'Falkland Islands', 'FK', 'FLK', 'Pounds', 'FKP', '£', '1', 1, 0),
(40, 'Fiji', 'FJ', 'FJI', 'Dollars', 'FJD', '$', '2.08', 1, 0),
(41, 'France', 'FR', 'FRA', 'Euro', 'EUR', '€', '0.86', 1, 0),
(42, 'Ghana', 'GH', 'GHA', 'Cedis', 'GHC', '¢', '1', 1, 0),
(43, 'Gibraltar', 'GI', 'GIB', 'Pounds', 'GIP', '£', '0.75', 1, 0),
(44, 'Greece', 'GR', 'GRC', 'Euro', 'EUR', '€', '0.86', 1, 0),
(45, 'Guatemala', 'GT', 'GTM', 'Quetzales', 'GTQ', 'Q', '7.48', 1, 0),
(46, 'Guernsey', 'GG', 'GGY', 'Pounds', 'GGP', '£', '1', 1, 0),
(47, 'Guyana', 'GY', 'GUY', 'Dollars', 'GYD', '$', '208.84', 1, 0),
(49, 'Honduras', 'HN', 'HND', 'Lempiras', 'HNL', 'L', '23.89', 1, 0),
(50, 'Hong Kong', 'HK', 'HKG', 'Dollars', 'HKD', '$', '7.85', 1, 0),
(51, 'Hungary', 'HU', 'HUN', 'Forint', 'HUF', 'Ft', '279.48', 1, 0),
(52, 'Iceland', 'IS', 'ISL', 'Kronur', 'ISK', 'kr', '108.40', 1, 0),
(53, 'India', 'IN', 'IND', 'Rupees', 'INR', '&#8377', '68.16', 1, 0),
(54, 'Indonesia', 'ID', 'IDN', 'Rupiahs', 'IDR', 'Rp', '14162.80', 1, 0),
(55, 'Iran', 'IR', 'IRN', 'Rials', 'IRR', '?', '42626.50', 1, 0),
(56, 'Ireland', 'IE', 'IRL', 'Euro', 'EUR', '€', '0.86', 1, 0),
(57, 'Isle of Man', 'IM', 'IMN', 'Pounds', 'IMP', '£', '1', 1, 0),
(58, 'Israel', 'IL', 'ISR', 'New Shekels', 'ILS', '?', '3.62', 1, 0),
(59, 'Italy', 'IT', 'ITA', 'Euro', 'EUR', '€', '0.86', 1, 0),
(60, 'Jamaica', 'JM', 'JAM', 'Dollars', 'JMD', 'J$', '131.06', 1, 0),
(61, 'Japan', 'JP', 'JPN', 'Yen', 'JPY', '¥', '109.52', 1, 0),
(62, 'Jersey', 'JE', 'JEY', 'Pounds', 'JEP', '£', '1', 1, 0),
(63, 'Kazakhstan', 'KZ', 'KAZ', 'Tenge', 'KZT', '??', '339.50', 1, 0),
(64, 'North Korea', 'KP', 'PRK', 'Won', 'KPW', '?', '1', 1, 0),
(65, 'South Korea', 'KR', 'KOR', 'Won', 'KRW', '?', '1116.66', 1, 0),
(66, 'Kyrgyzstan', 'KG', 'KGZ', 'Soms', 'KGS', '??', '68.24', 1, 0),
(67, 'Laos', 'LA', 'LAO', 'Kips', 'LAK', '?', '8378.40', 1, 0),
(68, 'Latvia', 'LV', 'LVA', 'Lati', 'LVL', 'Ls', '1', 1, 0),
(69, 'Lebanon', 'LB', 'LBN', 'Pounds', 'LBP', '£', '1510.75', 1, 0),
(70, 'Liberia', 'LR', 'LBR', 'Dollars', 'LRD', '$', '146.92', 1, 0),
(71, 'Liechtenstein', 'LI', 'LIE', 'Switzerland Francs', 'CHF', 'CHF', '0.99', 1, 0),
(72, 'Lithuania', 'LT', 'LTU', 'Litai', 'LTL', 'Lt', '1', 1, 0),
(73, 'Luxembourg', 'LU', 'LUX', 'Euro', 'EUR', '€', '0.86', 1, 0),
(74, 'Macedonia', 'MK', 'MKD', 'Denars', 'MKD', '???', '52.90', 1, 0),
(75, 'Malaysia', 'MY', 'MYS', 'Ringgits', 'MYR', 'RM', '4.01', 1, 0),
(76, 'Malta', 'MT', 'MLT', 'Euro', 'EUR', '€', '0.86', 1, 0),
(77, 'Mauritius', 'MU', 'MUS', 'Rupees', 'MUR', '?', '34.92', 1, 0),
(78, 'Mexico', 'MX', 'MEX', 'Pesos', 'MXN', '$', '20.14', 1, 0),
(79, 'Mongolia', 'MN', 'MNG', 'Tugriks', 'MNT', '?', '1', 1, 0),
(80, 'Mozambique', 'MZ', 'MOZ', 'Meticais', 'MZN', 'MT', '59.30', 1, 0),
(81, 'Namibia', 'NA', 'NAM', 'Dollars', 'NAD', '$', '13.44', 1, 0),
(82, 'Nepal', 'NP', 'NPL', 'Rupees', 'NPR', '?', '108.26', 1, 0),
(83, 'Netherlands Antilles', 'AN', 'ANT', 'Guilders', 'ANG', 'ƒ', '1.81', 1, 0),
(84, 'Netherlands', 'NL', 'NLD', 'Euro', 'EUR', '€', '0.86', 1, 0),
(85, 'New Zealand', 'NZ', 'NZL', 'Dollars', 'NZD', '$', '1.45', 1, 0),
(86, 'Nicaragua', 'NI', 'NIC', 'Cordobas', 'NIO', 'C$', '31.63', 1, 0),
(87, 'Nigeria', 'NG', 'NGA', 'Nairas', 'NGN', '?', '360.00', 1, 0),
(88, 'North Korea', 'KP', 'PRK', 'Won', 'KPW', '?', '1', 1, 0),
(89, 'Norway', 'NO', 'NOR', 'Krone', 'NOK', 'kr', '8.14', 1, 0),
(90, 'Oman', 'OM', 'OMN', 'Rials', 'OMR', '?', '0.39', 1, 0),
(91, 'Pakistan', 'PK', 'PAK', 'Rupees', 'PKR', '?', '121.47', 1, 0),
(92, 'Panama', 'PA', 'PAN', 'Balboa', 'PAB', 'B/.', '1.00', 1, 0),
(93, 'Paraguay', 'PY', 'PRY', 'Guarani', 'PYG', 'Gs', '5688.50', 1, 0),
(94, 'Peru', 'PE', 'PER', 'Nuevos Soles', 'PEN', 'S/.', '3.27', 1, 0),
(95, 'Philippines', 'PH', 'PHL', 'Pesos', 'PHP', 'Php', '53.49', 1, 0),
(96, 'Poland', 'PL', 'POL', 'Zlotych', 'PLN', 'z?', '3.72', 1, 0),
(97, 'Qatar', 'QA', 'QAT', 'Rials', 'QAR', '?', '3.64', 1, 0),
(98, 'Romania', 'RO', 'ROU', 'New Lei', 'RON', 'lei', '4.01', 1, 0),
(99, 'Russia', 'RU', 'RUS', 'Rubles', 'RUB', '???', '62.94', 1, 0),
(100, 'Saint Helena', 'SH', 'SHN', 'Pounds', 'SHP', '£', '1', 1, 0),
(101, 'Saudi Arabia', 'SA', 'SAU', 'Riyals', 'SAR', '?', '3.75', 1, 0),
(102, 'Serbia', 'RS', 'SRB', 'Dinars', 'RSD', '???.', '101.44', 1, 0),
(103, 'Seychelles', 'SC', 'SYC', 'Rupees', 'SCR', '?', '13.54', 1, 0),
(104, 'Singapore', 'SG', 'SGP', 'Dollars', 'SGD', '$', '1.36', 1, 0),
(105, 'Slovenia', 'SI', 'SVN', 'Euro', 'EUR', '€', '0.86', 1, 0),
(106, 'Solomon Islands', 'SB', 'SLB', 'Dollars', 'SBD', '$', '7.88', 1, 0),
(107, 'Somalia', 'SO', 'SOM', 'Shillings', 'SOS', 'S', '581.00', 1, 0),
(108, 'South Africa', 'ZA', 'ZAF', 'Rand', 'ZAR', 'R', '13.56', 1, 0),
(109, 'South Korea', 'KR', 'KOR', 'Won', 'KRW', '?', '1116.66', 1, 0),
(110, 'Spain', 'ES', 'ESP', 'Euro', 'EUR', '€', '0.86', 1, 0),
(111, 'Sri Lanka', 'LK', 'LKA', 'Rupees', 'LKR', '?', '158.82', 1, 0),
(112, 'Sweden', 'SE', 'SWE', 'Kronor', 'SEK', 'kr', '8.90', 1, 0),
(113, 'Switzerland', 'CH', 'CHE', 'Francs', 'CHF', 'CHF', '0.99', 1, 0),
(114, 'Suriname', 'SR', 'SUR', 'Dollars', 'SRD', '$', '7.47', 1, 0),
(115, 'Syria', 'SY', 'SYR', 'Pounds', 'SYP', '£', '1', 1, 0),
(116, 'Taiwan', 'TW', 'TWN', 'New Dollars', 'TWD', 'NT$', '30.41', 1, 0),
(117, 'Thailand', 'TH', 'THA', 'Baht', 'THB', '?', '32.99', 1, 0),
(118, 'Trinidad and Tobago', 'TT', 'TTO', 'Dollars', 'TTD', 'TT$', '6.73', 1, 0),
(119, 'Turkey', 'TR', 'TUR', 'Lira', 'TRY', 'TL', '4.71', 1, 0),
(120, 'Turkey', 'TR', 'TUR', 'Liras', 'TRL', '£', '1', 1, 0),
(121, 'Tuvalu', 'TV', 'TUV', 'Dollars', 'TVD', '$', '1', 1, 0),
(122, 'Ukraine', 'UA', 'UKR', 'Hryvnia', 'UAH', '?', '26.19', 1, 0),
(123, 'United Kingdom', 'GB', 'GBR', 'Pounds', 'GBP', '£', '0.76', 1, 0),
(124, 'United States of America', 'US', 'USA', 'Dollars', 'USD', '$', '0.27', 1, 0),
(125, 'Uruguay', 'UY', 'URY', 'Pesos', 'UYU', '$U', '31.69', 1, 0),
(126, 'Uzbekistan', 'UZ', 'UZB', 'Sums', 'UZS', '??', '7871.10', 1, 0),
(127, 'Vatican', 'VA', 'VAT', 'Euro', 'EUR', '€', '0.86', 1, 0),
(128, 'Venezuela', 'VE', 'VEN', 'Bolivares Fuertes', 'VEF', 'Bs', '79900.00', 1, 0),
(129, 'Vietnam', 'VN', 'VNM', 'Dong', 'VND', '?', '22858.00', 1, 0),
(130, 'Yemen', 'YE', 'YEM', 'Rials', 'YER', '?', '250.30', 1, 0),
(131, 'Zimbabwe', 'ZW', 'ZWE', 'Zimbabwe Dollars', 'ZWD', 'Z$', '1', 1, 0),
(132, 'Arabic', 'AE', 'ARE', 'United Arab Emirates Dirham', 'AED', '?.?', '3.67', 1, 0),
(133, 'Arabic', 'AE', 'ARE', 'United Arab Emirates Dirham', 'AED', 'AED', '3.67', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `eb_book`
--

CREATE TABLE `eb_book` (
  `id` int(150) NOT NULL,
  `user_id` int(150) NOT NULL,
  `categorie` int(10) DEFAULT NULL,
  `sub_categorie` int(10) DEFAULT NULL,
  `title` varchar(150) NOT NULL,
  `summary` longtext CHARACTER SET utf8,
  `banner` text NOT NULL,
  `price_type` text NOT NULL,
  `price` int(10) NOT NULL,
  `currency` varchar(150) NOT NULL,
  `language` varchar(10) DEFAULT NULL,
  `isbn` text NOT NULL,
  `created_date` datetime NOT NULL,
  `published_date` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eb_book`
--

INSERT INTO `eb_book` (`id`, `user_id`, `categorie`, `sub_categorie`, `title`, `summary`, `banner`, `price_type`, `price`, `currency`, `language`, `isbn`, `created_date`, `published_date`, `status`) VALUES
(31, 1, NULL, NULL, 'asdasd123', '<p>asdasdasd</p>', '5b236a923cbd6.jpg', 'Paid', 100, 'INR', NULL, '123123', '2018-06-12 11:06:43', '2018-06-12 11:06:43', 1),
(32, 1, NULL, NULL, 'asdasd123', '<p>asdasdasd</p>', '', 'Paid', 100, 'INR', NULL, '123123', '2018-06-25 06:06:37', '2018-06-25 06:06:37', 1),
(33, 1, NULL, NULL, 'asdasd123', '<p>asdasdasd</p>', '5b308a1f125a7.jpg', 'Paid', 100, 'EUR', NULL, '123123', '2018-06-25 06:06:22', '2018-06-25 06:06:22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eb_book_files`
--

CREATE TABLE `eb_book_files` (
  `id` int(150) NOT NULL,
  `book_id` int(150) NOT NULL,
  `type` int(5) NOT NULL,
  `file` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eb_book_files`
--

INSERT INTO `eb_book_files` (`id`, `book_id`, `type`, `file`) VALUES
(1, 33, 1, '5b308a1f8d3c2.jpg'),
(2, 33, 1, '5b308a1f9d986.jpg'),
(3, 33, 1, '5b308a1fb1ff3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `eb_book_purchase`
--

CREATE TABLE `eb_book_purchase` (
  `id` int(150) NOT NULL,
  `uniq_id` text NOT NULL,
  `user_id` int(150) NOT NULL,
  `author_id` int(150) NOT NULL,
  `book_id` int(150) NOT NULL,
  `price` text NOT NULL,
  `address` text NOT NULL,
  `state` text NOT NULL,
  `city` text NOT NULL,
  `postal` text NOT NULL,
  `country` text NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eb_book_purchase`
--

INSERT INTO `eb_book_purchase` (`id`, `uniq_id`, `user_id`, `author_id`, `book_id`, `price`, `address`, `state`, `city`, `postal`, `country`, `created`) VALUES
(1, '5b3b1f5306aa1', 8, 8, 33, '100', 'dasdasd', 'asdasd', 'asda', '234', 'India', '2018-07-03 07:01:39'),
(2, '5b3b786466103', 8, 8, 33, '100', 'dasdasd', 'asdasd', 'asda', '234', 'India', '2018-07-03 13:21:40'),
(3, '5b3b786466103', 8, 8, 32, '100', 'dasdasd', 'asdasd', 'asda', '234', 'India', '2018-07-03 13:21:40'),
(4, '5b3c5dfb07e48', 8, 8, 33, '100', 'dasdasd', 'asdasd', 'asda', '234', 'India', '2018-07-04 05:41:15'),
(5, '5b3c76c240e8f', 8, 8, 33, '100', 'dasdasd', 'asdasd', 'asda', '234', 'India', '2018-07-04 07:26:58'),
(6, '5b3c9c5e6445e', 12, 8, 33, '100', 'dasdasd', 'asdasd', 'asda', '234', 'India', '2018-07-04 10:07:26'),
(7, '5b3db18fa8414', 12, 8, 32, '100', 'dasdasd', 'asdasda', 'asdad', 'sdfsdf', 'India', '2018-07-05 05:50:07'),
(8, '5b3f204e7342e', 12, 8, 33, '100', 'dasdasd', 'asdasda', 'asdad', 'sdfsdf', 'India', '2018-07-06 07:54:54'),
(9, '5b3f20a02fcfc', 12, 8, 33, '100', 'dasdasd', 'asdasda', 'asdad', '6215', 'India', '2018-07-06 07:56:16'),
(10, '5b3f20a02fcfc', 12, 8, 32, '100', 'dasdasd', 'asdasda', 'asdad', '6215', 'India', '2018-07-06 07:56:16');

-- --------------------------------------------------------

--
-- Table structure for table `eb_book_wish`
--

CREATE TABLE `eb_book_wish` (
  `id` int(150) NOT NULL,
  `book_id` int(150) NOT NULL,
  `user_id` int(150) NOT NULL,
  `college` int(2) NOT NULL DEFAULT '0',
  `created` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eb_book_wish`
--

INSERT INTO `eb_book_wish` (`id`, `book_id`, `user_id`, `college`, `created`) VALUES
(2, 33, 12, 0, '1531481235'),
(5, 33, 8, 0, '1531486945'),
(6, 31, 12, 0, '1531563622');

-- --------------------------------------------------------

--
-- Table structure for table `eb_college_book`
--

CREATE TABLE `eb_college_book` (
  `id` int(150) NOT NULL,
  `user_id` int(150) NOT NULL,
  `semester` int(10) DEFAULT NULL,
  `degree` int(10) DEFAULT NULL,
  `title` varchar(150) NOT NULL,
  `summary` longtext CHARACTER SET utf8,
  `banner` text NOT NULL,
  `price_type` text NOT NULL,
  `price` int(10) NOT NULL,
  `currency` varchar(150) NOT NULL,
  `isbn` text NOT NULL,
  `prefix` text,
  `created_date` datetime NOT NULL,
  `published_date` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eb_college_book`
--

INSERT INTO `eb_college_book` (`id`, `user_id`, `semester`, `degree`, `title`, `summary`, `banner`, `price_type`, `price`, `currency`, `isbn`, `prefix`, `created_date`, `published_date`, `status`) VALUES
(1, 13, 1, NULL, 'asdasdad', '<p>asdasdasd</p>', '5b45da437aace.jpg', 'Paid', 100, '', '123123', 'DSLR12', '2018-07-10 12:07:53', '2018-07-10 12:07:53', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eb_college_book_files`
--

CREATE TABLE `eb_college_book_files` (
  `id` int(150) NOT NULL,
  `book_id` int(150) NOT NULL,
  `type` int(5) NOT NULL,
  `file` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eb_college_book_files`
--

INSERT INTO `eb_college_book_files` (`id`, `book_id`, `type`, `file`) VALUES
(9, 1, 1, '5b45dc6768114.jpg'),
(10, 1, 1, '5b45dc6775700.jpg'),
(11, 1, 1, '5b45dc6781b50.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `eb_college_book_purchase`
--

CREATE TABLE `eb_college_book_purchase` (
  `id` int(150) NOT NULL,
  `uniq_id` text NOT NULL,
  `user_id` int(150) NOT NULL,
  `author_id` int(150) NOT NULL,
  `book_id` int(150) NOT NULL,
  `price` text NOT NULL,
  `access` text NOT NULL,
  `ISBN` text NOT NULL,
  `address` text NOT NULL,
  `state` text NOT NULL,
  `city` text NOT NULL,
  `postal` text NOT NULL,
  `country` text NOT NULL,
  `login` int(2) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eb_college_book_purchase`
--

INSERT INTO `eb_college_book_purchase` (`id`, `uniq_id`, `user_id`, `author_id`, `book_id`, `price`, `access`, `ISBN`, `address`, `state`, `city`, `postal`, `country`, `login`, `created`) VALUES
(11, '5b49e638f31da', 12, 3, 1, '100', '', '', 'dasdasd', 'asdasda', 'asdad', '6215', 'India', 0, '2018-07-14 12:02:00'),
(12, '5b4c4b07e1745', 12, 3, 1, '100', '', '', 'dasdasd', 'asdasda', 'asdad', '6215', 'India', 0, '2018-07-16 07:36:39'),
(13, '5b50288a9b9a3', 12, 3, 1, '100', 'DSLR12_VJHF1453', '123123', 'dasdasd', 'asdasda', 'asdad', '6215', 'India', 0, '2018-07-19 05:58:34');

-- --------------------------------------------------------

--
-- Table structure for table `eb_degrees`
--

CREATE TABLE `eb_degrees` (
  `id` int(150) NOT NULL,
  `name` text NOT NULL,
  `status` int(5) NOT NULL DEFAULT '1',
  `created` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eb_degrees`
--

INSERT INTO `eb_degrees` (`id`, `name`, `status`, `created`) VALUES
(2, 'ms test', 1, '1531215949');

-- --------------------------------------------------------

--
-- Table structure for table `eb_semesters`
--

CREATE TABLE `eb_semesters` (
  `id` int(150) NOT NULL,
  `name` text NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `start` text NOT NULL,
  `end` text NOT NULL,
  `created` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eb_semesters`
--

INSERT INTO `eb_semesters` (`id`, `name`, `status`, `start`, `end`, `created`) VALUES
(1, 'asd', 1, '', '', '1531208321'),
(2, 'asd', 1, '3', '6', '1531553290');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `position` int(11) DEFAULT NULL,
  `menu_type` int(11) NOT NULL DEFAULT '1',
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `position`, `menu_type`, `icon`, `name`, `title`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, NULL, 0, NULL, 'User', 'User', NULL, NULL, NULL),
(2, NULL, 0, NULL, 'Role', 'Role', NULL, NULL, NULL),
(3, 1, 1, 'fa fa-user', 'Profile', 'Profile', NULL, NULL, NULL),
(4, 2, 1, 'fa fa-building-o', 'Company', 'Universities', 20, NULL, '2018-02-27 07:15:12'),
(5, 3, 1, 'fa fa-user', 'Teacher', 'Teacher', 20, NULL, '2018-02-27 07:15:12'),
(6, 1, 1, NULL, 'Tb_Categories_Type', 'Course Category', 15, NULL, NULL),
(8, 1, 1, NULL, 'sdream_Countries', 'Countries', 16, NULL, NULL),
(9, 1, 1, NULL, 'sdream_States', 'States', 16, '2018-02-24 06:36:15', '2018-02-24 06:36:15'),
(10, 1, 1, NULL, 'sdream_Cities', 'Cities', 16, '2018-02-24 06:36:15', '2018-02-24 06:36:15'),
(11, 1, 1, NULL, 'Tb_Languages', 'Language View', 14, '2018-02-24 06:36:15', '2018-02-24 06:36:15'),
(12, 1, 1, NULL, 'Tb_Languages_Settings', 'Language Settings', 14, '2018-02-24 06:36:15', '2018-02-24 06:36:15'),
(13, 0, 1, 'fa fa-indent', 'Tb_Courses', 'Quiz Management', 15, '2018-02-26 02:10:29', '2018-05-26 01:26:57'),
(14, 3, 2, 'fa fa-language', 'Languages', 'Languages', NULL, '2018-02-27 07:12:17', '2018-03-29 22:58:44'),
(15, 4, 2, 'fa fa-book', 'Courses', 'Quiz', NULL, '2018-02-27 07:13:19', '2018-05-26 01:26:44'),
(16, 5, 2, 'fa fa-cogs', 'General_Settings', 'General Settings', NULL, '2018-02-27 07:14:15', '2018-03-29 22:58:45'),
(17, 0, 1, 'fa fa-users', 'Learners', 'Learners', 20, '2018-03-06 02:09:04', '2018-03-06 02:09:04'),
(18, 0, 1, 'fa fa-sitemap', 'Tb_Course_Assign', 'University Courses', 15, '2018-03-27 04:58:08', '2018-03-27 04:58:08'),
(20, 2, 2, 'fa-users', 'Manage_Users', 'Manage Users', NULL, '2018-03-29 22:55:26', '2018-03-29 22:58:44'),
(21, 6, 1, 'fa fa-ticket', 'Support_System', 'Support System', NULL, '2018-04-08 22:53:18', '2018-04-08 22:54:10'),
(22, 0, 2, 'fa-database', 'Project_Management', 'Project Management', NULL, '2018-05-02 05:15:11', '2018-05-02 05:15:11'),
(23, 0, 3, 'fa-database', 'Learner_Project_Management', 'Manage Projects', 22, '2018-05-02 05:17:18', '2018-05-02 05:17:41'),
(24, 0, 2, 'fa-cogs', 'Reports', 'Reports', NULL, '2018-05-11 01:26:19', '2018-05-11 01:26:19'),
(25, 0, 3, 'fa-book', 'Course_Report', 'Course Wise', 24, '2018-05-11 01:28:07', '2018-05-11 01:28:07'),
(26, 0, 2, 'fa-random', 'Coupon_Code', 'coupon code', NULL, '2018-05-16 05:57:04', '2018-05-16 05:57:04'),
(27, 0, 1, 'fa-database', 'Coupon', 'coupon', 26, '2018-05-16 06:01:26', '2018-05-16 06:01:26'),
(28, 0, 2, 'fa-book', 'Create_Book', 'Manage Quiz', NULL, '2018-05-18 04:58:40', '2018-05-26 01:25:50'),
(29, 0, 3, 'fa-database', 'AuthorBookCreate', 'View All Quiz', 28, '2018-05-18 05:07:08', '2018-05-26 01:26:13'),
(30, 0, 2, 'fa-database', 'Books', 'Books', NULL, '2018-05-26 01:41:14', '2018-05-26 01:41:14'),
(31, 0, 3, 'fa-database', 'AuthorBooks', 'View all', 30, '2018-05-26 01:42:29', '2018-05-26 01:42:29'),
(32, 0, 3, 'fa-database', 'BookLanguage', 'Book Languages', 33, '2018-05-26 05:24:09', '2018-05-29 05:28:59'),
(33, 0, 2, 'fa-database', 'Book_Property_Management', 'Book property management', NULL, '2018-05-29 05:28:34', '2018-05-29 05:28:34'),
(34, 0, 3, 'fa-database', 'AuthoBookCatg', 'Book categories', 33, '2018-05-29 05:34:26', '2018-05-29 05:34:26'),
(35, 0, 3, 'fa-database', 'AuthoBookSubCatg', 'Book sub-categories', 33, '2018-05-29 05:36:52', '2018-05-29 05:36:52'),
(36, 0, 2, 'fa-universal-access', 'Generate_Access_Key', 'Generate Access Key', NULL, '2018-06-05 23:45:42', '2018-06-05 23:45:42'),
(37, 0, 3, 'fa-database', 'GenerateAccessKey', 'Access Key Generate', 36, '2018-06-05 23:46:58', '2018-06-05 23:46:58'),
(38, 0, 2, 'fa-database', 'Manage_E_Book_Purchase', 'Manage e-book purchase', NULL, '2018-07-04 04:53:22', '2018-07-04 04:53:22'),
(39, 0, 3, 'fa-database', 'EbookPurchase', 'View list', 38, '2018-07-04 04:55:17', '2018-07-04 04:55:48'),
(40, 0, 2, 'fa-database', 'Purchase_History', 'Purchase History', NULL, '2018-07-04 06:28:15', '2018-07-04 06:28:15'),
(41, 0, 3, 'fa-database', 'PurchaseHistory', 'View list', 40, '2018-07-04 06:28:47', '2018-07-04 06:28:47'),
(42, 0, 2, 'fa-database', 'Manage_The_Access_Key', 'Manage the access key', NULL, '2018-07-04 23:23:20', '2018-07-04 23:23:20'),
(43, 0, 3, 'fa-database', 'ManageBookAccessKey', 'View list', 42, '2018-07-04 23:23:57', '2018-07-04 23:23:57'),
(44, 0, 2, 'fa-database', 'Manage_The_Author_Plans', 'Manage the author plans', NULL, '2018-07-08 23:31:28', '2018-07-08 23:31:28'),
(45, 0, 3, 'fa-database', 'AuthorPlans', 'View All', 44, '2018-07-08 23:32:02', '2018-07-08 23:32:02'),
(46, 0, 2, 'fa-database', 'Agreements_And_Contracts', 'Agreements and contracts', NULL, '2018-07-09 03:29:38', '2018-07-09 03:29:38'),
(47, 0, 3, 'fa-database', 'AuthorAgreements', 'View', 46, '2018-07-09 03:30:14', '2018-07-09 03:30:14'),
(48, 0, 2, 'fa-database', 'Manage_Semesters', 'Manage semesters', NULL, '2018-07-09 23:24:14', '2018-07-09 23:24:14'),
(49, 0, 3, 'fa-database', 'Semesters', 'View all', 48, '2018-07-09 23:24:34', '2018-07-09 23:24:34'),
(50, 0, 2, 'fa-database', 'Manage_College_Books', 'Manage college books', NULL, '2018-07-10 03:41:00', '2018-07-10 03:41:00'),
(51, 0, 3, 'fa-database', 'CollegeBooks', 'View all', 50, '2018-07-10 03:41:34', '2018-07-10 03:41:34'),
(52, 0, 2, 'fa-database', 'Manage_Degrees', 'Manage Degrees', NULL, '2018-07-10 04:08:16', '2018-07-10 04:08:16'),
(53, 0, 3, 'fa-database', 'Degrees', 'View all', 52, '2018-07-10 04:08:36', '2018-07-10 04:08:36'),
(54, 0, 2, 'fa-database', 'Manage_Offere\'s', 'Manage offere\'s', NULL, '2018-07-14 00:12:08', '2018-07-14 00:12:08'),
(55, 0, 3, 'fa-database', 'ManageOffere', 'View', 54, '2018-07-14 00:12:32', '2018-07-14 00:12:32'),
(56, 0, 2, 'fa-database', 'Set_Payout', 'Set Payout', NULL, '2018-07-16 00:45:39', '2018-07-16 00:45:39'),
(57, 0, 3, 'fa-database', 'AuthorPayout', 'Setting', 56, '2018-07-16 00:46:11', '2018-07-16 00:46:11'),
(58, 0, 2, 'fa-database', 'Manage_College_Book_Purchase', 'Manage College book purchase', NULL, '2018-07-16 03:47:28', '2018-07-16 03:47:28'),
(59, 0, 3, 'fa-database', 'ColgPurchaseHistory', 'View List', 58, '2018-07-16 03:48:30', '2018-07-16 03:48:30');

-- --------------------------------------------------------

--
-- Table structure for table `menu_role`
--

CREATE TABLE `menu_role` (
  `menu_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_role`
--

INSERT INTO `menu_role` (`menu_id`, `role_id`) VALUES
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(4, 1),
(5, 1),
(6, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(12, 4),
(13, 1),
(13, 4),
(14, 1),
(14, 4),
(15, 1),
(15, 4),
(16, 1),
(17, 1),
(18, 1),
(20, 1),
(21, 1),
(21, 3),
(21, 4),
(22, 1),
(22, 4),
(23, 1),
(23, 4),
(24, 1),
(24, 4),
(25, 1),
(25, 4),
(26, 1),
(27, 1),
(28, 5),
(29, 5),
(30, 5),
(31, 5),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 5),
(37, 5),
(38, 1),
(39, 1),
(40, 5),
(41, 5),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 5),
(47, 5),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 5),
(57, 5),
(58, 1),
(59, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_10_10_000000_create_menus_table', 1),
(4, '2015_10_10_000000_create_roles_table', 1),
(5, '2015_10_10_000000_update_users_table', 1),
(6, '2015_12_11_000000_create_users_logs_table', 1),
(7, '2016_03_14_000000_update_menus_table', 1),
(8, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(9, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(10, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(11, '2016_06_01_000004_create_oauth_clients_table', 2),
(12, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(13, '2018_02_20_123648_create_sdream_states_table', 3),
(14, '2018_02_20_123758_create_sdream_cities_table', 3),
(15, '2018_02_20_123847_create_tb_professional_table', 3),
(16, '2018_02_20_124417_create_tb_categories_type_table', 3),
(17, '2018_02_20_125033_create_tb_languages_table', 3),
(18, '2018_02_20_125312_create_tb_languages_settings_table', 3),
(19, '2018_02_24_111645_create_sdream_countries_table', 4),
(20, '2018_02_24_120615_create_xxx_table', 5),
(21, '2018_02_24_121401_create_xxxx_dddd_table', 6),
(22, '2018_02_26_074029_create_tb_courses_table', 7),
(23, '2018_03_28_073641_create_ccc_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(150) NOT NULL,
  `label` text NOT NULL,
  `type` int(2) NOT NULL,
  `offer` decimal(10,0) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `label`, `type`, `offer`, `status`) VALUES
(1, 'Entire semester', 1, '20', 1),
(2, 'Academic year', 2, '50', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `popular_book`
--

CREATE TABLE `popular_book` (
  `id` int(150) NOT NULL,
  `book_id` int(150) NOT NULL,
  `no_view` int(150) NOT NULL,
  `no_booking` int(150) NOT NULL,
  `no_review` int(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '2018-02-16 20:52:45', '2018-02-16 20:52:45'),
(2, 'Learners', '2018-02-16 20:52:45', '2018-03-06 03:05:47'),
(3, 'University', '2018-02-26 02:21:02', '2018-03-29 22:36:26'),
(4, 'Teacher', '2018-02-26 02:21:08', '2018-02-26 02:21:08'),
(5, 'Author', '2018-05-16 00:10:16', '2018-05-16 00:10:16'),
(6, 'Publishing House', '2018-05-16 00:10:30', '2018-05-16 00:10:30'),
(7, 'Research Center', '2018-05-16 00:10:40', '2018-05-16 00:10:40'),
(8, 'Member', '2018-05-16 00:22:53', '2018-05-16 00:22:53');

-- --------------------------------------------------------

--
-- Table structure for table `tb_book_categorie`
--

CREATE TABLE `tb_book_categorie` (
  `id` int(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `status` int(10) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_book_categorie`
--

INSERT INTO `tb_book_categorie` (`id`, `name`, `status`) VALUES
(1, 'Fixen', 1),
(2, 'catg1234', 1),
(5, 'catgqwe', 1),
(6, 'sem1', 1),
(7, 'sem2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_book_language`
--

CREATE TABLE `tb_book_language` (
  `id` int(150) NOT NULL,
  `value` varchar(10) NOT NULL,
  `short` varchar(10) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_book_language`
--

INSERT INTO `tb_book_language` (`id`, `value`, `short`, `status`) VALUES
(1, '', '', 2),
(4, 'spanish', 'sp', 1),
(5, 'french', 'fr', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_book_sub_categorie`
--

CREATE TABLE `tb_book_sub_categorie` (
  `id` int(150) NOT NULL,
  `cat_id` int(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `status` int(150) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_book_sub_categorie`
--

INSERT INTO `tb_book_sub_categorie` (`id`, `cat_id`, `name`, `status`) VALUES
(1, 1, 'fixen sub 1', 1),
(2, 1, 'fixen sub 2', 1),
(3, 2, 'catg1234 1', 1),
(4, 2, 'catg1234 2', 1),
(5, 5, 'catgqwe 1', 1),
(6, 5, 'catgqwe 2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_categories_type`
--

CREATE TABLE `tb_categories_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `icon` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_categories_type`
--

INSERT INTO `tb_categories_type` (`id`, `icon`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'fa fa-paper-plane', 'semester 1', 'active', '2018-02-26 01:46:42', '2018-05-25 00:50:35', NULL),
(2, 'fa fa-paper-plane', 'semester 2', 'active', '2018-03-22 01:04:38', '2018-05-25 00:50:29', NULL),
(3, 'fa fa-paper-plane', 'semester 3', 'active', '2018-03-22 01:04:47', '2018-05-25 00:50:25', NULL),
(4, 'fa fa-paper-plane', 'Video Editing', 'active', '2018-03-22 01:04:56', '2018-05-25 00:50:40', '2018-05-25 00:50:40'),
(5, 'fa fa-paper-plane', 'Digital Media', 'active', '2018-03-22 01:05:02', '2018-05-25 00:50:42', '2018-05-25 00:50:42'),
(6, 'fa fa-paper-plane', 'Media technologies', 'active', '2018-03-22 01:05:08', '2018-05-25 00:50:44', '2018-05-25 00:50:44');

-- --------------------------------------------------------

--
-- Table structure for table `tb_courses`
--

CREATE TABLE `tb_courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` enum('Draft','Pending','Accept','Reject') COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(10) NOT NULL,
  `teacher_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `book_id` int(150) NOT NULL,
  `prof_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promo_video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assessment_test` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  `price_type` enum('Free','Paid') COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `added_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `published_date` date DEFAULT NULL,
  `keyGen_status` int(5) NOT NULL DEFAULT '0',
  `book_for` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_courses`
--

INSERT INTO `tb_courses` (`id`, `status`, `quantity`, `teacher_id`, `book_id`, `prof_id`, `cat_id`, `title`, `summary`, `image`, `promo_video`, `assessment_test`, `price_type`, `price`, `added_by`, `created_at`, `updated_at`, `deleted_at`, `published_date`, `keyGen_status`, `book_for`) VALUES
(22, 'Accept', 12, '3', 0, 0, 1, 'test test', 'printer', '1526552942-Inconnet contract management.odt', NULL, 'yes', 'Free', '0', 1, '2018-05-17 04:59:02', '2018-05-17 04:59:02', NULL, NULL, 0, NULL),
(23, 'Accept', 23, '8', 0, 8, 0, 'dasd', 'dasdasd', '1526715282-Inconnet_doc', NULL, 'yes', 'Free', '0', 8, '2018-05-19 02:04:42', '2018-05-19 02:04:42', NULL, NULL, 0, 'public'),
(24, 'Accept', 12, '8', 0, 8, 0, 'dasd', 'dasdasd', '1526715396-Inconnet_doc', NULL, 'yes', 'Free', '0', 8, '2018-05-19 02:06:36', '2018-05-19 02:06:36', NULL, NULL, 0, 'public'),
(25, 'Accept', 12, '3', 0, 0, 1, 'dads', 'printer', '1527252408-bookDetailPage.png', NULL, 'yes', 'Free', '0', 1, '2018-05-25 07:16:48', '2018-05-25 07:16:48', NULL, NULL, 0, 'college'),
(26, 'Pending', 3, '13', 0, 0, 1, 'Sfsa', 'asfdasdf', '1531981954-burrito-chicken-close-up-461198.jpg', NULL, 'yes', 'Free', '0', 13, '2018-07-19 01:02:34', '2018-07-19 01:02:34', NULL, NULL, 1, 'college'),
(27, 'Pending', 0, '13', 0, 0, 1, 'asdasd', 'asdasd', '1531983367-burrito-chicken-close-up-461198.jpg', NULL, 'yes', 'Free', '0', 13, '2018-07-19 01:26:07', '2018-07-19 01:26:07', NULL, NULL, 0, NULL),
(28, 'Pending', 0, '13', 0, 0, 0, 'sdfdf', 'sdfsdf', '1531994408-2018-50mm-analogue-1027148.jpg', NULL, 'yes', 'Free', '0', 13, '2018-07-19 04:30:08', '2018-07-19 04:30:08', NULL, NULL, 0, NULL),
(29, 'Pending', 0, '13', 1, 0, 0, 'asdasddas', 'dasdasda', '1531994510-no-image.jpeg', NULL, 'yes', 'Free', '0', 13, '2018-07-19 04:31:50', '2018-07-19 04:31:50', NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_courses_new1`
--

CREATE TABLE `tb_courses_new1` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` enum('Draft','Pending','Accept','Reject') COLLATE utf8mb4_unicode_ci NOT NULL,
  `teacher_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prof_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promo_video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assessment_test` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  `price_type` enum('Free','Paid') COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `added_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `published_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_courses_new1`
--

INSERT INTO `tb_courses_new1` (`id`, `status`, `teacher_id`, `prof_id`, `cat_id`, `title`, `summary`, `image`, `promo_video`, `assessment_test`, `price_type`, `price`, `added_by`, `created_at`, `updated_at`, `deleted_at`, `published_date`) VALUES
(1, 'Pending', '3', 0, 5, 'course_1', 'course_summary_1', '1522385787-course1.jpg', NULL, 'yes', 'Paid', '100', 1, '2018-03-29 23:26:27', '2018-05-10 11:59:18', '2018-05-10 11:59:18', '2018-03-30'),
(2, 'Pending', '3', 0, 5, 'course_1', 'course_summary_1', '1522385787-course1.jpg', NULL, 'yes', 'Paid', '100', 1, '2018-03-29 23:26:27', '2018-05-10 11:59:18', '2018-05-10 11:59:18', '2018-03-30'),
(3, 'Draft', '3', 0, 5, 'course_1', 'course_summary_1', '1522385787-course1.jpg', NULL, 'yes', 'Paid', '100', 1, '2018-03-29 23:26:27', '2018-05-10 11:59:18', '2018-05-10 11:59:18', '2018-03-30'),
(4, 'Pending', '3', 0, 2, 'course_6', 'course_summary_6', '1522468878-course2.jpg', NULL, 'no', 'Paid', '100', 1, '2018-03-30 22:31:18', '2018-05-10 11:59:18', '2018-05-10 11:59:18', NULL),
(9, 'Pending', '3', 0, 1, 'course_8', 'course_summary_8', '1522471834-course3.jpg', NULL, 'yes', 'Free', '0', 1, '2018-03-30 23:20:34', '2018-05-10 11:59:18', '2018-05-10 11:59:18', NULL),
(10, 'Pending', '3', 0, 1, 'course_7', 'course_summary_7', '1522472135-flim1.jpg', NULL, 'yes', 'Free', '0', 1, '2018-03-30 23:25:35', '2018-05-10 11:59:18', '2018-05-10 11:59:18', NULL),
(11, 'Pending', '3', 0, 2, 'course_17', 'course_summary_17', '1522929582-hanuman.jpg', NULL, 'yes', 'Paid', '100', 1, '2018-04-05 06:29:42', '2018-05-10 11:59:18', '2018-05-10 11:59:18', NULL),
(12, 'Pending', '3', 0, 2, 'course_171', 'course_summary_171', '1522929646-images.jpeg', NULL, 'yes', 'Paid', '100', 1, '2018-04-05 06:30:46', '2018-05-10 11:59:18', '2018-05-10 11:59:18', NULL),
(13, 'Accept', '3', 0, 2, 'comp_science_engg', 'comp_science_engg_summary', '1525943141-pexels-photo-935756.jpeg', NULL, 'yes', 'Free', '0', 1, '2018-05-10 12:18:18', '2018-05-10 18:33:19', NULL, '2018-05-10');

-- --------------------------------------------------------

--
-- Table structure for table `tb_course_assign`
--

CREATE TABLE `tb_course_assign` (
  `id` int(11) NOT NULL,
  `univ_id` int(11) NOT NULL,
  `c_id` int(11) NOT NULL,
  `s_limit` int(11) NOT NULL,
  `s_date` date NOT NULL,
  `e_date` date NOT NULL,
  `c_key` varchar(500) NOT NULL,
  `status` enum('active') NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_course_assign`
--

INSERT INTO `tb_course_assign` (`id`, `univ_id`, `c_id`, `s_limit`, `s_date`, `e_date`, `c_key`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 1, 200, '2018-03-30', '2018-10-25', 'f7rgJEzhEO', 'active', '2018-03-30 07:44:50', '2018-03-30 07:44:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_course_reject`
--

CREATE TABLE `tb_course_reject` (
  `id` int(11) NOT NULL,
  `c_id` int(11) NOT NULL,
  `reason` mediumtext NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_languages`
--

CREATE TABLE `tb_languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_languages`
--

INSERT INTO `tb_languages` (`id`, `name`, `short_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'English', 'en', '2018-02-27 07:25:42', '2018-02-27 07:25:42', NULL),
(2, 'Hindi', 'ar', '2018-03-30 06:20:34', '2018-03-30 06:20:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_languages_settings`
--

CREATE TABLE `tb_languages_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `tb_languages_id` int(11) NOT NULL,
  `key_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `uniq_record` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `added_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_languages_settings`
--

INSERT INTO `tb_languages_settings` (`id`, `tb_languages_id`, `key_name`, `value`, `uniq_record`, `created_at`, `updated_at`, `deleted_at`, `added_by`) VALUES
(1, 1, 'course_1', 'The Complete Video Production Bootcamp', NULL, '2018-03-30 05:27:12', '2018-03-30 05:28:06', NULL, 1),
(2, 1, 'course_summary_1', '<div class=\"description__title\">1111Description</div>\r\n\r\n<p><strong><em>This online video production course will teach you how to make amazing videos, whether you use a smartphone, webcam, DSLR, mirrorless, or professional camera.&nbsp;</em></strong></p>\r\n\r\n<p>Whether you&#39;re a YouTuber, blogger, vlogger, business owner, aspiring filmmaker, or just someone who wants to create videos, you will learn how to make professional videos with this course.</p>\r\n\r\n<p><strong>Master Video Production Techniques to Create Amazing Videos that Boost Your Views, Revenue and Drive Traffic To Your Business</strong></p>\r\n\r\n<p>While there are plenty of tutorials and courses that focus on specific cameras or styles, it&#39;s hard to find a comprehensive course like this one, which covers everything from coming up with great video ideas, executing them in production and post-production, and distributing them to a wide audience online.</p>\r\n\r\n<p><strong>COURSE&nbsp;BONUSES:</strong></p>\r\n\r\n<ul>\r\n	<li>Direct feedback from instructors on any of your video projects</li>\r\n	<li>Downloadable guides that will help you in every section of the course</li>\r\n	<li>Case studies that break down&nbsp;real world film projects</li>\r\n</ul>\r\n\r\n<p>This video course is designed for all levels of video makers who want to improve their skills, create stellar videos, and even make money with their videos.</p>\r\n\r\n<p><strong>Key things you will learn:</strong></p>\r\n\r\n<ul>\r\n	<li>Come up with great video topics that people will love watching and sharing</li>\r\n	<li>Master shooting your video in manual mode on a DSLR, mirrorless, or professional cinema camera</li>\r\n	<li>Expose and compose better shots that tell your story</li>\r\n	<li>Shoot amazing videos with smartphones, webcams, or even screencasts</li>\r\n	<li>Aerial videography with DJI drones (coming soon)</li>\r\n	<li>Light your videos with professional techniques, on a small budget</li>\r\n	<li>Record crisp and clean audio with different microphones in any environment</li>\r\n	<li>Edit videos to make them more engaging</li>\r\n	<li>Know what equipment to purchase to create great videos</li>\r\n	<li>Export the best quality videos for online viewing</li>\r\n	<li>Get more views, likes, and shares on social media</li>\r\n	<li>Grow a YouTube channel that ultimately brings in revenue</li>\r\n</ul>\r\n\r\n<p><strong>Make Professional Videos on Any Budget</strong></p>\r\n\r\n<p>Regardless of what your level of experience is or what type of camera you use, this in-depth course is designed to provide you with everything you need to take your video production skills to the next level.</p>\r\n\r\n<p>Unlike other video courses or tutorials that are more limited in scope, this complete course teaches you the entire process.</p>\r\n\r\n<p><strong>Contents and Overview</strong></p>\r\n\r\n<p>This video course starts from the beginning, which is about coming up with great video ideas. You&#39;ll learn what makes a video great, and how to come up with video topics for your target audience. You&#39;ll walk through the pre-production process to ensure a smooth video shoot.</p>\r\n\r\n<p>Before diving into how to shoot videos, we&#39;ll cover our recommended equipment. We share our favorite equipment for any budget - including cameras, audio gear, lighting kits, and editing applications.</p>\r\n\r\n<p>You&#39;ll learn cinematography basics such as how to expose your video, how to compose great shots, how to film yourself, how to get great focus, and how to stabilize your shots. We cover how to do this for DSLR, mirrorless, smartphone, and webcam cameras. We even have a section on aerial drone videography!</p>\r\n\r\n<p>You&#39;ll learn how to record great audio. First, we cover the different types of microphones, and how to choose the right microphone for your video project. Then you learn how to use the different types of microphones. Plus, you&#39;ll learn how to record audio in any environment, including getting rid of echo.</p>\r\n\r\n<p>Lighting is one of the most important parts of video production, whether you&#39;re using a smartphone, webcam, or DSLR or mirrorless camera. You&#39;ll learn how to use free and inexpensive lighting techniques, and how to set up a lighting kit like the pros using the three-point lighting system.</p>\r\n\r\n<p>Once you understand everything about shooting your video, you&#39;ll learn how to use editing to make your videos even better and more engaging. You&#39;ll learn how to find free music for your videos, how to design better titles, and how to use calls to action to increase engagement and conversion.</p>\r\n\r\n<p>After all this, you&#39;ll learn how to better share your videos with the world. Learn how to choose the right platform for your video content. Get more views, likes, and shares with our tips for sharing on social media. And learn how to grow a YouTube channel with our best practices.</p>\r\n\r\n<p><strong>If you want to make better videos, this is the course for you.</strong></p>\r\n\r\n<p>Remember, there is a 30-day 100% money-back guarantee. There is no reason to hesitate. Enroll now, see if you enjoy the course, and start making better videos today!</p>\r\n\r\n<p>Cheers,</p>\r\n\r\n<p>Phil, Will, and Sam</p>', NULL, '2018-03-30 05:28:58', '2018-03-30 05:28:58', NULL, 1),
(3, 2, 'course_summary_1', '<div class=\"description__title\">dfsdfdfsdfDescription</div>\r\n\r\n<p><strong><em>This online video production course will teach you how to make amazing videos, whether you use a smartphone, webcam, DSLR, mirrorless, or professional camera.&nbsp;</em></strong></p>\r\n\r\n<p>Whether you&#39;re a YouTuber, blogger, vlogger, business owner, aspiring filmmaker, or just someone who wants to create videos, you will learn how to make professional videos with this course.</p>\r\n\r\n<p><strong>Master Video Production Techniques to Create Amazing Videos that Boost Your Views, Revenue and Drive Traffic To Your Business</strong></p>\r\n\r\n<p>While there are plenty of tutorials and courses that focus on specific cameras or styles, it&#39;s hard to find a comprehensive course like this one, which covers everything from coming up with great video ideas, executing them in production and post-production, and distributing them to a wide audience online.</p>\r\n\r\n<p><strong>COURSE&nbsp;BONUSES:</strong></p>\r\n\r\n<ul>\r\n	<li>Direct feedback from instructors on any of your video projects</li>\r\n	<li>Downloadable guides that will help you in every section of the course</li>\r\n	<li>Case studies that break down&nbsp;real world film projects</li>\r\n</ul>\r\n\r\n<p>This video course is designed for all levels of video makers who want to improve their skills, create stellar videos, and even make money with their videos.</p>\r\n\r\n<p><strong>Key things you will learn:</strong></p>\r\n\r\n<ul>\r\n	<li>Come up with great video topics that people will love watching and sharing</li>\r\n	<li>Master shooting your video in manual mode on a DSLR, mirrorless, or professional cinema camera</li>\r\n	<li>Expose and compose better shots that tell your story</li>\r\n	<li>Shoot amazing videos with smartphones, webcams, or even screencasts</li>\r\n	<li>Aerial videography with DJI drones (coming soon)</li>\r\n	<li>Light your videos with professional techniques, on a small budget</li>\r\n	<li>Record crisp and clean audio with different microphones in any environment</li>\r\n	<li>Edit videos to make them more engaging</li>\r\n	<li>Know what equipment to purchase to create great videos</li>\r\n	<li>Export the best quality videos for online viewing</li>\r\n	<li>Get more views, likes, and shares on social media</li>\r\n	<li>Grow a YouTube channel that ultimately brings in revenue</li>\r\n</ul>\r\n\r\n<p><strong>Make Professional Videos on Any Budget</strong></p>\r\n\r\n<p>Regardless of what your level of experience is or what type of camera you use, this in-depth course is designed to provide you with everything you need to take your video production skills to the next level.</p>\r\n\r\n<p>Unlike other video courses or tutorials that are more limited in scope, this complete course teaches you the entire process.</p>\r\n\r\n<p><strong>Contents and Overview</strong></p>\r\n\r\n<p>This video course starts from the beginning, which is about coming up with great video ideas. You&#39;ll learn what makes a video great, and how to come up with video topics for your target audience. You&#39;ll walk through the pre-production process to ensure a smooth video shoot.</p>\r\n\r\n<p>Before diving into how to shoot videos, we&#39;ll cover our recommended equipment. We share our favorite equipment for any budget - including cameras, audio gear, lighting kits, and editing applications.</p>\r\n\r\n<p>You&#39;ll learn cinematography basics such as how to expose your video, how to compose great shots, how to film yourself, how to get great focus, and how to stabilize your shots. We cover how to do this for DSLR, mirrorless, smartphone, and webcam cameras. We even have a section on aerial drone videography!</p>\r\n\r\n<p>You&#39;ll learn how to record great audio. First, we cover the different types of microphones, and how to choose the right microphone for your video project. Then you learn how to use the different types of microphones. Plus, you&#39;ll learn how to record audio in any environment, including getting rid of echo.</p>\r\n\r\n<p>Lighting is one of the most important parts of video production, whether you&#39;re using a smartphone, webcam, or DSLR or mirrorless camera. You&#39;ll learn how to use free and inexpensive lighting techniques, and how to set up a lighting kit like the pros using the three-point lighting system.</p>\r\n\r\n<p>Once you understand everything about shooting your video, you&#39;ll learn how to use editing to make your videos even better and more engaging. You&#39;ll learn how to find free music for your videos, how to design better titles, and how to use calls to action to increase engagement and conversion.</p>\r\n\r\n<p>After all this, you&#39;ll learn how to better share your videos with the world. Learn how to choose the right platform for your video content. Get more views, likes, and shares with our tips for sharing on social media. And learn how to grow a YouTube channel with our best practices.</p>\r\n\r\n<p><strong>If you want to make better videos, this is the course for you.</strong></p>\r\n\r\n<p>Remember, there is a 30-day 100% money-back guarantee. There is no reason to hesitate. Enroll now, see if you enjoy the course, and start making better videos today!</p>\r\n\r\n<p>Cheers,</p>\r\n\r\n<p>Phil, Will, and Sam</p>', NULL, '2018-03-30 05:28:58', '2018-03-30 05:28:58', NULL, 1),
(4, 1, 'lession_1', 'What is PHP?', NULL, '2018-03-30 06:23:07', '2018-03-30 06:23:07', NULL, 1),
(5, 1, 'lession_des_1', '<ul>\r\n	<li>PHP is an acronym for &quot;PHP: Hypertext Preprocessor&quot;</li>\r\n	<li>PHP is a widely-used, open source scripting language</li>\r\n	<li>PHP scripts are executed on the server</li>\r\n	<li>PHP is free to download and use</li>\r\n</ul>', NULL, '2018-03-30 06:23:26', '2018-03-30 06:23:26', NULL, 1),
(6, 1, 'lesson1_question_1', '<p>1. What does PHP stand for?</p>\r\n\r\n<div>i) Personal Home Page</div>\r\n\r\n<div>ii) Hypertext Preprocessor</div>\r\n\r\n<div>iii) Pretext Hypertext Processor</div>\r\n\r\n<div>iv) Preprocessor Home Page</div>', NULL, '2018-03-30 06:25:34', '2018-03-30 06:25:34', NULL, 1),
(7, 1, 'lesson1_question1_answer1', 'Both (i) and (ii)', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(8, 1, 'lesson1_question1_answer2', 'Both (ii) and (iv)', NULL, '2018-03-30 06:27:51', '2018-03-30 06:27:51', NULL, 1),
(9, 1, 'lesson1_question1_answer3', 'Only (ii)', NULL, '2018-03-30 06:28:15', '2018-03-30 06:28:15', NULL, 1),
(10, 1, 'lesson1_question1_answer3', 'Both (i) and (iii)', NULL, '2018-03-30 06:31:35', '2018-03-30 06:31:35', NULL, 1),
(11, 1, 'course_summary_1', '<p>course_summary_1</p>', NULL, '2018-03-30 06:31:50', '2018-03-30 06:31:50', NULL, 1),
(12, 1, 'lesson1_question_2', 'PHP’s numerically indexed array begin with position', NULL, '2018-03-30 06:25:34', '2018-03-30 06:25:34', NULL, 1),
(13, 1, 'lesson1_question_3', 'Which of the functions is used to sort an array in descending order?', NULL, '2018-03-30 06:25:34', '2018-03-30 06:25:34', NULL, 1),
(14, 1, 'lesson1_question_4', ' Which of the following are correct ways of creating an array?', NULL, '2018-03-30 06:25:34', '2018-03-30 06:25:34', NULL, 1),
(15, 1, 'lesson1_question_5', 'What will be the output of the following php code? ', NULL, '2018-03-30 06:25:34', '2018-03-30 06:25:34', NULL, 1),
(16, 1, 'lesson1_question_6', 'Which function will return true if a variable is an array or false if it is not? ', NULL, '2018-03-30 06:25:34', '2018-03-30 06:25:34', NULL, 1),
(17, 1, 'lesson1_question_7', 'Which in-built function will add a value to the end of an array? ', NULL, '2018-03-30 06:25:34', '2018-03-30 06:25:34', NULL, 1),
(18, 1, 'lesson1_question_8', 'What will be the output of the following PHP code? ', NULL, '2018-03-30 06:25:34', '2018-03-30 06:25:34', NULL, 1),
(19, 1, 'lesson1_question2_answer1', 'Both (i) and (ii)', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(20, 1, 'lesson1_question2_answer2', '(i)', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(21, 1, 'lesson1_question3_answer1 	', '(i)', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(22, 1, 'lesson1_question3_answer2', '4', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(23, 1, 'lesson1_question4_answer1', '5', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(24, 1, 'lesson1_question4_answer2', '8', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(25, 1, 'lesson1_question5_answer1', '45', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(26, 1, 'lesson1_question5_answer2', '(II)', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(27, 1, 'lesson1_question6_answer1', '5', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(28, 1, 'lesson1_question6_answer2', '586', NULL, '2018-03-30 06:25:34', '2018-03-30 06:25:34', NULL, 1),
(29, 1, 'computer_networks', '<pre>\r\ncomputer networks\r\nThis module defines the network as a group of computers connected by cards, cables and specialized software to share resources. The network has many benefits, the most important of which are the sharing of files, applications and storage media, the support of central data management and the provision of expenses.\r\nNetworks are classified according to their geographic reach to two main types: the LAN, which covers a limited area of ​​one or several adjacent buildings, and the Wide Area Network (WAN) covering a wide geographical area within or outside a country. User connectivity, custom cost, and other considerations.</pre>\r\n\r\n<pre>\r\n\r\n&nbsp;</pre>\r\n\r\n<p>&nbsp;</p>', NULL, '2018-05-10 17:16:07', '2018-05-10 17:35:17', '2018-05-10 17:35:17', 1),
(30, 3, 'computer_networks', '<p>شبكات الحاسوب<br />\r\nتعرِّف هذه الوحدة الشبكة بأنها مجموعة حواسيب متصلة ببعضها عن طريق بطاقات وكابلات وبرمجيات متخصصة بهدف المشاركة في الموارد، ولإنشاء الشبكات فوائد متعددة من أهمها مشاركة الملفات والتطبيقات ووسائط التخزين، ودعم الإدارة المركزية للبيانات، وتوفير المصروفات.<br />\r\nوتصنَّف الشبكات تبعًا لامتداده الجغرافي إلى نوعين رئيسين: الشبكة المحلية (LAN) التي تغطي منطقة محدودة في مبنى واحد أو عدة مبانٍ متقاربة، والشبكة الموسعة (WAN) التي تغطي منطقة جغرافية واسعة داخل البلد الواحد أو خارجه، ويمكن توصيل الشبكات بعدة أشكال تبعًا لنوع وسيط التوصيل المستخدم، والتكلفة المادية المخصصة، واعتباراتٍ أخرى.</p>', NULL, '2018-05-10 17:19:27', '2018-05-10 17:35:17', '2018-05-10 17:35:17', 1),
(31, 1, 'Navigate_between_Windows_screens', '<pre>\r\nNavigate between Windows 8.1 screens\r\nYou can switch between the start screen and the desktop using the Windows key from the keyboard, and the ALT + Tab keys can be used to move between open or active application screens, and there are many keys that can be used to shorten many of the navigation functions to computer disks, To the Settings menu, or to the System Information page.</pre>\r\n\r\n<pre>\r\n\r\n&nbsp;</pre>', NULL, '2018-05-10 17:22:31', '2018-05-10 17:22:31', NULL, 1),
(32, 3, 'Navigate_between_Windows_screens', '<p>التنقل بين شاشات ويندوز 8.1<br />\r\nيمكن التبديل بين شاشة البدء وسطح المكتب باستخدام مفتاح ويندوز من لوحة المفاتيح، كما يمكن استخدام المفتاحين (ALT+Tab) للتنقل بين شاشات التطبيقات المفتوحة أو النشطة، وهناك العديد من المفاتيح التي يمكن استخدامها لتختصر القيام بكثير من وظائف التنقل إلى أقراص جهاز الحاسوب، أو إلى قائمة الإعدادات، أو إلى صفحة معلومات النظام.</p>', NULL, '2018-05-10 17:23:05', '2018-05-10 17:23:05', NULL, 1),
(33, 1, 'Desktop_and_taskbar', '<pre>\r\nDesktop and taskbar\r\nA desktop is a screen that contains a set of icons and icons that represent system or application programs. The taskbar is located below the desktop, showing the active and currently used programs and files, as well as the clock icon, date, and some other features of the computer.</pre>\r\n\r\n<pre>\r\n\r\n&nbsp;</pre>', NULL, '2018-05-10 17:24:02', '2018-05-10 17:36:11', '2018-05-10 17:36:11', 1),
(34, 3, 'Desktop_and_taskbar', '<p>سطح المكتب وشريط المهام<br />\r\nسطح المكتب هو تلك الشاشة التي تضم مجموعة من الرموز والأيقونات التي تمثل برامج النظام أو البرامج التطبيقية، ويقع شريط المهام أسفل سطح المكتب، وتظهر به البرامج والملفات النشطة والمستخدمة في الوقت الحالي، كما يظهر به رمز الساعة والتاريخ وبعض الخصائص الأخرى لجهاز الحاسوب.</p>', NULL, '2018-05-10 17:25:08', '2018-05-10 17:36:11', '2018-05-10 17:36:11', 1),
(35, 1, 'Windows_Defender', '<pre>\r\nWindows Defender\r\nWindows Defender is an anti-virus program that helps protect your computer from hacking and malware. It automatically installs on Windows 8.1 and provides several recognizable scan options for accompanying videos. It can also be updated with the latest version of the software.</pre>\r\n\r\n<pre>\r\n\r\n&nbsp;</pre>', NULL, '2018-05-10 17:26:18', '2018-05-10 17:36:11', '2018-05-10 17:36:11', 1),
(36, 3, 'Windows_Defender', '<p>برنامج Windows Defender<br />\r\nويندوز ديفيندر (Windows Defender) هو برنامج مضاد للفيروسات يساعد على حماية الحاسوب من الاختراق والبرمجيات الضارة، ويأتي مثبّتًا تلقائيًا على نظام التشغيل ويندوز 8.1، ويوفر عدة خيارات للفحص يمكن التعرّف عليها بمتابعة الفيديو المصاحب، كما يمكن تحديثه بتحميل أحدث إصداراته بسهولة ويُسر. &nbsp;&nbsp;<br />\r\n&nbsp;</p>', NULL, '2018-05-10 17:27:13', '2018-05-10 17:36:11', '2018-05-10 17:36:11', 1),
(37, 1, 'test', '<p>test</p>', NULL, '2018-05-10 17:34:20', '2018-05-10 17:36:12', '2018-05-10 17:36:12', 1),
(38, 1, 'computer_networks_title', '<p>Computer Networks</p>', NULL, '2018-05-10 17:37:23', '2018-05-10 17:47:32', NULL, 1),
(39, 1, 'computer_networks_description', '<p>This module defines the network as a group of computers connected by cards, cables and specialized software to share resources. The network has many benefits, the most important of which are the sharing of files, applications and storage media, the support of central data management and the provision of expenses.<br />\r\nNetworks are classified according to their geographic reach to two main types: the LAN, which covers a limited area of ​​one or several adjacent buildings, and the Wide Area Network (WAN) covering a wide geographical area within or outside a country. User connectivity, custom cost, and other considerations.</p>', NULL, '2018-05-10 17:39:37', '2018-05-10 17:39:37', NULL, 1),
(40, 3, 'computer_networks_title', '<p>شبكات الحاسوب</p>', NULL, '2018-05-10 17:41:27', '2018-05-10 17:48:14', NULL, 1),
(41, 3, 'computer_networks_description', '<p>تعرِّف هذه الوحدة الشبكة بأنها مجموعة حواسيب متصلة ببعضها عن طريق بطاقات وكابلات وبرمجيات متخصصة بهدف المشاركة في الموارد، ولإنشاء الشبكات فوائد متعددة من أهمها مشاركة الملفات والتطبيقات ووسائط التخزين، ودعم الإدارة المركزية للبيانات، وتوفير المصروفات.<br />\r\nوتصنَّف الشبكات تبعًا لامتداده الجغرافي إلى نوعين رئيسين: الشبكة المحلية (LAN) التي تغطي منطقة محدودة في مبنى واحد أو عدة مبانٍ متقاربة، والشبكة الموسعة (WAN) التي تغطي منطقة جغرافية واسعة داخل البلد الواحد أو خارجه، ويمكن توصيل الشبكات بعدة أشكال تبعًا لنوع وسيط التوصيل المستخدم، والتكلفة المادية المخصصة، واعتباراتٍ أخرى.</p>', NULL, '2018-05-10 17:43:53', '2018-05-10 17:43:53', NULL, 1),
(42, 1, 'Navigate_between_Windows_screens_title', '<p>Navigate between Windows 8.1 screens</p>', NULL, '2018-05-10 17:46:10', '2018-05-10 17:46:10', NULL, 1),
(43, 1, 'Navigate_between_Windows_screens_description', '<p>You can switch between the start screen and the desktop using the Windows key from the keyboard, and the ALT + Tab keys can be used to move between open or active application screens, and there are many keys that can be used to shorten many of the navigation functions to computer disks, To the Settings menu, or to the System Information page.</p>', NULL, '2018-05-10 17:49:52', '2018-05-10 17:49:52', NULL, 1),
(44, 3, 'Navigate_between_Windows_screens_title', '<p>التنقل بين شاشات ويندوز 8.1</p>', NULL, '2018-05-10 17:50:59', '2018-05-10 17:50:59', NULL, 1),
(45, 3, 'Navigate_between_Windows_screens_description', '<p>يمكن التبديل بين شاشة البدء وسطح المكتب باستخدام مفتاح ويندوز من لوحة المفاتيح، كما يمكن استخدام المفتاحين (ALT+Tab) للتنقل بين شاشات التطبيقات المفتوحة أو النشطة، وهناك العديد من المفاتيح التي يمكن استخدامها لتختصر القيام بكثير من وظائف التنقل إلى أقراص جهاز الحاسوب، أو إلى قائمة الإعدادات، أو إلى صفحة معلومات النظام.</p>', NULL, '2018-05-10 17:52:36', '2018-05-10 17:52:36', NULL, 1),
(46, 1, 'Desktop_and_taskbar_title', '<p>Desktop and taskbar</p>', NULL, '2018-05-10 17:54:15', '2018-05-10 17:54:15', NULL, 1),
(47, 1, 'Desktop_and_taskbar_description', '<p>A desktop is a screen that contains a set of icons and icons that represent system or application programs. The taskbar is located below the desktop, showing the active and currently used programs and files, as well as the clock icon, date, and some other features of the computer.</p>', NULL, '2018-05-10 17:55:33', '2018-05-10 17:55:33', NULL, 1),
(48, 3, 'Desktop_and_taskbar_title', '<p>سطح المكتب وشريط المهام</p>', NULL, '2018-05-10 17:56:42', '2018-05-10 17:56:42', NULL, 1),
(49, 3, 'Desktop_and_taskbar_title', '<p>سطح المكتب وشريط المهام</p>', NULL, '2018-05-10 17:57:38', '2018-05-10 17:57:57', '2018-05-10 17:57:57', 1),
(50, 3, 'Desktop_and_taskbar_description', '<p>سطح المكتب هو تلك الشاشة التي تضم مجموعة من الرموز والأيقونات التي تمثل برامج النظام أو البرامج التطبيقية، ويقع شريط المهام أسفل سطح المكتب، وتظهر به البرامج والملفات النشطة والمستخدمة في الوقت الحالي، كما يظهر به رمز الساعة والتاريخ وبعض الخصائص الأخرى لجهاز الحاسوب.</p>', NULL, '2018-05-10 17:58:45', '2018-05-10 17:58:45', NULL, 1),
(51, 1, 'Windows_Defender_title', '<p>Windows Defender</p>', NULL, '2018-05-10 18:00:32', '2018-05-10 18:00:32', NULL, 1),
(52, 1, 'Windows_Defender_description', '<p>Windows Defender is an anti-virus program that helps protect your computer from hacking and malware. It automatically installs on Windows 8.1 and provides several recognizable scan options for accompanying videos. It can also be updated with the latest version of the software.</p>', NULL, '2018-05-10 18:02:03', '2018-05-10 18:02:03', NULL, 1),
(53, 3, 'Windows_Defender_title', '<p>برنامج</p>', NULL, '2018-05-10 18:03:11', '2018-05-10 18:03:11', NULL, 1),
(54, 3, 'Windows_Defender_description', '<p>ويندوز ديفيندر (Windows Defender) هو برنامج مضاد للفيروسات يساعد على حماية الحاسوب من الاختراق والبرمجيات الضارة، ويأتي مثبّتًا تلقائيًا على نظام التشغيل ويندوز 8.1، ويوفر عدة خيارات للفحص يمكن التعرّف عليها بمتابعة الفيديو المصاحب، كما يمكن تحديثه بتحميل أحدث إصداراته بسهولة ويُسر.</p>', NULL, '2018-05-10 18:04:23', '2018-05-10 18:04:23', NULL, 1),
(55, 1, 'comp_science_engg', '<p>Computer Science &amp; Engg&nbsp;&nbsp; &nbsp;</p>', NULL, '2018-05-10 18:24:24', '2018-05-10 18:24:24', NULL, 1),
(56, 3, 'comp_science_engg', '<pre>\r\nعلوم الكمبيوتر والهندسة\r\n</pre>', NULL, '2018-05-10 18:25:42', '2018-05-10 18:25:42', NULL, 1),
(57, 1, 'comp_science_engg_summary', '<p>Learn Computer Science and Engineering</p>', NULL, '2018-05-10 18:31:19', '2018-05-10 18:31:19', NULL, 1),
(58, 3, 'comp_science_engg_summary', '<p>تعلم علوم الكمبيوتر والهندسة</p>', NULL, '2018-05-10 18:32:18', '2018-05-10 18:32:18', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_languages_settings_new`
--

CREATE TABLE `tb_languages_settings_new` (
  `id` int(10) UNSIGNED NOT NULL,
  `tb_languages_id` int(11) NOT NULL,
  `key_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `uniq_record` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `added_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_languages_settings_new`
--

INSERT INTO `tb_languages_settings_new` (`id`, `tb_languages_id`, `key_name`, `value`, `uniq_record`, `created_at`, `updated_at`, `deleted_at`, `added_by`) VALUES
(1, 1, 'course_1', 'The Complete Video Production Bootcamp', NULL, '2018-03-30 05:27:12', '2018-03-30 05:28:06', NULL, 1),
(2, 1, 'course_summary_1', '<div class=\"description__title\">1111Description</div>\r\n\r\n<p><strong><em>This online video production course will teach you how to make amazing videos, whether you use a smartphone, webcam, DSLR, mirrorless, or professional camera.&nbsp;</em></strong></p>\r\n\r\n<p>Whether you&#39;re a YouTuber, blogger, vlogger, business owner, aspiring filmmaker, or just someone who wants to create videos, you will learn how to make professional videos with this course.</p>\r\n\r\n<p><strong>Master Video Production Techniques to Create Amazing Videos that Boost Your Views, Revenue and Drive Traffic To Your Business</strong></p>\r\n\r\n<p>While there are plenty of tutorials and courses that focus on specific cameras or styles, it&#39;s hard to find a comprehensive course like this one, which covers everything from coming up with great video ideas, executing them in production and post-production, and distributing them to a wide audience online.</p>\r\n\r\n<p><strong>COURSE&nbsp;BONUSES:</strong></p>\r\n\r\n<ul>\r\n	<li>Direct feedback from instructors on any of your video projects</li>\r\n	<li>Downloadable guides that will help you in every section of the course</li>\r\n	<li>Case studies that break down&nbsp;real world film projects</li>\r\n</ul>\r\n\r\n<p>This video course is designed for all levels of video makers who want to improve their skills, create stellar videos, and even make money with their videos.</p>\r\n\r\n<p><strong>Key things you will learn:</strong></p>\r\n\r\n<ul>\r\n	<li>Come up with great video topics that people will love watching and sharing</li>\r\n	<li>Master shooting your video in manual mode on a DSLR, mirrorless, or professional cinema camera</li>\r\n	<li>Expose and compose better shots that tell your story</li>\r\n	<li>Shoot amazing videos with smartphones, webcams, or even screencasts</li>\r\n	<li>Aerial videography with DJI drones (coming soon)</li>\r\n	<li>Light your videos with professional techniques, on a small budget</li>\r\n	<li>Record crisp and clean audio with different microphones in any environment</li>\r\n	<li>Edit videos to make them more engaging</li>\r\n	<li>Know what equipment to purchase to create great videos</li>\r\n	<li>Export the best quality videos for online viewing</li>\r\n	<li>Get more views, likes, and shares on social media</li>\r\n	<li>Grow a YouTube channel that ultimately brings in revenue</li>\r\n</ul>\r\n\r\n<p><strong>Make Professional Videos on Any Budget</strong></p>\r\n\r\n<p>Regardless of what your level of experience is or what type of camera you use, this in-depth course is designed to provide you with everything you need to take your video production skills to the next level.</p>\r\n\r\n<p>Unlike other video courses or tutorials that are more limited in scope, this complete course teaches you the entire process.</p>\r\n\r\n<p><strong>Contents and Overview</strong></p>\r\n\r\n<p>This video course starts from the beginning, which is about coming up with great video ideas. You&#39;ll learn what makes a video great, and how to come up with video topics for your target audience. You&#39;ll walk through the pre-production process to ensure a smooth video shoot.</p>\r\n\r\n<p>Before diving into how to shoot videos, we&#39;ll cover our recommended equipment. We share our favorite equipment for any budget - including cameras, audio gear, lighting kits, and editing applications.</p>\r\n\r\n<p>You&#39;ll learn cinematography basics such as how to expose your video, how to compose great shots, how to film yourself, how to get great focus, and how to stabilize your shots. We cover how to do this for DSLR, mirrorless, smartphone, and webcam cameras. We even have a section on aerial drone videography!</p>\r\n\r\n<p>You&#39;ll learn how to record great audio. First, we cover the different types of microphones, and how to choose the right microphone for your video project. Then you learn how to use the different types of microphones. Plus, you&#39;ll learn how to record audio in any environment, including getting rid of echo.</p>\r\n\r\n<p>Lighting is one of the most important parts of video production, whether you&#39;re using a smartphone, webcam, or DSLR or mirrorless camera. You&#39;ll learn how to use free and inexpensive lighting techniques, and how to set up a lighting kit like the pros using the three-point lighting system.</p>\r\n\r\n<p>Once you understand everything about shooting your video, you&#39;ll learn how to use editing to make your videos even better and more engaging. You&#39;ll learn how to find free music for your videos, how to design better titles, and how to use calls to action to increase engagement and conversion.</p>\r\n\r\n<p>After all this, you&#39;ll learn how to better share your videos with the world. Learn how to choose the right platform for your video content. Get more views, likes, and shares with our tips for sharing on social media. And learn how to grow a YouTube channel with our best practices.</p>\r\n\r\n<p><strong>If you want to make better videos, this is the course for you.</strong></p>\r\n\r\n<p>Remember, there is a 30-day 100% money-back guarantee. There is no reason to hesitate. Enroll now, see if you enjoy the course, and start making better videos today!</p>\r\n\r\n<p>Cheers,</p>\r\n\r\n<p>Phil, Will, and Sam</p>', NULL, '2018-03-30 05:28:58', '2018-03-30 05:28:58', NULL, 1),
(3, 2, 'course_summary_1', '<div class=\"description__title\">dfsdfdfsdfDescription</div>\r\n\r\n<p><strong><em>This online video production course will teach you how to make amazing videos, whether you use a smartphone, webcam, DSLR, mirrorless, or professional camera.&nbsp;</em></strong></p>\r\n\r\n<p>Whether you&#39;re a YouTuber, blogger, vlogger, business owner, aspiring filmmaker, or just someone who wants to create videos, you will learn how to make professional videos with this course.</p>\r\n\r\n<p><strong>Master Video Production Techniques to Create Amazing Videos that Boost Your Views, Revenue and Drive Traffic To Your Business</strong></p>\r\n\r\n<p>While there are plenty of tutorials and courses that focus on specific cameras or styles, it&#39;s hard to find a comprehensive course like this one, which covers everything from coming up with great video ideas, executing them in production and post-production, and distributing them to a wide audience online.</p>\r\n\r\n<p><strong>COURSE&nbsp;BONUSES:</strong></p>\r\n\r\n<ul>\r\n	<li>Direct feedback from instructors on any of your video projects</li>\r\n	<li>Downloadable guides that will help you in every section of the course</li>\r\n	<li>Case studies that break down&nbsp;real world film projects</li>\r\n</ul>\r\n\r\n<p>This video course is designed for all levels of video makers who want to improve their skills, create stellar videos, and even make money with their videos.</p>\r\n\r\n<p><strong>Key things you will learn:</strong></p>\r\n\r\n<ul>\r\n	<li>Come up with great video topics that people will love watching and sharing</li>\r\n	<li>Master shooting your video in manual mode on a DSLR, mirrorless, or professional cinema camera</li>\r\n	<li>Expose and compose better shots that tell your story</li>\r\n	<li>Shoot amazing videos with smartphones, webcams, or even screencasts</li>\r\n	<li>Aerial videography with DJI drones (coming soon)</li>\r\n	<li>Light your videos with professional techniques, on a small budget</li>\r\n	<li>Record crisp and clean audio with different microphones in any environment</li>\r\n	<li>Edit videos to make them more engaging</li>\r\n	<li>Know what equipment to purchase to create great videos</li>\r\n	<li>Export the best quality videos for online viewing</li>\r\n	<li>Get more views, likes, and shares on social media</li>\r\n	<li>Grow a YouTube channel that ultimately brings in revenue</li>\r\n</ul>\r\n\r\n<p><strong>Make Professional Videos on Any Budget</strong></p>\r\n\r\n<p>Regardless of what your level of experience is or what type of camera you use, this in-depth course is designed to provide you with everything you need to take your video production skills to the next level.</p>\r\n\r\n<p>Unlike other video courses or tutorials that are more limited in scope, this complete course teaches you the entire process.</p>\r\n\r\n<p><strong>Contents and Overview</strong></p>\r\n\r\n<p>This video course starts from the beginning, which is about coming up with great video ideas. You&#39;ll learn what makes a video great, and how to come up with video topics for your target audience. You&#39;ll walk through the pre-production process to ensure a smooth video shoot.</p>\r\n\r\n<p>Before diving into how to shoot videos, we&#39;ll cover our recommended equipment. We share our favorite equipment for any budget - including cameras, audio gear, lighting kits, and editing applications.</p>\r\n\r\n<p>You&#39;ll learn cinematography basics such as how to expose your video, how to compose great shots, how to film yourself, how to get great focus, and how to stabilize your shots. We cover how to do this for DSLR, mirrorless, smartphone, and webcam cameras. We even have a section on aerial drone videography!</p>\r\n\r\n<p>You&#39;ll learn how to record great audio. First, we cover the different types of microphones, and how to choose the right microphone for your video project. Then you learn how to use the different types of microphones. Plus, you&#39;ll learn how to record audio in any environment, including getting rid of echo.</p>\r\n\r\n<p>Lighting is one of the most important parts of video production, whether you&#39;re using a smartphone, webcam, or DSLR or mirrorless camera. You&#39;ll learn how to use free and inexpensive lighting techniques, and how to set up a lighting kit like the pros using the three-point lighting system.</p>\r\n\r\n<p>Once you understand everything about shooting your video, you&#39;ll learn how to use editing to make your videos even better and more engaging. You&#39;ll learn how to find free music for your videos, how to design better titles, and how to use calls to action to increase engagement and conversion.</p>\r\n\r\n<p>After all this, you&#39;ll learn how to better share your videos with the world. Learn how to choose the right platform for your video content. Get more views, likes, and shares with our tips for sharing on social media. And learn how to grow a YouTube channel with our best practices.</p>\r\n\r\n<p><strong>If you want to make better videos, this is the course for you.</strong></p>\r\n\r\n<p>Remember, there is a 30-day 100% money-back guarantee. There is no reason to hesitate. Enroll now, see if you enjoy the course, and start making better videos today!</p>\r\n\r\n<p>Cheers,</p>\r\n\r\n<p>Phil, Will, and Sam</p>', NULL, '2018-03-30 05:28:58', '2018-03-30 05:28:58', NULL, 1),
(4, 1, 'lession_1', 'What is PHP?', NULL, '2018-03-30 06:23:07', '2018-03-30 06:23:07', NULL, 1),
(5, 1, 'lession_des_1', '<ul>\r\n	<li>PHP is an acronym for &quot;PHP: Hypertext Preprocessor&quot;</li>\r\n	<li>PHP is a widely-used, open source scripting language</li>\r\n	<li>PHP scripts are executed on the server</li>\r\n	<li>PHP is free to download and use</li>\r\n</ul>', NULL, '2018-03-30 06:23:26', '2018-03-30 06:23:26', NULL, 1),
(6, 1, 'lesson1_question_1', '<p>1. What does PHP stand for?</p>\r\n\r\n<div>i) Personal Home Page</div>\r\n\r\n<div>ii) Hypertext Preprocessor</div>\r\n\r\n<div>iii) Pretext Hypertext Processor</div>\r\n\r\n<div>iv) Preprocessor Home Page</div>', NULL, '2018-03-30 06:25:34', '2018-03-30 06:25:34', NULL, 1),
(7, 1, 'lesson1_question1_answer1', 'Both (i) and (ii)', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(8, 1, 'lesson1_question1_answer2', 'Both (ii) and (iv)', NULL, '2018-03-30 06:27:51', '2018-03-30 06:27:51', NULL, 1),
(9, 1, 'lesson1_question1_answer3', 'Only (ii)', NULL, '2018-03-30 06:28:15', '2018-03-30 06:28:15', NULL, 1),
(10, 1, 'lesson1_question1_answer3', 'Both (i) and (iii)', NULL, '2018-03-30 06:31:35', '2018-03-30 06:31:35', NULL, 1),
(11, 1, 'course_summary_1', '<p>course_summary_1</p>', NULL, '2018-03-30 06:31:50', '2018-03-30 06:31:50', NULL, 1),
(12, 1, 'lesson1_question_2', 'PHP’s numerically indexed array begin with position', NULL, '2018-03-30 06:25:34', '2018-03-30 06:25:34', NULL, 1),
(13, 1, 'lesson1_question_3', 'Which of the functions is used to sort an array in descending order?', NULL, '2018-03-30 06:25:34', '2018-03-30 06:25:34', NULL, 1),
(14, 1, 'lesson1_question_4', ' Which of the following are correct ways of creating an array?', NULL, '2018-03-30 06:25:34', '2018-03-30 06:25:34', NULL, 1),
(15, 1, 'lesson1_question_5', 'What will be the output of the following php code? ', NULL, '2018-03-30 06:25:34', '2018-03-30 06:25:34', NULL, 1),
(16, 1, 'lesson1_question_6', 'Which function will return true if a variable is an array or false if it is not? ', NULL, '2018-03-30 06:25:34', '2018-03-30 06:25:34', NULL, 1),
(17, 1, 'lesson1_question_7', 'Which in-built function will add a value to the end of an array? ', NULL, '2018-03-30 06:25:34', '2018-03-30 06:25:34', NULL, 1),
(18, 1, 'lesson1_question_8', 'What will be the output of the following PHP code? ', NULL, '2018-03-30 06:25:34', '2018-03-30 06:25:34', NULL, 1),
(19, 1, 'lesson1_question2_answer1', 'Both (i) and (ii)', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(20, 1, 'lesson1_question2_answer2', '(i)', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(21, 1, 'lesson1_question3_answer1 	', '(i)', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(22, 1, 'lesson1_question3_answer2', '4', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(23, 1, 'lesson1_question4_answer1', '5', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(24, 1, 'lesson1_question4_answer2', '8', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(25, 1, 'lesson1_question5_answer1', '45', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(26, 1, 'lesson1_question5_answer2', '(II)', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(27, 1, 'lesson1_question6_answer1', '5', NULL, '2018-03-30 06:26:31', '2018-03-30 06:26:31', NULL, 1),
(28, 1, 'lesson1_question6_answer2', '586', NULL, '2018-03-30 06:25:34', '2018-03-30 06:25:34', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_learner_projects`
--

CREATE TABLE `tb_learner_projects` (
  `id` int(11) NOT NULL,
  `learner_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `file` varchar(5000) NOT NULL,
  `status` enum('waiting','progress','rejected','approved') NOT NULL DEFAULT 'waiting',
  `reason` mediumtext,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_learner_projects`
--

INSERT INTO `tb_learner_projects` (`id`, `learner_id`, `course_id`, `author_id`, `file`, `status`, `reason`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 1, 1, 'sdfsdfdsff', 'approved', NULL, '2018-05-02 13:02:13', '2018-05-02 07:32:13', NULL),
(2, 41, 1, 3, '1525084052-Roja Profile.odt', 'rejected', 'dfgfdgfdg', '2018-05-08 12:29:38', '2018-05-03 02:14:15', NULL),
(3, 41, 1, 3, '1525084129-Roja Profile.odt', 'rejected', NULL, '2018-05-08 12:29:40', NULL, NULL),
(4, 1, 1, 1, 'nx1gQjF.UO1jA', 'waiting', NULL, '2018-04-30 11:02:11', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_learner_test_store`
--

CREATE TABLE `tb_learner_test_store` (
  `id` int(11) NOT NULL,
  `learner_id` int(11) DEFAULT NULL,
  `keygen_id` int(11) DEFAULT NULL,
  `key_code` varchar(100) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `lesson_id` int(11) DEFAULT NULL,
  `attempt` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `answer_id` varchar(1000) DEFAULT NULL,
  `correct_ans` int(11) DEFAULT NULL,
  `learner_ans` int(11) DEFAULT NULL,
  `is_correct` int(11) DEFAULT NULL,
  `mark` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_learner_test_store`
--

INSERT INTO `tb_learner_test_store` (`id`, `learner_id`, `keygen_id`, `key_code`, `course_id`, `lesson_id`, `attempt`, `question_id`, `answer_id`, `correct_ans`, `learner_ans`, `is_correct`, `mark`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 1, '411021', 1, 1, 1, 1, '10~11~12', 12, 11, 0, 0, '2018-04-26 01:14:40', '2018-04-26 01:14:40', NULL),
(2, 4, 1, '411021', 1, 1, 1, 2, '13~14', 14, 13, 0, 0, '2018-04-26 01:14:40', '2018-04-26 01:14:40', NULL),
(3, 4, 1, '411021', 1, 1, 1, 3, '15~16', 15, 16, 0, 0, '2018-04-26 01:14:40', '2018-04-26 01:14:40', NULL),
(4, 4, 1, '411021', 1, 1, 1, 4, '17~18', 18, 18, 1, 2, '2018-04-26 01:14:41', '2018-04-26 01:14:41', NULL),
(5, 4, 1, '411021', 1, 1, 1, 5, '19~20', 19, 19, 1, 2, '2018-04-26 01:14:41', '2018-04-26 01:14:41', NULL),
(6, 4, 1, '411021', 1, 1, 1, 6, '21~22', 22, 22, 1, 2, '2018-04-26 01:14:41', '2018-04-26 01:14:41', NULL),
(7, 4, 2, '6199017788', 1, 1, 1, 2, '13~14', 14, NULL, 0, 0, '2018-04-26 04:47:29', '2018-04-26 04:47:29', NULL),
(8, 4, 2, '6199017788', 1, 1, 1, 2, '13~14', 14, NULL, 0, 0, '2018-04-26 04:48:02', '2018-04-26 04:48:02', NULL),
(9, 4, 2, '6199017788', 1, 1, 1, 1, '10~11~12', 12, NULL, 0, 0, '2018-04-26 04:48:36', '2018-04-26 04:48:36', NULL),
(10, 4, 2, '6199017788', 1, 1, 1, 2, '13~14', 14, NULL, 0, 0, '2018-04-26 04:48:36', '2018-04-26 04:48:36', NULL),
(11, 4, 2, '6199017788', 1, 1, 1, 3, '15~16', 15, NULL, 0, 0, '2018-04-26 04:48:36', '2018-04-26 04:48:36', NULL),
(12, 4, 2, '6199017788', 1, 1, 1, 4, '17~18', 18, NULL, 0, 0, '2018-04-26 04:48:36', '2018-04-26 04:48:36', NULL),
(13, 4, 2, '6199017788', 1, 1, 1, 5, '19~20', 19, NULL, 0, 0, '2018-04-26 04:48:36', '2018-04-26 04:48:36', NULL),
(14, 4, 2, '6199017788', 1, 1, 1, 6, '21~22', 22, NULL, 0, 0, '2018-04-26 04:48:36', '2018-04-26 04:48:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_lession`
--

CREATE TABLE `tb_lession` (
  `id` int(11) NOT NULL,
  `test_type` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `subtype_id` int(11) DEFAULT NULL,
  `video` longtext,
  `assessment_test` enum('yes','no') NOT NULL DEFAULT 'yes',
  `course_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `order` int(11) DEFAULT '1',
  `num_que` int(11) DEFAULT NULL,
  `hours` int(11) DEFAULT NULL,
  `minutes` int(11) DEFAULT NULL,
  `pass_percentage` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_lession`
--

INSERT INTO `tb_lession` (`id`, `test_type`, `name`, `description`, `subtype_id`, `video`, `assessment_test`, `course_id`, `author_id`, `status`, `order`, `num_que`, `hours`, `minutes`, `pass_percentage`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 'lession_1', 'lession_des_1', 1, '1522393697-1519965255-Learn PHP in 15 minutes.mp4', 'yes', 1, 3, 0, 1, 6, 1, 0, 50, NULL, NULL, NULL),
(2, 0, 'lession_2', 'lession_des_2', 2, NULL, 'yes', 2, 3, 0, 1, 10, 6, 45, 40, NULL, NULL, NULL),
(3, 0, 'lession_1', 'lession_des_1', 1, '1522393697-1519965255-Learn PHP in 15 minutes.mp4', 'yes', 2, 3, 0, 1, 6, 1, 0, 50, NULL, NULL, NULL),
(4, 0, 'Networking', 'Networking', 2, NULL, 'yes', 13, 3, 0, 1, 0, NULL, NULL, 0, NULL, '2018-05-10 14:13:27', '2018-05-10 14:13:27'),
(5, 0, 'computer_networks', 'This module defines the network as a group of computers connected by cards, cables and specialized software to share resources. The network has many benefits, the most important of which are the sharing of files, applications and storage media, the support of central data management and the provision of expenses. Networks are classified according to their geographic reach to two main types: the LAN, which covers a limited area of ??one or several adjacent buildings, and the Wide Area Network (WAN) covering a wide geographical area within or outside a country. User connectivity, custom cost, and other considerations.', 1, '1525947829-01- ????? ???????.mp4', 'yes', 13, 3, 0, 1, 1000, NULL, NULL, 100, NULL, '2018-05-10 15:11:12', '2018-05-10 15:11:12'),
(6, 0, 'computer_networks_title', 'computer_networks_description', 1, '1525961977-01- ????? ???????.mp4', 'yes', 13, 3, 0, 1, 1000, NULL, NULL, 100, NULL, NULL, NULL),
(7, 0, 'Navigate_between_Windows_screens_title', 'Navigate_between_Windows_screens_description', 1, '1525962107-02- ?????? ??? ????? ?????? 8.1.mp4', 'yes', 13, 3, 0, 1, 1000, NULL, NULL, 100, NULL, NULL, NULL),
(8, 0, 'Desktop_and_taskbar_title', 'Desktop_and_taskbar_description', 1, '1525962194-03- ??? ?????? ????? ??????.mp4', 'yes', 13, 3, 0, 1, 1000, NULL, NULL, 100, NULL, NULL, NULL),
(9, 0, 'Windows_Defender_title', 'Windows_Defender_description', 1, '1525962464-04- ?????? Windows Defender.mp4', 'yes', 13, 3, 0, 1, 1000, NULL, NULL, 100, NULL, NULL, NULL),
(10, 0, 'Ceep', 'Ceep', 1, '1526102816-bug 4.mp4', 'yes', 14, 3, 0, 1, 0, 8, 35, 0, NULL, '2018-05-11 23:57:23', '2018-05-11 23:57:23'),
(11, 0, 'Ceep', 'Ceep', 1, '1526102875-Sony 4K Demo_ Another World.mp4', 'yes', 14, 3, 0, 1, 2, 5, 10, 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_lession_answer`
--

CREATE TABLE `tb_lession_answer` (
  `id` int(11) NOT NULL,
  `qid` int(11) NOT NULL,
  `answer_type` int(11) DEFAULT NULL,
  `answer_subtype` int(11) DEFAULT NULL,
  `choice_type` int(11) NOT NULL DEFAULT '0',
  `answer` varchar(200) NOT NULL,
  `file_url` varchar(5000) DEFAULT NULL,
  `correct_answer` varchar(200) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_lession_answer`
--

INSERT INTO `tb_lession_answer` (`id`, `qid`, `answer_type`, `answer_subtype`, `choice_type`, `answer`, `file_url`, `correct_answer`, `deleted_at`) VALUES
(13, 2, 1, 1, 0, 'lesson1_question2_answer1', NULL, '0', NULL),
(11, 1, 2, 1, 0, 'lesson1_question1_answer2', '1522396476-96240515-db73-47fc-96f6-02826c16bc7d.jpg', '0', NULL),
(12, 1, 2, 1, 0, 'lesson1_question1_answer3', '1522396476-sports.jpg', '1', NULL),
(10, 1, 2, 1, 0, 'lesson1_question1_answer1', '1522396476-hanuman.jpg', '0', NULL),
(14, 2, 1, 1, 0, 'lesson1_question2_answer2', NULL, '1', NULL),
(15, 3, 1, 1, 0, 'lesson1_question3_answer1', NULL, '1', NULL),
(16, 3, 1, 1, 0, 'lesson1_question3_answer2', NULL, '0', NULL),
(17, 4, 1, 1, 0, 'lesson1_question4_answer1', NULL, '0', NULL),
(18, 4, 1, 1, 0, 'lesson1_question4_answer2', NULL, '1', NULL),
(19, 5, 1, 1, 0, 'lesson1_question5_answer1', NULL, '1', NULL),
(20, 5, 1, 1, 0, 'lesson1_question5_answer2', NULL, '0', NULL),
(21, 6, 1, 1, 0, 'lesson1_question6_answer1', NULL, '0', NULL),
(22, 6, 1, 1, 0, 'lesson1_question6_answer2', NULL, '1', NULL),
(23, 18, 5, NULL, 0, '0', '', '0', NULL),
(24, 19, 5, NULL, 0, 'Written Type', '', '0', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_lession_answer_new`
--

CREATE TABLE `tb_lession_answer_new` (
  `id` int(11) NOT NULL,
  `qid` int(11) NOT NULL,
  `answer_type` int(11) DEFAULT NULL,
  `answer_subtype` int(11) DEFAULT NULL,
  `choice_type` int(11) NOT NULL DEFAULT '0',
  `answer` varchar(200) NOT NULL,
  `file_url` varchar(5000) DEFAULT NULL,
  `correct_answer` varchar(200) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_lession_answer_new`
--

INSERT INTO `tb_lession_answer_new` (`id`, `qid`, `answer_type`, `answer_subtype`, `choice_type`, `answer`, `file_url`, `correct_answer`, `deleted_at`) VALUES
(13, 2, 1, 1, 0, 'lesson1_question2_answer1', NULL, '0', NULL),
(11, 1, 2, 1, 0, 'lesson1_question1_answer2', '1522396476-96240515-db73-47fc-96f6-02826c16bc7d.jpg', '0', NULL),
(12, 1, 2, 1, 0, 'lesson1_question1_answer3', '1522396476-sports.jpg', '1', NULL),
(10, 1, 2, 1, 0, 'lesson1_question1_answer1', '1522396476-hanuman.jpg', '0', NULL),
(14, 2, 1, 1, 0, 'lesson1_question2_answer2', NULL, '1', NULL),
(15, 3, 1, 1, 0, 'lesson1_question3_answer1', NULL, '1', NULL),
(16, 3, 1, 1, 0, 'lesson1_question3_answer2', NULL, '0', NULL),
(17, 4, 1, 1, 0, 'lesson1_question4_answer1', NULL, '0', NULL),
(18, 4, 1, 1, 0, 'lesson1_question4_answer2', NULL, '1', NULL),
(19, 5, 1, 1, 0, 'lesson1_question5_answer1', NULL, '1', NULL),
(20, 5, 1, 1, 0, 'lesson1_question5_answer2', NULL, '0', NULL),
(21, 6, 1, 1, 0, 'lesson1_question6_answer1', NULL, '0', NULL),
(22, 6, 1, 1, 0, 'lesson1_question6_answer2', NULL, '1', NULL),
(23, 17, 1, 0, 0, 'werwewer', '', '0', NULL),
(24, 17, 1, 0, 0, 'werwerew', '', '1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_lession_new`
--

CREATE TABLE `tb_lession_new` (
  `id` int(11) NOT NULL,
  `test_type` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `subtype_id` int(11) DEFAULT NULL,
  `video` longtext,
  `assessment_test` enum('yes','no') NOT NULL DEFAULT 'yes',
  `course_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `order` int(11) DEFAULT '1',
  `num_que` int(11) DEFAULT NULL,
  `hours` int(11) DEFAULT NULL,
  `minutes` int(11) DEFAULT NULL,
  `pass_percentage` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_lession_new`
--

INSERT INTO `tb_lession_new` (`id`, `test_type`, `name`, `description`, `subtype_id`, `video`, `assessment_test`, `course_id`, `author_id`, `status`, `order`, `num_que`, `hours`, `minutes`, `pass_percentage`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 'lession_1', 'lession_des_1', 1, '1522393697-1519965255-Learn PHP in 15 minutes.mp4', 'yes', 1, 3, 0, 1, 6, 1, 0, 50, NULL, NULL, NULL),
(2, 0, 'lession_2', 'lession_des_2', 2, NULL, 'yes', 2, 3, 0, 1, 10, 6, 45, 40, NULL, NULL, NULL),
(3, 0, 'lession_1', 'lession_des_1', 1, '1522393697-1519965255-Learn PHP in 15 minutes.mp4', 'yes', 2, 3, 0, 1, 6, 1, 0, 50, NULL, NULL, NULL),
(4, 0, 'dfdsf', 'dfsfdf', 2, 'sdfsdfsf', 'yes', 4, 3, 0, 1, 10, 7, 45, 10, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_lession_question`
--

CREATE TABLE `tb_lession_question` (
  `id` int(11) NOT NULL,
  `course_id` int(11) DEFAULT NULL,
  `lession_id` varchar(100) NOT NULL,
  `test_type` int(11) NOT NULL COMMENT '1 - Straight set, 2- Random set, 3 - Poll based(Yes or No), 4 - offline',
  `question_type` int(100) NOT NULL DEFAULT '0' COMMENT '1=Text, 2=Image, 3=Audio, 4=Video ',
  `question_subtype` int(100) NOT NULL DEFAULT '0' COMMENT '1=upload, 2=url',
  `question` varchar(200) NOT NULL,
  `file_url` text,
  `change_order` int(11) NOT NULL DEFAULT '0',
  `mark` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_lession_question`
--

INSERT INTO `tb_lession_question` (`id`, `course_id`, `lession_id`, `test_type`, `question_type`, `question_subtype`, `question`, `file_url`, `change_order`, `mark`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '1', 0, 2, 1, 'lesson1_question_1', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(2, 1, '1', 0, 2, 2, 'lesson1_question_2', 'https://tests4geeks.com/content/img/smp/php-sample-question-answer-1.png', 0, 2, NULL, NULL, NULL),
(3, 1, '1', 0, 4, 1, 'lesson1_question_3', '1522393697-1519965255-Learn PHP in 15 minutes.mp4', 0, 2, NULL, NULL, NULL),
(4, 1, '1', 0, 4, 2, 'lesson1_question_4', 'https://www.youtube.com/watch?v=fZRtL_xrKlw', 0, 2, NULL, NULL, NULL),
(5, 1, '1', 0, 3, 1, 'lesson1_question_5', 'SampleAudio_0.4mb.mp3', 0, 2, NULL, NULL, NULL),
(6, 1, '1', 0, 3, 2, 'lesson1_question_6', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(7, 1, '1', 0, 2, 1, 'lesson1_question_7', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(8, 1, '1', 0, 2, 1, 'lesson1_question_8', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(9, 1, '1', 0, 2, 1, 'lesson1_question_9', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(10, 1, '1', 0, 2, 1, 'lesson1_question_10', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(11, 1, '1', 0, 2, 1, 'lesson1_question_11', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(12, 1, '1', 0, 2, 1, 'lesson1_question_12', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(13, 1, '1', 0, 2, 1, 'lesson1_question_13', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(14, 1, '1', 0, 2, 1, 'lesson1_question_14', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(15, 1, '1', 0, 2, 1, 'lesson1_question_15', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(16, 1, '1', 0, 1, 0, 'test', '', 0, 50, NULL, NULL, NULL),
(17, 1, '1', 0, 1, 0, 'test', '', 0, 50, NULL, NULL, NULL),
(18, 1, '1', 0, 1, 0, 'test', '', 0, 50, NULL, NULL, NULL),
(19, 14, '11', 0, 1, 0, 'test', '', 0, 50, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_lession_question_new`
--

CREATE TABLE `tb_lession_question_new` (
  `id` int(11) NOT NULL,
  `course_id` int(11) DEFAULT NULL,
  `lession_id` varchar(100) NOT NULL,
  `test_type` int(11) NOT NULL COMMENT '1 - Straight set, 2- Random set, 3 - Poll based(Yes or No), 4 - offline',
  `question_type` int(100) NOT NULL DEFAULT '0' COMMENT '1=Text, 2=Image, 3=Audio, 4=Video ',
  `question_subtype` int(100) NOT NULL DEFAULT '0' COMMENT '1=upload, 2=url',
  `question` varchar(200) NOT NULL,
  `file_url` text,
  `change_order` int(11) NOT NULL DEFAULT '0',
  `mark` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_lession_question_new`
--

INSERT INTO `tb_lession_question_new` (`id`, `course_id`, `lession_id`, `test_type`, `question_type`, `question_subtype`, `question`, `file_url`, `change_order`, `mark`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '1', 0, 2, 1, 'lesson1_question_1', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(2, 1, '1', 0, 2, 2, 'lesson1_question_2', 'https://tests4geeks.com/content/img/smp/php-sample-question-answer-1.png', 0, 2, NULL, NULL, NULL),
(3, 1, '1', 0, 4, 1, 'lesson1_question_3', '1522393697-1519965255-Learn PHP in 15 minutes.mp4', 0, 2, NULL, NULL, NULL),
(4, 1, '1', 0, 4, 2, 'lesson1_question_4', 'https://www.youtube.com/watch?v=fZRtL_xrKlw', 0, 2, NULL, NULL, NULL),
(5, 1, '1', 0, 3, 1, 'lesson1_question_5', 'SampleAudio_0.4mb.mp3', 0, 2, NULL, NULL, NULL),
(6, 1, '1', 0, 3, 2, 'lesson1_question_6', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(7, 1, '1', 0, 2, 1, 'lesson1_question_7', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(8, 1, '1', 0, 2, 1, 'lesson1_question_8', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(9, 1, '1', 0, 2, 1, 'lesson1_question_9', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(10, 1, '1', 0, 2, 1, 'lesson1_question_10', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(11, 1, '1', 0, 2, 1, 'lesson1_question_11', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(12, 1, '1', 0, 2, 1, 'lesson1_question_12', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(13, 1, '1', 0, 2, 1, 'lesson1_question_13', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(14, 1, '1', 0, 2, 1, 'lesson1_question_14', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(15, 1, '1', 0, 2, 1, 'lesson1_question_15', '1522396476-apples-burn-fat-400x400.jpg', 0, 2, NULL, NULL, NULL),
(16, 4, '4', 0, 1, 0, 'dsfsdfs', '', 0, 10, NULL, NULL, NULL),
(17, 4, '4', 0, 1, 0, 'dsfsdfs', '', 0, 10, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_notification`
--

CREATE TABLE `tb_notification` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `n_user_id` int(11) DEFAULT NULL,
  `n_role` int(11) DEFAULT NULL,
  `n_uname` varchar(5000) DEFAULT NULL,
  `notification` varchar(5000) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_notification`
--

INSERT INTO `tb_notification` (`id`, `user_id`, `n_user_id`, `n_role`, `n_uname`, `notification`, `date`, `status`) VALUES
(1, 1, 1, 1, 'Admin', 'Admin created one course to you', '2018-04-07 07:15:16', 0),
(2, 1, 1, 1, 'Admin', 'Admin created one course to you', '2018-04-07 07:15:18', 0),
(3, 4, 1, 1, 'admin123', 'Author has approved your Project ( ID - 1).', '2018-05-02 13:04:03', 0),
(4, 3, 1, 1, 'Admin', 'Admin created one course to you', '2018-05-12 05:06:53', 0),
(5, 3, 1, 1, 'Admin', 'Admin created one course to you', '2018-05-17 06:38:26', 0),
(6, 3, 1, 1, 'Admin', 'Admin created one course to you', '2018-05-17 10:22:03', 0),
(7, 3, 1, 1, 'Admin', 'Admin created one course to you', '2018-05-17 10:27:11', 0),
(8, 3, 1, 1, 'Admin', 'Admin created one course to you', '2018-05-17 10:29:03', 0),
(9, 1, 8, 5, 'author', 'author created new course', '2018-05-19 07:36:37', 0),
(10, 3, 1, 1, 'Admin', 'Admin created one course to you', '2018-05-25 12:46:52', 0),
(11, 8, 1, 1, 'Admin', 'Your book has been created, Your book under admin approval', '2018-06-07 13:34:25', 0),
(12, 8, 1, 1, 'Admin', 'Your book has been created, Your book under admin approval', '2018-06-07 14:26:26', 0),
(13, 8, 1, 1, 'Admin', 'Your book has been created, Your book under admin approval', '2018-06-07 14:29:31', 0),
(14, 8, 1, 1, 'Admin', 'Your book has been created, Your book under admin approval', '2018-06-08 09:46:25', 0),
(15, 8, 1, 1, 'Admin', 'Your book has been created, Your book under admin approval', '2018-06-08 09:57:01', 0),
(16, 8, 1, 1, 'Admin', 'Your book has been created, Your book under admin approval', '2018-06-08 10:47:18', 0),
(17, 8, 1, 1, 'Admin', 'Your book has been created, Your book under admin approval', '2018-06-08 10:48:55', 0),
(18, 8, 1, 1, 'Admin', 'Your book has been created, Your book under admin approval', '2018-06-08 10:51:51', 0),
(19, 8, 1, 1, 'Admin', 'Your book has been created, Your book under admin approval', '2018-06-08 10:56:31', 0),
(20, 8, 1, 1, 'Admin', 'Your book has been created, Your book under admin approval', '2018-06-08 11:07:07', 0),
(21, 8, 1, 1, 'Admin', 'Your book has been created, Your book under admin approval', '2018-06-08 11:20:41', 0),
(22, 8, 1, 1, 'Admin', 'Your book has been created, Your book under admin approval', '2018-06-08 11:26:15', 0),
(23, 8, 1, 1, 'Admin', 'Your book has been created, Your book under admin approval', '2018-06-11 08:30:13', 0),
(24, 8, 1, 1, 'Admin', 'Your book has been created, Your book under admin approval', '2018-06-12 10:38:02', 0),
(25, 8, 1, 1, 'Admin', 'Your book has been created, Your book under admin approval', '2018-06-12 10:52:45', 0),
(26, 8, 1, 1, 'Admin', 'Your book has been created, Your book under admin approval', '2018-06-12 11:09:44', 0),
(27, 8, 1, 1, 'Admin', 'Your book has been created, Your book under admin approval', '2018-06-25 06:22:23', 0),
(28, 1, 8, 5, 'author', 'Hi author, student Buy your book at 2018-07-06 07:56:16. Order ID #5b3f20a02fcfc', '2018-07-06 02:26:16', 0),
(29, 1, 1, 1, 'E-Book', 'Order have been made at 2018-07-06 07:56:16. Book author author Book buyer student, Order ID #5b3f20a02fcfc', '2018-07-06 02:26:16', 0),
(30, 1, 8, 5, 'author', 'Hi author, student Buy your book at 2018-07-06 07:56:16. Order ID #5b3f20a02fcfc', '2018-07-06 02:26:16', 0),
(31, 1, 1, 1, 'E-Book', 'Order have been made at 2018-07-06 07:56:16. Book author author Book buyer student, Order ID #5b3f20a02fcfc', '2018-07-06 02:26:16', 0),
(32, 1, 12, 2, 'student', 'Greate, Your order has been complete, Invoice will send to your email.', '2018-07-06 02:26:16', 0),
(33, 1, 3, 4, 'Vani', 'Hi Vani, Admin create a new book under you', '2018-07-10 12:35:20', 0),
(34, 1, 3, 4, 'Vani', 'Hi Vani, Admin create a new book under you', '2018-07-10 12:37:54', 0),
(35, 1, 3, 4, 'Vani', 'Hi Vani, Admin create a new book under you', '2018-07-10 12:38:51', 0),
(36, 1, 8, 5, 'author', 'Your book has been created, Your book under admin approval', '2018-07-14 09:39:13', 0),
(37, 8, 1, 1, 'Admin', 'author created new book', '2018-07-14 09:39:13', 0),
(38, 1, 3, 5, 'Vani', 'Hi Vani, student Buy a book at 2018-07-14 12:02:00. Order ID #5b49e638f31da', '2018-07-14 06:32:01', 0),
(39, 1, 1, 1, 'E-Book', 'Order have been made at 2018-07-14 12:02:00. Book author Vani Book buyer student, Order ID #5b49e638f31da', '2018-07-14 06:32:01', 0),
(40, 1, 12, 2, 'student', 'Greate, Your order has been complete, Invoice will send to your email.', '2018-07-14 06:32:01', 0),
(41, 1, 3, 5, 'Vani', 'Hi Vani, student Buy a book at 2018-07-16 07:36:39. Order ID #5b4c4b07e1745', '2018-07-16 02:06:40', 0),
(42, 1, 1, 1, 'E-Book', 'Order have been made at 2018-07-16 07:36:39. Book author Vani Book buyer student, Order ID #5b4c4b07e1745', '2018-07-16 02:06:40', 0),
(43, 1, 12, 2, 'student', 'Greate, Your order has been complete, Invoice will send to your email.', '2018-07-16 02:06:40', 0),
(44, 1, 3, 5, 'Vani', 'Hi Vani, student Buy a book at 2018-07-19 05:58:34. Order ID #5b50288a9b9a3', '2018-07-19 00:28:34', 0),
(45, 1, 1, 1, 'E-Book', 'Order have been made at 2018-07-19 05:58:34. Book author Vani Book buyer student, Order ID #5b50288a9b9a3', '2018-07-19 00:28:34', 0),
(46, 1, 12, 2, 'student', 'Greate, Your order has been complete, Invoice will send to your email.', '2018-07-19 00:28:34', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_professional`
--

CREATE TABLE `tb_professional` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_professional`
--

INSERT INTO `tb_professional` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Painter', '2018-02-26 02:22:42', '2018-02-26 02:22:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_purchased_course`
--

CREATE TABLE `tb_purchased_course` (
  `id` int(11) NOT NULL,
  `learner_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `paid_amount` int(11) NOT NULL,
  `paid_amount_currency` varchar(100) NOT NULL,
  `payment_status` enum('pending','paid') NOT NULL DEFAULT 'pending',
  `payment_mode` enum('free','pay') DEFAULT NULL,
  `payment_type` enum('ccavenue','payfort') DEFAULT NULL,
  `change_id` varchar(500) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `paid_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_purchased_course`
--

INSERT INTO `tb_purchased_course` (`id`, `learner_id`, `teacher_id`, `course_id`, `paid_amount`, `paid_amount_currency`, `payment_status`, `payment_mode`, `payment_type`, `change_id`, `date`, `paid_date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 3, 14, 100, 'USD', 'paid', 'pay', 'ccavenue', 'sdffs43545rerfdgfdgdf', '2018-04-07 08:04:46', '2018-04-06 18:30:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_support_system`
--

CREATE TABLE `tb_support_system` (
  `id` int(11) NOT NULL,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `rep_id` int(11) DEFAULT NULL,
  `subject` varchar(2000) DEFAULT NULL,
  `priority` varchar(1000) DEFAULT NULL,
  `description` mediumtext NOT NULL,
  `status` enum('open','close','hold','replied') NOT NULL DEFAULT 'open',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_support_system`
--

INSERT INTO `tb_support_system` (`id`, `from_id`, `to_id`, `rep_id`, `subject`, `priority`, `description`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 1, NULL, 'dfsdfsdf', 'high', 'sdfsdf', 'hold', '2018-04-09 01:15:40', '2018-04-09 02:43:10', NULL),
(2, 3, 1, NULL, 'Exam issue', 'medium', 'sdfsdfsdfsd', 'hold', '2018-04-09 01:37:51', '2018-04-09 07:01:48', NULL),
(3, 3, 1, NULL, 'Exam issue', 'medium', 'sdfsdfsdfsd', 'close', '2018-04-09 01:38:15', '2018-04-10 00:25:20', NULL),
(4, 3, 1, NULL, 'Exam issue', 'medium', 'sdfsdfsdfsd', 'close', '2018-04-09 01:38:35', '2018-04-10 01:44:19', NULL),
(5, 3, 1, NULL, 'Exam Issue', 'low', 'fddfgfdg', 'replied', '2018-04-09 01:39:28', '2018-04-10 04:06:33', NULL),
(6, 1, 3, 4, '', '', 'Reply test', 'replied', '2018-04-10 01:38:35', '2018-04-10 00:23:26', NULL),
(7, 1, 3, 4, NULL, NULL, 'Hi', 'open', '2018-04-10 01:31:22', '2018-04-10 01:31:22', NULL),
(8, 1, 3, 4, NULL, NULL, 'dfgfdgfdgfdg', 'open', '2018-04-10 01:35:09', '2018-04-10 01:35:09', NULL),
(9, 1, 3, 4, NULL, NULL, 'dfgfdg', 'open', '2018-04-10 01:36:05', '2018-04-10 01:36:05', NULL),
(10, 1, 3, 4, NULL, NULL, 'dfgdfgdfgdf', 'open', '2018-04-10 01:36:59', '2018-04-10 01:36:59', NULL),
(11, 1, 3, 4, NULL, NULL, 'dfgdfgfd', 'open', '2018-04-10 01:38:09', '2018-04-10 01:38:09', NULL),
(12, 1, 3, 5, NULL, NULL, 'fghfghfgh', 'open', '2018-04-10 01:39:01', '2018-04-10 01:39:01', NULL),
(13, 1, 3, 5, NULL, NULL, 'fghfghfghfghfg', 'open', '2018-04-10 01:39:22', '2018-04-10 01:39:22', NULL),
(14, 1, 3, 5, NULL, NULL, 'fghfghfghfg', 'open', '2018-04-10 01:39:36', '2018-04-10 01:39:36', NULL),
(15, 1, 3, 4, NULL, NULL, 'fhfghfghfghfg', 'open', '2018-04-10 01:44:02', '2018-04-10 01:44:02', NULL),
(16, 1, 3, 5, NULL, NULL, 'dfgdfgdfg', 'open', '2018-04-10 04:06:33', '2018-04-10 04:06:33', NULL),
(17, 3, 1, NULL, 'Test Issue', 'high', 'Sample', 'open', '2018-04-10 04:31:12', '2018-04-10 04:31:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_test_code_generated`
--

CREATE TABLE `tb_test_code_generated` (
  `id` int(11) NOT NULL,
  `learner_id` int(11) NOT NULL,
  `key_code` varchar(5000) NOT NULL,
  `course_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `attempt` int(11) NOT NULL,
  `is_used` enum('yes','no') NOT NULL DEFAULT 'no',
  `is_pass` enum('yes','no') DEFAULT NULL,
  `percentage` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_test_code_generated`
--

INSERT INTO `tb_test_code_generated` (`id`, `learner_id`, `key_code`, `course_id`, `lesson_id`, `attempt`, `is_used`, `is_pass`, `percentage`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, '411021', 1, 1, 1, 'yes', 'yes', 50, '2018-04-18 02:23:58', '2018-04-18 02:23:58', NULL),
(2, 4, '6199017788', 2, 1, 1, 'yes', 'no', 0, '2018-04-18 02:24:33', '2018-04-18 02:24:33', NULL),
(3, 4, '4937335316', 2, 1, 1, 'yes', NULL, NULL, '2018-04-18 02:27:52', '2018-04-18 02:27:52', NULL),
(4, 4, '1257413061', 1, 1, 1, 'no', NULL, NULL, '2018-04-18 05:42:47', '2018-04-18 05:42:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_test_type`
--

CREATE TABLE `tb_test_type` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_test_type`
--

INSERT INTO `tb_test_type` (`id`, `name`, `deleted_at`) VALUES
(1, 'Straight set', NULL),
(2, 'Random set', NULL),
(3, 'Poll based(Yes or No)', NULL),
(0, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role_id` int(6) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(100) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` int(5) NOT NULL DEFAULT '0',
  `authorPlan` int(5) DEFAULT '0',
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `login_attempt` tinyint(2) DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `reminder` varchar(64) DEFAULT NULL,
  `activation` varchar(50) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `educational` varchar(100) DEFAULT NULL,
  `professional` varchar(100) DEFAULT NULL,
  `monthly_income` varchar(100) DEFAULT NULL,
  `employment` varchar(100) DEFAULT NULL,
  `dependants` varchar(100) DEFAULT NULL,
  `aspiration` varchar(100) DEFAULT NULL,
  `id_card` varchar(100) DEFAULT NULL,
  `last_activity` int(20) DEFAULT NULL,
  `experience` int(20) DEFAULT NULL,
  `country_code` varchar(100) DEFAULT NULL,
  `phone_number` bigint(20) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `zip_code` bigint(20) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `about_you` mediumtext,
  `android` varchar(100) DEFAULT NULL,
  `ios` varchar(100) DEFAULT NULL,
  `device` varchar(100) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `bio` text,
  `all_userdata` int(11) DEFAULT NULL,
  `age` varchar(50) DEFAULT NULL,
  `company1` varchar(100) DEFAULT NULL,
  `quiz` enum('1','2','3','4') NOT NULL DEFAULT '1',
  `mobile_uid` bigint(20) NOT NULL DEFAULT '0',
  `gsd_number` varchar(100) DEFAULT NULL,
  `entry_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `username`, `password`, `email`, `name`, `last_name`, `avatar`, `dob`, `gender`, `authorPlan`, `active`, `login_attempt`, `last_login`, `created_at`, `updated_at`, `deleted_at`, `reminder`, `activation`, `remember_token`, `educational`, `professional`, `monthly_income`, `employment`, `dependants`, `aspiration`, `id_card`, `last_activity`, `experience`, `country_code`, `phone_number`, `address`, `city`, `state`, `zip_code`, `country`, `about_you`, `android`, `ios`, `device`, `designation`, `rating`, `bio`, `all_userdata`, `age`, `company1`, `quiz`, `mobile_uid`, `gsd_number`, `entry_by`) VALUES
(1, 1, 'Superadmin', '$2y$10$kp.E/KvmnT4zwalCUzgKWOBw2yOp6FZyayyL.NIEQE7lsiRj4JOIa', 'admin@sdreamtech.com', 'admin123', 'admin', '1522472785-apples-burn-fat-400x400.jpg', NULL, 0, 0, 1, 12, '2017-12-21 05:27:35', '2014-03-12 03:48:46', '2018-03-30 23:36:25', NULL, 'SNLyM4Smv12Ck8jyopZJMfrypTbEDtVhGT5PMRzxs', NULL, 'DGkUQ4aVtlVMT89qtKYMHkREKsrOAhzs64yzlm4mQ5LMsZHnNeBtt4nxxgwD', 'dsf', '1', '3435345', 'dsf', 'sdf', 'sdf', 'P_I_11507208189.jpg', 1514005293, 4, NULL, 0, 'xxxxx', 'Madurai', 'TN', 452233, 'India', NULL, '', '', '', '', 0, 'PHP programmers create, launch, and maintain websites using the HTML-embedded scripting language known as Hypertext Preprocessor. Courses in PHP programming are usually taken as part of a whole certificate or degree program. Read on to learn more about what these courses entail.\r\n\r\nPHP programming classes are available through online and traditional on-campus certificate and undergraduate degree programs in computer programming, information technology, and Web design. PHP courses in such programs may either be part of the required coursework or be offered as electives. In addition, some certificate programs with a PHP specialization exist. The certificate programs available tend to include coursework mainly in the area of programming or Web design. Associate\'s and bachelor\'s degree programs provide students with additional skills in systems analysis, network management, database management and operating systems.\r\n', 1, '', '', '1', 0, '', ''),
(2, 3, 'Alagappa University', '$2y$10$slSh6sFIjp3y5r5Za7s/g.PMPrLdCYv86x5nOVhRTok8vKQoAI9v.', 'alagappa@gmail.com', 'Alagappa University', NULL, '1522384539-alagappa.JPG', NULL, 0, 0, 1, 0, NULL, '2018-03-29 23:05:39', '2018-03-29 23:05:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9865325241, 'Karaikudi,Sivagangai', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 0, NULL, NULL),
(3, 4, 'Vani', '$2y$10$4MbrYdaxpka/.lpKqljfl.LfrCPWCHpIwLHimZQd3LPPqPFBolHcm', 'vani@gmail.com', 'Vani', NULL, '1522384647-teacher1.jpg', '1992-03-15', 0, 0, 1, 0, NULL, '2018-03-29 23:07:27', '2018-03-29 23:07:27', NULL, NULL, NULL, NULL, 'BE', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9092330987, 'Madurai, Tamil Nadu, India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 0, NULL, NULL),
(4, 2, 'Learner 1', '$2y$10$BC0.qOQP9S.XsjEjEwQCC.hPkkrCdNsLryPyRWLHKFPyJb1Mz4OLe', 'learner1@gmail.com', 'Learner 1', NULL, '1523081330-hanuman.jpg', '1992-04-10', 0, 0, 1, 0, NULL, '2018-04-05 23:11:49', '2018-04-07 00:38:50', NULL, NULL, NULL, 'fK91ZuVLnZl512hTWPDaEhp3SYiwqzNC9uLvNUjwxGqdHdoptHez1Aw5VMAM', 'BE', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9092330987, 'Madurai, Tamil Nadu, India', NULL, NULL, NULL, NULL, 'I am afghfghfg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 0, NULL, NULL),
(5, 2, 'test', '$2y$10$/.m91EdqBAbcpfVgg1fjIeMC/l8Swoh0gUxlkSm.hR3ppmTt48gYK', 'test@123.com', 'test', NULL, NULL, NULL, 0, 0, 1, 0, NULL, '2018-05-16 00:25:36', '2018-05-16 00:25:36', NULL, NULL, NULL, 'DSW0pPMBfvPC6NENxqTUHdg0W9Cc78jVE0JCXiYZPHPbdwHBDRWUNVpJ7t15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 625016, NULL, 'Kochi', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 0, NULL, NULL),
(6, 8, 'test', '$2y$10$XMRsD7yCsWswk3VvI3taw.JWHYhfwq2.D7vvM6PzqwSKJx6txzUk6', 'test@te123st.com', 'test', NULL, NULL, NULL, 0, 0, 1, 0, NULL, '2018-05-16 00:26:21', '2018-05-16 00:26:21', NULL, NULL, NULL, '65VEIM1MNv41X1pnp7lOPkUlqazm1q8lMKKhjSYCEZXS1MydT8FrMrkDBK33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 625016, NULL, 'New York County', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 0, NULL, NULL),
(7, 8, 'praz', '$2y$10$wOco8z0zZm1db8X8bC7TsOFL5TJlbZOLoCDQNY70C/YGiBy26/OZ.', 'prasanna@sdreamtech.com', 'praz', NULL, NULL, NULL, 0, 0, 1, 0, NULL, '2018-05-16 06:57:37', '2018-05-16 06:57:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 987654, NULL, 'Madurai', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 0, NULL, NULL),
(8, 5, 'author', '$2y$10$Dgk8iF1A7n2H0UXd9caFJuyFRynZi/8kdxXZBAqAPTUOe2q1S75WK', 'author@gmail.com', 'author', NULL, NULL, NULL, 0, 2, 1, 0, NULL, '2018-05-16 23:28:55', '2018-05-16 23:28:55', NULL, NULL, NULL, '4YI3gaQOdQa90vNNGgpK0vtiUJRK5z93yzQdECEp597DNaAq20pQGlQzegyq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 976543210, NULL, 'tamilNadu', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 0, NULL, NULL),
(9, 2, 'asdsad', '$2y$10$Ry.khY.LzuREO7r/RjGjLeLWaLIOiVIkQ8DBrvqDSKz/MqdpJ0NR6', 'sadasd@asd.asd', 'asdsad', NULL, NULL, NULL, 0, 0, 1, 0, NULL, '2018-06-29 00:53:44', '2018-06-29 00:53:44', NULL, NULL, NULL, 'mn2Q9TIvhj9wP3vfTsE3XISmMOjtk3wi9D1N0RZpLkBQN2vOEGbEHC5knioI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 987654321, NULL, 'Madurai', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 0, NULL, NULL),
(10, 2, 'asdsad', '$2y$10$9HtDLxVgtgbsO62AESjrMespOgqC0ytCe1cwN796gTr76d1sMDZBC', 'ceep@ceep.dev', 'asdsad', NULL, NULL, NULL, 0, 0, 1, 0, NULL, '2018-06-29 00:54:40', '2018-06-29 00:54:40', NULL, NULL, NULL, '4rhKPIud7CpEonkVBMXbYMLYlwZtqQIDwFg3aUFb8eq8ZFjhbRvTTvmJYyEe', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 625016, NULL, 'Sao Paulo', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 0, NULL, NULL),
(11, 2, 'fsdffs', '$2y$10$FkS/lLUMWqimnXx/BnwES.uijbUM1WsDw2Ogzl4MVOYD.YI9iOIoe', 'arun@abs.com', 'fsdffs', NULL, NULL, NULL, 0, 0, 1, 0, NULL, '2018-06-29 01:08:03', '2018-06-29 01:08:03', NULL, NULL, NULL, 'QR2o48PSFsH14j3JsxHrZnQ3vEzej4vD1F4OGFUFqdLRjXvRwtXeP7eSxN8F', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 625016, NULL, 'Sao Paulo', NULL, NULL, 'Select Country', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 0, NULL, NULL),
(12, 2, 'student', '$2y$10$yIAn2EPwAbEZcPPBRYaH.Oow5wGnknRbWGFpWdNz0E2DDsxTnNCne', 'student@gmail.com', 'student', NULL, NULL, '2018-07-03', 1, 0, 1, 0, NULL, '2018-07-05 00:11:36', '2018-07-06 02:44:30', NULL, NULL, NULL, 'd2RkF0BPccaHjkKWHgdR5j9hLAHO7doH9JS8biC2flZKjFKmqmKFwb8W4r3i', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9876543210, NULL, 'Madurai', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 0, NULL, NULL),
(13, 4, 'rani', '$2y$10$/Wg5wk3OaGFrtnTIwewogOhABn2Qs4Vkj.h//KJf/qUfOMY9Uzflm', 'rani@gmail.com', 'rani', NULL, NULL, '2018-07-04', 0, 0, 1, 0, NULL, '2018-07-19 01:01:01', '2018-07-19 01:01:01', NULL, NULL, NULL, NULL, 'bed', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9876543210, 'BL14LG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_logs`
--

CREATE TABLE `users_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `action` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_model` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_logs`
--

INSERT INTO `users_logs` (`id`, `user_id`, `action`, `action_model`, `action_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'created', 'users', 2, '2018-02-26 00:01:18', '2018-02-26 00:01:18'),
(2, 2, 'updated', 'users', 2, '2018-02-26 00:02:45', '2018-02-26 00:02:45'),
(3, 1, 'updated', 'tb_users', 1, '2018-02-26 01:20:07', '2018-02-26 01:20:07'),
(4, 1, 'updated', 'tb_users', 1, '2018-02-26 01:22:17', '2018-02-26 01:22:17'),
(5, 1, 'updated', 'tb_users', 1, '2018-02-26 01:23:03', '2018-02-26 01:23:03'),
(6, 1, 'updated', 'tb_users', 1, '2018-02-26 01:28:12', '2018-02-26 01:28:12'),
(7, 1, 'updated', 'tb_users', 1, '2018-02-26 01:28:30', '2018-02-26 01:28:30'),
(8, 1, 'updated', 'tb_users', 1, '2018-02-26 01:37:27', '2018-02-26 01:37:27'),
(9, 1, 'updated', 'tb_users', 1, '2018-02-26 01:45:05', '2018-02-26 01:45:05'),
(10, 1, 'created', 'tb_categories_type', 1, '2018-02-26 01:46:42', '2018-02-26 01:46:42'),
(11, 1, 'created', 'tb_professional', 1, '2018-02-26 02:22:42', '2018-02-26 02:22:42'),
(12, 1, 'created', 'tb_users', 2, '2018-02-26 02:32:28', '2018-02-26 02:32:28'),
(13, 1, 'updated', 'tb_users', 2, '2018-02-26 02:32:55', '2018-02-26 02:32:55'),
(14, 1, 'created', 'tb_courses', 1, '2018-02-26 02:35:35', '2018-02-26 02:35:35'),
(15, 1, 'updated', 'tb_courses', 1, '2018-02-26 02:47:42', '2018-02-26 02:47:42'),
(16, 1, 'updated', 'tb_courses', 1, '2018-02-26 02:57:01', '2018-02-26 02:57:01'),
(17, 1, 'updated', 'tb_courses', 1, '2018-02-26 02:57:39', '2018-02-26 02:57:39'),
(18, 1, 'updated', 'tb_courses', 1, '2018-02-26 02:59:51', '2018-02-26 02:59:51'),
(19, 1, 'created', 'sdream_countries', 1, '2018-02-26 07:09:51', '2018-02-26 07:09:51'),
(20, 1, 'created', 'sdream_states', 1, '2018-02-26 07:09:56', '2018-02-26 07:09:56'),
(21, 1, 'created', 'tb_lession', 1, '2018-02-27 01:10:22', '2018-02-27 01:10:22'),
(22, 1, 'created', 'tb_lession', 2, '2018-02-27 01:12:25', '2018-02-27 01:12:25'),
(23, 1, 'updated', 'tb_lession', 2, '2018-02-27 01:46:25', '2018-02-27 01:46:25'),
(24, 1, 'deleted', 'tb_lession', 2, '2018-02-27 02:00:35', '2018-02-27 02:00:35'),
(25, 1, 'created', 'tb_courses', 2, '2018-02-27 02:24:25', '2018-02-27 02:24:25'),
(26, 1, 'created', 'tb_lession', 3, '2018-02-27 02:25:16', '2018-02-27 02:25:16'),
(27, 1, 'created', 'tb_languages', 1, '2018-02-27 07:25:42', '2018-02-27 07:25:42'),
(28, 1, 'updated', 'sdream_countries', 1, '2018-02-27 07:34:28', '2018-02-27 07:34:28'),
(29, 1, 'updated', 'sdream_states', 1, '2018-02-27 07:35:49', '2018-02-27 07:35:49'),
(30, 1, 'created', 'sdream_cities', 1, '2018-02-27 07:40:47', '2018-02-27 07:40:47'),
(31, 1, 'updated', 'tb_lession', 1, '2018-03-01 01:39:23', '2018-03-01 01:39:23'),
(32, 1, 'updated', 'tb_lession', 1, '2018-03-01 23:03:46', '2018-03-01 23:03:46'),
(33, 1, 'updated', 'tb_lession', 1, '2018-03-01 23:04:15', '2018-03-01 23:04:15'),
(34, 1, 'updated', 'tb_lession', 1, '2018-03-01 23:09:06', '2018-03-01 23:09:06'),
(35, 1, 'updated', 'tb_courses', 1, '2018-03-03 05:44:53', '2018-03-03 05:44:53'),
(36, 1, 'updated', 'users', 1, '2018-03-05 06:52:34', '2018-03-05 06:52:34'),
(37, 1, 'updated', 'users', 1, '2018-03-05 06:54:13', '2018-03-05 06:54:13'),
(38, 2, 'updated', 'users', 2, '2018-03-05 06:54:45', '2018-03-05 06:54:45'),
(39, 1, 'created', 'users', 4, '2018-03-06 04:15:29', '2018-03-06 04:15:29'),
(40, 1, 'created', 'users', 5, '2018-03-06 04:17:03', '2018-03-06 04:17:03'),
(41, 1, 'updated', 'users', 5, '2018-03-06 04:19:20', '2018-03-06 04:19:20'),
(42, 1, 'updated', 'users', 5, '2018-03-06 04:19:31', '2018-03-06 04:19:31'),
(43, 1, 'created', 'users', 6, '2018-03-06 04:20:34', '2018-03-06 04:20:34'),
(44, 1, 'updated', 'users', 6, '2018-03-06 04:21:02', '2018-03-06 04:21:02'),
(45, 1, 'updated', 'users', 6, '2018-03-06 04:21:21', '2018-03-06 04:21:21'),
(46, 1, 'created', 'users', 7, '2018-03-06 04:29:47', '2018-03-06 04:29:47'),
(47, 1, 'updated', 'users', 7, '2018-03-06 04:30:37', '2018-03-06 04:30:37'),
(48, 1, 'updated', 'users', 7, '2018-03-06 04:30:41', '2018-03-06 04:30:41'),
(49, 2, 'updated', 'users', 2, '2018-03-06 05:51:53', '2018-03-06 05:51:53'),
(50, 2, 'updated', 'users', 2, '2018-03-06 05:52:57', '2018-03-06 05:52:57'),
(51, 2, 'updated', 'users', 2, '2018-03-06 05:54:41', '2018-03-06 05:54:41'),
(52, 2, 'updated', 'users', 2, '2018-03-06 05:54:49', '2018-03-06 05:54:49'),
(53, 2, 'updated', 'users', 2, '2018-03-06 05:54:52', '2018-03-06 05:54:52'),
(54, 2, 'updated', 'users', 2, '2018-03-06 05:54:56', '2018-03-06 05:54:56'),
(55, 2, 'updated', 'users', 2, '2018-03-06 05:55:02', '2018-03-06 05:55:02'),
(56, 2, 'updated', 'users', 2, '2018-03-06 05:57:18', '2018-03-06 05:57:18'),
(57, 2, 'updated', 'users', 2, '2018-03-06 05:57:22', '2018-03-06 05:57:22'),
(58, 2, 'updated', 'users', 2, '2018-03-06 05:57:55', '2018-03-06 05:57:55'),
(59, 2, 'updated', 'users', 2, '2018-03-06 05:58:05', '2018-03-06 05:58:05'),
(60, 2, 'updated', 'users', 2, '2018-03-06 05:58:20', '2018-03-06 05:58:20'),
(61, 1, 'updated', 'users', 1, '2018-03-08 03:29:29', '2018-03-08 03:29:29'),
(62, 1, 'created', 'tb_courses', 3, '2018-03-19 05:41:08', '2018-03-19 05:41:08'),
(63, 1, 'created', 'tb_courses', 4, '2018-03-20 01:04:52', '2018-03-20 01:04:52'),
(64, 1, 'created', 'tb_courses', 5, '2018-03-20 01:09:28', '2018-03-20 01:09:28'),
(65, 2, 'deleted', 'tb_courses', 5, '2018-03-21 00:08:36', '2018-03-21 00:08:36'),
(66, 1, 'created', 'tb_courses', 6, '2018-03-21 23:54:50', '2018-03-21 23:54:50'),
(67, 1, 'created', 'tb_languages_settings', 1, '2018-03-21 23:56:52', '2018-03-21 23:56:52'),
(68, 1, 'created', 'tb_courses', 7, '2018-03-21 23:58:52', '2018-03-21 23:58:52'),
(69, 1, 'updated', 'tb_courses', 7, '2018-03-21 23:59:03', '2018-03-21 23:59:03'),
(70, 1, 'created', 'tb_courses', 8, '2018-03-22 00:00:02', '2018-03-22 00:00:02'),
(71, 1, 'created', 'tb_languages_settings', 2, '2018-03-22 00:00:47', '2018-03-22 00:00:47'),
(72, 1, 'updated', 'tb_languages_settings', 2, '2018-03-22 00:00:57', '2018-03-22 00:00:57'),
(73, 1, 'created', 'tb_languages_settings', 3, '2018-03-22 00:01:26', '2018-03-22 00:01:26'),
(74, 1, 'updated', 'tb_categories_type', 1, '2018-03-22 01:04:29', '2018-03-22 01:04:29'),
(75, 1, 'created', 'tb_categories_type', 2, '2018-03-22 01:04:38', '2018-03-22 01:04:38'),
(76, 1, 'created', 'tb_categories_type', 3, '2018-03-22 01:04:47', '2018-03-22 01:04:47'),
(77, 1, 'created', 'tb_categories_type', 4, '2018-03-22 01:04:56', '2018-03-22 01:04:56'),
(78, 1, 'created', 'tb_categories_type', 5, '2018-03-22 01:05:02', '2018-03-22 01:05:02'),
(79, 1, 'created', 'tb_categories_type', 6, '2018-03-22 01:05:09', '2018-03-22 01:05:09'),
(80, 1, 'created', 'tb_courses', 9, '2018-03-22 01:15:46', '2018-03-22 01:15:46'),
(81, 1, 'updated', 'tb_courses', 9, '2018-03-22 01:16:14', '2018-03-22 01:16:14'),
(82, 1, 'updated', 'tb_courses', 4, '2018-03-22 01:17:31', '2018-03-22 01:17:31'),
(83, 1, 'updated', 'tb_courses', 4, '2018-03-22 01:17:59', '2018-03-22 01:17:59'),
(84, 1, 'updated', 'tb_courses', 3, '2018-03-22 01:18:48', '2018-03-22 01:18:48'),
(85, 1, 'created', 'tb_languages_settings', 4, '2018-03-22 01:19:40', '2018-03-22 01:19:40'),
(86, 1, 'created', 'tb_languages_settings', 5, '2018-03-22 01:19:52', '2018-03-22 01:19:52'),
(87, 1, 'updated', 'tb_courses', 1, '2018-03-22 01:20:30', '2018-03-22 01:20:30'),
(88, 1, 'created', 'tb_languages_settings', 6, '2018-03-22 01:20:45', '2018-03-22 01:20:45'),
(89, 1, 'created', 'tb_languages_settings', 7, '2018-03-22 01:21:21', '2018-03-22 01:21:21'),
(90, 1, 'created', 'sdream_countries', 2, '2018-03-22 05:29:45', '2018-03-22 05:29:45'),
(91, 1, 'updated', 'users', 1, '2018-03-24 07:51:22', '2018-03-24 07:51:22'),
(92, 1, 'updated', 'users', 1, '2018-03-27 02:16:47', '2018-03-27 02:16:47'),
(93, 1, 'created', 'tb_course_assign', 2, '2018-03-27 23:08:05', '2018-03-27 23:08:05'),
(94, 1, 'updated', 'tb_course_assign', 1, '2018-03-27 23:37:45', '2018-03-27 23:37:45'),
(95, 1, 'updated', 'tb_course_assign', 1, '2018-03-27 23:42:16', '2018-03-27 23:42:16'),
(96, 1, 'deleted', 'tb_course_assign', 1, '2018-03-28 00:14:33', '2018-03-28 00:14:33'),
(97, 1, 'updated', 'tb_courses', 3, '2018-03-28 00:55:36', '2018-03-28 00:55:36'),
(98, 1, 'updated', 'tb_courses', 2, '2018-03-28 00:56:08', '2018-03-28 00:56:08'),
(99, 1, 'updated', 'tb_courses', 2, '2018-03-28 01:03:24', '2018-03-28 01:03:24'),
(100, 1, 'updated', 'tb_courses', 3, '2018-03-28 01:06:16', '2018-03-28 01:06:16'),
(101, 1, 'updated', 'tb_courses', 2, '2018-03-28 01:13:01', '2018-03-28 01:13:01'),
(102, 1, 'updated', 'tb_courses', 2, '2018-03-28 01:22:13', '2018-03-28 01:22:13'),
(103, 1, 'updated', 'tb_courses', 2, '2018-03-28 01:25:02', '2018-03-28 01:25:02'),
(104, 1, 'updated', 'tb_courses', 2, '2018-03-28 01:26:16', '2018-03-28 01:26:16'),
(105, 1, 'updated', 'tb_courses', 2, '2018-03-28 01:27:47', '2018-03-28 01:27:47'),
(106, 1, 'updated', 'tb_courses', 2, '2018-03-28 01:30:50', '2018-03-28 01:30:50'),
(107, 1, 'updated', 'tb_courses', 2, '2018-03-28 01:35:18', '2018-03-28 01:35:18'),
(108, 1, 'updated', 'tb_courses', 9, '2018-03-28 01:39:27', '2018-03-28 01:39:27'),
(109, 1, 'created', 'ccc', 1, '2018-03-28 02:08:00', '2018-03-28 02:08:00'),
(110, 1, 'created', 'tb_languages_settings', 8, '2018-03-28 02:10:14', '2018-03-28 02:10:14'),
(111, 1, 'updated', 'users', 1, '2018-03-28 07:26:58', '2018-03-28 07:26:58'),
(112, 2, 'updated', 'tb_courses', 6, '2018-03-29 00:19:59', '2018-03-29 00:19:59'),
(113, 2, 'updated', 'tb_courses', 6, '2018-03-29 00:34:14', '2018-03-29 00:34:14'),
(114, 1, 'updated', 'tb_courses', 6, '2018-03-29 07:20:36', '2018-03-29 07:20:36'),
(115, 1, 'updated', 'tb_courses', 6, '2018-03-29 07:20:40', '2018-03-29 07:20:40'),
(116, 1, 'updated', 'tb_courses', 6, '2018-03-29 07:23:26', '2018-03-29 07:23:26'),
(117, 1, 'updated', 'tb_courses', 6, '2018-03-29 07:23:54', '2018-03-29 07:23:54'),
(118, 1, 'updated', 'tb_courses', 6, '2018-03-29 07:24:15', '2018-03-29 07:24:15'),
(119, 1, 'updated', 'tb_courses', 6, '2018-03-29 07:24:35', '2018-03-29 07:24:35'),
(120, 1, 'updated', 'tb_courses', 6, '2018-03-29 07:25:46', '2018-03-29 07:25:46'),
(121, 1, 'updated', 'tb_courses', 6, '2018-03-29 07:26:25', '2018-03-29 07:26:25'),
(122, 1, 'created', 'sdream_countries', 1, '2018-03-29 22:53:07', '2018-03-29 22:53:07'),
(123, 1, 'updated', 'sdream_countries', 1, '2018-03-29 22:53:14', '2018-03-29 22:53:14'),
(124, 1, 'created', 'sdream_states', 1, '2018-03-29 22:53:26', '2018-03-29 22:53:26'),
(125, 1, 'created', 'sdream_cities', 1, '2018-03-29 22:53:49', '2018-03-29 22:53:49'),
(126, 1, 'created', 'sdream_cities', 2, '2018-03-29 22:53:59', '2018-03-29 22:53:59'),
(127, 1, 'created', 'users', 2, '2018-03-29 23:05:39', '2018-03-29 23:05:39'),
(128, 1, 'created', 'users', 3, '2018-03-29 23:07:28', '2018-03-29 23:07:28'),
(129, 1, 'created', 'tb_courses', 1, '2018-03-29 23:26:27', '2018-03-29 23:26:27'),
(130, 1, 'updated', 'tb_courses', 1, '2018-03-29 23:32:30', '2018-03-29 23:32:30'),
(131, 1, 'updated', 'tb_courses', 1, '2018-03-29 23:42:00', '2018-03-29 23:42:00'),
(132, 3, 'updated', 'tb_courses', 1, '2018-03-30 04:21:12', '2018-03-30 04:21:12'),
(133, 1, 'updated', 'tb_courses', 1, '2018-03-30 04:21:38', '2018-03-30 04:21:38'),
(134, 1, 'created', 'tb_languages_settings', 1, '2018-03-30 05:27:12', '2018-03-30 05:27:12'),
(135, 1, 'updated', 'tb_languages_settings', 1, '2018-03-30 05:28:06', '2018-03-30 05:28:06'),
(136, 1, 'created', 'tb_languages_settings', 2, '2018-03-30 05:28:59', '2018-03-30 05:28:59'),
(137, 1, 'created', 'tb_languages', 2, '2018-03-30 06:20:35', '2018-03-30 06:20:35'),
(138, 1, 'created', 'tb_languages_settings', 4, '2018-03-30 06:23:07', '2018-03-30 06:23:07'),
(139, 1, 'created', 'tb_languages_settings', 5, '2018-03-30 06:23:26', '2018-03-30 06:23:26'),
(140, 1, 'created', 'tb_languages_settings', 6, '2018-03-30 06:25:34', '2018-03-30 06:25:34'),
(141, 1, 'created', 'tb_languages_settings', 7, '2018-03-30 06:26:31', '2018-03-30 06:26:31'),
(142, 1, 'created', 'tb_languages_settings', 8, '2018-03-30 06:27:51', '2018-03-30 06:27:51'),
(143, 1, 'created', 'tb_languages_settings', 9, '2018-03-30 06:28:15', '2018-03-30 06:28:15'),
(144, 1, 'created', 'tb_languages_settings', 10, '2018-03-30 06:31:35', '2018-03-30 06:31:35'),
(145, 1, 'created', 'tb_languages_settings', 11, '2018-03-30 06:31:50', '2018-03-30 06:31:50'),
(146, 1, 'created', 'tb_course_assign', 1, '2018-03-30 07:44:50', '2018-03-30 07:44:50'),
(147, 1, 'updated', 'users', 1, '2018-03-30 22:20:07', '2018-03-30 22:20:07'),
(148, 1, 'created', 'tb_courses', 4, '2018-03-30 22:31:18', '2018-03-30 22:31:18'),
(149, 1, 'created', 'tb_courses', 5, '2018-03-30 23:15:39', '2018-03-30 23:15:39'),
(150, 1, 'created', 'tb_courses', 6, '2018-03-30 23:16:07', '2018-03-30 23:16:07'),
(151, 1, 'created', 'tb_courses', 7, '2018-03-30 23:16:25', '2018-03-30 23:16:25'),
(152, 1, 'created', 'tb_courses', 8, '2018-03-30 23:18:52', '2018-03-30 23:18:52'),
(153, 1, 'created', 'tb_courses', 9, '2018-03-30 23:20:34', '2018-03-30 23:20:34'),
(154, 1, 'created', 'tb_courses', 10, '2018-03-30 23:25:35', '2018-03-30 23:25:35'),
(155, 1, 'updated', 'users', 1, '2018-03-30 23:36:25', '2018-03-30 23:36:25'),
(156, 1, 'updated', 'users', 1, '2018-04-03 00:41:30', '2018-04-03 00:41:30'),
(157, 1, 'updated', 'users', 1, '2018-04-03 00:42:16', '2018-04-03 00:42:16'),
(158, 1, 'created', 'tb_courses', 11, '2018-04-05 06:29:42', '2018-04-05 06:29:42'),
(159, 1, 'created', 'tb_courses', 12, '2018-04-05 06:30:47', '2018-04-05 06:30:47'),
(160, 4, 'updated', 'users', 4, '2018-04-06 00:49:55', '2018-04-06 00:49:55'),
(161, 4, 'updated', 'users', 4, '2018-04-06 00:51:33', '2018-04-06 00:51:33'),
(162, 4, 'updated', 'users', 4, '2018-04-06 00:51:50', '2018-04-06 00:51:50'),
(163, 4, 'updated', 'users', 4, '2018-04-06 01:20:37', '2018-04-06 01:20:37'),
(164, 4, 'updated', 'users', 4, '2018-04-06 01:39:32', '2018-04-06 01:39:32'),
(165, 4, 'updated', 'users', 4, '2018-04-06 01:59:00', '2018-04-06 01:59:00'),
(166, 4, 'updated', 'users', 4, '2018-04-06 02:02:58', '2018-04-06 02:02:58'),
(167, 4, 'updated', 'users', 4, '2018-04-06 02:04:19', '2018-04-06 02:04:19'),
(168, 4, 'updated', 'users', 4, '2018-04-06 02:49:48', '2018-04-06 02:49:48'),
(169, 3, 'created', 'tb_support_system', 1, '2018-04-09 01:15:40', '2018-04-09 01:15:40'),
(170, 3, 'created', 'tb_support_system', 2, '2018-04-09 01:37:51', '2018-04-09 01:37:51'),
(171, 3, 'created', 'tb_support_system', 3, '2018-04-09 01:38:15', '2018-04-09 01:38:15'),
(172, 3, 'created', 'tb_support_system', 4, '2018-04-09 01:38:35', '2018-04-09 01:38:35'),
(173, 3, 'created', 'tb_support_system', 5, '2018-04-09 01:39:28', '2018-04-09 01:39:28'),
(174, 1, 'updated', 'tb_support_system', 1, '2018-04-09 02:43:11', '2018-04-09 02:43:11'),
(175, 1, 'updated', 'tb_support_system', 2, '2018-04-09 07:01:48', '2018-04-09 07:01:48'),
(176, 1, 'updated', 'tb_support_system', 3, '2018-04-09 23:56:53', '2018-04-09 23:56:53'),
(177, 1, 'updated', 'tb_support_system', 3, '2018-04-10 00:01:46', '2018-04-10 00:01:46'),
(178, 1, 'updated', 'tb_support_system', 4, '2018-04-10 00:05:11', '2018-04-10 00:05:11'),
(179, 1, 'updated', 'tb_support_system', 3, '2018-04-10 00:14:05', '2018-04-10 00:14:05'),
(180, 1, 'updated', 'tb_support_system', 4, '2018-04-10 00:15:48', '2018-04-10 00:15:48'),
(181, 1, 'updated', 'tb_support_system', 5, '2018-04-10 00:21:36', '2018-04-10 00:21:36'),
(182, 1, 'updated', 'tb_support_system', 3, '2018-04-10 00:22:53', '2018-04-10 00:22:53'),
(183, 1, 'updated', 'tb_support_system', 4, '2018-04-10 00:23:26', '2018-04-10 00:23:26'),
(184, 1, 'updated', 'tb_support_system', 3, '2018-04-10 00:25:20', '2018-04-10 00:25:20'),
(185, 1, 'created', 'tb_support_system', 7, '2018-04-10 01:31:23', '2018-04-10 01:31:23'),
(186, 1, 'created', 'tb_support_system', 8, '2018-04-10 01:35:09', '2018-04-10 01:35:09'),
(187, 1, 'created', 'tb_support_system', 9, '2018-04-10 01:36:05', '2018-04-10 01:36:05'),
(188, 1, 'created', 'tb_support_system', 10, '2018-04-10 01:37:00', '2018-04-10 01:37:00'),
(189, 1, 'created', 'tb_support_system', 11, '2018-04-10 01:38:09', '2018-04-10 01:38:09'),
(190, 1, 'created', 'tb_support_system', 12, '2018-04-10 01:39:01', '2018-04-10 01:39:01'),
(191, 1, 'created', 'tb_support_system', 13, '2018-04-10 01:39:22', '2018-04-10 01:39:22'),
(192, 1, 'created', 'tb_support_system', 14, '2018-04-10 01:39:36', '2018-04-10 01:39:36'),
(193, 1, 'created', 'tb_support_system', 15, '2018-04-10 01:44:02', '2018-04-10 01:44:02'),
(194, 1, 'updated', 'tb_support_system', 4, '2018-04-10 01:44:19', '2018-04-10 01:44:19'),
(195, 1, 'updated', 'tb_support_system', 5, '2018-04-10 04:06:33', '2018-04-10 04:06:33'),
(196, 1, 'created', 'tb_support_system', 16, '2018-04-10 04:06:33', '2018-04-10 04:06:33'),
(197, 3, 'created', 'tb_support_system', 17, '2018-04-10 04:31:12', '2018-04-10 04:31:12'),
(198, 4, 'updated', 'users', 4, '2018-04-10 05:30:57', '2018-04-10 05:30:57'),
(199, 1, 'updated', 'users', 1, '2018-04-13 06:09:14', '2018-04-13 06:09:14'),
(200, 4, 'created', 'tb_test_code_generated', 1, '2018-04-18 02:23:59', '2018-04-18 02:23:59'),
(201, 4, 'created', 'tb_test_code_generated', 2, '2018-04-18 02:24:33', '2018-04-18 02:24:33'),
(202, 4, 'created', 'tb_test_code_generated', 3, '2018-04-18 02:27:53', '2018-04-18 02:27:53'),
(203, 4, 'created', 'tb_test_code_generated', 4, '2018-04-18 05:42:47', '2018-04-18 05:42:47'),
(204, 4, 'created', 'tb_learner_test_store', 1, '2018-04-26 00:46:55', '2018-04-26 00:46:55'),
(205, 4, 'created', 'tb_learner_test_store', 2, '2018-04-26 00:46:55', '2018-04-26 00:46:55'),
(206, 4, 'created', 'tb_learner_test_store', 3, '2018-04-26 00:46:56', '2018-04-26 00:46:56'),
(207, 4, 'created', 'tb_learner_test_store', 4, '2018-04-26 00:46:56', '2018-04-26 00:46:56'),
(208, 4, 'created', 'tb_learner_test_store', 5, '2018-04-26 00:46:56', '2018-04-26 00:46:56'),
(209, 4, 'created', 'tb_learner_test_store', 6, '2018-04-26 00:46:56', '2018-04-26 00:46:56'),
(210, 4, 'created', 'tb_learner_test_store', 7, '2018-04-26 00:50:44', '2018-04-26 00:50:44'),
(211, 4, 'created', 'tb_learner_test_store', 8, '2018-04-26 00:50:44', '2018-04-26 00:50:44'),
(212, 4, 'created', 'tb_learner_test_store', 9, '2018-04-26 00:50:44', '2018-04-26 00:50:44'),
(213, 4, 'created', 'tb_learner_test_store', 10, '2018-04-26 00:50:44', '2018-04-26 00:50:44'),
(214, 4, 'created', 'tb_learner_test_store', 11, '2018-04-26 00:50:44', '2018-04-26 00:50:44'),
(215, 4, 'created', 'tb_learner_test_store', 12, '2018-04-26 00:50:45', '2018-04-26 00:50:45'),
(216, 4, 'created', 'tb_learner_test_store', 13, '2018-04-26 00:51:21', '2018-04-26 00:51:21'),
(217, 4, 'created', 'tb_learner_test_store', 14, '2018-04-26 00:51:21', '2018-04-26 00:51:21'),
(218, 4, 'created', 'tb_learner_test_store', 15, '2018-04-26 00:51:22', '2018-04-26 00:51:22'),
(219, 4, 'created', 'tb_learner_test_store', 16, '2018-04-26 00:51:22', '2018-04-26 00:51:22'),
(220, 4, 'created', 'tb_learner_test_store', 17, '2018-04-26 00:51:22', '2018-04-26 00:51:22'),
(221, 4, 'created', 'tb_learner_test_store', 18, '2018-04-26 00:51:22', '2018-04-26 00:51:22'),
(222, 4, 'created', 'tb_learner_test_store', 1, '2018-04-26 01:08:13', '2018-04-26 01:08:13'),
(223, 4, 'created', 'tb_learner_test_store', 2, '2018-04-26 01:08:13', '2018-04-26 01:08:13'),
(224, 4, 'created', 'tb_learner_test_store', 3, '2018-04-26 01:08:13', '2018-04-26 01:08:13'),
(225, 4, 'created', 'tb_learner_test_store', 4, '2018-04-26 01:08:13', '2018-04-26 01:08:13'),
(226, 4, 'created', 'tb_learner_test_store', 5, '2018-04-26 01:08:13', '2018-04-26 01:08:13'),
(227, 4, 'created', 'tb_learner_test_store', 6, '2018-04-26 01:08:13', '2018-04-26 01:08:13'),
(228, 4, 'created', 'tb_learner_test_store', 1, '2018-04-26 01:09:16', '2018-04-26 01:09:16'),
(229, 4, 'created', 'tb_learner_test_store', 2, '2018-04-26 01:09:16', '2018-04-26 01:09:16'),
(230, 4, 'created', 'tb_learner_test_store', 3, '2018-04-26 01:09:16', '2018-04-26 01:09:16'),
(231, 4, 'created', 'tb_learner_test_store', 4, '2018-04-26 01:09:17', '2018-04-26 01:09:17'),
(232, 4, 'created', 'tb_learner_test_store', 5, '2018-04-26 01:09:17', '2018-04-26 01:09:17'),
(233, 4, 'created', 'tb_learner_test_store', 6, '2018-04-26 01:09:17', '2018-04-26 01:09:17'),
(234, 4, 'created', 'tb_learner_test_store', 1, '2018-04-26 01:14:40', '2018-04-26 01:14:40'),
(235, 4, 'created', 'tb_learner_test_store', 2, '2018-04-26 01:14:40', '2018-04-26 01:14:40'),
(236, 4, 'created', 'tb_learner_test_store', 3, '2018-04-26 01:14:41', '2018-04-26 01:14:41'),
(237, 4, 'created', 'tb_learner_test_store', 4, '2018-04-26 01:14:41', '2018-04-26 01:14:41'),
(238, 4, 'created', 'tb_learner_test_store', 5, '2018-04-26 01:14:41', '2018-04-26 01:14:41'),
(239, 4, 'created', 'tb_learner_test_store', 6, '2018-04-26 01:14:41', '2018-04-26 01:14:41'),
(240, 4, 'created', 'tb_learner_test_store', 7, '2018-04-26 04:47:29', '2018-04-26 04:47:29'),
(241, 4, 'created', 'tb_learner_test_store', 8, '2018-04-26 04:48:02', '2018-04-26 04:48:02'),
(242, 4, 'created', 'tb_learner_test_store', 9, '2018-04-26 04:48:36', '2018-04-26 04:48:36'),
(243, 4, 'created', 'tb_learner_test_store', 10, '2018-04-26 04:48:36', '2018-04-26 04:48:36'),
(244, 4, 'created', 'tb_learner_test_store', 11, '2018-04-26 04:48:36', '2018-04-26 04:48:36'),
(245, 4, 'created', 'tb_learner_test_store', 12, '2018-04-26 04:48:36', '2018-04-26 04:48:36'),
(246, 4, 'created', 'tb_learner_test_store', 13, '2018-04-26 04:48:36', '2018-04-26 04:48:36'),
(247, 4, 'created', 'tb_learner_test_store', 14, '2018-04-26 04:48:37', '2018-04-26 04:48:37'),
(248, 1, 'updated', 'users', 1, '2018-05-02 05:10:20', '2018-05-02 05:10:20'),
(249, 1, 'updated', 'tb_learner_projects', 1, '2018-05-02 07:32:13', '2018-05-02 07:32:13'),
(250, 1, 'updated', 'tb_learner_projects', 1, '2018-05-02 07:32:27', '2018-05-02 07:32:27'),
(251, 1, 'updated', 'tb_learner_projects', 1, '2018-05-02 07:33:14', '2018-05-02 07:33:14'),
(252, 1, 'updated', 'tb_learner_projects', 1, '2018-05-02 07:34:03', '2018-05-02 07:34:03'),
(253, 1, 'updated', 'users', 1, '2018-05-02 23:30:36', '2018-05-02 23:30:36'),
(254, 3, 'updated', 'tb_learner_projects', 2, '2018-05-03 02:14:15', '2018-05-03 02:14:15'),
(255, 1, 'updated', 'users', 1, '2018-05-10 01:48:54', '2018-05-10 01:48:54'),
(256, 4, 'updated', 'users', 4, '2018-05-11 07:22:31', '2018-05-11 07:22:31'),
(257, 1, 'updated', 'users', 1, '2018-05-11 07:58:28', '2018-05-11 07:58:28'),
(258, 1, 'updated', 'users', 1, '2018-05-11 07:59:12', '2018-05-11 07:59:12'),
(259, 1, 'created', 'tb_courses', 14, '2018-05-11 23:36:53', '2018-05-11 23:36:53'),
(260, 1, 'deleted', 'tb_lession', 10, '2018-05-11 23:57:22', '2018-05-11 23:57:22'),
(261, 5, 'updated', 'users', 5, '2018-05-16 00:25:53', '2018-05-16 00:25:53'),
(262, 6, 'updated', 'users', 6, '2018-05-16 00:27:01', '2018-05-16 00:27:01'),
(263, 4, 'updated', 'users', 4, '2018-05-16 00:29:13', '2018-05-16 00:29:13'),
(264, 1, 'updated', 'users', 1, '2018-05-16 01:46:19', '2018-05-16 01:46:19'),
(265, 1, 'updated', 'users', 1, '2018-05-16 06:56:37', '2018-05-16 06:56:37'),
(266, 8, 'updated', 'users', 8, '2018-05-17 00:16:20', '2018-05-17 00:16:20'),
(267, 1, 'created', 'tb_courses', 15, '2018-05-17 01:08:26', '2018-05-17 01:08:26'),
(268, 1, 'created', 'tb_courses', 16, '2018-05-17 01:17:13', '2018-05-17 01:17:13'),
(269, 1, 'created', 'tb_courses', 17, '2018-05-17 01:17:42', '2018-05-17 01:17:42'),
(270, 1, 'created', 'tb_courses', 18, '2018-05-17 01:18:09', '2018-05-17 01:18:09'),
(271, 1, 'created', 'tb_courses', 19, '2018-05-17 04:51:20', '2018-05-17 04:51:20'),
(272, 1, 'created', 'tb_courses', 20, '2018-05-17 04:52:03', '2018-05-17 04:52:03'),
(273, 1, 'created', 'tb_courses', 21, '2018-05-17 04:57:10', '2018-05-17 04:57:10'),
(274, 1, 'created', 'tb_courses', 21, '2018-05-17 04:57:10', '2018-05-17 04:57:10'),
(275, 1, 'created', 'tb_courses', 22, '2018-05-17 04:59:02', '2018-05-17 04:59:02'),
(276, 1, 'created', 'tb_courses', 22, '2018-05-17 04:59:02', '2018-05-17 04:59:02'),
(277, 1, 'updated', 'users', 1, '2018-05-18 04:54:07', '2018-05-18 04:54:07'),
(278, 8, 'created', 'tb_courses', 23, '2018-05-19 02:04:42', '2018-05-19 02:04:42'),
(279, 8, 'created', 'tb_courses', 23, '2018-05-19 02:04:42', '2018-05-19 02:04:42'),
(280, 8, 'created', 'tb_courses', 24, '2018-05-19 02:06:36', '2018-05-19 02:06:36'),
(281, 8, 'created', 'tb_courses', 24, '2018-05-19 02:06:36', '2018-05-19 02:06:36'),
(282, 4, 'updated', 'users', 4, '2018-05-23 23:48:11', '2018-05-23 23:48:11'),
(283, 1, 'updated', 'tb_categories_type', 3, '2018-05-25 00:50:25', '2018-05-25 00:50:25'),
(284, 1, 'updated', 'tb_categories_type', 2, '2018-05-25 00:50:29', '2018-05-25 00:50:29'),
(285, 1, 'updated', 'tb_categories_type', 1, '2018-05-25 00:50:35', '2018-05-25 00:50:35'),
(286, 1, 'deleted', 'tb_categories_type', 4, '2018-05-25 00:50:40', '2018-05-25 00:50:40'),
(287, 1, 'deleted', 'tb_categories_type', 5, '2018-05-25 00:50:42', '2018-05-25 00:50:42'),
(288, 1, 'deleted', 'tb_categories_type', 6, '2018-05-25 00:50:44', '2018-05-25 00:50:44'),
(289, 4, 'updated', 'users', 4, '2018-05-25 01:48:13', '2018-05-25 01:48:13'),
(290, 1, 'created', 'tb_courses', 25, '2018-05-25 07:16:48', '2018-05-25 07:16:48'),
(291, 1, 'created', 'tb_courses', 25, '2018-05-25 07:16:49', '2018-05-25 07:16:49'),
(292, 4, 'updated', 'users', 4, '2018-05-28 04:54:51', '2018-05-28 04:54:51'),
(293, 1, 'updated', 'users', 1, '2018-05-29 04:53:15', '2018-05-29 04:53:15'),
(294, 8, 'updated', 'users', 8, '2018-05-29 05:26:27', '2018-05-29 05:26:27'),
(295, 1, 'updated', 'users', 1, '2018-06-05 05:02:22', '2018-06-05 05:02:22'),
(296, 8, 'updated', 'users', 8, '2018-06-05 23:36:28', '2018-06-05 23:36:28'),
(297, 8, 'updated', 'users', 8, '2018-06-07 05:04:14', '2018-06-07 05:04:14'),
(298, 8, 'updated', 'users', 8, '2018-06-11 04:36:17', '2018-06-11 04:36:17'),
(299, 8, 'updated', 'users', 8, '2018-06-12 05:42:22', '2018-06-12 05:42:22'),
(300, 9, 'updated', 'users', 9, '2018-06-29 00:53:52', '2018-06-29 00:53:52'),
(301, 10, 'updated', 'users', 10, '2018-06-29 00:55:08', '2018-06-29 00:55:08'),
(302, 11, 'updated', 'users', 11, '2018-06-29 01:08:22', '2018-06-29 01:08:22'),
(303, 1, 'updated', 'users', 1, '2018-06-29 01:12:24', '2018-06-29 01:12:24'),
(304, 8, 'updated', 'users', 8, '2018-07-04 04:46:51', '2018-07-04 04:46:51'),
(305, 1, 'updated', 'users', 1, '2018-07-04 06:29:04', '2018-07-04 06:29:04'),
(306, 1, 'updated', 'users', 1, '2018-07-04 23:38:35', '2018-07-04 23:38:35'),
(307, 8, 'updated', 'users', 8, '2018-07-05 00:05:21', '2018-07-05 00:05:21'),
(308, 8, 'updated', 'users', 8, '2018-07-05 00:11:02', '2018-07-05 00:11:02'),
(309, 12, 'updated', 'users', 12, '2018-07-05 05:46:08', '2018-07-05 05:46:08'),
(310, 12, 'updated', 'users', 12, '2018-07-05 05:46:41', '2018-07-05 05:46:41'),
(311, 12, 'updated', 'users', 12, '2018-07-05 05:47:28', '2018-07-05 05:47:28'),
(312, 12, 'updated', 'users', 12, '2018-07-05 05:51:15', '2018-07-05 05:51:15'),
(313, 12, 'updated', 'users', 12, '2018-07-05 06:31:37', '2018-07-05 06:31:37'),
(314, 12, 'updated', 'users', 12, '2018-07-05 06:31:53', '2018-07-05 06:31:53'),
(315, 12, 'updated', 'users', 12, '2018-07-05 06:47:48', '2018-07-05 06:47:48'),
(316, 12, 'updated', 'users', 12, '2018-07-05 06:48:17', '2018-07-05 06:48:17'),
(317, 12, 'updated', 'users', 12, '2018-07-05 06:48:18', '2018-07-05 06:48:18'),
(318, 12, 'updated', 'users', 12, '2018-07-05 06:48:30', '2018-07-05 06:48:30'),
(319, 12, 'updated', 'users', 12, '2018-07-05 09:13:08', '2018-07-05 09:13:08'),
(320, 12, 'updated', 'users', 12, '2018-07-06 02:29:32', '2018-07-06 02:29:32'),
(321, 12, 'updated', 'users', 12, '2018-07-06 02:32:54', '2018-07-06 02:32:54'),
(322, 12, 'updated', 'users', 12, '2018-07-06 02:34:09', '2018-07-06 02:34:09'),
(323, 12, 'updated', 'users', 12, '2018-07-06 02:37:22', '2018-07-06 02:37:22'),
(324, 12, 'updated', 'users', 12, '2018-07-06 02:44:30', '2018-07-06 02:44:30'),
(325, 12, 'updated', 'users', 12, '2018-07-06 02:44:30', '2018-07-06 02:44:30'),
(326, 12, 'updated', 'users', 12, '2018-07-06 02:44:36', '2018-07-06 02:44:36'),
(327, 12, 'updated', 'users', 12, '2018-07-06 04:26:03', '2018-07-06 04:26:03'),
(328, 12, 'updated', 'users', 12, '2018-07-06 05:04:32', '2018-07-06 05:04:32'),
(329, 8, 'updated', 'users', 8, '2018-07-06 05:04:37', '2018-07-06 05:04:37'),
(330, 8, 'updated', 'users', 8, '2018-07-06 05:28:12', '2018-07-06 05:28:12'),
(331, 1, 'updated', 'users', 1, '2018-07-09 02:29:37', '2018-07-09 02:29:37'),
(332, 1, 'updated', 'users', 1, '2018-07-09 03:30:18', '2018-07-09 03:30:18'),
(333, 12, 'updated', 'users', 12, '2018-07-12 01:49:36', '2018-07-12 01:49:36'),
(334, 8, 'updated', 'users', 8, '2018-07-12 01:49:55', '2018-07-12 01:49:55'),
(335, 8, 'updated', 'users', 8, '2018-07-12 23:56:44', '2018-07-12 23:56:44'),
(336, 1, 'updated', 'users', 1, '2018-07-14 00:35:20', '2018-07-14 00:35:20'),
(337, 12, 'updated', 'users', 12, '2018-07-14 03:30:39', '2018-07-14 03:30:39'),
(338, 8, 'updated', 'users', 8, '2018-07-14 04:16:30', '2018-07-14 04:16:30'),
(339, 8, 'updated', 'users', 8, '2018-07-14 04:23:45', '2018-07-14 04:23:45'),
(340, 8, 'updated', 'users', 8, '2018-07-14 04:24:27', '2018-07-14 04:24:27'),
(341, 12, 'updated', 'users', 12, '2018-07-14 04:25:49', '2018-07-14 04:25:49'),
(342, 12, 'updated', 'users', 12, '2018-07-14 04:41:37', '2018-07-14 04:41:37'),
(343, 8, 'updated', 'users', 8, '2018-07-14 04:43:46', '2018-07-14 04:43:46'),
(344, 12, 'updated', 'users', 12, '2018-07-14 05:04:20', '2018-07-14 05:04:20'),
(345, 12, 'updated', 'users', 12, '2018-07-14 05:04:41', '2018-07-14 05:04:41'),
(346, 12, 'updated', 'users', 12, '2018-07-14 05:05:26', '2018-07-14 05:05:26'),
(347, 8, 'updated', 'users', 8, '2018-07-14 05:06:55', '2018-07-14 05:06:55'),
(348, 8, 'updated', 'users', 8, '2018-07-14 05:07:40', '2018-07-14 05:07:40'),
(349, 12, 'updated', 'users', 12, '2018-07-16 00:44:12', '2018-07-16 00:44:12'),
(350, 8, 'updated', 'users', 8, '2018-07-16 01:05:51', '2018-07-16 01:05:51'),
(351, 12, 'updated', 'users', 12, '2018-07-17 23:33:30', '2018-07-17 23:33:30'),
(352, 8, 'updated', 'users', 8, '2018-07-17 23:36:24', '2018-07-17 23:36:24'),
(353, 1, 'updated', 'users', 1, '2018-07-17 23:49:44', '2018-07-17 23:49:44'),
(354, 12, 'updated', 'users', 12, '2018-07-18 01:59:54', '2018-07-18 01:59:54'),
(355, 12, 'updated', 'users', 12, '2018-07-19 00:59:46', '2018-07-19 00:59:46'),
(356, 1, 'created', 'users', 13, '2018-07-19 01:01:01', '2018-07-19 01:01:01'),
(357, 13, 'created', 'tb_courses', 26, '2018-07-19 01:02:34', '2018-07-19 01:02:34'),
(358, 13, 'created', 'tb_courses', 26, '2018-07-19 01:02:34', '2018-07-19 01:02:34'),
(359, 13, 'created', 'tb_courses', 27, '2018-07-19 01:26:07', '2018-07-19 01:26:07'),
(360, 13, 'created', 'tb_courses', 28, '2018-07-19 04:30:08', '2018-07-19 04:30:08'),
(361, 13, 'created', 'tb_courses', 29, '2018-07-19 04:31:50', '2018-07-19 04:31:50'),
(362, 13, 'created', 'tb_courses', 29, '2018-07-19 04:31:50', '2018-07-19 04:31:50'),
(363, 1, 'updated', 'users', 1, '2018-07-19 04:36:52', '2018-07-19 04:36:52'),
(364, 1, 'updated', 'users', 1, '2018-07-19 23:58:34', '2018-07-19 23:58:34');

-- --------------------------------------------------------

--
-- Table structure for table `users_old`
--

CREATE TABLE `users_old` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_old`
--

INSERT INTO `users_old` (`id`, `role_id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'niresh@sdreamtech.com', '$2y$10$UKdYpfXjle7FKttGuilzVOcgJHPU6LNk1W3Cbg6vukM.xDJI1VRPq', NULL, '2018-02-16 20:53:11', '2018-02-16 20:53:11'),
(2, 1, 'Admin1', 'admin@gmail.com', '$2y$10$56RinQxr7lqdSXDPwcrbzOHxbkuv/wKfUJzeLARi56ng9dVRJvEm.', 'DNTy8JkjNXIQeGQFzteNHdejA0EQhjJrGM5fU2IhW4zvn1LSNjeg7XgDnfr7', '2018-02-26 00:01:18', '2018-02-26 00:01:18');

-- --------------------------------------------------------

--
-- Table structure for table `user_ip_detail`
--

CREATE TABLE `user_ip_detail` (
  `id` int(11) NOT NULL,
  `lon` text,
  `lat` text,
  `ip` text,
  `countryCode` text,
  `city` text,
  `country` text,
  `region` text,
  `regionName` text,
  `zip` text,
  `status` text,
  `timezone` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_ip_detail`
--

INSERT INTO `user_ip_detail` (`id`, `lon`, `lat`, `ip`, `countryCode`, `city`, `country`, `region`, `regionName`, `zip`, `status`, `timezone`) VALUES
(1, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(11, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(12, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(13, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(14, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(15, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(16, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(17, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(18, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(19, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(20, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(21, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(22, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(23, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(24, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(25, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(26, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(27, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(28, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(29, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(30, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(31, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(32, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(33, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(34, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(35, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(36, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(37, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(38, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(39, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(40, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(41, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(42, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(43, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(44, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(45, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(46, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(47, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(48, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(49, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(50, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(51, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(52, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(53, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(54, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(55, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(56, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(57, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(58, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(59, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(60, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(61, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(62, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(63, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(64, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(65, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(66, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(67, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(68, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(69, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(70, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(71, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(72, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(73, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(74, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(75, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(76, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(77, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(78, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(79, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(80, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(81, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(82, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(83, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(84, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(85, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(86, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(87, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(88, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(89, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(90, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(91, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(92, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(93, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(94, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(95, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(96, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(97, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(98, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(99, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(100, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(101, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(102, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(103, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(104, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(105, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(106, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(107, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(108, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(109, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(110, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(111, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(112, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(113, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(114, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(115, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(116, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(117, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(118, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(119, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(120, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(121, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(122, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(123, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(124, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(125, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(126, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(127, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(128, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(129, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(130, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(131, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(132, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(133, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(134, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(135, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(136, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(137, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(138, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(139, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(140, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(141, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(142, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(143, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(144, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(145, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(146, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(147, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(148, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(149, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(150, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(151, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(152, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(153, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(154, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(155, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(156, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(157, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(158, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(159, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(160, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(161, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(162, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(163, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(164, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(165, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(166, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(167, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(168, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(169, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(170, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(171, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(172, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(173, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(174, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(175, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(176, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(177, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(178, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(179, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(180, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(181, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(182, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(183, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(184, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(185, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(186, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(187, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(188, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(189, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(190, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(191, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(192, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(193, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(194, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(195, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(196, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(197, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(198, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(199, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(200, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(201, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(202, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(203, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(204, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(205, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(206, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(207, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(208, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(209, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(210, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(211, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(212, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(213, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(214, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(215, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(216, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(217, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(218, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(219, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(220, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(221, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(222, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(223, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(224, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(225, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(226, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(227, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(228, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(229, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(230, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(231, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(232, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(233, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(234, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(235, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(236, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(237, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(238, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(239, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(240, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(241, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(242, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(243, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(244, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(245, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(246, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(247, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(248, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(249, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(250, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(251, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(252, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(253, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(254, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(255, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(256, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(257, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(258, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(259, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(260, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(261, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(262, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(263, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(264, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(265, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(266, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(267, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(268, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(269, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(270, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(271, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(272, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(273, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(274, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(275, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(276, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(277, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(278, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(279, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(280, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(281, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(282, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(283, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(284, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(285, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(286, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(287, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(288, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(289, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(290, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(291, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(292, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(293, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(294, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(295, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(296, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(297, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(298, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(299, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(300, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(301, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(302, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(303, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(304, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(305, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(306, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(307, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(308, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(309, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(310, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(311, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(312, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(313, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(314, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(315, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(316, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(317, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(318, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(319, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(320, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(321, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(322, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(323, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(324, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(325, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(326, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(327, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(328, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(329, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(330, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(331, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(332, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(333, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(334, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(335, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(336, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(337, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(338, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(339, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(340, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(341, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(342, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(343, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(344, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(345, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(346, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(347, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(348, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(349, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(350, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(351, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata');
INSERT INTO `user_ip_detail` (`id`, `lon`, `lat`, `ip`, `countryCode`, `city`, `country`, `region`, `regionName`, `zip`, `status`, `timezone`) VALUES
(352, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(353, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(354, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(355, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(356, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(357, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(358, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(359, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(360, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(361, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(362, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(363, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(364, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(365, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(366, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(367, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(368, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(369, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(370, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(371, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(372, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(373, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(374, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(375, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(376, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(377, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(378, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(379, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(380, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(381, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(382, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(383, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(384, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(385, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(386, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(387, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(388, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(389, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(390, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(391, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(392, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(393, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(394, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(395, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(396, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(397, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(398, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(399, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(400, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(401, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(402, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(403, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(404, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(405, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(406, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(407, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(408, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(409, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(410, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(411, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(412, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(413, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(414, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(415, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(416, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(417, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(418, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(419, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(420, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(421, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(422, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(423, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(424, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(425, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(426, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(427, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(428, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(429, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(430, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(431, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(432, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(433, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(434, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(435, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(436, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(437, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(438, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(439, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(440, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(441, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(442, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(443, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(444, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(445, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(446, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(447, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(448, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(449, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(450, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(451, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(452, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(453, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(454, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(455, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(456, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(457, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(458, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(459, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(460, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(461, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(462, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(463, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(464, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(465, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(466, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(467, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(468, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(469, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(470, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(471, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(472, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(473, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(474, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(475, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(476, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(477, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(478, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(479, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(480, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(481, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(482, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(483, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(484, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(485, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(486, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(487, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(488, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(489, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(490, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(491, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(492, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(493, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(494, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(495, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(496, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(497, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(498, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(499, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(500, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(501, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(502, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(503, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(504, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(505, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(506, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(507, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(508, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(509, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(510, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(511, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(512, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(513, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(514, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(515, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(516, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(517, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(518, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(519, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(520, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(521, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(522, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(523, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(524, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(525, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(526, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(527, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(528, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(529, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(530, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(531, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(532, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(533, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(534, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(535, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(536, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(537, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(538, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(539, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(540, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(541, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(542, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(543, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(544, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(545, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(546, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(547, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(548, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(549, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(550, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(551, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(552, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(553, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(554, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(555, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(556, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(557, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(558, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(559, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(560, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(561, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(562, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(563, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(564, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(565, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(566, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(567, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(568, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(569, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(570, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(571, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(572, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(573, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(574, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(575, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(576, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(577, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(578, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(579, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(580, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(581, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(582, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(583, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(584, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(585, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(586, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(587, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(588, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(589, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(590, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(591, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(592, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(593, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(594, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(595, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(596, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(597, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(598, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(599, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(600, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(601, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(602, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(603, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(604, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(605, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(606, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(607, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(608, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(609, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(610, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(611, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(612, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(613, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(614, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(615, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(616, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(617, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(618, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(619, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(620, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(621, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(622, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(623, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(624, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(625, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(626, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(627, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(628, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(629, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(630, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(631, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(632, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(633, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(634, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(635, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(636, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(637, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(638, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(639, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(640, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(641, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(642, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(643, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(644, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(645, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(646, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(647, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(648, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(649, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(650, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(651, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(652, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(653, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(654, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(655, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(656, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(657, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(658, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(659, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(660, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(661, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(662, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(663, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(664, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(665, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(666, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(667, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(668, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(669, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(670, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(671, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(672, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(673, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(674, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(675, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(676, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(677, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(678, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(679, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(680, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(681, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(682, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(683, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(684, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(685, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(686, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata');
INSERT INTO `user_ip_detail` (`id`, `lon`, `lat`, `ip`, `countryCode`, `city`, `country`, `region`, `regionName`, `zip`, `status`, `timezone`) VALUES
(687, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(688, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(689, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(690, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(691, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(692, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(693, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(694, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(695, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(696, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(697, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(698, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(699, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(700, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(701, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(702, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(703, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(704, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(705, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(706, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(707, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(708, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(709, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(710, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(711, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(712, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(713, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(714, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(715, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(716, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(717, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(718, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(719, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(720, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(721, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(722, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(723, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(724, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(725, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(726, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(727, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(728, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(729, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(730, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(731, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(732, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(733, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(734, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(735, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(736, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(737, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(738, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(739, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(740, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(741, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(742, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(743, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(744, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(745, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(746, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(747, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(748, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(749, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(750, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(751, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(752, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(753, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(754, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(755, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(756, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(757, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(758, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(759, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(760, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(761, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(762, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(763, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(764, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(765, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(766, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(767, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(768, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(769, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(770, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(771, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(772, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(773, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(774, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(775, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(776, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(777, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(778, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(779, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(780, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(781, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(782, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(783, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(784, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(785, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(786, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(787, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(788, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(789, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(790, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(791, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(792, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(793, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(794, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(795, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(796, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(797, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(798, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(799, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(800, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(801, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(802, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(803, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(804, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(805, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(806, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(807, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(808, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(809, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(810, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(811, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(812, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(813, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(814, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(815, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(816, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(817, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(818, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(819, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(820, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(821, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(822, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(823, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(824, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(825, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(826, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(827, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(828, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(829, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(830, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(831, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(832, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(833, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(834, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(835, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(836, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(837, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(838, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(839, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(840, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(841, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(842, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(843, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(844, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(845, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(846, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(847, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(848, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(849, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(850, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(851, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(852, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(853, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(854, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(855, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(856, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(857, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(858, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(859, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(860, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(861, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(862, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(863, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(864, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(865, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(866, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(867, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(868, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(869, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(870, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(871, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(872, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(873, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(874, '78.11669921875', '9.933300018310547', '103.113.189.113', 'IN', 'Madurai', 'India', 'TN', 'Tamil Nadu', '625001', 'success', 'Asia/Kolkata'),
(875, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(876, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(877, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(878, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(879, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(880, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(881, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(882, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(883, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(884, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(885, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(886, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(887, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(888, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(889, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(890, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(891, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(892, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata'),
(893, '76.95690155029297', '8.5068998336792', '171.61.233.249', 'IN', 'Thiruvananthapuram', 'India', 'KL', 'Kerala', '695003', 'success', 'Asia/Kolkata');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sdream_cities`
--
ALTER TABLE `sdream_cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sdream_countries`
--
ALTER TABLE `sdream_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sdream_states`
--
ALTER TABLE `sdream_states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accesscode`
--
ALTER TABLE `accesscode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accesskey`
--
ALTER TABLE `accesskey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `authorPlan`
--
ALTER TABLE `authorPlan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billingAddress`
--
ALTER TABLE `billingAddress`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eb_book`
--
ALTER TABLE `eb_book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eb_book_files`
--
ALTER TABLE `eb_book_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eb_book_purchase`
--
ALTER TABLE `eb_book_purchase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eb_book_wish`
--
ALTER TABLE `eb_book_wish`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eb_college_book`
--
ALTER TABLE `eb_college_book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eb_college_book_files`
--
ALTER TABLE `eb_college_book_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eb_college_book_purchase`
--
ALTER TABLE `eb_college_book_purchase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eb_degrees`
--
ALTER TABLE `eb_degrees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eb_semesters`
--
ALTER TABLE `eb_semesters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_role`
--
ALTER TABLE `menu_role`
  ADD UNIQUE KEY `menu_role_menu_id_role_id_unique` (`menu_id`,`role_id`),
  ADD KEY `menu_role_menu_id_index` (`menu_id`),
  ADD KEY `menu_role_role_id_index` (`role_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `popular_book`
--
ALTER TABLE `popular_book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_book_categorie`
--
ALTER TABLE `tb_book_categorie`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_book_language`
--
ALTER TABLE `tb_book_language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_book_sub_categorie`
--
ALTER TABLE `tb_book_sub_categorie`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_categories_type`
--
ALTER TABLE `tb_categories_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_courses`
--
ALTER TABLE `tb_courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_courses_new1`
--
ALTER TABLE `tb_courses_new1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_course_assign`
--
ALTER TABLE `tb_course_assign`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_course_reject`
--
ALTER TABLE `tb_course_reject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_languages`
--
ALTER TABLE `tb_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_languages_settings`
--
ALTER TABLE `tb_languages_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_languages_settings_new`
--
ALTER TABLE `tb_languages_settings_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_learner_projects`
--
ALTER TABLE `tb_learner_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_learner_test_store`
--
ALTER TABLE `tb_learner_test_store`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_lession`
--
ALTER TABLE `tb_lession`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_lession_answer`
--
ALTER TABLE `tb_lession_answer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_lession_answer_new`
--
ALTER TABLE `tb_lession_answer_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_lession_new`
--
ALTER TABLE `tb_lession_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_lession_question`
--
ALTER TABLE `tb_lession_question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_lession_question_new`
--
ALTER TABLE `tb_lession_question_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_notification`
--
ALTER TABLE `tb_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_professional`
--
ALTER TABLE `tb_professional`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_purchased_course`
--
ALTER TABLE `tb_purchased_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_support_system`
--
ALTER TABLE `tb_support_system`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_test_code_generated`
--
ALTER TABLE `tb_test_code_generated`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_test_type`
--
ALTER TABLE `tb_test_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_logs`
--
ALTER TABLE `users_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_old`
--
ALTER TABLE `users_old`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_ip_detail`
--
ALTER TABLE `user_ip_detail`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sdream_cities`
--
ALTER TABLE `sdream_cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sdream_countries`
--
ALTER TABLE `sdream_countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sdream_states`
--
ALTER TABLE `sdream_states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `accesscode`
--
ALTER TABLE `accesscode`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `accesskey`
--
ALTER TABLE `accesskey`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `authorPlan`
--
ALTER TABLE `authorPlan`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `billingAddress`
--
ALTER TABLE `billingAddress`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;

--
-- AUTO_INCREMENT for table `eb_book`
--
ALTER TABLE `eb_book`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `eb_book_files`
--
ALTER TABLE `eb_book_files`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `eb_book_purchase`
--
ALTER TABLE `eb_book_purchase`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `eb_book_wish`
--
ALTER TABLE `eb_book_wish`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `eb_college_book`
--
ALTER TABLE `eb_college_book`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `eb_college_book_files`
--
ALTER TABLE `eb_college_book_files`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `eb_college_book_purchase`
--
ALTER TABLE `eb_college_book_purchase`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `eb_degrees`
--
ALTER TABLE `eb_degrees`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `eb_semesters`
--
ALTER TABLE `eb_semesters`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `popular_book`
--
ALTER TABLE `popular_book`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_book_categorie`
--
ALTER TABLE `tb_book_categorie`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_book_language`
--
ALTER TABLE `tb_book_language`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_book_sub_categorie`
--
ALTER TABLE `tb_book_sub_categorie`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_categories_type`
--
ALTER TABLE `tb_categories_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_courses`
--
ALTER TABLE `tb_courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `tb_courses_new1`
--
ALTER TABLE `tb_courses_new1`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tb_course_assign`
--
ALTER TABLE `tb_course_assign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_course_reject`
--
ALTER TABLE `tb_course_reject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_languages`
--
ALTER TABLE `tb_languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_languages_settings`
--
ALTER TABLE `tb_languages_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `tb_languages_settings_new`
--
ALTER TABLE `tb_languages_settings_new`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tb_learner_projects`
--
ALTER TABLE `tb_learner_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_learner_test_store`
--
ALTER TABLE `tb_learner_test_store`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tb_lession`
--
ALTER TABLE `tb_lession`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tb_lession_answer`
--
ALTER TABLE `tb_lession_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tb_lession_answer_new`
--
ALTER TABLE `tb_lession_answer_new`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tb_lession_new`
--
ALTER TABLE `tb_lession_new`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_lession_question`
--
ALTER TABLE `tb_lession_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tb_lession_question_new`
--
ALTER TABLE `tb_lession_question_new`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tb_notification`
--
ALTER TABLE `tb_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `tb_professional`
--
ALTER TABLE `tb_professional`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_purchased_course`
--
ALTER TABLE `tb_purchased_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_support_system`
--
ALTER TABLE `tb_support_system`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tb_test_code_generated`
--
ALTER TABLE `tb_test_code_generated`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_test_type`
--
ALTER TABLE `tb_test_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users_logs`
--
ALTER TABLE `users_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=365;

--
-- AUTO_INCREMENT for table `users_old`
--
ALTER TABLE `users_old`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_ip_detail`
--
ALTER TABLE `user_ip_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=894;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `menu_role`
--
ALTER TABLE `menu_role`
  ADD CONSTRAINT `menu_role_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `menu_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
