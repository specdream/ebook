<script type="text/javascript">
    var remFile = $('#totalFile').val();
    var fileType = '<?php if(count($bookFile) > 0) echo $bookFile[0]->type ?>';

    if(remFile >= 1){
        $('.source_section').hide();
        $('.image-file-select').show();
        $('.up_btn').show();
        $('.canc_div').hide();
    }else{
        $('.source_section').show();
        $('.image-file-select').show();
        $('.inputDnD').hide();
        $('.up_btn').show();
        $('.canc_div').show();
    }
    function getext(id) {
      $('#fileType').val(id);
      if(id == '1'){
        $('#inputFile').attr('accept','image/*');
        $('.up_btn').html('Add Image');
    }else if(id == '2'){
        $('#inputFile').attr('accept','video/*');
        $('.up_btn').html('Add Video');
    }else if(id == '3'){
        $('#inputFile').attr('accept','application/pdf');
        $('.up_btn').html('Add PDF');
    }else if(id == '4'){
        $('#inputFile').attr('accept','application/vnd.ms-powerpoint');
        $('.up_btn').html('Add PowerPoint');
    }
    fileType = id;
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function readUrl(input) {

  if (input.files && input.files[0]) {
    let reader = new FileReader();
    reader.onload = (e) => {
      let imgData = e.target.result;
      let imgName = input.files[0].name;
      input.setAttribute("data-title", imgName);
      console.log(e.target.result);
  }
  reader.readAsDataURL(input.files[0]);
}

}
function deletefile(id,type){
    var r = confirm("Are you sure want to delete");
    if (r == false) {
        return false;
    }
    remFile = $('#totalFile').val();
        //alert(remFile);
        //return false;

        var book_id = $('#book_id').val();
        $.ajax({
            type: "GET",
            url: '<?php echo url('collegeBooks/deleteFile').'/' ?>'+id+'/'+book_id+'/'+type,
            data: '',
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $('.book_edit_image .row').html(data);
                //alert(remFile)
                if(remFile == '1'){
                    $('.source_section').show();
                    $('.source_section .btn-default').show();
                    $('.image-file-select').show();
                    $('.inputDnD').hide();
                    $('.up_btn').show();
                    $('.canc_div').show();
                }
            }
        });
    }

    $(document).on('change','#price_type',function(){
      if($(this).val()=='Paid'){
        $(".price_hide").show();
    }else{
        $(".price_hide").hide();
        $("#price").val(0);
    }
})
    
   
    $(document).ready(function(){
        $(".btn-auth-img").click(function(){
            $(".btn-auth-img").hide();
        });
        $(".btn-auth-img").click(function(){
            $(".inputDnD").show();
        });
        $(".btn-drop-close").click(function(){
            $(".btn-auth-img").show();
        });
        $(".btn-drop-close").click(function(){
            $('#inputFile').val('');
            $('#inputFile').attr('data-title', 'Drag and drop a file')
            $(".inputDnD").hide();
        });

    });




    $('#inputFile').change(function (e) {
        var filename = $(this).val();
        var book_id = $('#book_id').val();
        var formData=new FormData();

        var names = [];
        var file_data = $('[name="files[]"]')[0].files; 
         // for multiple files
         for(var i = 0;i<file_data.length;i++){
            formData.append("files["+i+"]", file_data[i]);
        }
        formData.append('files[]',names);

        formData.append("book_id",book_id);
        formData.append("_token",'<?php echo csrf_token() ?>');
        formData.append("fileType",fileType);
        $('#progress-div').show();
        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = (evt.loaded / evt.total) * 100;
                        $("#progress-bar").css("width", percentComplete + '%');
                        $("#progress-bar").html('<div id="progress-status">' + percentComplete +' %</div>')
                    }
                }, false);
                return xhr;
            },
            type: "POST",
            url: '<?php echo url('collegeBooks/edit/file') ?>',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $('#progress-div').hide();
                $('.book_edit_image .row').html(data);
                //alert(remFile);
                if(typeof remFile === "undefined"){
                    $('.source_section').hide();
                    $('.book_edit_image').show();
                    $('.image-file-select').show();
                    $('.inputDnD').show();
                    $('.up_btn').show();
                    $('.canc_div').hide();

                }
                remFile = $('#totalFile').val();
            }
        });
    });

    $('.banner_img').change(function (e) {
        var filename = $(this).val();
        var book_id = $('#book_id').val();
        var formData=new FormData();

        var names = [];
        var file_data = $('[name="banner"]')[0].files; 
         // for multiple files
         for(var i = 0;i<file_data.length;i++){
            formData.append("banner", file_data[i]);
        }
        formData.append('banner',names);

        formData.append("book_id",book_id);
        formData.append("_token",'<?php echo csrf_token() ?>');
        formData.append("fileType",fileType);
        $('#progress-div').show();
        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = (evt.loaded / evt.total) * 100;
                        $("#progress-bar").css("width", percentComplete + '%');
                        $("#progress-bar").html('<div id="progress-status">' + percentComplete +' %</div>')
                    }
                }, false);
                return xhr;
            },
            type: "POST",
            url: '<?php echo url('collegeBooks/edit/banner') ?>',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $('#progress-div').hide();
                $('.banner_place').attr("src",data);
            }
        });
    });
    
</script>