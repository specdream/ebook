	<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<style type="text/css">
		.invoice-title h2, .invoice-title h3 {
	    display: inline-block;
	}

	.table > tbody > tr > .no-line {
	    border-top: none;
	}

	.table > thead > tr > .no-line {
	    border-bottom: none;
	}

	.table > tbody > tr > .thick-line {
	    border-top: 2px solid;
	}
	</style>
	<?php 
	//$purchase = \DB::table('eb_book_purchase')->where('uniq_id','5b3b1f5306aa1')->get();
	$purchase = \DB::table('eb_book_purchase')->where('uniq_id',\Session::get('purUniq'))->get();
	?>
	<div class="container">
	    <div class="row">
	        <div class="col-xs-12">
	    		<div class="invoice-title">
	    			<h2>Invoice</h2><h3 class="pull-right">Order # {{ \Session::get('purUniq') }}</h3>
	    		</div>
	    		<hr>
	    		<div class="row">
	    			<div class="col-xs-6">
	    				<address>
	    				<strong>Shipped To:</strong><br>
	    					{{ $address->address }}<br>
	    					{{ $address->city }}<br>
	    					{{ $address->state }}<br>
	    					{{ $country->name.','.$address->zip }}
	    				</address>
	    			</div>
	    			<div class="col-xs-6 text-right">
	    				<address>
	        			<strong>Order Date:</strong><br>
	    					{{ date('F j, Y',strtotime(\Session::get('purdate'))) }}<br><br>
	    				</address>
	    			</div>
	    		</div>
	    	</div>
	    </div>
	    
	    <div class="row">
	    	<div class="col-md-12">
	    		<div class="panel panel-default">
	    			<div class="panel-heading">
	    				<h3 class="panel-title"><strong>Order summary</strong></h3>
	    			</div>
	    			<div class="panel-body">
	    				<div class="table-responsive">
	    					<table class="table table-condensed">
	    						<thead>
	                                <tr>
	        							<td><strong>Item</strong></td>
	        							<td class="text-center"><strong>Price</strong></td>
	        							<td class="text-center"><strong>Quantity</strong></td>
	        							<td class="text-right"><strong>Totals</strong></td>
	                                </tr>
	    						</thead>
	    						<tbody>
	    							<!-- foreach ($order->lineItems as $line) or some such thing here -->
	    							<?php $totalPrice = '0'; ?>
	    							@if(count($purchase)>0)
	    							@foreach($purchase as $value)
	    							<?php 
	    							$totalPrice += $value->price;
	    							$book = \DB::table('eb_book')->where('id',$value->book_id)->first(); ?>
	    							<tr>
	    								<td>{{ $book->title }}</td>
	    								<td class="text-center">${{ $value->price }}</td>
	    								<td class="text-center">1</td>
	    								<td class="text-right">${{ $value->price }}</td>
	    							</tr>
	    							@endforeach
	    							@endif
	    							<tr>
	    								<td class="thick-line"></td>
	    								<td class="thick-line"></td>
	    								<td class="thick-line text-center"><strong>Subtotal</strong></td>
	    								<td class="thick-line text-right">${{ $totalPrice }}</td>
	    							</tr>
	    							<!-- <tr>
	    								<td class="no-line"></td>
	    								<td class="no-line"></td>
	    								<td class="no-line text-center"><strong>Shipping</strong></td>
	    								<td class="no-line text-right">$15</td>
	    							</tr> -->
	    							<tr>
	    								<td class="no-line"></td>
	    								<td class="no-line"></td>
	    								<td class="no-line text-center"><strong>Total</strong></td>
	    								<td class="no-line text-right">${{ $totalPrice }}</td>
	    							</tr>
	    						</tbody>
	    					</table>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    </div>
	</div>