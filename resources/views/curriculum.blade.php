@include('layout.corseheader')
<div class="col-xs-12 no_pad course_goal">
  <div class="container">
    <section class="border_box">
      <div class="col-md-9 col-sm-9 col-xs-12 no_pad">
        <div class="course_title">
          <h4>Curriculum</h4>
          <a class="btn btn-previw" href="">Preview</a>
          <input class="btn btn-save" type="button" value="Bulk Uploader">
        </div>
        <div class="course_option">
          <div class="course_ques col-xs-12 no_pad">
            <strong class="col-md-8 col-sm-12 col-xs-12 no_pad">Start putting together your course by creating sections, lectures and practice (quizzes and coding exercises) below.</strong>
            <div class="course_intro">
              <p>Section 1 : Introduction</p>
              <div class="lecture_block">
                Lecture 1:  Introduction
                <buttton>+  Add Content</buttton>
                <i class="fa fa-angle-down"></i>
              </div>
            </div>
          </div>
          <div class="add_curiculum">
            <div class="col-md-6 col-sm-6 col-xs-12 pad_5"><button><i class="fa fa-plus-square"></i> Add Lecture</button></div>
            <div class="col-md-6 col-sm-6 col-xs-12 pad_5"><button><i class="fa fa-plus-square"></i> Add Quiz</button></div>
            <div class="col-md-4 col-sm-4 col-xs-12 pad_5"><button><i class="fa fa-plus-square"></i> Add Coding Exercise</button></div>
            <div class="col-md-4 col-sm-4 col-xs-12 pad_5"><button><i class="fa fa-plus-square"></i> Add Practice Test</button></div>
            <div class="col-md-4 col-sm-4 col-xs-12 pad_5"><button><i class="fa fa-plus-square"></i> Add Assignment</button></div>
            <div class="col-md-12 col-sm-12 col-xs-12 pad_5"><button><i class="fa fa-plus-square"></i> Add Section</button></div>
          </div>
          <div class="col-xs-12 no_pad">
            <a class="btn btn-previw" href="">Preview</a>
            <input class="btn btn-save pull-right" type="button" value="Bulk Uploader">
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12 course_right">
        <b>Helpful Tips</b>
        <b>Course Goals & Target Students</b>
        <p>Course goals and target students appear on the course landing page and help potential students decide to purchase your course. Learn more.</p>
        <b>Course prerequisites</b>
        <p>Let your students know if they need to review any material or need any software before starting the course.</p>
        <b>Course Goals</b>
        <p>Course goals will give you a clear idea of what your students will learn and help you structure your course.</p>
        <b>Target Student</b>
        <p>Determining who your target student is will help you customize the course to fit your students' needs.</p>
      </div>
    </section>
    <center><input class="btn btn-review" type="submit" value="Submit for Review"></center>
  </div>
</div>
@include('layout.footer')