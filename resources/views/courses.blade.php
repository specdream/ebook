@include('layout.header')

<section class="color_header-course">
      <div class="container">
        <h4>Business</h4>

      </div>
    </section>

    <div class="col-xs-12 no-pad">
    <div class="course-header">
    <div class="container">
          <div class="lpr_course col-md-3 col-sm-4 col-xs-12">
            <div class="slider-item">
              <div class="img-responsive course-imgs" style="background-image: url('sdream/img/achieve_2.jpg'); "></div>
              <span class="owl-content">
              <div class="owl-text">
              Learn to Code by Making Games - Complete C# Unity Developer</div>
              <div class="owl-text2">
              By the readable content of page - Complete C# Unity Developer</div>  
              </span>
              <div class="hr-div"></div>
               <div class="ratting-reviews">
              <div class="empty_star_rate">
             <div class="star_rate" style="width:50%;"></div>
              </div>
             <span class="product-amount footer-content">
                <span class="discount-price">Discount:</span>
                <span data-purpose="card-list-price" class="original-list-price">
                <span class="sr-only">Original price:</span>₹12,800</span>
                <span data-purpose="card-discount-price" class="discount-list-price">
                <span class="sr-only">Current price:</span>₹770</span>
			</span>
			</div>
              <div class="course_full_det">
              	<div class="course_publish">
              		<span class="date">Published: 11/2015</span>
              		<h4 class="col-xs-12 nopadding">The Web Developer Bootcamp</h4>
              		<div class="course_seller">
              			<div class="course_popup_badge flleft">
              				<span>Best Seller</span>
              			</div>
              			<div class="flleft course_dept_section">
              				<span class="dept_bullet"></span>
              				<span class="dept">Web development</span>
              			</div>
              		</div>
<!--               		<div class="col-xs-12 nopadding lect_section">
              			<span>
              				<i class="fa fa-play-circle" aria-hidden="true"></i>
              				277 lectures
              			</span>
              			<span>
              				<i class="fa fa-sliders" aria-hidden="true"></i>
              				All levels
              			</span>
              		</div> -->
              		<div class="col-xs-12 nopadding course_popup_det">
              			<p>Learn to create Machine Learning Algorithms in Python and R from two Data Science experts. Code templates included.</p>
              			<ul class="col-xs-12 nomargin">
              				<li>Master Machine Learning on Python & R</li>
              				<li>Have a great intuition of many Machine Learning models</li>
              				<li>Make accurate predictions</li>
              			</ul>
              			<div class="col-xs-12 add_cart_section">
              				<button type="button" class="add_cart_popular btn">Add to Cart</button>
              				<span class="flright"><a class="" href="javascript:void(0)"><i class="fa fa-heart-o" aria-hidden="true"></i></a></span>
              			</div>
              		</div>
              	</div>
              </div>
            </div>
		</div>

		             <div class="lpr_course col-md-3 col-sm-4 col-xs-12">
            <div class="slider-item">
              <div class="img-responsive course-imgs" style="background-image: url('sdream/img/achieve_1.jpg'); "></div>
              <span class="owl-content">
              <div class="owl-text">
              Learn to Code by Making Games - Complete C# Unity Developer</div>
              <div class="owl-text2">
              By the readable content of page - Complete C# Unity Developer</div>  
              </span>
              <div class="hr-div"></div>
               <div class="ratting-reviews">
              <div class="empty_star_rate">
             <div class="star_rate" style="width:50%;"></div>
              </div>
             <span class="product-amount footer-content">
                <span class="discount-price">Discount:</span>
                <span data-purpose="card-list-price" class="original-list-price">
                <span class="sr-only">Original price:</span>₹12,800</span>
                <span data-purpose="card-discount-price" class="discount-list-price">
                <span class="sr-only">Current price:</span>₹770</span>
			</span>
			</div>
              <div class="course_full_det">
              	<div class="course_publish">
              		<span class="date">Published: 11/2015</span>
              		<h4 class="col-xs-12 nopadding">The Web Developer Bootcamp</h4>
              		<div class="course_seller">
              			<div class="course_popup_badge flleft">
              				<span>Best Seller</span>
              			</div>
              			<div class="flleft course_dept_section">
              				<span class="dept_bullet"></span>
              				<span class="dept">Web development</span>
              			</div>
              		</div>
<!--               		<div class="col-xs-12 nopadding lect_section">
              			<span>
              				<i class="fa fa-play-circle" aria-hidden="true"></i>
              				277 lectures
              			</span>
              			<span>
              				<i class="fa fa-sliders" aria-hidden="true"></i>
              				All levels
              			</span>
              		</div> -->
              		<div class="col-xs-12 nopadding course_popup_det">
              			<p>Learn to create Machine Learning Algorithms in Python and R from two Data Science experts. Code templates included.</p>
              			<ul class="col-xs-12 nomargin">
              				<li>Master Machine Learning on Python & R</li>
              				<li>Have a great intuition of many Machine Learning models</li>
              				<li>Make accurate predictions</li>
              			</ul>
              			<div class="col-xs-12 add_cart_section">
              				<button type="button" class="add_cart_popular btn">Add to Cart</button>
              				<span class="flright"><a class="" href="javascript:void(0)"><i class="fa fa-heart-o" aria-hidden="true"></i></a></span>
              			</div>
              		</div>
              	</div>
              </div>
            </div>
		</div>

		           <div class="lpr_course col-md-3 col-sm-4 col-xs-12">
            <div class="slider-item">
             <div class="img-responsive course-imgs" style="background-image: url('sdream/img/achieve_3.jpg'); "></div>
              <span class="owl-content">
              <div class="owl-text">
              Learn to Code by Making Games - Complete C# Unity Developer</div>
              <div class="owl-text2">
              By the readable content of page - Complete C# Unity Developer</div>  
              </span>
              <div class="hr-div"></div>
               <div class="ratting-reviews">
              <div class="empty_star_rate">
             <div class="star_rate" style="width:50%;"></div>
              </div>
             <span class="product-amount footer-content">
                <span class="discount-price">Discount:</span>
                <span data-purpose="card-list-price" class="original-list-price">
                <span class="sr-only">Original price:</span>₹12,800</span>
                <span data-purpose="card-discount-price" class="discount-list-price">
                <span class="sr-only">Current price:</span>₹770</span>
			</span>
			</div>
              <div class="course_full_det">
              	<div class="course_publish">
              		<span class="date">Published: 11/2015</span>
              		<h4 class="col-xs-12 nopadding">The Web Developer Bootcamp</h4>
              		<div class="course_seller">
              			<div class="course_popup_badge flleft">
              				<span>Best Seller</span>
              			</div>
              			<div class="flleft course_dept_section">
              				<span class="dept_bullet"></span>
              				<span class="dept">Web development</span>
              			</div>
              		</div>
<!--               		<div class="col-xs-12 nopadding lect_section">
              			<span>
              				<i class="fa fa-play-circle" aria-hidden="true"></i>
              				277 lectures
              			</span>
              			<span>
              				<i class="fa fa-sliders" aria-hidden="true"></i>
              				All levels
              			</span>
              		</div> -->
              		<div class="col-xs-12 nopadding course_popup_det">
              			<p>Learn to create Machine Learning Algorithms in Python and R from two Data Science experts. Code templates included.</p>
              			<ul class="col-xs-12 nomargin">
              				<li>Master Machine Learning on Python & R</li>
              				<li>Have a great intuition of many Machine Learning models</li>
              				<li>Make accurate predictions</li>
              			</ul>
              			<div class="col-xs-12 add_cart_section">
              				<button type="button" class="add_cart_popular btn">Add to Cart</button>
              				<span class="flright"><a class="" href="javascript:void(0)"><i class="fa fa-heart-o" aria-hidden="true"></i></a></span>
              			</div>
              		</div>
              	</div>
              </div>
            </div>
		</div>
		             <div class="lpr_course col-md-3 col-sm-4 col-xs-12">
            <div class="slider-item">
              <div class="img-responsive course-imgs" style="background-image: url('sdream/img/achieve_2.jpg'); "></div>
              <span class="owl-content">
              <div class="owl-text">
              Learn to Code by Making Games - Complete C# Unity Developer</div>
              <div class="owl-text2">
              By the readable content of page - Complete C# Unity Developer</div>  
              </span>
              <div class="hr-div"></div>
               <div class="ratting-reviews">
              <div class="empty_star_rate">
             <div class="star_rate" style="width:50%;"></div>
              </div>
             <span class="product-amount footer-content">
                <span class="discount-price">Discount:</span>
                <span data-purpose="card-list-price" class="original-list-price">
                <span class="sr-only">Original price:</span>₹12,800</span>
                <span data-purpose="card-discount-price" class="discount-list-price">
                <span class="sr-only">Current price:</span>₹770</span>
			</span>
			</div>
              <div class="course_full_det">
              	<div class="course_publish">
              		<span class="date">Published: 11/2015</span>
              		<h4 class="col-xs-12 nopadding">The Web Developer Bootcamp</h4>
              		<div class="course_seller">
              			<div class="course_popup_badge flleft">
              				<span>Best Seller</span>
              			</div>
              			<div class="flleft course_dept_section">
              				<span class="dept_bullet"></span>
              				<span class="dept">Web development</span>
              			</div>
              		</div>
<!--               		<div class="col-xs-12 nopadding lect_section">
              			<span>
              				<i class="fa fa-play-circle" aria-hidden="true"></i>
              				277 lectures
              			</span>
              			<span>
              				<i class="fa fa-sliders" aria-hidden="true"></i>
              				All levels
              			</span>
              		</div> -->
              		<div class="col-xs-12 nopadding course_popup_det">
              			<p>Learn to create Machine Learning Algorithms in Python and R from two Data Science experts. Code templates included.</p>
              			<ul class="col-xs-12 nomargin">
              				<li>Master Machine Learning on Python & R</li>
              				<li>Have a great intuition of many Machine Learning models</li>
              				<li>Make accurate predictions</li>
              			</ul>
              			<div class="col-xs-12 add_cart_section">
              				<button type="button" class="add_cart_popular btn">Add to Cart</button>
              				<span class="flright"><a class="" href="javascript:void(0)"><i class="fa fa-heart-o" aria-hidden="true"></i></a></span>
              			</div>
              		</div>
              	</div>
              </div>
            </div>
		</div>
    <div class="lpr_course col-md-3 col-sm-4 col-xs-12">
            <div class="slider-item">
             <div class="img-responsive course-imgs" style="background-image: url('sdream/img/achieve_1.jpg'); "></div>
              <span class="owl-content">
              <div class="owl-text">
              Learn to Code by Making Games - Complete C# Unity Developer</div>
              <div class="owl-text2">
              By the readable content of page - Complete C# Unity Developer</div>  
              </span>
              <div class="hr-div"></div>
               <div class="ratting-reviews">
              <div class="empty_star_rate">
             <div class="star_rate" style="width:50%;"></div>
              </div>
             <span class="product-amount footer-content">
                <span class="discount-price">Discount:</span>
                <span data-purpose="card-list-price" class="original-list-price">
                <span class="sr-only">Original price:</span>₹12,800</span>
                <span data-purpose="card-discount-price" class="discount-list-price">
                <span class="sr-only">Current price:</span>₹770</span>
			</span>
			</div>
              <div class="course_full_det">
              	<div class="course_publish">
              		<span class="date">Published: 11/2015</span>
              		<h4 class="col-xs-12 nopadding">The Web Developer Bootcamp</h4>
              		<div class="course_seller">
              			<div class="course_popup_badge flleft">
              				<span>Best Seller</span>
              			</div>
              			<div class="flleft course_dept_section">
              				<span class="dept_bullet"></span>
              				<span class="dept">Web development</span>
              			</div>
              		</div>
              <!-- 		<div class="col-xs-12 nopadding lect_section">
              			<span>
              				<i class="fa fa-play-circle" aria-hidden="true"></i>
              				277 lectures
              			</span>
              			<span>
              				<i class="fa fa-sliders" aria-hidden="true"></i>
              				All levels
              			</span>
              		</div> -->
              		<div class="col-xs-12 nopadding course_popup_det">
              			<p>Learn to create Machine Learning Algorithms in Python and R from two Data Science experts. Code templates included.</p>
              			<ul class="col-xs-12 nomargin">
              				<li>Master Machine Learning on Python & R</li>
              				<li>Have a great intuition of many Machine Learning models</li>
              				<li>Make accurate predictions</li>
              			</ul>
              			<div class="col-xs-12 add_cart_section">
              				<button type="button" class="add_cart_popular btn">Add to Cart</button>
              				<span class="flright"><a class="" href="javascript:void(0)"><i class="fa fa-heart-o" aria-hidden="true"></i></a></span>
              			</div>
              		</div>
              	</div>
              </div>
            </div>
		</div>
      </div>
   </div>
      </div>
@include('layout.footer')