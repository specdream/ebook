@include('layout.header')
<script type="text/javascript">
$(document).ready(function() {
var f = $(".detail_left").offset().top;
  $(window).scroll(function(){
    var scroll = $(window).scrollTop();
    var s = ($('header').outerHeight() + $('.detail_brief').outerHeight() + $(".detail_detail").height()) - $(".detail_left").height();
    var test = $(".detail_brief").height()- $(".detail_left").height();
    if(scroll > s) {
        $(".detail_left").removeClass("fixed_div_ex").addClass("relative_div").css({"top": test});
    } else {
        if (scroll > f) {
            $(".detail_left").addClass("fixed_div_ex").removeClass("relative_div").css({"top": "0px"});
        }
        else
        {$(".detail_left").removeClass("fixed_div_ex");}
    }
  });

  });

</script>
<style type="text/css">
/*  .owl-nav div:before {font-family: 'FontAwesome';font-size: 40px;color: #767676;line-height:16px;}
   .owl-nav div.owl-next:before {content: '\f105';}
  .owl-nav div.owl-prev:before {content: '\f104';}
   .owl-prev{position: absolute;left: -45px;top: 95px;}
  .owl-next{position: absolute;top: 30%;right: -45px;transform: translateY(-50%);-moz-transform: translateY(-50%);-webkit-transform: translateY(-50%);}

   .owl-nav > div.owl-prev, .home_page .owl-nav > div.owl-next {border-radius: 50%;box-shadow: 0 0 1px 1px rgba(20,23,28,.1), 0 3px 1px 0 rgba(20,23,28,.1);background: #fff !important;font-size: 0px;margin: 0px;line-height: normal;}
 .owl-nav > div.owl-prev:hover, .home_page .owl-nav > div.owl-next:hover{box-shadow: 0 2px 8px 2px rgba(20,23,28,.15);color: #00576b;color: #ab0c22;}
 .owl-nav > div.owl-next{padding: 13px 13px 9px 17px;}
 .owl-nav > div.owl-prev{padding: 13px 17px 9px 13px;}
  .owl-theme .owl-nav .disabled{display:none;}*/

  .glyphicon { margin-right:5px;}
.rating .glyphicon {font-size: 22px;}
.rating-num { font-size: 1.8rem;color: #0d0d0d;font-weight: 400;font-style: normal;margin: 0 0 10px;padding-top: 0; }
.progress { margin-bottom: 5px;}
.progress-bar { text-align: left; }
.rating-desc .col-md-3 {padding-right: 0px;}
.sr-only { margin-left: 5px;overflow: visible;clip: auto; }
/*.with-login-msg{margin-top: -15px;}*/
.star-book-detail{display:inline-block;}
</style>
<div class="col-xs-12 no_pad detail_page">
<div class="header-container">
<section class="detail-page">
  <div class="col-xs-12 col-md-3 col-sm-3">
  <div class="img-detail">
  <?php //print_r($book);exit; ?>
  <img src="{!! asset('public/uploads/college_books_banner') . '/'.  $book->id . '/'.  $book->banner !!}">
  </div>
  <div class="save-prive-act">
  <div class="preview-actions"><span class="fa fa-search"></span><a href="<?php echo url('/book-flip').'/'.$book->id ?>"> Preview now</a></div>
  <div class="save-actions"><span class="fa fa-arrow-down"></span>Save Preview</div>
  </div>
  <div class="ratings-summary">
    <div class="stars-read-only">
      <i class="fa fa-star color_star"></i>
      <i class="fa fa-star color_star"></i>
      <i class="fa fa-star color_star"></i>
      <i class="fa fa-star color_star"></i>
      <i class="fa fa-star"></i><span>(28)</span>
    </div>
  </div>
  <!-- <ul class="category-rankings dontprazremove" >
  <li><span class="rank" translate="no">#55</span>in<span>Romance,</span><span>Contemporary</span></li>
  <li><span class="rank" translate="no">#15</span>in<span>Romance,</span><span>Romance Suspense</span></li>
    
  </ul> -->
</div>
<div class="col-xs-12 col-md-6 col-sm-6">
<div class="name-auth">
<h3 class="detail-pg-title">{{ $book->title }}</h3>
<!-- <p class="detail-des">A Standalone Romance</p> -->
<p class="detail-auther-name">Teacher<span> {{ \Helper::getUser($book->user_id,'name') }}</span></p>
</div>
<div class="synopsis-section">
  <h4 class="synop-title">Synopsis</h4>
  <div class="synop-prag">{!! $book->summary !!}</div>
  <div class="abt-book dontprazremove" style="display: none">
    <h2>About this book</h2>
    <div class="col-xs-12 col-sm-4">
      <div class="stat-icon">
        <img src="../sdream/img/book.png">
      </div>
      <div class="stat-desc">
        <strong>303</strong>
        <span >Pages</span>
      </div>
    </div>
    <div class="col-xs-12 col-sm-4">
     <div class="stat-icon">
       <img src="../sdream/img/time.png">
     </div>
     <div class="stat-desc">
     <strong>6 - 7</strong>
      <span >Hours to read</span>
    </div>
  </div>
  <div class="col-xs-12 col-sm-4">
   <div class="stat-icon">
     <img src="../sdream/img/open-book.png">
   </div>
   <div class="stat-desc">
    <strong>83k</strong>
    <span >Total words</span>
  </div>
</div>
</div>
</div>
</div>
<div class="col-xs-12 col-md-3 col-sm-3">
  <div class="primary-right-container">
    <div class="pricing-details">
      <div class="box ">
        <h2 class="pricing-title">Buy the eBook</h2>
        <div class="pricing-figures">
          
          <div class="detail-active-price">Your price <span class="price">
          @if($book->price_type == 'Paid')
          $ {{ $book->price }}
          @else
          Free
          @endif
          </span>
          </div>
        </div>            
        <div class="action-container">

          <button class="btn btn-default signup-btnn" onclick="addToCartCollege(<?php echo $book->id ?>)" style="margin-left:0;">Add to cart</button>
          <button class="btn btn-default add-cart-btn" onclick="addToBuyCollege(<?php echo $book->id ?>)">Buy Now</button>
          <button class="btn btn-default add-cart-btn" onclick="addToWishCollege(<?php echo $book->id ?>)">Add to Wishlist</button>
          
        </div>
      </div>
    </div>
  </div>
</div>
</section>
</div>
</div>
<div class="col-xs-12 no_pad">
  <div class="carousal-container">
 
   <section class="latest_courses col-xs-12 col-md-12">
    <div class="first-carousal">
    <h2 class="detail-units-title pink_color">Same Semester</h2>
    <div class="owl-carousel owl-theme">
    @foreach($similarSem as $value)
      <div class="slider-item">
            <a href="{!!URL::To('/detail/')!!}/{{$value->id}}"><img src="{!! asset('public/uploads/books_banner') . '/'.  $value->id . '/'.  $value->banner !!}"></a>
            <span class="owl-detail-content">

              <div class="price_block">
                <div class="col-md-9 col-sm-9 col-xs-9 pad_rl_5">
                  <div class="owl-text">{!! \Helper::getLangValue($value->title)!!}</div>
                  <div class="owl-sub-text">{!! \Helper::getUser($value->user_id,'name') !!}</div>
                </div>
               <div class="col-xs-12 no_pad">
                <div class="star-rate">
                  <i class="fa fa-star color_star"></i>
                  <i class="fa fa-star color_star"></i>
                  <i class="fa fa-star color_star"></i>
                  <i class="fa fa-star color_star"></i>
                  <i class="fa fa-star color_star"></i>
                </div>
              </div>
                <div class="book-price list_price pad_rl_5">
                <!-- <span class="dontprazremove">  
                @if($value->price_type == 'Paid')
                {!! \Session::get('ipCurrencySymbol') !!} {{ \Helper::currencyPriceVal($value->price,$value->currency) }}
                @else
                Free
                @endif
                </span> -->
                <span>@if($value->price_type == 'Free') Free @else ${!!$value->price!!} @endif</span>
                </div>
              </div>
              <button class="btn btn-normal add-cart-btn" onclick="addToCart(<?php echo $value->id ?>)">Add to Cart </button>
            </span>
          </div>
      @endforeach
    </div>
    </div>
  </section>

<div class="overall_review">
        <div class="col-xs-12 col-md-12">
            <div class="well well-sm over-rating">
                <div class="row">
                    <div class="col-xs-12 col-sm-3  col-md-2 text-center first-sec overall-rat">
                    <div class="ratin-sec">
                    <h3 class="rating-num">Overall rating</h3>
                    <h3 class="rating-num">4.2 out of 5</h3>
                    <h3 class="rating-num">
                    <div class="col-xs-12 no_pad">
              <div class="star-rate star-book-detail">
                <i class="fa fa-star color_star"></i>
                <i class="fa fa-star color_star"></i>
                <i class="fa fa-star color_star"></i>
                <i class="fa fa-star color_star"></i>
                <i class="fa fa-star color_star"></i>
              </div><span>(34)</span>
            </div></h3>                      
                    </div>
                    </div>
                    <div class="col-xs-12 col-md-4 col-sm-5 second-sec">
                        <div class="row rating-desc">
                            <div class="col-xs-3 col-sm-3 col-md-3 text-right">
                                <span>5 STARS</span>
                            </div>
                            <div class="col-xs-8 col-md-9">
                                <div class="progress progress-striped">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20"
                                        aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                        <span class="sr-only">80%</span>
                                    </div>
                                </div>
                            </div>
                            <!-- end 5 -->
                            <div class="col-xs-3 col-sm-3 col-md-3 text-right">
                                <span>4 STARS</span>
                            </div>
                            <div class="col-xs-8 col-md-9">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20"
                                        aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                        <span class="sr-only">60%</span>
                                    </div>
                                </div>
                            </div>
                            <!-- end 4 -->
                            <div class="col-xs-3  col-sm-3 col-md-3 col-sm-3 text-right">
                                 <span>3 STARS</span>
                            </div>
                            <div class="col-xs-8 col-md-9">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20"
                                        aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40%</span>
                                    </div>
                                </div>
                            </div>
                            <!-- end 3 -->
                            <div class="col-xs-3 col-sm-3 col-md-3 text-right">
                                 <span>2 STARS</span>
                            </div>
                            <div class="col-xs-8 col-md-9">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20"
                                        aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                        <span class="sr-only">20%</span>
                                    </div>
                                </div>
                            </div>
                            <!-- end 2 -->
                            <div class="col-xs-3 col-sm-3  col-md-3 text-right">
                                 <span>1 STARS</span>
                            </div>
                            <div class="col-xs-8 col-md-9">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80"
                                        aria-valuemin="0" aria-valuemax="100" style="width: 15%">
                                        <span class="sr-only">15%</span>
                                    </div>
                                </div>
                            </div>
                            <!-- end 1 -->
                        </div>
                        <!-- end row -->
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 third-sec">
                    <div class="third-review-sec">
                    <h3 class="rating-num">Share your thoughts</h3>
                    <div class="btn btn-default review-btnn">Write your review</div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="col-xs-12 col-md-12 col-sm-12 all-review-sec">
<div class="col-sx-12 col-sm-4 padd-left">
<h3 class="review-listing-title">All Reviews</h3>
</div>
<div>
<ul class="drop-section">
<li>
<div class="dropdown" >
    <button class="btn btn-default dropdown-toggle btn-drop-filter" type="button" data-toggle="dropdown">Dropdown Example
    <span class="caret"></span></button>
    <ul class="dropdown-menu" style="width: 100%;">
      <li><a href="#">HTML</a></li>
      <li><a href="#">CSS</a></li>
      <li><a href="#">JavaScript</a></li>
    </ul>
  </div>
</li>
<li>
<div class="dropdown">
    <button class="btn btn-default dropdown-toggle btn-drop-filter" type="button" data-toggle="dropdown">Dropdown Example
    <span class="caret"></span></button>
    <ul class="dropdown-menu" style="width: 100%;">
      <li><a href="#">HTML</a></li>
      <li><a href="#">CSS</a></li>
      <li><a href="#">JavaScript</a></li>
    </ul>
  </div>
</li>
</ul>
</div>

</div>

<div class="col-xs-12 col-md-12 col-sm-12 all-review-help">
  <div class="col-sx-12 col-sm-9 padd-left">
    <h3 class="review-listing-title"><span>         
    <div class="col-xs-12 col-sm-2 no_pad">
      <div class="star-rate star-book-detail">
        <i class="fa fa-star color_star"></i>
        <i class="fa fa-star color_star"></i>
        <i class="fa fa-star color_star"></i>
        <i class="fa fa-star color_star"></i>
        <i class="fa fa-star color_star"></i>
      </div>
    </div></span>excellent</h3>
  </div>
  <div class="drop-section col-xs-12 col-sm-3">
<p>person found this review helpful</p>
    </div>
  <div class="col-xs-12 padd-left">
<div class="col-sx-12 col-sm-9 " style="padding-left:0;">
<p>UNA BUENA HISTORIA RELACIOANA EL AMOR QUE TIENES QUE TENER HACIA LAS PERSONAS QUE TE CUIDAN Y NO SER INGRATO YA QUE PAGARAS MAL TODO BUEN LIBRO RECOMIENDO /v</p>
<p>by Leonardo Q. on June 02, 2018</p>
</div>
<div class="col-sx-12 col-sm-3">
    <div class="col-sx-12 col-sm-12">
      <div class="dropdown" >
        <button class="btn btn-default dropdown-toggle btn-drop-help" type="button" data-toggle="dropdown">Was this helpful to you?
          <span class="caret"></span></button>
          <ul class="dropdown-menu drop-help-menu" style="width: 100%;">
            <li><a href="#">Yes</a></li>
            <li><a href="#">no</a></li>

          </ul>
        </div>
      </div>
</div>
  </div>
  </div>

  <div class="col-xs-12 col-md-12 col-sm-12 all-review-help">
  <div class="col-sx-12 col-sm-6 padd-left">
    <h3 class="review-listing-title">eBook Details</h3>
    <address>
  <ul class="ebook-address">
<li>Cedrona Enterprises, April 2017</li>
<li>ISBN: 9780998338750</li>
<li>Language: English</li>
<li>Download options: EPUB 2 (DRM-Free)</li>
</ul>
    </address>
  </div>

  <div class="col-xs-12 col-sm-6 padd-left">
<p>You can read this item using any of the following Kobo apps and devices:
</p>
<ul class="supported-devices">
  <li><span class="fa fa-desktop"></span><div class="icon-txt">DESKTOP</div></li>
  <li><span class="fa fa-tablet"></span><div class="icon-txt">TABLETS</div></li>
  <li><span class="fa fa-mobile"></span><div class="icon-txt">eREADERS</div></li>
  <li><span class="fa fa-apple"></span><div class="icon-txt">IOS</div></li>
  <li><span class="fa fa-android"></span><div class="icon-txt">ANDROID</div></li>
  <li><span class="fa fa-windows"></span><div class="icon-txt">WINDOWS</div></li>
</ul>

  </div>
  </div>
</div>
</div>



@include('layout.footer')