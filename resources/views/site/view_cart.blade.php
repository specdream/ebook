@include('layout.header')

<div class="col-xs-12 background">
	<div class="container">
		<div class="shop-cart"><p>Shoping Cart<span class="shop-slas">/</span><span>Checkout<span></p></div>
	</div>
</div>
<div class="col-xs-12 no-pad checkout-main-div">
	<div class="container">
		<div class="col-md-8 col-sm-8 col-xs-12">
			<div class="add-cart">
				<div class="cart-item-head">Your item <span> (<span id="course_count"></span>)</span></div>
				<div id="append_data">
				</div>
			</div>
		</div>
   <div class="col-md-4 col-sm-4 col-xs-12">
     <div class="cart-total">Total :</div>
     <div class="cart-total-price">$766</div>
     <button class="checkout_btn">Checkout</button></div> 
   </div>
 </div>


 <script type="text/javascript">


  $(document).ready(function(){
   if(!$("#my-cart-modal").length) {
    var products = ProductManager.getAllProducts();
    var count=products.length;
    $.each(products, function(){
      var total = this.quantity * this.price;
      $("#append_data").append('<div class="clearfix cart-content" id="remove_crt' + this.id + '" data-id="' + this.id + '"><div class="col-md-3 col-sm-3 col-xs-12"><div class="cart-img" style="background-image: url(' + this.image + ');"></div></div><div class="col-md-7 col-sm-7 col-xs-12"><b>' + this.name + '</b><p> '+ this.summary +'.</p></div><div class="current-amt col-md-2 col-sm-2 col-xs-12">$ ' + this.price + '<div  class="remove_product my-product-remove1" data-id="' + this.id + '"><a title="Remove This Item" class=" my-product-remove1 remove_ptd" style="cursor:pointer">Remove</a></div><br><div class="form-group"><select class="form-control" id="exampleFormControlSelect1"><option>0</option><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option></select></div></div></div>');
    });
    $("#gtot").text("$" + ProductManager.getTotalPrice().toFixed(2));
    $("#course_count").html(count);
    drawTable();
  }
})
  var $cartBadge = $(".my-cart-badge");
  $(document).on('click', ".my-product-remove1", function(){
    var $tr = $(this).closest("tr");
    var id = $tr.data("id");
    $tr.hide(500, function(){
      ProductManager.removeProduct(id);
      drawTable();
      $cartBadge.text(ProductManager.getTotalQuantity());
    });
  });
  var drawTable = function(){
    var $cartTable = $(".mini-products-list");
    $cartTable.empty();

    var products = ProductManager.getAllProducts();
    $.each(products, function(){
      var total = this.quantity * this.price;

      $(".mini-products-list").append(
        '<li class="item odd" title="' + this.summary + '" data-id="' + this.id + '" data-price="' + this.price + '"> <a href="shopping_cart.html" title="Product title here" class="product-image"><img src="' + this.image + '" alt="html Template" width="65"></a>'+
        '<div class="product-details"> <a href="#" title="Remove This Item" class="remove-cart my-product-remove"><i class="pe-7s-trash"></i></a>'+
        '<p class="product-name"><a href="shopping_cart.html">' + this.name + '</a> </p>'+
        '<strong>' + this.quantity + '</strong> x <span class="price">$' + this.price + '</span> </div>'+
        '</li>'
        );
    });
      // $("#"+idGrandTotal).html()
      

      
      $("#gtot").text("$" + ProductManager.getTotalPrice().toFixed(2));
      $("#my-cart-grand-total").text("$" + ProductManager.getTotalPrice().toFixed(2));
    }

    var ProductManager = (function(){
      var objToReturn = {};

    /*
    PRIVATE
    */
    localStorage.products = localStorage.products ? localStorage.products : "";
    var getIndexOfProduct = function(id){
      var productIndex = -1;
      var products = getAllProducts();
      $.each(products, function(index, value){
        if(value.id == id){
          productIndex = index;
          return;
        }
      });
      return productIndex;
    }
    var setAllProducts = function(products){
      localStorage.products = JSON.stringify(products);
    }
    var addProduct = function(id, name, summary, price, quantity, image) {
      var products = getAllProducts();
      products.push({
        id: id,
        name: name,
        summary: summary,
        price: price,
        quantity: quantity,
        image: image
      });
      setAllProducts(products);
    }

    /*
    PUBLIC
    */
    var getAllProducts = function(){
      try {
        var products = JSON.parse(localStorage.products);
        return products;
      } catch (e) {
        return [];
      }
    }
    var updatePoduct = function(id, quantity) {
      var productIndex = getIndexOfProduct(id);
      if(productIndex < 0){
        return false;
      }
      var products = getAllProducts();
      products[productIndex].quantity = typeof quantity === "undefined" ? products[productIndex].quantity * 1 + 1 : quantity;
      setAllProducts(products);
      return true;
    }
    var setProduct = function(id, name, summary, price, quantity, image) {
      if(typeof id === "undefined"){
        console.error("id required")
        return false;
      }
      if(typeof name === "undefined"){
        console.error("name required")
        return false;
      }
      if(typeof image === "undefined"){
        console.error("image required")
        return false;
      }
      if(!$.isNumeric(price)){
        console.error("price is not a number")
        return false;
      }
      if(!$.isNumeric(quantity)) {
        console.error("quantity is not a number");
        return false;
      }
      summary = typeof summary === "undefined" ? "" : summary;

      if(!updatePoduct(id)){
        addProduct(id, name, summary, price, quantity, image);
      }
    }
    var clearProduct = function(){
      setAllProducts([]);
    }
    var removeProduct = function(id){
      var products = getAllProducts();
      products = $.grep(products, function(value, index) {
        return value.id != id;
      });
      setAllProducts(products);
    }
    var getTotalQuantity = function(){
      var total = 0;
      var products = getAllProducts();
      $.each(products, function(index, value){
        total += value.quantity * 1;
      });
      return total;
    }
    var getTotalPrice = function(){
      var products = getAllProducts();
      var total = 0;
      $.each(products, function(index, value){
        total += value.quantity * value.price;
      });
      return total;
    }

    objToReturn.getAllProducts = getAllProducts;
    objToReturn.updatePoduct = updatePoduct;
    objToReturn.setProduct = setProduct;
    objToReturn.clearProduct = clearProduct;
    objToReturn.removeProduct = removeProduct;
    objToReturn.getTotalQuantity = getTotalQuantity;
    objToReturn.getTotalPrice = getTotalPrice;
    return objToReturn;
  }());


    /*
    EVENT
    */


  </script>
  @include('layout.footer')