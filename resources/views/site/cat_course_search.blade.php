@include('layout.header')

<section class="color_header-course">
	<div class="container">
		<h4>{!!$aCat_name->name!!}</h4>

	</div>
</section>

<div class="col-xs-12 no-pad">
	<div class="course-header">
		<div class="container">
		@foreach($aCat_courses as $value)
			<div class="lpr_course col-md-3 col-sm-4 col-xs-12">
				<div class="slider-item">
					<a href="{!!URL::To('/detail/')!!}/{!!$value->id!!}"><img src="{!! asset('public/uploads') . '/'.  $value->image !!}"></a>
					<span class="owl-content">
						<div class="owl-text">{!! \Helper::getLangValue($value->title)!!}</div>
							<div class="owl-text2">
								{!!$value->teacher_name['name']!!}</div>  
							</span>
							<div class="hr-div"></div>
							<div class="ratting-reviews">
								<div class="empty_star_rate">
									<div class="star_rate" style="width:50%;"></div>
								</div>
								<span class="product-amount footer-content">
                <span class="discount-price">@lang('core.Discount'):</span>
                
                <span data-purpose="card-discount-price" class="discount-list-price">
                <span class="sr-only">@lang('core.Current_price'):</span>₹{!!$value->price!!}</span>
			</span>
									
									</div>
									<div class="course_full_det">
										<div class="course_publish">
											<span class="date">@lang('core.Published'): {!!date('m/Y',strtotime($value->published_date))!!}</span>
											<h4 class="col-xs-12 nopadding">{!!$value->tb_languages_settings['value']!!}</h4>
											<div class="course_seller">
												<div class="course_popup_badge flleft">
													<span>@lang('core.Best_Seller')</span>
												</div>
												<div class="flleft course_dept_section">
													<span class="dept_bullet"></span>
													<span class="dept">{!!$aCat_name->name!!}</span>
												</div>
											</div>
											<div class="col-xs-12 nopadding course_popup_det">
												{!!substr(\Helper::getLangValue($value->summary),0,300)!!}
												@if(strlen(\Helper::getLangValue($value->summary))>300) ... @endif

												<div class="col-xs-12 add_cart_section">
													<!-- <button type="button" class="add_cart_popular btn">@lang('core.Add_to_Cart')</button> -->
													<button type="button" id="add_to_cart_btn_id_{!!$value->id!!}"  class="add-to-cart my-cart-btn add_cart_popular btn" data-id="{!!$value->id!!}" data-name="{!!\Helper::getLangValue($value->title)!!}" data-summary="Click to view course details" data-price="{!!number_format($value->price,2)!!}" data-quantity="1" data-image="{!! asset('public/uploads') . '/'.  $value->image !!}">@lang('core.Add_To_Cart')</button>

													<a href="{!!URL::To('/')!!}/viewCart" id="add_to_cart_btn_id_hide_{!!$value->id!!}"  style="display: none;"><button type="button" class="add_cart_popular btn">@lang('core.Go_To_Cart')</button></a>
													<span class="flright"><a class="" href="javascript:void(0)"><i class="fa fa-heart-o" aria-hidden="true"></i></a></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
				@include('layout.footer')