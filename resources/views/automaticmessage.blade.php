@include('layout.corseheader')
<div class="col-xs-12 no_pad course_goal">
  <div class="container">
    <section class="border_box">
      <div class="col-md-9 col-sm-9 col-xs-12 no_pad">
        <div class="course_title">
          <h4>Communications</h4>
          <input class="btn btn-save" type="submit" value="Save">
        </div>
        <div class="course_option">
          <div class="course_ques col-xs-12 no_pad">
            <b>Write messages to your students (optional) that will be sent automatically when they join or complete your course to encourage students to engage with course content. If you do not wish to send a welcome or congratulations message, leave the text box blank.</b>
            <b>Welcome Message</b>
            <textarea class="col-xs-12" rows="5"></textarea>
          </div>
          <div class="course_ques col-xs-12 no_pad">
            <b>Congratulations Message</b>
            <textarea class="col-xs-12" rows="5"></textarea>
          </div>
          <div class="col-xs-12 no_pad">
            <input class="btn btn-save pull-right" type="submit" value="Save">
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12 course_right">
        <b>Helpful Tips</b>
        <b>Course Goals & Target Students</b>
        <p>Course goals and target students appear on the course landing page and help potential students decide to purchase your course. Learn more.</p>
        <b>Course prerequisites</b>
        <p>Let your students know if they need to review any material or need any software before starting the course.</p>
        <b>Course Goals</b>
        <p>Course goals will give you a clear idea of what your students will learn and help you structure your course.</p>
        <b>Target Student</b>
        <p>Determining who your target student is will help you customize the course to fit your students' needs.</p>
      </div>
    </section>
    <center><input class="btn btn-review" type="submit" value="Submit for Review"></center>
  </div>
</div>
@include('layout.footer')