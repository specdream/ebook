@include('layout.header')
<div class="col-xs-12 no_pad margin_tb_30 profile_page">
  <div class="container">
    <div class="col-xs-12 profile_right">
      @include('layout.profilemenu')
      <center class="profile_title">
        <h2>Profile</h2>
        <div>Add information about yourself to share on your profile.</div>
      </center>
      <label class="col-md-6 col-sm-6 col-xs-12">
        Name
        <input type="text" name="name" class="form-control">
      </label>
       <label class="col-md-6 col-sm-6 col-xs-12">
        Profession
        <input type="text" name="profession" class="form-control">
      </label>
       <label class="col-md-6 col-sm-6 col-xs-12">
        Experience
        <input type="text" name="experience" class="form-control">
      </label>
       <label class="col-md-6 col-sm-6 col-xs-12">
        Educational
        <input type="text" name="educational" class="form-control">
      </label>
      <label class="col-md-6 col-sm-6 col-xs-12">
        Age
        <input type="text" name="age" class="form-control">
      </label>
      <label class="col-md-6 col-sm-6 col-xs-12">
        Email
        <input type="email" name="email" class="form-control">
      </label>
      <label class="col-md-6 col-sm-6 col-xs-12">
        Phone Number
        <input type="text" name="phnumber" class="form-control">
      </label>
      <label class="col-md-6 col-sm-6 col-xs-12">
        Address
        <input type="text" name="address" class="form-control">
      </label>
      <label class="col-md-6 col-sm-6 col-xs-12">
        Monthly Income
        <input type="text" name="monthincome" class="form-control">
      </label>
      <label class="col-md-6 col-sm-6 col-xs-12">
        No of Dependents
        <input type="email" name="nod" class="form-control">
      </label>
      <label class="col-md-6 col-sm-6 col-xs-12">
        Amibtion
        <input type="email" name="amibtion" class="form-control">
      </label>
      <label class="col-md-12 col-sm-12 col-xs-12 text-right">
        <input class="save_btn" type="submit" value="Save">
      </label>
    </div>
  </div>
</div>
@include('layout.footer')