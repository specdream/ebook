@extends('admin.layouts.master')

@section('content')

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">{{ trans('quickadmin::templates.templates-customView_index-list') }}</div>
        </div>
        <div class="portlet-body">
        <a href="{{ url('admin/bookCreate') }}">
        <button class="btn btn-default create-btn">Create</button>
        </a>
            <table class="table table-striped table-hover table-responsive datatable" id="datatable tbl-book-create">
             <thead>
            <tr>
            	<th>S.No</th>
            	<th>Title</th>
                <th>Categories</th>
                <th>Sub-Categories</th>
                <th>Price Type</th>
                <th>Price</th>
                <th>ISBN</th>
            	<th></th>
            </tr>
             </thead>
             
            <?php $si = '1'; foreach($book as $bookVal){ 
                if($bookVal->price_type != 'Paid'){
                    $priceType = 'Free';
                }else{
                    $priceType = 'Paid';
                }
            ?> 
            <tbody>
            <tr>
            	<td>{{ $si }}</td>
            	<td>{{ $bookVal->title }}</td>
            	<td>{{ \Helper::categoryName($bookVal->categorie) }}</td>
            	<td>{{ \Helper::subcategoryName($bookVal->sub_categorie) }}</td>
                <td>{{ $priceType }}</td>
                <td>{{ $bookVal->price }}</td>
                <td>{{ $bookVal->isbn }}</td>
                <td><a href="{{ url('/authorbooks/edit').'/'.$bookVal->id }}" class="btn btn-xs btn-info" title="Click to edit univerisities"><i class="fa fa-edit"></i></a>
                <a href="{{ url('/authorbooks/delete').'/'.$bookVal->id }}" class="btn btn-xs btn-danger" onclick="return confirm('Are you sure you want to delete this item?');" title="Click to edit univerisities"><i class="fa fa-trash"></i></a>    
                </td>
                </tr>
            <?php $si++; } ?>    
            </tbody>
            </table>
        </div>
	</div>

@endsection