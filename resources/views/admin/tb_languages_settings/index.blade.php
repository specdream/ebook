@extends('admin.layouts.master')

@section('content')
<style type="text/css">
    #datatable_info,#datatable_paginate,#datatable_length{display: none!important;}
</style>
<p>{!! link_to_route(config('quickadmin.route').'.tb_languages_settings.create', trans('quickadmin::templates.templates-view_index-add_new') , null, array('class' => 'btn btn-success')) !!}</p>

@if ($tb_languages_settings->count())
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">{{ trans('quickadmin::templates.templates-view_index-list') }}</div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-hover table-responsive datatable" id="datatable">
            <thead>
                <tr>
                    <th>
                        {!! Form::checkbox('delete_all',1,false,['class' => 'mass']) !!}
                    </th>
                    <th>Language Name</th>
                    <th>Key Name</th>
                    <th>Value</th>

                    <th>&nbsp;</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($tb_languages_settings as $row)
                <tr>
                    <td>
                        {!! Form::checkbox('del-'.$row->id,1,false,['class' => 'single','data-id'=> $row->id]) !!}
                    </td>
                    <td>{{ isset($row->tb_languages->name) ? $row->tb_languages->name : '' }}</td>
                    <td>{{ $row->key_name }}</td>
                    <td>{!! $row->value !!}</td>

                    <td>
                        <!-- {!! link_to_route(config('quickadmin.route').'.tb_languages_settings.edit', trans('quickadmin::templates.templates-view_index-edit'), array($row->id), array('class' => 'btn btn-xs btn-info')) !!} -->
                        <a href="{!!\URL::To('/'.config('quickadmin.route'))!!}/tb_languages_settings/{{ $row->id }}/edit" class="btn btn-xs btn-info" title="Click to edit language key"><i class="fa fa-edit"></i></a>
                        {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => "return confirm('".trans("quickadmin::templates.templates-view_index-are_you_sure")."');",  'route' => array(config('quickadmin.route').'.tb_languages_settings.destroy', $row->id))) !!}
                        <button type="submit" class="btn btn-xs btn-danger" title="Click to delete"><i class="fa fa-trash"></i> </button>
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
         <div class="row ">
            <div class="col-xs-12">
            <div class=" pull-right"><?php echo $tb_languages_settings->render(); ?></div>
            <div class=" pull-left">
                <button class="btn btn-danger" id="delete">
                    {{ trans('quickadmin::templates.templates-view_index-delete_checked') }}
                </button>
                </div>
            </div>
        </div>
        {!! Form::open(['route' => config('quickadmin.route').'.tb_languages_settings.massDelete', 'method' => 'post', 'id' => 'massDelete']) !!}
        <input type="hidden" id="send" name="toDelete">
        {!! Form::close() !!}
    </div>
</div>
@else
{{ trans('quickadmin::templates.templates-view_index-no_entries_found') }}
@endif

@endsection

@section('javascript')
<script>
    $(document).ready(function () {
        $('#delete').click(function () {
            if (window.confirm('{{ trans('quickadmin::templates.templates-view_index-are_you_sure') }}')) {
                var send = $('#send');
                var mass = $('.mass').is(":checked");
                if (mass == true) {
                    send.val('mass');
                } else {
                    var toDelete = [];
                    $('.single').each(function () {
                        if ($(this).is(":checked")) {
                            toDelete.push($(this).data('id'));
                        }
                    });
                    send.val(JSON.stringify(toDelete));
                }
                $('#massDelete').submit();
            }
        });
    });
</script>
@stop