@extends('admin.layouts.master')

@section('content')


<p> <a href="{!!\URL::To('/'.config('quickadmin.route'))!!}/question/create/{!!$course_id!!}/{!!$lession_id!!}" class="btn btn-success">Add new</a></p>

@if($questions->count() > 0)
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">{{ trans('quickadmin::admin.questions-index-lessions_list') }}</div>
    </div>
    <div class="portlet-body">       
    <div><h4><b>Course Name : </b>{!!$course_name!!} ; <b>Lession Name :</b> {!!$lession_name!!}; </h4></div><br>
        <table id="datatable" class="table table-striped table-hover table-responsive datatable">
            <thead>
                <tr>
                    <th>{{ trans('quickadmin::admin.questions-index-id') }}</th>
                    <th>{{ trans('quickadmin::admin.questions-index-question') }}</th>
                    <th>Answer</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($questions as $question)
                <tr>
                    <td>{!!$question->id!!}</td>
                    <td>{!!$question->question!!}</td>
                    <td><a class="label label-success" data-toggle="modal" data-target="#answerModal{{$question->id}}"><i class="fa fa-eye" aria-hidden="true"></i> View</a>

                   <!-- Modal -->
                   <div class="modal fade" id="answerModal{{$question->id}}" role="dialog">
                    <div class="modal-dialog">
                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title"><b>Question ( {{$question->id}} ) Answers</b></h4>
                        </div>
                        <div class="modal-body">

                         <table class="table" width="100%">
                           <thead>
                             <tr>
                               <th>Sno</th>
                               <th>Answer</th>
                               <th></th>
                             </tr>
                           </thead>
                           <tbody>
                             @if(count(getAnswer($question->id))>0)
                             @foreach(getAnswer($question->id) as $key2=>$value2)
                             <tr>
                               <td>{!! ($key2+1) !!}</td>
                               <td>
                                 @if($question->question_type=='2')
                <?php $tooltip='Click to view image'; ?>
                @elseif($question->question_type=='3')
                <?php $tooltip='Click to play audio'; ?>
                @elseif($question->question_type=='4')
                <?php $tooltip='Click to play video'; ?>
                @endif
                               <div class="pull-left">{!! $value2->answer !!}</div>
                                 <div class="pull-right">
                                  <a data-hide="answerModal{{$question->id}}" data-id="{{$key2+1}}" class="label label-warning hideans" title="{!!$tooltip!!}" data-html='true'><i class="fa fa-eye"></i></a>
                                  
                                      <!-- Modal content-->
                                      <div class="ans_data{{$key2+1}}" style="display: none">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title"><b> 
                                            @if($value2->answer_type=='2')
                                            View
                                            @elseif($value2->answer_type=='3' || $value2->answer_type=='4')
                                            Play
                                            @endif
                                            ( {{ $value2->answer }} ) @if($value2->answer_type=='2')
                                            Image
                                            @elseif($value2->answer_type=='3')
                                            Audio
                                            @elseif($value2->answer_type=='4')
                                            Video
                                            @endif</b></h4>
                                          </div>
                                          <div class="modal-body">
                                            <div class="margin-bottom-50">
                                              @if($value2->answer_type=='4')
                                              @if($value2->answer_subtype=='1')
                                              <div class="embed-responsive embed-responsive-16by9 thumbnail">
                                                <video style="width: 100%" controls>
                                                  <source src="{!!\URL::To('/public/uploads/question/'.$value2->file_url)!!}" type="video/mp4">
                                                    Your browser does not support the video element.
                                                  </video>
                                                </div>
                                                @else
                                                <?php
                                                $url = $value2->file_url;
                                                preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
                                                $id = $matches[1];
                                                ?>
                                                <object width="570" height="350" data="http://www.youtube.com/v/{!!$id!!}" type="application/x-shockwave-flash"><param name="src" value="{!!$value2->file_url!!}" /></object>                            
                                                @endif    
                                                @endif
                                                @if($value2->answer_type=='2')
                                                <div  style="width: 100%" >
                                                  <img src="{!!\URL::To('/public/uploads/question/'.$value2->file_url)!!}" style="width: 100%;height: 100%;">
                                                </div>
                                                @endif
                                              </div>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-default modal_close" data-show="answerModal{{$question->id}}" >Close</button>
                                            </div>
                                          </div>

                                        
                                    </div>
                                  </td>
                                  <td>
                                   @if($value2->correct_answer==1)
                                   <span class="label label-success "><i class="fa fa-check"></i></span>
                                   @else
                                   <span class="label label-danger "><i class="fa fa-times"></i></span>
                                   @endif
                                 </td>
                               </tr>
                               @endforeach
                               @else
                               <tr>
                                <th colspan="3" style="text-align: center;font-size: 18px;">No detail found</th> 
                              </tr>
                              @endif
                            </tbody>

                          </table>

                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>

                    </div>
                  </div>

                </td>
                    <td>
                        <a href="{!!\URL::To('/'.config('quickadmin.route'))!!}/question/{!!$question->id!!}/edit/{!!$course_id!!}/{!!$lession_id!!}" class="btn btn-xs btn-info"><i class="fa fa-edit" title="Click to edit question"></i></a>
                        <form method="POST" action="{!!\URL::To('/'.config('quickadmin.route'))!!}/question/destroy/{!!$question->id!!}/{!!$course_id!!}/{!!$lession_id!!}" accept-charset="UTF-8" style="display: inline-block;" onsubmit="return confirm('Are you sure?');">
                            {{ csrf_field() }}
                           <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
 <!-- Modal -->
      <div class="modal fade" id="answerModalin" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content append">
          </div>
        </div>
      </div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
      <script type="text/javascript">
        $(document).on('click','.hideans',function(){
          var hide=$(this).attr('data-hide');
          var id=$(this).attr('data-id');
          var html=$(".ans_data"+id).html();
          $(".append").empty().html(html);
          $("#"+hide).modal('hide');
          $("#answerModalin").modal('show');
        })
        $(document).on('click','.modal_close',function(){
          var show=$(this).attr('data-show');
          $("#"+show).modal('show');
          $("#answerModalin").modal('hide');
        })
      </script>
@else
{{ trans('quickadmin::admin.questions-index-no_entries_found') }}
@endif

@endsection