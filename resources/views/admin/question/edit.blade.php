@extends('admin.layouts.master')

@section('content')

    <div class="row">
        <div class="col-sm-10 col-sm-offset-2">
            <h1>{{ trans('quickadmin::admin.lessions-edit-create_user') }}</h1>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        {!! implode('', $errors->all('
                        <li class="error">:message</li>
                        ')) !!}
                    </ul>
                </div>
            @endif
        </div>
    </div>
<form method="POST" action="{!!URL::To('/')!!}/admin/question/update/{{$question->id}}/{{$lession_id}}/{!!$course_id!!}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
    {{ csrf_field() }}
   <input type="hidden" name="qid" value="{!!$question->id!!}">
    <input type="hidden" name="course_id" value="{!!$course_id!!}">
    <input type="hidden" name="lession_id" value="{{ $lession_id }}">
    <input type="hidden" name="test_type" value="{{ $test_type_id }}">

    <div class="form-group" >
        {!! Form::label('question_type', 'Question Type *', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10" >
            <select name="question_type" id='type_id' class="form-control" required="">
                <option value=""> - select - </option>
                <option @if($question->question_type=='1') selected @endif value="1"  > Text </option>
                <option @if($question->question_type=='2') selected @endif value="2" > Image </option>
                <option @if($question->question_type=='3') selected @endif value="3" > Audio </option>
                <option @if($question->question_type=='4') selected @endif value="4" > Video </option>
            </select>
        </div> 
    </div>
    <div class="form-group " id="hiddenDiv" @if($question->question_type=='1') style="display:none" @endif>
        {!! Form::label('question_subtype', 'Question Subtype*', ['class'=>'col-sm-2 control-label']) !!}        
        <div class="col-md-10">
            <select name="question_subtype" id="subtype_id" class="form-control">
                <option value="" > - Select - </option>
                <option @if($question->question_subtype=='1') selected @endif value="1" > Upload </option>
                <option @if($question->question_subtype=='2') selected @endif value="2" > Url </option>
            </select>
        </div> 
    </div>
    <div class="form-group  " id="hiddenDiv1" @if($question->question_subtype!='1') style="display:none" @endif>
       {!! Form::label('uploads_url', 'Upload*', ['class'=>'col-sm-2 control-label']) !!}
       <div class="col-md-10">
        <input type="file" name="uploads_urlq" id='uploads_url' class='form-control uploads_urla'> 
    </div> 
</div>
<div class="form-group" id="hiddenDiv2" @if($question->question_subtype!='2') style="display:none" @endif>
    {!! Form::label('file_url', 'Url*', ['class'=>'col-sm-2 control-label']) !!}
    <div class="col-md-10">
        <input type="text" name='file_urlq' id='file_url' class='form-control ' >                                   
    </div>
</div>
    <div class="form-group">
        {!! Form::label('question', trans('quickadmin::admin.questions-create-question').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('question', old('question',$question->question), ['class'=>'form-control keyvalid','required'=>'', 'placeholder'=> trans('quickadmin::admin.questions-create-question_placeholder')]) !!}
            <span id="question_msg" style="color: #F00;"></span>
        </div>
    </div>
     <div class="form-group">
        {!! Form::label('mark', trans('quickadmin::admin.questions-create-mark').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('mark', old('mark',$question->mark), ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.questions-create-mark_placeholder')]) !!}
        </div>
    </div>
    <div class="form-group">
        <h2 class="col-sm-2 control-label">{{ trans('quickadmin::admin.questions-create-answer') }}</h2>
        <div class="col-sm-10"></div>
    </div>
    @foreach($answers as $key=>$value)
    <div @if($key!=0) id="apnd_id{{$key}}" @endif>
     @if($key==0)
     <div class="form-group" >
        {!! Form::label('ans_type', 'Choice Type *', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10" >
            <select name="ans_type" id='ans_type' class="form-control">
                <option value=""> - select - </option>
                <option @if($value->answer_type=='1') selected @endif value="1"  > Text </option>
                <option @if($value->answer_type=='2') selected @endif value="2" > Image </option>
                <option @if($value->answer_type=='3') selected @endif value="3" > Audio </option>
                <option @if($value->answer_type=='4') selected @endif value="4" > Video </option>
            </select>
            </div> 
    </div>
    <div class="form-group " id="hiddenDiva" @if($value->answer_type=='1') style="display:none" @endif>
    {!! Form::label('ans_subtype', 'Question Subtype*', ['class'=>'col-sm-2 control-label']) !!}        
        <div class="col-md-10">
            <select name="ans_subtype" id="ans_subtype" class="form-control">
                <option value="" > - Select - </option>
                <option @if($value->answer_subtype=='1') selected @endif value="1" > Upload </option>
                <option @if($value->answer_subtype=='2') selected @endif value="2" > Url </option>
            </select>
        </div> 
    </div><br><br>
    @endif
    <div class="form-group">
    {!! Form::label('answer', 'Answer *', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-8">
            {!! Form::text('answer['.$key.']', old('answer['.$key.']',$value->answer), ['class'=>'form-control keyvalid', 'placeholder'=> trans('quickadmin::admin.questions-create-answer_placeholder')]) !!}
            <span id="answer{{$key}}_msg" style="color: #F00;"></span> 
        </div>
        <div class="col-sm-2">@if($key==0)<button class="btn-sm btn btn-success add_ans" type="button">Add</button>
        <input type="hidden" id="inc" name="inc" value="{{count($answers)}}">@else <button class="btn-sm btn btn-danger rmv_ans" data-id="{{$key}}" type="button">Remove</button> @endif</div>
    </div>
    <div class="form-group  hiddenDiv1a" id="" @if($value->answer_subtype!='1') style="display:none" @endif >
         {!! Form::label('uploads_url', 'Upload*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-md-8">
            <input type="file" name="uploads_url{{$key}}" class='form-control' accept="image/*" > 
            </div> 
    </div>
    <div class="form-group hiddenDiv2a" id="" @if($value->answer_subtype!='2') style="display:none" @endif >
        {!! Form::label('file_url', 'Url*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-md-8">
            <input type="text" name='file_url{{$key}}' class='form-control '  value="{{$value->file_url}}">                                   
            </div>
    </div>
    <input type="hidden" name='file_url_org_{{$key}}' class='form-control ' value="{{$value->file_url}}"> 
    <div class="form-group">
        {!! Form::label('correct_answer'.$key, 'Correct answer *', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
        {!! Form::hidden('correct_answer'.$key,$value->correct_answer) !!}
        @if($value->correct_answer==0)
        {!! Form::radio('correct_answer['.$key.']', 0, true) !!} No
        {!! Form::radio('correct_answer['.$key.']', 1, false) !!} Yes
        @else
        {!! Form::radio('correct_answer['.$key.']', 0, false) !!} No
        {!! Form::radio('correct_answer['.$key.']', 1, true) !!} Yes
        @endif
        
    </div></div>
    <hr>
    </div>
    @endforeach
    <div id="append_val">
    
    </div>
  
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit(trans('quickadmin::admin.companies-edit-btnedit'), ['class' => 'btn btn-primary']) !!}
        </div>
    </div>

  </form>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">    
$(document).on('click','.add_ans',function(){
    var inc=$("#inc").val();
        inc++;
        var ans_subtype=$('#ans_subtype').val();

     $("#append_val").append('<div id="apnd_id'+inc+'"><div class="form-group"><label for="answer'+inc+'" class="col-sm-2 control-label">Answer *</label><div class="col-sm-8"><input class="form-control keyvalid" placeholder="Answer" name="answer['+inc+']" type="text"><span id="answer'+inc+'_msg" style="color: #F00;"></span> </div><div class="col-sm-2"><button class="btn-sm btn btn-danger rmv_ans" data-id="'+inc+'" type="button">Remove</button></div></div><div class="form-group  hiddenDiv1a" id="" style="display:none"><label for="uploads_url" class="col-sm-2 control-label">Upload*</label><div class="col-md-8"><input name="uploads_url'+inc+'" class="form-control uploads_urla"  type="file"></div></div><div class="form-group hiddenDiv2a" id="" style="display:none"><label for="file_url" class="col-sm-2 control-label">Url*</label><div class="col-md-8"><input name="file_url'+inc+'" class="form-control " type="text"></div></div><div class="form-group"><label for="correct_answer'+inc+'" class="col-sm-2 control-label">Correct answer *</label><div class="col-sm-10"><input name="correct_answer1" value="" id="correct_answer1" type="hidden"><input name="correct_answer['+inc+']" value="0" checked type="radio"> No<input name="correct_answer['+inc+']" value="1" type="radio"> Yes</div></div><hr></div>');
     if(ans_subtype=='1'){
        $(".hiddenDiv1a").show();$(".hiddenDiv2a").hide();
     }else if(ans_subtype=='2'){
        $(".hiddenDiv1a").hide();$(".hiddenDiv2a").show();
     }else{
        $(".hiddenDiv1a").hide();$(".hiddenDiv2a").hide();
     }

     $("#inc").val(inc);
})
$(document).on('click','.rmv_ans',function(){
    var id=$(this).attr('data-id');
    $("#apnd_id"+id).remove();
})

$(document).on('change','#type_id',function(){
    if($(this).val()!='1'){
        $("#hiddenDiv").show();
    }else{
        $("#hiddenDiv").hide();
    }
    if($(this).val()=='2'){
        $('.uploads_urlq').prop('accept','image/*');
    }else if($(this).val()=='3'){
        $('.uploads_urlq').prop('accept','audio/*');
    }else if($(this).val()=='4'){
        $('.uploads_urlq').prop('accept','video/*');
    }else{

    }
})
$(document).on('change','#subtype_id',function(){
    if($(this).val()!=''){
        if($(this).val()=='1'){
            $("#hiddenDiv1").show();
            $("#hiddenDiv2").hide();
        }else{
            $("#hiddenDiv1").hide();
            $("#hiddenDiv2").show();
        }
    }else{
        $("#hiddenDiv1").hide();
        $("#hiddenDiv2").hide();
    }
})


$(document).on('change','#ans_type',function(){
    if($(this).val()!='1'){
        $("#hiddenDiva").show();
    }else{
        $("#hiddenDiva").hide();
    }
    if($(this).val()=='2'){
        $('.uploads_urla').prop('accept','image/*');
    }else if($(this).val()=='3'){
        $('.uploads_urla').prop('accept','audio/*');
    }else if($(this).val()=='4'){
        $('.uploads_urla').prop('accept','video/*');
    }else{

    }
})


$(document).on('change','#ans_subtype',function(){
    if($(this).val()!=''){
        if($(this).val()=='1'){
            $(".hiddenDiv1a").show();
            $(".hiddenDiv2a").hide();
        }else{
            $(".hiddenDiv1a").hide();
            $(".hiddenDiv2a").show();
        }
    }else{
        $(".hiddenDiv1a").hide();
        $(".hiddenDiv2a").hide();
    }
})
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var base_url=$("#base_url").val();
$(document).on('keyup','.keyvalid',function(){
    var keyval=$(this).val();
    var id=$(this).attr('id');
    var cid=$(this).attr('data-id');
    var type=$(this).attr('data-type');
        // alert(id);
        var token=$('meta[name="csrf-token"]').attr('content');
        if(keyval!=''){
            $.ajax({
                type:'POST',
                url:base_url+"/admin/tb_courses/acheckkey_edit",
                dataType: 'json',
                data:{keyval:keyval,_token:token,cid:cid,type:type},
                success:function(data){
              // alert(data);
              if(data>0){
                // alert('Key already exist..');
                $("#"+id+"_msg").html('Key already exist..');
                $("#"+id).css("border", "#F00 solid 1px"); 
                $(".btn-primary").prop('disabled',true);
            }else{
                $("#"+id).css("border", "#ccc solid 1px");
                $("#"+id+"_msg").html('');
                $(".btn-primary").prop('disabled',false);
            }
        }
    });
        }else{
            $("#"+id).css("border", "#ccc solid 1px");
            $("#"+id+"_msg").html('');
            $(".btn-primary").prop('disabled',false);
        }
    })
</script>
@endsection


