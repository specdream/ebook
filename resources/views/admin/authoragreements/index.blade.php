@extends('admin.layouts.master')

@section('content')

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">{{ trans('quickadmin::templates.templates-customView_index-list') }}</div>
    </div>
    <div class="portlet-body">
     <div class="demo">
        <div class="container">
            <div class="row">
              <?php if(count($policyData) > 0){ ?>
              <div class="col-md-4 col-sm-6">
                <div class="pricingTable">
                    <div class="pricingTable-header">
                        <h3 class="title">{{ $policyData[0]->title }}</h3>
                        <span class="sub-title">{{ $policyData[0]->descp }}</span>
                        <!-- <span class="year">Pay only <br>$110/year</span> -->
                    </div>
                    <div class="price-value">
                        <div class="value">
                            <!-- <span class="currency">$</span> -->
                            <span class="amount">{{ $policyData[0]->price }}%<span></span></span>
                            <span class="month">/{!! trans('core.paid_ticket*Free') !!}</span>
                        </div>
                    </div>
                    <ul class="pricing-content">
                      <?php 
                      $listData = explode('||', $policyData[0]->etcOpt);
                      foreach ($listData as $listDataVal) { ?>


                      <li>{{ $listDataVal }}</li>
                      <!-- <li class="disable">20 Domains</li> -->
                      <?php }
                      ?>
                      <li class="disable">Testing</li>
                  </ul>
                  <div class="planDiv">
                   <input type="hidden" name="planId" value="1">
                   @if($activePlan->authorPlan == 1)
                   <a href="#" class="pricingTable-signup">{!! trans('core.curent-plan') !!}</a>
                   @else
                   <a href="#" class="pricingTable-signup changePlan">{!! trans('core.set-active') !!}</a>
                   @endif
               </div>
           </div>
       </div>
       <?php } 
       if(count($policyData) > 1){ 
        ?>
        <div class="col-md-4 col-sm-6">
            <div class="pricingTable purple">
                <div class="pricingTable-header">
                    <h3 class="title">{{ $policyData[1]->title }}</h3>
                    <span class="sub-title">{{ $policyData[1]->descp }}</span>
                    <!-- <span class="year">Pay only <br>$220/year</span> -->
                </div>
                <div class="price-value">
                    <div class="value">
                        <!-- <span class="currency">$</span> -->
                        <span class="amount">{{ $policyData[1]->price }}%<span></span></span>
                        <span class="month">/{!! trans('core.paid_ticket*Free') !!}</span>
                    </div>
                </div>
                <ul class="pricing-content">
                    <?php 
                    $listData = explode('||', $policyData[1]->etcOpt);
                    foreach ($listData as $listDataVal) { ?>

                    <li> {{ $listDataVal }}</li>

                    <?php }
                    ?>

                </ul>
                <div class="planDiv">
                    <input type="hidden" name="planId" value="2">
                    @if($activePlan->authorPlan == 2)
                    <a href="#" class="pricingTable-signup">{!! trans('core.curent-plan') !!}</a>
                    @else
                    <a href="#" class="pricingTable-signup changePlan">{!! trans('core.set-active') !!}</a>
                    @endif
                </div>
            </div>
        </div>
        <?php } 
        if(count($policyData) > 2){ 
            ?>
            <div class="col-md-4 col-sm-6">
                <div class="pricingTable blue">
                    <div class="pricingTable-header">
                        <h3 class="title">{{ $policyData[2]->title }}</h3>
                        <span class="sub-title">{{ $policyData[2]->descp }}</span>
                        <!-- <span class="year">Pay only <br>$220/year</span> -->
                    </div>
                    <div class="price-value">
                        <div class="value">
                            <!-- <span class="currency">$</span> -->
                            <span class="amount">{{ $policyData[2]->price }}%<span></span></span>
                            <span class="month">/{!! trans('core.paid_ticket*Free') !!}</span>
                        </div>
                    </div>
                    <ul class="pricing-content">
                       <?php 
                       $listData = explode('||', $policyData[2]->etcOpt);
                       foreach ($listData as $listDataVal) { ?>

                       <li> {{ $listDataVal }}</li>

                       <?php }
                       ?>
                       <li class="disable">Testing</li>
                   </ul>
                   <div class="planDiv">
                    <input type="hidden" name="planId" value="3">
                    @if($activePlan->authorPlan == 3)
                    <a href="#" class="pricingTable-signup">{!! trans('core.curent-plan') !!}</a>
                    @else
                    <a href="#" class="pricingTable-signup changePlan">{!! trans('core.set-active') !!}</a>
                    @endif
                </div>
            </div>
        </div>
        <?php } 
        ?>
    </div>
</div>
</div>
</div>
</div>


<script type="text/javascript">

  var base_url = '<?php echo url('') ?>';
    $('.changePlan').on('click',function(){
        var planId = $(this).closest('.planDiv').find('input[name="planId"]').val();
        $.ajax({
        type: 'POST',
        url : base_url+"/setPlan",
        data: {
            _token    : "{{ csrf_token() }}",
            plan_id  : planId,
        },
        success: function(res) {
            if(res == 'success'){
                location.reload(true);
            }
        }
    });
    });
</script>
@endsection