@extends('admin.layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
        <h1>{{ trans('quickadmin::templates.templates-view_create-add_new') }}</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

<!-- {!! Form::open(array('files' => true, 'route' => config('quickadmin.route').'.tb_courses.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!} -->
<form action="{{ url('admin/createBookLang') }}" id="form-with-validation" class="form-horizontal" method="post" enctype="multipart/form-data" >
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="form-group">
    <label class="col-sm-2 control-label">Language</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="language" value="" placeholder="English" required="">
        
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Key</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="key" value="" maxlength="2" placeholder="en" required="">
        
    </div>
</div>


<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
      {!! Form::submit( trans('quickadmin::templates.templates-view_create-create') , array('class' => 'btn btn-primary')) !!}
    </div>
</div>

{!! Form::close() !!}
@endsection