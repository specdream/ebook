@extends('admin.layouts.master')

@section('content')

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">{{ trans('quickadmin::templates.templates-customView_index-list') }}</div>
        </div>
        <div class="portlet-body">
    


<div id="content" class="bg-container" style="min-height: 425px;">
    <div class="outer">
        <div class="inner bg-container">
            <div class="col-xs-12">
            <div class="panel panel-default">
            <form action="{{ url('update/plan') }}" method="post">
            <input type="hidden" value="{{ $id }}" name="id">
            <input type="hidden" value="{{ csrf_token() }}" name="_token">
                    <div class="panel-heading admin-head">Organizer Pricing</div>
                    <div class="panel-body author-edit-plan">
                <div class="input_fields_container">
                <div class="col-md-6 col-sm-6 col-xs-12 margin-title">
                <div class="form-group">
                <label class="price-lable">Enter Title</label>
                <input class="input-btn-script col-md-8" type="text" value="{{ $policyData->title }}" style="margin-left:23px;" name="product_name">
                    </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                <label class="price-lable">Enter Description</label>    
                <input class="input-btn-script col-md-8" type="text" value="{{ $policyData->descp }}" name="product_descp">
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 amt-margin">
                <div class="form-group">
                <label class="price-lable">Enter Amount</label>
                <input class="input-btn-script col-md-2 col-sm-4 col-xs-4" type="text" value="{{ $policyData->price }}" name="product_amount">
            
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                    <?php
                    $listData = explode('||', $policyData->etcOpt);
                    $etCnt = '1';   
                    if(count($listData) > 0){
                    
                    foreach($listData as $etcOptVal){ 
                        if($etCnt == '1'){ ?>
                        <div class="form-group col-md-12">
                        <input class="input-btn-script col-md-4 col-sm-4 col-xs-12" type="text" name="product_etcOpt[]" value="{{ $etcOptVal }}">
                        <button class="btn btn-sm btn-normal add_more_button">Add More Fields</button>
                        </div>
                    <?php }else{ ?> 
                    <div class="form-group col-md-12">
                        <input class="input-btn-script col-md-4 col-sm-4 col-xs-12" type="text" name="product_etcOpt[]" value="{{ $etcOptVal }}">
                        <button class="btn  btn-normal add_more_button remove_field  style="margin-left:10px;">Remove</button>
                        </div>
                    <?php } $etCnt++; }
                    }else{ ?>
                    <div class="form-group col-md-12">
                        <input class="input-btn-script col-md-4 col-sm-4 col-xs-12" type="text" name="product_etcOpt[]">
                        <button class="btn btn-sm btn-normal add_more_button">Add More Fields</button>
                        </div>
                    <?php } ?>
                        
                </div>
                    </div>
                </div>
            </div>
            <div class="col-md-offset-2 col-sm-offset-1 org-submit" style="margin-bottom: 10px;">
            <button class="btn-normal add-submit-btn" type="submit" >Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
    </div>
    </div>
<style type="text/css">
.author-edit-plan input{    padding: 8px 5px;background-color: #fff;border: 1px solid #dbdbdb;border-radius: 2px!important;}
.price-lable{float: left;margin-right: 10px;padding-top: 6px; font-size:13px;}
.amt-margin{margin: 10px 0px;}
.add_more_button{padding: 5px 20px;background-color: #fff;border: 2px solid #2d9f61;color: #2d9f61;font-size: 14px;font-weight: bold;}
.add_more_button:hover{color:#fff;background-color:#2d9f61;}
.remove_field{padding: 5px 20px;background-color: #fff;border: 2px solid #bf0000;color: #bf0000;font-size: 14px;font-weight: bold;}
.remove_field:hover{background-color: #bf0000;color:#fff;}
.add-submit-btn{padding: 5px 20px;background-color: #fff;border: 2px solid #337ab7;color: #337ab7;font-size: 14px;font-weight: bold;}
.add-submit-btn:hover{background-color:#337ab7;color:#fff;}
@media (max-width: 768px){


}
</style>

<script src="{!! asset('sdream/themes/sdream/js/jquery.min.js') !!}"></script>
<script src="{!! asset('sdream/themes/sdream/js/bootstrap.min.js') !!}"></script>
<script type="text/javascript">
        $(document).ready(function() {
    var max_fields_limit  = 20; 
    var x = '<?php echo $etCnt; ?>';
    $('.add_more_button').click(function(e){ 
        e.preventDefault();
        if(x < max_fields_limit){ 
            x++; 
            $('.input_fields_container').append('<div class="form-group col-md-12"><input class="input-btn-script col-md-4 col-sm-4 col-xs-12 " type="text" name="product_etcOpt[]" id="" required="" aria-required="true" type="text" /><button class="btn  btn-normal add_more_button remove_field  style="margin-left:10px;">Remove</button></div>'); 
        }
    });  
    $('.input_fields_container').on("click",".remove_field", function(e){ 
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});

</script>
        </div>
	</div>

@endsection