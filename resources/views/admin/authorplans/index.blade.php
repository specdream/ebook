@extends('admin.layouts.master')

@section('content')

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">{{ trans('quickadmin::templates.templates-customView_index-list') }}</div>
    </div>
    <div class="portlet-body">
        <div id="content" class="bg-container" style="min-height: 425px;">
           <div class="outer">
              <div class="inner bg-container">
                <div class="panel panel-default">
                   <div class="panel-heading admin-head">Organizer View Table</div>
                   <div class="panel-body">
                    <div class="demo">
                        <div class="container">
                            <div class="row">
                                <?php if(count($policyData) > 0){ ?>
                                <div class="col-md-4 col-sm-6">
                                    <div class="pricingTable">
                                        <div class="pricingTable-header">
                                            <h3 class="title">{{ $policyData[0]->title }}</h3>
                                            <span class="sub-title">{{ $policyData[0]->descp }}</span>
                                            <!-- <span class="year">Pay only <br>$110/year</span> -->
                                        </div>
                                        <div class="price-value">
                                            <div class="value">
                                                <!-- <span class="currency">$</span> -->
                                                <span class="amount">{{ $policyData[0]->price }}%<span></span></span>
                                                <span class="month">/{!! trans('core.paid_ticket*Free') !!}</span>
                                            </div>
                                        </div>
                                        <ul class="pricing-content">
                                           <?php 
                                           $listData = explode('||', $policyData[0]->etcOpt);
                                           foreach ($listData as $listDataVal) { ?>
                                           <li>    {{ $listDataVal }}</li>
                                           <?php }
                                           ?>
                                       </ul>
                                       <a href="<?php echo url('/') ?>/edit/plan/1" class="pricingTable-signup">Edit/Add new</a>

                                   </div>
                               </div>

                               <?php } 
                               if(count($policyData) > 1){ 
                                ?>

                                <div class="col-md-4 col-sm-6">
                                    <div class="pricingTable purple">
                                        <div class="pricingTable-header">
                                            <h3 class="title">{{ $policyData[1]->title }}</h3>
                                            <span class="sub-title">{{ $policyData[1]->descp }}</span>
                                            <!-- <span class="year">Pay only <br>$220/year</span> -->
                                        </div>
                                        <div class="price-value">
                                            <div class="value">
                                                <!-- <span class="currency">$</span> -->
                                                <span class="amount">{{ $policyData[1]->price }}%<span></span></span>
                                                <span class="month">/{!! trans('core.paid_ticket*Free') !!}</span>
                                            </div>
                                        </div>
                                        <ul class="pricing-content">
                                            <?php 
                                            $listData = explode('||', $policyData[1]->etcOpt);
                                            foreach ($listData as $listDataVal) { ?>
                                            <li>{{ $listDataVal }}</li>
                                            <?php }
                                            ?>
                                        </ul>
                                        <a href="<?php echo url('/') ?>/edit/plan/2" class="pricingTable-signup">Edit/Add new</a>
                                    </div>
                                </div>
                                <?php } 
                                if(count($policyData) > 2){ 
                                    ?>

                                    <div class="col-md-4 col-sm-6">
                                        <div class="pricingTable blue">
                                            <div class="pricingTable-header">
                                                <h3 class="title">{{ $policyData[2]->title }}</h3>
                                                <span class="sub-title">{{ $policyData[2]->descp }}</span>
                                                <!-- <span class="year">Pay only <br>$220/year</span> -->
                                            </div>
                                            <div class="price-value">
                                                <div class="value">
                                                    <!-- <span class="currency">$</span> -->
                                                    <span class="amount">{{ $policyData[2]->price }}%<span></span></span>
                                                    <span class="month">/{!! trans('core.paid_ticket*Free') !!}</span>
                                                </div>
                                            </div>
                                            <ul class="pricing-content">
                                               <?php 
                                               $listData = explode('||', $policyData[2]->etcOpt);
                                               foreach ($listData as $listDataVal) { ?>

                                               <li>   {{ $listDataVal }}</li>  

                                               <?php }
                                               ?>
                                           </ul>
                                           <a href="<?php echo url('/') ?>/edit/plan/3" class="pricingTable-signup">Edit/Add new</a>
                                       </div>
                                   </div>
                                   <?php } 
                                   ?>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>
</div>

@endsection