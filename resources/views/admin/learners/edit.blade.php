@extends('admin.layouts.master')

@section('content')

    <div class="row">
        <div class="col-sm-10 col-sm-offset-2">
            <h1>Edit learners</h1>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        {!! implode('', $errors->all('
                        <li class="error">:message</li>
                        ')) !!}
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! Form::model($learner, array('files' => true, 'class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'PATCH', 'route' => array(config('quickadmin.route').'.learners.update', $learner->id))) !!}

    <div class="form-group">
        {!! Form::label('name', trans('quickadmin::admin.teacher-edit-name'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('name', old('name',$learner->name), ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.teacher-edit-name_placeholder')]) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('email', trans('quickadmin::admin.teacher-edit-email'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::email('email', old('email',$learner->email), ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.teacher-edit-email_placeholder')]) !!}
        </div>
    </div>

   <!--  <div class="form-group" style="display: none;">
        {!! Form::label('password', trans('quickadmin::admin.teacher-edit-password'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::password('password', ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.teacher-edit-password_placeholder')]) !!}
        </div>
    </div> -->
     <div class="form-group">
        {!! Form::label('dob', trans('quickadmin::admin.teacher-create-dob'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('dob', old('dob',$learner->dob), ['class'=>'form-control datepicker','required'=>'', 'placeholder'=> trans('quickadmin::admin.teacher-create-dob_placeholder')]) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('educational', trans('quickadmin::admin.teacher-create-educational'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('educational', old('educational',$learner->educational), ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.teacher-create-educational_placeholder')]) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('professional', trans('quickadmin::admin.teacher-create-professional'), array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::select('professional', $tb_professional, old('professional',$learner->professional), array('class'=>'form-control','required'=>'')) !!}

        </div>
    </div>
    <div class="form-group">
        {!! Form::label('phone_number', trans('quickadmin::admin.teacher-edit-phone_number'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('phone_number', old('phone_number',$learner->phone_number), ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.teacher-edit-phone_number_placeholder')]) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('address', trans('quickadmin::admin.teacher-edit-address'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('address', old('address',$learner->address), ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.teacher-edit-address_placeholder')]) !!}
        </div>
    </div>

    <div class="form-group">
    {!! Form::label('active', trans('quickadmin::admin.teacher-edit-status'), array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::hidden('active',$learner->address) !!}
        {!! Form::radio('active', 1, true) !!} {!! trans('quickadmin::admin.teacher-edit-status_active') !!}
        {!! Form::radio('active', 0, false) !!} {!! trans('quickadmin::admin.teacher-edit-status_inactive') !!}
        
    </div>
</div>

<div class="form-group">
    {!! Form::label('avatar', trans('quickadmin::admin.teacher-edit-image'), array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::file('avatar') !!}
        
    </div>
</div>

    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit(trans('quickadmin::admin.teacher-edit-btnedit'), ['class' => 'btn btn-primary']) !!}
        </div>
    </div>

    {!! Form::close() !!}

@endsection


