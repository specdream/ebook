@extends('admin.layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
        <h1>{{ trans('quickadmin::templates.templates-view_create-add_new') }}</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

<!-- {!! Form::open(array('files' => true, 'route' => config('quickadmin.route').'.tb_courses.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!} -->
<form action="{{ url('admin/createBookCatg') }}" id="form-with-validation" class="form-horizontal" method="post" enctype="multipart/form-data" >
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="form-group">
    <label class="col-sm-2 control-label">Categorie</label>
    <div class="col-sm-10 call-add-apnd">
    <div class="input">
        <input type="text" class="form-control" name="categorie[]" value="" placeholder="Categorie" required="">
        <p></p>
    </div>    
    </div>
</div>


<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
    <input class="btn btn-primary" value="Add More" id="add" type="button">
      {!! Form::submit( trans('quickadmin::templates.templates-view_create-create') , array('class' => 'btn btn-primary')) !!}
    </div>
</div>

{!! Form::close() !!}
<script type="text/javascript">
 var int = '1';
    $('#add').click(function(){
        var html = '<div class="input inp_'+int+'"><input type="text" class="form-control" name="categorie[]" value="" placeholder="Categorie" required=""><span id="form_input_clear" class="text_remove_'+int+'"><i class="fa fa-minus-circle" aria-hidden="true"></i></span></div>';
        $('.call-add-apnd').append(html);

        int++;
    })
    $(document).on('click','.fa',function(){
        
        $(this).closest('.input').hide();
        
    })        
</script>
@endsection