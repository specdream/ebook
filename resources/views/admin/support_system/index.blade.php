@extends('admin.layouts.master')

@section('content')

@if(\Auth::user()->role_id!='1')
<p> <a href="{!!\URL::To('/'.config('quickadmin.route'))!!}/support_system/create" class="btn btn-success">Add new</a></p>
@endif
@if ($ticket->count())
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">Ticket Details</div>
    </div>

    <div class="portlet-body">
        <div class="table-responsive">
            <table style="margin:0 auto;">
                <tbody><tr>
                <td><span data-toggle="tooltip" class="btn btn-danger" style="margin-right:10px;" title="Open Tickets">  Open&nbsp; <span class="badge badge-info span-sidebar">{!!getTicketCount('open')!!}</span></span></td>
                    <td><span data-toggle="tooltip" class="btn btn-info" style="margin-right:10px;" title="Replied Tickets">  Replied&nbsp; <span class="badge badge-info span-sidebar">{!!getTicketCount('replied')!!}</span></span></td>
                    <td><span data-toggle="tooltip" title="OnHold Tickets" class="btn btn-warning" style="margin-right:10px;">  OnHold <span class="badge badge-info span-sidebar">{!!getTicketCount('hold')!!}</span></span></td>
                    <td><span data-toggle="tooltip" title="Closed Tickets" class="btn btn-success">  Closed <span class="badge badge-info span-sidebar">{!!getTicketCount('closed')!!}</span></span></td>
                </tr>
            </tbody></table>
        </div>
        <table class="table table-striped table-hover table-responsive datatable" id="datatable">
            <thead>
                <tr>
                    <th>
                        {!! Form::checkbox('delete_all',1,false,['class' => 'mass']) !!}
                    </th>
                    <th>Ticket ID</th>
                    <th>Issue on</th>
                    <th>Last update</th>
                    <th>Priority</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
             @foreach ($ticket as $row)
             <tr>
                <td>
                    {!! Form::checkbox('del-'.$row->id,1,false,['class' => 'single','data-id'=> $row->id]) !!}
                </td>
                <td>{!!$row->id!!}</td>
                <td>
                <div class="btn-group" data-toggle="tooltip" title="Cilck to Toggle Quick Info's!!" style="margin-right:5px;"><a class="badge text-warning"    style="background-color:#434A54" data-toggle="dropdown"> <i class="fa  fa-info"></i> </a>

                    <ul class="dropdown-menu   margin-list-rounded with-triangle" style="border-color:#276E6D; ">
                        <li>
                            <ul class="mail-info-detail res_wid" style="overflow:auto; height:180px; font-weight:bold;list-style-type:none;padding-left: 10px; ">
                                <h5 align="center">Ticket Info's</h5>
                                <li class="divider"></li>
                                <li style="padding:5px;">From : <font style=" color:#900">{!!author_name($row->from_id)!!}</font></li>
                                <li style="padding:5px">{!!$row->description!!} </li>
                            </ul>
                        </li>

                    </ul>
                </div>
                {!!$row->subject!!}</td>
                <td>{!!date('d-M-Y',strtotime($row->updated_at))!!}</td>
                <td>
                @if($row->priority=='high')
                <?php $class='danger'; ?>
                @elseif($row->priority=='medium')
                <?php $class='warning'; ?>
                @else
                <?php $class='info'; ?>
                @endif
                <span class="label label-{!!$class!!}">{!!ucfirst($row->priority)!!}</span>
                </td>
                <td>
                @if($row->status=='open')
                <?php $class1='danger'; ?>
                @elseif($row->status=='hold')
                <?php $class1='warning'; ?>
                @elseif($row->status=='replied')
                <?php $class1='info'; ?>
                @else
                <?php $class1='success'; ?>
                @endif
                <span class="label label-{!!$class1!!}">{!!ucfirst($row->status)!!}</span></td>

                <td>
                    @if($row->status=='open' || $row->status=='replied')                                       
                    <a class="fancybox btn-xs btn btn-default" href="{!!\URL::To('/'.config('quickadmin.route'))!!}/support_system/reply/{!!$row->id!!}" data-type="iframe" data-toggle="tooltip" title="Ticket Reply"><i class="fa fa-reply fa-fw" style="text-decoration:none; color:#313940"></i></a>
                    <form method="POST" action="{!!URL::To('/')!!}/admin/support_system/hold/{!!$row->id!!}" accept-charset="UTF-8" style="display: inline-block;" onsubmit="return confirm('Are you sure?');">
                            {{ csrf_field() }}

                        <button type="submit" data-toggle="tooltip" title="Click To Hold Tickets" class="btn btn-xs btn-default"><b><i class="fa fa-pause"></i></b> </button>
                        </form>
                    @elseif($row->status=='hold') 
                    <a class="fancybox btn-xs btn btn-default " href="{!!\URL::To('/'.config('quickadmin.route'))!!}/support_system/reply/{!!$row->id!!}" data-type="iframe" data-toggle="tooltip" title="Ticket Reply"><i class="fa fa-reply fa-fw" style="text-decoration:none; color:#313940"></i></a>
                    @elseif($row->status=='close') 
                    <a class="fancybox btn-xs btn btn-default " href="{!!\URL::To('/'.config('quickadmin.route'))!!}/support_system/reply/{!!$row->id!!}" data-type="iframe" data-toggle="tooltip" title="Click to view chat history"><i class="fa fa-eye fa-fw" style="text-decoration:none; color:#313940"></i></a>
                    @endif
                </td>
             </tr>
            @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-xs-12">
                <button class="btn btn-danger" id="delete">
                    {{ trans('quickadmin::templates.templates-view_index-delete_checked') }}
                </button>
            </div>
        </div>
        {!! Form::open(['route' => config('quickadmin.route').'.tb_categories_type.massDelete', 'method' => 'post', 'id' => 'massDelete']) !!}
        <input type="hidden" id="send" name="toDelete">
        {!! Form::close() !!}
    </div>
</div>
@else
{{ trans('quickadmin::templates.templates-view_index-no_entries_found') }}
@endif

@endsection

@section('javascript')
    <!-- <script src="{{ url('js/fancybox') }}/jquery-1.8.2.min.js"></script> -->

<script>

    $(document).ready(function () {
        $('#delete').click(function () {
            if (window.confirm('{{ trans('quickadmin::templates.templates-view_index-are_you_sure') }}')) {
                var send = $('#send');
                var mass = $('.mass').is(":checked");
                if (mass == true) {
                    send.val('mass');
                } else {
                    var toDelete = [];
                    $('.single').each(function () {
                        if ($(this).is(":checked")) {
                            toDelete.push($(this).data('id'));
                        }
                    });
                    send.val(JSON.stringify(toDelete));
                }
                $('#massDelete').submit();
            }
        });
    });

    $(document).ready(function() {
        toastr.options={
            "closeButton":true,
            "positionClass":"toast-top-right",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"

        }
        <?php if(implode('', $errors->all(':message'))=='create'){?>
        toastr.success('Success ! <br/> Ticket created successfully </b>');
        <?php }else if(implode('', $errors->all(':message'))=='hold'){?>
        toastr.info('Success ! <br/> Ticket holded successfully </b>');
        <?php }else if(implode('', $errors->all(':message'))=='close'){?>
        toastr.success('Success ! <br/> Ticket closed successfully </b>');
        <?php } ?>
    });

</script>
<script src="{{ url('js/fancybox') }}/jquery-1.8.2.min.js"></script>
<script src="{{ url('js/fancybox') }}/jquery.fancybox.js"></script>
<script src="{{ url('js/fancybox') }}/jquery.fancybox.pack.js"></script>
<script type="text/javascript">
var jQuery_fancybox = $.noConflict(true);
    jQuery_fancybox(document).ready(function() {

    jQuery_fancybox('.fancybox').fancybox({
      
        fitToView   : true,
        width       : '90%',
        height      : '96%',  
        autoSize    : false,
        closeClick  : false,
        openEffect  : 'fade',
        closeEffect : 'none',
        type: 'iframe',
        topRatio     : 0,
        padding  :[0,0,0,0],
             helpers   : { 
       
          overlay : {closeClick: false}},
                afterClose : function() {
            // parent.location.reload("<?php echo URL::To('/');?>/admin/support_system");
        },
      });
});

</script>

@stop