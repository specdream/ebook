@extends('admin.layouts.fancybox')

@section('content')

<div class="row" style="margin-top:8px; ">
  <div class="col-sm-12">
    <div class="col-md-8 col-sm-6">
      <div class="panel panel-default panel-square panel-border">
        <div class="panel-heading" style="background-color:#884C55; color:#FFF">
        <h4 class="panel-title"><i class="fa fa-comments"></i> Chat History &nbsp;&nbsp; <span style=" float:right; margin-right:5px; font-size:14px; background-color:#A93E4D; color: #FFF" class="label label-warning">Chat Up: {!!author_name($ticket->from_id)!!} </span></h4>
        </div>
        <div id="panel-collapse-4" class="collapse in">
          <div class="panel-body">
            <div class="chat-wrap scroll-chat-widget" style="background-color:#F5F5F5; border:1px solid #F0F0F0">
              <ul class="media-list media-xs media-dotted media-chat">
                <li class="media">
                  <a class="pull-left">
                    <img class="media-object img-circle" src="{!!URL::To('/')!!}/public/uploads/{!!getAvatar($ticket->from_id)!!}" alt="Avatar">
                  </a>
                  <div class="media-body" style="background-color:#7C6B80; color:#FFF">
                    <p class="name"><small>{!!author_name($ticket->from_id)!!}</small>  <span style="float:right; background-color:#FFF; color:#000"  class="label label">{!!date('l jS F Y',strtotime($ticket->created_at))!!} [ {!!date('g:i A',strtotime($ticket->created_at))!!} ]</span></p>
                    <p class="name">Subject : {!!$ticket->subject!!}</p>
                    <p class="small" >
                      <div class="the-box" style="color:#000">
                       {!!$ticket->description!!}
                     </div>
                   </p>
                   <p style="color:#FFF"><small><i class="fa fa-clock-o"></i> 
                    5 hours, 12 seconds ago                                          </small></p>
                  </div>
                </li> 
                @foreach($reply_det as $reply_detail)
                <hr>  
                <li class="media">
                  <a class="pull-right" href="#fakelink">
                    <img class="media-object img-circle" src="{!!URL::To('/')!!}/public/uploads/{!!getAvatar($reply_detail->from_id)!!}" alt="Avatar">
                  </a>
                  <div class="media-body me" style="background-color:#CBC0CE; color:#000">
                    <p class="name"><span style="float:left; background-color:#FFF; color:#000" class="label label">{!!date('l jS F Y',strtotime($reply_detail->created_at))!!} [ {!!date('g:i A',strtotime($reply_detail->created_at))!!} ]</span><span style="float:right;">{!!author_name($reply_detail->from_id)!!}</span></p>
                    <p class="small" style="margin-top:30px">
                    </p><div class="the-box" style="color:#000">
                    {!!$reply_detail->description!!}
                  </div>
                  <p></p>
                  <p style="color: #333"> <small><i class="fa fa-clock-o"></i> 
                    504 days, 21 hours, 56 minutes, 19 seconds ago                       
                  </small></p>
                </div>
                <div></div></li>
                @endforeach

              </ul>
            </div>
            @if($ticket->status!='close')
            <div class="action-chat">
              <div class="row">
                <form method="POST" action="{!!URL::To('/')!!}/admin/support_system/chat/{!!$ticket->id!!}" accept-charset="UTF-8" >
          {{ csrf_field() }}
          <input type="hidden" name="chat_id" value="{!!$ticket->id!!}">
                  <div class="col-sm-10">
                    <div class="form-group">
                      <textarea name="description" rows="1" class="form-control no-resize rounded" placeholder="Enter The Comments !" required=""></textarea>
                    </div>

                  </div><!-- /.col-xs-8 -->
                  <div class="col-sm-1" style="margin-top:10px;">
                    <div class="form-group">
                      <button type="submit" data-toggle="tooltip" title="Post Reply" class="btn" style="background-color:#A93E4D; color:#FFF" ><i class="fa fa-comments-o"></i> Reply</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            @endif
            <br/>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-sm-6" >
      <!-- BEGIN TEAM LIST -->
      <div class="panel panel-default panel-square panel-border" style="background-color:">
        <div class="panel-heading" style="background-color:#884C55; color:#FFF">
          <div class="right-content">
          </div>
          <h4 class="panel-title"><i class="fa fa-envelope"></i> Ticket Info's &nbsp;&nbsp;

           <span class="label label-danger" style="margin-right:5px; float:right;">Ticket {!!ucfirst($ticket->status)!!}</span></h4>
         </div>
         <div id="panel-collapse-5" class="collapse in">
          <div class="panel-body">
            <ul class="media-list media-xs media-dotted">
              <li class="media">
                <div class="media-body">
                  <h4 class="media-heading"><span style="font-size:10px;"><i class="fa fa-hand-o-right"></i></span> <strong>TID :</strong>&nbsp; {!!$ticket->id!!}</h4>
                </div>
              </li>
              <li class="media">
                <div class="media-body">
                  <h4 class="media-heading"><span style="font-size:10px;"><i class="fa fa-hand-o-right"></i></span> <strong>From :</strong>&nbsp; {!!author_name($ticket->from_id)!!} [ {!!strtoupper(getRoleInId($ticket->from_id))!!} ]</h4>
                </div>
              </li>
              <li class="media">
                <div class="media-body">
                  <h4 class="media-heading"><span style="font-size:10px;"><i class="fa fa-hand-o-right"></i></span> <strong>Priority :</strong>&nbsp; <span class="">{!!$ticket->priority!!} </span>
                  </h4>
                </div>
              </li>
              <li class="media">
                <div class="media-body">
                  <h4 class="media-heading"><span style="font-size:10px;"><i class="fa fa-hand-o-right"></i></span>  <strong>Subject:</strong>&nbsp; {!!$ticket->subject!!}</h4>
                </div>
              </li>
              <li class="media">
               <div class="the-box">
                <p> {!!$ticket->description!!} </p>
              </div>
            </li>
          </ul>
          @if($ticket->status!='close')
          <form method="POST" action="{!!URL::To('/')!!}/admin/support_system/closed/{!!$ticket->id!!}" accept-charset="UTF-8" style="display: inline-block;" onsubmit="return confirm('Are you sure?');">
          {{ csrf_field() }}
            <button type="submit" data-toggle="tooltip" title="Close Ticket" class="btn alert alert-block fade btn-perspective in alert-dismissable" align="center" style=" background-color:#74AD3B; color:#FFF; text-decoration:none " ><strong>Solution Found !</strong> Click To Close Ticket Now </button>
          </form>
          @endif
         <br/><br/>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
   <script src="{{ url('js/fancybox') }}/jquery-1.8.2.min.js"></script>
    <script src="{{ url('js/fancybox') }}/jquery.fancybox.js"></script>
    <script src="{{ url('js/fancybox') }}/jquery.fancybox.pack.js"></script>
<script>

  $(document).ready(function() {
    toastr.options={
      "closeButton":true,
      "positionClass":"toast-top-right",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"

    }
    <?php if(implode('', $errors->all(':message'))=='reply'){?>
    toastr.success('Success ! <br/> Ticket Replied successfully </b>');
    <?php }else if(implode('', $errors->all(':message'))=='close'){?>
    toastr.success('Success ! <br/> Ticket closed successfully </b>');
    <?php } ?>
  });


</script>
@endsection