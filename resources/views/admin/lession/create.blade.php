@extends('admin.layouts.master')

@section('content')

    <div class="row">
        <div class="col-sm-10 col-sm-offset-2">
            <h1>{{ trans('quickadmin::admin.lessions-create-create_user') }}</h1>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        {!! implode('', $errors->all('
                        <li class="error">:message</li>
                        ')) !!}
                    </ul>
                </div>
            @endif
        </div>
    </div>
<form method="POST" action="{!!URL::To('/')!!}/admin/lession/store/{!!$course_id!!}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="course_id" value="{!!$course_id!!}">
    <input type="hidden" name="author_id" value="{{ isset($course->teacher_id) ? $course->teacher_id : '' }}">

    <div class="form-group">
        {!! Form::label('name', trans('quickadmin::admin.lessions-create-name').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('name', old('name'), ['class'=>'form-control keyvalid','required'=>'', 'placeholder'=> trans('quickadmin::admin.lessions-create-name_placeholder')]) !!}
            <span id="name_msg" style="color: #F00;"></span>
        </div>
    </div>
     <div class="form-group">
        {!! Form::label('description', trans('quickadmin::admin.lessions-create-description').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('description', old('description'), ['class'=>'form-control keyvalid','required'=>'', 'placeholder'=> trans('quickadmin::admin.lessions-create-description_placeholder')]) !!}
            <span id="description_msg" style="color: #F00;"></span>
        </div>
    </div>
    <div class="form-group">
    {!! Form::label('assessment_test', 'Assessment Test*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        <input type="radio" class="assessment_test" name="assessment_test" value="yes" checked=""> Yes 
        <input type="radio" class="assessment_test" name="assessment_test" value="no" > No 
    </div>
</div>

<div class="form-group hide_num">
        {!! Form::label('num_que', 'Number of Questions*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('num_que', old('num_que'), ['class'=>'form-control', 'placeholder'=>'Enter number of questions per each test']) !!}
        </div>
    </div>
   <div class="form-group hide_num">
        {!! Form::label('pass_percentage', 'Pass Percentage*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('pass_percentage', old('pass_percentage'), ['class'=>'form-control', 'placeholder'=>'Enter Pass Percentage','required'=>'']) !!}
        </div>
    </div> 

    <div class="form-group hide_num">
        {!! Form::label('time', 'Time *', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-3">
             <select name="hours" id="hours" class="form-control">
                <option value="" > - Select hours - </option>
                <option value="0" > 00 </option>
                <option value="1" > 01 </option>
                <option value="2" > 02 </option>
                <option value="3" > 03 </option>
                <option value="4" > 04 </option>
                <option value="5" > 05 </option>
                <option value="6" > 06 </option>
                <option value="7" > 07 </option>
                <option value="8" > 08 </option>
                <option value="9" > 09 </option>
                <option value="10"> 10 </option>
                <option value="11"> 11 </option>
                <option value="12"> 12 </option>
            </select>
        </div>
        <div class="col-sm-3">
             <select name="minutes" id="minutes" class="form-control">
                <option value="" > - Select minitus - </option>
                <option value="0" > 00 </option>
                <option value="05" > 05 </option>
                <option value="10" > 10 </option>
                <option value="15" > 15 </option>
                <option value="20" > 20 </option>
                <option value="25" > 25 </option>
                <option value="30" > 30 </option>
                <option value="35" > 35 </option>
                <option value="40" > 40 </option>
                <option value="45" > 45 </option>
                <option value="50" > 50 </option>
                <option value="55" > 55 </option>
                <option value="60" > 60 </option>
            </select>
        </div>
    </div>

    <div class="form-group" style="display: none;">
    {!! Form::label('test_type', trans('quickadmin::admin.lessions-create-test-type'), array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::select('test_type', $test_type, old('test_type'), array('class'=>'form-control')) !!}
        
    </div>
</div>
    <div class="form-group " id="hiddenDiv" >
    {!! Form::label('question_subtype', 'Lesson video type*', ['class'=>'col-sm-2 control-label']) !!}        
        <div class="col-md-10">
            <select name="subtype" id="subtype_id" class="form-control" required="">
                <option value="" > - Select - </option>
                <option value="1" > Upload </option>
                <option value="2" > Url </option>
            </select>
        </div> 
    </div>
    <div class="form-group  " id="hiddenDiv1" style="display:none">
         {!! Form::label('video', trans('quickadmin::admin.lessions-create-video').'*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::file('video',array('accept'=>'.mp4,.MP4')) !!}

        </div>
    </div>
    <div class="form-group" id="hiddenDiv2" style="display:none">
        {!! Form::label('file_url', 'Url*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-md-10">
            <input type="text" name='file_url' id='file_url' class='form-control ' >                                   
            </div>
    </div>
   

    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit(trans('quickadmin::admin.lessions-create-btncreate'), ['class' => 'btn btn-primary']) !!}
        </div>
    </div>

    </form>
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript">
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var base_url=$("#base_url").val();
    $(document).on('keyup','.keyvalid',function(){
        var keyval=$(this).val();
        var id=$(this).attr('name');
        // alert(id);
        var token=$('meta[name="csrf-token"]').attr('content');
        if(keyval!=''){
        $.ajax({
            type:'POST',
            url:base_url+"/admin/tb_courses/checkkey",
            dataType: 'json',
            data:{keyval:keyval,_token:token},
            success:function(data){
              // alert(data);
              if(data>0){
                // alert('Key already exist..');
                $("#"+id+"_msg").html('Key already exist..');
                 $("#"+id).css("border", "#F00 solid 1px"); 
                 $(".btn-primary").prop('disabled',true);
              }else{
                $("#"+id).css("border", "#ccc solid 1px");
                $("#"+id+"_msg").html('');
                $(".btn-primary").prop('disabled',false);
              }
          }
      });
    }else{
        $("#"+id).css("border", "#ccc solid 1px");
                $("#"+id+"_msg").html('');
                $(".btn-primary").prop('disabled',false);
    }
    })
    $(document).on('change','#subtype_id',function(){
    if($(this).val()!=''){
        if($(this).val()=='1'){
            $("#hiddenDiv1").show();
            $("#hiddenDiv2").hide();
        }else{
            $("#hiddenDiv1").hide();
            $("#hiddenDiv2").show();
        }
    }else{
        $("#hiddenDiv1").hide();
        $("#hiddenDiv2").hide();
    }
})
    $(document).on('change','.assessment_test',function(){
        if($(this).val()=='no'){
            $(".hide_num").hide();
        }else{
            $(".hide_num").show();
        }
    })
    </script>
@endsection


