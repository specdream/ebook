@extends('admin.layouts.master')

@section('content')

   
   <p> <a href="{!!\URL::To('/'.config('quickadmin.route'))!!}/lession/create/{!!$course_id!!}" class="btn btn-success">Add new</a></p>

    @if($lessions->count() > 0)
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ trans('quickadmin::admin.lessions-index-lessions_list') }}</div>
            </div>
            <div class="portlet-body">
            <div><h4><b>Course Name : </b>{!!$course_name!!} ;</h4></div><br>
                <table id="datatable" class="table table-striped table-hover table-responsive datatable">
                    <thead>
                    <tr>
                        <th>{{ trans('quickadmin::admin.lessions-index-id') }}</th>
                        <th>{{ trans('quickadmin::admin.lessions-index-title') }}</th>
                        <!-- <th>{{ trans('quickadmin::admin.lessions-index-test-type') }}</th> -->
                        <th>{{ trans('quickadmin::admin.lessions-index-description') }}</th>
                        <th>Assessment Test</th>
                        <th>Video</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($lessions as $lession)
                        <tr>
                            <td>{!!$lession->id!!}</td>
                            <td>{!!$lession->name!!}</td>
                            <!-- <td>{{ isset($lession->Test_type->name) ? $lession->Test_type->name : '' }}</td> -->
                            <td>{{ $lession->description }}</td>
                            <td>{{ ucfirst($lession->assessment_test) }}</td> 
                            <td><a class="label label-warning"  href="{!!\URL::To('/public/uploads/lesson/'.$lession->video)!!}" download="" title="Click to download"><i class="fa fa-download"></i> Video</a> &nbsp;&nbsp;<a data-toggle="modal" data-target="#playModal{{$lession->id}}" class="label label-info" title="Click to play video"><i class="fa fa-play-circle" aria-hidden="true"></i> Video</a>
                               <!-- Modal -->
                               <div class="modal fade" id="playModal{{$lession->id}}" role="dialog">
                                <div class="modal-dialog">
                                    
                                  <!-- Modal content-->
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title"><b>Play ( {{ $lession->name }} ) video</b></h4>
                                  </div>
                                  <div class="modal-body">
                                      <div class="margin-bottom-50">
                                        <div class="embed-responsive embed-responsive-16by9 thumbnail">
                                            <video style="width: 100%" controls>
                                                <source src="{!!\URL::To('/public/uploads/lesson/'.$lession->video)!!}" type="video/mp4">
                                                    Your browser does not support the video element.
                                                </video>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  </div>
                              </div>
                              
                          </div>
                      </div>
                  </td>
                  <td>
                                <a href="{!!\URL::To('/'.config('quickadmin.route'))!!}/lession/{!!$lession->id!!}/edit/{!!$course_id!!}" class="btn btn-xs btn-success" title="Click to edit lession"><i class="fa fa-edit"></i></a>
                                <form method="POST" action="{!!\URL::To('/'.config('quickadmin.route'))!!}/lession/destroy/{!!$lession->id!!}/{!!$course_id!!}" accept-charset="UTF-8" style="display: inline-block;" onsubmit="return confirm('Are you sure?');">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-xs btn-danger" title="Click to delete lession"><i class="fa fa-trash"></i> </button>
                                </form>
                                <a href="{!!\URL::To('/'.config('quickadmin.route'))!!}/question/{!!$course_id!!}/{{ $lession->id }}" class="btn btn-xs btn-info" title="Click to manage question"><i class="fa fa-question-circle"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @else
        {{ trans('quickadmin::admin.lessions-index-no_entries_found') }}
    @endif

@endsection