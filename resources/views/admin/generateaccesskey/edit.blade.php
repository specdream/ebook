@extends('admin.layouts.master')

@section('content')

    <div class="row">
        <div class="col-sm-10 col-sm-offset-2">
            <h1>EDIT COUPAN</h1>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        {!! implode('', $errors->all('
                        <li class="error">:message</li>
                        ')) !!}
                    </ul>
                </div>
            @endif
        </div>
    </div>

    <form action="{{ url('coupon/update') }}" method="post">

    
    <div class="form-group">
        <label class="col-sm-2 control-label">Coupan:</label>
        <div class="col-sm-10">
            <input type="text" value="{{ $company->code }}" required="" id="code" placeholder="Key" class="form-control" name="code">
            <div class="cou-icon">
            <i class="getKey fa fa-refresh"></i>
          </div>
        </div>
    </div>

    <div class="form-group">
        <label for="username" class="col-sm-2 control-label">Offer Type:</label>
        <div class="col-sm-10 cpmrgn">
          <select name="type" required="true" class="form-control">
              <option <?php if($company->type == 'discount') echo"selected"; ?> value="discount">Discount</option>
              <option <?php if($company->type == 'price-reduces') echo"selected"; ?> value="price-reduces">Price reduces</option>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="username" class="col-sm-2 control-label">Price:</label>
        <div class="col-sm-10 cpmrgn">
          <input class="form-control" required="true" placeholder="Price or discount"  name="price" type="number" value="{{ $company->price }}" id="price">
        </div>
      </div>
      <div class="form-group">
        <label for="username" class="col-sm-2 control-label">Date:</label>
        <div class="col-sm-10 cpmrgn">
          <input class="form-control" required="true" placeholder="Expire date"  name="date" type="Date" value="{{ $company->expire }}" id="date">
        </div>
      </div>



    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit(trans('quickadmin::admin.companies-edit-btnedit'), ['class' => 'btn btn-primary']) !!}
        </div>
    </div>

    </form>

@endsection
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.getKey').click(function(){
         $.ajax({
            type: "GET",
            url: '<?php echo url('genCoupon') ?>',
            success: function(data)
            {
                $('#code').val(data);
            }
        })                       
        })
    })
</script>


