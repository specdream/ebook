@extends('admin.layouts.master')

@section('content')

    <h2>Access Key GENERATE</h2>
    <form action="{{ url('coupon/store') }}" method="post" >
    {!! csrf_field() !!}
      <div class="form-group">     
        <label for="username" class="col-sm-2 control-label">Coupan:</label>
        <div class="col-sm-10 cpmrgn">
          <input class="form-control" required="true" placeholder="Key" id="code"  name="code" type="text" value="">
          <div class="cou-icon">
            <i class="getKey fa fa-refresh"></i>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label for="username" class="col-sm-2 control-label">Offer Type:</label>
        <div class="col-sm-10 cpmrgn">
          <select name="type" required="true" class="form-control">
              <option value="discount">Discount</option>
              <option value="price-reduces">Price reduces</option>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="username" class="col-sm-2 control-label">Price:</label>
        <div class="col-sm-10 cpmrgn">
          <input class="form-control" required="true" placeholder="Price or discount"  name="price" type="number" value="" id="price">
        </div>
      </div>
      <div class="form-group">
        <label for="username" class="col-sm-2 control-label">Date:</label>
        <div class="col-sm-10 cpmrgn">
          <input class="form-control" required="true" placeholder="Expire date"  name="date" type="Date" value="" id="date">
        </div>
      </div>
      <div class="centerbtn">
        <button class="btn btn-primary btn-colr bt-primary" type="submit"> Generate </button>
      </div>
    </form>

@endsection
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.getKey').click(function(){
         $.ajax({
            type: "GET",
            url: '<?php echo url('genCoupon') ?>',
            success: function(data)
            {
                $('#code').val(data);
            }
        })                       
        })
    })
</script>
