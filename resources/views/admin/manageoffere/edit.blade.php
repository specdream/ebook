@extends('admin.layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
        <h1>{{ trans('quickadmin::templates.templates-view_create-add_new') }}</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

<!-- {!! Form::open(array('files' => true, 'route' => config('quickadmin.route').'.tb_courses.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!} -->
<form action="{{ url('admin/editOffer').'/'.$offer->id }}" id="form-with-validation" class="form-horizontal" method="post" enctype="multipart/form-data" >
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="form-group">
    <label class="col-sm-2 control-label">Label</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="name" value="{{ $offer->label }}" placeholder="English" required="">
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Offer type</label>
    <div class="col-sm-10">
    <select name="type" id="offerType">
            <option @if($offer->type == '1') selected @endif value="1">Discount</option>
            <option @if($offer->type == '2') selected @endif value="2">Price reduce</option>
        </select>
        
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Offer</label>
    <div class="col-sm-10">
        
        
        <input type="text" class="form-control" name="offer" value="{{ $offer->offer }}" placeholder="English" required="">
        
        
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Status</label>
    <div class="col-sm-10">
         <select class="form-control" name="status">
            <option @if($offer->status == '1') selected @endif value="1">Active</option>
            <option @if($offer->status == '2') selected @endif value="2">De-Active</option>
        </select>
        
    </div>
</div>



<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
      <input class="btn btn-primary" value="Edit" name="submit" type="submit">
    </div>
</div>

{!! Form::close() !!}
@endsection