@extends('admin.layouts.master')

@section('content')

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">{{ trans('quickadmin::templates.templates-customView_index-list') }}</div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-hover table-responsive datatable" id="datatable">
            <thead>
            <tr>
            	<th>S.No</th>
            	<th>Title</th>
            	<th>Type</th>
            	<th>Price / Discount</th>
            	<th>Status</th>
            	<th> </th>
            </tr>
            </thead>
            <tbody>
            <?php $i='1'; foreach ($offers as $key => $value) {
            	if($value->type == '1'){
            		$type = 'Discount';
            		$price = $value->offer.' %';
            	}else{
            		$type = 'Price reduce';
            		$price = '$ '.$value->offer;
            	}
             ?>
            <tr>
            	<td>{{ $i }}</td>
            	<td>{{ $value->label }}</td>
            	<td>{{ $type }}</td>
            	<td>{{ $price }}</td>
            	<td>@if($value->status == '1') Active @else De-Active @endif</td>
            	<td>
            	<a href="{{ url('admin/editOffer').'/'.$value->id }}" class="btn btn-xs btn-info" title="Click to edit language"><i class="fa fa-edit"></i></a>
				</td>
               </tr> 
            <?php $i++; } ?>
            	</tbody>
            </table>
        </div>
	</div>

@endsection