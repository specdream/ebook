@extends('admin.layouts.master')

@section('content')

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption"> Manage Books </div>
        </div>
        <div class="portlet-body">
        <a href="{{ url('admin/bookCreate') }}"><button class="">Create</button></a>
            <table class="table table-striped table-hover table-responsive datatable" id="datatable">
            <tr>
            	<th>S.No</th>
            	<th>Title</th>
            	<th>Teacher Name</th>
            	<th>Action</th>
            </tr>
            <tr>
            	<td></td>
            	<td></td>
            	<td></td>
            	<td></td>
            </tr>
            </table>
        </div>
	</div>

@endsection