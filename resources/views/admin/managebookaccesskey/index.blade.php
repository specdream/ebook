@extends('admin.layouts.master')

@section('content')

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">{{ trans('quickadmin::templates.templates-customView_index-list') }}</div>
        </div>
        <div class="portlet-body">
            
            <table class="table table-striped table-hover table-responsive datatable" id="datatable tbl-book-create">
             <thead>
            <tr>
            	<th>S.No</th>
            	<th>Teacher</th>
            	<th>Title</th>
                <th>Semester</th>
                <th>Degree</th>
                <th>Price Type</th>
                <th>Price</th>
                <th>ISBN</th>
            	<th></th>
            </tr>
             </thead>
             <tbody>
            <?php $si = '1'; foreach($book as $bookVal){ 
                if($bookVal->price_type != 'Paid'){
                    $priceType = 'Free';
                }else{
                    $priceType = 'Paid';
                }
            ?> 
            <tr>
            	<td>{{ $si }}</td>
            	<td>{{ \Helper::getUser($bookVal->user_id,'name') }}</td>
            	<td>{{ $bookVal->title }}</td>
            	<td>{{ 'asd' }}</td>
            	<td>{{ 'as' }}</td>
                <td>{{ $priceType }}</td>
                <td>{{ $bookVal->price }}</td>
                <td>{{ $bookVal->isbn }}</td>
                <td>
        <ul class="list-keys">
        <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-list"></i><b class="caret"></b></a>
              <ul class="dropdown-menu list-key-drop">
                <li><a href="{{ url('/admin/bookKeys').'/'.$bookVal->id.'/store' }}" class="btn btn-xs " title="Manage"><i class="fa fa-list"></i>Store key</a></li>
                <li>   <a href="{{ url('/admin/bookKeys').'/'.$bookVal->id.'/online' }}" class="btn btn-xs " title="Manage"><i class="fa fa-list"></i>online key</a></li>
              </ul>
        </li>
        </ul>
                </td>
                </tr>
            <?php $si++; } ?>    
            </tbody>
            </table>
        </div>
	</div>

 <style type="text/css">
     .list-keys{list-style-type:none;padding:0;}
     .dropdown-menu.list-key-drop{left:-85px;}
     .dropdown-menu.list-key-drop:after{left: 85px;}
     .dropdown-menu.list-key-drop:before{left: 84px;}
     .list-key-drop .fa-list{margin-right:10px;margin-top:5px;}
 </style>   
<script type="text/javascript">
    $('li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});
</script>
@endsection