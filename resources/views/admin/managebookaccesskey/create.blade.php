@extends('admin.layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
        <h1>{{ trans('quickadmin::templates.templates-view_create-add_new') }}</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

<!-- {!! Form::open(array('files' => true, 'route' => config('quickadmin.route').'.tb_courses.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!} -->
<form action="{{ url('admin/storeBookKey') }}" id="form-with-validation" class="form-horizontal" method="post" enctype="multipart/form-data" >
<input type="hidden" name="_token" value="{{ csrf_token() }}">


<div class="form-group">
    <label class="col-sm-2 control-label">Book ID</label>
    <div class="col-sm-10 call-add-apnd">
    <div class="input">
        <input type="text" class="form-control" name="bookId" value="{{ $id }}" readonly="">
        <p></p>
    </div>    
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Prefix</label>
    <div class="col-sm-10 call-add-apnd">
    <div class="input">
        <input type="text" class="form-control" name="prefix" value="{{ $book->prefix }}" readonly="">
        <p></p>
    </div>    
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Quantity</label>
    <div class="col-sm-10 call-add-apnd">
    <div class="input">
        <select name="quantity" id="quantity">
            <option value="500">1000</option>
        </select>
        <p></p>
    </div>    
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Online / Store</label>
    <div class="col-sm-10 call-add-apnd">
    <div class="input">
        <select name="place" id="place">
            <option value="online">online</option>
            <option value="store">store</option>
        </select>
        <p></p>
    </div>    
    </div>
</div>

<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
    <!-- <input class="btn btn-primary" value="Add More" id="add" type="button"> -->
      {!! Form::submit( trans('quickadmin::templates.templates-view_create-create') , array('class' => 'btn btn-primary')) !!}
    </div>
</div>

{!! Form::close() !!}

@endsection