@extends('admin.layouts.master')

@section('content')

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">{{ trans('quickadmin::templates.templates-customView_index-list') }}</div>
        </div>
        <div class="portlet-body">
            <p style="float: left; margin-right: 5px"><a href="{{ url('admin/createBookKeys').'/'.$id }}" class="btn btn-success">Create</a></p>  @if($place == 'store')
            <p><a href="{{ url('admin/downloadBookKeys').'/'.$id.'/'.$place }}" class="btn btn-success">Download</a></p> 
            @endif
            <table class="table table-striped table-hover table-responsive datatable" id="datatable">
            <thead>
              <tr>
               <th>S.No</th>
               <th>Book_id</th>
               <th>Prefix</th>
               <th>Key</th>
               <th>Store/Online</th>
               <th>Status</th>
             </tr>
           </thead>
           <tbody>
            <?php 
            $row = 0;
            if(count($keys) > 0){
                foreach ($keys as $value) {
                    
                
if (($handle = fopen("public/uploads/access_csv/".$value->book_id."/".$value->type."/".$value->file, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $num = count($data);
        //echo "<p> $num fields in line $row: <br /></p>\n";
        $row++;
         ?>
            <tr>
        <?php for ($c=0; $c < $num; $c++) {
            echo "<td>";
            if($c == '0'){
               // echo "<input type='checkbox' name='check' value='".$row."'>";
            }
            echo "<input type='hidden' name='place' value='".$row."'><input type='hidden' name='key' value='".$c."'>".$data[$c] ;
            echo  "</td>";
        
         } ?>
        </tr>
        <?php 
    }
    fclose($handle);
}
} }
            ?>
        </div>
	</div>

@endsection