@extends('admin.layouts.master')

@section('content')

    <!-- <p>{!! link_to_route(config('quickadmin.route').'.company.create', trans('quickadmin::admin.companies-index-add_new'), [], ['class' => 'btn btn-success']) !!}</p> -->
    <p><a href="{{ url('admin/coupon/create') }}" class="btn btn-success">Add New</a></p>

    @if($companies->count() > 0)
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">Coupon List</div>
            </div>
            <div class="portlet-body">
                <table id="datatable" class="table table-striped table-hover table-responsive datatable">
                    <thead>
                    <tr>
                        <th>S No</th>
                        <th>Code</th>
                        <th>Expire</th>
                        <th>Price / Discount</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php $i = '1';
                    foreach ($companies as $company){
                        if($company->status == '1'){
                                $status = 'Active';
                            }else{
                                $status = 'De-Active';
                            }
                         ?>
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $company->code }}</td>
                            <td>{{ $company->expire }}</td>
                            <td>{{ $company->price }}</td>
                            <td>{{ $company->type }}</td>
                            <td>{{ $status }}</td>
                            <td>
                                <!-- {!! link_to_route(config('quickadmin.route').'.company.edit', trans('quickadmin::templates.templates-view_index-edit'), array($company->id), array('class' => 'btn btn-xs btn-info')) !!} -->
                                 <a href="{!!\URL::To('/'.config('quickadmin.route'))!!}/coupon/{{ $company->id }}/edit" class="btn btn-xs btn-info" title="Click to edit univerisities"><i class="fa fa-edit"></i></a>

                                {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => "return confirm('".trans("quickadmin::templates.templates-view_index-are_you_sure")."');",  'route' => array(config('quickadmin.route').'.company.destroy', $company->id))) !!}
                                <button type="submit" class="btn btn-xs btn-danger" title="Click to delete"><i class="fa fa-trash"></i> </button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    <?php  $i++; } ?>
                    </tbody>
                </table>
            </div>
        </div>

    @else
        {{ trans('quickadmin::admin.companies-index-no_entries_found') }}
    @endif

@endsection