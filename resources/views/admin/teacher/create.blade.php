@extends('admin.layouts.master')

@section('content')

    <div class="row">
        <div class="col-sm-10 col-sm-offset-2">
            <h1>{{ trans('quickadmin::admin.teacher-create-create_user') }}</h1>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        {!! implode('', $errors->all('
                        <li class="error">:message</li>
                        ')) !!}
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! Form::open(['files' => true, 'route' => config('quickadmin.route').'.teacher.store', 'class' => 'form-horizontal']) !!}

    <div class="form-group">
        {!! Form::label('name', trans('quickadmin::admin.teacher-create-name').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('name', old('name'), ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.teacher-create-name_placeholder')]) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('email', trans('quickadmin::admin.teacher-create-email').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::email('email', old('email'), ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.teacher-create-email_placeholder')]) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('password', trans('quickadmin::admin.teacher-create-password').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::password('password', ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.teacher-create-password_placeholder')]) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('dob', trans('quickadmin::admin.teacher-create-dob').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('dob', old('dob'), ['class'=>'form-control datepicker','required'=>'', 'placeholder'=> trans('quickadmin::admin.teacher-create-dob_placeholder')]) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('educational', trans('quickadmin::admin.teacher-create-educational').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('educational', old('educational'), ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.teacher-create-educational_placeholder')]) !!}
        </div>
    </div>
    <div class="form-group" style="display: none;">
        {!! Form::label('professional', trans('quickadmin::admin.teacher-create-professional'), array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::select('professional', $tb_professional, old('professional'), array('class'=>'form-control')) !!}

        </div>
    </div>
    <div class="form-group">
        {!! Form::label('phone_number', trans('quickadmin::admin.teacher-create-phone_number').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('phone_number', old('phone_number'), ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.teacher-create-phone_number_placeholder')]) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('address', trans('quickadmin::admin.teacher-create-address').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('address', old('address'), ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.teacher-create-address_placeholder')]) !!}
        </div>
    </div>

    <div class="form-group">
    {!! Form::label('active', trans('quickadmin::admin.teacher-create-status'), array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::hidden('active','') !!}
        {!! Form::radio('active', 1, true) !!} {!! trans('quickadmin::admin.teacher-create-status_active') !!}
        {!! Form::radio('active', 0, false) !!} {!! trans('quickadmin::admin.teacher-create-status_inactive') !!}
        
    </div>
</div>

<div class="form-group">
    {!! Form::label('avatar', trans('quickadmin::admin.teacher-create-image'), array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::file('avatar') !!}
        
    </div>
</div>

    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit(trans('quickadmin::admin.teacher-create-btncreate'), ['class' => 'btn btn-primary']) !!}
        </div>
    </div>

    {!! Form::close() !!}

@endsection


