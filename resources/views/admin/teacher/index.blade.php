@extends('admin.layouts.master')

@section('content')

    <p>{!! link_to_route(config('quickadmin.route').'.teacher.create', trans('quickadmin::admin.teacher-index-add_new'), [], ['class' => 'btn btn-success']) !!}</p>

    @if($teachers->count() > 0)
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ trans('quickadmin::admin.teacher-index-companies_list') }}</div>
            </div>
            <div class="portlet-body">
                <table id="datatable" class="table table-striped table-hover table-responsive datatable">
                    <thead>
                    <tr>
                        <th>{{ trans('quickadmin::admin.teacher-index-avatar') }}</th>
                        <th>{{ trans('quickadmin::admin.teacher-index-name') }}</th>
                        <th>{{ trans('quickadmin::admin.teacher-index-email') }}</th>
                        <th>{{ trans('quickadmin::admin.teacher-index-dob') }}</th>
                        <th>{{ trans('quickadmin::admin.teacher-index-educational') }}</th>
                        <th>{{ trans('quickadmin::admin.teacher-index-professional') }}</th>
                        <th>{{ trans('quickadmin::admin.teacher-index-phone_number') }}</th>
                        <th>{{ trans('quickadmin::admin.teacher-index-address') }}</th>
                        <th>{{ trans('quickadmin::admin.teacher-index-status') }}</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($teachers as $teacher)
                        <tr>
                            <td><img src="{!!\URL::To('public/uploads')!!}/{{ $teacher->avatar }}" width="50px" height="50px"></td>
                            <td>{{ $teacher->name }}</td>
                            <td>{{ $teacher->email }}</td>
                            <td>{{ $teacher->dob }}</td>
                            <td>{{ $teacher->educational }}</td>
                            <td>{{ isset($teacher->tb_professional->name) ? $teacher->tb_professional->name : '' }}</td>
                            <td>{{ $teacher->phone_number }}</td>
                            <td>{{ $teacher->address }}</td>
                            <td>@if($teacher->active==1) Active @else Inactive @endif</td>
                            <td>
                                <!-- {!! link_to_route(config('quickadmin.route').'.teacher.edit', trans('quickadmin::templates.templates-view_index-edit'), array($teacher->id), array('class' => 'btn btn-xs btn-info')) !!} -->

                                <a href="{!!\URL::To('/'.config('quickadmin.route'))!!}/teacher/{{ $teacher->id }}/edit" class="btn btn-xs btn-info" title="Click to edit teacher"><i class="fa fa-edit"></i></a>

                                {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => "return confirm('".trans("quickadmin::templates.templates-view_index-are_you_sure")."');",  'route' => array(config('quickadmin.route').'.teacher.destroy', $teacher->id))) !!}
                                <button type="submit" class="btn btn-xs btn-danger" title="Click to delete"><i class="fa fa-trash"></i> </button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @else
        {{ trans('quickadmin::admin.teacher-index-no_entries_found') }}
    @endif

@endsection