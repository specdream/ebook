@extends('admin.layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
        <h1>{{ trans('quickadmin::templates.templates-customView_index-list') }}</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

<!-- {!! Form::open(array('files' => true, 'route' => config('quickadmin.route').'.tb_courses.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!} -->
<form action="{{ url('admin/createSemester') }}" id="form-with-validation" class="form-horizontal" method="post" enctype="multipart/form-data" >
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="form-group">
    <label class="col-sm-2 control-label">Semester</label>
    <div class="col-sm-10 call-add-apnd">
    <div class="whole_input">
    <div class="input">
        <input type="text" class="form-control" name="Semester[]" value="" placeholder="Semester" required="">
        <p></p>
    </div>    
    <div class="input">
        <select name="start[]">
            <option value="1">January</option>
            <option value="2">February</option>
            <option value="3">March</option>
            <option value="4">April</option>
            <option value="5">May</option>
            <option value="6">June</option>
            <option value="7">July</option>
            <option value="8">August</option>
            <option value="9">September</option>
            <option value="10">October</option>
            <option value="11">November</option>
            <option value="12">December</option>
        </select>
        <p></p>
    </div>  
    <div class="input">
        <select name="end[]">
            <option value="1">January</option>
            <option value="2">February</option>
            <option value="3">March</option>
            <option value="4">April</option>
            <option value="5">May</option>
            <option value="6">June</option>
            <option value="7">July</option>
            <option value="8">August</option>
            <option value="9">September</option>
            <option value="10">October</option>
            <option value="11">November</option>
            <option value="12">December</option>
        </select>
        <p></p>
    </div>    
    </div>  
    </div>
    </div>
    </div>
</div>



<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
    <!-- <input class="btn btn-primary" value="Add More" id="add" type="button"> -->
      {!! Form::submit( trans('quickadmin::templates.templates-view_create-create') , array('class' => 'btn btn-primary')) !!}
    </div>
</div>

{!! Form::close() !!}
<script type="text/javascript">
 var int = '1';
    $('#add').click(function(){
        var html = '<div class="input inp_'+int+'"><input type="text" class="form-control" name="Semester[]" value="" placeholder="Semester" required=""><span id="form_input_clear" class="text_remove_'+int+'"><i class="fa fa-minus-circle" aria-hidden="true"></i></span></div>';

        $('.call-add-apnd').append(html);

        int++;
    })
    $(document).on('click','.fa',function(){
        
        $(this).closest('.input').hide();
        
    })        
</script>
@endsection