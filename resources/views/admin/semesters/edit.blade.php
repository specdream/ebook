@extends('admin.layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
        <h1>{{ trans('quickadmin::templates.templates-view_create-add_new') }}</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

<!-- {!! Form::open(array('files' => true, 'route' => config('quickadmin.route').'.tb_courses.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!} -->
<form action="{{ url('admin/editSemester').'/'.$semesters->id }}" id="form-with-validation" class="form-horizontal" method="post" enctype="multipart/form-data" >
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="form-group">
    <label class="col-sm-2 control-label">Semester</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="name" value="{{ $semesters->name }}" placeholder="English" required="">
        <div class="input">
        <select name="start">
            <option @if($semesters->start == '1') selected @endif value="1">January</option>
            <option @if($semesters->start == '2') selected @endif value="2">February</option>
            <option @if($semesters->start == '3') selected @endif value="3">March</option>
            <option @if($semesters->start == '4') selected @endif value="4">April</option>
            <option @if($semesters->start == '5') selected @endif value="5">May</option>
            <option @if($semesters->start == '6') selected @endif value="6">June</option>
            <option @if($semesters->start == '7') selected @endif value="7">July</option>
            <option @if($semesters->start == '8') selected @endif value="8">August</option>
            <option @if($semesters->start == '9') selected @endif value="9">September</option>
            <option @if($semesters->start == '10') selected @endif value="10">October</option>
            <option @if($semesters->start == '11') selected @endif value="11">November</option>
            <option @if($semesters->start == '12') selected @endif value="12">December</option>
        </select>
        <p></p>
    </div>  
    <div class="input">
        <select name="end">
            <option @if($semesters->end == '1') selected @endif value="1">January</option>
            <option @if($semesters->end == '2') selected @endif value="2">February</option>
            <option @if($semesters->end == '3') selected @endif value="3">March</option>
            <option @if($semesters->end == '4') selected @endif value="4">April</option>
            <option @if($semesters->end == '5') selected @endif value="5">May</option>
            <option @if($semesters->end == '6') selected @endif value="6">June</option>
            <option @if($semesters->end == '7') selected @endif value="7">July</option>
            <option @if($semesters->end == '8') selected @endif value="8">August</option>
            <option @if($semesters->end == '9') selected @endif value="9">September</option>
            <option @if($semesters->end == '10') selected @endif value="10">October</option>
            <option @if($semesters->end == '11') selected @endif value="11">November</option>
            <option @if($semesters->end == '12') selected @endif value="12">December</option>
        </select>
        <p></p>
    </div> 
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Status</label>
    <div class="col-sm-10">
        <select class="form-control" name="status">
            <option @if($semesters->status == '1') class="active" @endif value="1">Active</option>
            <option @if($semesters->status == '2') class="active" @endif value="2">De-Active</option>
        </select>
        
    </div>
</div>



<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
      <input class="btn btn-primary" value="Edit" name="submit" type="submit">
    </div>
</div>

{!! Form::close() !!}
@endsection