@include('admin.partials.header')
<div class="clearfix"></div>
<style type="text/css">
    .page-content,.page-container{margin: 0px!important;}
    .the-box {
        padding: 15px;
        margin-bottom: 30px;
        background: #fff;
        border: 1px solid #D5DAE0;
        position: relative;
    }
    .chat-wrap {
    height: 330px;
    overflow-y: auto;
    padding: 20px;
}
.img-circle {
    border-radius: 50%;
}
.media-chat .media-body {
    background: #F6F8F9;
        background-color: rgb(246, 248, 249);
    padding: 10px;
    border-radius: 8px;
}
.media, .media-body {
    overflow: hidden;
    zoom: 1;
}
.media-chat .media-body {
    background: #F6F8F9;
        background-color: rgb(246, 248, 249);
    padding: 10px;
    border-radius: 8px!important;
}
.action-chat {
    margin-top: 30px;
}
.fancybox-close {
    position: absolute;
    top: -18px;
    right: -18px;
    width: 36px;
    height: 36px;
    cursor: pointer;
    z-index: 8040;
}
.media-xs .media-object {
    width: 40px;
    height: 40px;
}

</style>
<div class="page-container">

    <div class="page-content-wrapper">
        <div class="page-content">

            <h3 class="page-title">
            @if(str_replace('Controller','',explode("@",class_basename(app('request')->route()->getAction()['controller']))[0])!="Company")

            <?php  $con1=str_replace("_"," ",preg_replace('/([a-z0-9])?([A-Z])/','$1 $2',str_replace('Controller','',explode("@",class_basename(app('request')->route()->getAction()['controller']))[0])));
                    $con2=str_replace("Tb","",$con1);
                    $con3=str_replace("sdream","",$con2);
             ?>
                {{ "Manage ".$con3 }}
                @else
                Universities
                @endif
            </h3>

            <div class="row">
                <div class="col-md-12">

                    @if (Session::has('message'))
                        <div class="note note-info">
                            <p>{{ Session::get('message') }}</p>
                        </div>
                    @endif

                    @yield('content')

                </div>
            </div>

        </div>
    </div>
</div>

<div class="scroll-to-top"
     style="display: none;">
    <i class="fa fa-arrow-up"></i>
</div>
@include('admin.partials.javascripts')

@yield('javascript')
@include('admin.partials.footer')


