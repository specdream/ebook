@extends('admin.layouts.master')

@section('content')

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">{{ trans('quickadmin::templates.templates-customView_index-list') }}</div>
        </div>
        <div class="portlet-body">
            <a href="{{ url('admin/collegeBookCreate') }}">
        <button class="btn btn-default create-btn">Create</button>
        </a>
            <table class="table table-striped table-hover table-responsive datatable" id="datatable tbl-book-create">
             <thead>
            <tr>
            	<th>S.No</th>
            	<th>Teacher</th>
            	<th>Title</th>
                <th>Semester</th>
                <th>Degree</th>
                <th>Price Type</th>
                <th>Price</th>
                <th>ISBN</th>
            	<th>Actions</th>
            </tr>
             </thead>
             <tbody>
            <?php $si = '1'; foreach($book as $bookVal){ 
                if($bookVal->price_type != 'Paid'){
                    $priceType = 'Free';
                }else{
                    $priceType = 'Paid';
                }
            ?> 
            <tr>
            	<td>{{ $si }}</td>
            	<td>{{ \Helper::getUser($bookVal->user_id,'name') }}</td>
            	<td>{{ $bookVal->title }}</td>
            	<td>{{ 'asd' }}</td>
            	<td>{{ 'as' }}</td>
                <td>{{ $priceType }}</td>
                <td>{{ $bookVal->price }}</td>
                <td>{{ $bookVal->isbn }}</td>
                <td><a href="{{ url('/admin/collegeBooks/edit').'/'.$bookVal->id }}" class="btn btn-xs btn-info" title="Click to edit univerisities"><i class="fa fa-edit"></i></a>
                <a href="{{ url('/collegeBooks/delete').'/'.$bookVal->id }}" class="btn btn-xs btn-danger" onclick="return confirm('Are you sure you want to delete this item?');" title="Click to edit univerisities"><i class="fa fa-trash"></i></a>    
                </td>
                </tr>
            <?php $si++; } ?>    
            </tbody>
            </table>
        </div>
	</div>

@endsection