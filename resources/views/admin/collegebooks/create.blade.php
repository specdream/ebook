@extends('admin.layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-12">
       <!--  <h3 class="book-page-title">{{ trans('quickadmin::templates.templates-view_create-add_new') }}</h3> -->

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

<!-- {!! Form::open(array('files' => true, 'route' => config('quickadmin.route').'.tb_courses.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!} -->
<form action="{{ url('admin/collegeBookStore') }}" id="form-with-validation" class="form-horizontal" method="post" enctype="multipart/form-data" >
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" id="fileType" name="fileType" value="" >
<input type="hidden" name="prof_id" value="@if(Auth::user()) Auth::user()->id @endif" >
<div class="book-cat-form col-xs-11 col-md-offset-1">
<div class="form-group">
   
    <div class="col-sm-10">
     {!! Form::label('Semester', 'Semester', array('class'=>'control-label')) !!}
        <?php $getSem = \DB::table('eb_semesters')->where('status','1')->get(); ?>
        <select name="semester" id="semester" class="form-control required">
        <option selected="selected" disabled="true">Select</option>
            @foreach($getSem as $getSemVal)
            <option value="{{ $getSemVal->id }}">{{ $getSemVal->name }}</option>
            @endforeach
        </select>
        <span id="summary_msg" style="color: #F00;"></span> 
    </div>
   
</div>
<div class="form-group sub-catDiv">
    
    <div class="col-sm-10">
        {!! Form::label('Degree', 'Degree', array('class'=>'control-label')) !!}
        <?php $getDegr = \DB::table('eb_degrees')->where('status','1')->get(); ?>
        <select name="degrees" id="degrees" class="form-control required">
        <option selected="selected" disabled="true">Select</option>
            @foreach($getDegr as $getDegrVal)
            <option value="{{ $getDegrVal->id }}">{{ $getDegrVal->name }}</option>
            @endforeach
        </select>
        <span id="summary_msg" style="color: #F00;"></span> 
    </div>
   
</div>
<div class="form-group">
    <div class="col-sm-10">
        {!! Form::label('teacher_id', 'Teacher Name*', array('class'=>'control-label')) !!}
        {!! Form::select('teacher_id', $tb_users, old('teacher_id'), array('class'=>'form-control','required'=>'')) !!}
        <span id="title_msg" style="color: #F00;"></span>
    </div>
</div>
<div class="form-group">
    
    <div class="col-sm-10">
    {!! Form::label('title', 'Title*', array('class'=>' control-label')) !!}
        {!! Form::text('title', old('title'), array('class'=>'form-control keyvalid','required'=>'')) !!}
         <span id="title_msg" style="color: #F00;"></span>
    </div>
   
</div>
<div class="form-group">
    
    <div class="col-sm-10">
    {!! Form::label('summary', 'Summary', array('class'=>' control-label')) !!}
        {!! Form::text('summary', old('summary'), array('class'=>'tinymce form-control keyvalid')) !!}
        <span id="summary_msg" style="color: #F00;"></span> 
    </div>
   
</div>
<div class="form-group">

    <div class="col-sm-10 Book-Ban">
       {!! Form::label('Book Banner', 'Book Banner*', array('class'=>'control-label book-banner')) !!}
       <input type="file" accept="image/*" name="banner" required="true">
       <span id="summary_msg" style="color: #F00;"></span> 
   </div>
   
</div>
<div class="form-group img-vid-sec">
  
  {!! Form::label('image', 'Source', array('class'=>' control-label price-section')) !!}  
 <div class="col-xs-10 col-sm-10 image-file-select">
  
    <div class="col-sm-3 col-xs-12">
<!--        <label class="img-file"><span class="fa fa-image"></span>Select your Image
        <input type="file" name="image" required="">     
       </label>   --> 
       <div class="btn btn-default btn-auth-img" onclick="getext(1)">Select Image</div>
 
    </div>

    <div class="col-sm-3 col-xs-12">
       <!-- <label class="img-file"><span class="fa fa-file-audio-o"></span>Select your Audio
        <input type="file" name="image" required="">     
       </label>    -->
       <div class="btn btn-default btn-auth-img" onclick="getext(2)" >Select Video</div>
    </div>

    <div class="col-sm-3 col-xs-12">
      <!--  <label class="img-file"><span class="fa fa-file-video-o"></span>Select your Video
        <input type="file" name="image" required="">     
       </label>   --> 
       <div class="btn btn-default btn-auth-img" onclick="getext(3)"" >Select Word-PDF </div>
    </div>

    <div class="col-sm-3 col-xs-12">
      <!--  <label class="img-file"><span class="fa fa-file-pdf-o"></span>Select your PDF
        <input type="file" name="image" required="">     
       </label>   --> 
       <div class="btn btn-default btn-auth-img" onclick="getext(4)"" >Select PowerPoint</div>
    </div>
    <div class="col-sm-12 no-pad">
 
      <div class="form-group inputDnD" style="display:none;">
           
        <label class="sr-only" for="inputFile">File Upload</label>
        <label class="file-up-book" style="display: none;">
        <input type="file" name="files[]" class="form-control-file font-weight-bold" id="inputFile" accept="image/*" onchange="readUrl(this)" multiple="true" data-title="Drag and drop a file">
        </label>
        
            <div class="col-sm-6 btn-margin-lef">
    <div class="col-sm-4">
 <div type="button" class="btn btn-primary btn-add-file up_btn" onclick="document.getElementById('inputFile').click()">Add Image</div>
 </div>
 <div class="col-sm-6">
<div class="btn btn-danger btn-drop-close">cancel</div>
</div>
  </div>
      </div>
    </div>

</div>
<div class="form-group">
    
    <div class="col-sm-10 no-pad dontprazremove" style="display: none;">
    <div class="col-sm-4">
    {!! Form::label('isbnCode', 'Book Pages*', array('class'=>' control-label')) !!}
        <input type="text" class="form-control" name="pages" value="" placeholder="Book Pages">
    </div>
    <div class="col-sm-4">
    {!! Form::label('isbnCode', 'Hours to read*', array('class'=>' control-label')) !!}
        <input type="text" class="form-control" name="Hours to read" value="" placeholder="Hours to read (E.g. 4 - 5)">
    </div>
    <div class="col-sm-4">
    {!! Form::label('isbnCode', 'Total words*', array('class'=>' control-label')) !!}
        <input type="text" class="form-control" name="Total words" value="" placeholder="Total words (E.g. 80 k)">
    </div>
    </div>
</div>



<div class="form-group" style="display: none;">
    {!! Form::label('promo_video', 'Promo Video', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::file('promo_video') !!}
        
    </div>

</div>
<div class="form-group">
   
    <div class="col-sm-10">
      {!! Form::label('price_type', 'Price Type*', array('class'=>' control-label')) !!}
              <select class="form-control" name="price_type" id="price_type">
              	<option value="Free">Free</option>
              	<option value="Paid">Paid</option>
              </select>  
    </div>
</div>

<div class="form-group price_hide" style="display: none;">
    <div class="col-sm-10">
    {!! Form::label('price', 'Price', array('class'=>' control-label price-section')) !!}
   
        {!! Form::text('price', old('price','0'), array('class'=>'form-control','onkeypress'=>"return isNumber(event)")) !!}
    </div>
    </div>

</div>
<!-- <div class="form-group">
     
    <div class="col-sm-10">
    {!! Form::label('Book Language', 'Book Language', array('class'=>'control-label')) !!}
        <?php $getCatg = \DB::table('tb_book_language')->where('status','1')->get(); ?>
        <select name="bLanguage" id="bLanguage" class="form-control required">
        <option selected="selected" disabled="true">Select</option>
            @foreach($getCatg as $getCatgVal)
            <option value="{{ $getCatgVal->id }}">{{ $getCatgVal->value }}</option>
            @endforeach
        </select>
        <span id="summary_msg" style="color: #F00;"></span> 
    </div>
   
</div> -->
<div class="form-group">
    
    <div class="col-sm-10">
    {!! Form::label('isbnCode', 'Book ISBN*', array('class'=>' control-label')) !!}
        <input type="text" class="form-control" name="ISBN" value="" required="">
        
    </div>
</div>

<div class="form-group">
    <div class="col-sm-10 ">
      {!! Form::submit( trans('quickadmin::templates.templates-view_create-create') , array('class' => 'btn btn-primary pull-right')) !!}
    </div>
</div>


{!! Form::close() !!}
<style type="text/css">.

    .img-file{
    padding: 10px;
    background: #fff; 
    display: table;
    color: #000;
    border-radius:none;
    box-shadow: 1px 1px 8px 4px rgba(209,209,209,1);
    padding:10px 10px;
    cursor:pointer;
     }

.img-file input[type="file"] {
    display: none;
}
/*.btn-drop-close{width: 150px;}
.btn-add-file{width: 150px;}*/
.file-up-book{border: 3px dashed #8c8888;width: 100%;float: left;}
.btn-margin-lef{margin-top:15px;}
.img-file span.fa{font-size:15px; margin-right:5px;color:#428bca;}
.img-file:hover{transform: translateY(-1px) scale(1.04);}
 .font-weight-bold{opacity: 0!important;}
.page-sidebar-menu > li :hover{transform: translateX(-3px);}

.inputDnD .form-control-file {position: relative;width: 100%;outline: none;visibility: hidden;cursor: pointer;background-color: #25313f;box-shadow: 0 0 5px solid currentColor;
    color:#4c4b4b;height: 130px;;
}
.inputDnD .form-control-file:before {content: attr(data-title);position: absolute;top: 0.5em;left: 0;width: 100%;min-height: 6em;line-height: 2em;padding-top: 1.5em;opacity: 1;visibility: visible;text-align: center;border: 0.25em dashed currentColor;transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);overflow: hidden;height: 130px;}

.inputDnD .form-control-file {position: relative;width: 100%;outline: none;visibility: hidden;cursor: pointer;box-shadow: 0 0 5px solid currentColor;
    color:#4c4b4b;height: 130px;min-height: 6em;line-height: 2em;padding-top: 1.5em;opacity: 1;visibility: visible;text-align: center;border: 0.25em dashed currentColor;transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);overflow: hidden;

}
.img-vid-sec>label{float:left;padding-left: 15px;}
/*.inputDnD .form-control-file:before {content: attr(data-title);position: absolute;top: 0.5em;left: 0;width: 100%;min-height: 6em;line-height: 2em;padding-top: 1.5em;opacity: 1;visibility: visible;text-align: center;border: 0.25em dashed currentColor;transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);overflow: hidden;height: 130px;
}*/
/*.inputDnD .form-control-file:hover:before {border-style: solid;box-shadow: inset 0px 0px 0px 0.25em currentColor;
}*/
.image-file-select{margin-bottom:10px;}
</style>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">

    $(document).on('change','#price_type',function(){
    	if($(this).val()=='Paid'){
            $(".price_hide").show();
        }else{
            $(".price_hide").hide();
            $("#price").val(0);
        }
    })
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    
    $(document).ready(function(){
    $(".btn-auth-img").click(function(){
        $(".btn-auth-img").hide();
    });
    $(".btn-auth-img").click(function(){
        $(".inputDnD").show();
    });
    $(".btn-drop-close").click(function(){
        $(".btn-auth-img").show();
    });
     $(".btn-drop-close").click(function(){
        $('#inputFile').val('');
        $('#inputFile').attr('data-title', 'Drag and drop a file')
        $(".inputDnD").hide();
    });
  
});
    function readUrl(input) {
  
  if (input.files && input.files[0]) {
    let reader = new FileReader();
    reader.onload = (e) => {
      let imgData = e.target.result;
      let imgName = input.files[0].name;
      input.setAttribute("data-title", imgName);
      console.log(e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }

}

function getext(id) {
  $('#fileType').val(id);
  if(id == '1'){
    $('#inputFile').attr('accept','image/*');
    $('.up_btn').html('Add Image');
  }else if(id == '2'){
    $('#inputFile').attr('accept','video/*');
    $('.up_btn').html('Add Video');
  }else if(id == '3'){
    $('#inputFile').attr('accept','application/pdf');
    $('.up_btn').html('Add PDF');
  }else if(id == '4'){
    $('#inputFile').attr('accept','application/vnd.ms-powerpoint');
    $('.up_btn').html('Add PowerPoint');
  }
}
</script>
<script>
  $(document).ready(function(){
    $('#tags').tagInput();
    $('#tags2').tagInput({labelClass:"label-warning"});
  });
  </script>
@endsection