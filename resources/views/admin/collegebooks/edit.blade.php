@extends('admin.layouts.master')

@section('content')

<div class="row">
  <div class="col-sm-12">
    

    @if ($errors->any())
    <div class="alert alert-danger">
     <ul>
      {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
    </ul>
  </div>
  @endif
</div>
</div>

<div id="progress-div"><div id="progress-bar"></div></div>




<!-- {!! Form::open(array('files' => true, 'route' => config('quickadmin.route').'.tb_courses.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!} -->
<form action="{{ url('admin/collegeBooks/edit').'/'.$book->id }}" id="form-with-validation" class="form-horizontal" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" id="fileType" name="fileType" value="" >
  <input type="hidden" id="book_id" name="book_id" value="{{ $book->id }}" >
  <input type="hidden" name="prof_id" value="@if(Auth::user()) Auth::user()->id @endif" >
  <div class="book-cat-form col-xs-11 col-md-offset-1">
    <div class="form-group">
   
    <div class="col-sm-10">
     {!! Form::label('Semester', 'Semester', array('class'=>'control-label')) !!}
        <?php $getSem = \DB::table('eb_semesters')->where('status','1')->get(); ?>
        <select name="semester" id="semester" class="form-control required">
        <option selected="selected" disabled="true">Select</option>
            @foreach($getSem as $getSemVal)
            <option @if($getSemVal->id == $book->semester) selected="selected" @endif value="{{ $getSemVal->id }}">{{ $getSemVal->name }}</option>
            @endforeach
        </select>
        <span id="summary_msg" style="color: #F00;"></span> 
    </div>
   
</div>
<div class="form-group sub-catDiv">
    
    <div class="col-sm-10">
        {!! Form::label('Degree', 'Degree', array('class'=>'control-label')) !!}
        <?php $getDegr = \DB::table('eb_degrees')->where('status','1')->get(); ?>
        <select name="degrees" id="degrees" class="form-control required">
        <option selected="selected" disabled="true">Select</option>
            @foreach($getDegr as $getDegrVal)
            <option @if($getDegrVal->id == $book->degree) selected="selected" @endif value="{{ $getDegrVal->id }}">{{ $getDegrVal->name }}</option>
            @endforeach
        </select>
        <span id="summary_msg" style="color: #F00;"></span> 
    </div>
   
</div>
<div class="form-group">
    <div class="col-sm-10">
        {!! Form::label('teacher_id', 'Teacher Name*', array('class'=>'col-sm-2 control-label')) !!}
        {!! Form::select('teacher_id', $tb_users, old('teacher_id',$book->user_id), array('class'=>'form-control','required'=>'')) !!}
        <span id="title_msg" style="color: #F00;"></span>
    </div>
</div>
    <div class="form-group">
      
      <div class="col-sm-10">
      {!! Form::label('Status', 'Status', array('class'=>' control-label')) !!}
        <select class="form-control status" name="status">
          <option @if($book->status == '1') selected="selected" @endif value="1">Active</option>
          <option @if($book->status == '0') selected="selected" @endif value="0">De-Active</option>
        </select>
      </div>

    </div>
    <div class="form-group">
     
      <div class="col-sm-10">
       {!! Form::label('title', 'Title*', array('class'=>' control-label')) !!}
        {!! Form::text('title', old('title',$book->title), array('class'=>'form-control keyvalid','required'=>'')) !!}
        <span id="title_msg" style="color: #F00;"></span>
      </div>

    </div>
    <div class="form-group">
      
      <div class="col-sm-10">
      {!! Form::label('summary', 'Summary', array('class'=>' control-label')) !!}
        {!! Form::text('summary', old('summary',$book->summary), array('class'=>'tinymce form-control keyvalid')) !!}
        <span id="summary_msg" style="color: #F00;"></span> 
      </div>

    </div>

    <div class="form-group">
      
      <div class="col-sm-10">
      {!! Form::label('Book Banner', 'Book Banner*', array('class'=>' control-label book-banner')) !!}
        <div>
      
        <img class="banner_place" src="{{ url('public/uploads/college_books_banner').'/'.$book->id.'/'.$book->banner }}">
        <div style="width:100%;float: left;margin: 15px 0px;">
        <input type="file" accept="image/*" class="banner_img" name="banner">
        </div>
        <span id="summary_msg" style="color: #F00;"></span> 
      </div>
      </div>
    </div>


    <div class="form-group">
      <div class="col-xs-10 col-sm-10 image-file-select">
      {!! Form::label('image', 'Source', array('class'=>' control-label')) !!}
        <div class="source_files">
          

          <div class="book_edit_image">
            <div class="row"> 
            @if(count($bookFile) > 0)
              <?php $msi = '0'; $mri = '0';
              for($mi=0;$mi<count($bookFile);$mi++){
                if($mi%4 == 0){
                  $mri = count($bookFile)-$mi;
                  if(4 > $mri){
                    if($mi != '0')
                      $msi = $mri+3;   
                    else
                      $msi = $mri-1;       
                  }else{
                    $msi = $mi+3;
                  }

                  echo "<input type='hidden' id='totalFile' value='".count($bookFile)."'><div class='column'>";
                } ?>
                @if($bookFile[0]->type == '1') 

                <div class="pre_image_div col-xs-12 col-sm-3 col-md-3 ">

                <a href="javascript:void(0)" onclick="deletefile(<?php echo $bookFile[$mi]->id; ?>,1)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                <img src="{{ url('public/uploads/college_books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file }}">
                <script type="text/javascript">
                $(document).ready(function(){
                  $('#inputFile').attr('accept','image/*');
                  $('.up_btn').html('Add Image');
                  })
                </script>
                </div>

                @elseif($bookFile[0]->type == '2')
                <div class="pre_image_div col-xs-12 col-sm-3 col-md-3 ">
                <a href="javascript:void(0)" onclick="deletefile(<?php echo $bookFile[$mi]->id; ?>,1)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                <video src="{{ url('public/uploads/college_books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file }}"></video>
                <script type="text/javascript">
                $(document).ready(function(){
                  $('#inputFile').attr('accept','video/*');
                  $('.up_btn').html('Add Video');
                  })

                </script>
                </div>
                @elseif($bookFile[0]->type == '3')
                 <div class="pre_image_div col-xs-12 col-sm-3 col-md-3 ">
                <a href="javascript:void(0)" onclick="deletefile(<?php echo $bookFile[$mi]->id; ?>,1)"><i class="fa fa-trash trash-pdf" aria-hidden="true"></i></a>
                <object data="{{ url('public/uploads/college_books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file }}" type="application/pdf" width="100%" height="100%">
                  <p>Alternative text - include a link <a href="{{ url('public/uploads/college_books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file }}">to the PDF!</a></p>
                </object>
                <script type="text/javascript">
                $(document).ready(function(){
                  $('#inputFile').attr('accept','application/pdf');
                  $('.up_btn').html('Add PDF');
                  })
                </script>
                </div>
                @elseif($bookFile[0]->type == '4')
                <div class="pre_image_div col-xs-12 col-sm-3 col-md-3 ">
                <a href="javascript:void(0)" onclick="deletefile(<?php echo $bookFile[$mi]->id; ?>,1)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                <object data="{{ url('public/uploads/college_books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file }}" type="application/vnd.ms-powerpoint" width="100%" height="100%">
                  <p>Alternative text - include a link <a href="{{ url('public/uploads/college_books').'/'.$bookFile[$mi]->book_id.'/'.$bookFile[$mi]->file }}">to the PDF!</a></p>
                </object>
                <script type="text/javascript">
                $(document).ready(function(){
                  $('#inputFile').attr('accept','application/vnd.ms-powerpoint');
                  $('.up_btn').html('Add PowerPoint');
                  })
                </script>
                </div>
                @endif
                <?php if($mi == $msi){
                  echo "</div>";    
                }
              }
              ?>
               @endif
            </div>
          </div>
         
          <div class="source_section" >


           <div class="col-xs-10 col-sm-10 image-file-select">
            <div class="col-sm-3 col-xs-12">
<!--        <label class="img-file"><span class="fa fa-image"></span>Select your Image
        <input type="file" name="image" required="">     
      </label>   --> 
      <div class="btn btn-default btn-auth-img" onclick="getext(1)">Select Image</div>

    </div>

    <div class="col-sm-3 col-xs-12">
       <!-- <label class="img-file"><span class="fa fa-file-audio-o"></span>Select your Audio
        <input type="file" name="image" required="">     
      </label>    -->
      <div class="btn btn-default btn-auth-img" onclick="getext(2)" >Select Video</div>
    </div>

    <div class="col-sm-3 col-xs-12">
      <!--  <label class="img-file"><span class="fa fa-file-video-o"></span>Select your Video
        <input type="file" name="image" required="">     
      </label>   --> 
      <div class="btn btn-default btn-auth-img" onclick="getext(3)"" >Select Word-PDF </div>
    </div>

    <div class="col-sm-3 col-xs-12">
      <!--  <label class="img-file"><span class="fa fa-file-pdf-o"></span>Select your PDF
        <input type="file" name="image" required="">     
      </label>   --> 
      <div class="btn btn-default btn-auth-img" onclick="getext(4)"" >Select PowerPoint</div>
    </div>
  </div>
</div>

</div>
</div>
<div class="col-sm-12 no-pad">

  <div class="form-group inputDnD" >

    <label class="sr-only" for="inputFile">File Upload</label>
    <label class="file-up-book" style="display: none;">
      <input type="file" name="files[]" class="form-control-file font-weight-bold" id="inputFile" accept="image/*" onchange="readUrl(this)" multiple="true" data-title="Drag and drop a file">
    </label>

    <div class="col-sm-6 btn-margin-lef">
      <div class="col-sm-3">
       <div type="button" class="btn btn-primary btn-add-file up_btn" onclick="document.getElementById('inputFile').click()">Add Image</div>
     </div>
     <div class="col-sm-3 canc_div" style="display:none;">
      <div class="btn btn-danger btn-drop-close">cancel</div>
    </div>
  </div>
</div>
</div>

</div>
<!-- 
<div class="form-group">
    <div class="abt-book">
    <div class="col-sm-10 no-pad">
    {!! Form::label('isbnCode', 'About the book', array('class'=>' control-label price-section')) !!}
    <div class="col-xs-12 col-sm-4">
      <div class="stat-icon">
        <img src="{!! asset('sdream/img/book.png') !!}">
      </div>
      <div class="stat-desc">
       <input type="text" class="form-control" name="Hours to read" value="" required="" placeholder="Hours to read (E.g. 4 - 5)">
        <span>Pages</span>
      </div>
    </div>
    <div class="col-xs-12 col-sm-4">
     <div class="stat-icon">
       <img src="{!! asset('sdream/img/time.png') !!}">
     </div>
     <div class="stat-desc">
       <input type="text" class="form-control" name="Total words" value="" required="" placeholder="Total words (E.g. 80 k)">
      <span>Hours to read</span>
    </div>
  </div>
  <div class="col-xs-12 col-sm-4">
   <div class="stat-icon">
     <img src="{!! asset('sdream/img/open-book.png') !!}">
   </div>
   <div class="stat-desc">
     <input type="text" class="form-control" name="pages" value="" required="" placeholder="Book Pages">
    <span>Total words</span>
  </div>
</div>
</div>
</div>
</div>

<div class="form-group">
    <div class="col-sm-10">
    
{!! Form::label('tags', 'book Tag*', array('class'=>' control-label')) !!}
      <div class="form-control tags" id="tags">
        <input type="text" class="labelinput">
        <input type="hidden" value="" name="result">
      </div>
</div> -->
<div class="form-group" style="display: none;">
 
  <div class="col-sm-10">
   {!! Form::label('promo_video', 'Promo Video', array('class'=>'control-label')) !!}
    {!! Form::file('promo_video') !!}

  </div>

</div>

<div class="form-group">

<div class="col-sm-10">
{!! Form::label('price_type', 'Price Type*', array('class'=>' control-label')) !!}
  <select class="form-control" name="price_type" id="price_type">
   <option @if($book->price_type == 'Free') selected="selected" @endif value="Free">Free</option>
   <option @if($book->price_type == 'Paid') selected="selected" @endif value="Paid">Paid</option>
 </select>  
</div>
</div>

<div class="form-group price_hide" @if($book->price_type == 'Free') style="display: none;" @endif>
 
  <div class="col-sm-10">
   {!! Form::label('price', 'Price', array('class'=>'col-sm-2 control-label')) !!}
    {!! Form::text('price', old('price',$book->price), array('class'=>'form-control','onkeypress'=>"return isNumber(event)")) !!}
  </div>

</div>

<div class="form-group">
  <div class="col-sm-10">
    {!! Form::label('isbnCode', 'Book ISBN*', array('class'=>' control-label')) !!}

    <input type="text" class="form-control" name="ISBN" value="{{ $book->isbn }}" required="">

  </div>
</div>

<div class="form-group">
  <div class="col-sm-10">
    <input class="btn btn-primary pull-right" value="Create" name="submit" type="submit">
  </div>
</div>
</div>

{!! Form::close() !!}
<style type="text/css">.

  .img-file{
    padding: 10px;
    background: #fff; 
    display: table;
    color: #000;
    border-radius:none;
    box-shadow: 1px 1px 8px 4px rgba(209,209,209,1);
    padding:10px 10px;
    cursor:pointer;
  }

  .img-file input[type="file"] {
    display: none;
  }
/*.btn-drop-close{width: 150px;}
.btn-add-file{width: 150px;}*/
.file-up-book{border: 3px dashed #8c8888;width: 100%;float: left;}
.btn-margin-lef{margin-top:15px; float:left;padding-left:0;}
.img-file span.fa{font-size:15px; margin-right:5px;color:#428bca;}
.img-file:hover{transform: translateY(-1px) scale(1.04);}
.font-weight-bold{opacity: 0!important;}
.page-sidebar-menu > li :hover{transform: translateX(-3px);}

.inputDnD .form-control-file {position: relative;width: 100%;outline: none;visibility: hidden;cursor: pointer;background-color: #25313f;box-shadow: 0 0 5px solid currentColor;
  color:#4c4b4b;height: 130px;;
}
.inputDnD .form-control-file:before {content: attr(data-title);position: absolute;top: 0.5em;left: 0;width: 100%;min-height: 6em;line-height: 2em;padding-top: 1.5em;opacity: 1;visibility: visible;text-align: center;border: 0.25em dashed currentColor;transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);overflow: hidden;height: 130px;}

  .inputDnD .form-control-file{position: relative;width: 100%;outline: none;visibility: hidden;cursor: pointer;box-shadow: 0 0 5px solid currentColor;
    color:#4c4b4b;height: 130px;min-height: 6em;line-height: 2em;padding-top: 1.5em;opacity: 1;visibility: visible;text-align: center;border: 0.25em dashed currentColor;transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);overflow: hidden;}
/*.inputDnD .form-control-file:before {content: attr(data-title);position: absolute;top: 0.5em;left: 0;width: 100%;min-height: 6em;line-height: 2em;padding-top: 1.5em;opacity: 1;visibility: visible;text-align: center;border: 0.25em dashed currentColor;transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);overflow: hidden;height: 130px;
}*/
/*.inputDnD .form-control-file:hover:before {border-style: solid;box-shadow: inset 0px 0px 0px 0.25em currentColor;
}*/
.image-file-select{margin-bottom:10px;}
</style>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

@include('script.authCollegeBookEdit')
@endsection