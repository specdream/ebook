@extends('admin.layouts.master')

@section('content')
	<p><a href="{{ url('admin/createBookSubCatg') }}" class="btn btn-success">Add new</a></p>
    <p><a href="#" class="btn btn-success import">Import CSV</a></p>
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">{{ trans('quickadmin::templates.templates-customView_index-list') }}</div>
        </div>

        <div class="portlet-body">
            <table class="table table-striped table-hover table-responsive datatable" id="datatable">
            <thead>
            <tr>
            	<th>S.No</th>
            	<th>Categorie</th>
                <th>Name</th>
            	<th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>
            <?php $i='1'; foreach ($getCatg as $key => $categorie) { ?>
            <tr>
              <tbody>
            	<td>{{ $i }}</td>
                <td>{{ Helper::categoryName($categorie->cat_id) }}</td>
            	<td>{{ $categorie->name }}</td>
            	<td>@if($categorie->status == '1') Active @else De-Active @endif</td>
            	<td>
            	<a href="{{ url('admin/editBookSubCatg').'/'.$categorie->id }}" class="btn btn-xs btn-info" title="Click to edit language"><i class="fa fa-edit"></i></a>
            	<a href="{{ url('admin/deleteBookSubCatg').'/'.$categorie->id }}" class="btn btn-xs btn-danger" onclick="return confirm('Are you sure you want to delete this item?');" title="Click to delete language"><i class="fa fa-trash"></i></a>
				</td>
               </tbody>
            </tr> 
            <?php $i++; } ?>
            	
            </table>
        </div>
	</div>

@endsection