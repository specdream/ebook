@extends('admin.layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
        <h1>{{ trans('quickadmin::templates.templates-view_create-add_new') }}</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

<!-- {!! Form::open(array('files' => true, 'route' => config('quickadmin.route').'.tb_courses.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!} -->
<form action="{{ url('admin/editBookSubCatg').'/'.$catg->id }}" id="form-with-validation" class="form-horizontal" method="post" enctype="multipart/form-data" >
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="form-group">
    <label class="col-sm-2 control-label">Categorie</label>
    <div class="col-sm-10">
       <?php $getCat = \DB::table('tb_book_categorie')->get(); ?>
        <select class="form-control" name="main">
        @foreach($getCat as $getCatVal)
            <option @if($catg->cat_id == $getCatVal->id) selected='selected' @endif value="{{ $getCatVal->id }}">{{ $getCatVal->name }}</option>
        @endforeach    
        </select>
        
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">SubCategorie</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="name" value="{{ $catg->name }}" placeholder="English" required="">
        
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Status</label>
    <div class="col-sm-10">
        <select class="form-control" name="status">
            <option value="1" @if($catg->status == '1') selected="selected" @endif >Active</option>
            <option value="2" @if($catg->status == '2') selected="selected" @endif >De-Active</option>
        </select>
        
    </div>
</div>



<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
      <input class="btn btn-primary" value="Create" name="submit" type="submit">
    </div>
</div>

{!! Form::close() !!}
@endsection