@extends('admin.layouts.master')

@section('content')

  <p> <a href="{!!\URL::To('/'.config('quickadmin.route'))!!}/tb_course_assign/create" class="btn btn-success">Add new</a></p>

@if ($tb_course_assign->count())
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">{{ trans('quickadmin::templates.templates-view_index-list') }}</div>
    </div>
    <?php //echo"<pre>"; print_r($tb_course_assign[0]->tb_courses);exit; ?>
    <div class="portlet-body">
        <table class="table table-striped table-hover table-responsive datatable" id="datatable">
            <thead>
                <tr>
                    <th>
                        {!! Form::checkbox('delete_all',1,false,['class' => 'mass']) !!}
                    </th>
                    <th>University Name</th>
                    <th>Course Title</th>
                    <th>Student Count</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($tb_course_assign as $row)
                <tr>
                    <td>
                        {!! Form::checkbox('del-'.$row->id,1,false,['class' => 'single','data-id'=> $row->id]) !!}
                    </td>
                    <td>{{ isset($row->users->name) ? $row->users->name : '' }}</td>
                    <td>{!!keyValue($row->tb_courses)!!}</td>
                    <td>{{ $row->s_limit }}</td>
                    <td>{{ date('d-m-Y',strtotime($row->s_date)) }}</td>
                    <td>{{ date('d-m-Y',strtotime($row->e_date)) }}</td>
                    <td>
                      <a href="{!!\URL::To('/'.config('quickadmin.route'))!!}/tb_course_assign/{{ $row->id }}/edit" class="btn btn-xs btn-info" title="Click to edit state"><i class="fa fa-edit"></i></a>
                      <form method="POST" action="{!!\URL::To('/'.config('quickadmin.route'))!!}/tb_course_assign/destroy/{!!$row->id!!}" accept-charset="UTF-8" style="display: inline-block;" onsubmit="return confirm('Are you sure?');">
                      	{{ csrf_field() }}
                      	<button type="submit" class="btn btn-xs btn-danger" title="Click to delete lession"><i class="fa fa-trash"></i> </button>
                      </form> 
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-xs-12">
                <button class="btn btn-danger" id="delete">
                    {{ trans('quickadmin::templates.templates-view_index-delete_checked') }}
                </button>
            </div>
        </div>
        {!! Form::open(['route' => config('quickadmin.route').'.sdream_states.massDelete', 'method' => 'post', 'id' => 'massDelete']) !!}
        <input type="hidden" id="send" name="toDelete">
        {!! Form::close() !!}
    </div>
</div>
@else
{{ trans('quickadmin::templates.templates-view_index-no_entries_found') }}
@endif

@endsection

@section('javascript')
<script>
    $(document).ready(function () {
        $('#delete').click(function () {
            if (window.confirm('{{ trans('quickadmin::templates.templates-view_index-are_you_sure') }}')) {
                var send = $('#send');
                var mass = $('.mass').is(":checked");
                if (mass == true) {
                    send.val('mass');
                } else {
                    var toDelete = [];
                    $('.single').each(function () {
                        if ($(this).is(":checked")) {
                            toDelete.push($(this).data('id'));
                        }
                    });
                    send.val(JSON.stringify(toDelete));
                }
                $('#massDelete').submit();
            }
        });
    });
</script>
@stop