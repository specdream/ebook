@extends('admin.layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
        <h1>Edit Assgined course</h1>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
            </div>
        @endif
    </div>
</div>
<?php
function keygen($length=10)
{
    $key = '';
    list($usec, $sec) = explode(' ', microtime());
    mt_srand((float) $sec + ((float) $usec * 100000));    
    $inputs = array_merge(range('z','a'),range(0,9),range('A','Z'));
    for($i=0; $i<$length; $i++)
    {
        $key .= $inputs{mt_rand(0,61)};
    }
    return $key;
}
?>
<?php //print_r($tb_course_assign); ?>
<form method="POST" action="{!!URL::To('/')!!}/admin/tb_course_assign/update/{!!$tb_course_assign->id!!}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
{{ csrf_field() }}
<input type="hidden" name="c_key" value="{!!keygen(10)!!}">
<div class="form-group">
    {!! Form::label('univ_id', 'University Name*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        <!-- {!! Form::select('univ_id', $Users, old('univ_id'), array('class'=>'form-control')) !!} -->
        <select name="univ_id" class="form-control" required="">
        <option value="" selected="">Please select</option>
            @foreach($Users as $User)
            <option @if($User->id==$tb_course_assign->univ_id) selected @endif value="{!!$User->id!!}">{!!$User->name!!}</option>
            @endforeach
        </select>
        
    </div>
</div>
<div class="form-group">
    {!! Form::label('c_id', 'Course Name*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        <!-- {!! Form::select('c_id', $Tb_Courses, old('c_id'), array('class'=>'form-control')) !!} -->
        <select name="c_id" class="form-control" required="">
        <option value="" selected="">Please select</option>
            @foreach($Tb_Courses as $Tb_Courses1)
            <option @if($Tb_Courses1->id==$tb_course_assign->c_id) selected @endif value="{!!$Tb_Courses1->id!!}">{!!$Tb_Courses1->tb_languages_settings['value']!!}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    {!! Form::label('s_limit', 'Student Count*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('s_limit', old('s_limit',$tb_course_assign->s_limit), array('class'=>'form-control','required'=>'')) !!}
        
    </div>
</div>
<div class="form-group">
    {!! Form::label('s_date', 'Start Date*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('s_date', old('s_date',$tb_course_assign->s_date), array('class'=>'form-control datepicker','required'=>'')) !!}
        
    </div>
</div>
<div class="form-group">
    {!! Form::label('e_date', 'End Date*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('e_date', old('e_date',$tb_course_assign->e_date), array('class'=>'form-control datepicker','required'=>'')) !!}
        
    </div>
</div>
<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
      {!! Form::submit( trans('quickadmin::templates.templates-view_create-create') , array('class' => 'btn btn-primary')) !!}
    </div>
</div>

</form>

@endsection