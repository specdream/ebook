@extends('admin.layouts.master')

@section('content')

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">{{ trans('quickadmin::templates.templates-customView_index-list') }}</div>
        </div>
        <div class="portlet-body">
          <table class="table table-striped table-hover table-responsive datatable" id="datatable">
            <thead>
              <tr>
               <th>S.No</th>
               <th>Title</th>
               <th>User Name</th>
               <th>Author</th>
               <th>Date of Purchase</th>
               <th>Actions </th>
             </tr>
           </thead>
           <tbody>
             <?php $i='1'; foreach ($historyData as $value) { ?>
             <tr>
              <td>{{ $i }}</td>		
              <td>{{ $value['title'] }}</td>
              <td>{{ $value['username'] }}</td>
              <td>{{ $value['author'] }}</td>
              <td>{{ $value['created'] }}</td>
              <td><a data-toggle="modal" data-target="#myModaledit" class="btn btn-default popDiv" uniqID="{{ $value['uniq_id'] }}"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
              </tr>
            <?php $i++; } ?>   
          </tbody>
        </table>
        </div>
	</div>

  <div class="modal fade" id="myModaledit" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
	</div>

<style type="text/css">
.table-purchase{}
.panel-history .panel-footer .pagination{margin:0; }
.panel-history .panel-footer .col{line-height: 34px;height: 34px;}	
.panel-history .panel-title{margin-top:5px;}
</style>
<script type="text/javascript">
$('.popDiv').click(function(){
  var uniq_id = $(this).attr('uniqID');
  $.ajax({
  url:'<?php echo url('invoicePopupAdmin').'/' ?>'+uniq_id,
  type:'GET',
  success:function(data){
    $('#myModaledit .modal-body').html(data);
  }
})
})



</script>
@endsection