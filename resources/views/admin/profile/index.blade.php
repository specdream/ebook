@extends('admin.layouts.master')

@section('content')
    
    <div class="col-sm-12">
    	<div class="col-sm-6">
    	<div class="row">
    		<div class="col-sm-10 col-sm-offset-2">
    			<h1>{{ trans('quickadmin::admin.profile-edit-create_user') }}</h1>

    			@if ($errors->any())
    			<div class="alert alert-danger">
    				<ul>
    					{!! implode('', $errors->all('
    					<li class="error">:message</li>
    					')) !!}
    				</ul>
    			</div>
    			@endif
    		</div>
    	</div>

    		{!! Form::model($profile, array('files' => true, 'class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'PATCH', 'route' => array(config('quickadmin.route').'.profile.update', $profile->id))) !!}

    		<div class="form-group">
    			{!! Form::label('username', trans('quickadmin::admin.profile-edit-username'), ['class'=>'col-sm-2 control-label']) !!}
    			<div class="col-sm-10">
    				{!! Form::text('username', old('username',$profile->username), ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.profile-edit-name_placeholder'),'readonly'=>'']) !!}
    			</div>
    		</div>
    		<div class="form-group">
    			{!! Form::label('name', trans('quickadmin::admin.profile-edit-name'), ['class'=>'col-sm-2 control-label']) !!}
    			<div class="col-sm-10">
    				{!! Form::text('name', old('name',$profile->name), ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.profile-edit-name_placeholder')]) !!}
    			</div>
    		</div>

    		<div class="form-group">
    			{!! Form::label('email', trans('quickadmin::admin.profile-edit-email'), ['class'=>'col-sm-2 control-label']) !!}
    			<div class="col-sm-10">
    				{!! Form::email('email', old('email',$profile->email), ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.profile-edit-email_placeholder')]) !!}
    			</div>
    		</div>

    		<div class="form-group">
    			{!! Form::label('avatar', trans('quickadmin::admin.profile-edit-image'), array('class'=>'col-sm-2 control-label')) !!}
    			<div class="col-sm-10">
    				{!! Form::file('avatar') !!}

    			</div>
    		</div>
            <div class="form-group">
                <label class="col-sm-2"></label>
                <div class="col-sm-10">
                    <img src="{!!\URL::To('public/uploads')!!}/{{ $profile->avatar }}" width="50px" height="50px">
                </div>
            </div>

    		<div class="form-group">
    			<div class="col-sm-10 col-sm-offset-2">
    				{!! Form::submit(trans('quickadmin::admin.profile-edit-btnedit'), ['class' => 'btn btn-primary']) !!}
    			</div>
    		</div>

    		{!! Form::close() !!}
    	</div>
    	<div class="col-sm-6">
    		<div class="row">
    			<div class="col-sm-10 col-sm-offset-2">
    			<h1>{{ trans('quickadmin::admin.profile-edit-change-pwd') }}</h1>

    			</div>
    		</div>
            <form method="POST" action="{!!URL::To('/')!!}/profile/updatepwd/1" accept-charset="UTF-8" class="form-horizontal" id="form-with-validation" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input name="_method" value="POST" type="hidden">
    		<!-- {!! Form::model($profile, array('files' => true, 'class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'POST', 'route' => array(config('quickadmin.route').'.profile.update', $profile->id))) !!} -->
    		{!! Form::hidden('username', old('username',$profile->username), ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.profile-edit-name_placeholder'),'readonly'=>'']) !!}
    		<div class="form-group">
    			{!! Form::label('oldpwd', trans('quickadmin::admin.profile-edit-old-pwd'), ['class'=>'col-sm-2 control-label']) !!}
    			<div class="col-sm-10">
    				{!! Form::password('oldpwd', ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.profile-edit-old-pwd')]) !!}
    			</div>
    		</div>
    		<div class="form-group">
    			{!! Form::label('newpwd', trans('quickadmin::admin.profile-edit-new-pwd'), ['class'=>'col-sm-2 control-label']) !!}
    			<div class="col-sm-10">
    				{!! Form::password('newpwd', ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.profile-edit-new-pwd')]) !!}
    			</div>
    		</div>

    		<div class="form-group">
    			{!! Form::label('password', trans('quickadmin::admin.profile-edit-confirm-pwd'), ['class'=>'col-sm-2 control-label']) !!}
    			<div class="col-sm-10">
    				{!! Form::password('password', ['class'=>'form-control check_pwd', 'placeholder'=> trans('quickadmin::admin.profile-edit-confirm-pwd')]) !!}
    			</div>
    			<span id="err" style="color: #F00;"></span>
    		</div>

    		<div class="form-group">
    			<div class="col-sm-10 col-sm-offset-2">
    				{!! Form::submit(trans('quickadmin::admin.profile-edit-btnedit'), ['class' => 'btn btn-primary check','disabled'=>'disabled']) !!}
    			</div>
    		</div>
</form>
    		<!-- {!! Form::close() !!} -->
    	</div>
    </div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

    <script type="text/javascript">
        $(".check_pwd").bind("keyup change", function(e) {
            var newpwd=$("input[name=newpwd]").val();
            if(newpwd==''){
                $("#err").html('');
                $("input[name=password]").val('');
                $(".check").attr('disabled',true);
            }
            var conpwd=$("input[name=password]").val();
            if(conpwd!=''){
              if(newpwd!=conpwd){
               $("#err").html('Incorrect new password');
               $(".check").attr('disabled',true);
           }else{
               $("#err").html('');
               $(".check").attr('disabled',false);

           }
       }
   })
        $("input[name=newpwd]").bind("keyup change", function(e) {
            var newpwd=$("input[name=newpwd]").val();
            if(newpwd==''){
                $("#err").html('');
                $("input[name=password]").val('');
                $(".check").attr('disabled',true);
            }
            var conpwd=$("input[name=password]").val();
            if(conpwd!=''){
              if(newpwd!=conpwd){
               $("#err").html('Incorrect new password');
               $(".check").attr('disabled',true);
           }else{
               $("#err").html('');
               $(".check").attr('disabled',false);

           }
       }
   })
    </script>
@endsection


