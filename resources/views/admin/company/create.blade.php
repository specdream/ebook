@extends('admin.layouts.master')

@section('content')

    <div class="row">
        <div class="col-sm-10 col-sm-offset-2">
            <h1>{{ trans('quickadmin::admin.companies-create-create_user') }}</h1>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        {!! implode('', $errors->all('
                        <li class="error">:message</li>
                        ')) !!}
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! Form::open(['files' => true, 'route' => config('quickadmin.route').'.company.store', 'class' => 'form-horizontal']) !!}

    <div class="form-group">
        {!! Form::label('name', trans('quickadmin::admin.companies-create-name').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('name', old('name'), ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.companies-create-name_placeholder')]) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('email', trans('quickadmin::admin.companies-create-email').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::email('email', old('email'), ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.companies-create-email_placeholder')]) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('password', trans('quickadmin::admin.companies-create-password').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::password('password', ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.companies-create-password_placeholder')]) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('phone_number', trans('quickadmin::admin.companies-create-phone_number').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('phone_number', old('phone_number'), ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.companies-create-phone_number_placeholder')]) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('address', trans('quickadmin::admin.companies-create-address').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('address', old('address'), ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.companies-create-address_placeholder')]) !!}
        </div>
    </div>

    <div class="form-group">
    {!! Form::label('active', trans('quickadmin::admin.companies-create-status'), array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::hidden('active','') !!}
        {!! Form::radio('active', 1, true) !!} {!! trans('quickadmin::admin.companies-create-status_active') !!}
        {!! Form::radio('active', 0, false) !!} {!! trans('quickadmin::admin.companies-create-status_inactive') !!}
        
    </div>
</div>

<div class="form-group">
    {!! Form::label('avatar', trans('quickadmin::admin.companies-create-image'), array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::file('avatar') !!}
        
    </div>
</div>

    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit(trans('quickadmin::admin.companies-create-btncreate'), ['class' => 'btn btn-primary']) !!}
        </div>
    </div>

    {!! Form::close() !!}

@endsection


