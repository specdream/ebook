@extends('admin.layouts.master')

@section('content')

    <div class="row">
        <div class="col-sm-10 col-sm-offset-2">
            <h1>{{ trans('quickadmin::admin.companies-edit-create_user') }}</h1>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        {!! implode('', $errors->all('
                        <li class="error">:message</li>
                        ')) !!}
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! Form::model($company, array('files' => true, 'class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'PATCH', 'route' => array(config('quickadmin.route').'.company.update', $company->id))) !!}

    <div class="form-group">
        {!! Form::label('name', trans('quickadmin::admin.companies-edit-name').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('name', old('name',$company->name), ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.companies-edit-name_placeholder')]) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('email', trans('quickadmin::admin.companies-edit-email').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::email('email', old('email',$company->email), ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.companies-edit-email_placeholder')]) !!}
        </div>
    </div>

    <!-- <div class="form-group">
        {!! Form::label('password', trans('quickadmin::admin.companies-edit-password'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::password('password', ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.companies-edit-password_placeholder')]) !!}
        </div>
    </div> -->

    <div class="form-group">
        {!! Form::label('phone_number', trans('quickadmin::admin.companies-edit-phone_number').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('phone_number', old('phone_number',$company->phone_number), ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.companies-edit-phone_number_placeholder')]) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('address', trans('quickadmin::admin.companies-edit-address').'*', ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('address', old('address',$company->address), ['class'=>'form-control','required'=>'', 'placeholder'=> trans('quickadmin::admin.companies-edit-address_placeholder')]) !!}
        </div>
    </div>

    <div class="form-group">
    {!! Form::label('active', trans('quickadmin::admin.companies-edit-status'), array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::hidden('active',$company->address) !!}
        {!! Form::radio('active', 1, true) !!} {!! trans('quickadmin::admin.companies-edit-status_active') !!}
        {!! Form::radio('active', 0, false) !!} {!! trans('quickadmin::admin.companies-edit-status_inactive') !!}
        
    </div>
</div>

<div class="form-group">
    {!! Form::label('avatar', trans('quickadmin::admin.companies-edit-image'), array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::file('avatar') !!}
        
    </div>
</div>

    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit(trans('quickadmin::admin.companies-edit-btnedit'), ['class' => 'btn btn-primary']) !!}
        </div>
    </div>

    {!! Form::close() !!}

@endsection


