@extends('admin.layouts.master')

@section('content')

    <p>{!! link_to_route(config('quickadmin.route').'.company.create', trans('quickadmin::admin.companies-index-add_new'), [], ['class' => 'btn btn-success']) !!}</p>

    @if($companies->count() > 0)
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ trans('quickadmin::admin.companies-index-companies_list') }}</div>
            </div>
            <div class="portlet-body">
                <table id="datatable" class="table table-striped table-hover table-responsive datatable">
                    <thead>
                    <tr>
                        <th>{{ trans('quickadmin::admin.companies-index-avatar') }}</th>
                        <th>{{ trans('quickadmin::admin.companies-index-name') }}</th>
                        <th>{{ trans('quickadmin::admin.companies-index-email') }}</th>
                        <th>{{ trans('quickadmin::admin.companies-index-phone_number') }}</th>
                        <th>{{ trans('quickadmin::admin.companies-index-address') }}</th>
                        <th>{{ trans('quickadmin::admin.companies-index-status') }}</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($companies as $company)
                        <tr>
                            <td><img src="{!!\URL::To('public/uploads')!!}/{{ $company->avatar }}" width="50px" height="50px"></td>
                            <td>{{ $company->name }}</td>
                            <td>{{ $company->email }}</td>
                            <td>{{ $company->phone_number }}</td>
                            <td>{{ $company->address }}</td>
                            <td>@if($company->active==1) Active @else Inactive @endif</td>
                            <td>
                                <!-- {!! link_to_route(config('quickadmin.route').'.company.edit', trans('quickadmin::templates.templates-view_index-edit'), array($company->id), array('class' => 'btn btn-xs btn-info')) !!} -->
                                 <a href="{!!\URL::To('/'.config('quickadmin.route'))!!}/company/{{ $company->id }}/edit" class="btn btn-xs btn-info" title="Click to edit univerisities"><i class="fa fa-edit"></i></a>

                                {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => "return confirm('".trans("quickadmin::templates.templates-view_index-are_you_sure")."');",  'route' => array(config('quickadmin.route').'.company.destroy', $company->id))) !!}
                                <button type="submit" class="btn btn-xs btn-danger" title="Click to delete"><i class="fa fa-trash"></i> </button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @else
        {{ trans('quickadmin::admin.companies-index-no_entries_found') }}
    @endif

@endsection