@extends('admin.layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
        <h1>{{ trans('quickadmin::templates.templates-view_create-add_new') }}</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

{!! Form::open(array('files' => true, 'route' => config('quickadmin.route').'.tb_courses.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}


@if(\Auth::user()->role_id=='1')
<!-- <div class="form-group">
    {!! Form::label('status', 'Status*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::select('status', $status, old('status'), array('class'=>'form-control')) !!}
        
    </div>
</div> -->
{!! Form::hidden('status', old('status','Pending'), array('class'=>'form-control')) !!}
<div class="form-group">
    {!! Form::label('teacher_id', 'Teacher Name*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::select('teacher_id', $tb_users, old('teacher_id'), array('class'=>'form-control','required'=>'')) !!}
    </div>
</div>
@else
{!! Form::hidden('teacher_id', old('teacher_id',\Auth::user()->id), array('class'=>'form-control')) !!}
{!! Form::hidden('status', old('status','Pending'), array('class'=>'form-control')) !!}
@endif
<div class="form-group" style="display: none;">
    {!! Form::label('prof_id', 'Profession*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::select('prof_id', $tb_professional, old('prof_id'), array('class'=>'form-control','required'=>'')) !!}
        
    </div>
</div>
<div class="form-group">
    {!! Form::label('cat_id', 'Book*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        <select name="book" class="form-control keyvalid" required>
            @foreach($book  as $val)
            <option value="{{ $val->id }}">{{ $val->title }}</option>
            @endforeach
        </select>
    </div>
</div><div class="form-group">
    {!! Form::label('title', 'Title*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('title', old('title'), array('class'=>'form-control keyvalid','required'=>'')) !!}
         <span id="title_msg" style="color: #F00;"></span>
    </div>
   
</div>
<div class="form-group">
    {!! Form::label('summary', 'Summary', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('summary', old('summary'), array('class'=>'form-control keyvalid')) !!}
        <span id="summary_msg" style="color: #F00;"></span> 
    </div>
   
</div><div class="form-group">
    {!! Form::label('image', 'Source', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        <input type="file" name="image" required="">        
    </div>
</div>
<div class="form-group" style="display: none;">
    {!! Form::label('promo_video', 'Promo Video', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::file('promo_video') !!}
        
    </div>

</div><div class="form-group">
    {!! Form::label('price_type', 'Price Type*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::select('price_type', $price_type, old('price_type'), array('class'=>'form-control','required'=>'')) !!}
        
    </div>
</div>

<div class="form-group price_hide" style="display: none;">
    {!! Form::label('price', 'Price', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('price', old('price','0'), array('class'=>'form-control','onkeypress'=>"return isNumber(event)")) !!}
    </div>
   
</div>

<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
      {!! Form::submit( trans('quickadmin::templates.templates-view_create-create') , array('class' => 'btn btn-primary')) !!}
    </div>
</div>

{!! Form::close() !!}
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
    $(document).on('change','#price_type',function(){
        if($(this).val()=='Paid'){
            $(".price_hide").show();
        }else{
            $(".price_hide").hide();
            $("#price").val(0);
        }
    })
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var base_url=$("#base_url").val();
    $(document).on('keyup','.keyvalid',function(){
        var keyval=$(this).val();
        var id=$(this).attr('name');
        // alert(id);
        var token=$('meta[name="csrf-token"]').attr('content');
        if(keyval!=''){
        $.ajax({
            type:'POST',
            url:base_url+"/admin/tb_courses/checkkey",
            dataType: 'json',
            data:{keyval:keyval,_token:token},
            success:function(data){
              // alert(data);
              if(data>0){
                // alert('Key already exist..');
                $("#"+id+"_msg").html('Key already exist..');
                 $("#"+id).css("border", "#F00 solid 1px"); 
                 $(".btn-primary").prop('disabled',true);
              }else{
                $("#"+id).css("border", "#ccc solid 1px");
                $("#"+id+"_msg").html('');
                $(".btn-primary").prop('disabled',false);
              }
          }
      });
    }else{
        $("#"+id).css("border", "#ccc solid 1px");
                $("#"+id+"_msg").html('');
                $(".btn-primary").prop('disabled',false);
    }
    })
</script>
@endsection