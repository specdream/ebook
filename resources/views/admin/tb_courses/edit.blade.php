@extends('admin.layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
        <h1>{{ trans('quickadmin::templates.templates-view_edit-edit') }}</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>
<input type="hidden" id="cid" value="{!!$tb_courses->id!!}">
{!! Form::model($tb_courses, array('files' => true, 'class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'PATCH', 'route' => array(config('quickadmin.route').'.tb_courses.update', $tb_courses->id))) !!}



@if(\Auth::user()->role_id=='1')
<!-- <div class="form-group">
    {!! Form::label('status', 'Status*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::select('status', $status, old('status',$tb_courses->status), array('class'=>'form-control')) !!}
        
    </div>
</div> -->
{!! Form::hidden('status', old('status',$tb_courses->status), array('class'=>'form-control')) !!}
<div class="form-group">
    {!! Form::label('teacher_id', 'Teacher Name*', array('class'=>'col-sm-2 control-label','required'=>'')) !!}
    <div class="col-sm-10">
        {!! Form::select('teacher_id', $tb_users, old('teacher_id',$tb_courses->teacher_id), array('class'=>'form-control')) !!}
        
    </div>
</div>
@else
{!! Form::hidden('teacher_id', old('teacher_id',\Auth::user()->id), array('class'=>'form-control')) !!}
{!! Form::hidden('status', old('status',$tb_courses->status), array('class'=>'form-control')) !!}
@endif
<div class="form-group" style="display: none;">
    {!! Form::label('prof_id', 'Profession*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::select('prof_id', $tb_professional, old('prof_id',$tb_courses->prof_id), array('class'=>'form-control')) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('cat_id', 'Category*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::select('cat_id', $tb_categories_type, old('cat_id',$tb_courses->cat_id), array('class'=>'form-control ','required'=>'')) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('title', 'Title*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('title', old('title',$tb_courses->title), array('class'=>'form-control keyvalid','required'=>'')) !!}
       <span id="title_msg" style="color: #F00;"></span> 
    </div>
</div><div class="form-group">
    {!! Form::label('summary', 'Summary', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('summary', old('summary',$tb_courses->summary), array('class'=>'form-control keyvalid')) !!}
        <span id="summary_msg" style="color: #F00;"></span> 
    </div>
</div><div class="form-group">
    {!! Form::label('image', 'Source', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::file('image') !!}
        {!! Form::hidden('image_w', 4096) !!}
        {!! Form::hidden('image_h', 4096) !!}
        
    </div>
</div><div class="form-group" style="display: none;">
    {!! Form::label('promo_video', 'Promo Video', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::file('promo_video') !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('price_type', 'Price Type*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::select('price_type', $price_type, old('price_type',$tb_courses->price_type), array('class'=>'form-control','required'=>'')) !!}
        
    </div>
</div>
<div class="form-group price_hide" @if($tb_courses->price_type=='Free') style="display: none;" @endif>
    {!! Form::label('price', 'Price', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('price', old('price', $tb_courses->price), array('class'=>'form-control','onkeypress'=>"return isNumber(event)")) !!}
    </div>
   
</div>
<div class="form-group">
    {!! Form::label('isbnCode', 'Book ISBN*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        <input type="text" class="form-control" name="ISBN" value="{{ $tb_courses->ISBN }}" required="">
        
    </div>
</div>
<div class="form-group">
    {!! Form::label('quantity', 'Book Quantity*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        <input type="number" class="form-control" name="quantity" value="{{ $tb_courses->quantity }}" required="">
        
    </div>
</div>
<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
      {!! Form::submit(trans('quickadmin::templates.templates-view_edit-update'), array('class' => 'btn btn-primary')) !!}
      {!! link_to_route(config('quickadmin.route').'.tb_courses.index', trans('quickadmin::templates.templates-view_edit-cancel'), null, array('class' => 'btn btn-default')) !!}
    </div>
</div>

{!! Form::close() !!}
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
    $(document).on('change','#price_type',function(){
        if($(this).val()=='Paid'){
            $(".price_hide").show();
        }else{
            $(".price_hide").hide();
            $("#price").val(0);
        }
    })
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var base_url=$("#base_url").val();
    $(document).on('keyup','.keyvalid',function(){
        var keyval=$(this).val();
        var id=$(this).attr('name');
        var cid=$("#cid").val();
        // alert(id);
        var token=$('meta[name="csrf-token"]').attr('content');
        if(keyval!=''){
        $.ajax({
            type:'POST',
            url:base_url+"/admin/tb_courses/checkkey_edit",
            dataType: 'json',
            data:{keyval:keyval,_token:token,cid:cid},
            success:function(data){
              // alert(data);
              if(data>0){
                // alert('Key already exist..');
                $("#"+id+"_msg").html('Key already exist..');
                 $("#"+id).css("border", "#F00 solid 1px"); 
                 $(".btn-primary").prop('disabled',true);
              }else{
                $("#"+id).css("border", "#ccc solid 1px");
                $("#"+id+"_msg").html('');
                $(".btn-primary").prop('disabled',false);
              }
          }
      });
    }else{
        $("#"+id).css("border", "#ccc solid 1px");
                $("#"+id+"_msg").html('');
                $(".btn-primary").prop('disabled',false);
    }
    })
</script>
@endsection