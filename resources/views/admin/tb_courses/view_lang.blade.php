@extends('admin.layouts.master')

@section('content')

<div class="row">
  <div class="col-sm-10 col-sm-offset-1">
    <h3><b>Course Detailed view</b></h3>
  </div>
</div>

<div class="row">
  <div class="col-sm-10 col-sm-offset-1">
    <table class="table">
      <tbody>
        <tr>
          <th rowspan="6" width="20%"> @if($course->image != '')<img src="{{ asset('public/uploads') . '/'.  $course->image }}" width="170px" height="170px">@endif</th>
          <th width="15%">Course Name</th>
          <th width="2%">:</th>
          <td>{!! get_lang_value($course->title,$lang) !!}</td>
        </tr>
        <tr>
          <th>Teacher Name</th>
          <th>:</th>
          <td>{{ isset($course->teacher_name->name) ? $course->teacher_name->name : '' }}</td>
        </tr>
        <!-- <tr>
          <th>Profession Name</th>
          <th>:</th>
          <td>{{ isset($course->tb_professional->name) ? $course->tb_professional->name : '' }}</td>
        </tr> -->
        <tr>
          <th>Category Name</th>
          <th>:</th>
          <td>{{ isset($course->tb_categories_type->name) ? $course->tb_categories_type->name : '' }}</td>
        </tr>
        <tr>
          <th>Summary</th>
          <th>:</th>
          <td>{!!  get_lang_value($course->summary,$lang) !!}</td>
        </tr>
        <tr>
          <th>Payment  Type</th>
          <th>:</th>
          <td>{{ $course->price_type }}</td>
        </tr>
        @if($course->price_type=='Paid')
        <tr>
          <th>Payment  Type</th>
          <th>:</th>
          <td>{{ $course->price }}</td>
        </tr>
        @endif
        <!-- <tr>
          <th>Promo Video</th>
          <th>:</th>
          <td>{{ $course->promo_video }}</td>
        </tr> -->
      </tbody>
    </table> 
  </div>
</div>

<div class="row">
  <div class="col-sm-10 col-sm-offset-1">
    <h3><b>Lesson Details</b></h3>
  </div>
  <div class="col-sm-10 col-sm-offset-1">
    <h4>Total number of lesson(s) : <b>{{ $course->lession_details->count() }}</b> </h4>
  </div>
</div>


<div class="row">
  <div class="col-sm-10 col-sm-offset-1">
    <table class="table">
      <tbody>
        @foreach($course->lession_details as $key=>$value)
        <tr style="background-color: #cccc">                    
          <th width="15%"><h4><b>Lesson ({{$key+1}}) Name</b></h4></th>
          <th width="2%"><h4>:</h4></th>
          <td><h4>{!!get_lang_value($value['name'],$lang)!!}</h4></td>
          <td></td>
          <th><a class="label label-warning"  href="{!!\URL::To('/public/uploads/lesson/'.$value['video'])!!}" download="" title="Click to download"><i class="fa fa-download"></i> Video</a> &nbsp;&nbsp; <a data-toggle="modal" data-target="#playModal{{$key+1}}" class="label label-info" title="Click to play video"><i class="fa fa-play-circle" aria-hidden="true"></i> Video</a>


            <!-- Modal -->
            <div class="modal fade" id="playModal{{$key+1}}" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Play ( {!!get_lang_value($value['name'],$lang)!!} ) video</b></h4>
                  </div>
                  <div class="modal-body">
                    <div class="margin-bottom-50">
                      <div class="embed-responsive embed-responsive-16by9 thumbnail">
                        <video style="width: 100%" controls>
                          <source src="{!!\URL::To('/public/uploads/lesson/'.$value['video'])!!}" type="video/mp4">
                            Your browser does not support the video element.
                          </video>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>

                </div>
              </div>
            </th>
          </tr>
          <tr>
            <td colspan="5 "><h4><b>Lesson Description :</b> {!!get_lang_value($value['description'],$lang)!!}</td>
          </tr>
          <tr><td colspan="5 "><h4>Total number of question(s) : <b>{{ count(getQuestions($value['course_id'],$value['id'])) }}</b></td></tr>
          <tr>                    
            <th width="15%">Sno</th>
            <th width="2%"></th>
            <th>Question</th>
            <th>Mark</th>
            <th>Answer</th>
          </tr>
          @if(count(getQuestions($value['course_id'],$value['id']))>0)
          @foreach(getQuestions($value['course_id'],$value['id']) as $key1=>$value1)
          <tr>                    
            <td width="15%">{{$key1+1}}</td>
            <th width="2%"></th>
            <td><div class="pull-left">{!!get_lang_value($value1->question,$lang)!!}</div> 
              @if($value1->question_type>'1')
              <div class="pull-right">
                @if($value1->question_type=='2')
                <?php $tooltip='Click to view image'; ?>
                @elseif($value1->question_type=='3')
                <?php $tooltip='Click to play audio'; ?>
                @elseif($value1->question_type=='4')
                <?php $tooltip='Click to play video'; ?>
                @endif
                <a data-toggle="modal" data-target="#questionModal{{$key1+1}}" class="label label-warning" title="{!!$tooltip!!}" data-html='true'><i class="fa fa-eye"></i></a>
                <!-- Modal -->
                <div class="modal fade" id="questionModal{{$key1+1}}" role="dialog">
                  <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><b> 
                          @if($value1->question_type=='2')
                          View
                          @elseif($value1->question_type=='3' || $value1->question_type=='4')
                          Play
                          @endif
                          ( {{ $value1->question }} ) @if($value1->question_type=='2')
                          Image
                          @elseif($value1->question_type=='3')
                          Audio
                          @elseif($value1->question_type=='4')
                          Video
                          @endif</b></h4>
                        </div>
                        <div class="modal-body">
                          <div class="margin-bottom-50">
                            @if($value1->question_type=='4')
                            @if($value1->question_subtype=='1')
                            <div class="embed-responsive embed-responsive-16by9 thumbnail">
                              <video style="width: 100%" controls>
                                <source src="{!!\URL::To('/public/uploads/question/'.$value1->file_url)!!}" type="video/mp4">
                                  Your browser does not support the video element.
                                </video>
                              </div>
                              @else
                              <?php
                              $url = $value1->file_url;
                              preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
                              $id = $matches[1];
                              ?>
                              <object width="570" height="350" data="http://www.youtube.com/v/{!!$id!!}" type="application/x-shockwave-flash"><param name="src" value="{!!$value1->file_url!!}" /></object>                            
                              @endif    
                              @endif
                              @if($value1->question_type=='2')
                              <div  style="width: 100%" >
                                <img src="{!!\URL::To('/public/uploads/question/'.$value1->file_url)!!}" style="width: 100%;height: 100%;">
                              </div>
                              @endif
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>

                  @endif</td>
                  <td>{!! $value1->mark !!}</td>
                  <td><a class="label label-success" data-toggle="modal" data-target="#answerModal{{$value1->id}}"><i class="fa fa-eye" aria-hidden="true"></i> View</a>

                   <!-- Modal -->
                   <div class="modal fade" id="answerModal{{$value1->id}}" role="dialog">
                    <div class="modal-dialog">
                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title"><b>Question ( {{$key1+1}} ) Answers</b></h4>
                        </div>
                        <div class="modal-body">

                         <table class="table" width="100%">
                           <thead>
                             <tr>
                               <th>Sno</th>
                               <th>Answer</th>
                               <th></th>
                             </tr>
                           </thead>
                           <tbody>
                             @if(count(getAnswer($value1->id))>0)
                             @foreach(getAnswer($value1->id) as $key2=>$value2)
                             <tr>
                               <td>{!! ($key2+1) !!}</td>
                               <td><div class="pull-left">{!!get_lang_value($value2->answer,$lang)!!}</div>
                                 <div class="pull-right">
                                  <a data-hide="answerModal{{$key1+1}}" data-id="{{$key2+1}}" class="label label-warning hideans" title="{!!$tooltip!!}" data-html='true'><i class="fa fa-eye"></i></a>
                                  
                                      <!-- Modal content-->
                                      <div class="ans_data{{$key2+1}}" style="display: none">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title"><b> 
                                            @if($value2->answer_type=='2')
                                            View
                                            @elseif($value2->answer_type=='3' || $value2->answer_type=='4')
                                            Play
                                            @endif
                                            ( {{ $value2->answer }} ) @if($value2->answer_type=='2')
                                            Image
                                            @elseif($value2->answer_type=='3')
                                            Audio
                                            @elseif($value2->answer_type=='4')
                                            Video
                                            @endif</b></h4>
                                          </div>
                                          <div class="modal-body">
                                            <div class="margin-bottom-50">
                                              @if($value2->answer_type=='4')
                                              @if($value2->answer_subtype=='1')
                                              <div class="embed-responsive embed-responsive-16by9 thumbnail">
                                                <video style="width: 100%" controls>
                                                  <source src="{!!\URL::To('/public/uploads/question/'.$value2->file_url)!!}" type="video/mp4">
                                                    Your browser does not support the video element.
                                                  </video>
                                                </div>
                                                @else
                                                <?php
                                                $url = $value2->file_url;
                                                preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
                                                $id = $matches[1];
                                                ?>
                                                <object width="570" height="350" data="http://www.youtube.com/v/{!!$id!!}" type="application/x-shockwave-flash"><param name="src" value="{!!$value2->file_url!!}" /></object>                            
                                                @endif    
                                                @endif
                                                @if($value2->answer_type=='2')
                                                <div  style="width: 100%" >
                                                  <img src="{!!\URL::To('/public/uploads/question/'.$value2->file_url)!!}" style="width: 100%;height: 100%;">
                                                </div>
                                                @endif
                                              </div>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-default modal_close" data-show="answerModal{{$key1+1}}" >Close</button>
                                            </div>
                                          </div>

                                        
                                    </div>
                                  </td>
                                  <td>
                                   @if($value2->correct_answer==1)
                                   <span class="label label-success "><i class="fa fa-check"></i></span>
                                   @else
                                   <span class="label label-danger "><i class="fa fa-times"></i></span>
                                   @endif
                                 </td>
                               </tr>
                               @endforeach
                               @else
                               <tr>
                                <th colspan="3" style="text-align: center;font-size: 18px;">No detail found</th> 
                              </tr>
                              @endif
                            </tbody>

                          </table>

                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>

                    </div>
                  </div>

                </td>
              </tr>
              @endforeach
              @else
              <tr>
                <th colspan="5" style="text-align: center;font-size: 18px;">No detail found</th>
              </tr>
              @endif
              @endforeach
            </tbody>
          </table> 
        </div>
      </div>
      <!-- Modal -->
      <div class="modal fade" id="answerModalin" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content append">
          </div>
        </div>
      </div>
      <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
      <script type="text/javascript">
        $(document).on('click','.hideans',function(){
          var hide=$(this).attr('data-hide');
          var id=$(this).attr('data-id');
          var html=$(".ans_data"+id).html();
          $(".append").empty().html(html);
          $("#"+hide).modal('hide');
          $("#answerModalin").modal('show');
        })
        $(document).on('click','.modal_close',function(){
          var show=$(this).attr('data-show');
          $("#"+show).modal('show');
          $("#answerModalin").modal('hide');
        })
      </script>
      @endsection