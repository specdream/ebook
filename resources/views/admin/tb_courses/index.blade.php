@extends('admin.layouts.master')

@section('content')
<p>{!! link_to_route(config('quickadmin.route').'.tb_courses.create', trans('quickadmin::templates.templates-view_index-add_new') , null, array('class' => 'btn btn-success')) !!}</p>

@if ($tb_courses->count())
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">{{ trans('quickadmin::templates.templates-view_index-list') }}</div>
    </div>
    <div class="portlet-body">
        <div><h4><b>Approved Count : </b> <span class="label label-success">{!!getCourseScount('Accept')!!}</span> ;<b>Pending Count : </b> <span class="label label-danger">{!!getCourseScount('Pending')!!}</span> ;<b>Waiting Count : </b> <span class="label label-warning">{!!getCourseScount('Draft')!!}</span> ;</h4></div><br>
        <table class="table table-striped table-hover table-responsive datatable" id="datatable">
            <thead>
                <tr>
                    <th>
                        {!! Form::checkbox('delete_all',1,false,['class' => 'mass']) !!}
                    </th>
                    
                    <th>Image</th>
                    @if(\Auth::user()->role_id=='1')<th>Teacher Name</th>@endif
                    
                    <th>Category</th>
                    <th>Title</th>
                    <th>Summary</th>
                    
                    <!-- <th>Promo Video</th> -->
                    <th>Price Type/Price</th>
                    <th>Status</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($tb_courses as $row)
                <tr>
                    <td>
                        {!! Form::checkbox('del-'.$row->id,1,false,['class' => 'single','data-id'=> $row->id]) !!}
                    </td>
                    <td>@if($row->image != '')<img src="{{ asset('public/uploads') . '/'.  $row->image }}" width="50px" height="50px">@endif</td>
                    @if(\Auth::user()->role_id=='1')<td>{{ isset($row->teacher_name->name) ? $row->teacher_name->name : '' }}</td>@endif
                    
                    <td>{{ isset($row->tb_categories_type->name) ? $row->tb_categories_type->name : '' }}</td>
                    <td>{{ $row->title }}</td>
                    <td>{{ $row->summary }}</td>
                    
                    <!-- <td>{{ $row->promo_video }}</td> -->
                    <td>{{ $row->price_type }} @if($row->price_type=='Paid') / <span class="label label-success">$ - {{ $row->price }}</span> @endif</td>
                    <td>
                    @if($row->status=='Draft')
                     @if(\Auth::user()->role_id=='1')
                     <span class="label label-warning" title="Waiting for your approval" style="cursor: pointer;">W.Approval</span>
                     @else
                     <span class="label label-warning" title="Waiting for admin approval" style="cursor: pointer;">W.Approval</span>
                     @endif
                    @elseif($row->status=='Accept')
                        <span class="label label-success">Approved</span>
                    @elseif($row->status=='Reject')

                        <span class="label label-danger">Rejected</span>&nbsp;<i class="fa fa-question-circle" data-html="true" data-toggle="popover" title="<b>Reason</b> / <b>Date : </b>{!!reject_reason($row->id,'date')!!}" data-content="{!!reject_reason($row->id,'reason')!!}" style="cursor: pointer;"></i>
                    @else
                    <span class="label label-danger">{{ $row->status }}</span>
                    @endif
                    </td>
                    <td>

                        @if(\Auth::user()->role_id=='1')
                        @if($row->status=='Draft')
                        <form method="POST" action="{!!\URL::To('/'.config('quickadmin.route'))!!}/tb_courses/approve/{{ $row->id }}" accept-charset="UTF-8" style="display: inline-block;" onsubmit="return confirm('Are you sure want to approve?');">
                            {{ csrf_field() }}

                            <button type="submit" class="btn btn-xs btn-warning" title="Click to approve"><i class="fa fa-thumbs-o-up"></i> </button>
                        </form>
                        <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#reject_popup{{ $row->id }}" title="Click to reject"><i class="fa fa-thumbs-down"></i> </button>

                        <!-- Modal -->
                        <div id="reject_popup{{ $row->id }}" class="modal fade" role="dialog">
                          <div class="modal-dialog  modal-sm">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title ">Notification</h4>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure. Want to reject this course ? </p>
                                <label><b>Reason :</b></label>
                                <textarea name="reason{{ $row->id }}" id="reason{{ $row->id }}" class="form-control" placeholder="Type your reason"></textarea>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success reject_course" data-id="{{ $row->id }}">Yes</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                            </div>
                        </div>

                    </div>
                    </div>

                        @elseif($row->status=='Accept')
                        <button type="button" class="btn btn-xs btn-success" title="This course already approved"><i class="fa fa-thumbs-up"></i> </button>
                        @else
                        
                        @endif

                        @endif

                        @if(\Auth::user()->role_id=='4')
                        @if($row->status=='Pending')
                        <form method="POST" action="{!!\URL::To('/'.config('quickadmin.route'))!!}/tb_courses/complete/{{ $row->id }}" accept-charset="UTF-8" style="display: inline-block;" onsubmit="return confirm('Are you sure. Complete everything?');">
                            {{ csrf_field() }}

                            <button type="submit" class="btn btn-xs btn-success" title="Click to change status for completed"><i class="fa fa-thumbs-o-up"></i> </button>
                        </form>
                        @endif
                        @endif

                        <a href="{!!\URL::To('/'.config('quickadmin.route'))!!}/tb_courses/{{ $row->id }}/edit" class="btn btn-xs btn-info" title="Click to edit course"><i class="fa fa-edit"></i></a>

                        {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => "return confirm('".trans("quickadmin::templates.templates-view_index-are_you_sure")."');",  'route' => array(config('quickadmin.route').'.tb_courses.destroy', $row->id))) !!}

                        <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> </button>
                        {!! Form::close() !!}

                        <a href="{!!\URL::To('/'.config('quickadmin.route'))!!}/lession/{{ $row->id }}" title=" Click to Manage Lesson"><button type="button" class="btn btn-xs btn-success"><i class="fa fa-book"></i> </button></a>
                        <span style="position: relative;">
                        <a title=" Click to view course Details" class="drop_box  dropdown-toggle" data-toggle="dropdown"><button type="button" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> </button></a>
                        <ul class="dropdown-menu">
                            <li><a href="{!!\URL::To('/'.config('quickadmin.route'))!!}/tb_courses/view/{{ $row->id }}" title=" Click to view key format"><i class="fa fa-key"></i> Key format</a></li>
                            <li class="divider"></li>
                            @if(count($language)>0)
                            @foreach($language as $lang)
                            <li><a href="{!!\URL::To('/'.config('quickadmin.route'))!!}/tb_courses/view/{{ $row->id }}/{!!$lang->id!!}" title=" Click to view key format"><i class="fa fa-language"></i> {!!$lang->name!!}</a></li>
                            <li class="divider"></li>
                            @endforeach
                            @endif

                        </ul>
                        </span>
                        


                        <!-- <a href="{!!\URL::To('/'.config('quickadmin.route'))!!}/tb_courses/view/{{ $row->id }}" title=" Click to view course Details"><button type="button" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> </button></a> -->
                    </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-xs-12">
            <button class="btn btn-danger" id="delete">
                {{ trans('quickadmin::templates.templates-view_index-delete_checked') }}
            </button>
        </div>
    </div>
    {!! Form::open(['route' => config('quickadmin.route').'.tb_courses.massDelete', 'method' => 'post', 'id' => 'massDelete']) !!}
    <input type="hidden" id="send" name="toDelete">
    {!! Form::close() !!}
</div>
</div>
@else
{{ trans('quickadmin::templates.templates-view_index-no_entries_found') }}
@endif

@endsection

@section('javascript')
<script>
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover(); 
        $('#delete').click(function () {
            if (window.confirm('{{ trans('quickadmin::templates.templates-view_index-are_you_sure') }}')) {
                var send = $('#send');
                var mass = $('.mass').is(":checked");
                if (mass == true) {
                    send.val('mass');
                } else {
                    var toDelete = [];
                    $('.single').each(function () {
                        if ($(this).is(":checked")) {
                            toDelete.push($(this).data('id'));
                        }
                    });
                    send.val(JSON.stringify(toDelete));
                }
                $('#massDelete').submit();
            }
        });
    });
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
     var base_url=$("#base_url").val();
    $(document).on('click','.reject_course',function(){
        var id=$(this).attr('data-id');
        var reason=$("#reason"+id).val();
        if(reason!=''){
            var token=$('meta[name="csrf-token"]').attr('content');
            $.ajax({
            type:'POST',
            url:base_url+"/admin/tb_courses/course_reject",
            dataType: 'json',
            data:{reason:reason,_token:token,id:id},
            success:function(data){
              location.reload();
          }
      });

        }else{
            $("#reason"+id).css('border','1px solid #F00');
        }
    })
</script>
@stop