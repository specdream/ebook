<link href="{!! asset('quickadmin/css/animate.css') !!}" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
@if(Auth::user())
<?php $notifications=notification(); ?>
@else
<?php $notifications = array(); ?>
@endif

<style type="text/css">
    .link-block {
    font-size: 12px;
    padding: 10px;
}
</style>
<div class="page-header navbar navbar-fixed-top top-author-nav">
    <div class="page-header-inner">
        <div class="page-header-inner">
            <div class="navbar-header">
                <a href="{{ url(config('quickadmin.homeRoute')) }}" class="navbar-brand">
                    <img src="{!!\URL::To('/')!!}/sdream/img/logo.jpeg"></a>
                </a>
            </div>
            <a href="javascript:;"
            class="menu-toggler responsive-toggler"
            data-toggle="collapse"
            data-target=".navbar-collapse">
        </a>

        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">

            <li id="refresh_cnt" class="user dropdown">   
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle count-info" aria-expanded="true">
        <span data-toggle="tooltip" title="Click to view experience details"> <i class="icon-yelp fa fa-bell-o"></i>  <span class=" label label-danger feacnt1" style="border-radius: 50%;">
                          {{count($notifications)}}</span>
                      </span></a>
                      <ul class="dropdown-menu dropdown-menu-right icons-right flipInY animated" code=""  style="width: 400px;max-height: 354px;overflow:auto">
                        <li><div class="text-center"><label><h4><b>Notification(s)</b></h4></label></div><hr></li>
                         @if(count($notifications)>0)
                         @foreach($notifications as $notifications1)
                        <li id="ffproid_{{$notifications1->id}}" >
            <div class="text-center link-block col-sm-12">
                <a href="{{ url('rooms/') }}/" target="_blank">
                    <span data-toggle="tooltip" title="Click to view this room details"> 
                        <div class="col-sm-2"><img src="{{URL::To('/')}}/public/uploads/{!!getAvatar($notifications1->n_user_id)!!}" class="origin round user_image" style="width: 35px;"></div>
                        <div class="col-sm-10" style="text-align: left;font-size:12px"><strong>User Role :</strong> {{getRole($notifications1->n_role)}}<br><strong>User Name :</strong>{{$notifications1->n_uname}} <br><strong>Notification :</strong>
                            @if(strlen($notifications1->notification)<=40)
                            {{$notifications1->notification}}
                            @else
                            {{substr($notifications1->notification,0,40)}}...
                            @endif
                        </div></span></a>
                        
                    </div>
                </li>                       
                        @endforeach
                        @endif
                        <li id="npro_nodet"  @if(count($notifications)>0) style="display: none;" @endif ><div class="text-center">No details found</div></li>
                    </ul>

                </li>  

                <li class="user dropdown"><a class="dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown"><img src="{!! asset('sdream/img/client-img.jpg') !!}" class="rounded-circle" alt=""><i class="caret"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right icons-right flipInY animated">
                    <li><a href=""><i class="fa  fa-user"></i> @if(Auth::user()) Auth::user()->name @endif</a></li>
                        <li><a href="{!!URL::To('/')!!}/admin"><i class="fa  fa-laptop"></i> Dashboard</a></li>
                        <li><a href="{!!URL::To('/')!!}" target="_blank"><i class="fa fa-desktop"></i> Main Site</a></li>
                        <li><a href="{!!URL::To('/')!!}/admin/profile"><i class="fa fa-user"></i> Profile</a></li>
                        <!-- <li><a href="http://sdreamtechdemo.com/products/airstar/airbnb/user/logout"><i class="fa fa-sign-out"></i> Logout</a>  </li> -->
                        <li> {!! Form::open(['url' => 'logout']) !!}
                <button type="submit" class="logout" style="color: #555;">
                    <i class="fa fa-sign-out fa-fw"></i>
                    <span class="title">{{ trans('quickadmin::admin.partials-sidebar-logout') }}</span>
                </button>
                {!! Form::close() !!}</li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
</div>
<style type="text/css">
    .user.dropdown img {
    height: 50px;
    width: 50px;
}

.rounded-circle {
    border-radius: 50%!important;
}
.user.dropdown img {
    max-width: 4.75rem;
}

</style>