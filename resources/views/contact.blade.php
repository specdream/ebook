@include('layout.header')
<div class="contact-head"> 
	<div class="contact-banner-img">
		<span class="overlay-top-header" style="background:rgba(0,0,0,0.5);"></span>
		<div class="container">
			<div class="text-left banner-txt ">
				<div class="col-lg-12 col-md-12 col-sm-12 cont-banner col-xs-12">
					<!-- <h1>Keep place with Change</h1> -->
					<h5>Contact</h5>

				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<ul class="contact-nav">
		<li><a>Home</a></li>
		<li><a>Contact</a></li>
	</ul>

	<div class="col-xs-12 col-sm-6">
		<div class="contact_heading   text-left">
			<h3 class="title">Contact Info</h3>
			<p class="sub-heading" style="">Welcome to our Website. We are glad to have you around.</p>
			<span class="line"></span>
		</div>

	</div>
	<div class="col-xs-12 col-sm-6">
		<div class="contact_heading   text-left">
			<h3 class="title">Send A Message</h3>
			<p class="sub-heading" style="">Your email address will not be published. Required fields are marked.</p>
			<span class="line"></span>
		</div>
	</div>
<div class="col-xs-12 col-sm-12 contact-info-form">
	<div class="col-xs-12 col-sm-6 pad-left">
		<div class="col-xs-12 col-sm-12 contact-infomation">
			<div class="col-xs-12 col-sm-6 contact-info pad-left">
				<div class="contact-title"><span class="contact-ico fa fa-phone"></span>Phone</div>
				<div class="contact-number" >+7 (800) 123 45 69</div>
			</div>
			<div class="col-xs-12 col-sm-6 pad-left">
				<div class="contact-title"><span class="contact-ico fa fa-envelope-o"></span>Email</div>
				<div class="contact-number" ><a href="mailto:khawarizmm@gmail.com">khawarizmm@gmail.com</a></div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 contact-infomation">
			<div class="contact-title"><span class="contact-ico fa fa-map-marker"></span>Address</div>
			<div class="contact-number" >PO Box 97845 Baker st. 567, Los Angeles, California, US.</div>
		</div>
	<div class="col-xs-12 col-sm-12">
		<div class='demopadding'>
  <div class='cont-icon social fb'><i class='fa fa-facebook'></i></div>
  <div class='cont-icon social tw'><i class='fa fa-twitter'></i></div>
  <div class='cont-icon social in'><i class='fa fa-linkedin'></i></div>
	</div>
	</div>
	</div>

<div class="col-xs-12 col-sm-6 contact-form">

<div class="col-xs-6 col-md-6 form-group pad-left ">
<input class="form-control " id="name" name="name" placeholder="Name" type="text" required autofocus />
</div>
<div class="col-xs-6 col-md-6 form-group pad-right">
<input class="form-control" id="email" name="email" placeholder="Email" type="email" required />
</div>
<div class="col-xs-12 col-md-12 form-group no_pad">
<input class="form-control" id="name" name="name" placeholder="Name" type="text" required autofocus />
</div>
<textarea class="form-control" id="message" name="message" placeholder="Message" rows="5"></textarea>

<div class="btn btn-contact-submit pull-left">Submit</div>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-12 no-pad">
	<div class="contact-sub-banner-img">
	<span class="overlay-top-header" style="background:rgba(0,0,0,0.5);"></span>
	<div class="col-lg-12 col-md-12 col-sm-12 cont-img-banner col-xs-12 text-center">
	<h4>Subscribe To Our News</h4>
		    <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3 ">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search" id="txtSearch"/>
        <div class="input-group-btn">
          <button class="btn  btn-sub-submit" type="submit">
            Submit
          </button>
        </div>
      </div>
    </div>
	</div>

	</div>
	<div class="container">
	<div class="contact_heading   text-left">
			<h3 class="title">Location on map</h3>
		<span class="line"></span>
	</div>
	</div>
<iframe src="https://www.google.com/maps/embed?pb=!1m12!1m10!1m3!1d62882.811781191056!2d78.0854159!3d9.9193176!2m1!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1523542739401" width="100%" height="400px" frameborder="0" style="border:0" allowfullscreen > </iframe>
</div>
@include('layout.footer')