@include('layout.header')
<div class="col-xs-12 no_pad margin_tb_30 payhistory_page">
  <div class="container">
    <div class="col-xs-12 profile_right">
      @include('layout.profilemenu')
      <center class="profile_title">
        <h2>Payment History</h2>
        <div>Payment information for courses.</div>
      </center>
      <div class="col-xs-12 table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>No</th>
              <th>Date</th>
              <th>Course</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>5-4-2018</td>
              <td>Course 1</td>
            </tr>
            <tr>
              <td>2</td>
              <td>5-4-2018</td>
              <td>Course 2</td>
            </tr>
            <tr>
              <td>3</td>
              <td>5-4-2018</td>
              <td>Course 3</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@include('layout.footer')