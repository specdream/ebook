	@include('layout.header')

<style>
	.searchEmptyState_main {
    width: 100%;
    height: 400px;
}
.searchEmptyState {
    background-image: url('{{ url('public/uploads/searchEmptyState.png') }}');
    background-size: 500px;
    background-position: center;
    background-repeat: space;
    width: 100%;
    height: 300px;
}
.search-pagination{float:right;}
.filter-section{font-size:18px;font-weight:500;}
</style>
	<div class="header-container">
		<section class="search-header col-xs-12 col-sm-12">
			<div class="search-results-text">
				<div class="col-xs-12 col-sm-6 pull-left padd-left">
					<!-- <h2>
						<span class="dontprazremove">{{ $book->firstItem() }}</span>-<span>{{ $book->lastItem() }}</span>of<span>{{ $totalCount }}</span></span>	
					</h2>	 -->
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="dropdown filter-drop">
					<!-- <button class="btn btn-default dropdown-toggle btn-fil-tig" type="button" data-toggle="dropdown">Dropdown Example
						<span class="caret"></span></button>
						<ul class="dropdown-menu search-drop-filt">

							<li><a href="">Bestsellers</a></li>
							<li><a href="">Highest Rated</a></li>
							<li><a href="">Price: Low to High</a></li>
							<li><a href="">Title: A to Z</a></li>
							<li><a href="">Title: Z to A</a></li>
							<li><a href="">Date: Newest to Oldest</a></li>
							<li><a href="">Date: Oldest to Newest</a></li>
						</ul> -->
						@if($totalCount > 0 )
						<h2 class="filter-section">
						<span class="dontprazremove">{{ $book->firstItem() }}</span> - <span>{{ $book->lastItem() }}</span> of <span>{{ $totalCount }}</span>	
					</h2>		
					@endif
					</div>

				</div>
			</section>

			<section class="seach-content">
				<div class="col-xs-12 col-sm-3 col-md-3">
					<ul id="accordion" class="accordion">
						<li>
							<div class="link">Categories<i class="fa fa-chevron-down"></i></div>
							<ul class="filter-submenu" style="display: block;">
								<li><a href="{{ url('search') }}">All <!-- ({{$getOverAllCount}}) --></a></li>
								@foreach(\Helper::bookCategory() as $value)
								@if(Request::get('categories'))
								@if(Request::get('categories') == $value->id)
								<li @if(Request::get('subCategories') == '') class='active' @endif ><a href="{{ url('search').'?categories='.$value->id.'&language='.Request::get('language').'&author='.Request::get('author') }}">{{ $value->name }} <!-- ({{ $totalCount }}) --></a></li>
								@if(count($subCategories) > '0')
								<li class="dontprazremove">
									<div class="link">Sub Categories<i class="fa fa-chevron-down"></i></div>
									<ul class="filter-submenu" style="display: block;">
										<li><a href="{{ url('search').'?categories='.Request::get('categories').'&language='.Request::get('language').'&author='.Request::get('author') }}">All</a></li>
										@if(count($subCategories) > 0)
										@foreach($subCategories as $value)
										<li @if(Request::get('subCategories') != '') class='active' @endif><a href="{{ url('search').'?categories='.Request::get('categories').'&subCategories='.$value['id'].'&language='.Request::get('language').'&author='.Request::get('author') }}">{{$value['name']}} <!-- ({{$value['count']}}) --></a></li>
										@endforeach
										@endif
									</ul>
								</li>
								@endif
								@endif
								@else
								<li><a href="{{ url('search').'?categories='.$value->id.'&author='.Request::get('author') }}">{{ $value->name }} <!-- (379612) --></a></li>
								@endif
								@endforeach


							</ul>
						</li>

						<li>
							<div class="link">Language<i class="fa fa-chevron-down"></i></div>
							<ul class="filter-submenu" style="display: block;">
								<li><a href="{{ url('search').'?categories='.Request::get('categories').'&subCategories='.Request::get('subCategories').'&author='.Request::get('author') }}">All</a></li>
								@if(count($language) > 0)
								@foreach($language as $value)
								<li @if(Request::get('language') != '' && Request::get('language') == $value['id']) class='active' @endif><a href="{{ url('search').'?categories='.Request::get('categories').'&subCategories='.Request::get('subCategories').'&author='.Request::get('author').'&language='.$value['id'] }}">{{ $value['name'] }}</a></li>
								@endforeach
								@endif
							</ul>
						</li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-9 col-md-9">
					@foreach($book as $value)
					<div class="result-items">
						<div class="col-xs-5 col-sm-3">	
							<img src="{!! asset('public/uploads/books_banner') . '/'.  $value->id . '/'.  $value->banner !!}">
						</div>

						<div class="col-xs-7 col-sm-9 padd-left">
							<div class="search-book-content">
								<h2 class="search-book-titile">{{ $value->title }}</h2>
								<!-- <h3 class="sub-book-title">The Chronicles of St. Mary's series</h3> -->
								<div class="search-author-name">by <span>{!! \Helper::getUser($value->user_id,'name') !!}</span>
								</div>
			<!-- <div class="search-series-name">series<span>The Chronicles of St. Mary's series #1</span>
		</div> -->
		<div class="book-paragraph">
			{!! $value->summary !!}
			<p class="read-more"><a href="{{ url('detail').'/'.$value->id }}" class="button">Read More</a></p>
		</div>
		<div class="search-book-ratings">
			<div class="stars-read-only">
				<i class="fa fa-star color_star"></i>
				<i class="fa fa-star color_star"></i>
				<i class="fa fa-star color_star"></i>
				<i class="fa fa-star color_star"></i>
				<i class="fa fa-star"></i>
				<span>(28)</span>
			</div>
		</div>
		<h3 class="book-rate-type">
			<!-- dontprazremove -->
				<!-- @if($value->price_type == 'Paid')
                {!! \Session::get('ipCurrencySymbol') !!} {{ \Helper::currencyPriceVal($value->price,$value->currency) }}
                @else
                Free
                @endif -->
                @if($value->price_type == 'Free') Free @else ${!!$value->price!!} @endif
            </h3>

        </div>
    </div>
</div>

@endforeach
@if($totalCount == 0 )
<div class="searchEmptyState_main">
<div class="searchEmptyState"></div>
</div>
@endif
<div class="search-pagination" id="pagination">
	<?php 
	$c_url = $_SERVER['REQUEST_URI'];

	echo str_replace('/?', '?', $book->appends(Request::except('page'))->render()) ?>
</div>

</div>
</section>
</div>

<script type="text/javascript">
	$(function() {
		var Accordion = function(el, multiple) {
			this.el = el || {};
			this.multiple = multiple || false;

		// Variables privadas
		var links = this.el.find('.link');
		// Evento
		links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
	}

	Accordion.prototype.dropdown = function(e) {
		var $el = e.data.el;
		$this = $(this),
		$next = $this.next();

		$next.slideToggle();
		$this.parent().toggleClass('open');

		if (!e.data.multiple) {
			$el.find('.filter-submenu').not($next).slideUp().parent().removeClass('open');
		};
	}	

	var accordion = new Accordion($('#accordion'), false);
});
</script>
@include('layout.footer')  