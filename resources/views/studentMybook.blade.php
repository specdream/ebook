@include('layout.header')  
 <section class="col-xs-12 col-sm-12 col-md-12 no-pad mybook-section">
 	<div class="header-container">
 		<div class="pull-left">
 			<h3>My Book</h3>
 		</div>
 		<div class="pull-right">
 			<!-- <ul class="nav navbar-nav">
 				<li>
 					<input type="hidden" name="coursetitle_id" id="coursetitle_id">
 					<div class="search_box">
 						<input type="text" name="coursetitle" id="coursetitle"  placeholder="Search My Books" >
 						<button type="submit">
 							<span class="fa fa-search"></span>
 						</button>
 					</div>
 				</li>
 			</ul> -->
 		</div>
 	</div>
 </section>

    	<section class="col-xs-12 col-sm-12 col-md-12 no-pad">
    		<div class="header-container">
    			<div class="library-items">
    				
    			</div>
    			<div class="pull-right">
    				
    				</div>
    			</div>
    	</section>

    	<section class=" col-xs-12 col-sm-12 col-md-12 no-pad mybook-list" >
    		<div class="header-container">
    		
    			<ul class="book-list">
 <?php 
 $i = '0';
            foreach($books as $value){ ?>
    				<li class="col-xs-12 col-sm-4 col-md-3">
    				<div style="width: 18rem;float:left;">
    					<div class="images">
              <img src="{!! asset('public/uploads/college_books_banner') . '/'.  $value['id'] . '/'.  $value['banner'] !!}">
    					</div>
    					<div class="mybook-content">
    					<div class="book-price">
              <a href="javascript:void(0)">$ {{ $value['price'] }}</a> 
              <span class="dropdown">
              <div class="more-menu  dropdown-toggle" id="menu1" type="button" data-toggle="dropdown">
                </div>
                <ul class="dropdown-menu drop-mybook" role="menu" aria-labelledby="menu1">
                  <li data-toggle="modal" data-target="#myModal" ><a role="menuitem" tabindex="-1" href="#">Log in</a></li>
                </ul>
              </span>
    					</div>
    					
    					<div id="" class="drop-mybook"> 
    						<ul>
    							<li>Mark as Unread<span class="more-menu"></span></li>
    							<li>Write a  Review</li>
                  <li>Download</li>
                  <li>Move to Archive</li>
    						</ul>
    					</div>
    					
    					</div>
    						<div class="book-title">{{ $value['title'] }} </div>
    					<div class="book-para">{!! $value['summary'] !!}</div>
    					<div class="book-auth-name">{{ $value['author'] }}</div>
    					</div>
    				</li>
    					 <?php $i++; } ?>
    			</ul>
    		
    		</div> 
        <div class="search-pagination" id="pagination">
  <?php 
  $c_url = $_SERVER['REQUEST_URI'];

  echo str_replace('/?', '?', $books->appends(Request::except('page'))->render()) ?>
</div>
    	</section>
    
    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-small">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Student Details</h4>
        </div>
        <div class="modal-body">
          <div class="modal-body stud-review">
        <div class="">
          <form action="{{ url('') }}" id="book_login" method="post">
          <div class="form-group col-sm-12 col-xs-12">
          <label class="control-label col-sm-4 std-label no-pad">Student Id</label>
            <div class="col-sm-7 Book-Ban no-pad">
              
              <input type="text" class="form-control" name="std id" value="" required="" placeholder="">
              <span id="summary_msg" style="color: #F00;"></span> 
            </div>
          </div>
          <div class="form-group col-sm-12 col-xs-12">
          <label class="control-label col-sm-4 std-label no-pad">Book Id</label>
            <div class="col-sm-7 Book-Ban no-pad">
              
              <input type="text" class="form-control" name="book item-link-underlay" value="" required="" placeholder="">
              <span id="summary_msg" style="color: #F00;"></span> 
            </div>
          </div>
          <div class="form-group col-sm-12 col-xs-12">
          <label class="control-label col-sm-4 std-label no-pad">Book Name</label>
            <div class="col-sm-7 Book-Ban no-pad">
              
              <input type="text" class="form-control" name="book name" value="" required="" placeholder="">
              <span id="summary_msg" style="color: #F00;"></span> 
            </div>
          </div>
          </form>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        
          <button type="button" class="btn btn-default">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div>


<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0px";

}


$(document).ready(function(){
    $('#mydropdown').change(function(){
      $selected_value=$('#mydropdown option:selected').text();
      $('#result').text($selected_value);
    });
  });

</script>
@include('layout.footer')
