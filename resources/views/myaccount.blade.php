
@include('layout.myacc_header')

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<div class="header-container">
	<section class="myaccount-page" style="background-color:#f8f8f8;margin-bottom: 40px;>
		<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 ">	

			<div class="col-xs-12 col-md-6 col-sm-6  col-lg-6 signin-div">
				<section class="acc-preference ac-prifer">
					<h2>Preferences and Personalization</h2>
					<p class="status_message"></p>
					<form action="{{ url('student/profile') }}" method="post" id="studentProfile">
					{{ csrf_field() }}
					<input type="hidden" name="place" value="1">
						<div class="form-group col-lg-6 acc-padding-left">
							<label>Name</label>
							<input type="text" name="name" class="form-control" id="name" value="{{ $user->username }}">
						</div>

						<div class="form-group col-lg-6 acc-padding-right">
							<label>Email</label>
							<input type="text" name="email" class="form-control" id="email" value="{{ $user->email }}">
						</div>

						<div class="form-group">
							<label>Date Of Birth</label>
							<input type="text" id="datepicker" class="form-control" name="dob" value="{{ $user->dob }}" placeholder="Choose">
						</div>
						<div class="">
							<div class="" style="font-size:14px;text-transform:uppercase;margin:15px 0px;">gender(option)</div>
							<div class="form-check">
								<label class="dob-radio">
									<input id="radio" type="radio" value="1" name="radio" @if($user->gender == '1') checked @endif > <span class="label-text">Male</span>
								</label>
							</div>
							<div class="form-check">
								<label class="dob-radio" style="float:none;">
									<input id="radio" type="radio" value="2" name="radio" @if($user->gender == '2') checked @endif > <span class="label-text">Female</span>
								</label>
							</div>
						</div>
						
						<button type="submit" class="btn btn-default acc-btn-save pull-right">Save</button>
					</form>
				</section>	
				
			</div>

			<div class="col-xs-12 col-md-6 col-sm-6  col-lg-6 ">
				<section class="acc-change-pass">
					<h2>Change password</h2>
					<p>Your password must be a minimum of 8 characters long. Passwords are case-sensitive.</p>
					<form action="{{ url('student/profile') }}" method="post" id="studentProfile2">
					{{ csrf_field() }}
					<input type="hidden" name="place" value="2">

						<div class="form-group">
							<label for="acc-pwd">current password:</label>
							<input type="password" class="form-control" id="old_password" placeholder="current password" name="old_password">
						</div>

						<div class="form-group">
							<label for="acc-pwd">New password:</label>
							<input type="password" class="form-control" id="new_password" placeholder="New password" name="new_password">
						</div>

						<div class="form-group">
							<label for="acc-pwd">Confirm password:</label>
							<input type="password" class="form-control" id="conform_new_password" placeholder="Confirm password" name="conform_new_password">
						</div>


						<button type="submit" class="btn btn-default acc-btn-save pull-right">Save</button>
					</form>
				</section>	
				<section class="acc-deactivate">
		<h2>Deactivate Account?</h2>
		<p class="pull-left">Want to deactivate your account?</p>
		<div class="pull-right de-activate"><p data-toggle="modal" data-target="#deactivate-acc">Deactivate Account</p></div>
		<div class="modal fade" id="deactivate-acc" role="dialog">

    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Deactivate Account?</h4>
        </div>
        <div class="modal-body">
          <p>If you proceed with this action you will not be able to:.</p>
          <ul>
          	<li>Update your e-book library</li>
          	<li>Sign in with your linked accounts</li>
          	<li>Synchronize your library between devices</li>
          	<li>Buy new eBooks from the Kobo Store</li>
          	<li>Download new copies of your Kobo eBooks
</li>	
          </ul>
          <p>By clicking Deactivate Account below, you will be signed out and your account will be closed immediately.</p>
        </div>
        <div class="modal-footer">
          <form action="{{ url('student/profile') }}" method="post" id="studentProfile3">
					{{ csrf_field() }}
					<input type="hidden" name="place" value="3">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-default acc-btn-save" >Deactivate Account</button>
          </form>
        </div>
      </div>
      
    </div>
  </div>
	</section>
			</div>

		</div>
	</section>

	
</div>
<script type="text/javascript">
	$(document).ready(function(){
	$('#studentProfile').submit(function(e){
		e.preventDefault();
		var name = $('#name').val();
		var email = $('#email').val();
		var datepicker = $('#datepicker').val();
		var radio = $('#radio').val();
		var message = '';
		if(name == ''){
			message = 'Feild required';
		}else if(email == ''){
			message = 'Feild required';
		}else if(datepicker == ''){
			message = 'Feild required';
		}else if(radio == '0'){
			message = 'Feild required';
		}else if(email != ''){
				var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
			    if (filter.test(email)) {
			        //return true;
			    }
			    else {
			        message = 'Enter the valid email';
			    }
			}
		//alert(message);
		if(message != ''){
		$('.status_message').show();
		$('.status_message').text(message);
		return false;
		}
		$('.status_message').show();
		$('.status_message').text('Updating......');
		$.ajax({
			url:$(this).attr('action'),
			type:$(this).attr('method'),
			data:$(this).serialize(),
			success:function(data){
				if(data == 'success'){
				$('.status_message').text('Saved..');
				setTimeout(function(){ $('.status_message').hide(); },500);
				}else if(data == 'noLogin'){
				window.location.replace('<?php echo url('login'); ?>'); 
				}
			}
		})
	})
	$('#studentProfile2').submit(function(e){
		e.preventDefault();
		var old_password = $('#old_password').val();
		var new_password = $('#new_password').val();
		var conform_new_password = $('#conform_new_password').val();
		var message = '';
		if(old_password == ''){
			message = 'Feild required';
		}else if(new_password == ''){
			message = 'Feild required';
		}else if(conform_new_password == ''){
			message = 'Feild required';
		}else if(old_password != conform_new_password){
			message = 'New password and Confirm password must be same';
		}

		if(message != ''){
		$('.status_message').show();
		$('.status_message').text(message);
		return false;
		}
		$('.status_message').show();
		$('.status_message').text('Updating......');
		$.ajax({
			url:$(this).attr('action'),
			type:$(this).attr('method'),
			data:$(this).serialize(),
			success:function(data){
				if(data == 'success'){
				$('.status_message').text('Saved..');
				setTimeout(function(){ $('.status_message').hide(); },500);
				}else if(data == 'WrongPwd'){
				$('.status_message').text('Please enter the valid password');
				}else if(data == 'noLogin'){
				window.location.replace('<?php echo url('login'); ?>'); 
				}
			}
		})
	})
	})
</script>
<style type="text/css">
	input[type="radio"] + .label-text:before{
		content: "\f10c";
		font-family: "FontAwesome";
		speak: none;
		font-style: normal;
		font-weight: normal;
		font-variant: normal;
		text-transform: none;
		line-height: 1;
		-webkit-font-smoothing:antialiased;
		width: 1em;
		display: inline-block;
		margin-right: 5px;
	}

	input[type="radio"]:checked + .label-text:before{
		content: "\f192";
		color: #00a7ce;
		animation: effect 250ms ease-in;
		font-size: 19px;
	}

	input[type="radio"]:disabled + .label-text{
		color: #aaa;
	}

	input[type="radio"]:disabled + .label-text:before{
		content: "\f111";
		color: #ccc;
	}
	

	input[type="checkbox"], input[type="radio"]{
		position: absolute;
		right: 9000px;
	}
	.status_message {
    position: absolute;
    color: red;
    right: 10px;
}
</style>
<script type="text/javascript">
	$(function() {
		$( "#datepicker" ).datepicker({
			dateFormat : 'yy-mm-dd',
			changeMonth : true,
			changeYear : true,
			yearRange: '-100y:c+nn',
			maxDate: '-1d'
		});
	});
</script>
@include('layout.footer')


