@include('layout.header')
<link href="{!! asset('sdream/css/animate.css') !!}" rel="stylesheet">
<link href="{!! asset('sdream/css/animate.min.css') !!}" rel="stylesheet">
<div class="col-xs-12 lesson-background">
	<div class="container">

		<div class="lesson-content ">
			<div class="col-md-4">
				<div class="lesson-img"></div>
			</div>
			<div class="col-md-7">
				<h1 class="lesson-code-title">Enter Your Lesson Code</h1>
				<div class="">
					<i class="fas fa-gift"></i>
					<input class="form-control" type="text" name="" placeholder="Enter The Code">
				</div>
				<td>

					<input class="btn btn-lesson-submit" type="submit" name="" value="Submit">

				</td>
			</div>
		</div>
	</div>
</div>


</style>
<script type="text/javascript">
	setInterval(function() {
		    console.log('Running animation');
		$('.lesson-content').addClass('animated pulse');
	}, 500);
</script>
@include('layout.footer')

