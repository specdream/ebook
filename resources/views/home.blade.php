@include('layout.header')
<script type="text/javascript">
jQuery(function(){
  jQuery("#typed").typed({
      
    strings: [<?php foreach(\Helper::getCourseCategoryDetails() as $category) { echo '"'."$category->name".'",'; }?>],
      typeSpeed: 100,
      backSpeed: 100,
      backDelay: 1000,
      showCursor: false,
      loop: true,
      contentType: 'html',
      fadeOut: true,
      fadeOutClass: 'typed-fade-out',
      fadeOutDelay: 500,
      loopCount: false,
      smartBackspace: true,
      callback: function(){ foo(); },
      resetCallback: function() { newTyped(); }
  });
});
function newTyped(){}
function foo(){ console.log("Callback"); }
</script>

    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:350px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="{!! asset('sdream/img/slider/spin.svg') !!} " />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
            <div data-p="170.00">
                <img data-u="image" src="{!! asset('sdream/img/slider/hero.jpg') !!}" />
            </div>
            <div data-p="170.00">
                <img data-u="image" src="{!! asset('sdream/img/slider/002.jpg') !!}" />
            </div>
            <div data-p="170.00">
                <img data-u="image" src="{!! asset('sdream/img/slider/003.jpg') !!}" />
            </div>
      
        </div>
        <!-- Bullet Navigator -->
<!--         <div data-u="navigator" class="jssorb051" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                </svg>
            </div>
        </div> -->
        <!-- Arrow Navigator -->
<!--         <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div> -->
    </div>

<div class="home_page">
<!--   <div class="banner-image">
    <div class="container">
      <div class="text-center banner-txt ">
        <div class="col-lg-12 col-md-12 col-sm-12 bannerimg col-xs-12">
          
          <h5>Get mastered in</h5>
          <h1><span id="typed" style="white-space:pre;"></span></h1>
          <h5>@lang('core.Log_in_or_sign_up_to_get_courses_for') </h5>
          <a href="{!!URL::To('/')!!}/login"><button type="button" class="btn btn-outerline">@lang('core.Click_Here')</button></a>
        </div>
      </div>
    </div>
  </div> -->
<!--   <section class="col-xs-12 no_pad">
    <div class="container">
      <div class="accomod_block row">
        <div class="col-md-4 col-sm-4 col-xs-12">
          <img src="sdream/img/accomod_1.jpg">
          <h5>@lang('core.Great_Facilities')</h5>
          <p>@lang('core.Great_Facilities_content')</p>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
          <img src="sdream/img/accomod_2.jpg">
          <h5>@lang('core.Our_Campus')</h5>
          <p>@lang('core.Our_Campus_content') </p>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
          <img src="sdream/img/accomod_3.jpg">
          <h5>@lang('core.Accomodation')</h5>
          <p>@lang('core.Accomodation_content') </p>
        </div>
      </div>
    </div>
  </section> -->
<!--   <div class="col-xs-12 udemy-work">
    <div class="container">
      <div class="col-md-4 col-sm-4 col-xs-12 text-center">
        <img src="sdream/img/study.png">
        <div class="how-udemy-works">
          <h4>@lang('core.online_courses')</h4>
          <p>@lang('core.Explore_a_variety_of_fresh_topics')</p>
        </div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-12 text-center">
        <img src="sdream/img/expert.png">
        <div class="how-udemy-works">
          <h4>@lang('core.Expert_instruction')</h4>
          <p>@lang('core.Find_the_right_instructor_for_you')</p>
        </div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-12 text-center">
        <img src="sdream/img/lifeTime.png">
        <div class="how-udemy-works">
          <h4>@lang('core.Lifetime_access')</h4>
          <p>@lang('core.Learn_on_your_schedule')</p>
        </div>
      </div>
    </div>
  </div> -->
  <div class="col-xs-12 no_pad">
    <div class="container">
      <section class="latest_courses">
        <h2 class="discovery-units-title pink_color">@lang('core.Latest_courses')</h2>
        <div class="owl-carousel owl-theme">
          @foreach($book as $value)
          <div class="slider-item">
            <a href="{!!URL::To('/detail/')!!}/{{$value->id}}"><img src="{!! asset('public/uploads/books_banner') . '/'.  $value->id . '/'.  $value->banner !!}"></a>
            <span class="owl-content">

              <div class="price_block">
                <div class="col-md-9 col-sm-9 col-xs-9 pad_rl_5">
                  <div class="owl-text">{!! \Helper::getLangValue($value->title)!!}</div>
                  <div class="owl-sub-text">{!! \Helper::getUser($value->user_id,'name') !!}</div>
                </div>
               <div class="col-xs-12 no_pad">
                <div class="star-rate">
                  <i class="fa fa-star color_star"></i>
                  <i class="fa fa-star color_star"></i>
                  <i class="fa fa-star color_star"></i>
                  <i class="fa fa-star color_star"></i>
                  <i class="fa fa-star color_star"></i>
                </div>
              </div>
                <div class="book-price list_price pad_rl_5">
                <!-- <span class="dontprazremove">  
                @if($value->price_type == 'Paid')
                {!! \Session::get('ipCurrencySymbol') !!} {{ \Helper::currencyPriceVal($value->price,$value->currency) }}
                @else
                Free
                @endif
                </span> -->  
                <span>@if($value->price_type == 'Free') Free @else ${!!$value->price!!} @endif</span></div>
              </div>
              <button class="btn btn-normal add-cart-btn" onclick="addToCart(<?php echo $value->id ?>)">Add to Cart </button>
            </span>
          </div>
          @endforeach
        </div>
      </section>

<!--       <section class="achive_goal">
        <h2 class="discovery-units-title violt_color">@lang('core.Achieve_Your_Goals')</h2>
        <div class="owl-carousel owl-theme">
          <div class="slider-item">
            <a href="#"><img src="sdream/img/achieve_1.jpg"></a>
          </div>
          <div class="slider-item">
            <a href="#"><img src="sdream/img/achieve_2.jpg"></a>
          </div>
          <div class="slider-item">
            <a href="#"><img src="sdream/img/achieve_3.jpg"></a>
          </div>
          <div class="slider-item">
            <a href="#"><img src="sdream/img/achieve_1.jpg"></a>
          </div>
        </div>
      </section> --> 

      @foreach($categories as $category)
<?php //print_r(\Helper::getCourseDetails($category->id)); ?>
      @if(count(\Helper::getCourseDetails($category->id))>0)
      <section class="top_courses">
        <h2 class="discovery-units-title green_color">{!!$category->name!!}</h2>
        <div class="owl-carousel owl-theme">
          @foreach(\Helper::getCourseDetails($category->id) as $value)
          <div class="slider-item">
            <a href="{!!URL::To('/detail/')!!}/{{$value->id}}"><img src="{!! asset('public/uploads/books_banner') . '/'.  $value->id . '/'.  $value->banner !!}"></a>
            <span class="owl-content">

              <div class="price_block">
                <div class="col-md-9 col-sm-9 col-xs-9 pad_rl_5">
                  <div class="owl-text">{!! \Helper::getLangValue($value->title)!!}</div>
                  <div class="owl-sub-text">{!! \Helper::getUser($value->user_id,'name') !!}</div>
                </div>
               <div class="col-xs-12 no_pad">
                <div class="star-rate">
                  <i class="fa fa-star color_star"></i>
                  <i class="fa fa-star color_star"></i>
                  <i class="fa fa-star color_star"></i>
                  <i class="fa fa-star color_star"></i>
                  <i class="fa fa-star color_star"></i>
                </div>
              </div>
                <div class="book-price list_price pad_rl_5">
                <!-- <span class="dontprazremove">  
                @if($value->price_type == 'Paid')
                {!! \Session::get('ipCurrencySymbol') !!} {{ \Helper::currencyPriceVal($value->price,$value->currency) }}
                @else
                Free
                @endif
                </span> -->  
                <span>@if($value->price_type == 'Free') Free @else ${!!$value->price!!} @endif</span></div>
              </div>
              <button class="btn btn-normal add-cart-btn" onclick="addToCart(<?php echo $value->id ?>)">Add to Cart </button>
            </span>
          </div>
          @endforeach
        </div>
      </section>
      @endif
      @endforeach
    </div>
  </div>
</div>
<style type="text/css">

        /*jssor slider loading skin spin css*/
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from { transform: rotate(0deg); }
            to { transform: rotate(360deg); }
        }

        /*jssor slider bullet skin 051 css*/
        .jssorb051 .i {position:absolute;cursor:pointer;}
        .jssorb051 .i .b {fill:#fff;fill-opacity:0.5;}
        .jssorb051 .i:hover .b {fill-opacity:.7;}
        .jssorb051 .iav .b {fill-opacity: 1;}
        .jssorb051 .i.idn {opacity:.3;}

        /*jssor slider arrow skin 051 css*/
        .jssora051 {display:block;position:absolute;cursor:pointer;}
        .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
        .jssora051:hover {opacity:.8;}
        .jssora051.jssora051dn {opacity:.5;}
        .jssora051.jssora051ds {opacity:.3;pointer-events:none;}

</style>
<script type="text/javascript" src="{!! asset('sdream/js/slider/jssor.slider-27.1.0.min.js') !!}"></script>

<script type="text/javascript">
   jssor_1_slider_init = function() {

            var jssor_1_SlideshowTransitions = [
              {$Duration:5800,$Opacity:8}
            ];

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 1140;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 20);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
</script>
<script type="text/javascript">jssor_1_slider_init();</script>

@include('layout.footer')