@include('layout.header')
<!-- <div class="col-xs-12 gray_bg">
	<div class="header-container">
		<div class="shop-cart"><p>Step 1/2: Payment Methods</p></div>
	</div>
</div> -->
<style type="text/css">
  .saveSucc {
    position: absolute;
    top: 0;
    left: 60%;
    color: red;
    display: none;
}
#error-message,.error-message {
    color: red;
    font-size: 17px;
    background: #ffffff80;
    padding: 10px;
    margin-bottom: 10px;
}
</style>

<div class="col-xs-12 col-sm-12 col-md-12 check-section">
<div class="header-container">
<div class="checkout-pagess">
<div class="col-xs-12 col-sm-6">
<p style="display: none;" class="error-message">Please fill the valid detail</p>
 <form class="form-horizontal ajaxUpdateFrom" action="{{ url('billing/address') }}" role="form">
 <input type="hidden" name="_token" value="{{ csrf_token() }}">
<fieldset>
  <legend>Billing Info <p class="saveSucc">Save...</p></legend>
	<div class="form-group">
		<div class="col-sm-9">
			<input type="text" class="form-control ajaxUpdate" name="Address" id="Address" placeholder="Address" value="<?php if(count($address) > '0') echo $address->address  ?>">
		</div>
	</div>
  <div class="form-group">
    <div class="col-sm-9">
        <input type="text" class="form-control ajaxUpdate" name="state" id="state" placeholder="State" value="<?php if(count($address) > '0') echo $address->state ?>">
    </div>
  </div>
	<div class="form-group">
		<div class="col-sm-9">
			<div class="col-xs-12 col-sm-6" style="padding-left:0;">
				<input type="text" class="form-control ajaxUpdate" name="City" id="City" placeholder="City" value="<?php if(count($address) > '0') echo $address->city ?>">
			</div>
			<div class="col-xs-12 col-sm-6">
				<input type="text" class="form-control ajaxUpdate" name="postal" id="postal" placeholder="Postal/Zip Code" value="<?php if(count($address) > '0') echo $address->zip ?>">
			</div>
		</div>
	</div>

	<div class="form-group">
		<div class="col-xs-12 col-sm-12 col-md-6">

			<select class="form-control select-country ajaxUpdate" name="country" id="country">
				<option>@lang('core.Select_Country')</option>
                        @foreach(\Helper::getCountryDetails() as $aCountry)
                        <option <?php if(count($address) > '0'){ if($aCountry->id == $address->country){ echo "selected='selected'"; } } ?> value="{!!$aCountry->id!!}" data-type="{!!$aCountry->phonecode!!}">{!!$aCountry->name!!}</option>
                        @endforeach
			</select>
		</div>
	</div>


	<div class="form-group">
		<div class="col-xs-12 col-sm-12 col-md-6">
			<div class="">
				<input type="text" class="form-control ajaxUpdate" name="phone" id="phone" placeholder="Phone Number" value="<?php if(count($address) > '0')  echo $address->phone ?>">
			</div>
		</div>
	</div>
</fieldset>
</form>
</div>
<div class="col-xs-12 col-sm-6">
<div id="error-message" style="display: none;"></div>
 <form class="form-horizontal" role="form" style="display: none;">
    <fieldset>
      <legend>Payment Info</legend>

     <div class="form-group">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <select class="form-control  select-country" name="expiry-month" id="expiry-month">
                <option value="01">Visa Credit / Visa Debit</option>
                <option value="02">MasterCard</option>
            </select>
        </div>
      </div>

      <div class="form-group">
        <div class="col-xs-12 col-sm-12 col-md-9">
          <input type="text" class="form-control demoInputBox" name="card-holder-name" id="card-holder-name" placeholder="Name of the card">
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-7">

          <input type="text" class="form-control demoInputBox" name="card-number" id="card-number" placeholder="Card Number">
        </div>

        <div class="col-sm-4">
          <input type="text" class="form-control demoInputBox" name="cvv" id="cvv" placeholder="Security Code">
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-9">

          <div class="row">
           <span class="exp-date">Expiry date</span>
            <div class="col-xs-12 col-sm-4 col-md-4 month-select">
              <select class="form-control demoInputBox" name="expiry-month" id="expiry-month">
                <option>Month</option>
                <option value="01">Jan (01)</option>
                <option value="02">Feb (02)</option>
                <option value="03">Mar (03)</option>
                <option value="04">Apr (04)</option>
                <option value="05">May (05)</option>
                <option value="06">June (06)</option>
                <option value="07">July (07)</option>
                <option value="08">Aug (08)</option>
                <option value="09">Sep (09)</option>
                <option value="10">Oct (10)</option>
                <option value="11">Nov (11)</option>
                <option value="12">Dec (12)</option>
              </select>
            </div>
            <div class="col-xs-2 col-sm-1 slash-div no-pad">
            <span>/</span>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3 year-select">
            <?php $y = date('y'); 
            $ny = $y+10; //print_r($ny); ?>
              <select class="form-control demoInputBox" name="expiry-year">
              <option selected="selected">Year</option>
                <?php for($i=$y;$i<$ny;$i++){ ?>
                <option value="$i">20{{ $i }}</option>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
      </div>


    </fieldset>
  </form>
</div>
</div>
</div>
</div>

<div class="col-xs-12 gray_bggg">
<div class="col-xs-12 col-sm-6">
<p>We won't charge your credit card until you confirm your purchase in the next step!</p>
</div>

<div class="col-xs-12 col-sm-4">
<form id="paymentform" class="paymentforms" @if($totalPrice > 0) action="{{ url('paymentPaytabs') }}" @else action="{{ url('paymentPaytabsColg') }}" @endif enctype="multipart/form-data" method="get" role="form" style="display: block;" onSubmit="return validate();">
<div class="col-xs-12 choose-category">
  <ul>
    <li class="btn btn-default" id="book" @if($totalPrice == 0) style="display:none" @endif>General</li>
    <li class="btn btn-default" id="college_book" @if($cogTotalPrice == 0) style="display:none" @endif>College</li>
  </ul>
</div>
<div class="col-xs-12">
<button class="checkout_btn" type="submit">Checkout</button>
</div>
</form>
</div>
</div>

<div class="col-xs-12 col-md-12">
<div class="checkout-container">
<div class="col-xs-12 col-sm-4">
<!-- <p class="setp-conform">Step 2/2: Purchase Confirmation</p> -->
</div>
<div class="col-xs-12 col-sm-4">
<!-- <p class="credit-balance">(Store Credit Balance RS. 0.00)</p> -->
</div>
<div class="col-xs-12 col-sm-4 no-pad">

<div class="tot-amount norl" @if($totalPrice == 0) style="display:none" @endif><p class="tot-ams"><span>Total:</span> $. {{ $totalPrice }}</p></div>

<div class="tot-amount colg" @if($cogTotalPrice == 0) style="display:none" @endif><p class="tot-ams"><span>Total:</span> $. {{ $cogTotalPrice }}</p></div>

</div>
</div>
</div>
<script type="text/javascript">
$('.tot-amount.colg').hide();
  $('#book').click(function(){
    $('.tot-amount.norl').fadeIn('300');
    $('.tot-amount.colg').fadeOut('300');
    var url = '{{ url('paymentPaytabs') }}';
    $('#paymentform').attr('action',url);
  })
  $('#college_book').click(function(){
    $('.tot-amount.colg').fadeIn('300');
    $('.tot-amount.norl').fadeOut('300');
    var url = '{{ url('paymentPaytabsColg') }}';
    $('#paymentform').attr('action',url);
  })
  
</script>


<!-- <div class="col-xs-12 no-pad checkout-main-div">

	<div class="header-container">
		<div class="col-md-9 col-sm-9 col-xs-12">
			<div class="add-cart">
				<div class="cart-item-head">Your item <span> (1)</span></div>
				<div class="clearfix cart-content">
					<div class="col-md-3 col-sm-3 col-xs-12">
						<div class="cart-img" style="background-image: url('sdream/img/accomod_1.jpg');"></div>
					</div>
					<div class="col-md-7 col-sm-7 col-xs-12">
						<b>ZDResearch Reverse Engineering</b>
						<p>The Web Development Bootcamp By Colt Steele.</p>
					</div>
					<div class="current-amt col-md-2 col-sm-2 col-xs-12">
						$ 766
						<div class="remove_product"><a class="remove_ptd">Remove</a></div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<div class="cart-total">Total :</div>
			<div class="cart-total-price">$766</div>
			<button class="checkout_btn">Checkout</button>
		</div>
	</div>
</div> -->
<script type="text/javascript">
    
    $('.ajaxUpdate').on('change',function(){
      $.ajax({
        type     : "POST",
        cache    : false,
        url      : $('.ajaxUpdateFrom').attr('action'),
        data     : $('.ajaxUpdateFrom').serializeArray(),
        success:function(data){
          if(data == 'success'){
            $('.saveSucc').fadeIn('50');
            setTimeout(function(){
              $('.saveSucc').fadeOut('300');
            },1000);
          }
        }        
      })
      
    })
</script>
<script src="{!! asset('sdream/js/jquery.creditCardValidator.js') !!}"></script>
<script>
function validate(){
  var total = '6';
  var Address = $('#Address').val();
  var state = $('#state').val();
  var City = $('#City').val();
  var postal = $('#postal').val();
  var country = $('#country').val();
  var phone = $('#phone').val();
  if(Address != '' && Address != 'Address'){
    total--;
  }
  if(postal != '' && postal != 'Postal/Zip Code'){
    total--;
  }
  if(state != '' && state != 'State'){
    total--;
  }
  if(City != '' && City != 'City'){
    total--;
  }
  if(country != '' && country != 'Select Country'){
    total--;
  }
  if(phone != '' && phone != 'Phone Number'){
    total--;
  }
  
  if(total != '0'){
    $('.error-message').show();
    return false;
  }
  
  
  }
/*function validate(){
  var valid = true;  
    $(".demoInputBox").css('background-color','');
    var message = "";

    var cardHolderNameRegex = /^[a-z ,.'-]+$/i;
    var cvvRegex = /^[0-9]{3,3}$/;
    
    var cardHolderName = $("#card-holder-name").val();
    var cardNumber = $("#card-number").val();
    var cvv = $("#cvv").val();
    console.log(cardHolderName);
    console.log(cardNumber);
    console.log(cvv);
    if(cardHolderName == "" || cardNumber == "" || cvv == "") {
         message  += "<div>All Fields are Required.</div>";  
         if(cardHolderName == "") {
           $("#card-holder-name").css('background-color','#FFFFDF');
         }
         if(cardNumber == "") {
           $("#card-number").css('background-color','#FFFFDF');
         }
         if (cvv == "") {
           $("#cvv").css('background-color','#FFFFDF');
         }
       valid = false;
    }
    alert()
    if (cardHolderName != "" && !cardHolderNameRegex.test(cardHolderName)) {
        message  += "<div>Card Holder Name is Invalid</div>";    
        $("#card-holder-name").css('background-color','#FFFFDF');
        valid = false;
    }
    
    if(cardNumber != "") {
          $('#card-number').validateCreditCard(function(result){
            if(!(result.valid)){
                  message  += "<div>Card Number is Invalid</div>";    
                $("#card-number").css('background-color','#FFFFDF');
                valid = false;
            }
        });
    }
    
    if (cvv != "" && !cvvRegex.test(cvv)) {
        message  += "<div>CVV is Invalid</div>";    
        $("#cvv").css('background-color','#FFFFDF');
        valid = false;
    }
    
    if(message != "") {
        $("#error-message").show();
        $("#error-message").html(message);
    }
    return valid;
}*/
</script>

@include('layout.footer')