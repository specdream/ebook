@include('layout.header')
    <div class="container">
<div class="col-xs-12 col-md-12 col-sm-12 lesson-detail-head">
<h2 class="text-center">@lang('core.l_Test_code_verification')</h2>
<div class="lesson-detail-card"> 
    <div class="col-md-6">
      <div class="pricing hover-effect">
        <div class="pricing-head">
          <h3>@lang('core.l_Course_Details')</h3>
        </div>
        <ul class="pricing-content list-unstyled">
          <li>
           <div class="detail-title" width="35%"><i class="fa fa-pencil-square-o"></i>@lang('core.l_Course_Name'):</div>
            <div class="detail-ans" width="55%">{!!\Helper::getLangValue(\Helper::getCourseKey($Key_Generated->course_id,'key'))!!}</div>
          </li>
          <li>
              <div class="detail-title"><i class="fa fa-certificate"></i>@lang('core.l_Course_Catogory'):</div>
              <div class="detail-ans">{!!\Helper::getCategory($Key_Generated->course_id)!!}</div>
          </li>
        </ul>
        <form method="POST" action="{!!URL::To('/')!!}/learner/testPage/{!!$Key_Generated->id!!}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
        {{ csrf_field() }}
         <div class=" hover-effect">
        <div class="pricing-head">
          <h3>@lang('core.l_Lesson_Details') </h3>
        </div>
         <ul class="pricing-content list-unstyled">
         <li>
             <div class="detail-title"><i class="fa fa-sort-numeric-asc"></i>@lang('core.l_Lesson_No'):</div>
             <div class="detail-ans">{!!\Helper::getLessonNo($Key_Generated->lesson_id,$Key_Generated->course_id)!!} @lang('core.l_Lesson')</div>
           </li>
           <li>
             <div class="detail-title"><i class="fa fa-files-o"></i>@lang('core.l_Lesson_Name'):</div>
             <div class="detail-ans">{!!\Helper::getLangValue(\Helper::getLessonKey($Key_Generated->lesson_id,'key'))!!}</div>
           </li>
           <li>
           <tr>
             <td>
             
               <div class="checkbox checkbox-success ">
                        <input id="checkbox3" type="checkbox" required="">
                        <label for="checkbox3">
                            @lang('core.l_Verified')
                        </label>
                    </div>
            
             </td>
           </tr>
           </li>
         </ul>

   
        </div>
        <div class="pricing-footer">
          <p>
             
          </p>
           <button type="submit" class="btn yellow-crusta">@lang('core.l_Go')</button>
        </div>
         </form>
      </div>
    </div>

    <div class="col-md-6">
      <div class="pricing hover-effect">
        <div class="pricing-head">
          <h3>@lang('core.l_Learner_Details')</h3>
          <h4>        
      <div class="profile-header-container">   
        <div class="profile-header-img">
                <img class="img-circle" src="{!!URL::To('/public/uploads/')!!}/{{\Auth::user()->avatar}}" />
                <!-- badge -->
                <div class="rank-label-container">
                    <span class="label label-default rank-label">{{\Auth::user()->name}}</span>
                </div>
            </div>
        </div> 
          </h4>
        </div>
        <ul class="pricing-content list-unstyled">
          <li>
           <div class="detail-title" "><i class="fa fa-envelope"></i>@lang('core.l_Email'):</div>
            <div class="detail-ans" >{{\Auth::user()->email}}</div>
          </li>
          <li>
          <div class="detail-title" ><i class="fa fa-id-card"></i>@lang('core.l_Id'):</div>
            <div class="detail-ans" ">549984</div>
          </li>
          <!-- <li>
           <div class="detail-title"><i class="fa fa-university"></i>University :</div>
            <div class="detail-ans">Anna</div>
          </li> -->
  
        </ul>

      </div>
    </div>
</div>
</div>
</div>


<style type="text/css">
.profile-header-container{
    margin: 0 auto;
    text-align: center;
}
.detail-title i{ margin-right:5px; color:#002e3b;}
.profile-header-img {
    padding: 6px;
}

.profile-header-img > img.img-circle {
    width: 120px;
    height: 120px;
    border: 2px solid #51D2B7;
}

.profile-header {
    margin-top: 43px;
}

/**
 * Ranking component
 */
.rank-label-container {
    margin-top: -19px;
    /* z-index: 1000; */
    text-align: center;
}

.label.label-default.rank-label {
    background-color: rgb(10, 46, 59);
    padding: 5px 10px 5px 10px;
    border-radius: 27px;
    margin: 10px 100px;
}

</style>
@include('layout.footer')