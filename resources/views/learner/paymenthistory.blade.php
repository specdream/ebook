@include('layout.header')
<div class="col-xs-12 no_pad margin_tb_30 payhistory_page">
  <div class="container">
    <div class="col-xs-12 profile_right">
      @include('layout.profilemenu')
      <center class="profile_title">
        <h2>@lang('core.l_Payment_History')</h2>
        <div>@lang('core.l_Payment_information_for_courses').</div>
      </center>
      <div class="col-xs-12 table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>@lang('core.l_No')</th>
              <th>@lang('core.l_Date')</th>
              <th>@lang('core.l_Course')</th>
              <th>@lang('core.l_Amount')</th>
              <th>@lang('core.l_Status')</th>
            </tr>
          </thead>
          <tbody>
          @if(count($Purchased_Course)>0)
          @foreach($Purchased_Course as $key=>$value)
            <tr>
              <td>{!!$key+1!!}</td>
              <td>{!!date('d-m-Y',strtotime($value->paid_date))!!}</td>
              <td>{!!\Helper::getLangValue(\Helper::getCourseKey($value->course_id,'key'))!!}</td>
              <td>{!!$value->paid_amount!!} {!!$value->paid_amount_currency!!}</td>
              <td>@if($value->payment_status=='pending') <span class="label label-danger">{!!$value->payment_status!!}</span> @else <span class="label label-success">{!!$value->payment_status!!}</span> @endif</td>
            </tr>
            @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@include('layout.footer')