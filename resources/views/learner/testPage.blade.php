@include('layout.header')
<style type="text/css">
	/*.loading{position:absolute;display:block;top:50%;left:50%;z-index:10;content:" ";background-image:url(fancybox_loading.gif);height:33px;width:33px;margin-top:-15px;margin-left:-15px;}*/
	.loading:before{position:absolute;display:block;top:50%;left:50%;z-index:10;content:" ";background-image:url(../../css/loading.gif);height:33px;width:33px;margin-top:-15px;margin-left:-15px;}
.profile_pic_container{display:inline-block;position:relative;}
.loading:after{position:absolute;display:block;top:0;bottom:0;left:0;right:0;background-color:#fff;content:" ";z-index:9;opacity:.9;-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=90)";filter:alpha(opacity=90);}
</style>
<div class="col-xs-12 question_page margin_tb_30 " >
	<div class="container no_pad">
		<section id="add_load" class="col-md-8 col-sm-8 col-xs-12 right_qus  ">
	
		<form method="POST" action="{!!URL::To('/')!!}/learner/textSubmit" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
       				{{ csrf_field() }}
       				<input type="hidden" name="keygen_id" value="{!!$keygen_id!!}">
		@foreach($question as $key=>$value)
		<input type="hidden" name="ques_id[{{($key+1)}}]" value="{!!$value['id']!!}">
		<div class="tab" id="tab{{$key+1}}" @if(($key+1)>1) style="display:none" @endif >
		<div class="queation_answer">
			<b>Question {{$key+1}} of {{$lesson->num_que}}</b>
			<p>{!!\Helper::getLangValue($value['question'])!!}</p>
			@if($value['question_type']==2)
				@if($value['question_subtype']==1)
				<img src="{!!URL::To('/')!!}/public/uploads/question/{!!$value['file_url']!!}" width="150px">
				@else
				<img src="{!!$value['file_url']!!}">
				@endif
			@elseif($value['question_type']==3)
				@if($value['question_subtype']==1)
				<div class="audio_palyers">
				<audio controls class="player_audio">
					<source  src="{!!\URL::To('/public/uploads/question/'.$value['file_url'])!!}" type="audio/mpeg">
					Your browser does not support the audio element.
				</audio>
				</div>
				@else
				<div class="audio_palyers">
				<audio controls class="player_audio">
					<source  src="{!!$value['file_url']!!}" type="audio/mpeg">
					Your browser does not support the audio element.
				</audio>
				</div>
				@endif
			@elseif($value['question_type']==4)
				@if($value['question_subtype']==1)
				<div class="embed-responsive embed-responsive-16by9 thumbnail">
					<video style="width: 100%" controls class="player_audio">
						<source  src="{!!\URL::To('/public/uploads/question/'.$value['file_url'])!!}" type="video/mp4">
							Your browser does not support the video element.
						</video>
					</div>
				@else
								<?php
                              $url = $value['file_url'];
                              preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
                              $id = $matches[1];
                              ?>
                              <div class="text-center">
                              <object width="500" height="350" class="player_audio" data="http://www.youtube.com/v/{!!$id!!}" type="application/x-shockwave-flash"><param name="src" value="{!!$value['file_url']!!}" /></object>  </div>
				@endif
			@endif

			<ul>
			<?php $aAns_id=array();?>
			@if(count(\Helper::getAllAnswer($value['id']))>0)
			@foreach(\Helper::getAllAnswer($value['id']) as $key1=>$value1)
				<li>
				<div class="question_answer col-xs-12 col-sm-3">
				<div class="answer_images" style="background-image: url('{!!URL::To('/')!!}/public/uploads/question/{!!$value1->file_url!!}');background-position:center;height:150px;background-size: cover;"></div>
				<!-- <img src="{!!URL::To('/')!!}/public/uploads/question/{!!$value1->file_url!!}" width="150px"> -->
				<div class="image-options">
				<label for="c_{{($key+1)}}_{{($key1)}}"><input id="c_{{($key+1)}}_{{($key1)}}" class="answer_color" data-id="{{($key+1)}}" name="answer[{{($key+1)}}]" value="{!!$value1->id!!}" type="radio">{!!\Helper::getLangValue($value1->answer)!!}</label></div></div>

				@if($value1->answer_type==2)
				@if($value1->answer_subtype==1)
				
				@else
				<img src="{!!$value1->file_url!!}">
				@endif
			@elseif($value1->answer_type==3)
				@if($value1->answer_subtype==1)
				<div class="audio_palyers">
				<audio controls class="player_audio">
					<source  src="{!!\URL::To('/public/uploads/question/'.$value1->file_url)!!}" type="audio/mpeg">
					Your browser does not support the audio element.
				</audio>
				</div>
				@else
				<div class="audio_palyers">
				<audio controls class="player_audio">
					<source  src="{!!$value1->file_url!!}" type="audio/mpeg">
					Your browser does not support the audio element.
				</audio>
				</div>
				@endif
			@elseif($value1->answer_type==4)
				@if($value1->answer_subtype==1)
		
				<div class="embed-responsive embed-responsive-16by9 thumbnail question_video">
					<video controls class="player_audio">
						<source  src="{!!\URL::To('/public/uploads/question/'.$value1->file_url)!!}" type="video/mp4">
							Your browser does not support the video element.
						</video>
					</div>
				
				@else
								<?php
                              $url = $value1->file_url;
                              preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
                              $id = $matches[1];
                              ?>
                              <object width="570" height="350" class="player_audio" data="http://www.youtube.com/v/{!!$id!!}" type="application/x-shockwave-flash"><param name="src" value="{!!$value1->file_url!!}" /></object>  
				@endif
				@endif
				</li>
				<?php $aAns_id[]=$value1->id;?>
			@endforeach
			@endif
			<input type="hidden" name="ans_id[{{($key+1)}}]" value="{!!implode('~',$aAns_id)!!}">
			</ul>
			</div>
		<div class="col-xs-12 col-md-12">
			<div class="mark-review">

                 <label>
                 <input type="checkbox" name="review" data-id="{{$key+1}}" id="mrkreview_{{$key+1}}" value="1" onClick="activateItem({{$key+2}})">
				&nbsp;Mark For Review</label>
				<div class="pull-right"><button class="btn btn-success next_question" id="btn_id_{{$key+1}}" data-current="{{$key+1}}" data-next="{{$key+2}}" type="button">Next</button></div>
                </div>
			
			</div>
			</div>

		@endforeach
		</section>
		<section class="col-md-4 col-sm-4 col-xs-12 left_qus">
			<div class="timmer" id="timmer">
				<div class="col-xs-4"><b id="demo_h">{{sprintf("%02d", $lesson->hours)}}</b><br>Hour</div>
				<div class="col-xs-4"><b id="demo_m">{{sprintf("%02d", $lesson->minutes)}}</b><br>Minutes</div>
				<div class="col-xs-4"><b id="demo_s">{{sprintf("%02d",00)}}</b><br>Seconds</div>
						</div>
			<div class="box_hint col-xs-12 no_pad">
				<div class="col-xs-6"><span class="small_box green_clr"></span> Attempted</div>
				<div class="col-xs-6"><span class="small_box"></span> Unattempted</div>
				<div class="col-xs-6"><span class="small_box orange_clr"></span> Current</div>
				<div class="col-xs-6"><span class="exclamation badge">!</span> Marked For Reviews</div>
			</div>
			<div class="qustion_no">
			<input type="hidden" name="tot_que" id="tot_que" value="{!!$lesson->num_que!!}">
			<input type="hidden" name="is_complete" id="is_complete" value="0">
			<span class="row">
			@for($i=1;$i<=$lesson->num_que;$i++)
				<div><span class="smal_box @if($i==1) orange_clr @endif" id="color_tab{{$i}}" data-ques="{{$i}}">{{$i}}</div>
			@endfor
			</div>
			<div style="display: none;" id="sub_btn">
			<hr>
				 <p align="center">
                                                <font size="-1"> I'm Done</font> </p><p align="center">
                                                
                                            <button type="submit" name="subm" class="btn btn-perspective btn-primary">SUBMIT MY TEST</button> </p>  
                                            <p id="err" style="color: #F00;"></p>
			</div>
			</div>
			</form>
			</div>
		</section>
	</div>
</div>
<script type="text/javascript">
	$(document).on('click','.next_question',function(){
		var current=$(this).attr('data-current');
		var next=$(this).attr('data-next');
		var check_arr=Array();
		
		if($("#is_complete").val()==0){
			if(current!=$("#tot_que").val()){
				$("#tab"+current).hide();$("#tab"+next).show();

				$("#color_tab"+current).removeClass('orange_clr');$("#color_tab"+next).addClass('orange_clr');
				// alert((parseInt(current)+1)+"||"+$("#tot_que").val())
				if((parseInt(current)+1)==$("#tot_que").val()){$("#sub_btn").show();}
				$('.player_audio').trigger('pause');
				$('#player_audio')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*'); 

			} else{
				$("#is_complete").val(1);
				
				$.each($("input[name='review']:checked"), function(){ 

					check_arr.push($(this).attr('data-id'));

				});
				var cky=0;var cvl=0;
				$.each( check_arr, function( key, value ) {
					// alert( key + ": " + value );
					if(current==$("#tot_que").val()){
						if(key==0){
							 cky=key;cvl=value;
				$("#tab"+current).hide();
				$("#tab"+value).show();
				$("#mrkreview_"+value).attr('checked',false);
				$("#mrkreview_"+value).attr('disabled',true);
				$("#color_tab"+value).html(value);
				$("#color_tab"+current).removeClass('orange_clr');$("#color_tab"+value).addClass('orange_clr');
			} 
			// alert(key+"**"+parseInt(cky+1)+"/"+cvl+"||"+value);
			if(key==parseInt(cky+1)){ $("#btn_id_"+cvl).attr('data-next',value);
			 }
		}
				});
			}
		}else{
			$.each($("input[name='review']:checked"), function(){ 
				check_arr.push($(this).attr('data-id'));
			});
			var cky=0;var cvl=0;
			$.each( check_arr, function( key, value ) {
					// alert( key + ": " + value );

					if(current==value){ cky=key;cvl=value; } 
					if(key==parseInt(cky+1) || check_arr.length==parseInt(key+1)){
						$("#tab"+current).hide();
						$("#tab"+next).show();
						$("#color_tab"+current).removeClass('orange_clr');$("#color_tab"+next).addClass('orange_clr');
						$("#mrkreview_"+next).attr('checked',false);
						$("#mrkreview_"+next).attr('disabled',true);
						$("#color_tab"+next).html(next);
						if(check_arr.length>parseInt(cky+1)){
						if(key==parseInt(cky+2)){ $("#btn_id_"+cvl).attr('data-next',value);}
					}else{
						$("#btn_id_"+cvl).attr('data-next',current);
					}
					}
				});
		}		
	})
	$(document).on('click','.answer_color',function(){
		var current=$(this).attr('data-id');
		$("#color_tab"+current).addClass('green_clr');
	})

	$(document).on('click',"input[name='review']",function(){
		var current=$(this).attr('data-id');
		if($(this).prop('checked')==true){
			$("#color_tab"+current).html(current+'<span class="exclamation_mark badge">!</span></span>');
		}else{
			$("#color_tab"+current).html(current);
		}
		

	})
</script>
<script>
// Set the date we're counting down to

var oldDate = new Date();
var hour = oldDate.getHours();
var min = oldDate.getMinutes();
var newDate = oldDate.setHours(hour + <?php echo sprintf("%02d", $lesson->hours);?>);
var newDate = oldDate.setMinutes(min + <?php echo sprintf("%02d", $lesson->minutes);?>);
var countDownDate = new Date(newDate).getTime();
// Update the count down every 1 second
var x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
    document.getElementById("demo_h").innerHTML = hours;
    document.getElementById("demo_m").innerHTML = minutes;
    document.getElementById("demo_s").innerHTML = seconds;
    
    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("timmer").innerHTML = "EXPIRED";
        $("#add_load").addClass('loading');
        $("#sub_btn").show();
        $("#err").html('Submit your test');
        $("#demo").removeClass('label label-default').addClass('label label-danger')
    }
}, 1000);
</script>

@include('layout.footer')