@include('layout.header')
<div class="col-xs-12 no_pad margin_tb_30 payhistory_page">
  <div class="container">
    <div class="col-xs-12 profile_right">
      @include('layout.profilemenu')
      <center class="profile_title">
        <h2>@lang('core.l_Purchased_Courses')</h2>
        <div>@lang('core.l_Purchased_Courses_lists').</div>
      </center>
      <div class="col-xs-12 table-responsive">
        
          <table class="table table-bordered">
          <thead>
            <tr>
              <th>@lang('core.l_No')</th>
              <th>@lang('core.l_Image')</th>
              <th>@lang('core.l_Course')</th>
              <th>@lang('core.l_Teacher')</th>
              <th>@lang('core.l_Category')</th>              
              <th>@lang('core.l_Action')</th>             
              <th>@lang('core.l_Status')</th>
            </tr>
          </thead>
          <tbody>
          @if(count($Purchased_Course)>0)
          @foreach($Purchased_Course as $key=>$value)
            <tr>
              <td>{!!$key+1!!}</td>
              <td><a href="{!!URL::To('/learner_detail/')!!}/{{$value->course_id}}"><img src="{!! asset('public/uploads') . '/'.  \Helper::getCourseKey($value->course_id,'image') !!}" width="150px"></a></td>
              <td>{!!\Helper::getLangValue(\Helper::getCourseKey($value->course_id,'key'))!!}</td>
              <td>{!!\Helper::getCourseKey($value->course_id,'teacher')!!}</td>
              <td>{!!\Helper::getCourseKey($value->course_id,'category')!!}</td>
              <td><a href="{!!\URL::To('/')!!}/learner/purchasedcourse/lession/{{ $value->id }}" title=" Click to view lesson"><button type="button" class="btn btn-xs btn-success"><i class="fa fa-book"></i> </button></a></td>
              <td>{!!\Helper::getCourseStatus($value->course_id)!!}</td>
            </tr>
            @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@include('layout.footer')