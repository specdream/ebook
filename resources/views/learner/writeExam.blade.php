@include('layout.header')
<link href="{!! asset('sdream/css/animate.css') !!}" rel="stylesheet">
<link href="{!! asset('sdream/css/animate.min.css') !!}" rel="stylesheet">
<div class="col-xs-12 lesson-background">
	<div class="container">

		<div class="lesson-content ">
			<div class="col-md-4">
				<div class="lesson-img"></div>
			</div>
			<div class="col-md-7">
				<h1 class="lesson-code-title">@lang('core.l_Enter_Your_Lesson_Code')</h1>
				 <form method="POST" action="{!!URL::To('/')!!}/learner/examKeyValidation" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
       				{{ csrf_field() }}
				<div class="">
					<i class="fas fa-gift"></i>
					<input class="form-control" type="text" name="key_code" placeholder="Enter The Code" required="">
				</div>
				 <?php if(implode('', $errors->all(':message'))=='invalid'){ ?> <p style="color: red"><b>@lang('core.l_Key_is_Invalid')</b></p> <?php } else if(implode('', $errors->all(':message'))=='used'){ ?> <p style="color: red"><b>@lang('core.l_Key_is_already_used')</b></p> <?php } ?>
				<button class="btn btn-lesson-submit" type="submit">@lang('core.l_Submit')</button>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
 
  <?php if(implode('', $errors->all(':message'))=='success'){?>
    $.notify('Success !  Updated successfully ', "success");
    <?php }else if(implode('', $errors->all(':message'))=='invalid'){?>
    $.notify('Warning ! Key is Invalid', "info");
    <?php }else if(implode('', $errors->all(':message'))=='used'){?>
    $.notify('Warning ! Key is already used', "info");
    <?php } ?>
});
</script>
<script type="text/javascript">
	setInterval(function() {
		$('.lesson-content').addClass('animated pulse');
	}, 500);
</script>
@include('layout.footer')

