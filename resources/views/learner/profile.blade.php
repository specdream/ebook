@include('layout.header')
<div class="col-xs-12 no_pad margin_tb_30 profile_page">
  <div class="container">

  <form method="POST" action="{!!URL::To('/')!!}/learner/update/{{$user->id}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="col-xs-12 profile_right">
      @include('layout.profilemenu')
      <center class="profile_title">
        <h2>@lang('core.l_Profile')</h2>
        <div>@lang('core.Add_information_about_yourself_to_share_on_your_profile').</div>
      </center>
      <label class="col-md-6 col-sm-6 col-xs-12">
        @lang('core.l_Name')
        <input type="text" name="name" class="form-control" value="{!!$user->name!!}" required="">
      </label>
       <label class="col-md-6 col-sm-6 col-xs-12">
        @lang('core.l_Profession')
        {!! Form::select('professional', $tb_professional, old('professional',$user->professional), array('class'=>'form-control','required'=>'')) !!}
      </label>
       <label class="col-md-6 col-sm-6 col-xs-12">
        @lang('core.l_Educational')
        <input type="text" name="educational" class="form-control" value="{!!$user->educational!!}" required="">
      </label>
      <label class="col-md-6 col-sm-6 col-xs-12">
       @lang('core.l_DOB') 
        <input type="text" name="dob" class="form-control datepicker" value="{!!$user->dob!!}" required="">
      </label>
      <label class="col-md-6 col-sm-6 col-xs-12">
        @lang('core.l_Email')
        <input type="email" name="email" class="form-control" value="{!!$user->email!!}" required="">
      </label>
      <label class="col-md-6 col-sm-6 col-xs-12">
        @lang('core.l_Phone_Number')
        <input type="text" name="phone_number" class="form-control" value="{!!$user->phone_number!!}" required="">
      </label>
      <label class="col-md-6 col-sm-6 col-xs-12">
        @lang('core.l_Address')
        <input type="text" name="address" class="form-control" value="{!!$user->address!!}" required="">
      </label>
      <label class="col-md-6 col-sm-6 col-xs-12">
        @lang('core.l_Profile_Image')
        <input type="file" name="avatar" class="form-control">
      </label>
      <label class="col-md-12 col-sm-12 col-xs-12">
        @lang('core.l_About_You')
        <textarea class="form-control" name="about_you">{!!$user->about_you!!}</textarea>
      </label>
      <label class="col-md-12 col-sm-12 col-xs-12 text-right">
        <input class="save_btn" type="submit" value="Save">
      </label> 
    </div>
    </form>
  </div>
</div>


<script type="text/javascript">
$(document).ready(function(){
  <?php if ($errors->any()) { ?>
  $.notify("<?php echo implode('', $errors->all(':message'));?>", "success");
  <?php } ?>
});
</script>
@include('layout.footer')