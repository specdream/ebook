@include('layout.header')
<div class="col-xs-12 no_pad margin_tb_30 payhistory_page">
  <div class="container">
    <div class="col-xs-12 profile_right">
      @include('layout.profilemenu')
      <center class="profile_title">
        <h2>@lang('core.l_Completed_Course')</h2>
        <div>@lang('core.l_Purchased_Courses_lists').</div>
      </center>
      <div class="col-xs-12 table-responsive">
        
          <table class="table table-bordered">
          <thead>
            <tr>
              <th>@lang('core.l_No')</th>
              <th>@lang('core.l_Image')</th>
              <th>@lang('core.l_Course')</th>
              <th>@lang('core.l_Teacher')</th>
              <th>@lang('core.l_Category')</th>              
              <th>@lang('core.l_Project_Status')</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
          @if(count($Completed_Course)>0)
          @foreach($Completed_Course as $key=>$value)
            <tr>
              <td>{!!$key+1!!}</td>
              <td><a href="{!!URL::To('/learner_detail/')!!}/{{$value->course_id}}"><img src="{!! asset('public/uploads') . '/'.  \Helper::getCourseKey($value->course_id,'image') !!}" width="150px"></a></td>
              <td>{!!\Helper::getLangValue(\Helper::getCourseKey($value->course_id,'key'))!!}</td>
              <td>{!!\Helper::getCourseKey($value->course_id,'teacher')!!}</td>
              <td>{!!\Helper::getCourseKey($value->course_id,'category')!!}</td>
              <td></td>
              <td><a data-placement="left" data-toggle="tooltip" title="Click to view all project details for this course" href="{!!URL::To('/')!!}/completedCourse/manageProject/{{$value->course_id}}"><i class="fa fa-cog"></i></a></td>
            </tr>
            @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('layout.footer')