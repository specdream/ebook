@include('layout.header')
<div class="col-xs-12 ">
	<div class="container">
	<div class="col-xs-12 col-sm-12">
			<div class="panel panel-default test-result ">
				<div class="panel-heading test-result-heading">Result</div>
				<div class="panel-body">
				
				<div class="col-xs-12 col-sm-8">
					<table width="100%" class="table">
						<tbody>
							<tr>
								<td>Test Code</td>
								<td>{!!$Key_Generated->key_code!!}</td>
							</tr>
							<tr>
								<td>Attempt</td>
								<td>{!!$Key_Generated->attempt!!}</td>
							</tr>
							<tr>
								<td>Course</td>
								<td>{!!\Helper::getLangValue(\Helper::getCourseKey($Key_Generated->course_id,'key'))!!}</td>
							</tr>
							<tr>
								<td>Lession</td>
								<td>{!!\Helper::getLangValue(\Helper::getLessonKey($Key_Generated->lesson_id,'key'))!!}</td>
							</tr>
							<tr>
								<td>Result</td>
								<td><div class="btn bt-test-pass"> @if($Key_Generated->is_pass=='yes') Pass</div><div class="btn bt-test-fail"> @else Fail @endif</div></td>
							</tr>
							<tr>
								<td>Percentage</td>
								<td>{!!$Key_Generated->percentage!!} %</td>
							</tr>
						</tbody>
					</table>
					</div>
					<div class="col-xs-12 col-sm-3">
				 <div class="home-logo"><a href="{!!URL::To('/')!!}"><img src="{!!\URL::To('/')!!}/sdream/img/logo.png"></a></div>
				</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12">
			<h3>Question Details</h3>
			<table width="100%" class="table table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Question</th>
						<th>Your Answer</th>
						<th>Correct Answer</th>
					</tr>
				</thead>
				<tbody>
					@if(count($test_store)>0)
					@foreach($test_store as $key=>$value)
					<tr>
						<td>{!!($key+1)!!}</td>
						<td>{!!\Helper::getLangValue(\Helper::getQuestionKey($value->question_id,'key'))!!}</td>
						<td>{!!\Helper::getLangValue(\Helper::getAnswerKey($value->learner_ans,'key'))!!}</td>
						<td>{!!\Helper::getLangValue(\Helper::getAnswerKey($value->correct_ans,'key'))!!}</td>
					</tr>
					@endforeach
					@endif
				</tbody>
			</table>

		</div>
	</div>
</div>
@include('layout.footer')

