@include('layout.header')
<script type="text/javascript">
$(document).ready(function() {
var f = $(".detail_left").offset().top;
  $(window).scroll(function(){
    var scroll = $(window).scrollTop();
    var s = ($('header').outerHeight() + $('.detail_brief').outerHeight() + $(".detail_detail").height()) - $(".detail_left").height();
    var test = $(".detail_brief").height()- $(".detail_left").height();
    if(scroll > s) {
        $(".detail_left").removeClass("fixed_div_ex").addClass("relative_div").css({"top": test});
    } else {
        if (scroll > f) {
            $(".detail_left").addClass("fixed_div_ex").removeClass("relative_div").css({"top": "0px"});
        }
        else
        {$(".detail_left").removeClass("fixed_div_ex");}
    }
  });
});
</script>
<div class="col-xs-12 no_pad detail_page">
    
  <section class="detail_header">
    <div class="container">
      <div class="col-md-4 col-sm-4 col-xs-12">
        
        <div class="col-xs-12 no_pad">
        <div class="detail_left">          
          <div class="detail_img" style="background-image:url('{!! asset('public/uploads') . '/'.  $aCat_courses->image !!}')"></div>
          <img src="{!! asset('public/uploads') . '/'.  $aCat_courses->image !!}" width="150px">
        </div>
        </div>
      </div>
      <div class="col-md-8 col-sm-8 col-xs-12 detail_detail">
        <h3>{!!\Helper::getLangValue($aCat_courses->title)!!}</h3>
        <!-- <h4>The only course you need to learn web development - HTML, CSS, JS, Node, and More!</h4> -->
        <div class="col-md-6 col-sm-6 col-xs-12 pad_b_20">
          244,947 @lang('core.students_enrolled')
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 pad_b_20">&nbsp;</div>
        <div class="col-md-6 col-sm-6 col-xs-12 pad_b_20"><span class="pad_r_15">@lang('core.Created_by') {!!$aCat_courses->creater_name['name']!!}</span> @lang('core.Last_updated') {!!date('m/Y',strtotime($aCat_courses->updated_at))!!}</div>
        <!-- <div class="col-md-6 col-sm-6 col-xs-12 pad_b_20">
          <span class="pad_r_15"><i class="fa fa-comment"></i> English</span>
          <span><span class="fa_cc">cc</span> English, Dutch</span>
        </div> -->
      </div>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="col-md-8 col-sm-8 col-md-offset-4 col-sm-offset-4 col-xs-12 detail_brief">
        <b>@lang('core.Description')</b>
        <div class="view_more_block">
          {!!\Helper::getLangValue($aCat_courses->summary)!!}

          <!-- <div class="view_more">View More +</div> -->
        </div>
      </div>
    </div>
  </section>
</div>
@include('layout.footer')