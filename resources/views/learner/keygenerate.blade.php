@include('layout.header')
<div class="col-xs-12 no_pad margin_tb_30 payhistory_page">
  <div class="container">
    <div class="col-xs-12 profile_right">
      @include('layout.profilemenu')
      <center class="profile_title">
        <h2>@lang('core.l_Test_Key_Generation')</h2>
        <div class="success-show_toggel"><span class="label label-success show_toggel">@lang('core.l_Click_Generate_New_Key')</span></div>
      </center>
      <center class="profile_title show_hide_form " style="display: none;">
      <form method="POST" action="{!!URL::To('/')!!}/learner/keygenerateadd" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
       {{ csrf_field() }}
      <!-- <h2>@lang('core.l_Test_Key_Generation_Form')</h2> -->
     <!--  <div class="key-gen-icon " ></div> -->
     <div>
     <div class=" key-gen-icon fa fa-gear"></div>
     <span class="key-gen-head">@lang('core.l_Test_Key_Generation_Form')</span>
     </div>
        <div align="right"><span class="label label-danger hide_toggel" style="display: none;"><i class="fa fa-eye-slash"></i> @lang('core.l_Hide_form')</span></div> <br>
        <div class="col-sm-offset-2">
        <div class="row">
        <div class="col-sm-12">
          <label align="left" class="col-sm-2 course-title-label">@lang('core.l_Courses') :</label>
          <div class="col-sm-6">
          <select class="form-control" id="course_id" name="course_id" required="">
            <option value="">@lang('core.l_Select_course')</option>
            @if(count($Purchased_Course)>0)
            @foreach($Purchased_Course as $key=>$value)
            <option value="{{$value->course_id}}">{!!\Helper::getLangValue(\Helper::getCourseKey($value->course_id,'key'))!!}</option>
            @endforeach
            @endif
          </select>
          </div>
          </div>
        </div>
        <br>
        <div class="row">
        <div class="col-sm-12">
          <label align="left" class="col-sm-2 course-title-label">@lang('core.l_Lesson') :</label>
          <input type="hidden" name="lesson_id" id="lesson_id">
          <div class="col-sm-6" id="lesson_id_show">
          <input type="text" name="lesson_name" id="lesson_name" required="" class="form-control" placeholder="Lesson Name" readonly="">
          </div>
          </div>
        </div>
        </div>
        <br>
        <div class="row">
        <div class="col-sm-12">
        <p id="error_msg" style="color: #F00;"></p>
         <button type="submit" class="btn btn-success gen_btn"> @lang('core.l_Generate')</button>
          </div>
        </div>
        </form>
      </center>
      <div class="col-xs-12 table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>@lang('core.l_No')</th>
              <th>@lang('core.l_Date')</th>
              <th>@lang('core.l_Course')</th>
              <th>@lang('core.l_Lesson')</th>
              <th>@lang('core.l_Key')</th>
              <th>@lang('core.l_Atempted')</th>
              <th>@lang('core.l_Status')</th>
            </tr>
          </thead>
          <tbody>
          @if(count($Key_Generated)>0)
          @foreach($Key_Generated as $key=>$value)
          <tr>
              <td>{!!$key+1!!}</td>
              <td>{!!date('d-M-Y',strtotime($value->created_at))!!}</td>
              <td>{!!\Helper::getLangValue(\Helper::getCourseKey($value->course_id,'key'))!!}</td>              
              <td>{!!\Helper::getLangValue(\Helper::getLessonKey($value->lesson_id,'key'))!!}</td>
              <td>{!!$value->key_code!!}</td>
              <td>{!!$value->attempt!!}</td>
              <td>@if($value->is_used=='no') <span class="label-danger label">@lang('core.l_Not_Yet_Used')</span> @else <span class="label-success label">@lang('core.l_Used')</span> @endif</td>
            </tr>
          @endforeach
          @else
          <tr><td colspan="7">No details found</td></tr>
          @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
 
  <?php if(implode('', $errors->all(':message'))=='success'){?>
    $.notify('Success !  Updated successfully ', "success");
    <?php }else if(implode('', $errors->all(':message'))=='pending'){?>
    $.notify('Warning ! Already generated key is NOT YET used. So you may use it for this key ', "danger");
    <?php } ?>
});
</script>
<script type="text/javascript">
$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
  $(document).on('click','.show_toggel',function(){
    $(".show_hide_form").toggle('slow');
    $(".show_toggel").toggle();
    $(".hide_toggel").toggle();
  })
  $(document).on('click','.hide_toggel',function(){
    $(".show_hide_form").toggle('slow');
    $(".show_toggel").toggle();
    $(".hide_toggel").toggle();
  })
  $(document).on('change','#course_id',function(){
    var id=$(this).val();
     var token=$('meta[name="csrf-token"]').attr('content');
     $('#lesson_id').val('');$('#lesson_name').val('');
      $.ajax({
            type:'POST',
            url:base_url+"/learner/getLesson",
            dataType: 'json',
            data:{id:id,_token:token},
            success:function(data){ 
              // alert(data.msg);
            if(data.msg==''){
              $('#lesson_id').val(data.id);$('#lesson_name').val(data.name);
              $(".gen_btn").show();$("#error_msg").html('');
            } else{
              $("#error_msg").html(data.msg);
              $(".gen_btn").hide();
            }           
              // $('#lesson_id').html($('<option>', { value : '' }).text('Select Lesson'));
              // $.each(data, function(key, value) {
              // $('#lesson_id').append($('<option>', { value : value['id'] }).text(value['name']));
              // });
          }
      });
  })
</script>
@include('layout.footer')