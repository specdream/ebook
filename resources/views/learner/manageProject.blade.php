@include('layout.header')
<div class="col-xs-12 no_pad margin_tb_30 payhistory_page">
  <div class="container">
    <div class="col-xs-12 profile_right">
      @include('layout.profilemenu')
      <center class="profile_title">
        <h2>@lang('core.l_Project_Management')</h2>
        <div>{!!\Helper::getLangValue(\Helper::getCourseKey($cid,'key'))!!}</div>
      </center>
      <div class="col-xs-12 table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>@lang('core.l_No')</th>
              <th>@lang('core.l_Project_file')</th>
              <th>@lang('core.l_uploaded_date')</th>
              <th>@lang('core.l_Status')</th>
            </tr>
          </thead>
          <tbody>
          @if(count($learner_projects)>0)
          @foreach($learner_projects as $key=>$value)
          <tr>
              <td>{!!$key+1!!}</td>
              <td>{!!$value->file!!}</td>              
              <td>{!!date('d-M-Y',strtotime($value->created_at))!!}</td>
              <td>
              @if($value->status=='waiting') <span class="label-warning label">@lang('core.l_waiting')</span> 
              @elseif($value->status=='rejected') <span class="label-danger label">@lang('core.l_rejected')</span> 
              @elseif($value->status=='progress') <span class="label-info label">@lang('core.l_progress')</span> 
              @else
              <span class="label-success label">@lang('core.l_approved')</span>
              @endif</td>
            </tr>
          @endforeach
          @else
          <tr><td colspan="7">@lang('core.l_No_details_found')</td></tr>
          @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@include('layout.footer')