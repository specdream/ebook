@include('layout.header')
<div class="col-xs-12 no_pad margin_tb_30 payhistory_page">
  <div class="container">
    <div class="col-xs-12 profile_right">
      @include('layout.profilemenu')
      <center class="profile_title">
        <h2>@lang('core.l_Lesson_View')</h2>
        <div>@lang('core.l_Lesson_View_for') {!!\Helper::getLangValue(\Helper::getCourseKey($course_id,'key'))!!}</div>
      </center>
      <div class="col-xs-12 table-responsive">
        
          <table class="table table-bordered">
          <thead>
            <tr>
              <th>@lang('core.l_No')</th>
              <th>@lang('core.l_Lesson')</th>             
              <th>@lang('core.l_Status')</th>             
              <th>@lang('core.l_Action')</th>
            </tr>
          </thead>
          <tbody>
          @if(count($lessions)>0)
          @foreach($lessions as $key=>$value)
            <tr>
              <th>{!!$key+1!!}</th>
              <th>{!!\Helper::getLangValue($value->name)!!}</th>             
              <th>@if(\Helper::getLearnerLessonStatus($value->id,$course_id,\Auth::user()->id)>0) <span class="label label-success">Completed</span> @else <span class="label label-danger">Pending</span> @endif</th>             
              <th><a title="Click to view this lesson details" class="label label-success" href="{!!URL::To('/')!!}/learner/purchasedcourse/lessonView/{!!$course_id!!}/{!!$value->id!!}"><i class="fa fa-eye"></i></a></th>
            </tr>
            @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@include('layout.footer')