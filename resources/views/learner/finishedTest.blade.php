@include('layout.header')
<div class="col-xs-12 no_pad margin_tb_30 payhistory_page">
  <div class="container">
    <div class="col-xs-12 profile_right">
      @include('layout.profilemenu')
      <center class="profile_title">
        <h2>@lang('core.l_Finished_Test_Details')</h2>
      </center>
      <div class="col-xs-12 table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>@lang('core.l_No')</th>
              <th>@lang('core.l_Course')</th>
              <th>@lang('core.l_Lesson')</th>
              <th>@lang('core.l_Key')</th>
              <th>@lang('core.l_Atempted')</th>
              <th>@lang('core.l_Result')</th>
              <th>@lang('core.l_Percentage')</th>
              <th>@lang('core.l_View')</th>
            </tr>
          </thead>
          <tbody>
          @if(count($Key_Generated)>0)
          @foreach($Key_Generated as $key=>$value)
          <tr>
              <td>{!!$key+1!!}</td>
              <td>{!!\Helper::getLangValue(\Helper::getCourseKey($value->course_id,'key'))!!}</td>              
              <td>{!!\Helper::getLangValue(\Helper::getLessonKey($value->lesson_id,'key'))!!}</td>
              <td>{!!$value->key_code!!}</td>
              <td>{!!$value->attempt!!}</td>
              <td>@if($value->is_pass=='yes') <span class="label label-success">Pass</span>  @else <span class="label label-danger">Fail</span> @endif</td>
              <td>{!!$value->percentage!!} %</td>
              <td><a href="{!!URL::To('/learner/lessonResult/'.$value->id)!!}"><span class="label label-success"><i class="fa fa-eye"></i></span></a></td>
            </tr>
          @endforeach
          @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@include('layout.footer')