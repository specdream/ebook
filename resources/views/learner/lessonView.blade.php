@include('layout.header')
<div class="col-xs-12 no_pad margin_tb_30 payhistory_page">
  <div class="container">
    <div class="col-xs-12 profile_right">
      @include('layout.profilemenu')
      <center class="profile_title">
        <h2>@lang('core.l_Lesson_Detailed_View')</h2>
        <div>&nbsp;</div>
        <ul class="col-xs-12 lesson-detail-ico">
          <li class="col-sm-3">Chat</li>
          <li class="col-sm-3">Quiz</li>
          <li class="col-sm-3">Ask a Question</li>
          <li class="col-sm-3">Remarks</li>
        </ul>
      </center>
      <div class="col-xs-12 table-responsive">
      	<table class="table table-bordered">
      		<thead>
      		<tr>
      			<th>@lang('core.l_Title')</th>
      			<td>{!!\Helper::getLangValue($lesson->name)!!}</td>
      		</tr>
      		<tr>
      			<th>@lang('core.l_Description')</th>
      			<td>{!!\Helper::getLangValue($lesson->description)!!}</td>
      		</tr>
      		<tr>
      			<th>@lang('core.l_Video')</th>
      			<td>@if($lesson->subtype_id==1)
				<div class="embed-responsive embed-responsive-16by9 thumbnail">
					<video style="width: 100%" controls class="player_audio">
						<source  src="{!!\URL::To('/public/uploads/lesson/'.$lesson->video)!!}" type="video/mp4">
							Your browser does not support the video element.
						</video>
					</div>
				@else
								<?php
                              $url = $lesson->file_url;
                              preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
                              $id = $matches[1];
                              ?>
                              <div class="text-center">
                              <object width="500" height="350" class="player_audio" data="http://www.youtube.com/v/{!!$id!!}" type="application/x-shockwave-flash"><param name="src" value="{!!$lesson->file_url!!}" /></object>  </div>
				@endif</td>
      		</tr>
      		</thead>
      	</table>
      </div>
    </div>
  </div>
</div>
@include('layout.footer')