@include('layout.header')
<div class="col-xs-12 no_pad user_dashboard">
    <section class="color_header">
      <div class="container">
        <h4>Instructor Dashboard
          <a href="">Create new <i class="fa fa-angle-down"></i></a>
        </h4>
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#courses">Courses</a></li>
          <li><a data-toggle="tab" href="#insights">Insights</a></li>
        </ul>
      </div>
    </section>
    <section class="dashboard_block">
      <div class="container">
        <div class="tab-content">
          <div id="courses" class="courses_block tab-pane fade in active">
            <div class="col-xs-12 no_pad mag_t_20">
              <div class="search_course">
                <input type="text" placeholder="Search your courses">
                <button><i class="fa fa-search"></i></button>
              </div>
              <div class="sort_by">
                Sort By
                <select>
                  <option>Date created - Newest</option>
                  <option>Date created - old</option>
                </select>
              </div>
            </div>
            <div class="course_lists">
              <div class="course_list">
                <div class="course_detail col-md-6 col-sm-6 col-xs-12">
                  <img src="{!! asset('sdream/img/data_image.jpg')!!}">
                  <div class="course_space">
                    <h5>HTML,CSS</h5>
                    <div class="cource_pub_name">Patrick Stanly</div>
                  </div>
                  <div class="draft_block">
                    <b class="col-xs-6 no_pad">DRAFT</b>
                    <div class="col-xs-6 no_pad small_text">Free - Public</div>
                  </div>
                </div>
                <div class="course_count col-md-6 col-sm-6 col-xs-12">
                  <div class="small_text">Published Curriculum Items</div>
                  <b>1</b>
                  <div class="small_text">Total (550 Max)</div>
                  <b>0</b>
                </div>
              </div>
              <div class="course_list">
                <div class="course_detail col-md-6 col-sm-6 col-xs-12">
                  <img src="{!! asset('sdream/img/data_image.jpg')!!}">
                  <div class="course_space">
                    <h5>Education</h5>
                    <div class="cource_pub_name">John charles</div>
                  </div>
                  <div class="draft_block">
                    <b class="col-xs-6 no_pad">DRAFT</b>
                    <div class="col-xs-6 no_pad small_text">Free - Public</div>
                  </div>
                </div>
                <div class="course_count col-md-6 col-sm-6 col-xs-12">
                  <div class="small_text">Published Curriculum Items</div>
                  <b>5</b>
                  <div class="small_text">Total (550 Max)</div>
                  <b>0</b>
                </div>
              </div>
              <div class="course_list">
                <div class="course_detail col-md-6 col-sm-6 col-xs-12">
                  <img src="{!! asset('sdream/img/data_image.jpg')!!}">
                  <div class="course_space">
                    <h5>HTML,CSS</h5>
                    <div class="cource_pub_name">Patrick Stanly</div>
                  </div>
                  <div class="draft_block">
                    <b class="col-xs-6 no_pad">DRAFT</b>
                    <div class="col-xs-6 no_pad small_text">Free - Public</div>
                  </div>
                </div>
                <div class="course_count col-md-6 col-sm-6 col-xs-12">
                  <div class="small_text">Published Curriculum Items</div>
                  <b>2</b>
                  <div class="small_text">Total (550 Max)</div>
                  <b>0</b>
                </div>
              </div>
            </div>
          </div>
          <div id="insights" class="tab-pane fade">
            <h3>Menu 1</h3>
            <p>Some content in menu 1.</p>
          </div>
        </div>
      </div>
    </section>
    <section class="text-center instruct_resource">
      <h5>Have questions? Here are some of our most popular instructor resources.</h5>
      <a href="">Create an Engaging Course</a>
      <a href="">Get Started with Video</a>
      <a href="">Build Your Audience</a>
    </section>
    <section class="step_process">
      <div class="container text-center">
        <div class="col-md-05 col-sm-4 col-xs-12">
          <span class="dash_icon dash_icon_1"></span>
          <b>Create a test video</b>
          <p>Get feedback from Udemy video experts.</p>
        </div>
        <div class="col-md-05 col-sm-4 col-xs-12">
          <span class="dash_icon dash_icon_2"></span>
          <b>SMTC Community</b>
          <p>Learn more from our instructor community.</p>
        </div>
        <div class="col-md-05 col-sm-4 col-xs-12">
          <span class="dash_icon dash_icon_3"></span>
          <b>Teach hub articles</b>
          <p>Find helpful articles on every aspect of teaching with us.</p>
        </div>
        <div class="col-md-05 col-md-ffset-0 col-sm-offset-2 col-sm-4 col-xs-12">
          <span class="dash_icon dash_icon_4"></span>
          <b>SMTC Insight courses</b>
          <p>Use our courses for instructors to guide you through the process.</p>
        </div>
        <div class="col-md-05 col-sm-4 col-xs-12">
          <span class="dash_icon dash_icon_5"></span>
          <b>Support team</b>
          <p>can't find an answer? Our support team is happy to help.</p>
        </div>
      </div>
    </section>
</div>
@include('layout.footer')