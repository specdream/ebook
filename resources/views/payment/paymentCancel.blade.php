@include('layout.header')
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <style type="text/css">
    .invoice-title h2, .invoice-title h3 {
      display: inline-block;
  }

  .table > tbody > tr > .no-line {
      border-top: none;
  }

  .table > thead > tr > .no-line {
      border-bottom: none;
  }

  .table > tbody > tr > .thick-line {
      border-top: 2px solid;
  }
  </style>
  <div class="container">
      <div class="row">
          <div class="col-xs-12">
          <div class="invoice-title">
            <h2>Payment Cancelled</h2><h3 class="pull-right"></h3>
          </div>
          <hr>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title"><strong>Message</strong></h3>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
             {{ $message }}
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
@include('layout.footer')