@include('layout.header')
<div class="col-xs-12 question_page margin_tb_30">
	<div class="container no_pad">
		<section class="col-md-8 col-sm-8 col-xs-12 right_qus">
			<b>Question 2 of 30</b>
			<p>Let the least number of six digits, 4, 6, 10</p>
			<ul>
				<li><label for="c_1"><input id="c_1" name="answer" type="radio">A: 6</label></li>
				<li><label for="c_2"><input id="c_2" name="answer" type="radio">B: 4</label></li>
				<li><label for="c_3"><input id="c_3" name="answer" type="radio">C: 5</label></li>
				<li><label for="c_4"><input id="c_4" name="answer" type="radio">D: </label></li>
			</ul>
		</section>
		<section class="col-md-4 col-sm-4 col-xs-12 left_qus">
			<div class="timmer">
				<div class="col-xs-4"><b>1</b><br>Hour</div>
				<div class="col-xs-4"><b>29</b><br>Minutes</div>
				<div class="col-xs-4"><b>29</b><br>Seconds</div>
			</div>
			<div class="box_hint col-xs-12 no_pad">
				<div class="col-xs-6"><span class="small_box green_clr"></span> Attempted</div>
				<div class="col-xs-6"><span class="small_box"></span> Unattempted</div>
				<div class="col-xs-6"><span class="small_box orange_clr"></span> Current</div>
			</div>
			<div class="qustion_no">
				<div><span class="smal_box green_clr">1</span></div>
				<div><span class="smal_box green_clr">2</span></div>
				<div><span class="smal_box green_clr">3</span></div>
				<div><span class="smal_box green_clr">4</span></div>
				<div><span class="smal_box orange_clr">5</span></div>
				<div><span class="smal_box">6</span></div>
				<div><span class="smal_box">7</span></div>
				<div><span class="smal_box">8</span></div>
				<div><span class="smal_box">9</span></div>
				<div><span class="smal_box">10</span></div>
				<div><span class="smal_box">11</span></div>
				<div><span class="smal_box">12</span></div>
				<div><span class="smal_box">13</span></div>
				<div><span class="smal_box">14</span></div>
				<div><span class="smal_box">15</span></div>
				<div><span class="smal_box green_clr">1</span></div>
				<div><span class="smal_box green_clr">2</span></div>
				<div><span class="smal_box green_clr">3</span></div>
				<div><span class="smal_box green_clr">4</span></div>
				<div><span class="smal_box orange_clr">5</span></div>
				<div><span class="smal_box">6</span></div>
				<div><span class="smal_box">7</span></div>
				<div><span class="smal_box">8</span></div>
				<div><span class="smal_box">9</span></div>
				<div><span class="smal_box">10</span></div>
				<div><span class="smal_box">11</span></div>
				<div><span class="smal_box">12</span></div>
				<div><span class="smal_box">13</span></div>
				<div><span class="smal_box">14</span></div>
				<div><span class="smal_box">15</span></div>
				<div><span class="smal_box green_clr">1</span></div>
				<div><span class="smal_box green_clr">2</span></div>
				<div><span class="smal_box green_clr">3</span></div>
				<div><span class="smal_box green_clr">4</span></div>
				<div><span class="smal_box orange_clr">5</span></div>
				<div><span class="smal_box">6</span></div>
				<div><span class="smal_box">7</span></div>
				<div><span class="smal_box">8</span></div>
				<div><span class="smal_box">9</span></div>
				<div><span class="smal_box">10</span></div>
				<div><span class="smal_box">11</span></div>
				<div><span class="smal_box">12</span></div>
				<div><span class="smal_box">13</span></div>
				<div><span class="smal_box">14</span></div>
				<div><span class="smal_box">15</span></div>
				<div><span class="smal_box green_clr">1</span></div>
				<div><span class="smal_box green_clr">2</span></div>
				<div><span class="smal_box green_clr">3</span></div>
				<div><span class="smal_box green_clr">4</span></div>
				<div><span class="smal_box orange_clr">5</span></div>
				<div><span class="smal_box">6</span></div>
				<div><span class="smal_box">7</span></div>
				<div><span class="smal_box">8</span></div>
				<div><span class="smal_box">9</span></div>
				<div><span class="smal_box">10</span></div>
				<div><span class="smal_box">11</span></div>
				<div><span class="smal_box">12</span></div>
				<div><span class="smal_box">13</span></div>
				<div><span class="smal_box">14</span></div>
				<div><span class="smal_box">15</span></div>
			</div>
		</section>
	</div>
</div>
@include('layout.footer')