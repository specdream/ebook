@include('layout.header')

<div class="container">
	<section id="breadcrumbs">
		<div class="breadcrumbs"><a href="{!!URL::To('/')!!}">Home</a> <i class="fa fa-angle-right"></i> <strong>Privacy Policy</strong></div>
	</section>
	<article class="item">	
		<header>
			<button class="print-btn"  onclick='printDiv();'>Print this page</button>
			<h1 class="title">Privacy Policy</h1>
		</header>
		<div class="static_page clearfix printTable" id="DivIdToPrint">

			<h3>What information do we collect?</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>

			<h3>What do we use your information for?</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
			<ul>
				<li>To personalize your experience<br> (your information helps us to better respond to your individual needs)</li>
				<li>To improve our website<br> (we continually strive to improve our website offerings based on the information and feedback we receive from you)</li>
				<li>To improve customer service<br> (your information helps us to more effectively respond to your customer service requests and support needs)</li>
				<li>To process transactions Your information, whether public or private, will not be sold, exchanged, transferred, or given to any other company for any reason whatsoever, without your consent, other than for the express purpose of delivering the purchased product or service requested.</li>
				<li>To administer a contest, promotion, survey or other site feature</li>
				<li>To send periodic emails<br> The email address you provide for order processing, may be used to send you information and updates pertaining to your order, in addition to receiving occasional company news, updates, related product or service information, etc.</li>
			</ul>
		</div>			
	</article>
</div>
<script>

	function printDiv() {
		var divToPrint = document.getElementById('DivIdToPrint');
		var newWin = window.open('', 'Print-Window');
		newWin.document.open();
		newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
		newWin.document.close();
		setTimeout(function() {
			newWin.close();
		}, 10);
	}

</script>
@include('layout.footer')