@include('layout.header')
    <div class="container">
<div class="col-xs-12 col-md-12 col-sm-12 lesson-detail-head">
<h2 class="text-center">Key verification</h2>
<div class="lesson-detail-card"> 
    <div class="col-md-6">
      <div class="pricing hover-effect">
        <div class="pricing-head">
          <h3> Course Details </h3>
        </div>
        <ul class="pricing-content list-unstyled">
          <li>
           <div class="detail-title" width="35%"><i class="fa fa-pencil-square-o"></i>Course Name:</div>
            <div class="detail-ans" width="55%">Angularjs</div>
          </li>
          <li>
              <div class="detail-title"><i class="fa fa-certificate"></i>Course Catogory:</div>
              <div class="detail-ans">Programing</div>
          </li>
        </ul>
         <div class=" hover-effect">
        <div class="pricing-head">
          <h3>Lesson Details </h3>
        </div>
         <ul class="pricing-content list-unstyled">
         <li>
             <div class="detail-title"><i class="fa fa-sort-numeric-asc"></i>Lesson No:</div>
             <div class="detail-ans">789652</div>
           </li>
           <li>
             <div class="detail-title"><i class="fa fa-files-o"></i>Lesson Name:</div>
             <div class="detail-ans">Angularjs</div>
           </li>
           <li>
           <tr>
             <td>
               <div class="checkbox checkbox-success ">
                        <input id="checkbox3" type="checkbox">
                        <label for="checkbox3">
                            Verified
                        </label>
                    </div>
             </td>
           </tr>
           </li>
         </ul>

   
        </div>
        <div class="pricing-footer">
          <p>
             
          </p>
          <a href="javascript:;" class="btn yellow-crusta">
          Go
          </a>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="pricing hover-effect">
        <div class="pricing-head">
          <h3>Learning Details</h3>
          <h4>        
      <div class="profile-header-container">   
        <div class="profile-header-img">
                <img class="img-circle" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" />
                <!-- badge -->
                <div class="rank-label-container">
                    <span class="label label-default rank-label">Super mario</span>
                </div>
            </div>
        </div> 
          </h4>
        </div>
        <ul class="pricing-content list-unstyled">
          <li>
           <div class="detail-title" "><i class="fa fa-user"></i>Name:</div>
            <div class="detail-ans" >SDream</div>
          </li>
          <li>
          <div class="detail-title" ><i class="fa fa-id-card"></i>Id:</div>
            <div class="detail-ans" ">549984</div>
          </li>
          <li>
           <div class="detail-title"><i class="fa fa-university"></i>University :</div>
            <div class="detail-ans">Anna</div>
          </li>
  
        </ul>

      </div>
    </div>
</div>
</div>
</div>


<style type="text/css">
/*
    .table-striped>tbody>tr:nth-child(even)>td, .table-striped>tbody>tr:nth-child(even)>th {
        background-color: #ececec;
    }
    .table-striped{color:#000}
    .table-striped i{padding-right:15px;color:#0a2e3b;font-size: 16px;}
    .course-head td{background-color:#113f50;color:#fff;font-weight:bold;}
    .table-striped{box-shadow: -3px 1px 21px 1px rgb(214, 212, 214);*/

.profile-header-container{
    margin: 0 auto;
    text-align: center;
}
.detail-title i{ margin-right:5px; color:#002e3b;}
.profile-header-img {
    padding: 6px;
}

.profile-header-img > img.img-circle {
    width: 120px;
    height: 120px;
    border: 2px solid #51D2B7;
}

.profile-header {
    margin-top: 43px;
}

/**
 * Ranking component
 */
.rank-label-container {
    margin-top: -19px;
    /* z-index: 1000; */
    text-align: center;
}

.label.label-default.rank-label {
    background-color: rgb(10, 46, 59);
    padding: 5px 10px 5px 10px;
    border-radius: 27px;
    margin: 10px 100px;
}

</style>
@include('layout.footer')