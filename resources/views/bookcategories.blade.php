@include('layout.header')
<div class="col-xs-12 col-sm-12 col-md-12 brows-page">
<div class="header-container">
<div class="header-url">
<ul>
<li><a href="{!!URL::To('/')!!}">Home</a></li>
<li><a href="">Browse Categories</a></li>
</ul>
</div>
		<div class="brows-cat">
			<h1>Browse Categories</h1>
		</div>
	<section class="brows-book-catogories">

		<div class="list-book-cat">
			<div class="book-catog">
			<button class="accordion-book-cat">Biography & Memoir</button>
				<div class="panel-book ">
				<h2>Biography & Memoir</h2>
				<ul>
					<li><a href="javascript:void(0)">Artists, Architects & Photographers</a></li>
					<li><a href="javascript:void(0)">Business</a></li>
					<li><a href="javascript:void(0)">Composers & Musicians</a></li>
					<li><a href="javascript:void(0)">Entertainment & Performing Arts</a></li>
					<li><a href="javascript:void(0)">Historical</a></li>
					<li><a href="javascript:void(0)">Literary</a></li>
					<li><a href="javascript:void(0)">Philosophers</a></li>
					<li><a href="javascript:void(0)">Political</a></li>
					<li><a href="javascript:void(0)">Reference</a></li>
					<li><a href="javascript:void(0)">Religious</a></li>
				</ul>
			</div>
			</div>
		</div>

				<div class="list-book-cat">
			<div class="book-catog">
			<button class="accordion-book-cat">Artists, Architects & Photographers</button>
				<div class="panel-book ">
				<h2>Artists, Architects & Photographers</h2>
				<ul>
					<li><a href="javascript:void(0)">Artists, Architects & Photographers</a></li>
					<li><a href="javascript:void(0)">Business</a></li>
					<li><a href="javascript:void(0)">Composers & Musicians</a></li>
					<li><a href="javascript:void(0)">Entertainment & Performing Arts</a></li>
					<li><a href="javascript:void(0)">Historical</a></li>
					<li><a href="javascript:void(0)">Literary</a></li>
					<li><a href="javascript:void(0)">Philosophers</a></li>
					<li><a href="javascript:void(0)">Political</a></li>
					<li><a href="javascript:void(0)">Reference</a></li>
					<li><a href="javascript:void(0)">Religious</a></li>
				</ul>
			</div>
			</div>
		</div>
				<div class="list-book-cat">
			<div class="book-catog">
			<button class="accordion-book-cat">Biography & Memoir</button>
				<div class="panel-book ">
				<h2>Biography & Memoir</h2>
				<ul>
					<li><a href="javascript:void(0)">Artists, Architects & Photographers</a></li>
					<li><a href="javascript:void(0)">Business</a></li>
					<li><a href="javascript:void(0)">Composers & Musicians</a></li>
					<li><a href="javascript:void(0)">Entertainment & Performing Arts</a></li>
					<li><a href="javascript:void(0)">Historical</a></li>
					<li><a href="javascript:void(0)">Literary</a></li>
					<li><a href="javascript:void(0)">Philosophers</a></li>
					<li><a href="javascript:void(0)">Political</a></li>
					<li><a href="javascript:void(0)">Reference</a></li>
					<li><a href="javascript:void(0)">Religious</a></li>
				</ul>
			</div>
			</div>
		</div>

		<div class="list-book-cat">
			<div class="book-catog">
			<button class="accordion-book-cat">Fiction & Literature</button>
				<div class="panel-book ">
				<h2>Fiction & Literature</h2>
				<ul>
					<li><a href="javascript:void(0)">Artists, Architects & Photographers</a></li>
					<li><a href="javascript:void(0)">Business</a></li>
					<li><a href="javascript:void(0)">Composers & Musicians</a></li>
					<li><a href="javascript:void(0)">Entertainment & Performing Arts</a></li>
					<li><a href="javascript:void(0)">Historical</a></li>
					<li><a href="javascript:void(0)">Literary</a></li>
					<li><a href="javascript:void(0)">Philosophers</a></li>
					<li><a href="javascript:void(0)">Political</a></li>
					<li><a href="javascript:void(0)">Reference</a></li>
					<li><a href="javascript:void(0)">Religious</a></li>

				</ul>
			</div>
			</div>
		</div>
		<div class="list-book-cat">
			<div class="book-catog">
			<button class="accordion-book-cat">Mystery & Suspense</button>
				<div class="panel-book ">
				<h2>Mystery & Suspense</h2>
				<ul>
					<li><a href="javascript:void(0)">Artists, Architects & Photographers</a></li>
					<li><a href="javascript:void(0)">Business</a></li>
					<li><a href="javascript:void(0)">Composers & Musicians</a></li>
					<li><a href="javascript:void(0)">Entertainment & Performing Arts</a></li>
					<li><a href="javascript:void(0)">Historical</a></li>
					<li><a href="javascript:void(0)">Literary</a></li>
					<li><a href="javascript:void(0)">Philosophers</a></li>
					<li><a href="javascript:void(0)">Political</a></li>
					<li><a href="javascript:void(0)">Reference</a></li>
					<li><a href="javascript:void(0)">Religious</a></li>
					<li><a href="javascript:void(0)">Literary</a></li>
					<li><a href="javascript:void(0)">Philosophers</a></li>
					<li><a href="javascript:void(0)">Political</a></li>
					<li><a href="javascript:void(0)">Reference</a></li>
					<li><a href="javascript:void(0)">Religious</a></li>
				</ul>
			</div>
			</div>
		</div>
		<div class="list-book-cat">
			<div class="book-catog">
			<button class="accordion-book-cat">Business & Finance</button>
				<div class="panel-book ">
				<h2>Business & Finance</h2>
				<ul>
					<li><a href="javascript:void(0)">Artists, Architects & Photographers</a></li>
					<li><a href="javascript:void(0)">Business</a></li>
					<li><a href="javascript:void(0)">Composers & Musicians</a></li>
					<li><a href="javascript:void(0)">Entertainment & Performing Arts</a></li>
					<li><a href="javascript:void(0)">Historical</a></li>
					<li><a href="javascript:void(0)">Literary</a></li>
					<li><a href="javascript:void(0)">Philosophers</a></li>
					<li><a href="javascript:void(0)">Political</a></li>
					<li><a href="javascript:void(0)">Reference</a></li>
					<li><a href="javascript:void(0)">Religious</a></li>
				</ul>
			</div>
			</div>
		</div>
		<div class="list-book-cat">
			<div class="book-catog">
			<button class="accordion-book-cat">Comics & Graphic Novels</button>
				<div class="panel-book ">
				<h2>Comics & Graphic Novels</h2>
				<ul>
					<li><a href="javascript:void(0)">Artists, Architects & Photographers</a></li>
					<li><a href="javascript:void(0)">Business</a></li>
					<li><a href="javascript:void(0)">Composers & Musicians</a></li>
					<li><a href="javascript:void(0)">Entertainment & Performing Arts</a></li>
					<li><a href="javascript:void(0)">Historical</a></li>
					<li><a href="javascript:void(0)">Literary</a></li>
					<li><a href="javascript:void(0)">Philosophers</a></li>
					<li><a href="javascript:void(0)">Political</a></li>
					<li><a href="javascript:void(0)">Reference</a></li>
					<li><a href="javascript:void(0)">Religious</a></li>
					<li><a href="javascript:void(0)">Literary</a></li>
					<li><a href="javascript:void(0)">Philosophers</a></li>
					<li><a href="javascript:void(0)">Political</a></li>
					<li><a href="javascript:void(0)">Reference</a></li>
					<li><a href="javascript:void(0)">Religious</a></li>
				</ul>
			</div>
			</div>
		</div>
		<div class="list-book-cat">
			<div class="book-catog">
			<button class="accordion-book-cat">Science Fiction & Fantasy</button>
				<div class="panel-book ">
				<h2>Science Fiction & Fantasy</h2>
				<ul>
					<li><a href="javascript:void(0)">Artists, Architects & Photographers</a></li>
					<li><a href="javascript:void(0)">Business</a></li>
					<li><a href="javascript:void(0)">Composers & Musicians</a></li>
					<li><a href="javascript:void(0)">Entertainment & Performing Arts</a></li>
					<li><a href="javascript:void(0)">Historical</a></li>
					<li><a href="javascript:void(0)">Literary</a></li>
					<li><a href="javascript:void(0)">Philosophers</a></li>
					<li><a href="javascript:void(0)">Political</a></li>
					<li><a href="javascript:void(0)">Reference</a></li>
					<li><a href="javascript:void(0)">Religious</a></li>
				</ul>
			</div>
			</div>
		</div>
	</section>
</div>
</div>
<style type="text/css">
	.brows-page{background-color:#ececec;}
	.list-book-cat{width:100%;}
	.book-catog{    border-radius: 6px;background: #fff;overflow: hidden;margin: 0 1.5% 1.9em;display: inline-block;width: 100%;}
	.brows-book-catogories{padding: 30px 30px 0;}
	.brows-cat{width:100%;float:left;}

.accordion-book-cat{background-color: #eee;color: #444;cursor: pointer;padding: 10px 10px;width: 100%;border: none;text-align: left;outline: none;font-size: 15px;transition: 0.4s;
}

.active, .accordion-book-cat:hover {background-color: #ccc;}

.accordion-book-cat:after {content: '\002B';color: #777;font-weight: bold;float: right;margin-left: 5px;}
.active:after {
    content: "\2212";
}

.panel-div {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
    margin-bottom:0;
    padding-bottom:0;
}
</style>
<script type="text/javascript">

       var width = $(window).width();
$( window ).resize(function() {
if ($(window).width()  < 569) {
   $(".panel-book").addClass("panel-div");
}
else {
   $(".panel-book").removeClass("panel-div");
}
       
 }); 

</script>
<script>
var acc = document.getElementsByClassName("accordion-book-cat");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}

</script>
@include('layout.footer')