@include('layout.header')

   <link href="{!! asset('sdream/bookFlip/css/slick-theme.css') !!}" rel="stylesheet">
   <link href="{!! asset('sdream/bookFlip/css/slick.css') !!}" rel="stylesheet">


  <style type="text/css">
    .slider {
        width:80%;
        margin: 100px auto;
    }

    .slick-slide {
      margin: 0px 20px;
    }

    .slick-slide img {
      width: 100%;
    }
/*    .slick-prev.slick-arrow:before{
        content: "\f104" !important;
    }*/

    .slick-prev:before,
    .slick-next:before {
      color: black;
    }

    .slick-slide.slick-current.slick-active{width:500px;}
    .slick-slide {
      transition: all ease-in-out .3s;
      opacity: .2;
    }
    
    .slick-active {
      opacity: .5;
    }

    .slick-current {
      opacity: 1;
    }
  </style>
  <section class="lazy slider" data-sizes="50vw">
    <div>
      <img data-lazy="{!! asset('sdream/bookFlip/img/1-large.jpg') !!}" data-srcset="{!! asset('sdream/bookFlip/img/1-large.jpg') !!}" data-sizes="100vw">
    </div>
    <div>
      <img data-lazy="{!! asset('sdream/bookFlip/img/2-large.jpg') !!}" data-srcset="{!! asset('sdream/bookFlip/img/2-large.jpg') !!}" data-sizes="100vw">
    </div>
    <div>
      <img data-lazy="{!! asset('sdream/bookFlip/img/3-large.jpg') !!}"  data-srcset="{!! asset('sdream/bookFlip/img/3-large.jpg') !!}" data-sizes="100vw">
    </div>
    <div>
      <img data-lazy="{!! asset('sdream/bookFlip/img/4-large.jpg') !!}"  data-srcset="{!! asset('sdream/bookFlip/img/4-large.jpg') !!}" data-sizes="100vw">
    </div>
    <div>
      <img data-lazy="{!! asset('sdream/bookFlip/img/5-large.jpg') !!}"  data-srcset="{!! asset('sdream/bookFlip/img/5-large.jpg') !!}" data-sizes="100vw">
    </div>
    <div>
      <!-- this slide should inherit the sizes attr from the parent slider -->
      <img data-lazy="{!! asset('sdream/bookFlip/img/6-large.jpg') !!}"  data-srcset="{!! asset('sdream/bookFlip/img/6-large.jpg') !!}">
    </div>
        <div>
      <img data-lazy="{!! asset('sdream/bookFlip/img/7-large.jpg') !!}"  data-srcset="{!! asset('sdream/bookFlip/img/7-large.jpg') !!}" data-sizes="100vw">
    </div>
        <div>
      <img data-lazy="{!! asset('sdream/bookFlip/img/8-large.jpg') !!}"  data-srcset="{!! asset('sdream/bookFlip/img/8-large.jpg') !!}" data-sizes="100vw">
    </div>
        <div>
      <img data-lazy="{!! asset('sdream/bookFlip/img/9-large.jpg') !!}"  data-srcset="{!! asset('sdream/bookFlip/img/9-large.jpg') !!}" data-sizes="100vw">
    </div>
        <div>
      <img data-lazy="{!! asset('sdream/bookFlip/img/10-large.jpg') !!}"  data-srcset="{!! asset('sdream/bookFlip/img/10-large.jpg') !!}" data-sizes="100vw">
    </div>
        <div>
      <img data-lazy="{!! asset('sdream/bookFlip/img/10-large.jpg') !!}"  data-srcset="{!! asset('sdream/bookFlip/img/11-large.jpg') !!}" data-sizes="100vw">
    </div>
        <div>
      <img data-lazy="{!! asset('sdream/bookFlip/img/12-large.jpg') !!}"  data-srcset="{!! asset('sdream/bookFlip/img/12-large.jpg') !!}" data-sizes="100vw">
    </div>

  </section>
   <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
     <script type="text/javascript" src="{!! asset('sdream/bookFlip/js/slick.js') !!}"></script>



   <script type="text/javascript">


    $(document).on('ready', function() {
     
      $(".lazy").slick({
        lazyLoad: 'ondemand',
        rtl: false,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        adaptiveHeight: true
      });
    });
</script>



@include('layout.footer')
