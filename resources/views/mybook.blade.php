@include('layout.header')  
 <section class="col-xs-12 col-sm-12 col-md-12 no-pad mybook-section">
 	<div class="header-container">
 		<div class="pull-left">
 			<h3>My Book</h3>
 		</div>
 		<div class="pull-right">
 			<!-- <ul class="nav navbar-nav">
 				<li>
 					<input type="hidden" name="coursetitle_id" id="coursetitle_id">
 					<div class="search_box">
 						<input type="text" name="coursetitle" id="coursetitle"  placeholder="Search My Books" >
 						<button type="submit">
 							<span class="fa fa-search"></span>
 						</button>
 					</div>
 				</li>
 			</ul> -->
 		</div>
 	</div>
 </section>

    	<section class="col-xs-12 col-sm-12 col-md-12 no-pad">
    		<div class="header-container">
    			<div class="library-items">
    				
    			</div>
    			<div class="pull-right">
    				
    				</div>
    			</div>
    	</section>

    	<section class=" col-xs-12 col-sm-12 col-md-12 no-pad mybook-list" >
    		<div class="header-container">
    		
    			<ul class="book-list">
 <?php 
 $i = '0';
            foreach($books as $value){ ?>
    				<li class="col-xs-12 col-sm-4 col-md-3">
    				<div style="width: 18rem;float:left;">
    					<div class="images">
              <img src="{!! asset('public/uploads/books_banner') . '/'.  $value['id'] . '/'.  $value['banner'] !!}">
    					</div>
    					<div class="mybook-content">
    					<div class="book-price">
              <a href="javascript:void(0)">$ {{ $value['price'] }}</a> 
            <span class="dropdown">
              <div class="more-menu  dropdown-toggle" id="menu1" type="button" data-toggle="dropdown">
                </div>
                <ul class="dropdown-menu drop-mybook" role="menu" aria-labelledby="menu1">
                  <li data-toggle="modal" data-target="#book-review" ><a role="menuitem" tabindex="-1" href="#">Review</a></li>
                </ul>
              </span>
    					</div>
    					
    					<div id="" class="drop-mybook"> 
    						<ul>
    							<li>Mark as Unread<span class="more-menu"></span></li>
    							<li>Write a  Review</li>
                  <li>Download</li>
                  <li>Move to Archive</li>
    						</ul>
    					</div>
    					
    					</div>
    						<div class="book-title">{{ $value['title'] }} </div>
    					<div class="book-para">{!! $value['summary'] !!}</div>
    					<div class="book-auth-name">{{ $value['author'] }}</div>
    					</div>
    				</li>
    					 <?php $i++; } ?>
    			</ul>
    		
    		</div> 
        <div class="search-pagination" id="pagination">
  <?php 
  $c_url = $_SERVER['REQUEST_URI'];

  echo str_replace('/?', '?', $books->appends(Request::except('page'))->render()) ?>
</div>
    	</section>
    
    <div class="modal fade" id="book-review" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Review your book </h4>
        </div>
        <div class="modal-body">
          <div class="modal-body stud-review">
        <div class="col-xs-12 com-sm-4 col-md-3">
    <section class="book-metadata">
                <div class="book-cover-container" translate="no">
                    <img class="book-cover" src="//kbimages1-a.akamaihd.net/f9263eed-0752-410d-8705-dfa15238d966/166/300/False/defeat-is-an-orphan.jpg" alt="book cover">
                </div>

                    <p class="sub-section-title item-title">Defeat Is an Orphan</p>
                    <p class="book-author">by <span translate="no">Myra Macdonald</span></p>
            </section>
    </div>
    <div class="col-xs-12 col-sm-9 col-md-8">
    <section class="form-header ">
            <h2 class="write-form-title" style="display: none;">Share your thoughts</h2>
            <h2 class="complete-form-title">Complete your review</h2>
            <p class="sub-title">Tell readers what you thought by rating and reviewing this book.</p>
        </section>

        <section class="form-section rating-row">
        <div class="book-review">
               <h3 id="rateIt" class="section-title rating" style="display: none;">Rate it *</h3>
                <h3 id="ratedIt" class="section-title rating">You Rated it *</h3>

      <div class="stars-read-only star-review-sect">
        <i class="fa fa-star color_star"></i>
        <i class="fa fa-star color_star"></i>
        <i class="fa fa-star color_star"></i>
        <i class="fa fa-star color_star"></i>
        <i class="fa fa-star"></i>
        <span>(28)</span>
      </div>
           </div>

      <section class="form-section review-row">
        <section class="review-header">
          <h3 class="section-title">Add a review <span class="add-review-required">* Required</span></h3>

          <div class="col-sm-12 col-xs-12 no-pad review-comment-section">
            <label for="title" class="control-label">Review*</label>
            <textarea class="form-control wirte-review-inp" rows="5" id="comment" placeholder="Tell us if you enjoyed the story. What was the author's style like? What did you like best and the least? Who would you recommend this book for?" title="Tell us if you enjoyed the story. What was the author's style like? What did you like best and the least? Who would you recommend this book for?"></textarea>
            <p class="pull-right">(0) 50 characters minimum</p>
            <span id="title_msg" style="color: #F00;"></span>
          </div>

          <div class="col-sm-12 no-pad input-div">
            <label for="title" class=" control-label">Title*</label>
            <input class="form-control keyvalid" required="" name="title" type="text" id="title" placeholder="Briefly give your overall impression.">
            <span id="title_msg" style="color: #F00;"></span>
          </div>


          <div class="col-sm-12 no-pad input-div">
            <label for="title" class=" control-label">Display Name*</label>
            <input class="form-control keyvalid" required="" name="title" type="text" id="title" placeholder="e.g. Jane Smith">
            <span id="title_msg" style="color: #F00;"></span>
          </div>

        </section>

<!--         <section class="submition-sec">
        <div class="sub-btns">
      
       
        </div>
        </section> -->

      </section>

    </div>
  </section>
        </div>
        </div>
        <div class="modal-footer">
        <div class="sub-btns">
          <button type="button" class="btn btn-normal btn-review-cancel" data-dismiss="modal">Cancel</button>
       <button class="btn btn-normal btn-review-submit">Submit</button>
          </div>

        </div>
      </div>
    </div>
  </div>


<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0px";

}


$(document).ready(function(){
    $('#mydropdown').change(function(){
      $selected_value=$('#mydropdown option:selected').text();
      $('#result').text($selected_value);
    });
  });

</script>
@include('layout.footer')
