@include('layout.myacc_header')
<div class="header-container">
<section class="purchase-history" style="background-color:#f8f8f8;margin-bottom: 40px;">
<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
	<div class="purchase-info">
		<h2>Purchase History</h2>
		<p>Below is a complete record of your purchases with Khawarizm. Even if you delete a book from your library, you'll find a record of it here. If you have questions or need help with your purchases, please <span><a href="">contact us</a></span> or refer to our  <span><a href="">Terms of Sale.</a></span>

</p>
<p>You haven't purchased any items yet.</p>
	</div>
	
    
   
    
        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default  panel-history">
              <div class="panel-heading">
                <div class="row">
                  <div class="col col-xs-6">
                    <h3 class="panel-title">Purchase History</h3>
                  </div>
                  <div class="col-sm-4 col-xs-6 pull-right">
                   <!--  <button type="button" class="btn btn-sm btn-primary btn-create">Create New</button> -->
                   <input class="form-control" id="purchase-input" type="text" placeholder="Search..">
                  </div>
                </div>
              </div>
              <div class="panel-body">
                <table class="table table-striped table-bordered table-purchase">
                  <thead>
                    <tr>
                        
                        <th class="hidden-xs">ID</th>
                        <th>Title</th>
                        <th>Author</th>
                        <th class="text-center">Action</th>
                    </tr> 
                  </thead>
                  <tbody id="myPurchaseTable">
                  		<?php 
                      $i = '1';
			foreach($historyData as $value) {
			?>
                  		<tr>
                            <td class="hidden-xs">{{ $i }}</td>
                            <td>{{ $value['title'] }}</td>
                            <td>{{ $value['author'] }}</td>

                            <td align="center">
                              <a data-toggle="modal" onclick="invoice('{{ $value['uniq_id'] }}')" data-target="#myModaledit" class="btn btn-default"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            </td>
                          </tr>
                    <?php $i++; } ?>

                        </tbody>
                </table>
            
              </div>
              <!-- <div class="panel-footer">
                <div class="row">
                  <div class="col col-xs-4">Page 1 of 5
                  </div>
                  <div class="col col-xs-8">
                    <ul class="pagination hidden-xs pull-right">
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li><a href="#">5</a></li>
                    </ul>
                    <ul class="pagination visible-xs pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                  </div>
                </div>
              </div> -->
            </div>

</div></div>



  <div class="modal fade" id="myModaledit" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="btnclose" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</section>

</div>

<style type="text/css">
.table-purchase{}
.panel-history .panel-footer .pagination{margin:0; }
.panel-history .panel-footer .col{line-height: 34px;height: 34px;}	
.panel-history .panel-title{margin-top:5px;}
</style>
<script type="text/javascript">
function invoice(uniq_id){
$.ajax({
  url:'<?php echo url('invoicePopup').'/' ?>'+uniq_id,
  type:'GET',
  success:function(data){
    $('#myModaledit .modal-body').html(data);
  }
})
}
$('#modal').on('hidden.bs.modal', function(){
  //remove the backdrop
  $('.modal-backdrop').remove();
})

</script>