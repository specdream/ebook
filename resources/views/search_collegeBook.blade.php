	@include('layout.header')
	<div class="col-xs-12 col-sm-12 col-md-12 student-book-section no-pad">
		<div class="header-container">
			<div class="col-xs-12 col-sm-4 col-md-3">
				<section class="book-filter-sec">
					@if(count($semester) > 0)
					<button class="book-search-accordion">Semesters</button>
					<div class="panel-search-book" style="max-height:500px;">
						<ul>
						@foreach($semester as $value)
							<li><a href="{{ url('college-book').'?semester='.$value.'&degree='.Request::get('degree') }}">{{ \Helper::semName($value) }}</a></li>
						@endforeach
						</ul>
					</div>
					@endif							
					<button class="book-search-accordion">Degrees</button>
					<div class="panel-search-book" style="max-height:500px;">
						<ul>
							@foreach($degree as $value)
							<li><a href="{{ url('college-book').'?degree='.$value.'&semester='.Request::get('semester') }}">{{ \Helper::degreeName($value) }}</a></li>
							@endforeach
						</ul>
					</div>

				</section>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9 no-pad">
					@foreach($book as $value)
					<div class="result-items">
						<div class="col-xs-5 col-sm-3">	
							<img src="{!! asset('public/uploads/college_books_banner') . '/'.  $value->id . '/'.  $value->banner !!}">
						</div>

						<div class="col-xs-7 col-sm-9 padd-left">
							<div class="search-book-content">
								<h2 class="search-book-titile">{{ $value->title }}</h2>
								<!-- <h3 class="sub-book-title">The Chronicles of St. Mary's series</h3> -->
								<div class="search-author-name">by <span>{!! \Helper::getUser($value->user_id,'name') !!}</span>
								</div>
			<!-- <div class="search-series-name">series<span>The Chronicles of St. Mary's series #1</span>
		</div> -->
		<div class="book-paragraph">
			{!! $value->summary !!}
			<p class="read-more"><a href="{{ url('college_details').'/'.$value->id }}" class="button">Read More</a></p>
		</div>
		<div class="search-book-ratings">
			<div class="stars-read-only">
				<i class="fa fa-star color_star"></i>
				<i class="fa fa-star color_star"></i>
				<i class="fa fa-star color_star"></i>
				<i class="fa fa-star color_star"></i>
				<i class="fa fa-star"></i>
				<span>(28)</span>
			</div>
		</div>
		<h3 class="book-rate-type">
			
                @if($value->price_type == 'Free') Free @else ${!!$value->price!!} @endif
            </h3>

        </div>
    </div>
</div>

@endforeach
<div class="search-pagination" id="pagination">
	<?php 
	$c_url = $_SERVER['REQUEST_URI'];

	echo str_replace('/?', '?', $book->appends(Request::except('page'))->render()) ?>
</div>
			</div>
		</div>
	</div>
	<style>

</style>

<script>
var acc = document.getElementsByClassName("book-search-accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>

	@include('layout.footer')