<div class="profile_menu">
  <section>
    <center>
      <div style="background-image:url('{!!URL::To('/')!!}/public/uploads/{!!\Auth::user()->avatar!!}')" class="profile_img"></div>
      <b title="User Name">{!!\Auth::user()->name!!}</b>
    </center>
    <a href="{!!URL::To('/')!!}/learner/profile">@lang('core.l_Profile')</a>
    <a href="{!!URL::To('/')!!}/learner/purchasedcourse">@lang('core.l_Purchased_Course')</a>
    <a href="{!!URL::To('/')!!}/learner/paymenthistory">@lang('core.Payment_History')</a>    
    <a href="{!!URL::To('/')!!}/learner/keygenerate">@lang('core.l_Key_Generation')</a>
    <a href="{!!URL::To('/')!!}/learner/finishedTest">@lang('core.l_Finished_Test')</a>  
    <a href="{!!URL::To('/')!!}/learner/completedCourse">@lang('core.l_Completed_Course')</a>  
    <a href="{!!URL::To('/')!!}/learner/writeExam">@lang('core.l_Write_your_exam_here')</a>
  </section>
</div>
