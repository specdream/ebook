<?php $lang=\Helper::language(); ?>
<?php 
$url = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
$lastsegment = array_pop($url);


?>
<style type="text/css">
	.sp-feedback-container {
		top: 320px;
		position: fixed;
		right: 0;
		z-index: 99999;
	}
	.sp-rotate {
		transform: rotate(-90deg);
		-webkit-transform: rotate(-90deg);
		-moz-transform: rotate(-90deg);
		-ms-transform: rotate(-90deg);
		-o-transform: rotate(-90deg);
		filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
		transform-origin: right bottom;
	}
	.sp-feedbackButton {
		padding: 7px 16px;
		border-radius: 5px 5px 0 0;
		border: 3px solid #0065BD;
		background-color: #0065BD;
		color: #fff;
		display: inline-block;
		margin: 0 10px;
		text-decoration: none;
		-webkit-transition: background-color 200ms,color 200ms;
		transition: background-color 200ms,color 200ms;
	}
	.rich-header a:focus, .rich-header button:focus {
		outline: thin solid #0065bd;
	}
	.sp-feedbackButton:hover {
		text-decoration: none;
		background-color: #fff;
		color: #0065BD;
	}

	.dropdown-submenu {
		position:relative;
	}
	.dropdown-submenu>.dropdown-menu {
		top:0;left:100%;
		margin-top:-6px;margin-left:-1px;
		-webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;
	}

	.dropdown-submenu > a:after {
		border-color: transparent transparent transparent #333;
		border-style: solid;
		border-width: 5px 0 5px 5px;
		content: " ";
		display: block;
		float: right;  
		height: 0;     
		margin-right: -10px;
		margin-top: 5px;
		width: 0;
	}

	.dropdown-submenu:hover>a:after {
		border-left-color:#555;
	}

	.dropdown-menu > li > a:hover, .dropdown-menu > .active > a:hover {
		text-decoration: none;
	}  
/*.jquery-loader.dontprazremove {
    position: fixed;
    z-index: 100;
    width: 100%;
    height: 100%;
    background: #fff;
}*/
.jquery-loader {
	position: absolute;
	z-index: 0;
	width: 100%;
	height: 100%;
	background: #fff;
}
.jquery-loader.dontprazremove img {
	top: 40%;
	position: absolute;
	right: 45%;
}
</style>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>E-Book</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">

	<link href="{!! asset('sdream/css/bootstrap.min.css') !!}" rel="stylesheet">
	<link href="{!! asset('sdream/css/owl.theme.default.css') !!}" rel="stylesheet">
	<link href="{!! asset('sdream/css/owl.carousel.min.css') !!}" rel="stylesheet">
	<link href="{!! asset('sdream/css/font-awesome.css') !!}" rel="stylesheet">
	<link href="{!! asset('sdream/css/jquery.autocomplete.css') !!}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Berkshire+Swash" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

	<link href="{!! asset('sdream/css/base.css') !!}" rel="stylesheet">
	<link href="{!! asset('quickadmin/css/animate.css') !!}" rel="stylesheet">  
	<link href="{!! asset('sdream/css/cart.css') !!}" rel="stylesheet">
	<input type="hidden" name="base_url" id="base_url" value="{!!URL::To('/')!!}">
	<script type="text/javascript" src="{!! asset('sdream/js/jquery.min.js') !!}"></script>

	<script type="text/javascript" src="{!! asset('sdream/js/bootstrap.min.js') !!}"></script>
	<script type="text/javascript" src="{!! asset('sdream/js/owl.carousel.js') !!}"></script>
	<script type="text/javascript" src="{!! asset('sdream/js/jquery.mousewheel.js') !!}"></script>

	<script type="text/javascript" src="{!! asset('sdream/js/typed.js') !!}"></script>
	<script type="text/javascript" src="{!! asset('sdream/js/custom.js') !!}"></script>
	<script type="text/javascript" src="{!! asset('sdream/js/cart.js') !!}"></script>
	<script type="text/javascript" src="{!! asset('sdream/js/jquery.autocomplete.js') !!}"></script>
	<script type="text/javascript" src="{!! asset('sdream/js/notify.js') !!}"></script>
	<script type="text/javascript" src="{!! asset('sdream/js/notify.min.js') !!}"></script>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">





	<script>

		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();

			var counter = 0;
			$('.pluss,#plusss').click( function(){
				$('#display,#display1').html(++counter )});
			$('.minuss,#minusss').click( function(){
				$('.display,#display1').html((counter-1<0)?counter:--counter )});

		});
	</script>
	<script type="text/javascript">



		$(function () {
    // document.getElementById("uploadBtn").onchange = function () {
    //   document.getElementById("uploadFile").value = this.value;
    // };

    var goToCartIcon = function($addTocartBtn){
    	var $cartIcon = $(".my-cart-icon");
    	var $image = $('<img width="30px" height="30px" src="' + $addTocartBtn.data("image") + '"/>').css({"position": "fixed", "z-index": "999"});
    	$addTocartBtn.prepend($image);
    	var position = $cartIcon.position();      
    	$image.remove();
    }

    $('.my-cart-btn').myCart({
    	classCartIcon: 'my-cart-icon',
    	classCartBadge: 'my-cart-badge',
    	classProductQuantity: 'my-product-quantity',
    	classProductRemove: 'my-product-remove',
    	classCheckoutCart: 'my-cart-checkout',
    	affixCartIcon: true,
    	showCheckoutModal: true,
    	clickOnAddToCart: function($addTocart){
    		goToCartIcon($addTocart);
    	},
    	clickOnCartIcon: function($cartIcon, products, totalPrice, totalQuantity) {
    		console.log("cart icon clicked", $cartIcon, products, totalPrice, totalQuantity);
    	},
    	checkoutCart: function(products, totalPrice, totalQuantity) {
    		console.log("checking out", products, totalPrice, totalQuantity);
    	},
    	getDiscountPrice: function(products, totalPrice, totalQuantity) {
    		console.log("calculating discount", products, totalPrice, totalQuantity);
    		return 0 * 0.5;
    	}
    });


});

		(function() {
			"use strict";

			var supportsMultiple = self.HTMLInputElement && "valueLow" in HTMLInputElement.prototype;

			var descriptor = Object.getOwnPropertyDescriptor(HTMLInputElement.prototype, "value");

			self.multirange = function(input) {
				if (supportsMultiple || input.classList.contains("multirange")) {
					return;
				}

				var value = input.getAttribute("value");
				var values = value === null ? [] : value.split(",");
				var min = +(input.min || 0);
				var max = +(input.max || 100);
				var ghost = input.cloneNode();

				input.classList.add("multirange", "original");
				ghost.classList.add("multirange", "ghost");

				input.value = values[0] || min + (max - min) / 2;
				ghost.value = values[1] || min + (max - min) / 2;

				input.parentNode.insertBefore(ghost, input.nextSibling);

				Object.defineProperty(input, "originalValue", descriptor.get ? descriptor : {
    // Fuck you Safari >:(
    get: function() { return this.value; },
    set: function(v) { this.value = v; }
});

				Object.defineProperties(input, {
					valueLow: {
						get: function() { return Math.min(this.originalValue, ghost.value); },
						set: function(v) { this.originalValue = v; },
						enumerable: true
					},
					valueHigh: {
						get: function() { return Math.max(this.originalValue, ghost.value); },
						set: function(v) { ghost.value = v; },
						enumerable: true
					}
				});

				if (descriptor.get) {
    // Again, fuck you Safari
    Object.defineProperty(input, "value", {
    	get: function() { return this.valueLow + "," + this.valueHigh; },
    	set: function(v) {
    		var values = v.split(",");
    		this.valueLow = values[0];
    		this.valueHigh = values[1];
    		update();
    	},
    	enumerable: true
    });
}

if (typeof input.oninput === "function") {
	ghost.oninput = input.oninput.bind(input);
}

function update() {
	ghost.style.setProperty("--low", 100 * ((input.valueLow - min) / (max - min)) + 1 + "%");
	ghost.style.setProperty("--high", 100 * ((input.valueHigh - min) / (max - min)) - 1 + "%");
}

input.addEventListener("input", update);
ghost.addEventListener("input", update);

update();
}

multirange.init = function() {
	[].slice.call(document.querySelectorAll("input[type=range][multiple]:not(.multirange)")).forEach(multirange);
}

if (document.readyState == "loading") {
	document.addEventListener("DOMContentLoaded", multirange.init);
}
else {
	multirange.init();
}

})();

</script>
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
@if (Session::has('message'))
<div class="note note-info">
	<div class="container">
		<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
	</div>
</div>
@endif
</head>
<!-- Jquery page loader -->
<div class="jquery-loader dontprazremove">
	<!-- <img src="{{ url('public/uploads/jquery_loder.gif') }}"> -->
</div>
<!-- Jquery page loader  -->
<body class="lang_{!!$lang!!}">
	<div type="button" class="sp-feedback-container sp-rotate" data-toggle="modal" data-target="#feedback-model"><a class="sp-feedbackButton" href="#" data-ga-info="{&quot;description&quot;:&quot;Open Feedback Modal&quot;}">Feedback</a></div>


	<!-- Modal -->
	<div class="modal fade" id="feedback-model" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header feedback-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">  </h4>
				</div>
				<div class="modal-body feedback-body">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<img src="https://kbmerch1-a.akamaihd.net/magento/store_performance/tier0_feedback/feedback_icon.svg" alt="Feedback Icon">
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4">
						<img src="https://kbmerch1-a.akamaihd.net/magento/store_performance/tier0_feedback/help_icon.svg" alt="Help Icon">
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4">
						<img src="https://kbmerch1-a.akamaihd.net/magento/store_performance/tier0_feedback/submit_icon.svg" alt="Request Icon">
					</div>


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<header class="bs-docs-nav  navbar-static-top" id=top>
		<nav class="small_header">
			<div class="header-container">
      <!--     <div class="pull-left text-left">
            <span><i class="fa fa-map-marker"></i>@lang('core.smtc_location')</span>
            <span><i class="fa fa-phone"></i><a href="tel:123456789101">+123 456 789 101</a></span>
            <span><i class="fa fa-paper-plane"></i><a href="mailto:contact@smtc.com">contact@smtc.com</a></span>
        </div> -->
        <div class="pull-right text-right">
        	<select class="lang_select">
        		<option value="">@lang('core.Select_Language')</option>
        		<option @if(Session('lang')=='en' || Session('lang')=='') selected="" @endif value="en">English</option>
        		<option @if(Session('lang')=='ar') selected="" @endif value="ar">Arabic</option>
        	</select>
        	<!-- <a class="share_social" href=""><div class=""></div></a> -->
        	<a class="share_social" href=""><div class="">Help</div></a>
        	<!-- <a class="share_social" href=""><i class="fa fa-google-plus"></i></a> -->
        </div>
    </div>
</nav>
<nav class="navbar navbar-findcond">
	<div class="header-container">
		<div class="navbar-header">
			<div class="home-logo pull-right"><a href="{!!URL::To('/')!!}"><img src="{!!\URL::To('/')!!}/sdream/img/logo.jpeg"></a>
				<!-- <span class="fa fa-search"></span> -->
			</div>
			<button type="button" class="navbar-toggle collapsed nav-toggle-bar"onclick="openNav()">
				<span class="sr-only">@lang('core.Toggle_navigation')</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>

		<div id="mySidenav" class="sidenav">
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
			<ul class="nav navbar-nav">


				<li>
					<input type="hidden" name="coursetitle_id" id="coursetitle_id" >
					@if(Request::segment(1) != 'college-book')
					<div class="search_box ui-widget">
						<input id="search_box" type="text" name="coursetitle"  placeholder="Search by title, author, series or ISBN" >
						<ul class="sugges-box" id="suggesstion-box" style="display: none;">

						</ul>

						<button type="submit">
							<span class="fa fa-search auth-book-ser"></span>
						</button>
					</div>
					@endif
				</li>
				<li>


					<div class="panel-group header-first-accordian" id="faqAccordion">
						<div class="panel panel-default ">
							<div class="text-left selectbox-nav">
								<select class="lang_select">
									<option value="">@lang('core.Select_Language')</option>
									<option @if(Session('lang')=='en' || Session('lang')=='') selected="" @endif value="en">English</option>
									<option @if(Session('lang')=='ar') selected="" @endif value="ar">Arabic</option>
								</select>
								<!-- <a class="share_social" href=""><div class=""></div></a> -->
								<a class="share_social" href=""><div class="">Help</div></a>
								<!-- <a class="share_social" href=""><i class="fa fa-google-plus"></i></a> -->
							</div>
							<a href="{!!URL::To('/')!!}"><div class="nav-home-link">home</div></a>
							<div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question0">
								<h4 class="panel-title">
									<a href="#" class="ing">e-BOOKs</a>
								</h4>

							</div>
							<div id="question0" class="panel-collapse collapse" style="height: 0px;">
								<div class="panel-body">

									<ul class="ebook-category-nav">
										<li class="list-book-item">Fiction & Literature</li>
										<li>Romance</li>
										<li>Nonfiction</li>
										<div class="brows-link" ><a class="browse-all" href="">Browse all categories</a></div>
									</ul>
									<div class="recomendes-book-link">
										<a href="#">Recommended for You</a>
										<a href="#">Trending Now</a>
										<a href="#">Free eBooks</a>
										<a href="#">Books on Films and TV</a>
										<a href="#">New Releases</a>
										<a href="#">Bestsellers</a>

									</div>
								</div>

							</div>

						</div>
        <!-- <div class="panel panel-default dontprazremove">
            <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question1">
                 <h4 class="panel-title">
                    <a href="#" class="ing">APPS & eREADERS</a>
              </h4>

            </div>
            <div id="question1" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-body">
            <div class="recomendes-book-link">
                <a href="#">Android</a>
                <a href="#">Apple</a>
                <a href="#">BlackBerry 10</a>
                <a href="#">Kobo Desktop - Win | Mac</a>
                <a href="#">Windows 8</a>
                <a style="text-align:left;color:#bf0000;" href="#">Learn more</a>
                <div class="rec-accessories">
                <a href="">eREADERS</a>
                <a href="">Accessories</a>
                </div>
                </div>
            </div>
        </div>        
    </div> -->
    <div class="nav-wishlist">wishlist</div>
    <div class="nav-wishlist">feedback</div>
</li>
</ul>
<ul class="nav navbar-nav navbar-right third-nav">
	@if(!isset(\Auth::user()->role_id))
	<li>
		<li class="ac-nav-class"><a class="signup-btn" href="{!!\URL::To('/')!!}/register"><button class="btn btn-normal signup-btnn">Create free account</button></a>
			<a class="res-signin" href="{!!\URL::To('/')!!}/login">Sign in</a>

		</li>
		<li><a class="normal-signin"  href="{!!\URL::To('/')!!}/login">Sign in</a></li>

	</li>
	@else
	<li>

		<div class="drop_box user_login" class=" dropdown-toggle" data-toggle="dropdown">
			<ul>
				<li class="ac-image-right"><img src="{!!\URL::To('/')!!}/sdream/img/user.png"></li>
				<li><a href=""> Welcome</a></li>
				<li><a href="">My Account</a></li>
				<li><span class="fa fa-angle-down sign-font-btn"></span></li>
			</ul>
		</div>
		@if(isset(\Auth::user()->role_id) && \Auth::user()->role_id!='')

		<ul class="dropdown-menu user-login-drop">
			<li><a href="<?php echo url('/') ?>/mybook">My Book</a></li>
			<li><a href="<?php echo url('/') ?>/inbox">My Inbox</a></li>

			<li><a href="<?php echo url('/') ?>/wishlist">My Wishlist</a></li>
			@if(\Auth::user()->role_id == '2')
			<li class="dropdown dropdown-submenu"><a href="#" class="dropdown-toggle drop-std" data-toggle="dropdown">College Books</a>
				<ul class="dropdown-menu drop-student-menu">
					<li><a href="<?php echo url('/') ?>/college-book">Search college book</a></li>
					<li><a href="<?php echo url('/') ?>/student-mybook">My Book</a></li>             
				</ul>
			</li>
			@endif
			<li><a href="<?php echo url('/') ?>/purchase-history">Purchase History</a></li>
			<li><a href="{!!\URL::To('/')!!}/myaccount">@lang('core.Profile')</a></li>
			<li>{!! Form::open(['url' => 'logout']) !!}
				<button type="submit" class="logout">

					<span class="logout-btn"> <i class="fa fa-sign-out fa-fw"></i>{{ trans('quickadmin::admin.partials-sidebar-logout') }}</span>
				</button>
				{!! Form::close() !!}</li>

			</ul>
			@else
			<ul class="dropdown-menu">
				<li><a href="{!!\URL::To('/')!!}/login">Sign in</a></li>
				<li><a href="{!!\URL::To('/')!!}/register">Sign up</a></li>
			</ul>
			@endif
			@endif
		</li> 
	</ul>

	<!-- </nav> -->
</div>
</div>
</nav>

<nav class="navbar navbar-findcond nav-third-menu" @if(Request::segment(1) == 'checkout') style="display: none;" @endif>
	<div class="header-container">
		<div class="navbar-header">

			<!-- <a class="navbar-brand" href="#">WebSiteName</a> -->
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<!-- <li class="active"><a href="#">Home</a></li> -->
				<li class="dropdown">
					<div class="dropdown-ebook">
						<button class="dropbtn">eBOOK <span class="fa fa-angle-down"></span></button>
						<div class="dropdown-content col-md-12">
							<div class="col-xs-8 col-md-8 ebook-main-div">
								<ul class="ebook-category">
									@foreach(\Helper::bookCategory() as $value)
									<li onclick="location.href = '{{ url('search').'?categories='.$value->id }}';">{{ $value->name }}</li>
									@endforeach
								</ul>
								<div class="brows-link-header " ><a class="browse-all-cat" href="<?php echo url('/') ?>/bookcategories">Browse all categories</a></div>
							</div>
							<div class="col-xs-4 col-md-4 recomendes-book-link">
								<a href="#">Recommended for You</a>
								<a href="#">Trending Now</a>
								<a href="#">Free eBooks</a>
								<a href="#">Books on Films and TV</a>
								<a href="#">New Releases</a>
								<a href="#">Bestsellers</a>

								<div class="brows-link-rec " ><a class="browse-all-cat" href="">Browse all eBooks</a></div>
							</div>

						</div>
					</div>
				</li>
				<li class="dropdown">
           <!-- <div class="dropdown-ebook dontprazremove">
            <button class="dropbtn">APPS & eREADERS <span class="fa fa-angle-down"></span></button>
            <div class="dropdown-content col-md-12">
              <div class="col-xs-4 tabs-menus">
                <ul class="nav nav-tabs tabs-left nav-tab-apps">
                  <li class="active"><a href="#home" data-toggle="tab"><i class="fa fa-mobile-phone"></i>e-Book App</a></li>
                  <li><a href="#profile" data-toggle="tab"><i class="fa fa-tablet"> </i>e-Reader</a></li>
                  <li><a href="#messages" data-toggle="tab"><i class="fa fa-tablet"></i>Accessories</a></li>
                </ul>
              </div>
              <div class="col-xs-8 no-pad">
                <div class="tab-content">
                 <div class="tab-pane active" id="home">
                   <div class="col-xs-6 no-pad divice-first">
                     <ul class="divice-tab-panel">
                       <li>Android </li>
                       <li>Apple</li>
                       <li>BlackBerry10</li>
                       <li>Desktop- Win|Mac|Linux</li>
                     </ul>
                     <div class="tab-learn-more">Learn more</div>
                   </div>
                   <div class="col-xs-6">
                    <img src="https://kbmerch1-a.akamaihd.net/magento/onestore-top-nav/images/071417/dropdown-EN.jpg" style="max-width:100%">
                  </div>
                </div>
                <div class="tab-pane" id="profile">
                  <div class="col-xs-6 no-pad divice-first">
                   <ul class="divice-tab-panel">
                     <li>e-Book Aura ONE</li>
                     <li>e-Book Aura H2O</li>
                     <li>e-Book Clara HD</li>
                     <li>All eReaders</li>
                   </ul>
                   <div class="tab-learn-more">Where to Buy</div>
                 </div>
                 <div class="col-xs-6">
                  <img src="//kbmerch1-a.akamaihd.net/magento/onestore/primary-navigation/apps-ereaders-panel/images/ca/ereaders.jpg" style="max-width:100%">
                </div>
              </div>
              <div class="tab-pane" id="messages">
                <div class="col-xs-6 no-pad divice-first">
                 <ul class="divice-tab-panel">
                   <li>View All</li>
                 </ul>
                 <div class="tab-learn-more">Where to Buy</div>
               </div>
               <div class="col-xs-6">
                <img src="//kbmerch1-a.akamaihd.net/magento/onestore/primary-navigation/apps-ereaders-panel/images/ca/ereaders.jpg" style="max-width:100%">
              </div>
            </div>
          </div>
        </div>
      </div>
  </div> -->
</li>
</ul>
<ul class="nav navbar-nav navbar-right">
	<li class="fave-ebook">
		<a href="<?php echo url('/') ?>/wishlist"> <span class="fa fa-heart-o"></span></a>

	</li>

	<li>
		<?php 
		$key_cart = \Helper::getCartCookie();
		$getCart = \DB::table('cart')->where('college','0')->where('key_cart',$key_cart)->get();
		$getCartCollege = \DB::table('cart')->where('college','1')->where('key_cart',$key_cart)->get();
		?>
		<div class="top-cart-contain">
			<div  class="basket dropdown-toggle">
				<div class="cart"><img src="{!! asset('sdream/img/cart.png') !!}"><span class="badge cart-badge">@if(count($getCart) > 0 || count($getCartCollege) > 0) {{ count($getCart)+count($getCartCollege) }} @endif</span><span class="fa fa-angle-down down-cart-arr" ></span></div>
				<!--  <span class="my-cart-icon my-cart-badge"></span> -->
			</div>
		</div>
	</li>
</ul>
</div>
</div>
</nav>
<div style="width:100%;float:left;">
	<?php
	if(\Auth::check()){ 

		?>

		@if($lastsegment == '')<div class="header-container">@endif
		<div class="with-login-msg">
			<a href="" class="kwp_ecPromotion" title="GET Rs100 OFF YOUR FIRST EBOOK AT CHECKOUT.">
				<div class="innerWrapper">
				  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
  

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
      	<span class="kwp_desktop">
      		<span class="kwp_mainCopy">A <strong>Rs100 DISCOUNT</strong> WILL BE APPLIED TO YOUR FIRST EBOOK AT CHECKOUT.</span>
      		<span class="kwp_expires">Selected title must be Rs101 or greater. Learn More &gt;</span>
      	</span>
      </div>

      <div class="item">
        <span class="kwp_desktop">
        	<span class="kwp_mainCopy">A <strong>Rs1000 DISCOUNT</strong> WILL BE APPLIED TO YOUR FIRST EBOOK AT CHECKOUT.</span>
        	<span class="kwp_expires">Selected title must be Rs101 or greater. Learn More &gt;</span>
        </span>
      </div>
    
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="fa fa-angle-left"></span>
      <span class="sr-only"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="fa fa-angle-right"></span>
      <span class="sr-only"></span>
    </a>
  </div>

				</div>
			</a>
		</div>
		@if($lastsegment == '')</div>@endif
		<?php
	}else{
		?>
		<div class="without-login-msg">
			<a href="" class="kwp_ecPromotion" title="Sign up today and GET Rs100 OFF your first eBook.">
				<div class="innerWrapper">
					<span class="kwp_desktop">Sign up today and <strong>GET Rs100 OFF</strong> your first eBook. Learn More &gt;</span>
				</div>
			</a>
		</div>
		<?php 
	} 
	?>
</div>
</header>
<section class="without-cart-block" style="display:none;">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="container">
			<div class=" without-cart" >
				<div class="text-center" style="" >
					<h3>Your Shopping Cart is empty</h3> 
					<p> There are currently no items in your Shopping Cart.</p>
					<button class="btn btn-normal add-cart-btn cart-nbtn" >Continue shopping</button>
				</div>
			</div>
  <!--               <div class="with-cart">
        <div class="col-xs-12 col-sm-12 col-md-12 ">
        <div class="col-xs-12 col-sm-6 col-md-6">
        <h3>More titles to consider</h3>
        <img class="img-responsive" src="sdream/img/book1.jpg">
        <p>RS. 680.00</p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
        lzdkdvjn
        </div>
        </div>
    </div> -->
</div>
</div>
</section>

<section class="with-cart-block">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="container contai-ner">
			<div class="with-cart" >

				<div class="col-xs-12 col-sm-12 col-md-12 with-cart-div ">
					<div class="col-xs-12 col-sm-6 col-md-6 padd-right dontprazremove" style="display: none;">
						<h3 class="shopping-cart">More titles to consider</h3>
						<div class="col-xs-3">
							<img class="img-responsive" src="//kbimages1-a.akamaihd.net/ef22a49a-2754-4c41-865a-7ac2617b6ad6/110/170/False/the-masnavi-book-one.jpg">
							<p class="related-rating" >RS. 680.00</p>
							<div class="price-cart-fa">
								<span class="fa fa-cart-plus" ></span>
							</div>
							<!-- <div class="shop-cart-div"><span class="fa fa-shopping-cart"></span></div> -->
						</div>

					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 rightside-wish">
						<div class="">
							<h3 class="shopping-cart">Shopping Cart</h3>

							<div class="cartHeadDiv col-xs-12 col-sm-6" style="height:350px; overflow:auto;">
								<?php 
								$totalPrice = '0';
								if(count($getCart) > 0){

									foreach ($getCart as $value) {
										$book = \DB::table('eb_book')->where('id',$value->product)->first();
										?> 
										<div class="main-div-cart col-xs-12 col-md-12">
											<div class="first-div-cart">
												<div class="add-cart-img">
													<img class="img-responsive" src="{{ asset('public/uploads/books_banner') . '/'.  $book->id . '/'.  $book->banner }}">
												</div>
												<div class="add-cart-detail">
													<h3>{{ $book->title }}</h3>
													<p>by {{ Helper::getUser($book->user_id,'name') }}</p>
													<a href="javascript:void(0)" onclick="removeToCart({{ $book->id }} )">Remove</a>
												</div>
											</div>
											<div class="cart-price-table pull-right">
           <!--<h3 class="dontprazremove">
          @if($book->price_type == 'Paid')
          {{ \Session::get('ipCurrencySymbol').' '.\Helper::currencyPriceVal($book->price,$book->currency) }}
          <?php /* $totalPrice += \Helper::currencyPriceVal($book->price,$book->currency); */ ?>
          @else
          $echoData .= 'Free';
          @endif
      </h3>-->

      <h3>

      	@if($book->price_type == 'Paid')
      	$ {{ $book->price}}
      	<?php $totalPrice += $book->price ?>
      	@else
      	{!! 'Free' !!}
      	@endif    

      </h3>
  </div>
</div>
<?php }}  ?>


</div>
<div class="collegeCartHeadDiv col-xs-12 col-sm-6" style="height:350px; overflow:auto;">
	<?php 
        //$totalPrice = '0';
	if(count($getCartCollege) > 0){

		foreach ($getCartCollege as $value) {
			$book = \DB::table('eb_college_book')->where('id',$value->product)->first();
			?> 
			<div class="main-div-cart col-xs-12 col-md-12">
				<div class="first-div-cart">
					<div class="add-cart-img">
						<img class="img-responsive" src="{{ asset('public/uploads/college_books_banner') . '/'.  $book->id . '/'.  $book->banner }}">
					</div>
					<div class="add-cart-detail">
						<h3>{{ $book->title }}</h3>
						<p>by {{ Helper::getUser($book->user_id,'name') }}</p>
						<a href="javascript:void(0)" onclick="removeToCartCollege({{ $book->id }} )">Remove</a>
					</div>
				</div>
				<div class="cart-price-table pull-right">


					<h3>

						@if($book->price_type == 'Paid')
						$ {{ $book->price}}
						<?php $totalPrice += $book->price ?>
						@else
						{!! 'Free' !!}
						@endif    

					</h3>
				</div>
			</div>
			<?php }}  ?>


		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-6 pull-right">

		<div class="cart-add-total pull-right">
			<div class="pull-right ">Subtotal
				<!-- <span class="cartTotal dontprazremove">{!! \Session::get('ipCurrencySymbol') !!} <b>{{ $totalPrice }}</b></span> -->
				<span class="cartTotal dontprazremove">$ <b>{{ $totalPrice }}</b></span>

			</div>
			<br>
			<div class="pull-right sub-total-point">
				<form action="{{ url('checkout') }}" method="get">
					@if(count($getCart) > 0 || count($getCartCollege) > 0)
					<button class="btn btn-normal cart-shop-btnn">Checkout</button>
					@else
					<button class="btn btn-normal cart-shop-btnn" disabled="true">Checkout</button>
					@endif 
				</form>
			</div>

			<div class="pull-left again-shopping">
				<a href="">Continue shopping</a>
			</div>

		</div>



	</div>
</div>
</div>
  <!--               <div class="with-cart">
        <div class="col-xs-12 col-sm-12 col-md-12 ">
        <div class="col-xs-12 col-sm-6 col-md-6">
        <h3>More titles to consider</h3>
        <img class="img-responsive" src="sdream/img/book1.jpg">
        <p>RS. 680.00</p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
        lzdkdvjn
        </div>
        </div>
    </div> -->
</div>
</div>

</section>

<style type="text/css">
	.note.note-info {
		position: fixed;
		top: 0;
		right: 0;
		padding: 20px;
		z-index: 100;
		width: 100%;
		text-align: center;
	}
	.alert-warning {

		color: #8a6d3b;
		background-color: #f3f3f3;
		border-color: #faebcc;
		box-shadow: 1px 4px 7px -1px #3A3A3A;
		font-size: 18px;
		font-weight: bold;

	}
	.sugges-box{list-style-type:none;padding-left:0;max-height: 719px;overflow:auto;box-shadow:0 3px 5px rgba(50,50,50,.34);z-index: 5000;position: absolute;width: 100%;top: 45px;background-color: #ffff;left: 2px;}
	.sugges-box li{margin: 15px 15px 5px;font-size: 16px;font-weight: 500;cursor:pointer;color:#595959}
	.title-name{color: #595959;font-family: "Trebuchet MS",Trebuchet,Arial,Helvetica,sans-serif;    font-weight: 700 !important;font-size: 15px !important;}
	.search-all-cat{background-color: #ededed;height: 30px;margin: 5px 10px 0 10px;padding: 0 10px;position: relative;}
	.search-all-cat a{color: #595959;line-height: initial;min-height: initial;transition: padding .2s,color .1s;font-family: "Trebuchet MS",Trebuchet,Arial,Helvetica,sans-serif;font-style: normal;font-size: 14px;padding: 7.5px 0 0 0;transition: padding .2s;}
	.auth-book-sera{bottom: 2px;display: inline;height: 20px;position: absolute;right: 10px;width: 20px;}
	.authorBar{display: inline-block;margin:7px; color:#595959;}
	.authorBar:hover{color:#000;}
	.sugges-box li:hover{transform: translateX(5px);color:#000;}
	.search-all-cat:hover, .title-name:hover{transform: none!important;}

.carousel-control.left, .carousel-control.right{background-image:none;}
.fa-angle-left{font-size: 45px;color: #bf0000;margin-top: -9px;float:left;margin-left: -13px;}
.fa-angle-right{font-size: 45px;color: #bf0000;margin-top: -9px;float:right;margin-right: -13px;}
</style>
<script type="text/javascript">
	$(window).on('load', function() {
		hideAjaxLoad();
		hideNotificationLoad();
	});
	function showAjaxLoad(){
		$('.jquery-loader.dontprazremove').show();
		$(".jquery-loader.dontprazremove").animate({height:'100%'}, 500);
	}
	function hideAjaxLoad(){
		$(".jquery-loader.dontprazremove").animate({height:0}, 500);
		setTimeout(function(){ $('.jquery-loader.dontprazremove').hide(); }, 500);
	}
	function hideNotificationLoad(){

		setTimeout(function(){ $(".note.note-info").animate({top:'-100px'}, 500); }, 2000);
	}
</script>
<script type="text/javascript">
	var base_url=$("#base_url").val();
	$(document).ready(function(e) {
		$("#coursetitle").autocomplete({
			valueKey:'title',
			source:[{
				url:base_url+"/getCourseTitle?&query=%QUERY%",
				type:'remote',
				ajax:{  
					dataType : 'json' 
				}
			}]}).on('selected.xdsoft',function(e,suggestion){  
				$("#coursetitle_id").val(suggestion.data);
				window.location.href = base_url+"/detail/"+suggestion.data;
			});

        // $(".cart").click(function(){
        //   $( ".without-cart-block" ).click(function() {
        //     if ( $(this).height() != 50)
        //       $( this ).animate({ height: 0 }, 1000 );
        //     else
        //       $( this ).animate({ height: 400 }, 1000 );
        //   });
        // }); 
        ;
    });
	$(document).on('change','.lang_select',function(){
		window.location.href = base_url+"/lang/"+$(this).val();
	})

   //  var div = $(".without-cart-block");
   //  var carti = '1';
   //  $(".cart").click(function(){
   //    if(carti%2 != '0'){
   //      div.animate({height:300}, 500); 
   //    }            
   //    else{
   //     div.animate({height:0}, 500);

   //   }
   //   carti++;
   // });

   var div = $(".with-cart-block");
   var carti = '1';
   $(".cart").click(function(){
   	if(carti%2 != '0'){
   		div.animate({height:525}, 500); 
   	}            
   	else{
   		div.animate({height:0}, 500);

   	}
   	carti++;
   });

   $(".cart-nbtn").click(function() {
   	$('html,body').animate({
   		scrollTop: $(".latest_courses").offset().top},
   		'slow');
   });


</script>

<script>
	function openNav() {
		document.getElementById("mySidenav").style.width = "250px";
	}

	function closeNav() {
		document.getElementById("mySidenav").style.width = "0px";

	}

	function addToCart(id){
		showAjaxLoad();
  //alert(id)
  $.ajax({
  	type:'POST',
  	url:"<?php echo url('/addToCart'); ?>",
  	data:{ id:id,_token:'<?php echo csrf_token(); ?>'},
  	success:function(data){
  		hideAjaxLoad()
  		if(data != 'exists'){
  			$('.cartHeadDiv').html(data);
  			var totalCart = $('#totalCart').val();
  			var cartTotal = $('#totalCartPrice').val();
  			$('.cart-badge').text(totalCart); 
  			$('.cartTotal b').text(cartTotal); 
  			$(".with-cart-block").animate({height:525}, 500);
  			$('.cart-shop-btnn').removeAttr("disabled");
  			carti++;
  		}else{

  		}
  	}
  })
}
function removeToCart(id){
	showAjaxLoad();
  //alert(id)
  $.ajax({
  	type:'POST',
  	url:"<?php echo url('/removeToCart'); ?>",
  	data:{ id:id,_token:'<?php echo csrf_token(); ?>'},
  	success:function(data){
  		hideAjaxLoad()
  		
  			$('.cartHeadDiv').html(data);
  			var totalCart = $('.cartHeadDiv #totalCart').val();
  			var cartTotal = $('.cartHeadDiv #totalCartPrice').val();
  			$('.cart-badge').text(totalCart); 
  			$('.cartTotal b').text(cartTotal); 
  			$(".with-cart-block").animate({height:525}, 500);
  			if(cartTotal > 0){
  			$('.cart-shop-btnn').removeAttr("disabled");
  			}else{
  			$('.cart-shop-btnn').addAttr("disabled");	
  			}
  	}
  })
}
function addToBuy(id){
	showAjaxLoad();
  //alert(id)
  $.ajax({
  	type:'POST',
  	url:"<?php echo url('/addToCart'); ?>",
  	data:{ id:id,_token:'<?php echo csrf_token(); ?>'},
  	success:function(data){
  		hideAjaxLoad()
  		if(data != 'exists'){
  			$('.cartHeadDiv').html(data);
  			var totalCart = $('#totalCart').val();
  			var cartTotal = $('#totalCartPrice').val();
  			$('.cart-badge').text(totalCart); 
  			$('.cartTotal b').text(cartTotal);
  			window.location.replace('<?php echo url('checkout'); ?>'); 
        //window.location.href = '<?php echo url('checkout'); ?>'; 
        $(".with-cart-block").animate({height:525}, 500);
        $('.cart-shop-btnn').removeAttr("disabled");
        carti++;
    }else{
    	window.location.replace('<?php echo url('checkout'); ?>'); 
    }
}
})
}
function addToWish(id){
	showAjaxLoad();
  //alert(id)
  $.ajax({
  	type:'POST',
  	url:"<?php echo url('/addToWish'); ?>",
  	data:{ id:id,_token:'<?php echo csrf_token(); ?>'},
  	success:function(data){
  		hideAjaxLoad()

  		$(".fave-ebook a .fa-heart-o").animate({
  			color: '#bf0000'
  		});


  		if(data == 'success'){
  			alert('Wish list added');
  		}else if(data == 'exists'){
  			alert('Wish list added'); 
  		}else if(data == 'notLogin'){
  			window.location.replace('<?php echo url('login'); ?>'); 
  		}
  	}
  })
}


function addToCartCollege(id){
	showAjaxLoad();
  //alert(id)
  $.ajax({
  	type:'POST',
  	url:"<?php echo url('/addToCart'); ?>",
  	data:{ id:id,_token:'<?php echo csrf_token(); ?>',college:'1'},
  	success:function(data){
  		hideAjaxLoad()
  		if(data != 'exists'){
  			$('.collegeCartHeadDiv').html(data);
  			var totalCart = $('#totalCart').val();
  			var cartTotal = $('#totalCartPrice').val();
  			$('.cart-badge').text(totalCart); 
  			$('.cartTotal b').text(cartTotal); 
  			$(".with-cart-block").animate({height:525}, 500);
  			$('.cart-shop-btnn').removeAttr("disabled");
  			carti++;
  		}else{

  		}
  	}
  })
}
function removeToCartCollege(id){
	showAjaxLoad();
  //alert(id)
  $.ajax({
  	type:'POST',
  	url:"<?php echo url('/removeToCart'); ?>",
  	data:{ id:id,_token:'<?php echo csrf_token(); ?>',college:'1'},
  	success:function(data){
  		hideAjaxLoad()
  		

  			$('.collegeCartHeadDiv').html(data);
  			var totalCart = $('.collegeCartHeadDiv #totalCart').val();
  			var cartTotal = $('.collegeCartHeadDiv #totalCartPrice').val();
  			$('.cart-badge').text(totalCart); 
  			$('.cartTotal b').text(cartTotal); 
  			$(".with-cart-block").animate({height:525}, 500);
  			if(cartTotal > 0){
  			$('.cart-shop-btnn').removeAttr("disabled");
  			}else{
  			$('.cart-shop-btnn').addAttr("disabled");	
  			}

  		
  	}
  })
}
function addToBuyCollege(id){
	showAjaxLoad();
  //alert(id)
  $.ajax({
  	type:'POST',
  	url:"<?php echo url('/addToCart'); ?>",
  	data:{ id:id,_token:'<?php echo csrf_token(); ?>',college:'1'},
  	success:function(data){
  		hideAjaxLoad()
  		if(data != 'exists'){
  			$('.cartHeadDiv').html(data);
  			var totalCart = $('#totalCart').val();
  			var cartTotal = $('#totalCartPrice').val();
  			$('.cart-badge').text(totalCart); 
  			$('.cartTotal b').text(cartTotal);
  			window.location.replace('<?php echo url('checkout'); ?>'); 
        //window.location.href = '<?php echo url('checkout'); ?>'; 
        $(".with-cart-block").animate({height:525}, 500);
        $('.cart-shop-btnn').removeAttr("disabled");
        carti++;
    }else{
    	window.location.replace('<?php echo url('checkout'); ?>'); 
    }
}
})
}
function addToWishCollege(id){
	showAjaxLoad();
  //alert(id)
  $.ajax({
  	type:'POST',
  	url:"<?php echo url('/addToWish'); ?>",
  	data:{ id:id,_token:'<?php echo csrf_token(); ?>',college:'1'},
  	success:function(data){
  		hideAjaxLoad()

  		$(".fave-ebook a .fa-heart-o").animate({
  			color: '#bf0000'
  		});


  		if(data == 'success'){
  			alert('Wish list added');
  		}else if(data == 'exists'){
  			alert('Wish list added'); 
  		}else if(data == 'notLogin'){
  			window.location.replace('<?php echo url('login'); ?>'); 
  		}
  	}
  })
}
</script>

<script>




	$('#search_box').on('keyup',function(){
		var key = $(this).val();
		var len = key.length;
		if(len > 1){
			$.ajax({
				type:"GET",
				url:"{{ url('search/suggestion') }}",
				data:{'key':key},
				success:function(data){
					$("#suggesstion-box").html(data);
				}
			})
			$("#suggesstion-box").show();
			$("#search_box").css("background","#FFF");
		}else{
			$("#suggesstion-box").hide();
			$("#search_box").css("background","#FFF");
		} 

	})

$('.owl-slider').owlCarousel({
    autoplay: false,
    stagePadding: 0,
    margin:0,
    singleItem:true,
    loop:true,
    responsiveClass:true,
    navigationText: ["<img src='sdream/img/owl-back.png'>", "<img src='sdream/img/owl-next.png'>"],
    navigationClass:['owl-arow-prev', 'owl-arow-next'],
    dots: false,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:false,
            loop:false
        }
    }
})


</script>


