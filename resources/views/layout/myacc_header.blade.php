@include('layout.header')

<?php 
    $url = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    $lastsegment = array_pop($url);

?>    

   


    <section class="header-container" style="margin-top: 45px;">
    <ul class="acc-header-menu">
    <li><a href="{!!URL::To('/')!!}">
        Home</a> &gt;</li>
    <li>
        My Account</li>
</ul>    
<div class="acc-Container" class="">
    
    <ul class="kb_navTabs">
        
        <li @if($lastsegment == 'myaccount' ) class="active" @endif>
            <a href="<?php echo url('/') ?>/myaccount"><span>
                My Account</span></a></li>
        
        

        <li @if($lastsegment == 'payment_info' ) class="active" @endif>
            <a href="<?php echo url('/') ?>/payment_info"><span>
                Payment Information</span></a></li>

        <li @if($lastsegment == 'purchase-history' ) class="active" @endif>
            <a href="<?php echo url('/') ?>/purchase-history"><span>General Book Purchase History</span></a></li>

        <li @if($lastsegment == 'college-purchase-history' ) class="active" @endif>
            <a href="<?php echo url('/') ?>/college-purchase-history"><span>College Book Purchase History</span></a></li>

    </ul>
    <b></b>

</div>    
    </section>

