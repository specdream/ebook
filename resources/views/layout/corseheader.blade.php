@include('layout.header')
<div class="col-xs-12 course_header">
  <div class="container">
    <section>
      <div class="goal_top">
        <div class="goal_img"></div>
        <div class="goad_detail">
          <h5 class="no_margin"><b>Photoshop CS6</b>
            <div class="course_seting pull-right"><i class="fa fa-cog"></i> Course Settings</div>
          </h5>
          <p>Patrick Stanly</p>
          <div class="draft">DRAFT</div>
        </div>
      </div>
      <div class="light_blue">
        <i class="fa fa-close"></i>
        <b>Want more help producing videos?</b>
        <p>Get equipment tips and a professional review of your audio, video, and delivery with our fresh test video service.</p>
      </div>
    </section>
    <section class="step_step">
      <div class="col-md-2 col-sm-2 col-xs-4 active">
        <p>Course Goals</p>
        <a href="">1</a>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-4">
        <p>Curriculum</p>
        <a href="">2</a>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-4">
        <p>Course Landing Page</p>
        <a href="">3</a>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-4">
        <p>Price & Coupons</p>
        <a href="">4</a>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-4">
        <p>Captions</p>
        <a href="">5</a>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-4">
        <p>Automatic messages</p>
        <a href="">6</a>
      </div>
    </section>
  </div>
</div>
