	@include('layout.header')
	<div class="header-container">
	<section class="wishlist-page">
			<div class="wishlist-header">
			<h1 class="title">My Books	</h1>
		</div>
		<?php 
			for ($x = 0; $x <= 2; $x++) {

			?>
		<div class="wish-detail">
			<ul class="wishlist-content no-pad"> 
				<li class="wishlist-item">
					<a class="item-link-underlay" href=""></a>
					<div class="col-xs-12 col-sm-12 col-md-12 no-pad">
						<div class="col-md-2 col-sm-2 col-xs-12 text-center">
							<img class="" src="//kbimages1-a.akamaihd.net/73f556a9-833d-4d30-bfc2-a17fd78cb44d/180/1000/False/interconnecting-smart-objects-with-ip.jpg">
						</div>
						<div class="col-md-8 col-sm-8 col-xs-12 wishli-content">
							<div class="item-info">
								<a class="heading-link" href="javascript:void(0)">
									<p class="title">Interconnecting Smart Objects with IP</p>
								</a>
								<div class="auther-name">by<span>Zadie Smith</span></div>
								<div class="star-rate">
									<i class="fa fa-star color_star"></i>
									<i class="fa fa-star color_star"></i>
									<i class="fa fa-star color_star"></i>
									<i class="fa fa-star color_star"></i>
									<i class="fa fa-star color_star"></i>
									<span>(38)</span>
								</div>
								<div class="synopsis-field">
								<a class="synopsis read-more" href="javascript:void(0)">
								<span class="synopsis-text">
								<p><strong>Semester VI</strong></p>
								<p><strong>'Smith's finest. Extraordinary, truly marvellous' <em>Observer</em></strong><br><strong>'Superb'</strong> <em>Financial Times</em> <strong>'Breathtaking'</strong> <em>TLS</em> <strong>'P****itch-perfect'</strong> <em>Daily Telegraph</em><br><strong>'A tale of two girls who meet in a West London dance class... A page-turner that's also beautifully written ' <em>Glamour</em></strong><br><strong>'There is still ...</strong></p>
								</span>
								</a>
								</div>
							</div>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-12">
							<div class="product-price-container">
								<p class="price">$. 132.79</p>
								<div class="cart-btn">
									<button class="btn btn-normal add-cart-btn" data-toggle="modal" data-target="#view-student">View</button>
								</div>

							</div>
						</div>
					</div>
				</li>
			</ul> 
		</div>
		<?php 
	}
		?>
		</section>
		</div>

		  <!-- Modal -->
  <div class="modal fade" id="view-student" role="dialog">
    <div class="modal-dialog student-popup">
    
      <div class="modal-content zoomIn animated">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Student Section</h4>
        </div>
        <div class="modal-body stud-review">
        <div class="">
        	<div class="form-group col-sm-12 col-xs-12">
        	<label class="control-label col-sm-3 std-label">Student name</label>
        		<div class="col-sm-6 Book-Ban">
        			
        			<input type="text" class="form-control" name="name" value="" required="" placeholder="">
        			<span id="summary_msg" style="color: #F00;"></span> 
        		</div>
        	</div>
        	<div class="form-group col-sm-12 col-xs-12">
        	<label class="control-label col-sm-3 std-label">Student Id</label>
        		<div class="col-sm-6 Book-Ban">
        			
        			<input type="text" class="form-control" name="std id" value="" required="" placeholder="">
        			<span id="summary_msg" style="color: #F00;"></span> 
        		</div>
        	</div>
        	<div class="form-group col-sm-12 col-xs-12">
        	<label class="control-label col-sm-3 std-label">Book Id</label>
        		<div class="col-sm-6 Book-Ban">
        			
        			<input type="text" class="form-control" name="book item-link-underlay" value="" required="" placeholder="">
        			<span id="summary_msg" style="color: #F00;"></span> 
        		</div>
        	</div>
        	<div class="form-group col-sm-12 col-xs-12">
        	<label class="control-label col-sm-3 std-label">Book Name</label>
        		<div class="col-sm-6 Book-Ban">
        			
        			<input type="text" class="form-control" name="book name" value="" required="" placeholder="">
        			<span id="summary_msg" style="color: #F00;"></span> 
        		</div>
        	</div>
        </div>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default btn-std-submit" data-dismiss="modal">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	@include('layout.footer')