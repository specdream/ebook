@include('layout.header')
<div class="col-xs-12 no_pad detail_page">
  <section class="detail_header">
    <div class="container">
      <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="gift_course">Gift This Course <i class="fa fa-heart-o"></i></div>
        <div class="detail_left">          
          <div class="detail_img" style="background-image:url('<?php echo asset("sdream/img/footer.jpg")?>')"></div>
          <div class="detail_book">
            <div class="detail_price"><span class="org_price">$960</span><strike class="stick_price">$2,800</strike><span class="pecnt_price">92% off</span></div>
            <div class="days_left">6 days left at this price!</div>
            <button class="buy_now">Buy Now</button>
            <button class="add_cart">Add To Cart</button>
            <small>30-Day Money-Back Guarantee</small>
          </div>
        </div>
      </div>
      <div class="col-md-8 col-sm-8 col-xs-12 detail_detail">
        <h3>The Web Developer Bootcamp</h3>
        <h4>The only course you need to learn web development - HTML, CSS, JS, Node, and More!</h4>
        <div class="col-md-6 col-sm-6 col-xs-12 pad_b_20">
          <i class="fa fa-star color_star"></i>
          <i class="fa fa-star color_star"></i>
          <i class="fa fa-star color_star"></i>
          <i class="fa fa-star color_star"></i>
          <i class="fa fa-star"></i>
          <span>4.7 (58,836 ratings)</span>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 pad_b_20">244,947 students enrolled</div>
        <div class="col-md-6 col-sm-6 col-xs-12 pad_b_20"><span class="pad_r_15">Created by Colt Steele</span> Last updated 2/2018</div>
        <div class="col-md-6 col-sm-6 col-xs-12 pad_b_20">
          <span class="pad_r_15"><i class="fa fa-comment"></i> English</span>
          <span><span class="fa_cc">cc</span> English, Dutch</span>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="col-md-8 col-sm-8 col-md-offset-4 col-sm-offset-4 col-xs-12 detail_brief">
        <b>Description</b>
        <div class="view_more_block">
          <p>Become a Python Programmer and learn one of employer's most request skills of 2017!<br><br>
          This is the most comprehensive, yet straight-forward, course for the Python programming language on Udemy! Whether you have never programmed before, already know basic syntax, or want to learn about the advanced features of Python, this course is for you! In this course we will teach you both versions of Python (2 and 3) so you can easily adapt your skill set to either version!<br><br>
          With over 100 lectures and more than 10 hours of video this comprehensive course leaves no stone unturned! This course includes quizzes, tests, and homework assignments as well as 3 major projects to create a Python project portfolio!<br><br>
          </p>
          <div class="view_more">View More +</div>
        </div>
        <b>Compare to Other Python Courses</b>
        <div class="conpare_courses">
          <div class="compare_c_block">
            <div class="col-md-2 col-sm-2 col-xs-12 pad_rl_5">
              <div class="conpare_courses_img" style="background-image:url('<?php echo asset("sdream/img/footer.jpg")?>')"></div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 pad_rl_5">
              <div>Complete Python Bootcamp: Go from zero to hero in Python</div>
              <div>Updated 12/2017</div>
            </div>
            <center class="col-md-6 col-sm-6 col-xs-12 pad_rl_5">
              <span class="pull-left"><i class="fa fa-star color_star"></i>4.5</span>
              <strike>$2,300</strike><strong>$960</strong>
              <span class="pull-right"><i class="fa fa-heart-o color_star"></i></span>
            </center>
          </div>
          <div class="compare_c_block">
            <div class="col-md-2 col-sm-2 col-xs-12 pad_rl_5">
              <div class="conpare_courses_img" style="background-image:url('<?php echo asset("sdream/img/footer.jpg")?>')"></div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 pad_rl_5">
              <div>Complete Python Bootcamp: Go from zero to hero in Python</div>
              <div>Updated 12/2017</div>
            </div>
            <center class="col-md-6 col-sm-6 col-xs-12 pad_rl_5">
              <span class="pull-left"><i class="fa fa-star color_star"></i>4.5</span>
              <strike>$2,300</strike><strong>$960</strong>
              <span class="pull-right"><i class="fa fa-heart-o color_star"></i></span>
            </center>
          </div>
          <div class="compare_c_block">
            <div class="col-md-2 col-sm-2 col-xs-12 pad_rl_5">
              <div class="conpare_courses_img" style="background-image:url('<?php echo asset("sdream/img/footer.jpg")?>')"></div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 pad_rl_5">
              <div>Complete Python Bootcamp: Go from zero to hero in Python</div>
              <div>Updated 12/2017</div>
            </div>
            <center class="col-md-6 col-sm-6 col-xs-12 pad_rl_5">
              <span class="pull-left"><i class="fa fa-star color_star"></i>4.5</span>
              <strike>$2,300</strike><strong>$960</strong>
              <span class="pull-right"><i class="fa fa-heart-o color_star"></i></span>
            </center>
          </div>
          <div class="view_more">View More +</div>
        </div>
        <b>Featured Review</b>
        <div class="overall_review">
          <center class="col-md-3 col-sm-4 col-xs-12">
            <h1>4.7</h1>
            <div>
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star"></i>
            </div>
            <div>Average Rating</div>
          </center>
           <div class="star_bar col-md-9 col-sm-8 col-xs-12">
            <div class="progress">
              <div class="progress-bar progress-gray" role="progressbar" aria-valuenow="50"
              aria-valuemin="0" aria-valuemax="100" style="width:72%"></div>
            </div>
            <div class="seperat_star">
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star color_star"></i>
              <span>72%</span>
            </div>
          </div>
          <div class="star_bar col-md-9 col-sm-8 col-xs-12">
            <div class="progress">
              <div class="progress-bar progress-gray" role="progressbar" aria-valuenow="50"
              aria-valuemin="0" aria-valuemax="100" style="width:23%"></div>
            </div>
            <div class="seperat_star">
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star"></i>
              <span>23%</span>
            </div>
          </div>
          <div class="star_bar col-md-9 col-sm-8 col-xs-12">
            <div class="progress">
              <div class="progress-bar progress-gray" role="progressbar" aria-valuenow="50"
              aria-valuemin="0" aria-valuemax="100" style="width:4%"></div>
            </div>
            <div class="seperat_star">
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <span>4%</span>
            </div>
          </div>
          <div class="star_bar col-md-9 col-sm-8 col-xs-12">
            <div class="progress">
              <div class="progress-bar progress-gray" role="progressbar" aria-valuenow="50"
              aria-valuemin="0" aria-valuemax="100" style="width:1%"></div>
            </div>
            <div class="seperat_star">
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <span>1%</span>
            </div>
          </div>
          <div class="star_bar col-md-9 col-sm-8 col-xs-12">
            <div class="progress">
              <div class="progress-bar progress-gray" role="progressbar" aria-valuenow="50"
              aria-valuemin="0" aria-valuemax="100" style="width:1%"></div>
            </div>
            <div class="seperat_star">
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <span><1%</span>
            </div>
          </div>
        </div>
        <div class="user_review col-xs-12">
          <div class="user_review_detail">
            <div class="img-circle" style="background-image:url('<?php echo asset("sdream/img/study.png")?>')"></div>
            <div><strong>Marharyta Dymytrova</strong><span>(14 courses, 5 reviews)</span></div>
            <div class="col-md-6 col-sm-6 col-xs-12 no_pad">
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star"></i>
            </div>
            <div class="col-xs-12 no_pad">4 months ago</div>
          </div>
          <p>general, I liked the course. I've never felt completely lost and confused, even with the backend node/express part, although I'm a beginner and I thought I couldn't make it to the end. Also I was afraid that the course covers tools that I won't need or won't be able to use anywhere else. But at the end everything seems to be reusable for my own projects. I have an impression that the course is unfinished, I didn't get the sense of a closure. But overall experience was good!</p>
          <div class="review_help">Was this review helpful?
            <label for="help_y"><input id="help_y" type="radio" name="helpful">Yes</label>
            <label for="help_n"><input id="help_n" type="radio" name="helpful">No</label>
            Report
          </div>
        </div>
        <div class="user_review col-xs-12">
          <div class="user_review_detail">
            <div class="img-circle" style="background-image:url('<?php echo asset("sdream/img/study.png")?>')"></div>
            <div><strong>Marharyta Dymytrova</strong><span>(14 courses, 5 reviews)</span></div>
            <div class="col-md-6 col-sm-6 col-xs-12 no_pad">
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star color_star"></i>
              <i class="fa fa-star"></i>
            </div>
            <div class="col-xs-12 no_pad">4 months ago</div>
          </div>
          <p>general, I liked the course. I've never felt completely lost and confused, even with the backend node/express part, although I'm a beginner and I thought I couldn't make it to the end. Also I was afraid that the course covers tools that I won't need or won't be able to use anywhere else. But at the end everything seems to be reusable for my own projects. I have an impression that the course is unfinished, I didn't get the sense of a closure. But overall experience was good!</p>
          <div class="review_help">Was this review helpful?
            <label for="help_y"><input id="help_y" type="radio" name="helpful">Yes</label>
            <label for="help_n"><input id="help_n" type="radio" name="helpful">No</label>
            Report
          </div>
        </div>
        <center><button class="show_review">Show More Reviews</button></center>
      </div>
    </div>
  </section>
</div>
@include('layout.footer')