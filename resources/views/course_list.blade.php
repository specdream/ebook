@include('layout.header')
<div class="col-xs-12 no_pad course_listin margin_tb_30">
  <div class="container">
    <h1>Course listing</h1>
    <div class="border_layout">
      <div class="col-md-3 col-sm-4 col-xs-12 no_pad">
        <ul class="nav nav-pills nav-stacked">
          <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
          <li><a data-toggle="tab" href="#menu1">Menu 1</a></li>
          <li><a data-toggle="tab" href="#menu2">Menu 2</a></li>
          <li><a data-toggle="tab" href="#menu3">Menu 3</a></li>
        </ul>
      </div>
      <div class="col-md-9 col-sm-8 col-xs-12 border_left">
        <div class="tab-content">
          <div id="home" class="tab-pane fade in active">
            <h3 class="text-center">HOME</h3>
            <ul>
              <li><a href="">Lession 1</a></li>
              <li><a href="">Lession 2</a></li>
              <li><a href="">Lession 3</a></li>
              <li><a href="">Lession 4</a></li>
              <li><a href="">Lession 5</a></li>
            </ul>
          </div>
          <div id="menu1" class="tab-pane fade">
            <h3 class="text-center">Menu 1</h3>
            <p>Some content in menu 1.</p>
          </div>
          <div id="menu2" class="tab-pane fade">
            <h3 class="text-center">Menu 2</h3>
            <p>Some content in menu 2.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('layout.footer')