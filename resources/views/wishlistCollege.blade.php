@include('layout.header')
<div class="col-xs-12 pull-left whithout-show">
<ul class="">
	<li class="btn btn-default"><a href="{{ url('wishlist') }}">General</a></li>
	<li class="btn btn-default"><a href="{{ url('wishlistCollege') }}">College</a></li>
</ul>
</div>
<div class="header-container">
	<section class="wishlist-page">
		<div class="wishlist-header">
			<h1 class="title">My Wishlist <span>({{ count($wish) }} items)</span></h1>
		</div>

		<?php 
		foreach ($wish as $values) {
			$value =  \DB::table('eb_college_book')->where('id',$values->book_id)->first();
			?>
			<div class="wish-detail">
				<ul class="wishlist-content no-pad">
					<li class="wishlist-item">
						<a class="item-link-underlay" href=""></a>
						<div class="col-xs-12 col-sm-12 col-md-12 no-pad">
							<div class="col-md-2 col-sm-2 col-xs-12 text-center">
								<img class="" src="{!! asset('public/uploads/college_books_banner') . '/'.  $value->id . '/'.  $value->banner !!}">
							</div>
							<div class="col-md-8 col-sm-8 col-xs-12 wishli-content">
								<div class="item-info">
									<a class="heading-link" href="javascript:void(0)">
										<p class="title">{!! \Helper::getLangValue($value->title)!!}</p>
									</a>
									<div class="auther-name">by<span>{!! \Helper::getUser($value->user_id,'name') !!}</span></div>
									<div class="star-rate">
										<i class="fa fa-star color_star"></i>
										<i class="fa fa-star color_star"></i>
										<i class="fa fa-star color_star"></i>
										<i class="fa fa-star color_star"></i>
										<i class="fa fa-star color_star"></i>
										<span>(38)</span>
									</div>
									<div class="synopsis-field">
										<a class="synopsis read-more" href="{{ url('college_details').'/'.$value->id }}">
											{!! $value->summary !!}
											<span class="text secondary-link">Read more</span>
										</span></a>
									</div>
								</div>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-12">
								<div class="product-price-container">
									<p class="price">@if($value->price_type == 'Free') Free @else ${!!$value->price!!} @endif</p>
									<div class="cart-btn">
										<button onclick="addToCartCollege(<?php echo $value->id ?>)" class="btn btn-normal add-cart-btn">Add to Cart </button>
									</div>
									<div class="remove-list">
										<a class="remove-wish" onclick="removeToWishCollege(<?php echo $value->id ?>)" href="javascript:void(0)">Remove from Wishlist</a>
									</div>
								</div>
							</div>
						</div>
					</li>
				</ul> 
			</div>
			<?php 
		}
		?>
	</section>
	<script type="text/javascript">
		function removeToWishCollege(id){
			showAjaxLoad();
  //alert(id)
  $.ajax({
  	type:'POST',
  	url:"<?php echo url('/removeToWish'); ?>",
  	data:{ id:id,_token:'<?php echo csrf_token(); ?>',college:'1'},
  	success:function(data){
  		hideAjaxLoad()

  		$(".fave-ebook a .fa-heart-o").animate({
  			color: '#bf0000'
  		});


  		if(data == 'success'){
  			window.location.replace('<?php echo url('wishlistCollege'); ?>'); 
  		}else if(data == 'notLogin'){
  			window.location.replace('<?php echo url('login'); ?>'); 
  		}
  	}
  })
}
</script>
</div>

@include('layout.footer')