@include('layout.header')
<div class="about-banner-img">
	<span class="overlay-top-header" style="background:rgba(0,0,0,0.5);"></span>
	<div class="container">
		<div class="text-center banner-txt ">
			<div class="col-lg-12 col-md-12 col-sm-12 about-banner col-xs-12">
				<!-- <h1>Keep place with Change</h1> -->
				<h5>About Us</h5>
			</div>
		</div>
	</div>
</div>

<div class="container">
<!--   <h2>Carousel Example</h2>   -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="sdream/img/university/slider-1.jpg" alt="Los Angeles" style="width:100%;">
      </div>

      <div class="item">
        <img src="sdream/img/university/slider-2.jpg" alt="Chicago" style="width:100%;">
      </div>
    
      <div class="item">
        <img src="sdream/img/university/slider-3.jpg" alt="New york" style="width:100%;">
      </div>
    </div>

    <!-- Left and right controls -->
   
  </div>
  <section id="about_container" class="parallax-window" data-parallax="scroll" data-image-src="img/home_bg_1.jpg" data-natural-width="1400" data-natural-height="470">

<div class="parallax-content-1">&Acirc;&nbsp;</div>

</section>

<!-- End section -->

<div class="container margin_60">

<div class="main_title">

<h2>ABOUT US</h2>

<p>Felis turpis nostra arcu tempus rutrum hac ullamcorper torquent volutpat pellentesque adipiscing ut elementum sollicitudin sodales congue quam rutrum habitant accumsan elit scelerisque accumsan proin varius purus scelerisque taciti nam Venenatis diam malesuada faucibus euismod congue eleifend ultricies bibendum augue dapibus nostra diam amet tincidunt commodo ullamcorper lorem maecenas nibh</p>

</div>

<div class="col-md-6 wow fadeIn" data-wow-delay="0.1s">

<div class="feature"><img src="sdream/themes/sdream/img/user_1.jpg" alt="website template image" />

<h3>Work hard. Play hard.</h3>

<p>Scelerisque ornare ante pellentesque id ad eget volutpat consequat aptent lacus penatibus consectetur vivamus magnis ornare venenatis mattis rhoncus interdum quam tempor commodo ultrices augue risus senectus ornare aliquam leo rutrum nam duis nisi fringilla convallis praesent libero faucibus fermentum convallis pretium aptent nisl morbi odio justo vel nascetur tristique pulvinar senectus etiam quam taciti dignissim etiam tincidunt cras molestie</p>

</div>

</div>

<div class="col-md-6 wow fadeIn" data-wow-delay="0.2s">

<div class="feature"><img src="sdream/themes/sdream/img/user_2.jpg" alt="website template image" />

<h3>Our Approach</h3>

<p>Cubilia gravida mus senectus donec consectetur dis ac vel quis a suscipit potenti purus aptent nibh gravida aliquet vestibulum varius dictum consectetur semper consectetur at varius donec sociis habitasse vivamus eget faucibus tempus donec lorem etiam volutpat blandit aliquam varius molestie nibh mattis adipiscing sodales dictumst volutpat quam rhoncus sodales pulvinar senectus etiam quam taciti dignissim etiam tincidunt cras molestie</p>

</div>

</div>

<!-- End row --><hr />

<div class="col-xs-12 no_pad">

<div class="col-xs-12 no_pad">

<h4>Rockstar Team</h4>

<p>Auctor sit parturient aenean sodales maecenas fusce potenti tristique lectus dapibus hendrerit habitant tristique vulputate vehicula quisque nisl curabitur varius enim quam maecenas facilisis inceptos auctor gravida massa facilisis risus</p>

</div>

</div>

<!-- End row -->

<div class="col-xs-12 no_pad">

<div class="row">
  <div class="column">
    <div class="card">
      <img src="sdream/img/lesson2.jpg" alt="Jane" style="width:100%">
      <div class="container">
        <h2>Jane Doe</h2>
        <p class="title">CEO & Founder</p>
        <p>Some text that describes me lorem ipsum ipsum lorem.</p>
        <p>example@example.com</p>
        <p><button class="button">Contact</button></p>
      </div>
    </div>
  </div>

  <div class="column">
    <div class="card">
      <img src="sdream/img/lesson2.jpg" alt="Mike" style="width:100%">
      <div class="container">
        <h2>Mike Ross</h2>
        <p class="title">Art Director</p>
        <p>Some text that describes me lorem ipsum ipsum lorem.</p>
        <p>example@example.com</p>
        <p><button class="button">Contact</button></p>
      </div>
    </div>
  </div>
  <div class="column">
    <div class="card">
      <img src="sdream/img/lesson2.jpg" alt="John" style="width:100%">
      <div class="container">
        <h2>John Doe</h2>
        <p class="title">Designer</p>
        <p>Some text that describes me lorem ipsum ipsum lorem.</p>
        <p>example@example.com</p>
        <p><button class="button">Contact</button></p>
      </div>
    </div>
  </div>
</div>

</div>

</div>  
</div>

<style type="text/css">

  box-sizing: border-box;
}

*, *:before, *:after {
  box-sizing: inherit;
}

.column {
  float: left;
  width: 33.3%;
  margin-bottom: 16px;
  padding: 0 8px;
}

@media screen and (max-width: 650px) {
  .column {
    width: 100%;
    display: block;
  }
}

.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}

.container {
  padding: 0 16px;
}

.container::after, .row::after {
  content: "";
  clear: both;
  display: table;
}

.title {
  color: grey;
}

.button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
}

.button:hover {
  background-color: #555;
}

	.carousel.slide{
		margin-top:50px;
	}
	.main_title {
    text-align: center;
    font-size: 16px;
    margin-bottom: 30px;
}

.main_title h2 {
    text-transform: uppercase;
    font-weight: 700;
    letter-spacing: -1px;
    font-size: 30px;
    margin: 10px 0px;
}
.main_title p {
    font-weight: 300;
    font-size: 14px;
}
.feature {
    padding: 30px 30px 20px 120px;
    position: relative;
    background: #fff;
    margin-bottom: 30px;
    color: #888;
    box-shadow: 0 0 5px 0 rgba(0,0,0,.1);
}

.feature img {
    position: absolute;
    left: 15px;
    width: 85px;
    top: 55px;
    border-radius: 50%;
}
.single-member {
    margin: 0;
    padding: 0;
}	



@media (min-width: 768px)
{
.person img {
    height: 175px;
    object-fit: cover;
}
}

</style>
@include('layout.footer')