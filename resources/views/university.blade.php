@include('layout.header')

<div class="univer-banner-img">
	<span class="overlay-top-header" style="background:rgba(0,0,0,0.5);"></span>
	<div class="container">
		<div class="text-center banner-txt ">
			<div class="col-lg-12 col-md-12 col-sm-12 univer-banner col-xs-12">
				<!-- <h1>Keep place with Change</h1> -->
				<h5>Top Universitys</h5>
			</div>
		</div>
	</div>
</div>
<section class="Universitys">
	<div class="container">
		<div class="col-xs-12 col-sm-6 col-md-4">
			<div class="image-flip" ontouchstart="this.classList.toggle('hover');">
				<div class="mainflip">
					<div class="frontside">
						<div class="card">
							<div class="card-body text-center">
								<p><img class=" img-fluid" src="sdream/img/university/1.jpg" alt="card image"></p>
								<h4 class="card-title">University of Dubai</h4>
								<p class="card-text">Academic City Emirates Road, Exit 49 - Dubai - United Arab Emirates </p>
								<!-- <a href="#" class="btn btn-primary  btn-sm"><i class="fa fa-plus"></i></a> -->
							</div>
						</div>
					</div>
					<div class="backside">
						<div class="card">
							<div class="card-body text-center mt-4">
								<h4 class="card-title">University of Dubai</h4>
								<p class="card-text">University of Dubai offers accredited degrees in Business, Law & IT which are aligned with international standards. The Business college is accredited by AACSB & the IT college is accredited by ABET.</p>
								<ul class="list-inline">
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-facebook"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-twitter"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-skype"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-google"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4">
			<div class="image-flip" ontouchstart="this.classList.toggle('hover');">
				<div class="mainflip">
					<div class="frontside">
						<div class="card">
							<div class="card-body text-center">
								<p><img class=" img-fluid" src="sdream/img/university/2.jpg" alt="card image"></p>
								<h4 class="card-title">Canadian University</h4>
								<p class="card-text">Sheikh Zayed Road, Behind the Shangri-La Hotel, 308th Road - Dubai - United Arab Emirates</p>
								<!-- <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a> -->
							</div>
						</div>
					</div>
					<div class="backside">
						<div class="card">
							<div class="card-body text-center mt-4">
								<h4 class="card-title">Canadian University</h4>
								<p class="card-text">Canadian University Dubai was established in 2006 to deliver high-quality undergraduate, graduate, continuing and corporate education in the UAE, and to provide a gateway for students </p>
								<ul class="list-inline">
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-facebook"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-twitter"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-skype"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-google"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4">
			<div class="image-flip" ontouchstart="this.classList.toggle('hover');">
				<div class="mainflip">
					<div class="frontside">
						<div class="card">
							<div class="card-body text-center">
								<p><img class=" img-fluid" src="sdream/img/university/3.png" alt="card image"></p>
								<h4 class="card-title">British University in Dubai</h4>
								<p class="card-text">This is basic card with image on top, title, description and button.</p>
								
							</div>
						</div>
					</div>
					<div class="backside">
						<div class="card">
							<div class="card-body text-center mt-4">
								<h4 class="card-title">British University in Dubai</h4>
								<p class="card-text">This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.</p>
								<ul class="list-inline">
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-facebook"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-twitter"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-skype"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-google"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

				<div class="col-xs-12 col-sm-6 col-md-4">
			<div class="image-flip" ontouchstart="this.classList.toggle('hover');">
				<div class="mainflip">
					<div class="frontside">
						<div class="card">
							<div class="card-body text-center">
								<p><img class=" img-fluid" src="sdream/img/university/1.jpg" alt="card image"></p>
								<h4 class="card-title">University of Dubai</h4>
								<p class="card-text">Academic City Emirates Road, Exit 49 - Dubai - United Arab Emirates </p>
								<!-- <a href="#" class="btn btn-primary  btn-sm"><i class="fa fa-plus"></i></a> -->
							</div>
						</div>
					</div>
					<div class="backside">
						<div class="card">
							<div class="card-body text-center mt-4">
								<h4 class="card-title">University of Dubai</h4>
								<p class="card-text">University of Dubai offers accredited degrees in Business, Law & IT which are aligned with international standards. The Business college is accredited by AACSB & the IT college is accredited by ABET.</p>
								<ul class="list-inline">
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-facebook"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-twitter"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-skype"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-google"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	<div class="col-xs-12 col-sm-6 col-md-4">
			<div class="image-flip" ontouchstart="this.classList.toggle('hover');">
				<div class="mainflip">
					<div class="frontside">
						<div class="card">
							<div class="card-body text-center">
								<p><img class=" img-fluid" src="sdream/img/university/3.png" alt="card image"></p>
								<h4 class="card-title">British University in Dubai</h4>
								<p class="card-text">This is basic card with image on top, title, description and button.</p>
								
							</div>
						</div>
					</div>
					<div class="backside">
						<div class="card">
							<div class="card-body text-center mt-4">
								<h4 class="card-title">British University in Dubai</h4>
								<p class="card-text">This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.</p>
								<ul class="list-inline">
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-facebook"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-twitter"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-skype"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-google"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


<div class="col-xs-12 col-sm-6 col-md-4">
			<div class="image-flip" ontouchstart="this.classList.toggle('hover');">
				<div class="mainflip">
					<div class="frontside">
						<div class="card">
							<div class="card-body text-center">
								<p><img class=" img-fluid" src="sdream/img/university/2.jpg" alt="card image"></p>
								<h4 class="card-title">Canadian University</h4>
								<p class="card-text">Sheikh Zayed Road, Behind the Shangri-La Hotel, 308th Road - Dubai - United Arab Emirates</p>
								<!-- <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a> -->
							</div>
						</div>
					</div>
					<div class="backside">
						<div class="card">
							<div class="card-body text-center mt-4">
								<h4 class="card-title">Canadian University</h4>
								<p class="card-text">Canadian University Dubai was established in 2006 to deliver high-quality undergraduate, graduate, continuing and corporate education in the UAE, and to provide a gateway for students </p>
								<ul class="list-inline">
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-facebook"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-twitter"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-skype"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a class="social-icon text-xs-center" target="_blank" href="#">
											<i class="fa fa-google"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<style type="text/css">

	
</style>
@include('layout.footer')