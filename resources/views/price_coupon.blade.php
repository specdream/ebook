@include('layout.corseheader')
<div class="col-xs-12 no_pad course_goal">
  <div class="container">
    <section class="border_box">
      <div class="col-md-9 col-sm-9 col-xs-12 no_pad">
        <div class="course_title">
          <h4>Price & Coupons</h4>
        </div>
        <div class="course_option">
          <div class="course_ques col-xs-12 no_pad">
            <b>Course Price Tier</b>
            <strong class="col-md-8 col-sm-12 col-xs-12 no_pad margin_b_20">Please select the price tier for your course below and click 'Save'. The list price that students will see in other currencies is calculated using the price tier matrix, based on the tier that it corresponds to.</strong>
          </div>
          <div class="course_price">
            <div class="col-md-5 col-sm-12 col-xs-12 no_pad">
              <div class="col-md-4 col-sm-5 col-xs-12 pad_rl_5">
                <select>
                  <option>USD</option>
                  <option>INR</option>
                  <option>GBP</option>
                </select>
              </div>
              <div class="col-md-4 col-sm-5 col-xs-12 pad_rl_5">
                <select>
                  <option>Select</option>
                  <option>Select1</option>
                </select>
              </div>
              <div class="col-md-4 col-sm-2 col-xs-12 pad_rl_5">
                <button>Save</button>
              </div>
            </div>
            <div class="col-md-7 col-sm-12 col-xs-12 pad_rl_5 pay_approve">Please complete the premium instructor application here in order to set a price for your course. You can set your course price as soon as your linked payment method is approved.</div>
          </div>
          <div class="course_coupon">
            <b>Course Coupons</b>
            <button>Create new coupon</button>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12 course_right">
        <b>Helpful Tips</b>
        <b>Course Goals & Target Students</b>
        <p>Course goals and target students appear on the course landing page and help potential students decide to purchase your course. Learn more.</p>
        <b>Course prerequisites</b>
        <p>Let your students know if they need to review any material or need any software before starting the course.</p>
        <b>Course Goals</b>
        <p>Course goals will give you a clear idea of what your students will learn and help you structure your course.</p>
        <b>Target Student</b>
        <p>Determining who your target student is will help you customize the course to fit your students' needs.</p>
      </div>
    </section>
    <center><input class="btn btn-review" type="submit" value="Submit for Review"></center>
  </div>
</div>
@include('layout.footer')