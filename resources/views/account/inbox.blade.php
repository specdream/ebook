@include('layout.header')  
<section class="col-xs-12 col-sm-12 col-md-12 no-pad mybook-section">
    <div class="header-container">
        <div class="pull-left">
            <h3>My Inbox</h3>
        </div>
        <div class="pull-right">
            
        </div>
    </div>
 </section>

 <section class="inbox-div">
    <div class="header-container">
      <?php 
      foreach($inbox as $value) {
      ?>

    <div class="inbox col-xs-12 col-sm-10 col-md-offset-1 no-pad inbox_view_{{ $value->status }}">
    <div class="commen-inbox-div">
        <div class="col-xs-12 col-sm-3 col-md-3">
            <div class="sender-title">
                <div class="sender-date"><h3>{{ \Helper::getUser($value->user_id,'name') }}</h3></div>
                <img src="{{ \Helper::getUser($value->user_id,'avatar') }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-7 col-md-9 msg-content-div">
        <div class="subject"><span>Date:</span> {{ date('F j, Y',strtotime($value->date)) }}</div>
        <div class="user-msg"><span>Message:</span> {{ $value->notification }}</div>
        <button class="btn btn-default btn-repaly" data-toggle="modal" data-target="#myModal">Reply</button>
        </div>
        </div>
    </div>
    <?php } ?>
    <div class="search-pagination" id="pagination">
    <?php 
    $c_url = $_SERVER['REQUEST_URI'];

    echo str_replace('/?', '?', $inbox->appends(Request::except('page'))->render()) ?>
</div>
    </div>
</section>


 <style type="text/css">
 /*.msg-content-div{border-bottom: 1px solid #dbdbdb;}*/    
 .inbox{padding: 5px 0;border: 2px solid #dbdbdb;margin-bottom: 20px;}
 .commen-inbox-div{width:100%;float:left;}
 .inbox-div{margin-top: 40px; margin-bottom: 43px; width: 100%;float: left;}
 .btn-repaly{padding: 3px 15px;border-radius: 0px;background-color: #bf0000;color: #fff;font-weight: bold;margin: 10px 0px;}
 .btn-repaly:hover, .btn-repaly:active, .btn-repaly:focus, .btn-repaly:visited{background-color: #bf0000;color: #fff;}
 .subject span{font-weight: bold;font-size: 14px;line-height: 1.43;color: #484848;}
 .sender-date{color:#9d9a9a;font-size: 11px;}
 .user-msg span{font-weight:bold;font-size: 14px;color: #484848;}
 .sender-title h3{font-size: 20px;text-transform: capitalize;font-weight: 700;margin-top: 0;position: absolute;background-color: rgba(0, 0, 0, 0.34);width: 100%;color: #fff;padding: 5px 3px;}
    .mybook-section h3 {
    -webkit-transition: padding .15s ease-in-out 0s,color .15s ease-in-out 0stransition: padding .15s ease-in-out 0s,color .15s ease-in-out 0s;
    font-family: "Trebuchet MS"Trebuchet,Arial,Helvetica,sans-serif;
    font-size: 1.5rem;
    font-style: normal;
    font-weight: 700;
    color: #000;
    text-transform: uppercase;
    float: left;
    height: 1.9rem;
    margin-right: 3rem;
    padding-bottom: 3.3rem;
    border-bottom: 0.5rem solid #bf0000;
    margin-bottom: 0;
    cursor: pointer;
}
.sender-title{position:relative;}

.mybook-section {
    background-color: #ededed;
    width: 100%;
    height: inherit;
}
.mybook-section {
    padding-top: 25px;
}
.sender-title img{width: 100%;height: 180px;object-fit: cover;}
 </style>

@include('layout.footer')
