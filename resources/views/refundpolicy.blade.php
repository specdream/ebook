@include('layout.header')

<div class="container">
	<section id="breadcrumbs">
		<div class="breadcrumbs"><a href="{!!URL::To('/')!!}">Home</a> <i class="fa fa-angle-right"></i> <strong>Refund Policy</strong></div>
	</section>
	<article class="item">	
		<header>
			<button class="print-btn"  onclick='printDiv();'>Print this page</button>
			<h1 class="title">Refund Policy</h1>
		</header>
		<div class="static_page clearfix printTable" id="DivIdToPrint">

			<h3>Refund policy</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>

			<h3>Contacting us</h3>
			<p>If you have any questions about this Policy, please contact us.</p>
					
		</div>			
	</article>
</div>
<script>

	function printDiv() {
		var divToPrint = document.getElementById('DivIdToPrint');
		var newWin = window.open('', 'Print-Window');
		newWin.document.open();
		newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
		newWin.document.close();
		setTimeout(function() {
			newWin.close();
		}, 10);
	}

</script>
@include('layout.footer')