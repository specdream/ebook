	@include('layout.header')

	<div class="header-container">
	<section class="col-xs-12 col-sm-12 col-md-12 review-page">
		<div class="col-xs-12 col-sm-3 col-md-3">
		<section class="book-metadata">
                <div class="book-cover-container" translate="no">
                    <img class="book-cover" src="//kbimages1-a.akamaihd.net/f9263eed-0752-410d-8705-dfa15238d966/166/300/False/defeat-is-an-orphan.jpg" alt="book cover">
                </div>

                    <p class="sub-section-title item-title">Defeat Is an Orphan</p>
                    <p class="book-author">by <span translate="no">Myra Macdonald</span></p>
            </section>
		</div>
		<div class="col-xs-12 col-sm-9 col-md-8">
		<section class="form-header ">
            <h2 class="write-form-title" style="display: none;">Share your thoughts</h2>
            <h2 class="complete-form-title">Complete your review</h2>
            <p class="sub-title">Tell readers what you thought by rating and reviewing this book.</p>
        </section>

        <section class="form-section rating-row">
        <div class="book-review">
        	     <h3 id="rateIt" class="section-title rating" style="display: none;">Rate it *</h3>
                <h3 id="ratedIt" class="section-title rating">You Rated it *</h3>

			<div class="stars-read-only star-review-sect">
				<i class="fa fa-star color_star"></i>
				<i class="fa fa-star color_star"></i>
				<i class="fa fa-star color_star"></i>
				<i class="fa fa-star color_star"></i>
				<i class="fa fa-star"></i>
				<span>(28)</span>
			</div>
           </div>

			<section class="form-section review-row">
				<section class="review-header">
					<h3 class="section-title">Add a review <span class="add-review-required">* Required</span></h3>

					<div class="col-sm-12 col-xs-12 no-pad review-comment-section">
						<label for="title" class="control-label">Review*</label>
						<textarea class="form-control wirte-review-inp" rows="5" id="comment" placeholder="Tell us if you enjoyed the story. What was the author's style like? What did you like best and the least? Who would you recommend this book for?" title="Tell us if you enjoyed the story. What was the author's style like? What did you like best and the least? Who would you recommend this book for?"></textarea>
						<p class="pull-right">(0) 50 characters minimum</p>
						<span id="title_msg" style="color: #F00;"></span>
					</div>

					<div class="col-sm-12 no-pad input-div">
						<label for="title" class=" control-label">Title*</label>
						<input class="form-control keyvalid" required="" name="title" type="text" id="title" placeholder="Briefly give your overall impression.">
						<span id="title_msg" style="color: #F00;"></span>
					</div>


					<div class="col-sm-12 no-pad input-div">
						<label for="title" class=" control-label">Display Name*</label>
						<input class="form-control keyvalid" required="" name="title" type="text" id="title" placeholder="e.g. Jane Smith">
						<span id="title_msg" style="color: #F00;"></span>
					</div>

				</section>

				<section class="submition-sec">
				<div class="sub-btns">
				<button class="btn btn-normal btn-review-cancel">Cancel</button>
				<button class="btn btn-normal btn-review-submit">Submit</button>
				</div>
				</section>

			</section>

		</div>
	</section>
	</div>
		

	@include('layout.footer')  