@include('layout.header')
<div class="container login_page">
    <div class="panel panel-default panel-login">
        <div class="panel-heading">
            <img src="{!! asset('sdream/img/stairs.png') !!}">
        </div>
        <div class="panel-body">

                        <form class="" role="form" method="POST" action="">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <i class="fa fa-users"></i>
                    <select name="who" id="who" class="form-control">
                        <option value="0" disabled="true" >who am I</option>
                        <option value="2" >Student</option>
                        <option value="8" >Member</option>
                        <option value="5" >Author</option>
                    </select>
                </div>

                <div class="form-group">
                    <i class="fa fa-user"></i>
                    <input type="text" class="form-control" name="fname" value="" placeholder="@lang('core.Full_Name')">
                </div>

                <div class="form-group">
                    <i class="fa fa-envelope"></i>
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@lang('core.E_mail')">
                </div>

                <div class="form-group">
                    <i class="fa fa-map-marker"></i>
                    <select name="country" id="country" class="form-control">
                        <option>@lang('core.Select_Country')</option>
                        @foreach(\Helper::getCountryDetails() as $aCountry)
                        <option value="{!!$aCountry->id!!}" data-type="{!!$aCountry->phonecode!!}">{!!$aCountry->name!!}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <i class="fa fa-mobile"></i>
                    <input type="text" class="form-control" name="city" placeholder="@lang('core.Enter_your_city')">
                </div>

                <div class="form-group">
                <input type="hidden" name="country_code">
                    <span id="country_code1">+91</span><input type="text" class="form-control" name="phone_number" placeholder="@lang('core.Mobile_Number')" onkeypress="return isNumber(event)">
                </div>

                <div class="form-group">
                    <i class="fa fa-lock"></i>
                    <input type="password" class="form-control" name="password" placeholder="@lang('core.password')">
                </div>

                <div class="form-group">
                    <i class="fa fa-lock"></i>
                    <input type="password" class="form-control" name="confirmpassword" placeholder="@lang('core.confirm_password')">
                </div>                

                <div class="form-group">
                    <label class="remember_me" for="remember_me"><input id="remember_me" type="checkbox" name="remember">@lang('core.Remember_Me')</label>
                     <small>@lang('core.by_signing_up_you_agree_to_our_terms_of_use_and_privacy_policy').</small>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn login-btn">@lang('core.Sign_up')</button>
                </div>
                <center>
                   
                    <div class="sign_up">@lang('core.Do_have_an_account') <a href="{!!\URL::To('/')!!}/login">@lang('core.Sign_in')</a></div>
                </center>
            </form>

            <div class="or_text"><span>@lang('core.OR')</span></div>

            <a class="social_login facebook_btn" href=""><i class="fa fa-facebook-f"></i>@lang('core.Continue_with_Facebook')</a>
            <a class="social_login google_btn" href=""><img src="{!! asset('sdream/img/google_plus.png')!!}">@lang('core.Continue_with_Google')</a>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>{!! trans('quickadmin::auth.whoops') !!}</strong> {!! trans('quickadmin::auth.some_problems_with_input') !!}
                <br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            

        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on('change','#country',function(){
        $("#country_code1").html($(this).find(':selected').attr('data-type'));
         $("#country_code").val($(this).find(':selected').attr('data-type'));
    })
    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
@include('layout.footer')
