@include('layout.header')
<div class="container login_page">
    <div class="panel panel-default panel-login">
        <div class="panel-heading">
            <img src="{!! asset('sdream/img/login.png') !!}">
        </div>
        <div class="panel-body">
                 <form class="" role="form" method="POST" action="{{ url('login') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <i class="fa fa-envelope"></i>
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@lang('core.E_mail')">
                </div>

                <div class="form-group">
                    <i class="fa fa-lock"></i>
                    <input type="password" class="form-control" name="password" placeholder="@lang('core.password')">
                </div>

                <div class="form-group">
                    <label class="remember_me" for="remember_me"><input id="remember_me" type="checkbox" name="remember">@lang('core.Remember_Me')</label>
                     <small>@lang('core.by_signing_up_you_agree_to_our_terms_of_use_and_privacy_policy') .</small>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn login-btn">@lang('core.Log_In')</button>
                </div>
                <center>
                    <div class="forget_pwd">@lang('core.or') <a href="">@lang('core.Forgot_Password')</a></div>
                   
                    <div class="sign_up">@lang('core.Dont_have_an_account') <a href="{!!\URL::To('/')!!}/register">@lang('core.Sign_up')</a></div>
                </center>
            </form>

        <div class="or_text"><span>@lang('core.OR')</span></div>

            <a class="social_login facebook_btn" href=""><i class="fa fa-facebook-f"></i>@lang('core.Continue_with_Facebook')</a>
            <a class="social_login google_btn" href=""><img src="{!! asset('sdream/img/google_plus.png')!!}">@lang('core.Continue_with_Google')</a>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>{!! trans('quickadmin::auth.whoops') !!}</strong> {!! trans('quickadmin::auth.some_problems_with_input') !!}
                <br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            
   
        </div>
    </div>
</div>
<style type="text/css">
  
</style>
@include('layout.footer')
