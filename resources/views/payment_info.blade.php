
@include('layout.myacc_header')

<div class="header-container">
	<section class="payment-info" style="background-color:#f8f8f8;margin-bottom: 40px;">
		<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 ">	
			<div class="col-xs-12 col-md-6 col-sm-6  col-lg-6 bill-adr">
				<div class="billing-add">
					<h2>Billing Address</h2>
					<p>Used to calculate taxes on your purchases.</p>
					<form action="">


						<div class="form-group">
							<label for="acc-pwd">country:</label>
							<select class="form-control county-selct-box">
								<option value="CA">Canada</option>
								<option value="GB">United Kingdom</option>
								<option value="NZ">New Zealand</option>
								<option value="AU">Australia</option>
								<option value="AF">Afghanistan</option>
								<option value="AX">Aland Islands</option>
								<option value="AL">Albania</option>
								<option value="DZ">Algeria</option>
								<option value="AS">American Samoa</option>
								<option>India</option>
							</select>
						</div>

						<div class="form-group">
							<label for="acc-pwd">street address:</label>
							<input type="text" class="form-control" id="pwd" placeholder="" name="pwd">
						</div>

						<div class="form-group">
							<label for="acc-pwd">city:</label>
							<input type="text" class="form-control" id="pwd" placeholder="" name="pwd">
						</div>

						<div class="form-group">
							<label for="text"> postal/zip code:</label>
							<input type="password" class="form-control" id="pwd" placeholder="" name="pwd">
						</div>

						<div class="form-group">
							<label for="text">phone number:</label>
							<input type="password" class="form-control" id="pwd" placeholder="" name="pwd">
						</div>
						<div class="pull-right">
						<button type="submit" class="btn btn-default acc-btn-save ">Cancel</button>

						<button type="submit" class="btn btn-default acc-btn-save">Save</button>
						</div>
					</form>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 col-sm-6  col-lg-6">
			<section class="cridet-add">
					<h2>Billing Address</h2>
					<div class="credit-bal-amount">
					<div>RS. 0.00</div>
					<p>We will automatically apply your credit towards your next purchase.</p>
					</div>
			</section>

			<section class="payment-info-sub">
			<h2>Payment Information</h2>
			<p>Please enter a valid billing address first before entering credit card information.</p>
			</section>
			</div>
		</div>
	</section>
</div>

@include('layout.footer')