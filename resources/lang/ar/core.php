<?php

return [
// Category wise search page keys
    'Best_Seller'             => 'الأفضل مبيعا',
    'Add_to_Cart'             => 'إضافة إلى سلة المشتروات',
    'Published'               => 'تم النشر',
    'Current_price'           => 'السعر الحالي',
    'Discount'                => 'الخصم',
    // End category wise search page keys
    // Home page keys
    'Latest_courses'          => 'آخر الدورات التدريبية',
    'Achieve_Your_Goals'      => 'حقق أهدافك',
    'Lifetime_access'         => 'اشتراك مدى الحياة',
    'Learn_on_your_schedule'  => 'تعلم في أي وقت',
    'Find_the_right_instructor_for_you'  => 'اعثر على أفضل المدربين',
    'Expert_instruction'      => 'مدربون معتمدون',
    'online_courses'          => 'كل شيء في وسائل الاعلام',
    'Explore_a_variety_of_fresh_topics'          => 'استكشاف افضل ما في وسائل الاعلام',
    'Keep_place_with_Change'  => 'حافظ على مكانتك العلمية',
    'Log_in_or_sign_up_to_get_courses_for'  => 'سجل دخولك أو اشترك لتحصل على دورات مقابل 960 دولار',
    'Click_Here'              => 'انقر هنا',
    'Great_Facilities'        => 'أوراق اعتماد الطالب',
    'Great_Facilities_content'=> 'سنساعدك على إتقان المهارات التي يبحث عنها أصحاب العمل وتسليط الضوء على إنجازاتك بشهادات ستفخر بمشاركتها.',
    'Our_Campus'              => '100% أونلاين',
    'Our_Campus_content'      => 'مع تواريخ بدء مرنة ، مواعيد استحقاق قابلة للتعديل ، واجهة سهلة الاستخدام ، يمكنك معرفة متى وأين تريد.',
    'Accomodation'            => 'التعلم المبتكر',
    'Accomodation_content'    => 'لقد ابدعنا في تجربة تعلم مصممة لمساعدتك على إتقان المهارات الحقيقية التي تمكنك البدء في التقدم نحو حياتك المهنية على الفور',
    // Detail page
    'Last_updated'            => 'آخر تحديث',
    'Description'             => 'الوصف',
    'Created_by'              => 'أنشأت بواسطة',
    'ratings'                 => 'التقييم',
    'students_enrolled'       => 'طلاب مسجلين',
    'Gift_This_Course'        => 'رشح المحتوى العلمي',
    'Buy_Now'                 => 'اشتري الآن',
    'Add_To_Cart'             => 'إضافة إلى السلة',
    'Day_Money_Back_Guarantee'=> 'يوم ضمان مع استعادة المبلغ المدفوع',
    'Compare_to_Other'        => 'مقارنة بالآخر',
    'Updated'                 => 'تم التحديث',
    'Featured_Review'         => 'استعراض الميزات',
    'Average_Rating'          => 'متوسط التقييم',    
    'Go_To_Cart'              => 'الذهاب إلى السلة',

    // End detail page keys

    // Footer keys
    'About_Us'               => 'من نحن',
    'Check_All_Our_Courses'     => 'تحقق من جميع دوراتنا',
    'Professional_Professors'     => 'الأساتذة المحترفون',
    'Our_Prices'     => 'الأسعار',
    'FREE'     => 'مجاناً',
    'Fast'     => 'سريع',
    'Latest_Courses'     => 'اخر الكورسات',
    'Contact_Details'     => 'بانات الاتصاال',
    'Instagram'     => 'انستقرام',
    'SMTC_online'     => 'SMTC أونلاين',
    'Become_a_PHP_Master_and_Make_Money'     => 'Become a PHP Master and Make Money',
    'Introduction_LearnPress_LMS_plugin'     => 'Introduction LearnPress - LMS plugin',
    'Terms'     => 'الشروط',
    'Learn_More_About_Us'     => 'تعرف علينا أكثر',
    'Privacy_Policy'     => 'سياسة الخصوصية',
    'Refund_Policy'     => 'سياسة الاسترجاع',

    // End footer keys

    // Login and registration form keys

    'Log_In_to_Your_Account'    =>  'تسجيل دخول',
    'Continue_with_Facebook'    =>  'الاستمرار بواسطة فيسبوك',    
    'Continue_with_Google'      =>  'الاستمرار بواسطة جوجل',    
    'OR'                        =>  'أو',
    'E_mail'                    =>  'البريد الإلكتروني',
    'password'                  =>  'كلمة المرور',
    'Remember_Me'               =>  'تذكرني',
    'Log_In'                    =>  'تسجيل دخول',
    'or'                        =>  'أو',
    'Forgot_Password'           =>  'نسيت كلمة المرور',
    'By_signing_up_you_agree_to_our'=>  'من خلال التوقيع ، فإنك توافق على موقعنا',
    'Terms_of_Use'=>  'شروط الاستخدام',
    'and'=>  'و',
    'Privacy_Policy'=>  'سياسة الخصوصية',
    'Dont_have_an_account'=>  'ليس لديك حساب؟',
    'Sign_up'=>  'Sign up',
    'by_signing_up_you_agree_to_our_terms_of_use_and_privacy_policy' => 'من خلال التسجيل ، فإنك توافق على شروط الاستخدام وسياسة الخصوصية الخاصة بنا',

    'Full_Name'=>  'الاسم كاملا',
    'Select_Country'=>  'اختر الدولة',
    'Enter_your_city'=>  'اختر المدينة',
    'Mobile_Number'=>  'رقم الجوال',
    'confirm_password'=>  'تأكيد كلمة المرور',

    // End login and registration form keys

    // Header keys
    'courses'                  =>'الكورسات',
    'smtc_location'            =>'احفظ المكان',
    'Select_Language'          =>'اختر اللغة',
    'Toggle_navigation'        =>'التنقل',
    'Categories'               =>'فئات التصنيف',
    'SMTC_for_University'      =>'SMTC for University',
    'Cart'                     =>'سلة المشتروات',
    'Recently_added_items'     =>'الأصناف التي تمت اضافتها مؤخرا',
    'Subtotal'                 =>'الإجمالي',
    'Checkout'                 =>'اتمام الشراء',
    'View_Cart'                =>'استعراض السلة',
    'Profile'                  =>'الملف الشخصي',
    'Logout'                   =>'تسجيل خروج',

    // End Header keys

    // Learner page

    'Payment_History'           => 'المدفوعات السابقة',    
    'l_Profile'                 =>'الملف الشخصي',
    'Add_information_about_yourself_to_share_on_your_profile' => 'معلومات خاصة بك',
    'l_Name'                    =>'الاسم',
    'l_Profession'              =>'الوظيفة',
    'l_Educational'             =>'الدرجة العلمية',
    'l_DOB'                     =>'تاريخ الميلاد',
    'l_Phone_Number'            =>'رقم الهاتف',
    'l_Profile_Image'           =>'الصورة الشخصية',
    'l_Address'                 =>'العنوان',
    'l_About_You'               =>'من أنت',
    'l_Payment_History'         =>'المدفوعات السابقة',
    'l_Payment_information_for_courses' =>'بيانات الدفع',
    'l_No'                      =>'لا',
    'l_Date'                    =>'التاريخ',
    'l_Course'                  =>'الكورس',
    'l_Amount'                  =>'السعر',
    'l_Status'                  =>'الحالة',
    'l_Purchased_Course'        =>'الكورسات الخاصة بي',
    'l_Project_Status'          =>'حالة المشروع',
    'l_Completed_Course'        =>'الكورسات المنتهاة',
    'l_Purchased_Courses'       =>'الكتب المشتراه',
    'l_Purchased_Courses_lists' =>'قائمة الكورسات',
    'l_Image'                   =>'صورة',
    'l_Teacher'                 =>'مدرس',
    'l_Category'                =>'الفئة',
    'l_Action'                  =>'الحدث',
    'l_Lesson_View'             =>'استعراض الدرس',
    'l_Lesson_View_for'         =>'استعراض الدرس لـ',
    'l_Lesson'                  =>'الدرس',
    'l_Key_Generation'          =>'إنشاء المفتاح',
    'l_Test_Key_Generation'     =>'اختبار المفتاح',
    'l_Test_Key_Generation_Form'=>'نموذج اختبار المفتاح',
    'l_Atempted'                =>'تمت المحاولة',
    'l_Key'                     =>'كود الاختبار',
    'l_Lesson'                  =>'الدرس',
    'l_Write_your_exam_here'    =>'اكتب اختبارك هنا',
    'l_Finished_Test'           =>'تم الانتهاء من الاختبار',


    // End learner page

    //Key generate page

    'l_Click_Generate_New_Key'     =>'اضفط على زر إنشاء مفتاح جديد',
    'l_Hide_form'                  =>'اخفاء النموذج',
    'l_Courses'                    =>'الكورسات',
    'l_Select_course'              =>'اختر كورس',
    'l_Lesson'                     =>'الدرس',
    'l_Select_course'              =>'اختر كورس',
    'l_Generate'                   =>'إنشاء',
    'l_Not_Yet_Used'               =>'لم تستخدم بعد',
    'l_Used'                       =>'استخدم من قبل',

    //WriteExam Page
    'l_Enter_Your_Lesson_Code'     =>'ادخل الكود',
    'l_Key_is_Invalid'             =>'المفتاح غير صالح للاستخدام',
    'l_Key_is_already_used'        =>'المفتاح تم استخدامه من قبل',
    'l_Submit'                     =>'ارسال',


    //finishedTest Page
    'l_Result'                     =>'النتائج',
    'l_Percentage'                 =>'نسبة مئوية',
    'l_View'                       =>'عرض',
    //ViewExam Page

    'l_Test_code_verification'     =>'تم التحقق من كود الاختبار',
    'l_Course_Details'             =>'تفاصيل الكورس',
    'l_Course_Name'                =>'اسم الكورس',
    'l_Course_Catogory'            =>'تصنيف الكورس',
    'l_Lesson_Details'             =>'تفاصيل الدرس',
    'l_Lesson_No'                  =>'رقم الدرس',
    'l_Lesson_Name'                =>'اسم الدرس',
    'l_Learner_Details'            =>'تفاصيل الطالب',
    'l_Verified'                   =>'تم التحقق',
    'l_Go'                         =>'اذهب',
    'l_Email'                      =>'البريد الإلكتروني',
    'l_Id'                         =>'الرقم',
    'l_Lesson'                     =>'الدرس',

    //LessonView Page
    'l_Title'                     =>'العنوان',
    'l_Description'               =>'الوصف',
    'l_Video'                     =>'الفيديو',
    'l_Lesson_Detailed_View'      =>'عرض تفاصيل الدرس',

    //manageProject Page
    'l_Project_Management'        =>'إدارة المشروع',
    'l_No_details_found'          =>'لا توجد تفاصيل متاحة',
    'l_Project_file'              =>'ملف المشروع',
    'l_uploaded_date'             =>'تاريخ الرفع',
    'l_waiting'                   =>'في الانتظار',
    'l_progress'                  =>'جاري العمل',
    'l_rejected'                  =>'مرفوض',
    'l_approved'                 =>'تم الموافقة',
    ];
