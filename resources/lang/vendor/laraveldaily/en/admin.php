<?php

return [

    // dashboard
    'dashboard-title'                       => 'Welcome to your project dashboard',

    // partials-header
    'partials-header-title'                 => 'KHAWARIZM Admin Panel',

    'partials-header-title'                 => 'E-book',

    // partials-sidebar
    'partials-sidebar-menu'                 => 'Menu',
    'partials-sidebar-users'                => 'Users',
    'partials-sidebar-roles'                => 'Roles',
    'partials-sidebar-user-actions'         => 'User actions',
    'partials-sidebar-logout'               => 'Logout',

    // partials-topbar
    'partials-topbar-title'                 => 'QuickAdmin en',

    // users-create
    'users-create-create_user'              => 'Create user',
    'users-create-name'                     => 'Name',
    'users-create-name_placeholder'         => 'Name',
    'users-create-email'                    => 'Email',
    'users-create-email_placeholder'        => 'Email',
    'users-create-password'                 => 'Password',
    'users-create-password_placeholder'     => 'Password',
    'users-create-role'                     => 'Role',
    'users-create-btncreate'                => 'Create',

    // users-edit
    'users-edit-edit_user'                  => 'Edit user',
    'users-edit-name'                       => 'Name',
    'users-edit-name_placeholder'           => 'Name',
    'users-edit-email'                      => 'Email',
    'users-edit-email_placeholder'          => 'Email',
    'users-edit-password'                   => 'Password',
    'users-edit-password_placeholder'       => 'Password',
    'users-edit-role'                       => 'Role',
    'users-edit-btnupdate'                  => 'Update',

    // users-index
    'users-index-add_new'                   => 'Add new',
    'users-index-users_list'                => 'Users list',
    'users-index-name'                      => 'Name',
    'users-index-edit'                      => 'Edit',
    'users-index-delete'                    => 'Delete',
    'users-index-are_you_sure'              => 'Are you sure?',
    'users-index-no_entries_found'          => 'No entries found',

    // users-controller
    'users-controller-successfully_created' => 'User was successfully created!',
    'users-controller-successfully_updated' => 'User was successfully updated!',
    'users-controller-successfully_deleted' => 'User was successfully deleted!',

    // roles-index
    'roles-index-add_new'                   => 'Add new',
    'roles-index-roles_list'                => 'Roles list',
    'roles-index-title'                     => 'Title',
    'roles-index-edit'                      => 'Edit',
    'roles-index-delete'                    => 'Delete',
    'roles-index-are_you_sure'              => 'Are you sure?',
    'roles-index-no_entries_found'          => 'No entries found',

    // roles-create
    'roles-create-create_role'              => 'Create role',
    'roles-create-title'                    => 'Title',
    'roles-create-title_placeholder'        => 'Title',
    'roles-create-btncreate'                => 'Create',

    // roles-edit
    'roles-edit-edit_role'                  => 'Edit role',
    'roles-edit-title'                      => 'Title',
    'roles-edit-title_placeholder'          => 'Title',
    'roles-edit-btnupdate'                  => 'Update',

    // roles-controller
    'roles-controller-successfully_created' => 'Role was successfully created!',
    'roles-controller-successfully_updated' => 'Role was successfully updated!',
    'roles-controller-successfully_deleted' => 'Role was successfully deleted!',

    // companies-create
    'companies-create-create_user'              => 'Create Universities',
    'companies-create-name'                     => 'Name',
    'companies-create-name_placeholder'         => 'Name',
    'companies-create-email'                    => 'Email',
    'companies-create-email_placeholder'        => 'Email',
    'companies-create-phone_number'             => 'Phone Number',
    'companies-create-phone_number_placeholder' => 'Phone Number',
    'companies-create-address'                  => 'Address', 
    'companies-create-address_placeholder'      => 'Address', 
    'companies-create-status'                   => 'Status',
    'companies-create-status_active'            => 'Active',
    'companies-create-status_inactive'          => 'Inactive',
    'companies-create-password'                 => 'Password',
    'companies-create-password_placeholder'     => 'Password',
    'companies-create-image'                    => 'image',
    'companies-create-role'                     => 'Role',
    'companies-create-btncreate'                => 'Create',

    // companies-edit
    'companies-edit-create_user'                => 'Edit Universities',
    'companies-edit-name'                       => 'Name',
    'companies-edit-name_placeholder'           => 'Name',
    'companies-edit-email'                      => 'Email',
    'companies-edit-email_placeholder'          => 'Email',
    'companies-edit-phone_number'               => 'Phone Number',
    'companies-edit-phone_number_placeholder'   => 'Phone Number',
    'companies-edit-address'                    => 'Address', 
    'companies-edit-address_placeholder'        => 'Address', 
    'companies-edit-status'                     => 'Status',
    'companies-edit-status_active'              => 'Active',
    'companies-edit-status_inactive'            => 'Inactive',
    'companies-edit-password'                   => 'Password',
    'companies-edit-password_placeholder'       => 'Password',
    'companies-edit-image'                      => 'image',
    'companies-edit-role'                       => 'Role',
    'companies-edit-btnedit'                    => 'Edit',

    // companies-index
    'companies-index-add_new'                   => 'Add new',
    'companies-index-companies_list'            => 'Universities list',
    'companies-index-avatar'                    => 'Avatar',
    'companies-index-name'                      => 'Name',
    'companies-index-edit'                      => 'Edit',
    'companies-index-email'                     => 'Email',    
    'companies-index-phone_number'              => 'Phone Number',
    'companies-index-address'                   => 'Address', 
    'companies-index-status'                    => 'Status', 
    'companies-index-delete'                    => 'Delete',
    'companies-index-are_you_sure'              => 'Are you sure?',
    'companies-index-no_entries_found'          => 'No entries found',

    // companies-controller
    'companies-controller-successfully_created' => 'Universities was successfully created!',
    'companies-controller-successfully_updated' => 'Universities was successfully updated!',
    'companies-controller-successfully_deleted' => 'Universities was successfully deleted!',

    // Teacher-create
    'teacher-create-create_user'                => 'Create teacher',
    'teacher-create-name'                       => 'Name',
    'teacher-create-name_placeholder'           => 'Name',
    'teacher-create-email'                      => 'Email',
    'teacher-create-email_placeholder'          => 'Email',
    'teacher-create-dob'                        => 'DOB',
    'teacher-create-dob_placeholder'            => 'DOB',
    'teacher-create-educational'                => 'Educational',
    'teacher-create-educational_placeholder'    => 'Educational',
    'teacher-create-professional'               => 'Professional',
    'teacher-create-professional_placeholder'   => 'Professional',
    'teacher-create-phone_number'               => 'Phone Number',
    'teacher-create-phone_number_placeholder'   => 'Phone Number',
    'teacher-create-address'                    => 'Address', 
    'teacher-create-address_placeholder'        => 'Address', 
    'teacher-create-status'                     => 'Status',
    'teacher-create-status_active'              => 'Active',
    'teacher-create-status_inactive'            => 'Inactive',
    'teacher-create-password'                   => 'Password',
    'teacher-create-password_placeholder'       => 'Password',
    'teacher-create-image'                      => 'image',
    'teacher-create-role'                       => 'Role',
    'teacher-create-btncreate'                  => 'Create',

    // Teacher-edit
    'teacher-edit-create_user'                  => 'Edit teacher',
    'teacher-edit-name'                         => 'Name',
    'teacher-edit-name_placeholder'             => 'Name',
    'teacher-edit-email'                        => 'Email',
    'teacher-edit-email_placeholder'            => 'Email',
    'teacher-edit-phone_number'                 => 'Phone Number',
    'teacher-edit-phone_number_placeholder'     => 'Phone Number',
    'teacher-edit-address'                      => 'Address', 
    'teacher-edit-address_placeholder'          => 'Address', 
    'teacher-edit-status'                       => 'Status',
    'teacher-edit-status_active'                => 'Active',
    'teacher-edit-status_inactive'              => 'Inactive',
    'teacher-edit-password'                     => 'Password',
    'teacher-edit-password_placeholder'         => 'Password',
    'teacher-edit-image'                        => 'image',
    'teacher-edit-role'                         => 'Role',
    'teacher-edit-btnedit'                      => 'Edit',

    // Teacher-index
    'teacher-index-add_new'                     => 'Add new',
    'teacher-index-companies_list'              => 'Teachers list',
    'teacher-index-avatar'                      => 'Avatar',
    'teacher-index-name'                        => 'Name',
    'teacher-index-edit'                        => 'Edit',
    'teacher-index-email'                       => 'Email',
    'teacher-index-dob'                         => 'DOB',
    'teacher-index-educational'                 => 'Educational',
    'teacher-index-professional'                => 'Professional',    
    'teacher-index-phone_number'                => 'Phone Number',
    'teacher-index-address'                     => 'Address', 
    'teacher-index-status'                      => 'Status', 
    'teacher-index-delete'                      => 'Delete',
    'teacher-index-are_you_sure'                => 'Are you sure?',
    'teacher-index-no_entries_found'            => 'No entries found',

    // Teacher-controller
    'teacher-controller-successfully_created'   => 'Teacher was successfully created!',
    'teacher-controller-successfully_updated'   => 'Teacher was successfully updated!',
    'teacher-controller-successfully_deleted'   => 'Teacher was successfully deleted!',

    // Profile-edit
    'profile-edit-create_user'                  => 'Edit profile',
    'profile-edit-name'                         => 'Name',
    'profile-edit-name_placeholder'             => 'Name',
    'profile-edit-email'                        => 'Email',
    'profile-edit-email_placeholder'            => 'Email',
    'profile-edit-username'                     => 'User Name', 
    'profile-edit-username_placeholder'         => 'User Name',
    'profile-edit-image'                        => 'image',
    'profile-edit-change-pwd'                   => 'Change Password',
    'profile-edit-old-pwd'                      => 'Old Password',
    'profile-edit-confirm-pwd'                  => 'Confirm Password',
    'profile-edit-new-pwd'                      => 'new Password',
    'profile-edit-role'                         => 'Role',
    'profile-edit-btnedit'                      => 'Save',

        // Profile-controller
    'profile-controller-successfully_updated'   => 'Profile was successfully updated!',


    // Lessions-index
    'lessions-index-add_new'                    => 'Add new',
    'lessions-index-lessions_list'              => 'Lessons list',
    'lessions-index-id'                         => 'Id',
    'lessions-index-title'                      => 'Title',
    'lessions-index-test-type'                      => 'Test type',
    'lessions-index-edit'                       => 'Edit',
    'lessions-index-description'                => 'Description', 
    'lessions-index-status'                     => 'Status', 
    'lessions-index-delete'                     => 'Delete',
    'lessions-index-are_you_sure'               => 'Are you sure?',
    'lessions-index-no_entries_found'           => 'No entries found',


    // Lessions-create
    'lessions-create-create_user'              => 'Create lesson',
    'lessions-create-name'                     => 'Lesson name',
    'lessions-create-name_placeholder'         => 'Lesson name',
    'lessions-create-description'              => 'Lesson description',
    'lessions-create-description_placeholder'  => 'Lesson description',
    'lessions-create-test-type'                => 'Test type',
    'lessions-create-test-type_placeholder'    => 'Test type',
    'lessions-create-video'                    => 'Video upload',
    'lessions-create-role'                     => 'Role',
    'lessions-create-btncreate'                => 'Create',

 // Lessions-edit
    'lessions-edit-create_user'              => 'Edit lesson',
    'lessions-edit-name'                     => 'Lesson name',
    'lessions-edit-name_placeholder'         => 'Lesson name',
    'lessions-edit-description'              => 'Lesson description',
    'lessions-edit-description_placeholder'  => 'Lesson description',
    'lessions-edit-test-type'                => 'Test type',
    'lessions-edit-test-type_placeholder'    => 'Test type',
    'lessions-edit-video'                    => 'Video upload',

// Question-index
    'questions-index-add_new'                    => 'Add new',
    'questions-index-lessions_list'              => 'Questions list',
    'questions-index-id'                         => 'Id',
    'questions-index-question'                   => 'Question', 
    'questions-index-delete'                     => 'Delete',
    'questions-index-are_you_sure'               => 'Are you sure?',
    'questions-index-no_entries_found'           => 'No entries found',

// Question-create
    'questions-create-create_user'                => 'Create question',
    'questions-create-lessions_list'              => 'Questions list',
    'questions-create-question'                         => 'Question',
    'questions-create-question_placeholder'                   => 'Question', 
    'questions-create-mark'                     => 'Mark',
    'questions-create-mark_placeholder'               => 'Mark',
     'questions-create-btncreate'                => 'Create',
     'questions-create-answer'                => 'Answers',
     'questions-create-answer_placeholder'                => 'Answers',
];

