<?php
return [

  // generic   
  'whoops'  => 'Oops !',
  'some_problems_with_input' => 'there are some problems with input!',

  // auth-login
  'login-login' => 'Log In to Your Account!',
  'login-email' => 'E-mail',
  'login-password' => 'Password',
  'login-remember_me' => 'Remember Me',
  'login-btnlogin' => 'Log In',
  'login-btnfacebook' => 'Continue with Facebook',
  'login-btngoogle' => 'Continue with Google',
  'login-forgot-password' => 'Forgot Password',
  'login-agree' => 'By signing up, you agree to our',
  'login-terms-use' => 'Terms of Use',
  'login-privacy-policy' => 'Privacy Policy',
  'login-noaccount' => "Don't have an account?",
  'login-signup' => 'Sign up',

  //auth-signup
  'login-confirm-password' => 'Confirm Password',
  'login-fname' => 'First Name',
  'login-lname' => 'Last Name',
  'login-account' => "Have an account?",

  // auth-password
  'password-reset_password' => 'Reset password',
  'password-email' => 'Email',
  'password-btnsend_password' => 'Send Password Reset Link',

  // auth-reset
  'reset-reset_password' => 'Reset password',
  'reset-email' => 'Email',
  'reset-password' => 'Password',  
  'reset-confirm_password' => 'Confirm password',  
  'reset-btnreset_password' => 'Reset password',

];