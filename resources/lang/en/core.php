<?php

return [
// Category wise search page keys
    'Best_Seller'             => 'Best Seller',
    'Add_to_Cart'             => 'Add to Cart Seller',
    'Published'               => 'Published',
    'Current_price'           => 'Current price',
    'Discount'                => 'Discount',
    // End category wise search page keys
    // Home page keys
    'Latest_courses'          => 'Trending Now in eBooks',
    'Achieve_Your_Goals'      => 'Achieve Your Goals',
    'Lifetime_access'         => 'Easy access',
    'Learn_on_your_schedule'  => 'Learn anytime anyday',
    'Find_the_right_instructor_for_you'  => 'Brings out the best in your skills',
    'Expert_instruction'      => 'Certified Trainers',
    'online_courses'          => 'Everything in media',
    'Explore_a_variety_of_fresh_topics'          => 'Explore whats best in media',
    'Keep_place_with_Change'  => 'Keep place with Change',
    'Log_in_or_sign_up_to_get_courses_for'  => 'Log in or sign up to get courses for $960',
    'Click_Here'              => 'Click Here',
    'Great_Facilities'        => 'Indemand Credentials',
    'Great_Facilities_content'=> 'We\'ll help you master the skills employers are looking for and highlight your accomplishments with certificates you\'ll be proud to share.',
    'Our_Campus'              => '100% online',
    'Our_Campus_content'      => 'With flexible start dates, adjustable due dates, and easy to use interface, you can learn when and where you want.',
    'Accomodation'            => 'Innovative learning',
    'Accomodation_content'    => 'We\'ve created an intuitive learning experience designed to help you master real-world skills that you can start applying towards your career right away.',
    // Detail page
    'Last_updated'            => 'Last updated',
    'Description'             => 'Description',
    'Created_by'              => 'Created by',
    'ratings'                 => 'ratings',
    'students_enrolled'       => 'students enrolled',
    'Gift_This_Course'        => 'Gift This Course',
    'Buy_Now'                 => 'Buy Now',
    'Add_To_Cart'             => 'Add To Cart',
    'Day_Money_Back_Guarantee'=> 'Day Money-Back Guarantee',
    'Compare_to_Other'        => 'Compare to Other',
    'Updated'                 => 'Updated',
    'Featured_Review'         => 'Featured Review',
    'Average_Rating'          => 'Average Rating',    
    'Go_To_Cart'              =>'Go To Cart',

    // End detail page keys

    // Footer keys
    'About_Us'               => 'All about Khawarizm',
    'Check_All_Our_Courses'     => 'About Us',
    'Professional_Professors'     => 'Professional Professors',
    'Our_Prices'     => 'Our Prices',
    'FREE'     => 'FREE',
    'Fast'     => 'Fast',
    'Latest_Courses'     => 'Opportunities',
    'Contact_Details'     => 'Contact Details',
    'Instagram'     => 'Stay Connected',
    'SMTC_online'     => 'e-BOOK',
    'Become_a_PHP_Master_and_Make_Money'     => 'Become a PHP Master and Make Money',
    'Introduction_LearnPress_LMS_plugin'     => 'Introduction LearnPress - LMS plugin',
    'Terms'     => 'Terms',
    'Learn_More_About_Us'     => 'Learn More About Us',
    'Privacy_Policy'     => 'Privacy Policy',
    'Refund_Policy'     => 'Refund Policy',

    // End footer keys

    // Login and registration form keys

    'Log_In_to_Your_Account'    =>  'Log In to Your Account',
    'Continue_with_Facebook'    =>  'Continue with Facebook',
    'Continue_with_Google'      =>  'Continue with Google',    
    'OR'                        =>  'OR',
    'E_mail'                    =>  'E-mail',
    'password'                  =>  'password',
    'Remember_Me'               =>  'Remember Me',
    'Log_In'                    =>  'Continue',
    'or'                        =>  'or',
    'Forgot_Password'           =>  'Forgot Password',
    'By_signing_up_you_agree_to_our'=>  'By signing up, you agree to our',
    'Terms_of_Use'=>  'Terms of Use',
    'and'=>  'and',
    'Privacy_Policy'=>  'Privacy Policy',
    'Dont_have_an_account'=>  'Don\'t have an account?',
    'Do_have_an_account'   => 'Do you have an account',
    'Sign_up'=>  'Sign up',
    'Sign_in'=> 'Sign In',
    'by_signing_up_you_agree_to_our_terms_of_use_and_privacy_policy' => 'By signing up, you agree to our Terms of Use and Privacy Policy',

    'Full_Name'=>  'Full Name',
    'Select_Country'=>  'Select Country',
    'Enter_your_city'=>  'Enter your city',
    'Mobile_Number'=>  'Mobile Number',
    'confirm_password'=>  'confirm password',

    // End login and registration form keys

    // Header keys
    'courses'                  =>'Courses',
    'smtc_location'            =>'smtc location',
    'Select_Language'          =>'Select Language',
    'Toggle_navigation'        =>'Toggle navigation',
    'Categories'               =>'Categories',
    'SMTC_for_University'      =>'SMTC for University',
    'Cart'                     =>'Cart',
    'Recently_added_items'     =>'Recently added items',
    'Subtotal'                 =>'Subtotal',
    'Checkout'                 =>'Checkout',
    'View_Cart'                =>'View Cart',
    'Profile'                  =>'Profile',
    'Logout'                   =>'Logout',

    // End Header keys

    // Learner page

    'Payment_History'           => 'Payment History',    
    'l_Profile'                 =>'Profile',
    'Add_information_about_yourself_to_share_on_your_profile' => 'Add information about yourself to share on your profile',
    'l_Name'                    =>'Name',
    'l_Profession'              =>'Profession',
    'l_Educational'             =>'Educational',
    'l_DOB'                     =>'DOB',
    'l_Phone_Number'            =>'Phone Number',
    'l_Profile_Image'           =>'Profile Image',
    'l_Address'                 =>'Address',
    'l_About_You'               =>'About You',
    'l_Payment_History'         =>'Payment History',
    'l_Payment_information_for_courses' =>'Payment information for courses',
    'l_No'                      =>'No',
    'l_Date'                    =>'Date',
    'l_Course'                  =>'Course',
    'l_Amount'                  =>'Amount',
    'l_Status'                  =>'Status',
    'l_Purchased_Course'        =>'Purchased Course(s)',
    'l_Project_Status'          =>'Project Status',
    'l_Completed_Course'        =>'Completed Course(s)',
    'l_Purchased_Courses'       =>'Purchased Course(s)',
    'l_Purchased_Courses_lists' =>'Purchased Course lists',
    'l_Image'                   =>'Image',
    'l_Teacher'                 =>'Teacher',
    'l_Category'                =>'Category',
    'l_Action'                  =>'Action',
    'l_Lesson_View'             =>'Lesson View',
    'l_Lesson_View_for'         =>'Lesson View for',
    'l_Lesson'                  =>'Lesson',
    'l_Key_Generation'          =>'Key Generation',
    'l_Test_Key_Generation'     =>'Test Key Generation',
    'l_Test_Key_Generation_Form'=>'Test Key Generation Form',
    'l_Atempted'                =>'Attempted',
    'l_Key'                     =>'Test Code',
    'l_Lesson'                  =>'Lesson',
    'l_Write_your_exam_here'    =>'Write your exam here',
    'l_Finished_Test'           =>'Finished Test',


    // End learner page

    //Key generate page

    'l_Click_Generate_New_Key'     =>'Click Generate New Key',
    'l_Hide_form'                  =>'Hide form',
    'l_Courses'                    =>'Courses',
    'l_Select_course'              =>'Select course',
    'l_Lesson'                     =>'Lesson',
    'l_Select_course'              =>'Select course',
    'l_Generate'                   =>'Generate',
    'l_Not_Yet_Used'               =>'Not Yet Used',
    'l_Used'                       =>'Used',

    //WriteExam Page
    'l_Enter_Your_Lesson_Code'     =>'Enter Your Test Code',
    'l_Key_is_Invalid'             =>'Key is Invalid',
    'l_Key_is_already_used'        =>'Key is already used',
    'l_Submit'                     =>'Submit',


    //finishedTest Page
    'l_Result'                     =>'Result',
    'l_Percentage'                 =>'Percentage',
    'l_View'                       =>'View',
    //ViewExam Page

    'l_Test_code_verification'     =>'Test code verification',
    'l_Course_Details'             =>'Course Details',
    'l_Course_Name'                =>'Course Name',
    'l_Course_Catogory'            =>'Course Catogory',
    'l_Lesson_Details'             =>'Lesson Details',
    'l_Lesson_No'                  =>'Lesson No',
    'l_Lesson_Name'                =>'Lesson Name',
    'l_Learner_Details'            =>'Learner Details',
    'l_Verified'                   =>'Verified',
    'l_Go'                         =>'Go',
    'l_Email'                      =>'E-mail',
    'l_Id'                         =>'Id',
    'l_Lesson'                     =>'Lesson',

    //LessonView Page
    'l_Title'                     =>'Title',
    'l_Description'               =>'Description',
    'l_Video'                     =>'Video',
    'l_Lesson_Detailed_View'      =>'Lesson detailed view',

    //manageProject Page
    'l_Project_Management'        =>'Project Management',
    'l_No_details_found'          =>'No details found',
    'l_Project_file'              =>'Project File',
    'l_uploaded_date'             =>'Uploaded Date',
    'l_waiting'                   =>'Waiting',
    'l_progress'                  =>'Progress',
    'l_rejected'                  =>'Rejected',
    'l_approved'                  =>'Approved',
    'self-publish'                =>'Self publish',
    'affiliates'                  =>'Affiliates',
    'job-openings'                =>'Job Openings',
    'contact-us'                  =>'Contact Us',
    'paid_ticket*Free'            =>'paid ticket*Free for free tickets',
    'curent-plan'                 =>'Current Active Plan',
    'set-active'                  =>'Set as Active',





    ];
