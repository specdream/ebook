$(document).ready(function() {

$('.owl-carousel').owlCarousel({
    autoplay: false,
    loop:true,
    margin:10,
    responsiveClass:true,
    navigationText: ["<img src='sdream/img/owl-back.png'>", "<img src='sdream/img/owl-next.png'>"],
    navigationClass:['owl-arow-prev', 'owl-arow-next'],
    dots: false,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:2,
            nav:false
        },
        1000:{
            items:6,
            nav:true,
            loop:false
        }
    }
})


  $(window).scroll(function(){
    var scroll = $(window).scrollTop();
      if (scroll > 100) {
        $(".navbar-fixed-top, .stylish-input-group>input, .input-group-addon").css("background" , "#fff");
      }

      else{
          $(".navbar-fixed-top, .stylish-input-group>input, .input-group-addon").css("background" , "#fff0");   
      }
  })

  $("#purchase-input").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myPurchaseTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });

   
});






  
