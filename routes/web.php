<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::match(['get', 'post'], '/', function () {
//     return view('home');
// });
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
// Route::get('/home', function () {
//     return view('home');
// });

// Admin 
//Coupon
Route::get('genCoupon', 'Admin\CouponController@genCoupon');
Route::post('coupon/store', 'Admin\CouponController@store');

Route::view('/welcome', 'welcome');
Route::view('/dashboard', 'dashboard');
Route::view('/course_goal', 'coursegoal');
Route::view('/curriculum', 'curriculum');
Route::view('/price_coupon', 'price_coupon');

Route::view('/viewCart', 'site/view_cart');
Route::view('invoice', 'emails/invoice');
Route::view('/courses', 'courses');
Route::view('/messages', 'automaticmessage');
Route::view('/detail', 'detail');
Route::view('/edit-profile', 'profile');
Route::view('/payment-history', 'paymenthistory');
Route::view('/course-list', 'course_list');
Route::view('/question', 'question');
Route::view('/lesson', 'lesson');
Route::view('/lesson-detail', 'lesson-detail');
Route::view('/contact-us', 'contact');
Route::view('/university', 'university');
Route::view('/about-us', 'about-us');
Route::view('/terms', 'terms');
Route::view('/privacy ', 'privacy');
Route::view('/refund ', 'refundpolicy');

Route::view('/addpage ', 'addpage');
Route::view('/coupanpage ', 'coupanpage');
Route::view('/chatting ', 'chatting');





Route::view('/bookcategories', 'bookcategories');


//Route::view('/student-mybook', 'student-mybook');
//Route::view('/student-mybook', 'student-mybook');
Route::view('/book-review', 'book-review');
Route::view('/book-slider', 'book-slider');
Route::view('/404', '404');


// Access Key
Route::get('admin/bookKeys/{num}/{place}','Admin\ManageBookAccessKeyController@bookKeys');
Route::get('admin/createBookKeys/{num}','Admin\ManageBookAccessKeyController@create');
Route::post('admin/storeBookKey','Admin\ManageBookAccessKeyController@store');
Route::get('UniqueRandomNumbersWithinRange/{lng}','Admin\ManageBookAccessKeyController@UniqueRandomNumbersWithinRange');
Route::get('admin/downloadBookKeys/{num}/{place}','Admin\ManageBookAccessKeyController@downloadBookKeys');

// ccavenue payment gateway
Route::view('/ccavenue', 'ccavenue');

// Learner
Route::get('learner/profile', 'LearnerController@profile');
Route::post('learner/update/{id}', 'LearnerController@update');
Route::get('learner/paymenthistory', 'LearnerController@paymenthistory');
Route::get('learner/purchasedcourse', 'LearnerController@purchasedcourse');
Route::get('learner_detail/{id}', 'LearnerController@learnerDetail');
Route::get('learner/purchasedcourse/lession/{id}', 'LearnerController@learnerLesson');
Route::get('learner/purchasedcourse/lessonView/{cid}/{lid}', 'LearnerController@lessonView');
Route::get('learner/keygenerate', 'LearnerController@keyGenerate');
Route::post('learner/keygenerateadd', 'LearnerController@keyGenerateAdd');
Route::post('learner/getLesson', 'LearnerController@getLesson');
Route::get('learner/writeExam', 'LearnerController@writeExam');
Route::post('learner/examKeyValidation', 'LearnerController@examKeyValidation');
Route::get('learner/viewExam/{id}', 'LearnerController@viewExam');
Route::post('learner/testPage/{id}', 'ExamController@testPage');
Route::post('learner/textSubmit', 'ExamController@textSubmit');
Route::get('learner/lessonResult/{id}', 'ExamController@lessonResult');
Route::get('learner/finishedTest', 'ExamController@finishedTest');
Route::get('learner/completedCourse', 'LearnerController@completedCourse');
Route::get('learner/completedCourse/manageProject/{cid}', 'LearnerController@manageProject');

// Home page Autocomplete
Route::get('getCourseTitle', 'HomeController@courseTitle');
// Category Wise Search
Route::get('catCourse/{id}', 'HomeController@catCourse');

//Multi language
Route::get('lang/{lang}', 'UsersController@lang');

//Cart page
// Route::get('viewCart', 'HomeController@viewCart');

//corn function
Route::get('currencyDefaultValue', 'cronController@currencyDefaultValue');


// Detail Page
Route::get('detail/{id}', 'HomeController@detail');
Route::post('addToCart', 'cartController@addToCart');
Route::post('removeToCart', 'cartController@removeToCart');
Route::get('book-flip/{num}', 'HomeController@bookPreview');
Route::post('addToWish', 'cartController@addToWish');
Route::post('removeToWish', 'cartController@removeToWish');

//College Detail
Route::get('college_details/{id}', 'HomeController@college_details');

//checkout
Route::get('checkout', 'paymentController@index');
Route::post('billing/address', 'paymentController@billingAddress');
Route::get('paymentPaytabs', 'paymentController@paymentPaytabs');
Route::post('paymentSuccess', 'paymentController@paymentSuccess');
Route::get('paymentCancel/{message}', 'paymentController@paymentCancel');
Route::get('paymentSuccessInvoice', 'paymentController@paymentSuccessInvoice');

//Checkout college
Route::get('paymentPaytabsColg', 'paymentController@paymentPaytabsColg');
Route::get('paymentSuccessColg/{num}', 'paymentController@paymentSuccessColg');

//wishlist
Route::get('wishlist', 'cartController@wishlist');
Route::get('wishlistCollege', 'cartController@wishlistCollege');


// Social Login
Route::get('auth/facebook', 'Auth\AuthController@redirectToProvider');
Route::get('auth/facebook/callback', 'Auth\AuthController@handleProviderCallback');
Route::get('auth/google', 'Auth\AuthController@redirectToGoogle');
Route::get('auth/google/callback', 'Auth\AuthController@handleGoogleCallback');


Route::post('admin/tb_courses/approve/{id}', 'Admin\Tb_CoursesController@approve');
Route::post('admin/tb_courses/complete/{id}', 'Admin\Tb_CoursesController@complete');
Route::post('admin/profile/updatepwd/{id}', 'Admin\ProfileController@updatepwd');

Route::get('admin/tb_courses/view/{id}', 'Admin\Tb_CoursesController@view');
Route::get('admin/tb_courses/view/{id}/{lang}', 'Admin\Tb_CoursesController@view_lang');

Route::get('admin/lession/{course_id}', 'Admin\LessionController@index');
Route::get('admin/lession/create/{course_id}', 'Admin\LessionController@create');
Route::post('admin/lession/store/{course_id}', 'Admin\LessionController@store');
Route::get('admin/lession/{id}/edit/{course_id}', 'Admin\LessionController@edit');
Route::post('admin/lession/update/{id}/{course_id}', 'Admin\LessionController@update');
Route::post('admin/lession/destroy/{id}/{course_id}', 'Admin\LessionController@destroy');

Route::get('admin/question/{course_id}/{lession_id}', 'Admin\QuestionController@index');
Route::get('admin/question/create/{course_id}/{lession_id}', 'Admin\QuestionController@create');
Route::post('admin/question/store/{course_id}/{lession_id}', 'Admin\QuestionController@store');
Route::get('admin/question/{id}/edit/{course_id}/{lession_id}', 'Admin\QuestionController@edit');
Route::post('admin/question/update/{id}/{course_id}/{lession_id}', 'Admin\QuestionController@update');
Route::post('admin/question/destroy/{id}/{course_id}/{lession_id}', 'Admin\QuestionController@destroy');

Route::post('admin/tb_courses/checkkey', 'Admin\Tb_CoursesController@checkkey');
Route::post('admin/tb_courses/checkkey_edit', 'Admin\Tb_CoursesController@checkkey_edit');
Route::post('admin/tb_courses/acheckkey_edit', 'Admin\Tb_CoursesController@acheckkey_edit');
Route::post('admin/tb_courses/lcheckkey_edit', 'Admin\Tb_CoursesController@lcheckkey_edit');
Route::post('admin/tb_courses/course_reject', 'Admin\Tb_CoursesController@course_reject');


Route::get('admin/tb_course_assign/create', 'Admin\Tb_Course_AssignController@create');
Route::post('admin/tb_course_assign/store', 'Admin\Tb_Course_AssignController@store');
Route::post('admin/tb_course_assign/update/{id}', 'Admin\Tb_Course_AssignController@update');
Route::post('admin/tb_course_assign/destroy/{id}', 'Admin\Tb_Course_AssignController@destroy');

// Support System 
Route::get('admin/support_system/create', 'Admin\Support_SystemController@create');
Route::post('admin/support_system/store', 'Admin\Support_SystemController@store');
Route::post('admin/support_system/hold/{id}', 'Admin\Support_SystemController@hold');
Route::get('admin/support_system/reply/{id}', 'Admin\Support_SystemController@reply');
Route::post('admin/support_system/closed/{id}', 'Admin\Support_SystemController@closed');
Route::post('admin/support_system/chat/{id}', 'Admin\Support_SystemController@chat');

// Author section

Route::get('admin/authorBookCreate', 'Admin\AuthorBookCreateController@create');
Route::post('admin/authorBookStore', 'Admin\AuthorBookCreateController@store');

Route::get('admin/bookCreate', 'Admin\AuthorBooksController@create');
Route::post('admin/bookStore', 'Admin\AuthorBooksController@store');

Route::get('admin/bookLangCreate', 'Admin\BookLanguageController@create');


//Admin book language create
Route::get('admin/createBookLang', 'Admin\BookLanguageController@create');
Route::post('admin/createBookLang', 'Admin\BookLanguageController@store');
Route::get('admin/editBookLang/{num}', 'Admin\BookLanguageController@edit');
Route::post('admin/editBookLang/{num}', 'Admin\BookLanguageController@edit');
Route::get('admin/deleteBookLang/{num}', 'Admin\BookLanguageController@delete');

//Book categorie
Route::get('admin/createBookCatg', 'Admin\AuthoBookCatgController@create');
Route::post('admin/createBookCatg', 'Admin\AuthoBookCatgController@store');
Route::get('admin/editBookCatg/{num}', 'Admin\AuthoBookCatgController@edit');
Route::post('admin/editBookCatg/{num}', 'Admin\AuthoBookCatgController@edit');
Route::get('admin/deleteBookCatg/{num}', 'Admin\AuthoBookCatgController@delete');

//Book sub categorie
Route::get('admin/createBookSubCatg', 'Admin\AuthoBookSubCatgController@create');
Route::post('admin/createBookSubCatg', 'Admin\AuthoBookSubCatgController@store');
Route::get('admin/editBookSubCatg/{num}', 'Admin\AuthoBookSubCatgController@edit');
Route::post('admin/editBookSubCatg/{num}', 'Admin\AuthoBookSubCatgController@edit');
Route::get('admin/deleteBookSubCatg/{num}', 'Admin\AuthoBookSubCatgController@delete');


/*Service URL*/
Route::post('mobile/user/register', 'mobile\UserController@register');
Route::post('mobile/user/login', 'mobile\UserController@login');
Route::post('mobile/user/catg','mobile\UserController@Catagory');
Route::post('mobile/user/subcatg','mobile\UserController@SubCatagory');
Route::post('mobile/user/listroles','mobile\UserController@ListRoles');
Route::post('mobile/user/videoupload','mobile\UserController@VideoUpload');
Route::post('mobile/user/countrycitylist', 'mobile\UserController@postCountrycitylist');
Route::post('mobile/user/editprofile', 'mobile\UserController@postEditprofile');
Route::post('mobile/user/profileview', 'mobile\UserController@postProfileview');
Route::post('mobile/user/booklanguage', 'mobile\UserController@postBooklanguage');
Route::post('mobile/user/uploadbook', 'mobile\UserController@postUploadbook');
Route::post('mobile/user/detailpage', 'mobile\UserController@postDetailpage');
Route::post('mobile/user/search', 'mobile\UserController@postSearch');
Route::post('mobile/user/viewbooks', 'mobile\UserController@postViewbooks');
Route::post('mobile/user/filter', 'mobile\UserController@postFilter');
Route::post('mobile/user/userbook', 'mobile\UserController@postUserbook');
Route::post('mobile/user/addwish', 'mobile\UserController@postAddwish');
Route::post('mobile/user/wishlist', 'mobile\UserController@postWishlist');
Route::post('mobile/user/addcart', 'mobile\UserController@postAddtocart');
Route::post('mobile/user/showcart', 'mobile\UserController@postShowcart');
Route::post('mobile/user/removecart', 'mobile\UserController@postRemovecart');






Route::get('getSubcatg', 'Admin\AuthoBookSubCatgController@subCatg');

// Author access key
Route::get('admin/accessKey/create', 'Admin\GenerateAccessKeyController@create');

//Author book create
Route::any('authorbooks/edit/{num}', 'Admin\AuthorBooksController@edit');
Route::get('authorbooks/delete/{num}', 'Admin\AuthorBooksController@delete');
Route::post('authorBook/edit/file', 'Admin\AuthorBooksController@fileUpload');
Route::get('authorbooks/deleteFile/{id}/{book_id}/{type}', 'Admin\AuthorBooksController@deleteFile');
Route::post('authorBook/edit/banner', 'Admin\AuthorBooksController@uploadBanner');

////////////Cron////
Route::get('currencyDefaultValue', 'cronController@currencyDefaultValue');

////Search
Route::get('search ', 'searchController@index');
Route::get('search/suggestion ', 'searchController@suggestion');
Route::get('college-book', 'searchController@collegeBook');

//Account student
Route::get('purchase-history', 'accountController@purchase_history');
Route::get('invoicePopup/{uniq_id}', 'accountController@invoicePopup');
Route::get('invoicePopupAdmin/{uniq_id}', 'accountController@invoicePopupAdmin');

Route::get('college-purchase-history', 'accountController@college_purchase_history');
Route::get('collegeInvoicePopup/{uniq_id}', 'accountController@collegeInvoicePopup');
Route::get('collegeInvoicePopupAdmin/{uniq_id}', 'accountController@collegeInvoicePopupAdmin');

Route::get('myaccount ', 'accountController@myaccount');
Route::post('student/profile', 'accountController@profile');
Route::get('mybook ', 'accountController@mybook');
Route::get('inbox', 'accountController@inbox');
Route::view('/payment_info ', 'payment_info');

Route::get('student-mybook ', 'accountController@studentMybook');

// College book login
Route::post('bookLogin/{num}', 'accountController@bookLogin');

//Author plan management
Route::get('edit/plan/{num}', 'Admin\AuthorPlansController@edit');
Route::post('update/plan', 'Admin\AuthorPlansController@update');
Route::post('setPlan', 'Admin\AuthorAgreementsController@setPlan');

//Admin semesters
Route::get('admin/createSemester', 'Admin\SemestersController@create');
Route::post('admin/createSemester', 'Admin\SemestersController@store');
Route::get('admin/editSemester/{num}', 'Admin\SemestersController@edit');
Route::post('admin/editSemester/{num}', 'Admin\SemestersController@edit');
Route::get('admin/deleteSemester/{num}', 'Admin\SemestersController@delete');

//Admin degrees
Route::get('admin/createdegree', 'Admin\DegreesController@create');
Route::post('admin/createdegree', 'Admin\DegreesController@store');
Route::get('admin/editdegree/{num}', 'Admin\DegreesController@edit');
Route::post('admin/editdegree/{num}', 'Admin\DegreesController@edit');
Route::get('admin/deletedegree/{num}', 'Admin\DegreesController@delete');

//College book
Route::get('admin/collegeBookCreate', 'Admin\CollegeBooksController@create');
Route::post('admin/collegeBookStore', 'Admin\CollegeBooksController@store');
Route::any('admin/collegeBooks/edit/{num}', 'Admin\CollegeBooksController@edit');
Route::post('collegeBooks/edit/file', 'Admin\CollegeBooksController@fileUpload');
Route::get('collegeBooks/deleteFile/{id}/{book_id}/{type}', 'Admin\CollegeBooksController@deleteFile');
Route::post('collegeBooks/edit/banner', 'Admin\CollegeBooksController@uploadBanner');
Route::get('collegeBooks/delete/{num}', 'Admin\CollegeBooksController@delete');

// Admin offers
Route::any('admin/editOffer/{num}', 'Admin\ManageOffereController@edit');